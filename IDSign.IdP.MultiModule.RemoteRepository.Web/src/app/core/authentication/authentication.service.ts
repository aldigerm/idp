import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { UserProfile } from '@models/user';
import { BrowserStorage } from '@services/storage/browser-storage';
import { SiteCookieService } from '@services/storage/site-cookie.service';
import { UserCacheService } from '@services/storage/user-cache.service';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { ISubscription } from 'rxjs-compat/Subscription';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { share } from 'rxjs/operators';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';


@Injectable()
export class AuthenticationService implements OnDestroy {


  isDebug = true;
  eventsSubscribe: ISubscription;
  private ready = new BehaviorSubject<boolean>(false);

  constructor(
    private browserStorage: BrowserStorage,
    private oauthService: OAuthService,
    private userCacheService: UserCacheService,
    private cookieService: SiteCookieService,
    private logger: LoggerService,
    private router: Router,
    private siteAlertService: SweetAlertService
  ) {

    // create logging instance
    this.logger = this.logger.createInstance('AuthenticationService');

  }

  public getOAuthService(): OAuthService {
    return this.oauthService;
  }

  private setOAuthService(service: OAuthService) {
    this.oauthService = service;
  }

  public signinImplicitly() {
    this.logger.debug('Signing in implicitly. Reconfiguring and loading document...');
    this.getOAuthService().initImplicitFlow(null,
      {
        tenantCode: environment.Settings.currentClient.companyCode,
        projectCode: environment.Settings.currentClient.projectCode
      }
    );
  }

  public signout(): void {
    this.userCacheService.removeUser();
    this.removeBearerToken();
    if (this.hasValidToken()) {
      this.getOAuthService().logOut(false);
    } else {
      this.router.navigate(['/login']);
    }
  }

  public hasValidToken(): boolean {
    return this.getOAuthService().hasValidIdToken();
  }
  public isSignedIn(): Promise<string> {
    return new Promise<string>((resolve, reject) => {

      const authenticated = this.hasValidToken(); // && this.tokenNotExpired()
      if (this.isDebug) {

        this.logger.debug('Valid Access Token -> ' + this.hasValidToken()
          + ' token not expired -> ' + this.tokenNotExpired()
          + ' token expiration -> ' + this.getOAuthService().getIdTokenExpiration()
          + ' Access Token -> ' + this.getOAuthService().getAccessToken());
        this.logger.debug('Current user is authenticate ? Answer => ' + authenticated);
      }

      if (!authenticated) {
        this.userCacheService.removeUser();
        this.removeBearerToken();
        resolve(null);
      } else {
        const claims = this.getOAuthService().getIdentityClaims();
        this.logger.debug('Claims: ' + JSON.stringify(claims));
        if (claims === null || claims === undefined) {
          this.logger.error('User is logged in but the claims are null. Check if the issuer of the tokens'
            + ' is the same as the idp url. The url is case-sensitive.');
          this.removeBearerToken();
          resolve(null);
        } else {
          const username = (claims as UserProfile).sub;
          this.logger.debug('User: ' + username);
          this.setBearerToken(this.getOAuthService().getAccessToken());
          resolve(username);
        }
      }
    });
  }

  public subscribeSettingsLoaded(): Observable<boolean> {
    return this.ready;
  }

  public automaticLogin(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {

      this.getOAuthService().tryLogin()
        .then(_ => {
          if (!this.hasValidToken()) {
            this.signinImplicitly();
            resolve(false);
          } else {
            this.logger.debug('LOGGED IN -> CLAIMS:', this.getOAuthService().getIdentityClaims());
            resolve(true);
          }
        })
        .catch(error => {
          // if for some reason the discovery document gave an error, then we log out
          this.logger.error('Discovery document load failed on automatic login attempt.', error);
          this.browserStorage.set(environment.Settings.browserStorageKeys.userLoadErrorKey, 'true');
          window.location.reload();
          reject();
        });
    });
  }

  public getBearerToken(): string {
    const token = this.cookieService.get(environment.Settings.defaultValues.accessTokenCookieName);
    return token;
  }

  public setBearerToken(token: string) {
    this.cookieService.set(environment.Settings.defaultValues.accessTokenCookieName, token);
  }

  public removeBearerToken() {
    this.cookieService.delete(environment.Settings.defaultValues.accessTokenCookieName);
  }

  public setRedirectUri(uri: string): void {
    this.browserStorage.set(environment.Settings.browserStorageKeys.redirectUri, uri);
  }

  public removeRedirectUri(): void {
    this.browserStorage.remove(environment.Settings.browserStorageKeys.redirectUri);
  }

  public getRedirectUri(): string {
    return this.browserStorage.get(environment.Settings.browserStorageKeys.redirectUri);
  }

  public configureWithNewConfigApi(): Promise<boolean> {

    const oauthServiceToConfigure = this.getOAuthService();

    if (this.eventsSubscribe) {
      this.eventsSubscribe.unsubscribe();
    }
    return new Promise<boolean>((resolve, reject) => {

      this.logger.debug('Configuring oauthservice');

      this.logger.debug('Settings are ' + JSON.stringify(environment.Settings.idPConfig));
      oauthServiceToConfigure.configure(environment.Settings.idPConfig);
      oauthServiceToConfigure.strictDiscoveryDocumentValidation = false;

      oauthServiceToConfigure.tokenValidationHandler = new JwksValidationHandler();

      // debug enabled if not production
      oauthServiceToConfigure.showDebugInformation = !environment.Settings.production;

      this.removeIdpError();

      // register events
      this.eventsSubscribe = oauthServiceToConfigure.events.pipe(share()).subscribe(e => {
        this.logger.debug('oauth/oidc event -> ' + e.type, e);
        switch (e.type) {
          case 'discovery_document_loaded':
            this.logger.debug('Discovery document loaded successfully.');
            break;
          case 'silently_refreshed':
            this.logger.trace('Before silent refresh ' + this.getBearerToken());
            this.setBearerToken(this.oauthService.getAccessToken());
            this.logger.trace('After silent refresh' + this.getBearerToken());
            this.logger.info('Silently refreshed token session.');
            break;
          case 'discovery_document_load_error':
            this.logger.error('Discovery document couldnt be loaded.', e);
            this.setIdpError(e);
            this.signout();
            break;
          case 'session_terminated':
            this.logger.warn('Your session has been terminated!');
            const current = this;
            this.siteAlertService.show(
              new SweetAlertRequest({
                type: SweetAlertType.Error,
                title: 'Authentication.PopUp.session_terminated.Title',
                message: 'Authentication.PopUp.session_terminated.Message',
                confirmButton: 'Authentication.PopUp.session_terminated.OkButton',
                successCallback: () => {
                  current.signout();
                }
              }));
            break;
          case 'invalid_nonce_in_state':
            this.logger.error('Invalid nonce state. Signing out.');
            this.signout();
            break;
          case 'token_validation_error':
            this.logger.error('Token validation error. Signing out.');
            this.signout();
            break;
          case 'token_expires':
            this.logger.debug('Token about to expire. Authomatic silent refresh should occur.');
            break;
          default:
            // this.logger.debug(e.type + ' not specifically handled.');
            this.logger.debug(e.type + ' not specifically handled.', e);
            break;
        }
      }, error => {
        this.logger.error('Error thrown by event subscriber ', error);
      });

      // set up silent refresh
      oauthServiceToConfigure.setupAutomaticSilentRefresh();

      // we load the discovery document
      oauthServiceToConfigure.loadDiscoveryDocument()
        .then(() => {
          this.logger.debug('Discovery document loaded successfully.');
          this.removeIdpError();
          this.ready.next(true);
          resolve(true);
        })
        .catch(error => {
          // if for some reason the discovery document gave an error, then we log out
          this.logger.error('Discovery document load failed.', error);
          this.setIdpError(error);
          this.signout();
          this.ready.next(false);
          reject(false);
        });

      // closing of promise
    });

  }

  private setIdpError(e: any) {
    this.browserStorage.setObject(environment.Settings.browserStorageKeys.idpError, e);
  }

  private removeIdpError() {
    this.browserStorage.remove(environment.Settings.browserStorageKeys.idpError);
  }

  public performSilentRefresh() {
    this
      .oauthService
      .silentRefresh()
      .then(info => this.logger.debug('refresh ok', info))
      .catch(err => this.logger.error('refresh error', err));
  }

  private tokenNotExpired(): boolean {
    const token: string = this.getOAuthService().getAccessToken();
    return token != null && (this.getOAuthService().getAccessTokenExpiration() > new Date().valueOf());
  }

  ngOnDestroy(): void {
    if (this.eventsSubscribe) {
      this.eventsSubscribe.unsubscribe();
    }
  }
}
