/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SiteChangeDetectorService } from './site-change-detector.service';

describe('Service: SiteChangeDetector', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteChangeDetectorService]
    });
  });

  it('should ...', inject([SiteChangeDetectorService], (service: SiteChangeDetectorService) => {
    expect(service).toBeTruthy();
  }));
});
