/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { FeatureControlService } from './feature-control.service';

describe('Service: FeatureControl', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeatureControlService]
    });
  });

  it('should ...', inject([FeatureControlService], (service: FeatureControlService) => {
    expect(service).toBeTruthy();
  }));
});
