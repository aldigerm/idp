import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { SiteCookieService } from './storage/site-cookie.service';

@Injectable()
export class SiteTranslateService {

    acceptedLangs = new Array<string>();

    constructor(
        private translate: TranslateService
        , private logger: LoggerService
        , private cookie: SiteCookieService
    ) {

        this.logger = this.logger.createInstance('SiteTranslateService');
    }


    public setLanguage(lang: string): void {
        if (!this.acceptedLangs || this.acceptedLangs.length === 0) {
            this.initialize();
        }

        let found = false;
        this.logger.debug('Trying to set language to ' + lang);
        this.acceptedLangs.forEach(acceptedLanguage => {
            if (acceptedLanguage === lang) {
                this.logger.debug('Setting language to ' + lang);
                this.translate.use(lang);
                this.cookie.set('lang', lang);
                found = true;
            }
        });

        if (found) {
            this.logger.info('Language set to ' + lang);
        } else {
            this.logger.error('Language couldnt be set to ' + lang + ' as it wasnt found in the accepted languages.');
        }
    }

    public getCookieValue(): string {
        return this.cookie.get('lang');
    }

    public getLanguage(): string {
        return this.translate.getBrowserLang();
    }

    public addLanguages(languages: string[]): void {
        languages.forEach(language => {
            this.acceptedLangs.push(language);
        });
        this.translate.addLangs(languages);
    }

    public initialize(): void {
        this.addLanguages(['en', 'it']);
        let browserLang = this.getCookieValue();
        if (!browserLang) {
            browserLang = this.getLanguage();
        }
        this.logger.debug('Language is stored ' + browserLang);
        this.setLanguage(browserLang.match(/en|it/) ? browserLang : 'en');
        this.logger.debug('Language used ' + this.getLanguage());
    }
}
