import { Injectable, Inject } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { APP_BASE_HREF } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SiteCookieService {

  private path = '/';
  private readonly domain = '';

  constructor(
    private cookieService: CookieService
    , private logger: LoggerService
    , @Inject(APP_BASE_HREF) private baseHref: string) {
    this.logger = this.logger.createInstance('SiteCookieService');
    this.path = baseHref || '/';
  }

  public set(name: string, value: string) {
    const expiryDay = new Date().setTime(new Date().getTime() + environment.Settings.defaultValues.cookieExpiryDays * 24 * 60 * 60 * 1000);
    this.cookieService.set(name, value, expiryDay
      , this.path, this.domain, true, 'Strict');
  }

  public get(name: string): string {
    return this.cookieService.get(name);
  }

  public delete(name: string) {
    this.cookieService.delete(name, this.path, this.domain);
  }

  public deleteAll() {
    this.logger.warn('Deleting all cookies');
    this.cookieService.deleteAll(this.path, this.domain);
  }

  public deleteDepricated(relativePath: string) {

    // deprecated cookies are those set in the past
    const deprecatedPath = (this.path + relativePath).replace('//', '/');

    // we get the values we want to keep
    const lang = this.get(environment.Settings.defaultValues.languageTokenCookieName);
    const auth = this.get(environment.Settings.defaultValues.accessTokenCookieName);
    const siteConfig = this.get(environment.Settings.defaultValues.siteConfigCookieName);
    const loggerLevel = this.get(environment.Settings.defaultValues.loggerLevelCookieName);

    // we delete the rest
    let path = '/';
    this.logger.debug('Deleting deprecated ' + path);
    this.cookieService.deleteAll(path, this.domain);

    path = deprecatedPath;
    this.logger.debug('Deleting deprecated ' + path);
    this.cookieService.deleteAll(path, this.domain);

    path = deprecatedPath + '/';
    this.logger.debug('Deleting deprecated ' + path);
    this.cookieService.deleteAll(path, this.domain);

    if (this.path.length > 1) {
      path = this.path.substring(0, this.path.length - 1);
      this.logger.debug('Deleting deprecated ' + path);
      this.cookieService.deleteAll(path, this.domain);
    }

    // we save what we want
    if (lang) {
      this.set(environment.Settings.defaultValues.languageTokenCookieName, lang);
    }
    if (auth) {
      this.set(environment.Settings.defaultValues.accessTokenCookieName, auth);
    }
    if (siteConfig) {
      this.set(environment.Settings.defaultValues.siteConfigCookieName, siteConfig);
    }
    if (loggerLevel) {
      this.set(environment.Settings.defaultValues.loggerLevelCookieName, loggerLevel);
    }
  }
}
