import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { BrowserStorage } from './browser-storage';
import { CacheService } from './cache.service';
import { User } from '@models/user';
import { of } from 'rxjs/observable/of';

@Injectable()
export class UserCacheService {

    userProfileKey = 'userprofile';

    constructor(
        private logger: LoggerService
        , private browserStorage: BrowserStorage
    ) {

        // create logging instance
        this.logger = this.logger.createInstance('UserCacheService');
    }

    public removeUser(): void {
        this.logger.debug('removing user');

        // remove from cache
        this.browserStorage.remove(this.userProfileKey);
    }

    public setUser(user: User) {
        this.removeUser();
        this.browserStorage.setObject(this.userProfileKey, user);
    }

    public getUser(): Promise<User> {
        return new Promise<User>((resolve, reject) => {

            // check brower storage
            const browserUser = this.browserStorage.getObject(this.userProfileKey) as User;
            if (browserUser) {
                this.logger.debug('User found in browser ' + JSON.stringify(browserUser));
                resolve(browserUser);
            } else {
                this.removeUser();
                reject();
            }
        }
        );
    }
}
