/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserCacheService } from './user-cache.service';

describe('Service: UserCache', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCacheService]
    });
  });

  it('should ...', inject([UserCacheService], (service: UserCacheService) => {
    expect(service).toBeTruthy();
  }));
});