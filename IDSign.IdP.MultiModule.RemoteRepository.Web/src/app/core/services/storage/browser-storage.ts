import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment.prod';

/**
 * Set the BrowserStorage to use the same storage in AppModule.
 */
@Injectable() export class BrowserStorage {

    private hasStorage: boolean;

    constructor(
        private logger: LoggerService
    ) {
        this.logger = this.logger.createInstance('BrowserStorage');
        this.hasStorage = typeof Storage !== 'undefined';
    }

    public get(key: string): string {
        if (this.hasStorage && localStorage.getItem(key)) {
            const value = localStorage.getItem(key);
            this.logger.debug('Returning ' + key + ' -> ' + value);
            return value;
        }

        return null;
    }

    public set(key: string, value: string): void {
        if (this.hasStorage) {
            localStorage.setItem(key, value);
        }
    }

    public getObject(key: string): any {

        if (this.hasStorage) {
            const item = localStorage.getItem(key);
            if (item) {
                return JSON.parse(item);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public setObject(key: string, value: any): void {
        if (this.hasStorage) {
            localStorage.setItem(key, JSON.stringify(value));
        }
    }

    public remove(key: string): void {
        localStorage.removeItem(key);
    }

    public removeAllSiteData() {
        this.remove(environment.Settings.browserStorageKeys.countries);
    }

}
