/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SiteCookieService } from './site-cookie.service';

describe('Service: SiteCookie', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteCookieService]
    });
  });

  it('should ...', inject([SiteCookieService], (service: SiteCookieService) => {
    expect(service).toBeTruthy();
  }));
});
