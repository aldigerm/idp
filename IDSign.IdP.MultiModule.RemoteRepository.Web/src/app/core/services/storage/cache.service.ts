
import { throwError as observableThrowError, Subject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { tap } from 'rxjs/operators';

interface CacheContent {
  expiry: number;
  value: any;
}

/**
 * Cache Service is an observables based in-memory cache implementation
 * Keeps track of in-flight observables and sets a default expiry for cached values
 * @export
 * @class CacheService
 */

@Injectable()
export class CacheService {
  private cache: Map<string, CacheContent> = new Map<string, CacheContent>();
  private inFlightObservables: Map<string, Subject<any>> = new Map<string, Subject<any>>();
  readonly DEFAULT_MAX_AGE: number = 300000;

  constructor(
    private logger: LoggerService) {

    // create logging instance
    this.logger = this.logger.createInstance('CacheService');
  }

  /**
   * Gets the value from cache if the key is provided.
   * If no value exists in cache, then check if the same call exists
   * in flight, if so return the subject. If not create a new
   * Subject inFlightObservable and return the source observable.
   */
  get(key: string, fallback?: Observable<any>, maxAge?: number): Observable<any> | Subject<any> {

    if (this.hasValidCachedValue(key)) {
      this.logger.trace(`%cGetting from cache ${key}`, 'color: green');
      return of(this.cache.get(key).value);
    }

    if (!maxAge) {
      maxAge = this.DEFAULT_MAX_AGE;
    }

    if (this.inFlightObservables.has(key)) {
      return this.inFlightObservables.get(key);
    } else if (fallback && fallback instanceof Observable) {
      this.inFlightObservables.set(key, new Subject());
      this.logger.trace(`%c Calling api for ${key}`, 'color: purple');
      return fallback.pipe(tap((value) => { this.set(key, value, maxAge); }));
    } else {
      return observableThrowError('Requested key is not available in Cache');
    }

  }

  /**
   * Sets the value with key in the cache
   * Notifies all observers of the new value
   */
  set(key: string, value: any, maxAge: number = this.DEFAULT_MAX_AGE): void {
    this.cache.set(key, { value: value, expiry: Date.now() + maxAge });
    this.notifyInFlightObservers(key, value);
  }

  /**
   * Checks if the a key exists in cache
   */
  has(key: string): boolean {
    return this.cache.has(key);
  }

  /**
 * Checks if the a key exists in cache
 */
  remove(key: string): boolean {
    this.logger.debug(`%cDeleting ${key}`, 'color: orange');
    let deleted = false;
    if (this.cache.has(key)) {
      deleted = this.cache.delete(key);
      this.logger.debug(`%cDeleted from cache ${key} => ${deleted}`, 'color: orange');
    }
    if (this.inFlightObservables.has(key)) {
      const deletedInflight = this.inFlightObservables.delete(key);
      this.logger.debug(`%cDeleted from inflight ${key} => ${deletedInflight}`, 'color: orange');
      deleted = deleted && deletedInflight;
    }
    return deleted;
  }

  /**
   * Remove all keys that match the pattern
   * @param pattern RegExp pattern
   */
  removeByPattern(pattern: string): Number {
    let totalDeleted = 0;

    // a pattern must be provided
    if (pattern) {
      const regexp = new RegExp(pattern);
      this.logger.debug(`%cDeleting with pattern ${pattern}`, 'color: orange');

      // get copy of keys to loop through
      this.cache.forEach((value: CacheContent, key: string) => {
        this.logger.debug('Testing key ' + key);

        // use pattern to see if the key mathes
        if (regexp.test(key)) {
          this.logger.debug('Deleting key ' + key);

          // delete the key
          if (this.remove(key)) {
            this.logger.debug('Deleted');
            totalDeleted++;
          }
        }
      });
    }
    return totalDeleted;
  }

  /**
   * Publishes the value to all observers of the given
   * in progress observables if observers exist.
   */
  private notifyInFlightObservers(key: string, value: any): void {
    if (this.inFlightObservables.has(key)) {
      const inFlight = this.inFlightObservables.get(key);
      const observersCount = inFlight.observers.length;
      if (observersCount) {
        this.logger.debug(`%cNotifying ${inFlight.observers.length} flight subscribers for ${key}`, 'color: blue');
        inFlight.next(value);
      }
      inFlight.complete();
      this.inFlightObservables.delete(key);
    }
  }

  /**
   * Checks if the key exists and   has not expired.
   */
  private hasValidCachedValue(key: string): boolean {
    if (this.cache.has(key)) {
      if (this.cache.get(key).expiry < Date.now()) {
        this.cache.delete(key);
        return false;
      }
      return true;
    } else {
      return false;
    }
  }
}
