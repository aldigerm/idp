import { Injectable } from '@angular/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';

@Injectable()
export class SweetAlertService {

    private request = new BehaviorSubject<SweetAlertRequest>(null);

    constructor(
    ) {
        this.state().subscribe(request => {
            console.log('New swal to show is ' + JSON.stringify(request));
        });
    }

    public close() {
        this.request.next(new SweetAlertRequest({
            type: SweetAlertType.Close
        }));
    }

    public show(request: SweetAlertRequest): void {
        this.request.next(request);
    }

    public state(): Observable<SweetAlertRequest> {
        return this.request;
    }

}
