import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
declare const $: any;

@Injectable()
export class TableService {

    private _searchSuffix = '#search';
    private _footableTables = new Array<String>();
    private _footableSearchInputs = new Array<String>();
    searchTimeout = setTimeout(function () { }, 0);

    constructor(
        private _logger: LoggerService
    ) {
        this._logger = _logger.createInstance('TableService');
    }

    public restartPagination(tableSuffix: string) {
        // needed to restart the paging
        $(this._searchSuffix + tableSuffix).val('');
        $(this._searchSuffix + tableSuffix).trigger('input');
    }

    public initTable(footableSuffix: string) {
        const prefix = '#table';
        const tableId = prefix + footableSuffix;
        this._logger.trace('Initiating footable ' + tableId);

        this._logger.trace('Triggering filter footable');

        try {
            $(tableId).footable();

            this._afterVisibleCheck(tableId);

            let found = false;
            this._footableTables.forEach(table => {
                if (!found && tableId === table) {
                    found = true;
                }
            });

            if (!found) {
                this._footableTables.push(tableId);
            }

            found = false;
            const searchId = this._searchSuffix + footableSuffix;

            this._logger.trace('Initiating search with id ' + searchId);
            const parent = this;

            this._footableSearchInputs.forEach(search => {
                if (!found && searchId === search) {
                    found = true;
                }
            });
        } catch (error) {
            this._logger.debug('Error thrown white intialisng table', 'color:red');
        }
    }

    public search(value: string, footableSuffix: string, waitForEndOfInput?: number) {
        const prefix = '#table';
        const tableId = prefix + footableSuffix;
        const searchId = this._searchSuffix + footableSuffix;
        clearTimeout(this.searchTimeout);
        const parent = this;
        if (!waitForEndOfInput) {
            waitForEndOfInput = 0;
        }
        this.searchTimeout = setTimeout(function () {
            parent._logger.debug('Searching for ' + value);
            $(tableId).trigger('footable_filter', { filter: value });
        }, waitForEndOfInput);
    }

    private _afterVisibleCheck(tableId: string) {
        const maxTime = 3000; // 3 seconds
        let time = 0;
        const parent = this;
        const interval = setInterval(function () {
            if ($(tableId).is(':visible')) {
                clearInterval(interval);
                // visible, do something
                try {
                    $(tableId).trigger('footable_resize');
                    $(tableId).trigger('footable_redraw');
                } catch (error) {
                    parent._logger.debug('Error thrown by table resize/redraw', 'color:red');
                }
                parent._logger.debug('Table visible after ' + time + ' ms. Reinitiating.');
            } else {
                if (time > maxTime) {
                    // still hidden, after 2 seconds, stop checking
                    parent._logger.warn('Table not visible after ' + maxTime + ' ms. Resizing not occured.');
                    clearInterval(interval);
                    return;
                }

                parent._logger.trace('Table not visible...');
                // not visible yet, do something
                time += 200;
            }
        }, 200);
    }
}
