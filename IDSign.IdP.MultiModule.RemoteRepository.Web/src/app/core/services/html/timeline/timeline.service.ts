import { LoggerService } from '@app/core/logging/logger.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { WindowEventListenerService } from './../../window-listener.service';
import { Injectable, ElementRef } from '@angular/core';

@Injectable()
export class TimelineService {

    timelineComponentsGlobal = {};
    windowListener: ISubscription;
    initializing = false;

    constructor(
        private _windowListener: WindowEventListenerService
        , private _logger: LoggerService) {
        this._logger = _logger.createInstance('TimelineService');
    }

    public initialize(container: ElementRef) {
        if (this.windowListener) {
            this.windowListener.unsubscribe();
        }

        this.windowListener = this._windowListener.OnResizeAndMenu().subscribe(value => {
            if (value) {
                this.initTimeline(container);
            }
        });

        // needed to wait for dom to update
        this.initTimeline(container);
    }

    public next() {
        this.showNewContent(this.timelineComponentsGlobal, this.timelineComponentsGlobal['timelineTotWidth'], 'next');
    }

    public previous() {
        this.showNewContent(this.timelineComponentsGlobal, this.timelineComponentsGlobal['timelineTotWidth'], 'prev');
    }

    public itemClick(label, transitionCard?) {
        const link = $(this.timelineComponentsGlobal['eventsWrapper']).find('a[data-date="' + label + '"]');
        this._logger.debug('Clicked ' + $(link).attr('data-date'));
        this.showEvent('next', link, this.timelineComponentsGlobal, this.timelineComponentsGlobal['timelineTotWidth'], transitionCard);
    }

    initialiseSingleTimeline(timeline) {
        const parent = this;
        const timelineComponents = {};
        // cache timeline components
        timelineComponents['timeline'] = timeline;
        timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
        timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
        timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
        timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
        timelineComponents['timelineDates'] = parent.parseDate(timelineComponents['timelineEvents']);
        timelineComponents['eventsMinLapse'] = parent.minLapse(timelineComponents['timelineDates']);
        timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
        timelineComponents['eventsContent'] = timeline.children('.events-content');
        timelineComponents['eventWidth'] = $($(timelineComponents['timelineEvents']).get(0)).outerWidth();
        /*

        Uncomment for date relative spacing

        // assign a left postion to the single events along the timeline
        setDatePosition(timelineComponents, eventsMinDistance);

        */
        parent.setDatePositionByElement(timelineComponents, timelineComponents['eventWidth']);

        // assign a width to the timeline

        /*
        This sets timelines distances relative to date
        const timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);

        */

        const timelineTotWidth = parent.setTimelineWidthByElement(timelineComponents, timelineComponents['eventWidth']);
        timelineComponents['timelineTotWidth'] = timelineTotWidth;

        // the timeline has been initialize - show it
        timeline.addClass('loaded');

        parent.timelineComponentsGlobal = timelineComponents;

        setTimeout(() => {
            parent.clickCurrentStep(timeline, true);
            parent.initializing = false;
        });

        return timelineComponents;
    }

    initTimeline(container: ElementRef) {
        // we do not reinitialize if there is one already running
        if (this.initializing) {
            this._logger.debug('Already initialzing.');
            return;
        }

        this.initializing = true;
        const parent = this;
        setTimeout(() => {
            const timelines = $(container.nativeElement).find('.cd-horizontal-timeline');
            const timelinesLink = $(container.nativeElement).find('.cd-horizontal-timeline .timeline a');
            const timelinesContent = $(container.nativeElement).find('.cd-horizontal-timeline .events-content li');
            if (!(timelines.length > 0 && timelinesLink.length > 0 && timelinesContent.length > 0)) {
                parent._logger.debug('Timelines not found...');
                parent.initializing = false;
                return;
            }

            parent._logger.debug('Timelines found.');
            $.each(timelines, function (index, timelineItem) {
                const timeline = $(timelineItem),
                    timelineComponents = parent.initialiseSingleTimeline(timeline);
            });
        });
    }

    timelineReset() {
        if (this.timelineComponentsGlobal && this.timelineComponentsGlobal['timeline']) {
            setTimeout(() => {
                this.initialiseSingleTimeline(this.timelineComponentsGlobal['timeline']);
                // const newEvent = this.timelineComponentsGlobal['eventsWrapper'].find('.selected');
                // this.showEvent('next', newEvent, this.timelineComponentsGlobal, this.timelineComponentsGlobal['timelineTotWidth']);
            });
        }
    }

    clickCurrentStep(timeline, transitionCard?) {
        // we click either the currently selected or the originally assigned first step
        const currentStep = $(timeline).find('a.current-step').attr('data-date');
        if (currentStep) {
            this.itemClick(currentStep, transitionCard);
        }
    }

    clickSelectedStep(timeline) {
        // we click either the currently selected or the originally assigned first step
        const currentStep = $(timeline).find('a.selected').attr('data-date');
        if (currentStep) {
            this.itemClick(currentStep);
        }
    }

    showHideNavigation(timelineComponents) {
        const timelineWidth = $(timelineComponents['timeline']).find('.timeline').innerWidth();
        const totalWidth = Number(timelineComponents['timelineTotWidth']);

        this._logger.debug('Timeline is ' + timelineWidth + '. Total is ' + totalWidth);

        // resize wrapper to set it in middle
        $(timelineComponents['timelineWrapper']).css('width', (totalWidth) + 'px');
        if (timelineWidth < totalWidth) {
            $(timelineComponents['timelineNavigation']).show();
            this._logger.debug('Showing timeline navigation.');
        } else {
            $(timelineComponents['timelineNavigation']).hide();

            // slide contents to te left
            this.setTransformValue(timelineComponents['eventsWrapper'].get(0), 'translateX', '0px');
            this._logger.debug('Hiding timeline navigation.');
        }
    }

    updateSlide(timelineComponents, timelineTotWidth, string) {
        // retrieve translateX value of timelineComponents['eventsWrapper']
        const translateValue = this.getTranslateValue(timelineComponents['eventsWrapper']),
            wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));
        // translate the timeline to the left('next')/right('prev')
        (string === 'next') ?
            this.translateTimeline(timelineComponents, translateValue - wrapperWidth + timelineComponents['eventWidth'],
                wrapperWidth - timelineTotWidth) :
            this.translateTimeline(timelineComponents, translateValue + wrapperWidth - timelineComponents['eventWidth']
                , wrapperWidth - timelineTotWidth);
    }

    showNewContent(timelineComponents, timelineTotWidth, string) {
        // go from one event to the next/previous one
        const visibleContent = timelineComponents['eventsContent'].find('.selected'),
            newContent = (string === 'next') ? visibleContent.next() : visibleContent.prev();

        if (newContent.length > 0) { // if there's a next/prev event - show it
            const selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
                newEvent = (string === 'next') ? selectedDate.parent('li').next('li')
                    .children('a') : selectedDate.parent('li').prev('li').children('a');
            this.showEvent(string, newEvent, timelineComponents, timelineTotWidth);
        }
    }

    showEvent(string, newEvent, timelineComponents, timelineTotWidth, transitionCard?) {

        if (transitionCard) {
            this.updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
            this.updateVisibleContent(newEvent, timelineComponents['eventsContent']);
            $(timelineComponents['eventsWrapper']).find('a').removeClass('selected');
            newEvent.addClass('selected');
            this.updateOlderEvents(newEvent);
        }

        this.updateTimelinePosition(string, newEvent, timelineComponents);
    }

    updateTimelinePosition(string, event, timelineComponents) {
        // translate timeline to the left/right according to the position of the selected event
        const eventStyle = this.getComputedStyle(event.get(0), null),
            // eventLeft = Number(eventStyle.getPropertyValue('left').replace('px', '')),
            eventLeft = Number(this.getPropertyValue(eventStyle, 'left').replace('px', '')),
            eventWidth = $(event.get(0)).outerWidth(),
            buttonWidth = $(timelineComponents['timelineNavigation']).find('.prev').outerWidth(),
            timelineWidth = $(timelineComponents['timelineWrapper']).outerWidth() - $(timelineComponents['timelineWrapper']).offset().left,
            timelineTotWidth = buttonWidth + eventWidth + $(timelineComponents['eventsWrapper'])
                .outerWidth() - $(timelineComponents['eventsWrapper']).offset().left;
        const timelineTranslate = this.getTranslateValue(timelineComponents['eventsWrapper']);

        const mq = this.checkMQ();
        if (mq === 'mobile' || (string === 'next' && eventLeft > timelineWidth - timelineTranslate)
            || (string === 'prev' && eventLeft < -timelineTranslate)) {
            this.translateTimeline(timelineComponents,
                -(eventLeft + (eventWidth) / 2) + (timelineWidth + eventWidth / 2 + buttonWidth) / 2,
                timelineWidth - timelineTotWidth);
        }
    }

    translateTimeline(timelineComponents, value, totWidth) {
        const eventsWrapper = timelineComponents['eventsWrapper'].get(0);
        value = (value > timelineComponents['eventWidth']) ? timelineComponents['eventWidth'] / 2 : value; // only negative translate value
        value = (!(typeof totWidth === 'undefined') && value < totWidth) ? totWidth : value; // do not translate more than timeline width
        this.setTransformValue(eventsWrapper, 'translateX', value + 'px');

        // update navigation arrows visibility
        (value === 0) ? timelineComponents['timelineNavigation'].find('.prev')
            .addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
        (value === totWidth) ? timelineComponents['timelineNavigation'].find('.next')
            .addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
    }

    updateFilling(selectedEvent, filling, totWidth) {
        // change .filling-line length according to the selected event
        const eventStyle = this.getComputedStyle(selectedEvent.get(0), null),
            eventWidth = this.getPropertyValue(eventStyle, 'width');
        let eventLeft = this.getPropertyValue(eventStyle, 'left');
        eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', '')) / 2;
        const scaleValue = eventLeft / totWidth;
        this.setTransformValue(filling.get(0), 'scaleX', scaleValue);
    }

    setDatePosition(timelineComponents, min) {
        for (let i = 0; i < timelineComponents['timelineDates'].length; i++) {
            const distance = this.daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
                distanceNorm = Math.round(distance / timelineComponents['eventsMinLapse']) + 2;
            timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm * min + 'px');
        }
    }

    setDatePositionByElement(timelineComponents, min) {
        for (let i = 0; i < timelineComponents['timelineDates'].length; i++) {
            const distanceNorm = i;
            timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm * min + 'px');
        }
    }

    setTimelineWidth(timelineComponents, width) {
        let timeSpanNorm = <any>{};
        const timeSpan = this.daydiff(timelineComponents['timelineDates'][0]
            , timelineComponents['timelineDates'][timelineComponents['timelineDates'].length - 1]);
        timeSpanNorm = timeSpan / timelineComponents['eventsMinLapse'],
            timeSpanNorm = Math.round(timeSpanNorm) + 4;
        const totalWidth = timeSpanNorm * width;
        timelineComponents['eventsWrapper'].css('width', totalWidth + 'px');
        this.updateFilling(timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents['fillingLine'], totalWidth);
        this.updateTimelinePosition('next', timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents);

        return totalWidth;
    }

    setTimelineWidthByElement(timelineComponents, width) {
        const totalWidth = width * timelineComponents['timelineEvents'].length;
        timelineComponents['eventsWrapper'].css('width', totalWidth + 'px');
        this.updateFilling(timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents['fillingLine'], totalWidth);
        this.updateTimelinePosition('next', timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents);

        return totalWidth;
    }

    updateVisibleContent(event, eventsContent) {
        let classEnetering, classLeaving = '';
        const eventDate = event.data('date'),
            visibleContent = eventsContent.find('.selected'),
            selectedContent = eventsContent.find('[data-date="' + eventDate + '"]');

        if (selectedContent.index() > visibleContent.index()) {
            classEnetering = 'selected enter-right',
                classLeaving = 'leave-left';
        } else if (selectedContent.index() < visibleContent.index()) {
            classEnetering = 'selected enter-left',
                classLeaving = 'leave-right';
        }

        if (classEnetering && classLeaving) {
            // only show/hide if they are different contents
            if (visibleContent.data('date') !== selectedContent.data('date')) {
                selectedContent.attr('class', classEnetering);
                visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
                    visibleContent.removeClass('leave-right leave-left');
                    selectedContent.removeClass('enter-left enter-right');
                });
            }
            const selectedContentHeight = $(selectedContent).outerHeight(true) + 'px';
            eventsContent.css('height', selectedContentHeight);
            const minHeight = Number(eventsContent.css('min-height').replace('px', ''));
            if (minHeight === 0) {
                // this was required do to height not being rendered when switcing between tabs
                this._logger.debug('Setting min-height of events content to ' + selectedContentHeight);
                eventsContent.css('min-height', selectedContentHeight);
            }

        }
    }

    updateOlderEvents(event) {
        event.parent('li').prevAll('li').children('a').addClass('older-event')
            .end().end().nextAll('li').children('a').removeClass('older-event');
    }

    getTranslateValue(timeline) {
        let timelineTranslate, translateValue = <any>{};
        const timelineStyle = this.getComputedStyle(timeline.get(0), null);
        timelineTranslate = this.getPropertyValue(timelineStyle, '-webkit-transform') ||
            this.getPropertyValue(timelineStyle, '-moz-transform') ||
            this.getPropertyValue(timelineStyle, '-ms-transform') ||
            this.getPropertyValue(timelineStyle, '-o-transform') ||
            this.getPropertyValue(timelineStyle, 'transform');

        if (timelineTranslate.indexOf('(') >= 0) {
            timelineTranslate = timelineTranslate.split('(')[1];
            timelineTranslate = timelineTranslate.split(')')[0];
            timelineTranslate = timelineTranslate.split(',');
            translateValue = timelineTranslate[4];
        } else {
            translateValue = 0;
        }

        return Number(translateValue);
    }

    setTransformValue(element, property, value) {
        if (element) {
            element.style['-webkit-transform'] = property + '(' + value + ')';
            element.style['-moz-transform'] = property + '(' + value + ')';
            element.style['-ms-transform'] = property + '(' + value + ')';
            element.style['-o-transform'] = property + '(' + value + ')';
            element.style['transform'] = property + '(' + value + ')';
        }
    }

    // based on http:// stackoverflow.com/questions/542938/how-do-i-get-the-number-of-days-between-two-dates-in-javascript
    parseDate(events) {
        let dayComp, dateComp, timeComp, singleDate = <any>{};
        const dateArrays = [];
        $.each(events, (index, item) => {
            singleDate = $(item),
                dateComp = singleDate.data('date').split('T');
            if (dateComp.length > 1) { // both DD/MM/YEAR and time are provided
                dayComp = dateComp[0].split('/'),
                    timeComp = dateComp[1].split(':');
            } else if (dateComp[0].indexOf(':') >= 0) { // only time is provide
                dayComp = ['2000', '0', '0'],
                    timeComp = dateComp[0].split(':');
            } else { // only DD/MM/YEAR
                dayComp = dateComp[0].split('/'),
                    timeComp = ['0', '0'];
            }
            const newDate = new Date(dayComp[2], dayComp[1] - 1, dayComp[0], timeComp[0], timeComp[1]);
            dateArrays.push(newDate);
        });
        return dateArrays;
    }

    daydiff(first, second) {
        return Math.round((second - first));
    }

    minLapse(dates) {
        // determine the minimum distance among events
        const dateDistances = [];
        for (let i = 1; i < dates.length; i++) {
            const distance = this.daydiff(dates[i - 1], dates[i]);
            dateDistances.push(distance);
        }
        return Math.min.apply(null, dateDistances);
    }

    /*
        How to tell if a DOM element is visible in the current viewport?
        http:// stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
    */
    elementInViewport(el) {
        let top = el.offsetTop;
        let left = el.offsetLeft;
        const width = el.offsetWidth;
        const height = el.offsetHeight;

        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
        );
    }

    checkMQ() {
        if (window.matchMedia && window.matchMedia('(max-width: 767px)').matches) {
            this._logger.trace('This is mobile');
            return 'mobile';
        } else {
            this._logger.trace('This is desktop');
            return 'desktop';
        }
    }

    getPropertyValue(styles, name) {
        if (!styles) {
            return '';
        }
        return styles[name] || '';
    }


    // from : https:// dzone.com/articles/look-mom-no-jquery-getting-all
    getComputedStyle(dom, args) {
        let style;
        const returns = {};
        if (!dom) {
            return returns;
        }
        if (dom[0]) {
            dom = dom[0];
        }
        // FireFox and Chrome way
        if (window.getComputedStyle) {
            style = window.getComputedStyle(dom, null);
            for (let i = 0, l = style.length; i < l; i++) {
                const prop = style[i];
                const val = style.getPropertyValue(prop);
                returns[prop] = val;
            }
            return returns;
        }
        // IE and Opera way
        if (dom.currentStyle) {
            style = dom.currentStyle;
            for (const prop in style) {
                if (prop) {
                    returns[prop] = style[prop];
                }
            }
            return returns;
        }
        // Style from style attribute
        if (style = dom.style) {
            for (const prop in style) {
                if (typeof style[prop] !== 'function') {
                    returns[prop] = style[prop];
                }
            }
            return returns;
        }
        return returns;
    }
}
