/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { QueryStringUtilitiesService } from './query-string-utilities.service';

describe('Service: QueryStringUtilities', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryStringUtilitiesService]
    });
  });

  it('should ...', inject([QueryStringUtilitiesService], (service: QueryStringUtilitiesService) => {
    expect(service).toBeTruthy();
  }));
});