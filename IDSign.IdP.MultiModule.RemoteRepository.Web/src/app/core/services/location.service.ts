import { Observable ,  BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable()
export class LocationService {

    private stateChanges = new BehaviorSubject<string>(null);

    constructor(
        private _location: Location) { }

    public replaceState(newUrl: string): void {
        this._location.replaceState(newUrl);
        this.stateChanges.next(newUrl);
    }

    public path(state: boolean): string {
        return this._location.path(state);
    }

    public replaceStateChange(): Observable<string> {
        return this.stateChanges;
    }
}
