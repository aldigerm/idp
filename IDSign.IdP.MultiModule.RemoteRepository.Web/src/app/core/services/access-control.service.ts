import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { ModuleCode } from '@models/module-code';

@Injectable()
export class AccessControlService {

    allowedModules = [''];

    constructor(
        private logger: LoggerService
    ) {
        this.logger = this.logger.createInstance('AccessControlService');
    }

    setAllowedModules(modules: string[]): void {
        this.allowedModules = modules;
        this.logger.debug('Allowed modules are ' + JSON.stringify(this.allowedModules));
    }

    checkAccessByCode(moduleCode: ModuleCode): boolean {
        return this.checkAccess(ModuleCode[moduleCode]);
    }

    checkAccess(moduleCode: string): boolean {
        let access: boolean;
        this.logger.debug('Checking access for ' + moduleCode + ' in list ' + JSON.stringify(this.allowedModules));
        if (this.isStringInArray(ModuleCode[ModuleCode.SUPERUSER], this.allowedModules)) {
            access = true;
        } else if (this.isStringInArray(moduleCode, this.allowedModules)) {
            access = true;
        } else {
            access = false;
        }
        this.logger.debug('Access for ' + moduleCode + ' is ' + access);
        return access;
    }

    isStringInArray(str: string, strArray: string[]): boolean {
        let access = false;
        if (strArray) {
            for (let j = 0; j < strArray.length; j++) {
                if (strArray[j].match(str)) {
                    access = true;
                }
            }
        }
        return access;
    }

    checkAccessForUrl(url: string): boolean {
        let hasAccess = false;
        if (!url || url === '') {
            hasAccess = true;
            return hasAccess;
        } else {
            this.logger.debug('Checking access for ' + url);
            if (url === '') {
                hasAccess = true;
                return hasAccess;
            } else {
                this.getAccessibleUrlList().forEach(allowedUrl => {
                    this.logger.debug('Checking if starts with  ' + allowedUrl);
                    if (url.startsWith(allowedUrl)) {
                        this.logger.debug('Ok');
                        hasAccess = true;
                        return hasAccess;
                    }
                });
                if (!hasAccess) {
                    this.logger.error(url + ' not allowed');
                }
                return hasAccess;
            }
        }
    }

    checkAccessForSite(): boolean {
        return this.getAccessibleUrlList().length > 0;
    }

    getFirstAccessibleUrl(): string {
        const list = this.getAccessibleUrlList();
        if (list) {
            return this.getAccessibleUrlList()[0];
        } else {
            return null;
        }
    }

    getAccessibleUrlList(): Array<string> {
        const urls = new Array<string>();
        // empty url is accessible
        if (this.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_VIEW)) {
            urls.push('/users/detail');
        }
        if (this.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)) {
            urls.push('/users/list');
        }
        urls.push('/');
        return urls;
    }

}
