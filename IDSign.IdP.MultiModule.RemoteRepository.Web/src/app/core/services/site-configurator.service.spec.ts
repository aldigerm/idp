/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SiteConfiguratorService } from './site-configurator.service';

describe('Service: SiteConfigurator', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteConfiguratorService]
    });
  });

  it('should ...', inject([SiteConfiguratorService], (service: SiteConfiguratorService) => {
    expect(service).toBeTruthy();
  }));
});