/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SiteTranslateService } from './site-translate.service';

describe('Service: IdSignTranslate', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteTranslateService]
    });
  });

  it('should ...', inject([SiteTranslateService], (service: SiteTranslateService) => {
    expect(service).toBeTruthy();
  }));
});