import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { LoggerService } from '@app/core/logging/logger.service';

@Injectable()
export class FeatureControlService {

    enabledFeatures: string[];

    constructor(
        private logger: LoggerService) {
        this.logger = this.logger.createInstance('FeatureControlService');
    }

    private isStringInArray(str: string, strArray: string[]): boolean {
        let access = false;
        for (let j = 0; j < strArray.length; j++) {
            if (strArray[j] && strArray[j].match(str)) {
                access = true;
            }
        }
        return access;
    }

    public isFeatureEnabled(featureName: string): boolean {
        if (environment.Settings &&
            environment.Settings.featureControl &&
            environment.Settings.featureControl.allowedFeatures) {
            return this.isStringInArray(featureName, environment.Settings.featureControl.allowedFeatures);
        } else {
            return false;
        }
    }
}
