import { Injectable } from '@angular/core';

@Injectable()
export class QueryStringUtilitiesService {

    constructor() { }

    public removeURLParameter(url, parameter): string {
        // prefer to use l.search if you have a location/link object
        const urlparts = url.split('?');
        if (urlparts.length >= 2) {

            const prefix = encodeURIComponent(parameter) + '=';
            const pars = urlparts[1].split(/[&;]/g);

            // reverse iteration as may be destructive
            for (let i = pars.length; i-- > 0; ) {
                // idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
            return url;
        } else {
            return url;
        }
    }

    public getParameterByName(url, name): string {
        name = name.replace(/[\[\]]/g, '\\$&');
        const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) { return null; }
        if (!results[2]) { return ''; }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
}
