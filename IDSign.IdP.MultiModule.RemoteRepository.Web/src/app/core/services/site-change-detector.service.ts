import { Injectable, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { LoggerService } from '../logging/logger.service';

@Injectable({
  providedIn: 'root'
})
export class SiteChangeDetectorService {

  constructor() {
  }

  public detectChanges(changeDetectionRef: ChangeDetectorRef) {
    if (!changeDetectionRef['destroyed']) {
      changeDetectionRef.detectChanges();
    }
  }
}
