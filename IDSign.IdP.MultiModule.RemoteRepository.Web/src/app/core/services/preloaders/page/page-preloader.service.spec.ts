/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PagePreloaderService } from './page-preloader.service';

describe('Service: PagePreloader', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PagePreloaderService]
    });
  });

  it('should ...', inject([PagePreloaderService], (service: PagePreloaderService) => {
    expect(service).toBeTruthy();
  }));
});