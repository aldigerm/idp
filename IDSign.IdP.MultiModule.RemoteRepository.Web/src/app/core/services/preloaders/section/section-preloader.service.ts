import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject ,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';

@Injectable()
export class SectionPreloaderService {


    private shownState = new BehaviorSubject<boolean>(true);
    private shownMessage = new BehaviorSubject<string>('');

    constructor(
        private logger: LoggerService,
        private translate: TranslateService
    ) {
        this.logger = this.logger.createInstance('SectionPreloaderService');
    }


    public show(): void {
        this.logger.debug('Showing section preloader');
        this.logger.debug('New section preloader state triggered is true');
        this.shownState.next(true);
    }
    public hide(): void {
        this.logger.debug('Hiding section preloader');
        this.logger.debug('New section preloader state triggered is false');
        this.shownState.next(false);
        this.setMessage('');
    }

    public state(): Observable<boolean> {
        return this.shownState;
    }

    public setMessage(message: string) {
        this.logger.debug('Setting message : ' + message);
        this.shownMessage.next(message);
    }

    public setLocalisedMessage(message: string) {
        if (message) {
            this.translate.get(message)
                .subscribe(value => {
                    this.logger.debug('Setting localized message : ' + value);
                    this.setMessage(value);
                });
        } else {
            this.logger.debug('Setting message to empty');
            this.setMessage(message);
        }
    }

    public message(): Observable<string> {
        return this.shownMessage;
    }

}
