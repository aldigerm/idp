/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SectionPreloaderService } from './section-preloader.service';

describe('Service: PagePreloader', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SectionPreloaderService]
    });
  });

  it('should ...', inject([SectionPreloaderService], (service: SectionPreloaderService) => {
    expect(service).toBeTruthy();
  }));
});
