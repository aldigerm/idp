import { Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { SiteAction } from '@models/site-allowed-actions';
import { AllowedSiteActions } from '@app/shared/models/site-allowed-actions';

@Injectable()
export class SiteActionControlService {
    private allowedActions: AllowedSiteActions;
    constructor(
        private _logger: LoggerService) {
        this._logger = this._logger.createInstance('SiteActionControlService');
    }

    public setSiteActions(allowedActions: AllowedSiteActions) {
        this.allowedActions = allowedActions;
    }

    public isAllowed(action: SiteAction) {
        if (this.allowedActions && this.allowedActions.actions) {
            this._logger.debug('Checking if action ' + action + ' (' + SiteAction[action]
                + ') applies in ' + JSON.stringify(this.allowedActions));
            if (this.allowedActions.actions.filter(a => a === action).length > 0) {
                this._logger.debug('Action allowed.');
                return true;
            } else {
                this._logger.debug('Action not allowed.');
                return false;
            }
        } else {
            this._logger.debug('Action couldnt be checked due to undefined model.');
            return false;
        }
    }
}
