import { Injectable, EventEmitter } from '@angular/core';
import { IdSignConfig, UserInterface } from '@models/idsign-config';
import { IdpClientCredentials, IdpClientCredentialsList } from '@app/core/configs/global';
import { LoggerService } from '@app/core/logging/logger.service';
import { QueryStringUtilitiesService } from './query-string-utilities.service';
import { SiteTranslateService } from './site-translate.service';
import { environment } from '@environments/environment';
import { SiteCookieService } from '@services/storage/site-cookie.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SiteConfiguratorService {

    cookieSuffix_signingMode = '_sm';
    cookieSuffix_internationalUser = '_iu';
    cookieSuffix_fiscalCode = '_fc';
    cookieSuffix_viewSuite = '_vs';
    cookieSuffix_viewTheme = '_vt';
    configuration: IdSignConfig;

    private configurationObservable = new EventEmitter<IdSignConfig>();

    constructor(
        private logger: LoggerService
        , private cookieService: SiteCookieService
        , private queryStringUtilitiesService: QueryStringUtilitiesService
        , private siteTranslateService: SiteTranslateService) {
        this.logger = this.logger.createInstance('SiteConfiguratorService');
    }

    public configure(): Promise<IdSignConfig> {
        return new Promise<IdSignConfig>((resolve, reject) => {
            try {
                this.logger.debug('%cSetting configuration', 'color:green');

                let configuration: IdSignConfig = null;

                // lets check for the configuration in the url
                const siteConfigValue = this.queryStringUtilitiesService
                    .getParameterByName(window.location.href, environment.Settings.defaultValues.siteConfigParameterName);
                if (siteConfigValue) {

                    // there is a parameter in json encoded
                    // we decode
                    const decoded: any = decodeURI(siteConfigValue);
                    this.logger.debug('%cDecoded site configuration : ' + decoded, 'color:green');

                    // and parse the values
                    configuration = new IdSignConfig();
                    configuration = JSON.parse(decoded);
                    this.logger.info('%cConfiguration found in url : ' + JSON.stringify(configuration), 'color:green');

                } else {

                    // else check for a companyCode in the querystring
                    const companyCode = this.queryStringUtilitiesService
                        .getParameterByName(window.location.href, environment.Settings.defaultValues.companyCodeParameterName);

                    if (companyCode) {
                        this.logger.info('%Code found in url : ' + companyCode, 'color:green');

                        configuration = new IdSignConfig();
                        configuration.userInterface.viewTheme = companyCode;
                    }

                }

                const cookieConfiguration = configuration ? configuration
                    : this.getConfigurationFromCookieValues(environment.Settings.defaultValues.siteConfigCookieName);

                // the configuration is not in the parameter, so we get the cookie
                if (!this.isValidConfiguration(configuration) && this.isValidConfiguration(cookieConfiguration)) {
                    configuration = cookieConfiguration;
                    this.logger.info('%cConfiguration found in cookie : ' + JSON.stringify(configuration), 'color:green');
                }

                // if the configuration somehow wasn't initialised yet,
                // then set the defaults
                if (!this.isValidConfiguration(configuration)) {
                    configuration = this.getDefaultConfiguration();
                }
                this.setConfiguration(configuration);
                resolve(configuration);
            } catch (err) {
                this.logger.debug('Theme not loaded.', err);
                resolve(err);
            }
        });
    }

    public setConfigurationForCompany(companyCode: string) {
        const config = <IdSignConfig>{
            userInterface: <UserInterface>{
                viewTheme: companyCode
            }
        };
        this.setConfiguration(config);
    }

    public setConfiguration(configuration: IdSignConfig) {

        // setting idp configuration
        this.setIdpConfiguration(configuration.userInterface.viewTheme);

        // we save the configuration in the cookie
        this.setConfigurationInCookieValues(environment.Settings.defaultValues.siteConfigCookieName, configuration);

        // assign the configuration
        environment.Settings.siteConfig = configuration;
        this.logger.info('%cTheme set to ' + environment.Settings.siteConfig.userInterface.viewTheme, 'color:blue');
        this.triggerIdSignConfig(configuration);
    }


    private isValidConfiguration(configuration: IdSignConfig): boolean {
        if (configuration
            && configuration.userInterface
            && configuration.userInterface.viewTheme) {
            return true;
        } else {
            return false;
        }
    }

    private getDefaultConfiguration(): IdSignConfig {
        this.logger.debug('%cGetting default configuration from clients list', 'color:green');
        let defaultCredentials = environment.Settings.idpClientCredentialsList.list[0];
        environment.Settings.idpClientCredentialsList.list.forEach(item => {
            if (item.isDefault) {
                defaultCredentials = item;
            }
        });
        const configuration = new IdSignConfig();
        configuration.userInterface.viewSuite = defaultCredentials.companyCode;
        configuration.userInterface.viewTheme = defaultCredentials.companyCode;
        return configuration;
    }

    private setConfigurationInCookieValues(prefix: string, configuration: IdSignConfig): boolean {
        this.cookieService.set(prefix + this.cookieSuffix_signingMode, configuration.signingMode);
        this.cookieService.set(prefix + this.cookieSuffix_internationalUser, configuration.internationalUser);
        this.cookieService.set(prefix + this.cookieSuffix_fiscalCode, configuration.fiscalCode);
        this.cookieService.set(prefix + this.cookieSuffix_viewSuite, configuration.userInterface.viewSuite);
        this.cookieService.set(prefix + this.cookieSuffix_viewTheme, configuration.userInterface.viewTheme);
        return true;
    }
    private getConfigurationFromCookieValues(prefix: string): IdSignConfig {
        const configuration = new IdSignConfig();
        configuration.signingMode = this.cookieService.get(prefix + this.cookieSuffix_signingMode);
        configuration.internationalUser = this.cookieService.get(prefix + this.cookieSuffix_internationalUser)
            .toLowerCase()
            .trim();
        configuration.fiscalCode = this.cookieService.get(prefix + this.cookieSuffix_fiscalCode);
        configuration.userInterface.viewSuite = this.cookieService.get(prefix + this.cookieSuffix_viewSuite);
        configuration.userInterface.viewTheme = this.cookieService.get(prefix + this.cookieSuffix_viewTheme);

        return configuration;
    }


    setIdpConfiguration(settingsCode: string) {
        const credentials = this.getIdpCredentials(settingsCode, environment.Settings.idpClientCredentialsList);
        this.logger.info('%cCredentials to use are for ' + credentials.companyCode, 'color:blue');
        environment.Settings.idPConfig.clientId = credentials.client_id;
        environment.Settings.idPConfig.dummyClientSecret = credentials.client_secret;
        environment.Settings.currentClient = credentials;

        // set the language to default if one is provided in the settings
        // and if there isn't one set in the cookie already
        if (credentials.lang && !this.siteTranslateService.getCookieValue()) {
            this.siteTranslateService.setLanguage(credentials.lang);
        }

    }

    getIdpCredentials(settingsCode: string, credentialsList: IdpClientCredentialsList): IdpClientCredentials {
        let credentials: IdpClientCredentials;
        let defaultCredentials = environment.Settings.idpClientCredentialsList.list[0];
        credentialsList.list.forEach(item => {
            if (item.isDefault) {
                defaultCredentials = item;
            }
            if (item.settingsCode === settingsCode) {
                credentials = item;
                return credentials;
            }
        });
        if (!credentials) {
            return defaultCredentials;
        } else {
            return credentials;
        }
    }

    public triggerIdSignConfig(config: IdSignConfig): void {
        this.configurationObservable.next(config);
    }
    public idSignConfigChangeEvent(): Observable<IdSignConfig> {
        return this.configurationObservable.asObservable();
    }
}
