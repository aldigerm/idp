/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WindowEventListenerService } from './window-listener.service';

describe('Service: WindowListener', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WindowEventListenerService]
    });
  });

  it('should ...', inject([WindowEventListenerService], (service: WindowEventListenerService) => {
    expect(service).toBeTruthy();
  }));
});
