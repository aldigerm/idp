import { Router, NavigationEnd } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { MenuId } from '@models/menu';
import { LoggerService } from '@app/core/logging/logger.service';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { MultilevelNodes } from '@app/shared/components/ng-material-multilevel-menu/app.model';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { TenantDetailModel } from '@models/remote-repository/tenant/remote-repository-tenant-detail';
import { ProjectDetailModel } from '@models/remote-repository/project/remote-repository-project-detail';
import { UserService } from '@http/user/user.service';
import { UserGroupDetailModel } from '@models/remote-repository/user-group/remote-repository-user-group-detail';
import { RoleDetailModel } from '@models/remote-repository/role/remote-repository-role-detail';
import { ModuleDetailModel } from '@models/remote-repository/module/remote-repository-module-detail';
import { filter } from 'rxjs/operators';
import { AccessControlService } from './access-control.service';
import { ModuleCode } from '@models/module-code';
import { UserClaimTypeDetailModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type';
import { UserClaimDetailModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim';
import {
    UserGroupClaimTypeDetailModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type';
import { UserGroupClaimDetailModel } from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim';
import { RoleClaimTypeDetailModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type';
import { RoleClaimDetailModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim';
import { TenantClaimTypeDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type';
import { TenantClaimDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';

@Injectable()
export class MenuService {

    private mainMenu: MultilevelNodes;
    private enableSubMenues = true;
    private enableDetailMenues = false;
    private menuChanges = new BehaviorSubject<MultilevelNodes>(null);

    constructor(
        private _logger: LoggerService,
        private _userService: UserService,
        private _router: Router,
        private _accessControl: AccessControlService) {


        this._logger = this._logger.createInstance('MenuService');
        this.resetMainMenu();


        this._router.events.pipe(
            filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                if (this.isLeavingMenu(event.url, MenuId.UserDetail)) {
                    this.removeMenu(MenuId.UserDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.TenantDetail)) {
                    this.removeMenu(MenuId.TenantDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.ProjectDetail)) {
                    this.removeMenu(MenuId.ProjectDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.UserGroupDetail)) {
                    this.removeMenu(MenuId.UserGroupDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.RoleDetail)) {
                    this.removeMenu(MenuId.RoleDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.ModuleDetail)) {
                    this.removeMenu(MenuId.ModuleDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.UserClaimDetail)) {
                    this.removeMenu(MenuId.UserClaimDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.UserClaimTypeDetail)) {
                    this.removeMenu(MenuId.UserClaimTypeDetail);
                    this.setMenu(this.mainMenu);
                }
                if (this.isLeavingMenu(event.url, MenuId.PasswordPolicyDetail)) {
                    this.removeMenu(MenuId.UserClaimTypeDetail);
                    this.setMenu(this.mainMenu);
                }
            });
    }

    private isLeavingMenu(newUrl: string, menuId: MenuId): boolean {
        const menu = this.findMenu(menuId);
        if (!menu) {
            return false;
        } else {
            if (newUrl.startsWith(menu.link)) {
                return false;
            } else {
                return true;
            }
        }

    }

    public setMenuUserDetail(model: UserDetailModel) {
        this.addMenu(MenuId.UserDetail, model);
    }

    public setMenuTenantDetail(model: TenantDetailModel) {
        this.addMenu(MenuId.TenantDetail, model);
    }

    public setMenuProjectDetail(model: ProjectDetailModel) {
        this.addMenu(MenuId.ProjectDetail, model);
    }

    public setMenuUserGroupDetail(model: UserGroupDetailModel) {
        this.addMenu(MenuId.UserGroupDetail, model);
    }

    public setMenuRoleDetail(model: RoleDetailModel) {
        this.addMenu(MenuId.RoleDetail, model);
    }

    public setMenuModuleDetail(model: ModuleDetailModel) {
        this.addMenu(MenuId.ModuleDetail, model);
    }

    public setMenuUserClaimTypeDetail(model: UserClaimTypeDetailModel) {
        this.addMenu(MenuId.UserClaimTypeDetail, model);
    }

    public setMenuUserClaimDetail(model: UserClaimDetailModel) {
        this.addMenu(MenuId.UserClaimDetail, model);
    }

    public setMenuUserGroupClaimTypeDetail(model: UserGroupClaimTypeDetailModel) {
        this.addMenu(MenuId.UserGroupClaimTypeDetail, model);
    }

    public setMenuUserGroupClaimDetail(model: UserGroupClaimDetailModel) {
        this.addMenu(MenuId.UserGroupClaimDetail, model);
    }
    public setMenuRoleClaimTypeDetail(model: RoleClaimTypeDetailModel) {
        this.addMenu(MenuId.RoleClaimTypeDetail, model);
    }

    public setMenuRoleClaimDetail(model: RoleClaimDetailModel) {
        this.addMenu(MenuId.RoleClaimDetail, model);
    }

    public setMenuTenantClaimTypeDetail(model: TenantClaimTypeDetailModel) {
        this.addMenu(MenuId.TenantClaimTypeDetail, model);
    }

    public setMenuTenantClaimDetail(model: TenantClaimDetailModel) {
        this.addMenu(MenuId.TenantClaimDetail, model);
    }

    public setMenuPasswordPolicyDetail(model: PasswordPolicyDetailModel) {
        this.addMenu(MenuId.PasswordPolicyDetail, model);
    }
    public addMenu(id: MenuId, model: any) {
        let menuItem = <MultilevelNodes>{};
        if (this.enableDetailMenues) {
            switch (id) {
                case MenuId.UserDetail:

                    menuItem = this.buildMenuUserDetail(model);
                    const current = this.findMenu(MenuId.UserDetail);

                    if (!current || menuItem.link !== current.link) {
                        // remove any current detail
                        this.removeMenu(id);

                        // add detail
                        menuItem = this.buildMenuUserDetail(model);
                        this.addMenuInternal(MenuId.UserParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;
                case MenuId.TenantDetail:

                    menuItem = this.buildMenuTenantDetail(model);
                    const currentTenant = this.findMenu(MenuId.TenantDetail);

                    if (!currentTenant || menuItem.link !== currentTenant.link) {

                        // remove any current tenant
                        this.removeMenu(id);

                        // add tenant
                        menuItem = this.buildMenuTenantDetail(model);
                        this.addMenuInternal(MenuId.TenantParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;
                case MenuId.ProjectDetail:

                    menuItem = this.buildMenuProjectDetail(model);
                    const currentProject = this.findMenu(MenuId.ProjectDetail);

                    if (!currentProject || menuItem.link !== currentProject.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuProjectDetail(model);
                        this.addMenuInternal(MenuId.ProjectParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;
                case MenuId.UserGroupDetail:

                    menuItem = this.buildMenuUserGroupDetail(model);
                    const currentUserGroup = this.findMenu(MenuId.ProjectDetail);

                    if (!currentUserGroup || menuItem.link !== currentUserGroup.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuUserGroupDetail(model);
                        this.addMenuInternal(MenuId.UserGroupParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;

                case MenuId.RoleDetail:

                    menuItem = this.buildMenuRoleDetail(model);
                    const currentRole = this.findMenu(MenuId.RoleDetail);

                    if (!currentRole || menuItem.link !== currentRole.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuRoleDetail(model);
                        this.addMenuInternal(MenuId.RoleParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;

                case MenuId.ModuleDetail:

                    menuItem = this.buildMenuModuleDetail(model);
                    const currentModule = this.findMenu(MenuId.ModuleDetail);

                    if (!currentModule || menuItem.link !== currentModule.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuModuleDetail(model);
                        this.addMenuInternal(MenuId.ModuleParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;

                case MenuId.UserClaimDetail:

                    menuItem = this.buildMenuUserClaimDetail(model);
                    const currentUserClaim = this.findMenu(MenuId.UserClaimDetail);

                    if (!currentUserClaim || menuItem.link !== currentUserClaim.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuUserClaimDetail(model);
                        this.addMenuInternal(MenuId.UserClaimParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;

                case MenuId.UserClaimTypeDetail:

                    menuItem = this.buildMenuUserClaimTypeDetail(model);
                    const currentUserClaimType = this.findMenu(MenuId.UserClaimTypeDetail);

                    if (!currentUserClaimType || menuItem.link !== currentUserClaimType.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuUserClaimDetail(model);
                        this.addMenuInternal(MenuId.UserClaimTypeParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;

                case MenuId.PasswordPolicyDetail:

                    menuItem = this.buildMenuPasswordPolicyDetail(model);
                    const currentPasswordPolicy = this.findMenu(MenuId.PasswordPolicyDetail);

                    if (!currentPasswordPolicy || menuItem.link !== currentPasswordPolicy.link) {

                        // remove any current project
                        this.removeMenu(id);

                        // add project
                        menuItem = this.buildMenuUserClaimDetail(model);
                        this.addMenuInternal(MenuId.PasswordPolicyParent, menuItem);
                        this.setMenu(this.mainMenu);
                    }
                    break;
            }
        }
    }

    public removeMenu(id: MenuId, node?: MultilevelNodes) {
        if (!node) {
            node = this.mainMenu;
        }
        node.items = node.items.filter(m => m.id !== MenuId[id]);
        let res = null;
        for (let i = 0; res == null && i < node.items.length; i++) {
            const item = node.items[i];
            if (item) {
                res = this.removeMenu(id, item);
            }
        }
        return res;
    }


    public getMenu(): MultilevelNodes {
        return this.mainMenu;
    }

    private resetMainMenu() {
        this.mainMenu = this.buildMainMenu();
        this.setMenu(this.mainMenu);
    }

    private setMenu(menu: MultilevelNodes) {
        this.mainMenu = menu;
        this.menuChanges.next(this.mainMenu);
    }

    private buildMainMenu(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.Parent];
        menu.order = 0;
        menu.link = '';
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuUserCurrent());
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_USER_MANAGEMENT)) {
            menu.items.push(this.buildMenuUserGroups());
            menu.items.push(this.buildMenuUsers());
            menu.items.push(this.buildMenuPasswordPolicy());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)) {
            menu.items.push(this.buildMenuTenants());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)) {
            menu.items.push(this.buildMenuProjects());
            menu.items.push(this.buildMenuRoles());
            menu.items.push(this.buildMenuModules());
        }
        return menu;
    }

    private buildMenuPasswordPolicy(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.PasswordPolicyParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.PasswordPolicyParent];
        menu.order = 1;
        menu.link = this.enableSubMenues ? '' : '/password-policy/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuPasswordPolicyCreate());
            menu.items.push(this.buildMenuPasswordPolicyList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUsers(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserParent];
        menu.order = 1;
        menu.link = this.enableSubMenues ? '' : '/users/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuUserCreate());
            menu.items.push(this.buildMenuUserList());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_USER_MANAGEMENT)) {
            menu.items.push(this.buildMenuUserClaim());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)) {
            menu.items.push(this.buildMenuUserClaimTypes());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenants(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantParent];
        menu.order = 2;
        menu.link = this.enableSubMenues ? '' : '/tenants/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuTenantCreate());
            menu.items.push(this.buildMenuTenantList());
        }

        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)) {
            menu.items.push(this.buildMenuTenantClaim());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)) {
            menu.items.push(this.buildMenuTenantClaimTypes());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuProjects(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ProjectParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ProjectParent];
        menu.order = 3;
        menu.link = this.enableSubMenues ? '' : '/projects/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuProjectCreate());
            menu.items.push(this.buildMenuProjectList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserGroups(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupParent];
        menu.order = 5;
        menu.link = this.enableSubMenues ? '' : '/usergroups/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuUserGroupCreate());
            menu.items.push(this.buildMenuUserGroupList());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)) {
            menu.items.push(this.buildMenuUserGroupClaim());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)) {
            menu.items.push(this.buildMenuUserGroupClaimTypes());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoles(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleParent];
        menu.order = 5;
        menu.link = this.enableSubMenues ? '' : '/roles/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuRoleCreate());
            menu.items.push(this.buildMenuRoleList());
        }

        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)) {
            menu.items.push(this.buildMenuRoleClaim());
        }
        if (this._accessControl.checkAccessByCode(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)) {
            menu.items.push(this.buildMenuRoleClaimTypes());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuModules(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ModuleParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ModuleParent];
        menu.order = 6;
        menu.link = this.enableSubMenues ? '' : '/modules/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuModuleCreate());
            menu.items.push(this.buildMenuModuleList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserClaimTypes(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimTypeParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimTypeParent];
        menu.order = 7;
        menu.link = this.enableSubMenues ? '' : '/userclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuClaimTypeCreate());
            menu.items.push(this.buildMenuClaimTypeList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleClaimTypes(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimTypeParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimTypeParent];
        menu.order = 7;
        menu.link = this.enableSubMenues ? '' : '/roleclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuRoleClaimTypeCreate());
            menu.items.push(this.buildMenuRoleClaimTypeList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaimTypes(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimTypeParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimTypeParent];
        menu.order = 7;
        menu.link = this.enableSubMenues ? '' : '/tenantclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuTenantClaimTypeCreate());
            menu.items.push(this.buildMenuTenantClaimTypeList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserGroupClaimTypes(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimTypeParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimTypeParent];
        menu.order = 7;
        menu.link = this.enableSubMenues ? '' : '/usergroupclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuUserGroupClaimTypeCreate());
            menu.items.push(this.buildMenuUserGroupClaimTypeList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaim(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimParent];
        menu.order = 8;
        menu.link = this.enableSubMenues ? '' : '/tenantclaims/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuTenantClaimCreate());
            menu.items.push(this.buildMenuTenantClaimList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuRoleClaim(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimParent];
        menu.order = 8;
        menu.link = this.enableSubMenues ? '' : '/roleclaims/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuRoleClaimCreate());
            menu.items.push(this.buildMenuRoleClaimList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }


    private buildMenuUserGroupClaim(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimParent];
        menu.order = 8;
        menu.link = this.enableSubMenues ? '' : '/usergroupclaims/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuUserGroupClaimCreate());
            menu.items.push(this.buildMenuUserGroupClaimList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserClaim(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimParent];
        menu.isTop = true;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimParent];
        menu.order = 8;
        menu.link = this.enableSubMenues ? '' : '/userclaims/list';
        menu.items = new Array<MultilevelNodes>();
        if (this.enableSubMenues) {
            menu.items.push(this.buildMenuClaimCreate());
            menu.items.push(this.buildMenuClaimList());
        }
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuRoleList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleList];
        menu.order = 2;
        menu.link = '/roles/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuModuleList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ModuleList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ModuleList];
        menu.order = 2;
        menu.link = '/modules/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuClaimTypeList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimTypeList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimTypeList];
        menu.order = 2;
        menu.link = '/userclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleClaimList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimList];
        menu.order = 2;
        menu.link = '/roleclaims/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaimList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimList];
        menu.order = 2;
        menu.link = '/tenantclaims/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserGroupClaimList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimList];
        menu.order = 2;
        menu.link = '/usergroupclaims/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserGroupClaimTypeList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimTypeList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimTypeList];
        menu.order = 2;
        menu.link = '/usergroupclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleClaimTypeList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimTypeList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimTypeList];
        menu.order = 2;
        menu.link = '/roleclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaimTypeList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimTypeList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimTypeList];
        menu.order = 2;
        menu.link = '/tenantclaimtypes/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuClaimList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimList];
        menu.order = 2;
        menu.link = '/userclaims/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserGroupList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupList];
        menu.order = 2;
        menu.link = '/usergroups/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuProjectList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ProjectList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ProjectList];
        menu.order = 2;
        menu.link = '/projects/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuTenantList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantList];
        menu.order = 2;
        menu.link = '/tenants/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuPasswordPolicyList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.PasswordPolicyList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.PasswordPolicyList];
        menu.order = 2;
        menu.link = '/password-policy/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }


    private buildMenuUserList(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserList];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserList];
        menu.order = 2;
        menu.link = '/users/list';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuPasswordPolicyCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.PasswordPolicyCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.PasswordPolicyCreate];
        menu.order = 1;
        menu.link = '/password-policy/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserCreate];
        menu.order = 1;
        menu.link = '/users/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuProjectCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ProjectCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ProjectCreate];
        menu.order = 1;
        menu.link = '/projects/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuTenantCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantCreate];
        menu.order = 1;
        menu.link = '/tenants/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuClaimTypeCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimTypeCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimTypeCreate];
        menu.order = 1;
        menu.link = '/userclaimtypes/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleClaimCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimCreate];
        menu.order = 1;
        menu.link = '/roleclaims/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleClaimTypeCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleClaimTypeCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleClaimTypeCreate];
        menu.order = 1;
        menu.link = '/roleclaimtypes/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaimCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimCreate];
        menu.order = 1;
        menu.link = '/tenantclaims/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantClaimTypeCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantClaimTypeCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantClaimTypeCreate];
        menu.order = 1;
        menu.link = '/tenantclaimtypes/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserGroupClaimTypeCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimTypeCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimTypeCreate];
        menu.order = 1;
        menu.link = '/usergroupclaimtypes/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserGroupClaimCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupClaimCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupClaimCreate];
        menu.order = 1;
        menu.link = '/usergroupclaims/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuClaimCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimCreate];
        menu.order = 1;
        menu.link = '/userclaims/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuModuleCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ModuleCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ModuleCreate];
        menu.order = 1;
        menu.link = '/modules/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuRoleCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleCreate];
        menu.order = 1;
        menu.link = '/roles/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserGroupCreate(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupCreate];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupCreate];
        menu.order = 1;
        menu.link = '/usergroups/create';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuTenantCurrent(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantDetail];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantDetail];
        menu.order = 2;
        menu.link = '/tenant/detail';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserCurrent(): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserCurrent];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserCurrent];
        this._userService.getUser().then(user => {
            menu.localisationParam = { username: user && user.identifier ? user.identifier.username : '' };
        });
        menu.order = 0;
        menu.link = '/users/detail';
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        menu.isTop = true;
        return menu;
    }
    private buildMenuUserDetail(detail: UserDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserDetail];
        menu.localisationParam = { username: detail.identifier.username, firstname: detail.firstName, lastname: detail.lastName };
        menu.link = '/users/detail/' + detail.identifier.projectCode + '/'
            + detail.identifier.tenantCode + '/' + detail.identifier.username;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuUserData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuTenantDetail(detail: TenantDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.TenantDetail];
        menu.localisationParam = { tenantCode: detail.identifier.tenantCode, projectCode: detail.identifier.projectCode };
        menu.link = '/tenants/detail/' + detail.identifier.projectCode + '/' + detail.identifier.tenantCode;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuTenantData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuProjectDetail(detail: ProjectDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ProjectDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ProjectDetail];
        menu.localisationParam = { projectCode: detail.identifier.projectCode };
        menu.link = '/projects/detail/' + detail.identifier.projectCode;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuProjectData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleDetail(detail: RoleDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.RoleDetail];
        menu.localisationParam = { roleCode: detail.identifier.roleCode };
        menu.link = '/roles/detail/' + detail.identifier.projectCode + '/'
            + detail.identifier.tenantCode + '/' + detail.identifier.roleCode;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuRoleData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuModuleDetail(detail: ModuleDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ModuleDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.ModuleDetail];
        menu.localisationParam = { moduleCode: detail.identifier.moduleCode };
        menu.link = '/modules/detail/' + detail.identifier.projectCode
            + '/' + detail.identifier.moduleCode;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuModuleData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuPasswordPolicyDetail(detail: PasswordPolicyDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.PasswordPolicyDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.PasswordPolicyDetail];
        menu.localisationParam = { passwordPolicyCode: detail.identifier.passwordPolicyCode };
        menu.link = '/password-policy/detail/'
            + detail.identifier.projectCode
            + '/' + detail.identifier.tenantCode
            + '/' + detail.identifier.passwordPolicyCode;
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserClaimDetail(detail: UserClaimDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimDetail];
        menu.localisationParam = { moduleCode: detail.identifier.type };
        menu.link = '/userclaims/detail/' + detail.identifier.projectCode
            + '/' + detail.identifier.tenantCode
            + '/' + detail.identifier.username
            + '/' + detail.identifier.type
            + '/' + detail.identifier.value;
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserClaimTypeDetail(detail: UserClaimTypeDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserClaimTypeDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserClaimTypeDetail];
        menu.localisationParam = { moduleCode: detail.identifier.type };
        menu.link = '/userclaimtypes/detail/' + detail.identifier.projectCode
            + '/' + detail.identifier.type;
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuUserGroupDetail(detail: UserGroupDetailModel): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupDetail];
        menu.order = 3;
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.UserGroupDetail];
        menu.localisationParam = { userGroupCode: detail.identifier.userGroupCode };
        menu.link = '/usergroups/detail/' + detail.identifier.projectCode + '/'
            + detail.identifier.tenantCode + '/' + detail.identifier.userGroupCode;
        menu.items = new Array<MultilevelNodes>();
        menu.items.push(this.buildMenuUserGroupData(menu));
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuTenantData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.TenantData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuRoleData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.RoleData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserGroupData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserGroupData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private buildMenuModuleData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ModuleData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuProjectData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.ProjectData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }
    private buildMenuUserData(parent: MultilevelNodes): MultilevelNodes {
        const menu = <MultilevelNodes>{};
        menu.id = MenuId[MenuId.UserProfileData];
        menu.localisation = environment.Settings.defaultValues.localizationSidebarPrefix + MenuId[MenuId.Data];
        menu.order = 4;
        menu.link = parent.link;
        menu.linkQueryParams = { tab: 'data' };
        menu.items = new Array<MultilevelNodes>();
        menu.icon = this.getIcon(menu);
        return menu;
    }

    private addMenuInternal(parentId: MenuId, item: MultilevelNodes) {
        if (item && item.id) {
            const parentMenu = this.findMenu(parentId);
            if (parentMenu) {

                // initialise list if empty
                if (!parentMenu.items) {
                    parentMenu.items = new Array<MultilevelNodes>();
                }

                // look for child
                const index = parentMenu.items.findIndex(m => m.id === item.id);

                if (index >= 0) {
                    // remove child if it exist
                    parentMenu.items.splice(index, 1);
                }

                // add child
                parentMenu.items.push(item);

                // sort
                parentMenu.items.sort((a, b) => {
                    // sort asending
                    return a.order - b.order;
                });
            }
        }
    }

    private getIcon(node: MultilevelNodes) {
        const prefix = 'mdi ';
        switch (node.id) {
            case MenuId[MenuId.UserParent]:
                return prefix + 'mdi-account-multiple';
            case MenuId[MenuId.TenantParent]:
                return prefix + 'mdi-domain';
            case MenuId[MenuId.ProjectParent]:
                return prefix + 'mdi-home';
            case MenuId[MenuId.UserGroupParent]:
                return prefix + 'mdi-group';
            case MenuId[MenuId.RoleParent]:
                return prefix + 'mdi-polymer';
            case MenuId[MenuId.ModuleParent]:
                return prefix + 'mdi-eject';
            case MenuId[MenuId.PasswordPolicyParent]:
                return prefix + 'mdi-textbox-password';
            case MenuId[MenuId.UserClaimTypeParent]:
            case MenuId[MenuId.UserGroupClaimTypeParent]:
            case MenuId[MenuId.RoleClaimTypeParent]:
            case MenuId[MenuId.TenantClaimTypeParent]:
                return prefix + 'mdi-book-multiple';
            case MenuId[MenuId.UserClaimParent]:
            case MenuId[MenuId.UserGroupClaimParent]:
            case MenuId[MenuId.RoleClaimParent]:
            case MenuId[MenuId.TenantClaimParent]:
                return prefix + 'mdi-book';
            case MenuId[MenuId.UserList]:
            case MenuId[MenuId.TenantList]:
            case MenuId[MenuId.ProjectList]:
            case MenuId[MenuId.UserGroupList]:
            case MenuId[MenuId.RoleList]:
            case MenuId[MenuId.ModuleList]:
            case MenuId[MenuId.UserClaimList]:
            case MenuId[MenuId.UserClaimTypeList]:
            case MenuId[MenuId.UserGroupClaimList]:
            case MenuId[MenuId.UserGroupClaimTypeList]:
            case MenuId[MenuId.RoleClaimList]:
            case MenuId[MenuId.RoleClaimTypeList]:
            case MenuId[MenuId.TenantClaimList]:
            case MenuId[MenuId.TenantClaimTypeList]:
            case MenuId[MenuId.PasswordPolicyList]:
                return prefix + 'mdi-view-list';
            case MenuId[MenuId.ProjectCreate]:
            case MenuId[MenuId.TenantCreate]:
            case MenuId[MenuId.UserCreate]:
            case MenuId[MenuId.UserGroupCreate]:
            case MenuId[MenuId.RoleCreate]:
            case MenuId[MenuId.ModuleCreate]:
            case MenuId[MenuId.UserClaimCreate]:
            case MenuId[MenuId.UserClaimTypeCreate]:
            case MenuId[MenuId.UserGroupClaimCreate]:
            case MenuId[MenuId.UserGroupClaimTypeCreate]:
            case MenuId[MenuId.UserGroupClaimCreate]:
            case MenuId[MenuId.RoleClaimCreate]:
            case MenuId[MenuId.RoleClaimTypeCreate]:
            case MenuId[MenuId.TenantClaimCreate]:
            case MenuId[MenuId.TenantClaimTypeCreate]:
            case MenuId[MenuId.PasswordPolicyCreate]:
                return prefix + 'mdi-plus';
            case MenuId[MenuId.ProjectDetail]:
            case MenuId[MenuId.TenantDetail]:
            case MenuId[MenuId.UserDetail]:
            case MenuId[MenuId.UserGroupDetail]:
            case MenuId[MenuId.RoleDetail]:
            case MenuId[MenuId.ModuleDetail]:
            case MenuId[MenuId.UserClaimDetail]:
            case MenuId[MenuId.UserClaimTypeDetail]:
            case MenuId[MenuId.PasswordPolicyDetail]:
                return prefix + 'mdi-briefcase';
            case MenuId[MenuId.UserCurrent]:
                return prefix + 'mdi-account';
        }
    }

    public findMenu(id: MenuId, node?: MultilevelNodes): MultilevelNodes {
        if (!node) {
            node = this.mainMenu;
        }
        if (node.id === MenuId[id]) {
            return node;
        }
        const children = node.items;
        let res = null;
        for (let i = 0; res == null && i < children.length; i++) {
            const item = children[i];
            if (item) {
                res = this.findMenu(id, item);
            }
        }
        return res;
    }

    public findMenuByUrl(url: string, node?: MultilevelNodes): MultilevelNodes {
        if (!node) {
            node = this.mainMenu;
        }
        if (node.link === url) {
            return node;
        }
        const children = node.items;
        let res = null;
        for (let i = 0; res == null && i < children.length; i++) {
            const item = children[i];
            if (item) {
                res = this.findMenuByUrl(url, item);
            }
        }
        return res;
    }

    public menuChange(): Observable<MultilevelNodes> {
        return this.menuChanges;
    }
}
