import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Observable, merge } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';
import { BreakpointObserver } from '@angular/cdk/layout';
import { LoggerService } from '../logging/logger.service';
import { hasEqualProperties } from '@app/sharedfunctions/hasEqualProperties';

@Injectable()
export class WindowEventListenerService {


    public static readonly IDONBOARD_WINDOW_RESIZE = 'idonboard-window-resize';
    private static LAST_LAYOUT: LayoutSetup;

    private readonly _breakpointMobile = ['(max-width:767px)'];
    private readonly _breakpointTablet = ['(min-width:768px) and (max-width:1023px)'];
    private readonly _breakpointDesktop = ['(min-width:1024px)'];
    private readonly _allScreenSize = []
        .concat(
            this._breakpointMobile
            , this._breakpointTablet
            , this._breakpointDesktop);

    private _OnLayoutChange = new EventEmitter<LayoutSetup>();
    private _onResizeSubject = new EventEmitter<any>();
    private _onMenuSubject = new EventEmitter<any>();
    private _onResizeTimeout = setTimeout(function () { }, 0);
    private _breakpointSubscriber: ISubscription;

    constructor(
        private breakpointObserver: BreakpointObserver
        , private _logger: LoggerService
    ) {

        this._logger = _logger.createInstance('WindowEventListenerService');

        // we initialise the layout to the one found when the service is
        // being constructed at the given time
        WindowEventListenerService.LAST_LAYOUT = this.getCurrentLayoutSetup();

        this._breakpointSubscriber = this.breakpointObserver
            .observe(this._allScreenSize)
            .subscribe(o => {

                // a breakpoint was breached
                // so we get the new setup of the screen
                const newLayout = this.getCurrentLayoutSetup();

                // we get the old one to compare it with the new
                const oldLayout = WindowEventListenerService.LAST_LAYOUT;

                // see if there was some form of change in the setup
                // given our understanding of the different layouts
                if (!hasEqualProperties(oldLayout, newLayout)) {

                    // there was a change from the last time we had a change
                    this._logger.debug(`Screen layout change detected from ${JSON.stringify(oldLayout)} to ${JSON.stringify(newLayout)}`);

                    // we update what was the last layout of the screen
                    WindowEventListenerService.LAST_LAYOUT = newLayout;

                    // we fire the event to advise the listeners
                    this.emitLayout(newLayout);
                }
            });
    }

    public get isMobile() {
        return this.breakpointObserver.isMatched(this._breakpointMobile);
    }

    public get isTablet() {
        return this.breakpointObserver.isMatched(this._breakpointTablet);
    }

    public get isDesktop() {
        return this.breakpointObserver.isMatched(this._breakpointDesktop);
    }

    public getCurrentLayoutSetup(): LayoutSetup {
        return <LayoutSetup>{
            isDesktop: this.isDesktop,
            isTablet: this.isTablet,
            isMobile: this.isMobile
        };
    }

    private emitLayout(layout: LayoutSetup): void {
        if (!layout) {
            layout = this.getCurrentLayoutSetup();
        }
        this._OnLayoutChange.next(layout);
    }

    public TriggerOnResize() {
        clearTimeout(this._onResizeTimeout);
        const parent = this;
        this._onResizeTimeout = setTimeout(function () {
            parent._onResizeSubject.next(true);
            parent.callNewEvent(this.idonboardResize);
        }, 200);
    }

    public TriggerOnMenu() {
        this._onMenuSubject.next(true);
        setTimeout(() => {
            // this is needed due to transitions
            // until the fix for the detail pages is done
            this._onMenuSubject.next(true);
        }, 400);
    }

    public OnLayoutChange(): Observable<LayoutSetup> {
        return this._OnLayoutChange;
    }

    public OnResize(): Observable<any> {
        return this._onResizeSubject;
    }

    public OnResizeAndMenu(): Observable<any> {
        return merge(this._onMenuSubject, this._onResizeSubject);
    }

    public callNewEvent(eventName, params?) {
        try {
            dispatchEvent(new CustomEvent(eventName));
        } catch (exception) {
            // needed for IE browser as CustomEvent does not exist as a constructor
            // see https://stackoverflow.com/questions/37565722/cross-browser-compatible-customevent
            params = params || { bubbles: true, cancelable: true, detail: undefined };
            const evt = document.createEvent('CustomEvent');
            evt.initCustomEvent(eventName, params.bubbles, params.cancelable, params.detail);
            dispatchEvent(evt);
        }
    }

    ngOnDestroy(): void {
        if (this._breakpointSubscriber) {
            this._breakpointSubscriber.unsubscribe();
        }
    }
}

export interface LayoutSetup {
    isMobile: boolean;
    isTablet: boolean;
    isDesktop: boolean;
}
