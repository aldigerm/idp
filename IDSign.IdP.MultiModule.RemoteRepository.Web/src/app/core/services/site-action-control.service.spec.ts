/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SiteActionControlService } from './site-action-control.service';

describe('Service: SiteActionControlService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteActionControlService]
    });
  });

  it('should ...', inject([SiteActionControlService], (service: SiteActionControlService) => {
    expect(service).toBeTruthy();
  }));
});
