import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable ,  from } from 'rxjs';
/**
 * Decides if a route can be activated.
 */
@Injectable() export class ShowPreLoaderGuard implements CanDeactivate<any> {

    constructor(
        private _pagePreloaderService: PagePreloaderService
    ) {
    }

    canDeactivate(component: any): boolean | Observable<boolean> {
        this._pagePreloaderService.setLocalisedMessage('PagePreloader.Redirecting');
        this._pagePreloaderService.show();
        return from(new Promise<boolean>((resolve) => {
            setTimeout(() => {
                resolve(true);
            }, 1);
        }));
    }
}
