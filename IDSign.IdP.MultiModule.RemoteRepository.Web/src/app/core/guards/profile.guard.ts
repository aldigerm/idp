import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserService } from '@http/user/user.service';
import { AccessControlService } from '@services/access-control.service';
import { UserCacheService } from '@services/storage/user-cache.service';

import { Observable, from } from 'rxjs';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { environment } from '@environments/environment';

/**
 * Decides if a route can be activated.
 */
@Injectable() export class ProfileGuard implements CanActivate {

    constructor(
        private authenticationService: AuthenticationService
        , private router: Router
        , private userService: UserService
        , private logger: LoggerService
        , private userCacheService: UserCacheService
        , private accessControlService: AccessControlService
        , private pagePreloaderService: PagePreloaderService
        , private browserStorage: BrowserStorage) {

        // create logging instance
        this.logger = this.logger.createInstance('AuthGuard');
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const url: string = state.url;
        this.pagePreloaderService.setLocalisedMessage('PagePreloader.CheckLoginStatus');
        return from(
            this.authenticationService.isSignedIn()
                .then(username => {
                    return username;
                })
                .then(username => {
                    return this.userService.refreshUser()
                        .then(user => {
                            if (user) {

                                return true;
                            } else {
                                // he is not signed in, so no need to send the user to login
                                return this.redirectToLogin(url);
                            }
                        })
                        .catch(e => {
                            return this.redirectToLogin(url);
                        });
                })
                .catch(error => {
                    return this.redirectToLogin(url);
                }));
    }

    private redirectToLogin(url: string): boolean {
        // redirected to login with error
        this.browserStorage.set(environment.Settings.browserStorageKeys.userLoadErrorKey, 'true');
        this.authenticationService.setRedirectUri(url);
        return this.redirectToUrl('/login');
    }

    private redirectToUrl(url: string): boolean {
        this.logger.debug('Guard : Redirecting to ' + url);
        this.router.navigate([url]);
        return false;
    }

}
