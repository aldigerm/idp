import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { EnvironmentService } from '@http/environment/environment.service';
import { UserService } from '@http/user/user.service';
import { AccessControlService } from '@services/access-control.service';
import { BrowserStorage } from '@services/storage/browser-storage';

import { Observable, from } from 'rxjs';


/**
 * Decides if a route can be activated.
 */
@Injectable() export class AuthLoginGuard implements CanActivate {

    private signedIn = false;

    constructor(
        private authenticationService: AuthenticationService
        , private router: Router
        , private userService: UserService
        , private logger: LoggerService
        , private browserStorage: BrowserStorage
        , private environmentService: EnvironmentService
        , private accessControlService: AccessControlService) {

        // create logging instance
        this.logger = this.logger.createInstance('AuthLoginGuard');
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

        // we clear the environment cache on login
        // as there can be changes on the fly on the server
        this.logger.debug('Checking access and getting settings again.');
        this.environmentService.removeCachedEnvironment();

        if (this.browserStorage.get(environment.Settings.browserStorageKeys.userLoadErrorKey)) {
            this.logger.debug('There is a user load error in memory. redirecting to login.');
            return true;
        } else {
            return from(this.authenticationService.isSignedIn()
                .then(username => {
                    const url: string = state.url;

                    if (username) {
                        return this.userService.refreshUser()
                            .then(user => {
                                return this.userService.isRecognised(username)
                                    .then(recognised => {
                                        if (recognised) {
                                            // you do not go to the login page
                                            // if you are logged in and recognised
                                            this.logger.debug('Login page is not allowed. The user is already logged in '
                                                + 'and recognised.');
                                            this.accessControlService.setAllowedModules(recognised.allowedModules);
                                            const hasAccess = this.accessControlService.checkAccessForSite();
                                            if (!hasAccess) {
                                                // user can't see any tabs
                                                return true;
                                            } else {
                                                // user is allowed to be handled by the other guards
                                                this.router.navigate(['/']);
                                                return false;
                                            }
                                        } else {
                                            this.logger.debug('The user is not recognised. Go to login page.');
                                            return true;
                                        }
                                    })
                                    .catch(error => {
                                        // an error takes you to the login page
                                        this.logger.error('An error occured during recognition. Go to login page.' + JSON.stringify(error));
                                        return true;
                                    });
                            })
                            .catch(error => {
                                // an error takes you to the login page
                                this.logger.error('An error occured during user refresh. Go to login page.' + JSON.stringify(error));
                                return true;
                            });
                    } else {
                        // you're not signed in, so we go to login
                        this.logger.debug('You are not signed in through the idp. Go to login page.');
                        return true;
                    }
                }).catch(error => {
                    // an error takes you to the login page
                    this.logger.error('An error occured while checking if signed in. Go to login page.' + JSON.stringify(error));
                    return true;
                })
            );
        }
    }
}
