import { ShowPreLoaderGuard } from './show-preloader.guard';
import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';


describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [ShowPreLoaderGuard]
    });
  });

  it('should ...', inject([ShowPreLoaderGuard], (guard: ShowPreLoaderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
