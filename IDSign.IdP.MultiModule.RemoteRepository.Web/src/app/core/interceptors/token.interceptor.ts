import { APP_BASE_HREF } from '@angular/common';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { BrowserStorage } from '@services/storage/browser-storage';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/Observable';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { tap } from 'rxjs/operators';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor
    (
      private router: Router,
      private logger: LoggerService,
      private auth: AuthenticationService,
      private browserStorage: BrowserStorage,
      private siteAlertService: SweetAlertService,
      @Inject(APP_BASE_HREF) private baseHref: string
    ) {

    // create logging instance
    this.logger = this.logger.createInstance('TokenInterceptor');
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.logger.debug('intercepting ' + request.url);
    let isClientLog = false;
    if (request.url === environment.Settings.apiKeys.idP.ClientLogError) {
      isClientLog = true;
    }

    let setUrl = request.url;
    let isWebapi = false;
    let isTranslationGet = false;
    let isFileUpload = false;
    let isIdp = false;
    if (environment.Settings) {
      const fileUploadHeader = request.headers.get(environment.Settings.defaultValues.headerKeyForFileUpload);
      if (fileUploadHeader) {
        this.logger.debug('%cThis is a FileUpload call.', 'color:violet');
        isFileUpload = true;
      } else if (setUrl.split('?')[0] === environment.Settings.settingsUrl.split('?')[0]) {
        // we compare the path without the query string due to
        // version query string bump possibly being added
        this.logger.debug('%cThis is the settings call.', 'color:blue');
      } else if (setUrl.indexOf('assets/i18n') >= 0) {
        this.logger.debug('%cThis is an assets call.', 'color:blue');
      } else if (environment.Settings.idPConfig && setUrl.startsWith(environment.Settings.idPConfig.issuer)) {
        isIdp = true;
        this.logger.debug('%cThis is the idp call.', 'color:green');
      } else {
        if (setUrl.startsWith(environment.Settings.apiKeys.idP.TranslationGet)) {
          isTranslationGet = true;
          this.logger.debug('%cThis is translation call.', 'color:blue');
        }
        isWebapi = true;
      }

      const token = this.auth.getBearerToken();

      // we add the apiendpoint to webapi requests
      if (isWebapi || isFileUpload) {
        setUrl = environment.Settings.externalUrls.apiEndpoint + setUrl;
      } else if (setUrl.indexOf('i18n') >= 0) {
        // we add the baseHref to localization files
        setUrl = this.baseHref + setUrl;
        isWebapi = true;
      }

      if (isWebapi) {
        // setting headers
        request = request.clone({
          setHeaders: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json',
          },
          body: request.body
          , url: setUrl
        });
      } else if (isFileUpload) {
        request = request.clone({
          setHeaders: {
            Authorization: 'Bearer ' + token
          }
          , reportProgress: true
          , body: request.body
          , url: setUrl
        });
      }

      if (isTranslationGet && !token) {
        this.logger.debug('Faking results as user is not logged in for ' + request.url);
        return of(new HttpResponse(
          { status: 200, body: '{}' }
        ));
      }

    }


    this.logger.debug('intercepted : ' + request.url);
    return next.handle(request)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this.logger.debug('intercepted response ok.');
          }
        }, (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401 && !isClientLog && !isTranslationGet) {
              this.logger.error('intercepted response : you are no longer authorized. signing out.', err);
              const current = this;
              this.siteAlertService.show(
                new SweetAlertRequest({
                  type: SweetAlertType.Error,
                  title: 'Authentication.PopUp.session_terminated.Title',
                  message: 'Authentication.PopUp.session_terminated.Message',
                  confirmButton: 'Authentication.PopUp.session_terminated.OkButton',
                  successCallback: () => {
                    // we sign out and start login process again
                    current.auth.signout();
                  },
                  errorCode: (err && err.error) ? err.error.ErrorCodeDisplay : undefined,
                }));
            }
          }
        }
        )
      );
  }
}
