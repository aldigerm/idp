import { IdSignConfig } from '@models/idsign-config';
import { LoggingLevel } from '@models/logging/logging-level.enum';
import { AuthConfig } from 'angular-oauth2-oidc';

export class DateFormats {
  readonly TIME_FMT = 'HH:mm';
  readonly DATE_FMT = 'dd/MM/yyyy';
  readonly DATE_TIME_FMT = `dd/MM/yyyy HH:mm`;
}
export class DefaultValues {
  readonly defaultTheme = 'default';
  readonly defaultSuite = 'default';
  readonly loggerLevelParameterName = 'logger_level';
  readonly siteConfigParameterName = 'remote_user_config';
  readonly siteCompanyParameterName = 'remote_user_company';

  // cookies
  readonly cookiePrefix = 'idp_rep';
  readonly siteConfigCookieName = this.cookiePrefix + '_config';
  readonly loggerLevelCookieName = this.cookiePrefix + '_ll';
  readonly accessTokenCookieName = this.cookiePrefix + '_at';
  readonly languageTokenCookieName = this.cookiePrefix + '_lang';
  readonly cookieExpiryDays = 7;

  // headers
  readonly headerKeyForFileUpload = 'useFileUpload';

  // localization
  readonly localizationUser = 'User.Detail.Tabs.Chat.UserTab.Label';
  readonly localizationChatPrefix = 'Common.FileTypes.';
  readonly localizationSidebarPrefix = 'Sidebar.';
  readonly localizationChecklistCategoriesPrefix = 'Common.FileCategories.';
  readonly localizationChecklistFilePrefix = 'Common.FileTypes.';
  readonly localizationOverviewRejectionReasonsPrefix
    = 'User.Detail.Tabs.Overview.Steps.VerifyingDocumentsRisk.Actions.RejectByRisk.Reasons.';
  readonly localizationDocumentRejectionReasonsPrefix = 'User.Detail.Tabs.Checklist.Document.Actions.Reject.Reasons.';
  readonly localizationUserActivityPrefix = 'User.Detail.Tabs.History.Activity.Messages.';
  readonly localizationUserDocumentActionPrefix = 'User.Detail.Tabs.Checklist.Document.Actions.';
  readonly localizationUserStatusPrefix = 'User.Status.';
  readonly localizationUserFileTypePrefix = 'Common.UserFileTypes.';
  readonly projectCode = 'idonboard';
  readonly companyCodeParameterName = 'c';

  readonly availableLanguages: LanguageSettings[] = [
    <LanguageSettings>{ langCode: 'en', flag: 'gb' }
  ];

}

export class BrowserStorageKeys {
  readonly idpError = 'idpError';
  readonly userLoadErrorKey = 'userLoadError';
  readonly isIdpCallBack = 'isIdpCallBack';
  readonly lastGlobalError = 'globalError';
  readonly redirectUri = 'redirectUri';
  readonly countries = 'countries';
  readonly returnUrl = 'returnUrl';
  readonly environmentLoadingError = 'environmentLoadingError';
  readonly environmentSettingKey = 'environment_settings';
}

export class ApiKeys {
  public readonly idP = new IdP();

}

export class IdP {
  private readonly prefix = '/api';

  public readonly TranslationGet = this.prefix + '/translation/get';

  // User
  public readonly UserGet = this.prefix + '/user/get';

  // User
  public readonly UserFileUpload = this.prefix + '/user/upload';
  public readonly UserProfileImage = this.prefix + '/user/profileImage';
  public readonly UserCreate = this.prefix + '/user/create';
  public readonly UserUpdate = this.prefix + '/user/update';
  public readonly UserGetLoggedInDetail = this.prefix + '/user/get';
  public readonly UserGetDetail = this.prefix + '/user/detail';
  public readonly UserGetPageList = this.prefix + '/user/list';
  public readonly UserDelete = this.prefix + '/user/delete';
  public readonly UserSetUserGroups = this.prefix + '/user/SetUserGroups';
  public readonly UserSetUserTenants = this.prefix + '/user/SetUserTenants';
  public readonly UserSetManagedUserGroups = this.prefix + '/user/SetManagedUserGroups';
  public readonly UserSetPassword = this.prefix + '/user/SetPassword';
  public readonly UserProfileImageGet = this.prefix + '/user/profileimage';
  public readonly UserSetSecuritySettings = this.prefix + '/user/SetSecuritySettings';


  // Tenant
  public readonly TenantFileUpload = this.prefix + '/file/upload';
  public readonly TenantChecklistFileDownload = this.prefix + '/file/download';
  public readonly TenantCreate = this.prefix + '/tenant/create';
  public readonly TenantUpdate = this.prefix + '/tenant/update';
  public readonly TenantGetDetail = this.prefix + '/tenant/detail';
  public readonly TenantGetPageList = this.prefix + '/tenant/list';
  public readonly TenantDelete = this.prefix + '/tenant/delete';


  // Project
  public readonly ProjectFileUpload = this.prefix + '/file/upload';
  public readonly ProjectChecklistFileDownload = this.prefix + '/file/download';
  public readonly ProjectCreate = this.prefix + '/project/create';
  public readonly ProjectUpdate = this.prefix + '/project/update';
  public readonly ProjectGetDetail = this.prefix + '/project/detail';
  public readonly ProjectGetPageList = this.prefix + '/project/list';
  public readonly ProjectDelete = this.prefix + '/project/delete';


  // User Group
  public readonly UserGroupCreate = this.prefix + '/usergroup/create';
  public readonly UserGroupUpdate = this.prefix + '/usergroup/update';
  public readonly UserGroupGetDetail = this.prefix + '/usergroup/detail';
  public readonly UserGroupGetPageList = this.prefix + '/usergroup/list';
  public readonly UserGroupDelete = this.prefix + '/usergroup/delete';
  public readonly UserGroupSetInheritingUserGroups = this.prefix + '/usergroup/SetInheritingUserGroups';
  public readonly UserGroupSetPasswordPolicies = this.prefix + '/usergroup/SetPasswordPolicies';
  public readonly UserGroupSetUsers = this.prefix + '/usergroup/SetUsers';

  // Role
  public readonly RoleCreate = this.prefix + '/role/create';
  public readonly RoleUpdate = this.prefix + '/role/update';
  public readonly RoleGetDetail = this.prefix + '/role/detail';
  public readonly RoleGetPageList = this.prefix + '/role/list';
  public readonly RoleDelete = this.prefix + '/role/delete';
  public readonly RoleSetInheritingRoles = this.prefix + '/role/SetInheritingRoles';
  public readonly RoleSetModules = this.prefix + '/role/SetModules';
  public readonly RoleSetUserGroups = this.prefix + '/role/SetUserGroups';

  // Module
  public readonly ModuleCreate = this.prefix + '/module/create';
  public readonly ModuleUpdate = this.prefix + '/module/update';
  public readonly ModuleGetDetail = this.prefix + '/module/detail';
  public readonly ModuleGetPageList = this.prefix + '/module/list';
  public readonly ModuleDelete = this.prefix + '/module/delete';

  // User Claim Type
  public readonly UserClaimTypeGetPageList = this.prefix + '/userClaimType/list';
  public readonly UserClaimTypeCreate = this.prefix + '/userClaimType/create';
  public readonly UserClaimTypeUpdate = this.prefix + '/userClaimType/update';
  public readonly UserClaimTypeGetDetail = this.prefix + '/userClaimType/detail';
  public readonly UserClaimTypeDelete = this.prefix + '/userClaimType/delete';
  public readonly UserClaimTypeSetUsers = this.prefix + '/userClaimType/setUsers';


  // User Claim
  public readonly UserClaimGetPageList = this.prefix + '/userClaim/list';
  public readonly UserClaimGetDetail = this.prefix + '/userClaim/detail';
  public readonly UserClaimCreate = this.prefix + '/userClaim/create';
  public readonly UserClaimUpdate = this.prefix + '/userClaim/update';
  public readonly UserClaimDelete = this.prefix + '/userClaim/delete';
  public readonly UserClaimSetUsers = this.prefix + '/userClaim/setUsers';


  // UserGroup Claim Type
  public readonly UserGroupClaimTypeGetPageList = this.prefix + '/userGroupClaimType/list';
  public readonly UserGroupClaimTypeCreate = this.prefix + '/userGroupClaimType/create';
  public readonly UserGroupClaimTypeUpdate = this.prefix + '/userGroupClaimType/update';
  public readonly UserGroupClaimTypeGetDetail = this.prefix + '/userGroupClaimType/detail';
  public readonly UserGroupClaimTypeDelete = this.prefix + '/userGroupClaimType/delete';
  public readonly UserGroupClaimTypeSetUserGroups = this.prefix + '/userGroupClaimType/setUserGroups';


  // UserGroup Claim
  public readonly UserGroupClaimGetPageList = this.prefix + '/userGroupClaim/list';
  public readonly UserGroupClaimGetDetail = this.prefix + '/userGroupClaim/detail';
  public readonly UserGroupClaimCreate = this.prefix + '/userGroupClaim/create';
  public readonly UserGroupClaimUpdate = this.prefix + '/userGroupClaim/update';
  public readonly UserGroupClaimDelete = this.prefix + '/userGroupClaim/delete';
  public readonly UserGroupClaimSetUserGroups = this.prefix + '/userGroupClaim/setUserGroups';


  // Role Claim Type
  public readonly RoleClaimTypeGetPageList = this.prefix + '/roleClaimType/list';
  public readonly RoleClaimTypeCreate = this.prefix + '/roleClaimType/create';
  public readonly RoleClaimTypeUpdate = this.prefix + '/roleClaimType/update';
  public readonly RoleClaimTypeGetDetail = this.prefix + '/roleClaimType/detail';
  public readonly RoleClaimTypeDelete = this.prefix + '/roleClaimType/delete';
  public readonly RoleClaimTypeSetRoles = this.prefix + '/roleClaimType/setRoles';


  // Role Claim
  public readonly RoleClaimGetPageList = this.prefix + '/roleClaim/list';
  public readonly RoleClaimGetDetail = this.prefix + '/roleClaim/detail';
  public readonly RoleClaimCreate = this.prefix + '/roleClaim/create';
  public readonly RoleClaimUpdate = this.prefix + '/roleClaim/update';
  public readonly RoleClaimDelete = this.prefix + '/roleClaim/delete';
  public readonly RoleClaimSetRoles = this.prefix + '/roleClaim/setRoles';


  // Tenant Claim Type
  public readonly TenantClaimTypeGetPageList = this.prefix + '/tenantClaimType/list';
  public readonly TenantClaimTypeCreate = this.prefix + '/tenantClaimType/create';
  public readonly TenantClaimTypeUpdate = this.prefix + '/tenantClaimType/update';
  public readonly TenantClaimTypeGetDetail = this.prefix + '/tenantClaimType/detail';
  public readonly TenantClaimTypeDelete = this.prefix + '/tenantClaimType/delete';
  public readonly TenantClaimTypeSetTenants = this.prefix + '/tenantClaimType/setTenants';

  // Tenant Claim
  public readonly TenantClaimGetPageList = this.prefix + '/tenantClaim/list';
  public readonly TenantClaimGetDetail = this.prefix + '/tenantClaim/detail';
  public readonly TenantClaimCreate = this.prefix + '/tenantClaim/create';
  public readonly TenantClaimUpdate = this.prefix + '/tenantClaim/update';
  public readonly TenantClaimDelete = this.prefix + '/tenantClaim/delete';
  public readonly TenantClaimSetTenants = this.prefix + '/tenantClaim/setTenants';

  // Error
  public readonly ClientLogError = this.prefix + '/clientlog/error';

  // Password Policy
  public readonly PasswordPolicyDelete = this.prefix + '/passwordPolicy/delete';
  public readonly PasswordPolicyGetPageList = this.prefix + '/passwordPolicy/list';
  public readonly PasswordPolicyGetDetail = this.prefix + '/passwordPolicy/detail';
  public readonly PasswordPolicyCreate = this.prefix + '/passwordPolicy/create';
  public readonly PasswordPolicyUpdate = this.prefix + '/passwordPolicy/update';

  // Password Policy UserGroup
  public readonly PasswordPolicyUserGroupDelete = this.prefix + '/passwordPolicyUserGroup/delete';
  public readonly PasswordPolicyUserGroupUpdate = this.prefix + '/passwordPolicyUserGroup/update';
  public readonly PasswordPolicyUserGroupCreate = this.prefix + '/passwordPolicyUserGroup/create';
  public readonly PasswordPolicyUserGroupGetDetail = this.prefix + '/passwordPolicyUserGroup/detail';


  // Session
  public readonly SessionCreate = this.prefix + '/UserTenantSession/create';
  public readonly SessionGetDetail = this.prefix + '/UserTenantSession/detail';
  public readonly SessionAction = this.prefix + '/UserTenantSession/action';
  public readonly SessionDelete = this.prefix + '/UserTenantSession/delete';


}

export class RoutesKeys {
  public static readonly login = 'login';

}
export class AccessControlModel {
  public allowedModules: string[];
}
export class FeatureControlModel {
  public readonly DeleteDeprecatedCookies = 'DeleteDeprecatedCookies';
  public allowedFeatures: string[];
}
export class IdpClientCredentialsList {
  constructor(
    public list: Array<IdpClientCredentials>
  ) { }
}

export class ExternalUrls {
  public apiEndpoint: string;
}

export class IdpClientCredentials {
  constructor(
    public projectCode: string,
    public companyCode: string,
    public settingsCode?: string,
    public client_id?: string,
    public client_secret?: string,
    public isDefault?: boolean,
    public lang?: string
  ) { }
}
export class AppSettings {
  public version: string;
  public loggingLevelString: string;
  public baseHref: string;
  constructor(
    public envName: string,
    public settingsUrl: string,
    public production: boolean,
    public externalUrls: ExternalUrls,
    public idPConfig: AuthConfig,
    public dateFormats: DateFormats,
    public loggingLevel: LoggingLevel,
    public siteConfig: IdSignConfig,
    public defaultValues: DefaultValues,
    public apiKeys: ApiKeys,
    public accessControlModel: AccessControlModel,
    public featureControl: FeatureControlModel,
    public browserStorageKeys: BrowserStorageKeys,
    public idpClientCredentialsList: IdpClientCredentialsList,
    public currentClient: IdpClientCredentials
  ) { }
}

export interface LanguageSettings {
  langCode: string;
  flag: string;
}

export class EnvironmentModel {
  constructor(
    public Settings: AppSettings) {

  }
}
