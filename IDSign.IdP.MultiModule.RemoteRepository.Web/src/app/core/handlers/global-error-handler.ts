import { ErrorHandler, Injectable, Injector, OnDestroy } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { GlobalErrorSummary, GlobalError } from '@models/global-error-summary';
import { environment } from '@environments/environment';
import { LoggerService } from '@app/core/logging/logger.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { SubscriptionLike as ISubscription } from 'rxjs';

@Injectable()
export class GlobalErrorsHandler implements ErrorHandler, OnDestroy {

  private errorSubscriber: ISubscription;
  private logger: LoggerService;
  constructor(
    // Because the ErrorHandler is created before the providers, we’ll have to use the Injector to get them.
    private injector: Injector
  ) { }

  handleError(globalError: Error) {
    this.logger = this.injector.get(LoggerService).createInstance('GlobalErrorsHandler');
    this.errorSubscriber = this.logger.errorWait('Global Error Caught', globalError)
      .subscribe(result => {
        this.logger.debug('Error wait finished. Continuing.');
        globalError.message += ' ConnectionId:' + result;
        this.logic(globalError, result);
      }, error => {
        const message = 'WARNING: Error couldnt be sent to server! ' + JSON.stringify(error);
        console.error(message);
        globalError.message += message;
        this.logic(globalError, 'error');
      });
  }

  ngOnDestroy() {
    if (this.errorSubscriber) {
      this.errorSubscriber.unsubscribe();
    }
  }

  private logic(error: Error, connectionId: any) {
    const baseHref = this.injector.get(APP_BASE_HREF);
    let location = window.location.origin.toString() + baseHref;
    if (!location.endsWith('/')) {
      location = location + '/';
    }
    try {
      if (window.location.href.endsWith('/error')) {
        this.logger.error('The error was on the error page. Going to login.');

        // note: Router cannot be injected so we need to find another means to get around
        window.location.href = location + 'login';
      } else {
        const browserStorage = this.injector.get(BrowserStorage);
        const globalError = new GlobalErrorSummary();
        const safeError = new GlobalError();
        safeError.message = error.message;
        safeError.name = error.name;
        safeError.stack = error.stack.toString();
        globalError.error = safeError;
        globalError.url = window.location.href;
        globalError.connectionId = connectionId;
        globalError.time = new Date();
        browserStorage.setObject(environment.Settings.browserStorageKeys.lastGlobalError, globalError);

        // note: Router cannot be injected so we need to find another means to get around
        window.location.href = location + 'error';
      }
    } catch (ex) {
      console.error('Something wrong happened in handling the global error.');
      console.error(ex);
    }
  }
}
