import { NgModule } from '@angular/core';
import { EnvironmentService } from '@http/environment/environment.service';

@NgModule({
})

export class EnvironmentInitializerModule { }
export function onAppInitLoadEnvironment(environmentService: EnvironmentService): () => Promise<any> {
    return (): Promise<any> => {
        return environmentService.getSettingsFromServer();
    };
}
