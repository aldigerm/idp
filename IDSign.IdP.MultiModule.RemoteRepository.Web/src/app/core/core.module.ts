import { APP_BASE_HREF, CommonModule, PlatformLocation } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OAuthService, UrlHelperService, OAuthModule } from 'angular-oauth2-oidc';
import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from '@app/app-routing.module';
import { onAppInitLoadEnvironment } from '@app/core/initializers/environment-initializer';
import { GlobalErrorsHandler } from '@app/core/handlers/global-error-handler';
import { ShowSectionPreLoaderGuard } from '@app/modules/pages/guard/show-section-preloader.guard';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { FeatureControlPipeModule } from '@app/shared/pipes/control/feature-control/feature-control.pipe.module';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthLoginGuard } from './guards/auth.guard.login';
import { ShowPreLoaderGuard } from './guards/show-preloader.guard';
import { EnvironmentService } from '@http/environment/environment.service';
import { UserService } from '@http/user/user.service';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ClientLoggerService } from './logging/client-logger.service';
import { ConsoleLoggerWithClientService } from './logging/console-logger-with-client.service';
import { LoggerConfiguratorService } from './logging/logger-configurator.service';
import { LoggerService, Logger } from '@app/core/logging/logger.service';
import { AccessControlService } from '@services/access-control.service';
import { SiteConfiguratorService } from '@services/site-configurator.service';
import { QueryStringUtilitiesService } from '@services/query-string-utilities.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { CacheService } from '@services/storage/cache.service';
import { UserCacheService } from '@services/storage/user-cache.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { TableService } from '@services/html/table/table.service';
import { SiteTranslateService } from '@services/site-translate.service';
import { SweetAlertModule } from '@app/shared/components/sweet-alert/sweet-alert.module';
import { DateFormatPipe } from '@app/shared/pipes/calendar/date/date-format.pipe';
import { DateTimeFormatPipe } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { TimelineService } from '@services/html/timeline/timeline.service';
import { MenuService } from '@services/menu.service';
import { MultilevelMenuService } from '@app/shared/components/ng-material-multilevel-menu/multilevel-menu.service';
import { LocationService } from '@services/location.service';
import { FeatureControlService } from '@services/feature-control.service';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { RemoteRepositoryProjectService } from '@http/remote-repository/project/remote-repository-project.service';
import { RemoteRepositoryTenantService } from '@http/remote-repository/tenant/remote-repository-tenant.service';
import { PageGuard } from '@app/modules/pages/guard/page.guard';
import { ProfileGuard } from './guards/profile.guard';
import { SiteActionControlService } from '@services/site-action-control.service';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { RemoteRepositoryRoleService } from '@http/remote-repository/role/remote-repository-role.service';
import { RemoteRepositoryModuleService } from '@http/remote-repository/module/remote-repository-module.service';
import { SiteCookieService } from '@services/storage/site-cookie.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { RemoteRepositoryUserClaimTypeService } from '@http/remote-repository/user-claim-type/remote-repository-user-claim-type.service';
import { RemoteRepositoryUserClaimService } from './http/remote-repository/user-claim/remote-repository-user-claim.service';
import {
  RemoteRepositoryUserGroupClaimTypeService
} from '@http/remote-repository/user-group-claim-type/remote-repository-user-group-claim-type.service';
import { RemoteRepositoryUserGroupClaimService } from '@http/remote-repository/user-group-claim/remote-repository-user-group-claim.service';
import { RemoteRepositoryRoleClaimTypeService } from '@http/remote-repository/role-claim-type/remote-repository-role-claim-type.service';
import { RemoteRepositoryRoleClaimService } from '@http/remote-repository/role-claim/remote-repository-role-claim.service';
import {
  RemoteRepositoryTenantClaimTypeService
} from '@http/remote-repository/tenant-claim-type/remote-repository-tenant-claim-type.service';
import {
  RemoteRepositoryTenantClaimService
} from '@http/remote-repository/tenant-claim/remote-repository-tenant-claim.service';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';
import {
  RemoteRepositoryPasswordPolicyUserGroupService
} from '@http/remote-repository/password-policy-user-group/password-policy-user-group.service';
import { RemoteRepositorySessionService } from '@http/remote-repository/session/remote-repository-session.service';

export function getLogger(clientLoggerService: ClientLoggerService): Logger {
  return new ConsoleLoggerWithClientService().setClientLogger(clientLoggerService);
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

/* Swipe left and right configuration */
declare var Hammer: any;
export class MyHammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'pan-y',
    });
    return mc;
  }
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SweetAlertModule,
    NgbModule.forRoot(),
    CommonModule,
    AccessControlPipeModule,
    FeatureControlPipeModule,
    OAuthModule
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseHref,
      deps: [PlatformLocation]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: onAppInitLoadEnvironment,
      multi: true,
      deps: [EnvironmentService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorsHandler,
    },
    AuthGuard,
    AuthLoginGuard,
    ShowPreLoaderGuard,
    ShowSectionPreLoaderGuard,
    ProfileGuard,
    TableService,
    DateFormatPipe,
    DateTimeFormatPipe,
    SweetAlertService,
    {
      provide: LoggerService
      , useFactory: getLogger
      , deps: [ClientLoggerService]
    },
    QueryStringUtilitiesService,
    TranslateService,
    EnvironmentService,
    SiteConfiguratorService,
    CacheService,
    CookieService,
    UserService,
    BrowserStorage,
    UrlHelperService,
    CacheService,
    UserCacheService,
    SiteTranslateService,
    AuthenticationService,
    OAuthService,
    AccessControlService,
    PagePreloaderService,
    LoggerConfiguratorService,
    SectionPreloaderService,
    ClientLoggerService,
    WindowEventListenerService,
    ScrollService,
    TimelineService, {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    MenuService,
    MultilevelMenuService,
    LocationService,
    FeatureControlService,
    RemoteRepositoryUserService,
    RemoteRepositoryTenantService,
    RemoteRepositoryProjectService,
    RemoteRepositoryUserGroupService,
    RemoteRepositoryRoleService,
    RemoteRepositoryModuleService,
    RemoteRepositoryUserClaimTypeService,
    RemoteRepositoryUserClaimService,
    RemoteRepositoryUserGroupClaimTypeService,
    RemoteRepositoryUserGroupClaimService,
    RemoteRepositoryRoleClaimTypeService,
    RemoteRepositoryRoleClaimService,
    RemoteRepositoryTenantClaimTypeService,
    RemoteRepositoryTenantClaimService,
    RemoteRepositoryPasswordPolicyService,
    RemoteRepositoryPasswordPolicyUserGroupService,
    RemoteRepositorySessionService,
    PageGuard,
    SiteActionControlService,
    SiteCookieService,
    SiteChangeDetectorService
  ]
})
export class CoreModule { }
