import { Component, OnInit, AfterViewInit, OnDestroy, Output, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { User } from '@models/user';
import { faPowerOff, faHandshake } from '@fortawesome/free-solid-svg-icons';
import { EventEmitter } from 'events';
import { ISubscription } from 'rxjs/Subscription';
import { environment } from '@environments/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { Router } from '@angular/router';
import { UserService } from '@http/user/user.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { LoggerService } from '../logging/logger.service';
import { WindowEventListenerService, LayoutSetup } from '@services/window-listener.service';
import { LocationService } from '@services/location.service';
import { detectChangesWithCheck } from '@app/sharedfunctions/change-detector.function';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SiteTranslateService } from '@services/site-translate.service';
import { LanguageSettings } from '../configs/global';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, AfterViewInit, OnDestroy {
    name: string;
    showMiniBar: boolean;
    user: User;
    username: string;
    partnerCompanyCode: string;
    currentFlag = 'gb';
    langFlags = new Array<LanguageSettings>();
    faPowerOff = faPowerOff;
    faHandshake = faHandshake;
    profileLink = '#';

    private _windowSubscriber: ISubscription;
    private _langSubscriber: ISubscription;
    private _notificationAllSubscriber: ISubscription;
    private _notificationFetchSubscriber: ISubscription;
    private _notificationReadSubscriber: ISubscription;
    private _notificationConnectionSubscriber: ISubscription;
    private _notificationConnectionReceivedSubscriber: ISubscription;
    public readonly localizationString = 'Header';


    hasNewNotifications: boolean;
    private _isNotificationEnabled = false;

    constructor(private _authenticationService: AuthenticationService,
        private _router: Router
        , private userService: UserService
        , private siteTranslateService: SiteTranslateService
        , private translateService: TranslateService
        , private logger: LoggerService
        , private _changeDetectorRef: ChangeDetectorRef
        , private _windowListener: WindowEventListenerService
        , private _locationService: LocationService
        , private _siteAlertService: SweetAlertService
        , private _renderer: Renderer2
    ) {

        this.logger = this.logger.createInstance('NavigationComponent');

        this.showMiniBar = true;

        // fall back to the default list in case it is not specified for the company
        if (!this.langFlags || this.langFlags.length === 0) {
            this.langFlags = environment.Settings.defaultValues.availableLanguages;
        }
        this._langSubscriber = this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
            this.setFlag(event.lang);
        });
    }

    changeShowStatus() {
        this.setShowMinibar(!this.showMiniBar);
    }

    setLanguage(lang: string) {
        this.siteTranslateService.setLanguage(lang);
    }

    setFlag(lang: string) {
        let setting: LanguageSettings;
        if (lang) {
            setting = this.langFlags.find(f => f.langCode === lang);
        }
        if (setting && setting.flag) {
            this.currentFlag = setting.flag;
        } else {
            this.currentFlag = 'gb';
        }
    }


    updateMenuLayout(layout?: LayoutSetup) {

        // if we have a layout object then we use it
        // otherwise get the value
        const isDesktop = layout ? layout.isDesktop : this._windowListener.getCurrentLayoutSetup().isDesktop;

        // if we enter desktop, then we show the full menu
        // otherwise always hide it
        if (isDesktop) {
            this.setShowMinibar(false);
        } else {
            this.setShowMinibar(true);
        }

    }

    setShowMinibar(value: boolean) {
        this.showMiniBar = value;
        if (this.showMiniBar) {
            this._renderer.addClass(document.body, 'mini-sidebar');
        } else {
            this._renderer.removeClass(document.body, 'mini-sidebar');
        }
        this._windowListener.TriggerOnMenu();
        detectChangesWithCheck(this._changeDetectorRef);
    }

    logout(): boolean {
        this._authenticationService.signout();
        return false;
    }

    ngOnInit() {
        this.userService.getUser().then(user => {
            this.user = user;
            this.name = user.firstName + ' ' + user.lastName;
            this.username = user.userProfile.sub;
        });

        const currentLanguage = this.siteTranslateService.getLanguage();
        this.setFlag(currentLanguage);
        this.logger.debug('current flag is ' + this.currentFlag);

    }


    ngAfterViewInit() {

        // we set the layout for the given screen
        // on loading of page
        this.updateMenuLayout();

        // we update the menu if the screen layout
        // changes in between devices
        this._windowSubscriber = this._windowListener.OnLayoutChange()
            .subscribe(layout => {
                this.updateMenuLayout(layout);
            });

    }

    ngOnDestroy() {

        this._changeDetectorRef.detach();

        if (this._windowSubscriber) {
            this._windowSubscriber.unsubscribe();
        }
        if (this._langSubscriber) {
            this._langSubscriber.unsubscribe();
        }
        if (this._notificationAllSubscriber) {
            this._notificationAllSubscriber.unsubscribe();
        }
        if (this._notificationFetchSubscriber) {
            this._notificationFetchSubscriber.unsubscribe();
        }
        if (this._notificationReadSubscriber) {
            this._notificationReadSubscriber.unsubscribe();
        }
        if (this._notificationConnectionReceivedSubscriber) {
            this._notificationConnectionReceivedSubscriber.unsubscribe();
        }
        if (this._notificationConnectionSubscriber) {
            this._notificationConnectionSubscriber.unsubscribe();
        }
    }

}
