import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings } from '@app/core/configs/global';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment, RouteLinks } from '@environments/environment';
import { LoggingLevel } from '@models/logging/logging-level.enum';
import { SiteConfiguratorService } from '@services/site-configurator.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { UserService } from '@http/user/user.service';
import { EnvironmentVersion } from './environment.version';

@Injectable()
export class EnvironmentService {

    private cacheKey = environment.Settings.browserStorageKeys.environmentSettingKey;
    private envSpecificSubject: BehaviorSubject<AppSettings> = new BehaviorSubject<AppSettings>(null);
    private readonly redirectUri = 'callback';
    private readonly silentRefreshUri = 'silent-refresh.html';
    private readonly postLogoutRedirectUrl = 'login';
    private router: Router;
    private pagePreloader: PagePreloaderService;


    constructor(
        private logger: LoggerService,
        private injector: Injector,
        private browserStorage: BrowserStorage
    ) {
        this.logger = this.logger.createInstance('EnvironmentService');
    }

    public loadEnvironment(): AppSettings {
        return environment.Settings;
    }

    public removeCachedEnvironment() {
        this.browserStorage.remove(this.cacheKey);
        this.logger.debug('%cRemoved cached environment.', 'color:red');
    }

    public setEnvSpecific(es: AppSettings) {
        // This has already been set so bail out.
        if (es === null || es === undefined) {
            return;
        }

        environment.Settings = es;

        // remove object
        // this.browserStorage.remove(this.cacheKey);

        // set it again
        this.browserStorage.setObject(this.cacheKey, environment.Settings);

        if (this.envSpecificSubject) {
            this.envSpecificSubject.next(environment.Settings);
        }
    }

    /*
      Call this if you want to know when EnvSpecific is set.
    */
    public subscribe(caller: any, callback: (caller: any, es: AppSettings) => void) {
        this.envSpecificSubject
            .subscribe((es) => {
                if (es === null) {
                    return;
                }
                callback(caller, es);
            });
    }
    public getSettingsFromServer(): Promise<boolean> {

        const http = this.injector.get(HttpClient);
        const authenticationService = this.injector.get(AuthenticationService);
        const userService = this.injector.get(UserService);
        this.pagePreloader = this.injector.get(PagePreloaderService);
        this.router = this.injector.get(Router);
        const state = this.injector.get(Router);
        const url: string = window.location.pathname;
        const baseHref = this.injector.get(APP_BASE_HREF);
        const location = window.location.origin + baseHref;
        const loginLink = '/login';
        this.logger.info('Location is ' + location);

        const siteConfiguratorService = this.injector.get(SiteConfiguratorService);
        let isLogin = false;
        this.logger.debug('Url is ' + url);
        if (url === loginLink) {
            isLogin = true;
        }

        return new Promise((resolve, reject) => {
            const cachedSettings = this.browserStorage.getObject(this.cacheKey) as AppSettings;


            const hasError = this.browserStorage.get(environment.Settings.browserStorageKeys.environmentLoadingError);

            if (hasError) {
                this.logger.debug('There is an error stored. Allowing through.');
                resolve();
            } else {
                // if it is cached
                // or if the version of the cached setting matches the saved version
                // this is needed so that if the cached version is older than the deployed version
                // then the settings are retrieved

                this.logger.debug('Looking for version ' + EnvironmentVersion + ' of settings...');
                if (!isLogin && cachedSettings && cachedSettings.version === EnvironmentVersion) {
                    this.setEnvSpecific(cachedSettings);
                    this.logger.debug('Loaded from cache : ' + JSON.stringify(cachedSettings));
                    siteConfiguratorService.configure()
                        .then(result => {
                            this.configureAuthenticationService(authenticationService)
                                .then(() => {

                                    // the current client is expected to have been
                                    // set in siteConfiguratorService.configure()
                                    const companyCode = environment.Settings.currentClient.companyCode;

                                    resolve();

                                }).catch(error => {
                                    this.redirectToLoginWithErrorAuthentication(error);
                                    resolve();
                                });
                        }).catch(error => {
                            this.redirectToLoginWithSiteConfigurationErrorSettings(error);
                            resolve();
                        });
                } else {

                    this.pagePreloader.setLocalisedMessage('PagePreloader.GettingSettings');

                    // we clear the cached data
                    this.browserStorage.removeAllSiteData();

                    let oldVersion = EnvironmentVersion;
                    let requireUpdate = false;
                    if (cachedSettings && cachedSettings.version !== EnvironmentVersion) {
                        oldVersion = cachedSettings.version;
                        requireUpdate = true;
                        this.logger.warn('Settings version and application versions do not match. ' +
                            ' Settings : "' + cachedSettings.version
                            + '" Application : "' + EnvironmentVersion + '"');
                    }
                    const settingsUrl = environment.Settings.settingsUrl + '?v=' + oldVersion;
                    http.get<AppSettings>(settingsUrl)
                        .toPromise<AppSettings>()
                        .then(retrievedSettings => {

                            // read default values in hardcoded environment settings
                            const environmentToSave = environment.Settings;

                            // set production fag for angular app
                            environmentToSave.production = retrievedSettings.production;

                            // read settings from the server
                            environmentToSave.version = retrievedSettings.version;
                            environmentToSave.envName = retrievedSettings.envName;
                            environmentToSave.externalUrls = retrievedSettings.externalUrls;
                            environmentToSave.idpClientCredentialsList = retrievedSettings.idpClientCredentialsList;
                            environmentToSave.featureControl = retrievedSettings.featureControl;

                            // configure oauth settings
                            environmentToSave.idPConfig = retrievedSettings.idPConfig;

                            if (environmentToSave.idPConfig) {
                                environmentToSave.idPConfig.redirectUri =
                                    location + this.redirectUri;

                                // if a full url is not provided or one was provided that doesn't start with "http",
                                // then prepend the current domain location and the default suffix
                                if (!environmentToSave.idPConfig.silentRefreshRedirectUri
                                    || (environmentToSave.idPConfig.silentRefreshRedirectUri
                                        && !environmentToSave.idPConfig.silentRefreshRedirectUri.startsWith('http'))) {
                                    environmentToSave.idPConfig.silentRefreshRedirectUri =
                                        location + this.silentRefreshUri;
                                }

                                environmentToSave.idPConfig.postLogoutRedirectUri =
                                    location + this.postLogoutRedirectUrl;
                            }

                            // if the api endpoint url is not fully provided
                            // then we prepend the current domain to it
                            if (environmentToSave.externalUrls.apiEndpoint
                                && !environmentToSave.externalUrls.apiEndpoint.startsWith('http')) {
                                environmentToSave.externalUrls.apiEndpoint =
                                    window.location.origin + environmentToSave.externalUrls.apiEndpoint;
                            }

                            // read string logging level
                            environmentToSave.loggingLevel = LoggingLevel[retrievedSettings.loggingLevelString];

                            // set basehref for global use
                            environmentToSave.baseHref = baseHref;

                            this.setEnvSpecific(environmentToSave);
                            this.logger.setLevel(environmentToSave.loggingLevel);
                            this.logger.debug('Loaded settings : ' + JSON.stringify(environmentToSave));

                            if (requireUpdate && EnvironmentVersion === environmentToSave.version) {
                                this.logger.warn('Environment settings updated to version ' + environmentToSave.version);
                            }
                            siteConfiguratorService.configure()
                                .then(result => {
                                    this.configureAuthenticationService(authenticationService)
                                        .then(() => {

                                            // the current client is expected to have been
                                            // set in siteConfiguratorService.configure()
                                            const companyCode = environment.Settings.currentClient.companyCode;

                                            resolve();

                                        }).catch(error => {
                                            this.redirectToLoginWithErrorAuthentication(error);
                                            resolve();
                                        });
                                }).catch(error => {
                                    this.redirectToLoginWithSiteConfigurationErrorSettings(error);
                                    resolve();
                                });

                        })
                        .catch(error => {
                            this.logger.error('Couldnt load environment settings.', error);
                            this.redirectToLoginWithErrorSettings(error);
                            resolve();
                        });
                }
            }
        });
    }

    configureAuthenticationService(authenticationService: AuthenticationService): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            // we configure only if the settings changed
            authenticationService.configureWithNewConfigApi()
                .then(value => {
                    this.logger.debug('Authentication Configuration state is : ' + value);
                    resolve();
                })
                .catch(error => {
                    this.logger.error('Authentication Configuration gave an exception', error);
                    this.redirectToLoginWithErrorAuthentication(error);
                    resolve();
                });
        });
    }

    private redirectToLoginWithErrorAuthentication(error?: any): boolean {
        this.logger.error('Couldnt load settings in some way', error);
        this.browserStorage.set(environment.Settings.browserStorageKeys.environmentLoadingError, 'authentication');
        this.router.navigate([RouteLinks.login]);
        return false;
    }

    private redirectToLoginWithErrorSettings(error?: any): boolean {
        this.logger.error('Couldnt load settings in some way', error);
        this.browserStorage.set(environment.Settings.browserStorageKeys.environmentLoadingError, 'environment');
        this.router.navigate([RouteLinks.login]);
        return false;
    }

    private redirectToLoginWithSiteConfigurationErrorSettings(error?: any): boolean {
        this.logger.error('Couldnt load settings in some way', error);
        this.browserStorage.set(environment.Settings.browserStorageKeys.environmentLoadingError, 'siteconfiguration');
        this.router.navigate([RouteLinks.login]);
        return false;
    }
}
