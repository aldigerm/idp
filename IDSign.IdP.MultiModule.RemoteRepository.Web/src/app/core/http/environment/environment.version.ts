declare var require: any;
const { settingsVersion: settingsVersion } = require('./info.json');

export const EnvironmentVersion = settingsVersion;
