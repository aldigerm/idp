import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { TenantDetailModel } from '@models/remote-repository/tenant/remote-repository-tenant-detail';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { TenantListModel, TenantListItemModel } from '@models/remote-repository/tenant/list/remote-repository-tenant-list';
import { TenantUpdateModel } from '@models/remote-repository/tenant/remote-repository-tenant-update';
import { TenantCreateResult, TenantCreateModel } from '@models/remote-repository/tenant/remote-repository-tenant-create';
import { TenantListPageModel } from '@models/remote-repository/tenant/list/remote-repository-tenant-page';
import { ProjectIdentifier } from '@app/shared/models/identifiers/project-identifier';
import { TenantBasicModel } from '@models/remote-repository/tenant/remote-repository-tenant-basic';
import { TenantIdentifier } from '@app/shared/models/identifiers/tenant-identifier';

@Injectable()
export class RemoteRepositoryTenantService {

    private detailChangeObservable = new EventEmitter<TenantIdentifier>();
    private detailModelChangeObservable = new EventEmitter<TenantDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('TenantService');
    }

    public getPageList(identifier: ProjectIdentifier): Promise<Array<TenantListItemModel>> {
        return this.http.get<Array<TenantListItemModel>>(environment.Settings.apiKeys.idP.TenantGetPageList
            , { params: identifier as any })
            .toPromise();
    }

    public getDetail(identifier: TenantIdentifier): Promise<TenantDetailModel> {
        return this.http.get<TenantDetailModel>(environment.Settings.apiKeys.idP.TenantGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: TenantCreateModel): Promise<TenantIdentifier> {
        return new Promise<TenantIdentifier>((resolve, reject) => {
            this.http.post<TenantIdentifier>(environment.Settings.apiKeys.idP.TenantCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: TenantUpdateModel): Promise<TenantIdentifier> {
        return new Promise<TenantIdentifier>((resolve, reject) => {
            this.http.post<TenantIdentifier>(environment.Settings.apiKeys.idP.TenantUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: TenantIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.TenantDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: TenantIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: TenantIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<TenantIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<TenantDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
