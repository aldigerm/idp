/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryTenantService } from './remote-repository-tenant.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryTenantService]
    });
  });

  it('should ...', inject([RemoteRepositoryTenantService], (service: RemoteRepositoryTenantService) => {
    expect(service).toBeTruthy();
  }));
});
