/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryRoleClaimTypeService } from './remote-repository-role-claim-type.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryRoleClaimTypeService]
    });
  });

    it('should ...', inject([RemoteRepositoryRoleClaimTypeService], (service: RemoteRepositoryRoleClaimTypeService) => {
    expect(service).toBeTruthy();
  }));
});
