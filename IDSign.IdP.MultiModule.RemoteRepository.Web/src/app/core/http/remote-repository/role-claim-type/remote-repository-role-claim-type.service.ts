import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';
import { RoleClaimTypeDetailModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type';
import { RoleClaimTypeUpdateModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type-update';
import {
    RoleClaimTypeSetRoles
} from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type-set-roles';
import { RoleClaimTypeCreateModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type-create';
import {
    RoleClaimTypeListItemModel
} from '@models/remote-repository/role/claims/role-claim-type/list/remote-repository-role-claim-list-item';


@Injectable()
export class RemoteRepositoryRoleClaimTypeService {

    private detailChangeObservable = new EventEmitter<RoleClaimTypeIdentifier>();
    private detailModelChangeObservable = new EventEmitter<RoleClaimTypeDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('RoleClaimTypeService');
    }


    public getPageList(): Promise<Array<RoleClaimTypeListItemModel>> {
        return this.http.get<Array<RoleClaimTypeListItemModel>>(environment.Settings.apiKeys.idP.RoleClaimTypeGetPageList)
            .toPromise();
    }

    public getDetail(identifier: RoleClaimTypeIdentifier): Promise<RoleClaimTypeDetailModel> {
        return this.http.get<RoleClaimTypeDetailModel>(environment.Settings.apiKeys.idP.RoleClaimTypeGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: RoleClaimTypeCreateModel): Promise<RoleClaimTypeIdentifier> {
        return new Promise<RoleClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<RoleClaimTypeIdentifier>(environment.Settings.apiKeys.idP.RoleClaimTypeCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: RoleClaimTypeUpdateModel): Promise<RoleClaimTypeIdentifier> {
        return new Promise<RoleClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<RoleClaimTypeIdentifier>(environment.Settings.apiKeys.idP.RoleClaimTypeUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: RoleClaimTypeIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.RoleClaimTypeDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public setRoleMembership(model: RoleClaimTypeSetRoles): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.RoleClaimTypeSetRoles, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: RoleClaimTypeIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: RoleClaimTypeIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }
    public detailChangeEvent(): Observable<RoleClaimTypeIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<RoleClaimTypeDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
