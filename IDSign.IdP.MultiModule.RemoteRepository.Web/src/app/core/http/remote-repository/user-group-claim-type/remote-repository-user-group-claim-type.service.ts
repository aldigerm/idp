import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';
import {
    UserGroupClaimTypeDetailModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type';
import {
    UserGroupClaimTypeUpdateModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type-update';
import {
    UserGroupClaimTypeSetUserGroups
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type-set-user-groups';
import {
    UserGroupClaimTypeCreateModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type-create';
import {
    UserGroupClaimTypeListItemModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/list/remote-repository-user-group-claim-list-item';


@Injectable()
export class RemoteRepositoryUserGroupClaimTypeService {

    private detailChangeObservable = new EventEmitter<UserGroupClaimTypeIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserGroupClaimTypeDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserGroupClaimTypeService');
    }


    public getPageList(): Promise<Array<UserGroupClaimTypeListItemModel>> {
        return this.http.get<Array<UserGroupClaimTypeListItemModel>>(environment.Settings.apiKeys.idP.UserGroupClaimTypeGetPageList)
            .toPromise();
    }

    public getDetail(identifier: UserGroupClaimTypeIdentifier): Promise<UserGroupClaimTypeDetailModel> {
        return this.http.get<UserGroupClaimTypeDetailModel>(environment.Settings.apiKeys.idP.UserGroupClaimTypeGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: UserGroupClaimTypeCreateModel): Promise<UserGroupClaimTypeIdentifier> {
        return new Promise<UserGroupClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<UserGroupClaimTypeIdentifier>(environment.Settings.apiKeys.idP.UserGroupClaimTypeCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserGroupClaimTypeUpdateModel): Promise<UserGroupClaimTypeIdentifier> {
        return new Promise<UserGroupClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<UserGroupClaimTypeIdentifier>(environment.Settings.apiKeys.idP.UserGroupClaimTypeUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: UserGroupClaimTypeIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.UserGroupClaimTypeDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public setUserGroupMembership(model: UserGroupClaimTypeSetUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.UserGroupClaimTypeSetUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: UserGroupClaimTypeIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserGroupClaimTypeIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }
    public detailChangeEvent(): Observable<UserGroupClaimTypeIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserGroupClaimTypeDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
