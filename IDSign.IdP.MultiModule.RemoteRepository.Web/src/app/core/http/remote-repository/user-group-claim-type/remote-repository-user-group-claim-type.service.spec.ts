/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserGroupClaimTypeService } from './remote-repository-user-group-claim-type.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [RemoteRepositoryUserGroupClaimTypeService]
    });
  });

    it('should ...', inject([RemoteRepositoryUserGroupClaimTypeService], (service: RemoteRepositoryUserGroupClaimTypeService) => {
    expect(service).toBeTruthy();
  }));
});
