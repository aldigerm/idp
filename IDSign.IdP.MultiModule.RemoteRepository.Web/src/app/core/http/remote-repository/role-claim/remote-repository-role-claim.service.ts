import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { RoleClaimUpdateModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim-update';
import { RoleClaimDetailModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim';
import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';
import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import {
    RoleClaimSetRoles
} from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim-set-roles';
import { RoleClaimCreateModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim-create';
import { RoleClaimTypeDetailModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type';
import { RoleClaimListItemModel } from '@models/remote-repository/role/claims/role-claim/list/remote-repository-role-claim-list-item';

@Injectable()
export class RemoteRepositoryRoleClaimService {

    private detailChangeObservable = new EventEmitter<RoleClaimIdentifier>();
    private detailModelChangeObservable = new EventEmitter<RoleClaimDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('RoleClaimService');
    }


    public getPageList(): Promise<Array<RoleClaimListItemModel>> {
        return this.http.get<Array<RoleClaimListItemModel>>(environment.Settings.apiKeys.idP.RoleClaimGetPageList)
            .toPromise();
    }

    public getDetail(identifier: RoleClaimIdentifier): Promise<RoleClaimDetailModel> {
        return this.http.get<RoleClaimDetailModel>(environment.Settings.apiKeys.idP.RoleClaimGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: RoleClaimCreateModel): Promise<RoleClaimIdentifier> {
        return new Promise<RoleClaimIdentifier>((resolve, reject) => {
            return this.http.post<RoleClaimIdentifier>(environment.Settings.apiKeys.idP.RoleClaimCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: RoleClaimUpdateModel): Promise<RoleClaimIdentifier> {
        return new Promise<RoleClaimIdentifier>((resolve, reject) => {
            return this.http.post<RoleClaimIdentifier>(environment.Settings.apiKeys.idP.RoleClaimUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setRoleMembership(model: RoleClaimSetRoles): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.RoleClaimSetRoles, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: RoleClaimIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.RoleClaimDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: RoleClaimIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: RoleClaimIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<RoleClaimIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<RoleClaimDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
