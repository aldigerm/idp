/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryRoleClaimService } from './remote-repository-role-claim.service';

describe('Service: notarization', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [RemoteRepositoryRoleClaimService]
        });
    });

    it('should ...', inject([RemoteRepositoryRoleClaimService], (service: RemoteRepositoryRoleClaimService) => {
        expect(service).toBeTruthy();
    }));
});
