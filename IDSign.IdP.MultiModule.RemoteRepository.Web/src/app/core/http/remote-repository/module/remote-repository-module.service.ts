import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { ModuleBasicModel } from '@models/remote-repository/module/remote-repository-module-basic';
import { ModuleIdentifier } from '@models/identifiers/module-identifier';
import { ModuleCreateModel } from '@models/remote-repository/module/remote-repository-module-create';
import { ModuleUpdateModel } from '@models/remote-repository/module/remote-repository-module-update';
import { ModuleDetailModel } from '@models/remote-repository/module/remote-repository-module-detail';
import { ModuleListItemModel } from '@models/remote-repository/module/list/remote-repository-module-list';

@Injectable()
export class RemoteRepositoryModuleService {

    private detailChangeObservable = new EventEmitter<ModuleIdentifier>();
    private detailModelChangeObservable = new EventEmitter<ModuleDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('ModuleService');
    }

    public getPageList(): Promise<Array<ModuleListItemModel>> {
        return this.http.get<Array<ModuleListItemModel>>(environment.Settings.apiKeys.idP.ModuleGetPageList)
            .toPromise();
    }

    public getDetail(identifier: ModuleIdentifier): Promise<ModuleDetailModel> {
        return this.http.get<ModuleDetailModel>(environment.Settings.apiKeys.idP.ModuleGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: ModuleCreateModel): Promise<ModuleIdentifier> {
        return new Promise<ModuleIdentifier>((resolve, reject) => {
            this.http.post<ModuleIdentifier>(environment.Settings.apiKeys.idP.ModuleCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: ModuleUpdateModel): Promise<ModuleIdentifier> {
        return new Promise<ModuleIdentifier>((resolve, reject) => {
            this.http.post<ModuleIdentifier>(environment.Settings.apiKeys.idP.ModuleUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: ModuleIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.ModuleDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public triggerDetailChange(identfier: ModuleIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: ModuleIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<ModuleIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<ModuleDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
