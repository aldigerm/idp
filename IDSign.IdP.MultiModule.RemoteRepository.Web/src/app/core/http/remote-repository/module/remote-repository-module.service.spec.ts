/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryModuleService } from './remote-repository-module.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryModuleService]
    });
  });

    it('should ...', inject([RemoteRepositoryModuleService], (service: RemoteRepositoryModuleService) => {
    expect(service).toBeTruthy();
  }));
});
