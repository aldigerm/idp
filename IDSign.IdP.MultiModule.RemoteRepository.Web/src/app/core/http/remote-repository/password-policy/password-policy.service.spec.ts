/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryPasswordPolicyService } from './password-policy.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryPasswordPolicyService]
    });
  });

  it('should ...', inject([RemoteRepositoryPasswordPolicyService], (service: RemoteRepositoryPasswordPolicyService) => {
    expect(service).toBeTruthy();
  }));
});
