import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { PasswordPolicyIdentifier } from '@models/identifiers/password-policy-identifier';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { PasswordPolicyCreateModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-create';
import { PasswordPolicyUpdateModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-update';
import { PasswordPolicyListItemModel } from '@models/remote-repository/password-policy/list/remote-repository-password-policy-list';

@Injectable()
export class RemoteRepositoryPasswordPolicyService {

    private detailChangeObservable = new EventEmitter<PasswordPolicyIdentifier>();
    private detailModelChangeObservable = new EventEmitter<PasswordPolicyDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('PasswordPolicyService');
    }


    public getPageList(): Promise<Array<PasswordPolicyListItemModel>> {
        return this.http.get<Array<PasswordPolicyListItemModel>>(environment.Settings.apiKeys.idP.PasswordPolicyGetPageList)
            .toPromise();
    }

    public getDetail(identifier: PasswordPolicyIdentifier): Promise<PasswordPolicyDetailModel> {
        return this.http.get<PasswordPolicyDetailModel>(environment.Settings.apiKeys.idP.PasswordPolicyGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: PasswordPolicyCreateModel): Promise<PasswordPolicyIdentifier> {
        return new Promise<PasswordPolicyIdentifier>((resolve, reject) => {
            return this.http.post<PasswordPolicyIdentifier>(environment.Settings.apiKeys.idP.PasswordPolicyCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: PasswordPolicyUpdateModel): Promise<PasswordPolicyIdentifier> {
        return new Promise<PasswordPolicyIdentifier>((resolve, reject) => {
            return this.http.post<PasswordPolicyIdentifier>(environment.Settings.apiKeys.idP.PasswordPolicyUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public delete(identifier: PasswordPolicyIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.PasswordPolicyDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: PasswordPolicyIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: PasswordPolicyIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<PasswordPolicyIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<PasswordPolicyDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
