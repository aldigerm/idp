import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { UserTenantSessionDetailModel } from '@models/remote-repository/session/remote-repository-session-detail';
import { Observable } from 'rxjs';
import { UserTenantSessionActionRequestModel } from '@models/remote-repository/session/action/remote-repository-session-action-request';
import { UserTenantSessionActionResponseModel } from '@models/remote-repository/session/action/remote-repository-session-action-response';

@Injectable()
export class RemoteRepositorySessionService {

    private detailChangeObservable = new EventEmitter<string>();
    private detailModelChangeObservable = new EventEmitter<UserTenantSessionDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('SessionService');
    }

    public getDetail(identifier: string): Promise<UserTenantSessionDetailModel> {
        return this.http.get<UserTenantSessionDetailModel>(environment.Settings.apiKeys.idP.SessionGetDetail
            , { params: { identifier: identifier } })
            .toPromise();
    }

    public create(model: UserTenantSessionDetailModel): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.post<string>(environment.Settings.apiKeys.idP.SessionCreate, model)
                .toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public action(model: UserTenantSessionActionRequestModel): Promise<UserTenantSessionActionResponseModel> {
        return new Promise<UserTenantSessionActionResponseModel>((resolve, reject) => {
            this.http.post<UserTenantSessionActionResponseModel>(environment.Settings.apiKeys.idP.SessionAction, model)
                .toPromise()
                .then(response => {
                    this.triggerDetailChange(model.identifier);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
    public delete(identifier: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.SessionDelete
                , { params: { identifier: identifier } })
                .toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: string): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: string): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<string> {
        return this.detailChangeObservable.asObservable();
    }
    public detailModelChangeEvent(): Observable<UserTenantSessionDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
