/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositorySessionService } from './remote-repository-session.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositorySessionService]
    });
  });

  it('should ...', inject([RemoteRepositorySessionService], (service: RemoteRepositorySessionService) => {
    expect(service).toBeTruthy();
  }));
});
