/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryRoleService } from './remote-repository-role.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryRoleService]
    });
  });

  it('should ...', inject([RemoteRepositoryRoleService], (service: RemoteRepositoryRoleService) => {
    expect(service).toBeTruthy();
  }));
});
