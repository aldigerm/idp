import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { RoleBasicModel } from '@models/remote-repository/role/remote-repository-role-basic';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { RoleCreateModel } from '@models/remote-repository/role/remote-repository-role-create';
import { RoleUpdateModel } from '@models/remote-repository/role/remote-repository-role-update';
import { RoleDetailModel } from '@models/remote-repository/role/remote-repository-role-detail';
import { RoleSetInheritingRoles } from '@models/remote-repository/role/remote-repository-role-set-inheriting-roles';
import { RoleSetModules } from '@models/remote-repository/role/remote-repository-role-set-modules';
import { RoleSetUserGroups } from '@models/remote-repository/role/remote-repository-role-set-user-groups';
import { RoleListItemModel } from '@models/remote-repository/role/list/remote-repository-role-list';

@Injectable()
export class RemoteRepositoryRoleService {

    private detailChangeObservable = new EventEmitter<RoleIdentifier>();
    private detailModelChangeObservable = new EventEmitter<RoleDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('RoleService');
    }

    public getPageList(): Promise<Array<RoleListItemModel>> {
        return this.http.get<Array<RoleListItemModel>>(environment.Settings.apiKeys.idP.RoleGetPageList)
            .toPromise();
    }

    public getDetail(identifier: RoleIdentifier): Promise<RoleDetailModel> {
        return this.http.get<RoleDetailModel>(environment.Settings.apiKeys.idP.RoleGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: RoleCreateModel): Promise<RoleIdentifier> {

        return new Promise<RoleIdentifier>((resolve, reject) => {
            this.http.post<RoleIdentifier>(environment.Settings.apiKeys.idP.RoleCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: RoleUpdateModel): Promise<RoleIdentifier> {

        return new Promise<RoleIdentifier>((resolve, reject) => {
            this.http.post<RoleIdentifier>(environment.Settings.apiKeys.idP.RoleUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: RoleIdentifier): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.RoleDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setInheritingFromRoles(model: RoleSetInheritingRoles): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.RoleSetInheritingRoles, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setModules(model: RoleSetModules): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.RoleSetModules, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setUserGroups(model: RoleSetUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.RoleSetUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public triggerDetailChange(identfier: RoleIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: RoleIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<RoleIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<RoleDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
