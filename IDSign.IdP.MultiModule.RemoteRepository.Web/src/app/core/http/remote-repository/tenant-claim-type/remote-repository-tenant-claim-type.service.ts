import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';
import { TenantClaimTypeDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type';
import {
    TenantClaimTypeUpdateModel
} from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type-update';
import {
    TenantClaimTypeSetTenants
} from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type-set-tenants';
import {
    TenantClaimTypeCreateModel
} from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type-create';
import {
    TenantClaimTypeListItemModel
} from '@models/remote-repository/tenant/claims/tenant-claim-type/list/remote-repository-tenant-claim-list-item';


@Injectable()
export class RemoteRepositoryTenantClaimTypeService {

    private detailChangeObservable = new EventEmitter<TenantClaimTypeIdentifier>();
    private detailModelChangeObservable = new EventEmitter<TenantClaimTypeDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('TenantClaimTypeService');
    }


    public getPageList(): Promise<Array<TenantClaimTypeListItemModel>> {
        return this.http.get<Array<TenantClaimTypeListItemModel>>(environment.Settings.apiKeys.idP.TenantClaimTypeGetPageList)
            .toPromise();
    }

    public getDetail(identifier: TenantClaimTypeIdentifier): Promise<TenantClaimTypeDetailModel> {
        return this.http.get<TenantClaimTypeDetailModel>(environment.Settings.apiKeys.idP.TenantClaimTypeGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: TenantClaimTypeCreateModel): Promise<TenantClaimTypeIdentifier> {
        return new Promise<TenantClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<TenantClaimTypeIdentifier>(environment.Settings.apiKeys.idP.TenantClaimTypeCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: TenantClaimTypeUpdateModel): Promise<TenantClaimTypeIdentifier> {
        return new Promise<TenantClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<TenantClaimTypeIdentifier>(environment.Settings.apiKeys.idP.TenantClaimTypeUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: TenantClaimTypeIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.TenantClaimTypeDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public setTenantMembership(model: TenantClaimTypeSetTenants): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.TenantClaimTypeSetTenants, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: TenantClaimTypeIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: TenantClaimTypeIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }
    public detailChangeEvent(): Observable<TenantClaimTypeIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<TenantClaimTypeDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
