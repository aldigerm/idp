/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryProjectService } from './remote-repository-project.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryProjectService]
    });
  });

  it('should ...', inject([RemoteRepositoryProjectService], (service: RemoteRepositoryProjectService) => {
    expect(service).toBeTruthy();
  }));
});
