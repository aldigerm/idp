import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { ProjectDetailModel } from '@models/remote-repository/project/remote-repository-project-detail';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { ProjectListModel, ProjectListItemModel } from '@models/remote-repository/project/list/remote-repository-project-list';
import { ProjectUpdateModel } from '@models/remote-repository/project/remote-repository-project-update';
import { ProjectCreateResult, ProjectCreateModel } from '@models/remote-repository/project/remote-repository-project-create';
import { ProjectListPageModel } from '@models/remote-repository/project/list/remote-repository-project-page';
import { ProjectBasicModel } from '@models/remote-repository/project/remote-repository-project-basic';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';

@Injectable()
export class RemoteRepositoryProjectService {

    private detailChangeObservable = new EventEmitter<ProjectIdentifier>();
    private detailModelChangeObservable = new EventEmitter<ProjectDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('ProjectService');
    }

    public getPageList(): Promise<Array<ProjectListItemModel>> {
        return this.http.get<Array<ProjectListItemModel>>(environment.Settings.apiKeys.idP.ProjectGetPageList)
            .toPromise();
    }

    public getDetail(identifier: ProjectIdentifier): Promise<ProjectDetailModel> {
        return this.http.get<ProjectDetailModel>(environment.Settings.apiKeys.idP.ProjectGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: ProjectCreateModel): Promise<ProjectIdentifier> {
        return new Promise<ProjectIdentifier>((resolve, reject) => {
            this.http.post<ProjectIdentifier>(environment.Settings.apiKeys.idP.ProjectCreate, model)
                .toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: ProjectUpdateModel): Promise<ProjectIdentifier> {
        return new Promise<ProjectIdentifier>((resolve, reject) => {
            this.http.post<ProjectIdentifier>(environment.Settings.apiKeys.idP.ProjectUpdate, model)
                .toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: ProjectIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.ProjectDelete
                , { params: identifier as any })
                .toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: ProjectIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: ProjectIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<ProjectIdentifier> {
        return this.detailChangeObservable.asObservable();
    }
    public detailModelChangeEvent(): Observable<ProjectDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
