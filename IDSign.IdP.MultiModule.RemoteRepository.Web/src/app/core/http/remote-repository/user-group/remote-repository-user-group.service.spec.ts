/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserGroupService } from './remote-repository-user-group.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryUserGroupService]
    });
  });

  it('should ...', inject([RemoteRepositoryUserGroupService], (service: RemoteRepositoryUserGroupService) => {
    expect(service).toBeTruthy();
  }));
});
