import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserGroupBasicModel } from '@models/remote-repository/user-group/remote-repository-user-group-basic';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';
import { UserGroupCreateModel } from '@models/remote-repository/user-group/remote-repository-user-group-create';
import { UserGroupUpdateModel } from '@models/remote-repository/user-group/remote-repository-user-group-update';
import { UserGroupDetailModel } from '@models/remote-repository/user-group/remote-repository-user-group-detail';
import {
    UserGroupSetInheritingUserGroups
} from '@models/remote-repository/user-group/remote-repository-user-group-set-inheriting-user-groups';
import { UserGroupSetUsers } from '@models/remote-repository/user-group/remote-repository-user-group-set-users';
import { UserGroupListItemModel } from '@models/remote-repository/user-group/list/remote-repository-user-group-list';
import { UserGroupSetPasswordPolicies } from '@models/remote-repository/user-group/remote-repository-user-group-set-password-polcies';

@Injectable()
export class RemoteRepositoryUserGroupService {

    private detailChangeObservable = new EventEmitter<UserGroupIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserGroupDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserGroupService');
    }

    public getPageList(): Promise<Array<UserGroupListItemModel>> {
        return this.http.get<Array<UserGroupListItemModel>>(environment.Settings.apiKeys.idP.UserGroupGetPageList)
            .toPromise();
    }

    public getDetail(identifier: UserGroupIdentifier): Promise<UserGroupDetailModel> {
        return this.http.get<UserGroupDetailModel>(environment.Settings.apiKeys.idP.UserGroupGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: UserGroupCreateModel): Promise<UserGroupIdentifier> {
        return new Promise<UserGroupIdentifier>((resolve, reject) => {
            this.http.post<UserGroupIdentifier>(environment.Settings.apiKeys.idP.UserGroupCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserGroupUpdateModel): Promise<UserGroupIdentifier> {
        return new Promise<UserGroupIdentifier>((resolve, reject) => {
            this.http.post<UserGroupIdentifier>(environment.Settings.apiKeys.idP.UserGroupUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: UserGroupIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.UserGroupDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setInheritingFromUserGroups(model: UserGroupSetInheritingUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.UserGroupSetInheritingUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setUserMembership(model: UserGroupSetUsers): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.UserGroupSetUsers, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setPasswordPolicies(model: UserGroupSetPasswordPolicies): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.UserGroupSetPasswordPolicies, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: UserGroupIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserGroupIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<UserGroupIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserGroupDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
