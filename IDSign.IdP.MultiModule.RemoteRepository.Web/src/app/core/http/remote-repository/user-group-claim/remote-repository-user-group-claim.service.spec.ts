/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserGroupClaimService } from './remote-repository-user-group-claim.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [RemoteRepositoryUserGroupClaimService]
    });
  });

    it('should ...', inject([RemoteRepositoryUserGroupClaimService], (service: RemoteRepositoryUserGroupClaimService) => {
    expect(service).toBeTruthy();
  }));
});
