import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import {
    UserGroupClaimUpdateModel
} from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim-update';
import { UserGroupClaimDetailModel } from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim';
import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';
import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';
import {
    UserGroupClaimSetUserGroups
} from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim-set-user-groups';
import {
    UserGroupClaimCreateModel
} from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim-create';
import {
    UserGroupClaimTypeDetailModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type';
import {
    UserGroupClaimListItemModel
} from '@models/remote-repository/user-group/claims/user-group-claim/list/remote-repository-user-group-claim-list-item';

@Injectable()
export class RemoteRepositoryUserGroupClaimService {

    private detailChangeObservable = new EventEmitter<UserGroupClaimIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserGroupClaimDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserGroupClaimService');
    }


    public getPageList(): Promise<Array<UserGroupClaimListItemModel>> {
        return this.http.get<Array<UserGroupClaimListItemModel>>(environment.Settings.apiKeys.idP.UserGroupClaimGetPageList)
            .toPromise();
    }

    public getDetail(identifier: UserGroupClaimIdentifier): Promise<UserGroupClaimDetailModel> {
        return this.http.get<UserGroupClaimDetailModel>(environment.Settings.apiKeys.idP.UserGroupClaimGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: UserGroupClaimCreateModel): Promise<UserGroupClaimIdentifier> {
        return new Promise<UserGroupClaimIdentifier>((resolve, reject) => {
            return this.http.post<UserGroupClaimIdentifier>(environment.Settings.apiKeys.idP.UserGroupClaimCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserGroupClaimUpdateModel): Promise<UserGroupClaimIdentifier> {
        return new Promise<UserGroupClaimIdentifier>((resolve, reject) => {
            return this.http.post<UserGroupClaimIdentifier>(environment.Settings.apiKeys.idP.UserGroupClaimUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setUserGroupMembership(model: UserGroupClaimSetUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserGroupClaimSetUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: UserGroupClaimIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.UserGroupClaimDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: UserGroupClaimIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserGroupClaimIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<UserGroupClaimIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserGroupClaimDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
