/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserClaimService } from './remote-repository-user-claim.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [RemoteRepositoryUserClaimService]
    });
  });

    it('should ...', inject([RemoteRepositoryUserClaimService], (service: RemoteRepositoryUserClaimService) => {
    expect(service).toBeTruthy();
  }));
});
