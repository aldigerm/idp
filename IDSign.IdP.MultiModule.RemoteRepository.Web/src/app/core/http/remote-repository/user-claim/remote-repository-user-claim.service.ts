import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserClaimUpdateModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-update';
import { UserClaimDetailModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserClaimSetUsers } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-set-users';
import { UserClaimCreateModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-create';
import { UserClaimTypeDetailModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type';
import { UserClaimListItemModel } from '@models/remote-repository/user/claims/user-claim/list/remote-repository-user-claim-list-item';

@Injectable()
export class RemoteRepositoryUserClaimService {

    private detailChangeObservable = new EventEmitter<UserClaimIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserClaimDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserClaimService');
    }


    public getPageList(): Promise<Array<UserClaimListItemModel>> {
        return this.http.get<Array<UserClaimListItemModel>>(environment.Settings.apiKeys.idP.UserClaimGetPageList)
            .toPromise();
    }

    public getDetail(identifier: UserClaimIdentifier): Promise<UserClaimDetailModel> {
        return this.http.get<UserClaimDetailModel>(environment.Settings.apiKeys.idP.UserClaimGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: UserClaimCreateModel): Promise<UserClaimIdentifier> {
        return new Promise<UserClaimIdentifier>((resolve, reject) => {
            return this.http.post<UserClaimIdentifier>(environment.Settings.apiKeys.idP.UserClaimCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserClaimUpdateModel): Promise<UserClaimIdentifier> {
        return new Promise<UserClaimIdentifier>((resolve, reject) => {
            return this.http.post<UserClaimIdentifier>(environment.Settings.apiKeys.idP.UserClaimUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setUserMembership(model: UserClaimSetUsers): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserClaimSetUsers, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: UserClaimIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.UserClaimDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: UserClaimIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserClaimIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<UserClaimIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserClaimDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
