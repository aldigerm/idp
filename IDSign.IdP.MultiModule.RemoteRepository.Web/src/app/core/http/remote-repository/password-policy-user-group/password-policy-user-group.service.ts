import { Injectable, EventEmitter } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { PasswordPolicyUserGroupIdentifier } from '@models/identifiers/password-policy-user-group-identifier';
import {
  PasswordPolicyUserGroupDetailModel
} from '@models/remote-repository/password-policy/user-group/remote-repository-password-policy-user-group-detail';
import {
  PasswordPolicyUserGroupCreateModel
} from '@models/remote-repository/password-policy/user-group/remote-repository-password-policy-user-group-create';
import {
  PasswordPolicyUserGroupUpdateModel
} from '@models/remote-repository/password-policy/user-group/remote-repository-password-policy-user-group-update';
import { Observable } from 'rxjs';

@Injectable()
export class RemoteRepositoryPasswordPolicyUserGroupService {


  private detailChangeObservable = new EventEmitter<PasswordPolicyUserGroupIdentifier>();
  private detailModelChangeObservable = new EventEmitter<PasswordPolicyUserGroupDetailModel>();

  constructor(
    private _logger: LoggerService,
    private http: HttpClient) {
    this._logger = _logger.createInstance('PasswordPolicyUserGroupService');
  }

  public getDetail(identifier: PasswordPolicyUserGroupIdentifier): Promise<PasswordPolicyUserGroupDetailModel> {
    return this.http.get<PasswordPolicyUserGroupDetailModel>(environment.Settings.apiKeys.idP.PasswordPolicyUserGroupGetDetail
      , { params: identifier as any }).toPromise();
  }

  public create(model: PasswordPolicyUserGroupCreateModel): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      return this.http.post<any>(environment.Settings.apiKeys.idP.PasswordPolicyUserGroupCreate, model).toPromise()
        .then(identifier => {
          this.getDetailAndTriggerChange(identifier);
          resolve(identifier);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  public update(model: PasswordPolicyUserGroupUpdateModel): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      return this.http.post<any>(environment.Settings.apiKeys.idP.PasswordPolicyUserGroupUpdate, model).toPromise()
        .then(identifier => {
          this.getDetailAndTriggerChange(model.identifier);
          resolve(model.identifier);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  public delete(identifier: PasswordPolicyUserGroupIdentifier): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      return this.http.delete<any>(environment.Settings.apiKeys.idP.PasswordPolicyUserGroupDelete
        , { params: identifier as any }).toPromise()
        .then(ok => {
          this.triggerDetailChange(identifier);
          resolve(ok);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  public triggerDetailChange(identfier: PasswordPolicyUserGroupIdentifier): void {
    if (identfier && identfier != null) {
      this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
      this.detailChangeObservable.next(identfier);
    } else {
      this._logger.warn('Triggered detail change but identifier wasnt provided.');
    }
  }

  private getDetailAndTriggerChange(identifier: PasswordPolicyUserGroupIdentifier): void {
    this.getDetail(identifier)
      .then(model => {
        this.detailModelChangeObservable.next(model);
        this.triggerDetailChange(model.identifier);
      }).catch(error => {
        this._logger.error('Couldnt get detail after trigger', error);
      });
  }

  public detailChangeEvent(): Observable<PasswordPolicyUserGroupIdentifier> {
    return this.detailChangeObservable.asObservable();
  }

  public detailModelChangeEvent(): Observable<PasswordPolicyUserGroupDetailModel> {
    return this.detailModelChangeObservable.asObservable();
  }
}
