/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryPasswordPolicyUserGroupService } from './password-policy-user-group.service';

describe('Service: RemoteRepositoryPasswordPolicyUserGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryPasswordPolicyUserGroupService]
    });
  });

  it('should ...', inject([RemoteRepositoryPasswordPolicyUserGroupService], (service: RemoteRepositoryPasswordPolicyUserGroupService) => {
    expect(service).toBeTruthy();
  }));
});
