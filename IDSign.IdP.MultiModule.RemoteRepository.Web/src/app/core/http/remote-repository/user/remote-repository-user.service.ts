import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserListItemModel } from '@models/remote-repository/user/list/remote-repository-user-list';
import { UserUpdateModel } from '@models/remote-repository/user/remote-repository-user-update';
import { UserCreateModel } from '@models/remote-repository/user/remote-repository-user-create';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { UserSetUserGroups } from '@models/remote-repository/user/remote-repository-user-set-user-groups';
import { UserSetPasswordModel } from '@models/remote-repository/user/remote-repository-user-set-password';
import { UserSetUserTenants } from '@models/remote-repository/user/remote-repository-user-set-user-tenants';
import { UserSetSecuritySettingsViewModel } from '@models/remote-repository/user/remote-repository-user-set-security-settings';

@Injectable()
export class RemoteRepositoryUserService {

    private detailChangeObservable = new EventEmitter<UserIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserService');
    }

    public getPageList(tenantIdentifier: TenantIdentifier): Promise<Array<UserListItemModel>> {
        return this.http.get<Array<UserListItemModel>>(environment.Settings.apiKeys.idP.UserGetPageList
            + '?' + (tenantIdentifier.tenantCode ? 'tenantCode=' + tenantIdentifier.tenantCode : '')
            + '' + (tenantIdentifier.projectCode ? '&projectCode=' + tenantIdentifier.projectCode : ''))
            .toPromise();
    }

    public getLoggedInDetail(): Promise<UserDetailModel> {
        return this.http.get<UserDetailModel>(environment.Settings.apiKeys.idP.UserGetLoggedInDetail
            + '?getAccessDetail=' + true)
            .toPromise();
    }

    public getDetail(identifier: UserIdentifier): Promise<UserDetailModel> {
        if (!identifier.projectCode) {
            identifier.projectCode = '';
        }
        if (!identifier.tenantCode) {
            identifier.tenantCode = '';
        }
        if (!identifier.username) {
            identifier.username = '';
        }
        return this.http.get<UserDetailModel>(environment.Settings.apiKeys.idP.UserGetDetail
            , { params: identifier as any }).toPromise();
    }

    public delete(identifier: UserIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.UserDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public create(model: UserCreateModel): Promise<UserIdentifier> {
        return new Promise<UserIdentifier>((resolve, reject) => {
            return this.http.post<UserIdentifier>(environment.Settings.apiKeys.idP.UserCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserUpdateModel): Promise<UserIdentifier> {
        return new Promise<UserIdentifier>((resolve, reject) => {
            return this.http.post<UserIdentifier>(environment.Settings.apiKeys.idP.UserUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setUserGroups(model: UserSetUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserSetUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
    public setUserTenants(model: UserSetUserTenants): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserSetUserTenants, model).toPromise()
                .then(result => {
                    if (result && result.identifier && result.identifier.username) {
                        this.triggerDetailChange(result.identifier);
                    }
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
    public setManagedUserGroups(model: UserSetUserGroups): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserSetManagedUserGroups, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setPassword(model: UserSetPasswordModel): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserSetPassword, model).toPromise()
                .then(ok => {
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setSecuritySettings(model: UserSetSecuritySettingsViewModel): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.UserSetSecuritySettings, model).toPromise()
                .then(ok => {
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public getUserProfileImage(identifier: UserIdentifier): Promise<any> {

        // get users from api
        return this.http.get<any>(environment.Settings.apiKeys.idP.UserProfileImageGet, {
            params: identifier as any
        })
            .toPromise();
    }

    public triggerDetailChange(identfier: UserIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<UserIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
