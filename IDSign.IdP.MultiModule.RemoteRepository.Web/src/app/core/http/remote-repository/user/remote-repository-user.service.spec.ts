/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserService } from './remote-repository-user.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RemoteRepositoryUserService]
    });
  });

  it('should ...', inject([RemoteRepositoryUserService], (service: RemoteRepositoryUserService) => {
    expect(service).toBeTruthy();
  }));
});
