/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryTenantClaimService } from './remote-repository-tenant-claim.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [RemoteRepositoryTenantClaimService]
    });
  });

    it('should ...', inject([RemoteRepositoryTenantClaimService], (service: RemoteRepositoryTenantClaimService) => {
    expect(service).toBeTruthy();
  }));
});
