import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { TenantClaimUpdateModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim-update';
import { TenantClaimDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim';
import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';
import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import {
    TenantClaimSetTenants
} from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim-set-tenants';
import { TenantClaimCreateModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim-create';
import { TenantClaimTypeDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type';
import {
    TenantClaimListItemModel
} from '@models/remote-repository/tenant/claims/tenant-claim/list/remote-repository-tenant-claim-list-item';

@Injectable()
export class RemoteRepositoryTenantClaimService {

    private detailChangeObservable = new EventEmitter<TenantClaimIdentifier>();
    private detailModelChangeObservable = new EventEmitter<TenantClaimDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('TenantClaimService');
    }


    public getPageList(): Promise<Array<TenantClaimListItemModel>> {
        return this.http.get<Array<TenantClaimListItemModel>>(environment.Settings.apiKeys.idP.TenantClaimGetPageList)
            .toPromise();
    }

    public getDetail(identifier: TenantClaimIdentifier): Promise<TenantClaimDetailModel> {
        return this.http.get<TenantClaimDetailModel>(environment.Settings.apiKeys.idP.TenantClaimGetDetail
            , { params: identifier as any }).toPromise();
    }

    public create(model: TenantClaimCreateModel): Promise<TenantClaimIdentifier> {
        return new Promise<TenantClaimIdentifier>((resolve, reject) => {
            return this.http.post<TenantClaimIdentifier>(environment.Settings.apiKeys.idP.TenantClaimCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: TenantClaimUpdateModel): Promise<TenantClaimIdentifier> {
        return new Promise<TenantClaimIdentifier>((resolve, reject) => {
            return this.http.post<TenantClaimIdentifier>(environment.Settings.apiKeys.idP.TenantClaimUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public setTenantMembership(model: TenantClaimSetTenants): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.post<any>(environment.Settings.apiKeys.idP.TenantClaimSetTenants, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: TenantClaimIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this.http.delete<any>(environment.Settings.apiKeys.idP.TenantClaimDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: TenantClaimIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: TenantClaimIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }

    public detailChangeEvent(): Observable<TenantClaimIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<TenantClaimDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
