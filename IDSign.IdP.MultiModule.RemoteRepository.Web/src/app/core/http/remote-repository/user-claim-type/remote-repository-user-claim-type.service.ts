import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { UserClaimTypeDetailModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type';
import { UserClaimTypeUpdateModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-update';
import { UserClaimTypeSetUsers } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-set-users';
import { UserClaimTypeCreateModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-create';
import { UserClaimTypeListItemModel } from '@models/remote-repository/user/claims/user-claim-type/list/remote-repository-user-claim-list-item';


@Injectable()
export class RemoteRepositoryUserClaimTypeService {

    private detailChangeObservable = new EventEmitter<UserClaimTypeIdentifier>();
    private detailModelChangeObservable = new EventEmitter<UserClaimTypeDetailModel>();

    constructor(
        private _logger: LoggerService,
        private http: HttpClient) {
        this._logger = _logger.createInstance('UserClaimTypeService');
    }


    public getPageList(): Promise<Array<UserClaimTypeListItemModel>> {
        return this.http.get<Array<UserClaimTypeListItemModel>>(environment.Settings.apiKeys.idP.UserClaimTypeGetPageList)
            .toPromise();
    }

    public getDetail(identifier: UserClaimTypeIdentifier): Promise<UserClaimTypeDetailModel> {
        return this.http.get<UserClaimTypeDetailModel>(environment.Settings.apiKeys.idP.UserClaimTypeGetDetail
            , { params: identifier as any })
            .toPromise();
    }

    public create(model: UserClaimTypeCreateModel): Promise<UserClaimTypeIdentifier> {
        return new Promise<UserClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<UserClaimTypeIdentifier>(environment.Settings.apiKeys.idP.UserClaimTypeCreate, model).toPromise()
                .then(identifier => {
                    this.triggerDetailChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public update(model: UserClaimTypeUpdateModel): Promise<UserClaimTypeIdentifier> {
        return new Promise<UserClaimTypeIdentifier>((resolve, reject) => {
            this.http.post<UserClaimTypeIdentifier>(environment.Settings.apiKeys.idP.UserClaimTypeUpdate, model).toPromise()
                .then(identifier => {
                    this.getDetailAndTriggerChange(identifier);
                    resolve(identifier);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public delete(identifier: UserClaimTypeIdentifier): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.delete<any>(environment.Settings.apiKeys.idP.UserClaimTypeDelete
                , { params: identifier as any }).toPromise()
                .then(ok => {
                    this.triggerDetailChange(identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    public setUserMembership(model: UserClaimTypeSetUsers): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post<any>(environment.Settings.apiKeys.idP.UserClaimTypeSetUsers, model).toPromise()
                .then(ok => {
                    this.triggerDetailChange(model.identifier);
                    resolve(ok);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    public triggerDetailChange(identfier: UserClaimTypeIdentifier): void {
        if (identfier && identfier != null) {
            this._logger.debug('Triggered detail change for model ' + JSON.stringify(identfier));
            this.detailChangeObservable.next(identfier);
        } else {
            this._logger.warn('Triggered detail change but identifier wasnt provided.');
        }
    }

    private getDetailAndTriggerChange(identifier: UserClaimTypeIdentifier): void {
        this.getDetail(identifier)
            .then(model => {
                this.detailModelChangeObservable.next(model);
                this.triggerDetailChange(model.identifier);
            }).catch(error => {
                this._logger.error('Couldnt get detail after trigger', error);
            });
    }
    public detailChangeEvent(): Observable<UserClaimTypeIdentifier> {
        return this.detailChangeObservable.asObservable();
    }

    public detailModelChangeEvent(): Observable<UserClaimTypeDetailModel> {
        return this.detailModelChangeObservable.asObservable();
    }
}
