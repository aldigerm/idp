/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RemoteRepositoryUserClaimTypeService } from './remote-repository-user-claim-type.service';

describe('Service: notarization', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        providers: [RemoteRepositoryUserClaimTypeService]
    });
  });

    it('should ...', inject([RemoteRepositoryUserClaimTypeService], (service: RemoteRepositoryUserClaimTypeService) => {
    expect(service).toBeTruthy();
  }));
});
