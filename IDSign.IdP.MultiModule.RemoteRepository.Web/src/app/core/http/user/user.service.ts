import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { User, UserProfile } from '@models/user';
import { BrowserStorage } from '@services/storage/browser-storage';
import { UserCacheService } from '@services/storage/user-cache.service';

import { Observable } from 'rxjs';


@Injectable()
export class UserService {

  private userChangeObservable = new EventEmitter<User>();

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private userCacheService: UserCacheService,
    private logger: LoggerService,
    private browserStorage: BrowserStorage) {

    // create logging instance
    this.logger = this.logger.createInstance('UserService');
  }

  private getUserFromApi(): Observable<User> {

    // get users from api
    return this.http.get<User>(environment.Settings.apiKeys.idP.UserGet + '?getAccessDetail=' + true);
  }


  public isRecognised(username: string): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      this.getUser()
        .then(u => {
          if (u && u.userIdentifier && u.identifier.username === username) {
            this.logger.debug('User is recognised!');
            resolve(u);
          } else {
            this.logger.warn('User stored is not the same as provided.');
            resolve(null);
          }
        })
        .catch(e => {
          this.logger.debug('User is not in cache.');
          this.refreshUser()
            .then(user => {
              this.logger.debug('The refresh returned the user.');
              resolve(user);
            })
            .catch(error => {
              this.logger.debug('The refresh threw an error.');
              this.userCacheService.removeUser();
              resolve(null);
            });
        });
    });
  }

  public getUser(): Promise<User> {
    return this.userCacheService.getUser();
  }

  public userRefreshedEvent(): Observable<User> {
    return this.userChangeObservable;
  }

  public refreshUser(): Promise<User> {
    return new Promise<User>((resolve, reject) => {

      // if not logged in, then remove user and return blank

      this.authenticationService.isSignedIn()
        .then(isSigneIn => {
          if (!isSigneIn) {
            this.logger.debug('User is not even signed in! Not getting user profile');
            this.userCacheService.removeUser();
            reject();
          } else {
            const userProfile = new UserProfile();
            userProfile.sub = isSigneIn;
            this.getUserFromApi()
              .toPromise()
              .then(u => {
                this.browserStorage.remove(environment.Settings.browserStorageKeys.userLoadErrorKey);
                const user = u;
                user.userProfile = userProfile;
                this.logger.debug('User returned ok.');

                // save user to cache
                this.userCacheService.setUser(user);

                this.userChangeObservable.next(user);
                resolve(user);
              })
              .catch(e => {
                this.logger.error('error while getting user ' + userProfile.sub + ' . ' + JSON.stringify(e));
                this.browserStorage.set(environment.Settings.browserStorageKeys.userLoadErrorKey, 'true');
                this.userCacheService.removeUser();
                reject();
              });
          }
        }).catch(err => {
          // an error occured while getting details from OAuth
          // so we cannot return a user
          this.logger.error('Error while loading oauth discovery document. ' + JSON.stringify(err));
          this.browserStorage.set(environment.Settings.browserStorageKeys.userLoadErrorKey, 'true');
          this.userCacheService.removeUser();
          reject();
        });
    });
  }
}
