import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { environment } from '@environments/environment';
import { LoggingLevel } from '@models/logging/logging-level.enum';
import { ClientLoggerService } from './client-logger.service';
import { Logger } from './logger.service';

@Injectable()
export class ConsoleLoggerWithClientService implements Logger {

    source = 'ConsoleLoggerService';
    level = LoggingLevel.ERROR;
    private clientLoggerService: ClientLoggerService;

    private setSource(source: any): Logger {
        this.source = source;
        return this;
    }

    setClientLogger(clientLoggerService: ClientLoggerService): Logger {
        this.clientLoggerService = clientLoggerService;
        return this;
    }

    setLevel(level: LoggingLevel): Logger {
        environment.Settings.loggingLevel = level;
        console.log('Level set to ' + LoggingLevel[this.getLoggingLevel()]);
        return this;
    }

    createInstance(source: any): Logger {
        return new ConsoleLoggerWithClientService().setSource(source).setClientLogger(this.clientLoggerService);
    }

    trace(message: string, args?: any): void {
        if (!args) {
            args = '';
        }
        if (this.getLoggingLevel() >= LoggingLevel.TRACE) {
            console.log(this.source + ' => ' + message, args);
        }
        return;
    }

    debug(message: string, args?: any): void {
        if (!args) {
            args = '';
        }
        if (this.getLoggingLevel() >= LoggingLevel.DEBUG) {
            console.log(this.source + ' => ' + message, args);
        }
        return;
    }

    info(message: string, args?: any): void {
        if (!args) {
            args = '';
        }
        if (this.getLoggingLevel() >= LoggingLevel.INFO) {
            console.log(this.source + ' => ' + message, args);
        }
        return;
    }

    warn(message: string, args?: any): void {
        if (!args) {
            args = '';
        }
        if (this.getLoggingLevel() >= LoggingLevel.WARN) {
            console.warn(this.source + ' => ' + message, args);
        }
        return;
    }

    error(message: string, exception: any, args?: any): void {
        this.errorWait(message, exception, args).subscribe(result => {
            this.info('Client error log sent.');
        }, error => {
            console.error('Client log not sent :' + JSON.stringify(error));
        });
        return;
    }

    errorWait(message: string, exception: any, args?: any): Observable<any> {
        if (!args) {
            args = '';
        }
        if (exception) {
            if (exception.message) {
                message += '\nMessage:\n' + exception.message;
            }
            if (exception.stack) {
                message += '\nStack:\n' + exception.stack.toString();
            }
        }
        if (this.getLoggingLevel() >= LoggingLevel.ERROR) {
            console.error(this.source + ' => ' + message, args);
            return this.clientLoggerService.error(this.source, message);
        } else {
            return of('');
        }
    }

    fatal(message: string, args?: any): void {
        if (!args) {
            args = '';
        }
        if (this.getLoggingLevel() >= LoggingLevel.FATAL) {
            console.error(this.source + ' => ' + message, args);
        }
        return;
    }

    getLoggingLevel(): LoggingLevel {
        let level = LoggingLevel.WARN;
        if (environment && environment.Settings && environment.Settings.loggingLevel) {
            level = environment.Settings.loggingLevel;
        }
        return level;
    }
}
