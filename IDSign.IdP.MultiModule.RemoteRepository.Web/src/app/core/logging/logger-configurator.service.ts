import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { LoggingLevel } from '@models/logging/logging-level.enum';
import { LoggerService } from './logger.service';

@Injectable()
export class LoggerConfiguratorService {

    constructor(
        private logger: LoggerService
    ) {
        this.logger = this.logger.createInstance('LoggerConfiguratorService');
    }

    public setLevel(level: LoggingLevel) {
        this.logger.info('Logger level is ' +
            LoggingLevel[environment.Settings.loggingLevel]
            + ' and will be set to ' + LoggingLevel[level] + '.');
        environment.Settings.loggingLevel = level;
    }

}
