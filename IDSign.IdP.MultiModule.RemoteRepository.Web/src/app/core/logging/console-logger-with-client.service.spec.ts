/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ConsoleLoggerWithClientService } from "./console-logger-with-client.service";

describe('Service: ConsoleLogger', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsoleLoggerWithClientService]
    });
  });

  it('should ...', inject([ConsoleLoggerWithClientService], (service: ConsoleLoggerWithClientService) => {
    expect(service).toBeTruthy();
  }));
});
