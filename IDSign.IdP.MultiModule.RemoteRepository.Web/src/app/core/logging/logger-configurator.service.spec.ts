/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoggerConfiguratorService } from './logger-configurator.service';

describe('Service: LoggerConfigurator', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggerConfiguratorService]
    });
  });

  it('should ...', inject([LoggerConfiguratorService], (service: LoggerConfiguratorService) => {
    expect(service).toBeTruthy();
  }));
});