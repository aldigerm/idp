/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ClientLoggerService } from "./client-logger.service";

describe('Service: ClientLogger', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientLoggerService]
    });
  });

  it('should ...', inject([ClientLoggerService], (service: ClientLoggerService) => {
    expect(service).toBeTruthy();
  }));
});
