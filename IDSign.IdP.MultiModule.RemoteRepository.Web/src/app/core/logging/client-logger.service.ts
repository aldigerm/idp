import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable()
export class ClientLoggerService {


    http: HttpClient;

    constructor(
        private injector: Injector) {
    }

    public error(source: string, message: string): Observable<any> {
        this.http = this.injector.get(HttpClient);
        const body = {
            source: source,
            message: message
        };
        return this.http.post<any>(environment.Settings.apiKeys.idP.ClientLogError, body);
    }
}
