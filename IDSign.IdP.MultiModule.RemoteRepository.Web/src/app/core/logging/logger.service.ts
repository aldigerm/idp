import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoggingLevel } from '@models/logging/logging-level.enum';
import { ClientLoggerService } from './client-logger.service';
export abstract class Logger {


    setClientLogger(clientLoggerService: ClientLoggerService): Logger { return this; }
    createInstance(source: any): Logger { return this; }
    trace(message: string, args?: any): void { }
    fatal(message: string, args?: any): void { }
    debug(message: string, args?: any): void { }
    info(message: string, args?: any): void { }
    warn(message: string, args?: any): void { }
    error(message: string, error?: any, args?: any): void { }
    errorWait(message: string, error?: any, args?: any): Observable<any> { return undefined; }
    setLevel(level: LoggingLevel): Logger { return this; }
}

@Injectable()
export class LoggerService implements Logger {
    setClientLogger(clientLoggerService: ClientLoggerService): Logger { return this; }
    createInstance(source: any): LoggerService { return this; }
    trace(message: string, args?: any): void { }
    debug(message: string, args?: any): void { }
    info(message: string, args?: any): void { }
    warn(message: string, args?: any): void { }
    error(message: string, error?: any, args?: any): void { }
    errorWait(message: string, error?: any, args?: any): Observable<any> { return undefined; }
    fatal(message: string, args?: any): void { }
    setLevel(level: LoggingLevel): Logger { return this; }
}
