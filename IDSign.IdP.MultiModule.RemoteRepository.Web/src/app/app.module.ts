import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { IdpCallbackModule } from './modules/idp-callback/idp-callback.module';
import { SiteThemeModule } from '@app/shared/components/site-theme/site-theme.module';
import { LoggerLevelConfiguratorModule } from '@app/shared/components/logger-level-configurator/logger-level-configurator.module';
import { PagePreloaderModule } from '@app/shared/components/page-preloader/page-preloader.module';
import { SweetAlertModule } from '@app/shared/components/sweet-alert/sweet-alert.module';
import { WindowListenerComponent } from '@app/shared/components/window-listener/window-listener.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as $ from 'jquery';
import { OAuthModule } from 'angular-oauth2-oidc';
import { UserProfileModule } from './modules/user-profile/user-profile.module';

@NgModule({
  declarations: [
    AppComponent, WindowListenerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    CoreModule,
    SweetAlertModule,
    LoggerLevelConfiguratorModule,
    PagePreloaderModule,
    SweetAlertModule,
    SiteThemeModule,
    IdpCallbackModule,
    UserProfileModule,
    BrowserAnimationsModule,
    OAuthModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
