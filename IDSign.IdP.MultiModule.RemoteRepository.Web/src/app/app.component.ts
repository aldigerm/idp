import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SiteTranslateService } from '@services/site-translate.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { environment } from '@environments/environment';
import { AuthenticationService } from './core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderComponent } from '@app/shared/components/page-preloader/page-preloader.component';
import { SweetAlertComponent } from '@app/shared/components/sweet-alert/sweet-alert.component';
import { FeatureControlService } from '@app/core/services/feature-control.service';
import { Router, NavigationEnd } from '@angular/router';
import { SiteCookieService } from '@services/storage/site-cookie.service';
import { filter } from 'rxjs/operators';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { RemoteRepositoryTenantService } from '@http/remote-repository/tenant/remote-repository-tenant.service';
import { RemoteRepositoryProjectService } from '@http/remote-repository/project/remote-repository-project.service';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { RemoteRepositoryRoleService } from '@http/remote-repository/role/remote-repository-role.service';
import { RemoteRepositoryModuleService } from '@http/remote-repository/module/remote-repository-module.service';
import { RemoteRepositoryUserClaimTypeService } from '@http/remote-repository/user-claim-type/remote-repository-user-claim-type.service';
import { RemoteRepositoryUserClaimService } from '@http/remote-repository/user-claim/remote-repository-user-claim.service';
import { UserService } from '@http/user/user.service';
import {
  RemoteRepositoryUserGroupClaimTypeService
} from '@http/remote-repository/user-group-claim-type/remote-repository-user-group-claim-type.service';
import {
  RemoteRepositoryRoleClaimTypeService
} from '@http/remote-repository/role-claim-type/remote-repository-role-claim-type.service';
import {
  RemoteRepositoryTenantClaimTypeService
} from '@http/remote-repository/tenant-claim-type/remote-repository-tenant-claim-type.service';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'app';
  preloaderShown = true;
  private preloaderMessageSubscribe: ISubscription;
  private preloaderStateeSubscribe: ISubscription;
  private navigationCookieSubscribe: ISubscription;
  private _remoteRepositoryTenantServiceSubscribe: ISubscription;
  private _remoteRepositoryProjectServiceSubscribe: ISubscription;
  private _remoteRepositoryUserGroupServiceSubscribe: ISubscription;
  private _remoteRepositoryRoleServiceSubscribe: ISubscription;
  private _remoteRepositoryModuleServiceSubscribe: ISubscription;
  private _remoteRepositoryUserClaimTypeServiceSubscribe: ISubscription;
  private _remoteRepositoryUserGroupClaimTypeServiceSubscribe: ISubscription;
  private _remoteRepositoryRoleClaimTypeServiceSubscribe: ISubscription;
  private _remoteRepositoryTenantClaimTypeServiceSubscribe: ISubscription;
  private _remoteRepositoryPasswordPolicyServiceSubscribe: ISubscription;
  @ViewChild(SweetAlertComponent, { static: true }) sweetAlertComponent: SweetAlertComponent;
  @ViewChild(PagePreloaderComponent, { static: true }) pagePreloaderComponent: PagePreloaderComponent;

  readonly idpErrorKey = 'idpError';

  constructor(
    private translateService: SiteTranslateService
    , private _logger: LoggerService
    , private pagePreloaderService: PagePreloaderService
    , private authenticationService: AuthenticationService
    , private router: Router
    , private featureControl: FeatureControlService
    , private cookieService: SiteCookieService
    , private _userService: UserService
    , private _remoteRepositoryUserService: RemoteRepositoryUserService
    , private _remoteRepositoryTenantService: RemoteRepositoryTenantService
    , private _remoteRepositoryProjectService: RemoteRepositoryProjectService
    , private _remoteRepositoryUserGroupService: RemoteRepositoryUserGroupService
    , private _remoteRepositoryRoleService: RemoteRepositoryRoleService
    , private _remoteRepositoryModuleService: RemoteRepositoryModuleService
    , private _remoteRepositoryUserClaimTypeService: RemoteRepositoryUserClaimTypeService
    , private _remoteRepositoryUserGroupClaimTypeService: RemoteRepositoryUserGroupClaimTypeService
    , private _remoteRepositoryRoleClaimTypeService: RemoteRepositoryRoleClaimTypeService
    , private _remoteRepositoryTenantClaimTypeService: RemoteRepositoryTenantClaimTypeService
    , private _remoteRepositoryPasswordPolicyService: RemoteRepositoryPasswordPolicyService

  ) {

    // create logging instance
    this._logger = this._logger.createInstance('AppComponent');

    this.translateService.initialize();
  }

  ngOnInit() {

    this._remoteRepositoryUserService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryTenantServiceSubscribe = this._remoteRepositoryTenantService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryProjectServiceSubscribe = this._remoteRepositoryProjectService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryUserGroupServiceSubscribe = this._remoteRepositoryUserGroupService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryRoleServiceSubscribe = this._remoteRepositoryRoleService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryModuleServiceSubscribe = this._remoteRepositoryModuleService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryUserClaimTypeServiceSubscribe = this._remoteRepositoryUserClaimTypeService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryUserGroupClaimTypeServiceSubscribe
      = this._remoteRepositoryUserGroupClaimTypeService.detailChangeEvent().subscribe(
        identifier => {
          this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
            .catch(error => { this._logger.warn('Couldnt refresh user'); });
        });

    this._remoteRepositoryRoleClaimTypeServiceSubscribe = this._remoteRepositoryRoleClaimTypeService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryTenantClaimTypeServiceSubscribe = this._remoteRepositoryTenantClaimTypeService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });

    this._remoteRepositoryPasswordPolicyServiceSubscribe = this._remoteRepositoryPasswordPolicyService.detailChangeEvent().subscribe(
      identifier => {
        this._userService.refreshUser().then(() => { this._logger.debug('User refreshed.'); })
          .catch(error => { this._logger.warn('Couldnt refresh user'); });
      });
  }

  ngAfterViewInit() {
    const parent = this;
    this.preloaderStateeSubscribe = this.pagePreloaderService.state().subscribe(state => {
      this._logger.debug('New preloader state : ' + state);
      parent.pagePreloaderComponent.setLoaderState(state);
    });

    this.preloaderMessageSubscribe = this.pagePreloaderService.message().subscribe(message => {
      this._logger.debug('New preloader message : ' + message);
      this.pagePreloaderComponent.setLoaderMessage(message);
    });

    this._logger.debug('Environemnt settings are :' + JSON.stringify(environment));
  }

  ngOnDestroy() {
    if (this.preloaderMessageSubscribe) {
      this.preloaderMessageSubscribe.unsubscribe();
    }
    if (this.preloaderStateeSubscribe) {
      this.preloaderStateeSubscribe.unsubscribe();
    }
    if (this.navigationCookieSubscribe) {
      this.navigationCookieSubscribe.unsubscribe();
    }

    if (this._remoteRepositoryTenantServiceSubscribe) {
      this._remoteRepositoryTenantServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryProjectServiceSubscribe) {
      this._remoteRepositoryProjectServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryUserGroupServiceSubscribe) {
      this._remoteRepositoryUserGroupServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryRoleServiceSubscribe) {
      this._remoteRepositoryRoleServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryModuleServiceSubscribe) {
      this._remoteRepositoryModuleServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryUserClaimTypeServiceSubscribe) {
      this._remoteRepositoryUserClaimTypeServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryUserGroupClaimTypeServiceSubscribe) {
      this._remoteRepositoryUserGroupClaimTypeServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryRoleClaimTypeServiceSubscribe) {
      this._remoteRepositoryRoleClaimTypeServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryTenantClaimTypeServiceSubscribe) {
      this._remoteRepositoryTenantClaimTypeServiceSubscribe.unsubscribe();
    }
    if (this._remoteRepositoryPasswordPolicyServiceSubscribe) {
      this._remoteRepositoryPasswordPolicyServiceSubscribe.unsubscribe();
    }
  }
}
