import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { BrowserStorage } from '@services/storage/browser-storage';

@Component({
  selector: 'app-login',
  templateUrl: './idp-callback.component.html',
  styleUrls: ['./idp-callback.component.css']
})
export class IdpCallbackComponent implements OnInit, AfterViewInit {

  constructor(
    private router: Router,
    private storage: BrowserStorage,
    private logger: LoggerService,
    private pagePreloaderService: PagePreloaderService) {
    this.logger = this.logger.createInstance('IdpCallbackComponent');
  }

  ngOnInit() {
    this.logger.debug('Initialised');
  }

  ngAfterViewInit() {
    // this is a callback from the idp
    this.logger.debug('View initialized. Setting is callback and redirecting.');
    this.storage.set(environment.Settings.browserStorageKeys.isIdpCallBack, 'true');
    this.pagePreloaderService.setLocalisedMessage('PagePreloader.ProcessingCallback');
    this.router.navigate(['/login'], { queryParamsHandling: 'merge', preserveFragment: true });
  }
}
