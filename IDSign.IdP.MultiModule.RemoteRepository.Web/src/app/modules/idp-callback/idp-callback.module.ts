import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IdpCallbackComponent } from './pages/idp-callback.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    IdpCallbackComponent
  ]
})
export class IdpCallbackModule { }
