import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './pages/user-profile.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UserProfileComponent]
})
export class UserProfileModule { }
