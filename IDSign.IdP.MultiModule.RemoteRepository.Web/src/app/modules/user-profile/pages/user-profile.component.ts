import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserStorage } from '@services/storage/browser-storage';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, AfterViewInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private storage: BrowserStorage,
    private logger: LoggerService,
    private pagePreloaderService: PagePreloaderService) {
    this.logger = this.logger.createInstance('UserProfileComponent');
  }

  ngOnInit() {
    this.logger.debug('Initialised');
  }

  ngAfterViewInit() {
    // this is a callback from the idp
    this.logger.debug('Requested to view profile.');

    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');

    this.storage.set(environment.Settings.browserStorageKeys.returnUrl, returnUrl);
    this.pagePreloaderService.setLocalisedMessage('PagePreloader.RedirectingToProfile');
    this.router.navigate(['/users/detail']);
  }
}
