import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionComponent } from './session.component';
import { Routes, RouterModule } from '@angular/router';
import { ForcedNewPasswordComponent } from './components/forced-new-password/forced-new-password.component';
import { SpinnerModule } from '@app/sharedcomponents/spinner/spinner.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  PasswordPolicyValidationResultModule
} from '@app/sharedcontrols/password/password-policy-validation-result/password-policy-validation-result.module';
import { PasswordControl } from '@app/sharedcontrols/password/password.module';
import { ShowErrorsModule } from '@app/sharedcomponents/show-errors/show-errors.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SessionNavigationComponent } from './header/navigation.component';

const routes: Routes = [
  { path: '', component: SessionComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SpinnerModule,
    TranslateModule,
    PasswordPolicyValidationResultModule,
    FormsModule,
    PasswordControl,
    ShowErrorsModule,
    FontAwesomeModule,
    ReactiveFormsModule
  ],
  declarations: [
    SessionComponent,
    SessionNavigationComponent,
    ForcedNewPasswordComponent]
})
export class SessionModule { }
