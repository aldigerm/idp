import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserSetPasswordModel } from '@models/remote-repository/user/remote-repository-user-set-password';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorCode } from '@models/error-code';
import { IsJsonString } from '@app/sharedfunctions/utils.function';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { RemoteRepositorySessionService } from '@http/remote-repository/session/remote-repository-session.service';
import { UserTenantSessionActionRequestModel } from '@models/remote-repository/session/action/remote-repository-session-action-request';
import { UserTenantSessionDetailModel } from '@models/remote-repository/session/remote-repository-session-detail';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import {
  UserTenantSessionActionPasswordRequestModel
} from '@models/remote-repository/session/action/data/remote-repository-session-action-password-request';

@Component({
  selector: 'app-forced-new-password',
  templateUrl: './forced-new-password.component.html',
  styleUrls: ['./forced-new-password.component.css']
})
export class ForcedNewPasswordComponent implements OnInit {

  faBolt = faBolt;
  private _detailModel: UserTenantSessionDetailModel;
  formPassword: FormGroup;
  passwordPolicies: PasswordPolicyDetailModel[];
  passwordPoliciesResult: PasswordPolicyDetailModel[];
  @Input() set detailModel(value: UserTenantSessionDetailModel) {

    this._detailModel = value;
    if (value.data && IsJsonString(value.data)) {
      this.passwordPolicies = JSON.parse(value.data) as PasswordPolicyDetailModel[];
    } else {
      this.passwordPolicies = null;
    }
  }

  @Output() cancel = new EventEmitter<boolean>();
  @Output() success = new EventEmitter<boolean>();

  get detailModel(): UserTenantSessionDetailModel {
    return this._detailModel;
  }

  constructor(
    private _logger: LoggerService,
    private _sweetAlert: SweetAlertService,
    private _sessionService: RemoteRepositorySessionService,
    private _fb: FormBuilder
  ) {
    this._logger = this._logger.createInstance('ForcedNewPasswordComponent');
    this.initForm();
  }

  initForm() {
    if (this.formPassword) {
      this.formPassword.get('password').setValue('');
    } else {
      this.formPassword = this._fb.group({
        password: this._fb.control('', [Validators.required, noWhitespaceValidator])
      });
    }
  }
  cancelUpdate() {
    this.cancel.emit(true);
  }
  updatePassword() {

    const current = this;
    if (!this.formPassword.valid) {
      validateAllFormFields(this.formPassword);
    } else {
      const model = <UserTenantSessionActionRequestModel>{};
      const form = this.formPassword.value as UserTenantSessionActionPasswordRequestModel;
      model.identifier = this.detailModel.identifier;
      model.data = JSON.stringify(form);
      this._logger.debug('To update session password  : ' + JSON.stringify(model));

      const prefix = 'Session.ForceNewPassword.Actions.UpdatePassword.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._sessionService.action(model)
        .then(response => {
          this.passwordPoliciesResult = null;
          // reset password
          this.formPassword.get('password').setValue('');
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => {
                this.success.emit(true);
                current._sweetAlert.close();
              }
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user password', error);
          if (error && error.error && error.error.ErrorCode === ErrorCode[ErrorCode.USER_PASSWORD_FAILED_VALIDATION]) {

            if (error.error.ExceptionData) {
              const data = error.error.ExceptionData;
              if (IsJsonString(data)) {
                this.passwordPoliciesResult = JSON.parse(data) as PasswordPolicyDetailModel[];
              }
            }
          }
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));

        });
    }
  }
  ngOnInit() {
    this.initForm();
  }

}
