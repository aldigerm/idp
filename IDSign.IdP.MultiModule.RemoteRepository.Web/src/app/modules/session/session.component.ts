import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RemoteRepositorySessionService } from '@http/remote-repository/session/remote-repository-session.service';
import { UserTenantSessionDetailModel } from '@models/remote-repository/session/remote-repository-session-detail';
import { SlideBoolAnimation } from '@app/sharedanimations/slide-bool-animation';
import { RemoteRepositoryUserTenantSessionType } from '@models/remote-repository/session/remote-repository-user-tenant-session-type.enum';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css'],
  animations: [SlideBoolAnimation]
})
export class SessionComponent implements OnInit {

  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  newPasswordType = RemoteRepositoryUserTenantSessionType.ForceNewPassword;
  detailModel: UserTenantSessionDetailModel;

  sessionType: RemoteRepositoryUserTenantSessionType;
  redirectUri: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _sessionService: RemoteRepositorySessionService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService
  ) { }

  hideAll() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  openDetail(identifier: string) {
    this._sessionService.getDetail(identifier)
      .then(detail => {
        this.detailModel = detail;
        this.sessionType = RemoteRepositoryUserTenantSessionType[detail.userTenantSessionTypeCode];
        this.displayState = this.displayStateContent;
        this.hideAll();
      })
      .catch((error => {
        this.displayState = this.displayStateError;
        this.hideAll();
      }));
  }

  sendToRedirectUri(success?: boolean) {
    window.location.href = this.redirectUri;
  }

  ngOnInit() {
    const identifier = this._activatedRoute.snapshot.paramMap.get('sid');
    this.redirectUri = this._activatedRoute.snapshot.queryParamMap.get('redirectUri');
    if (identifier) {
      this.openDetail(identifier);
    } else {
      this.displayState = this.displayStateError;
      this.hideAll();
    }
  }

}
