import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { faPowerOff } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { User } from '@models/user';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { SiteTranslateService } from '@services/site-translate.service';
import { SiteCookieService } from '@services/storage/site-cookie.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { SubscriptionLike as ISubscription } from 'rxjs';


@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css'],
    animations: [SlideBoolAnimation]
})
export class SessionNavigationComponent implements OnInit, AfterViewInit, OnDestroy {
    name: string;
    showHide: boolean;
    user: User;
    username: string;
    currentFlag = 'gb';
    langFlags = new Map<string, string>();
    faPowerOff = faPowerOff;
    @Output() changeLanguage = new EventEmitter<string>();
    private _windowSubscriber: ISubscription;

    hasNewNotifications: boolean;

    constructor(private _authenticationService: AuthenticationService,
        private _router: Router
        , private cookie: SiteCookieService
        , private translateService: SiteTranslateService
        , private logger: LoggerService
        , private _changeDetectionRef: ChangeDetectorRef
        , private _siteChangeDetector: SiteChangeDetectorService
        , private _windowListener: WindowEventListenerService
    ) {

        this.showHide = true;

        this.langFlags.set('en', 'gb');
        this.langFlags.set('it', 'it');

    }


    changeShowStatus() {
        this.setShowHide(!this.showHide);
    }

    setLanguage(lang: string) {
        this.changeLanguage.emit(lang);
        this.translateService.setLanguage(lang);
        this.setFlag(lang);
    }

    setFlag(lang: string) {
        if (lang) {
            this.currentFlag = this.langFlags.get(lang);
        }
        if (!this.currentFlag) {
            this.currentFlag = 'gb';
        }
    }

    private setUser(user: User) {
        this.user = user;
        this.name = (user.firstName ? user.firstName + ' ' : '') + (user.lastName ? user.lastName : '');
        this.username = user.userProfile.sub;
    }

    ngOnInit() {

        // read from cookie
        let currentLanguage = this.cookie.get('lang');

        // if not present, read from service
        if (!currentLanguage) {
            currentLanguage = this.translateService.getLanguage();
        }
        this.setFlag(currentLanguage);
        this.logger.debug('current flag is ' + this.currentFlag);
    }

    ngAfterViewInit() {
        $(document).on('click', '.mega-dropdown', function (e) {
            e.stopPropagation();
        });

        $('.search-box a, .search-box .app-search .srh-btn').on('click', function () {
            $('.app-search').toggle(200);
        });
        this.toggleMenu();
        this._windowSubscriber = this._windowListener.OnResize().subscribe(() => {
            this.toggleMenu();
        });
    }

    ngOnDestroy() {
        if (this._windowSubscriber) {
            this._windowSubscriber.unsubscribe();
        }
    }

    toggleMenu() {
        const width = (window.innerWidth > 0) ? window.innerWidth : window.screen.width;
        if (width < 1170) {
            $('body').addClass('mini-sidebar');
            $('.navbar-brand span').hide();
            $('.sidebartoggler i').addClass('ti-menu');
            this.setShowHide(false);
        } else {
            $('body').removeClass('mini-sidebar');
            $('.navbar-brand span').show();
            this.setShowHide(true);
        }
    }

    setShowHide(value: boolean) {
        this.showHide = value;
        this._windowListener.callNewEvent('idonboard-resized-menu');
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    }

    logout(): boolean {
        this._authenticationService.signout();
        return false;
    }
}
