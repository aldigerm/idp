import { Component, OnInit } from '@angular/core';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['not-found.component.css']
})
export class NotFoundComponent implements OnInit {

    constructor(
        private pagePreloaderService: PagePreloaderService
    ) {
    }

    ngOnInit() {
        this.pagePreloaderService.hide();
    }
}
