import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@environments/environment';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { GlobalErrorSummary } from '@models/global-error-summary';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faArrowCircleRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  model = new GlobalErrorSummary();
  showDetails = false;
  faTimesCircle = faTimesCircle;
  faArrowCircleRight = faArrowCircleRight;
  faHome = faHome;
  constructor(
    private _browserStorage: BrowserStorage,
    private _pagePreloaderService: PagePreloaderService,
    private _sectonPreloaderService: SectionPreloaderService,
    private _router: Router
  ) { }

  ngOnInit() {
    const error = this._browserStorage.getObject(environment.Settings.browserStorageKeys.lastGlobalError);
    if (error && error != null) {
      this.model = error as GlobalErrorSummary;
    } else {
      this.model = new GlobalErrorSummary();
    }
    this._pagePreloaderService.hide();
    this._sectonPreloaderService.hide();
  }

  clearError() {
    this._browserStorage.remove(environment.Settings.browserStorageKeys.lastGlobalError);
  }

  goToHomePage() {
    this.clearError();
    this._router.navigate(['/login']);
  }

  returnToPreviousPage() {
    if (this.model.url) {
      this.clearError();
      window.location.href = this.model.url;
    } else {
      this.goToHomePage();
    }
  }
  toggleDetails() {
    this.showDetails = !this.showDetails;
  }
  copyToClipBoard() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    const messageValue = (this.model.error ? this.model.error.message : '');
    selBox.value = JSON.stringify(
      {
        url: this.model.url,
        connectionId: this.model.connectionId,
        message: messageValue,
        time: this.model.time
      }
    );
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
