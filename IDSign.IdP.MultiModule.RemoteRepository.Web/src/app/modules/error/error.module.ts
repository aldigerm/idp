import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './pages/error.component';

const routes: Routes = [
  { path: '', component: ErrorComponent }
];

@NgModule({
  imports:
    [
      RouterModule.forChild(routes),
      CommonModule,
      TranslateModule,
      FontAwesomeModule
    ],
  declarations: [ErrorComponent]
})
export class ErrorModule { }
