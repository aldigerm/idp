import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NavigationComponent } from '@app/core/header/navigation.component';
import { BreadcrumbComponent } from '@app/shared/components/breadcrumb/breadcrumb.component';
import { RightSidebarComponent } from '@app/shared/components/right-sidebar/rightsidebar.component';
import { SectionPreloaderModule } from '@app/shared/components/section-preloader/section-preloader.module';
import { SidebarMobileModule } from '@app/shared/components/sidebar-mobile/sidebar-mobile.module';
import { SidebarModule } from '@app/shared/components/sidebar/sidebar.module';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { NAV_DROPDOWN_DIRECTIVES } from '@app/shared/nav-dropdown.directive';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { SIDEBAR_TOGGLE_DIRECTIVES } from '@app/shared/sidebar.directive';
import { PagesRoutingModule } from './pages-routing.module';
import { PageComponent } from './pages.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { ProfilePictureModule } from '@app/sharedcomponents/profile-picture/profile-picture.module';
import { ReturnToMainSiteButtonModule } from '@app/sharedcontrols/buttons/return-to-main-site-button/return-to-main-site-button.module';

@NgModule({
    imports: [
        CommonModule,
        PagesRoutingModule,
        SidebarModule,
        SidebarMobileModule,
        SpinnerModule,
        DateTimeFormatPipeModule,
        SectionPreloaderModule,
        FontAwesomeModule,
        NgScrollbarModule,
        TranslateModule,
        ProfilePictureModule,
        ReturnToMainSiteButtonModule
    ],
    declarations: [
        PageComponent,
        NavigationComponent,
        BreadcrumbComponent,
        RightSidebarComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        NAV_DROPDOWN_DIRECTIVES
    ]
})
export class PagesModule { }
