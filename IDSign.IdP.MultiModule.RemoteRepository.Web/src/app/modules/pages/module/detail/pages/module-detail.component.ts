import { MenuService } from '@services/menu.service';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { LocationService } from '@services/location.service';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { faTimesCircle, faTrash } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { ModuleDataComponent } from '../components/data/module-data.component';
import { UserService } from '@http/user/user.service';
import { RemoteRepositoryModuleService } from '@app/core/http/remote-repository/module/remote-repository-module.service';
import { ModuleIdentifier } from '@models/identifiers/module-identifier';
import { ModuleDetailModel } from '@models/remote-repository/module/remote-repository-module-detail';
import { filter } from 'rxjs/operators';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
declare const $: any;

@Component({
  selector: 'app-module-detail',
  templateUrl: './module-detail.component.html',
  styleUrls: ['./module-detail.component.css'],
  animations: [SlideBoolAnimation]
})
export class ModuleDetailComponent implements OnInit, OnDestroy {
  faTimesCircle = faTimesCircle;
  faSync = faSync;
  faTrash = faTrash;
  toggle = false;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateError = 'Error';

  readonly linkdata = 'data';
  readonly linkDefault = this.linkdata;

  displayState = this.displayStateLoader;

  private _detailSubscribe: ISubscription;
  private _detailChangeSubscribe: ISubscription;
  private _windowSubscribe: ISubscription;
  private _locationSubscribe: ISubscription;
  private _routerSubscribe: ISubscription;
  private _deleteSubscribe: ISubscription;
  private _isDetailOfCurrentUser = false;
  private readonly _prefixDelete = 'Module.Detail.Actions.Delete.Popup';

  model: ModuleDetailModel;
  headerWidth = '400px';
  private _id = <ModuleIdentifier>{};
  @Output() closeComponent = new EventEmitter<any>();
  @ViewChild('detailCard', { static: true }) detailCard: ElementRef;
  @ViewChild(ModuleDataComponent, { static: false }) dataComponent: ModuleDataComponent;
  @ViewChild(MatTabGroup, { static: true }) tabGroup: MatTabGroup;
  constructor(
    private _remoteModuleService: RemoteRepositoryModuleService,
    private _userService: UserService,
    private _logger: LoggerService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _location: LocationService,
    private _windowListener: WindowEventListenerService,
    private _scroller: ScrollService,
    private _menuService: MenuService,
    private _sweetAlert: SweetAlertService
  ) {
    this._logger = this._logger.createInstance('userDetailComponent');
    this.displayState = this.displayStateLoader;
    this.model = <ModuleDetailModel>{};

    this._routerSubscribe = this._router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (event.url.startsWith('/modules/')) {
          this._logger.debug('Router navigation for user group detail detected.');
          this.init();
        }
      });
  }

  refresh() {
    this.retry();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }


  private init() {
    const projectCode = this._activatedRoute.snapshot.paramMap.get('projectCode');
    const moduleCode = this._activatedRoute.snapshot.paramMap.get('moduleCode');
    if (projectCode && moduleCode) {
      this._logger.debug('query params have "' + projectCode + '" "' + moduleCode + '"');
      const id = new ModuleIdentifier(moduleCode, projectCode);
      this.getDetail(id);
    } else {
      this.displayState = this.displayStateError;
      this._pagePreloader.hide();
      this._sectionPreloader.hide();
    }
  }

  private getDetail(moduleIdentifier: ModuleIdentifier) {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
    this.scrollTop();
    this._id = moduleIdentifier;
    if (!(moduleIdentifier && moduleIdentifier.moduleCode && moduleIdentifier.projectCode)) {
      this.displayState = this.displayStateError;
    } else {

      this.displayState = this.displayStateLoader;
      this._remoteModuleService.getDetail(moduleIdentifier)
        .then(detail => {

          this.setDetail(detail);
          const tab = this._activatedRoute.snapshot.paramMap.get('tab');
          if (tab) {
            this._logger.debug('Tab is ' + tab);
            this.showTab(tab);
          } else {
            this.showTab(this.linkDefault);
          }
          this._logger.debug('Detail retrieved successfully');
          this.displayState = this.displayStateContent;
          this.setDimensionsListener();
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        }).catch(error => {
          this._logger.error('Error while getting detail ', error);
          this.displayState = this.displayStateError;
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        });
    }
  }

  private setDetail(detail: ModuleDetailModel) {
    if (detail) {
      this.model = detail;
      this._id = detail.identifier;
      this._menuService.setMenuModuleDetail(detail);
      this.setModelInComponents(detail);
    }
  }

  /*

  Buttons

  */

  onTabChanged(event: MatTabChangeEvent) {
    if (event.index === 0) {
      this.openDataComponent();
    }
  }

  openDataComponent() {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(this.model);
      this.dataComponent.open();
    }
  }

  goToNextAvailableTab() {
    this.showTab(this.linkDefault);
  }

  setModelInComponents(model: ModuleDetailModel): void {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(model);
      this.dataComponent.open();
    }
  }

  updateLocation(tab: string) {
    // const current = this._location.path(false);
    // const root = current.substring(0, current.indexOf(this.model.id) + this.model.id.length);
    // const newlink = root + '/' + tab;
    // if (!current.startsWith(newlink)) {
    //   this._location.replaceState(newlink);
    // }
  }


  showTab(link: string) {
    const tab = this.tabGroup._tabs.find(t => t.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name') === link);
    if (tab) {
      if (this.tabGroup.selectedIndex === tab.position) {
        this.openTab(link);
      }
      this.tabGroup.selectedIndex = tab.position;
      this.tabGroup.realignInkBar();
    } else {
      this.showTab(this.linkDefault);
    }
  }


  openTab(tabName: string) {
    switch (tabName) {
      case this.linkdata: this.openDataComponent();
        break;
      default:
        this._logger.error('Defaulting to "' + this.linkDefault + '" as tabName not recognised : "' + tabName + '"');
        this.showTab(this.linkDefault);
        break;
    }
  }


  retry() {
    this.refreshDetail();
  }

  close() {
    this._sweetAlert.close();
    this._router.navigate(['/modules/list']);
  }
  refreshDetail() {
    this.getDetail(this._id);
  }
  delete() {
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Confirm,
        title: this._prefixDelete + '.Confirm.Title',
        message: this._prefixDelete + '.Confirm.Message',
        confirmButton: this._prefixDelete + '.Confirm.OkButton',
        cancelButton: this._prefixDelete + '.Confirm.CancelButton',
        successCallback: () => this.deleteConfirmed(),
        cancelCallback: () => this._sweetAlert.close()
      }));
  }

  private deleteConfirmed() {

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: this._prefixDelete + '.Processing',
        message: this._prefixDelete + '.Processing',
        confirmButton: this._prefixDelete + '.Processing'
      }));

    this._remoteModuleService.delete(this._id)
      .then(response => {
        this._logger.debug('Deleted');
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: this._prefixDelete + '.Success.Title',
            message: this._prefixDelete + '.Success.Message',
            confirmButton: this._prefixDelete + '.Success.OkButton',
            cancelButton: this._prefixDelete + '.Success.CancelButton',
            successCallback: () => this.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt delete user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: this._prefixDelete + '.Failed.Title',
            message: this._prefixDelete + '.Failed.Message',
            confirmButton: this._prefixDelete + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  private setDimensionsListener() {
    if (!this.setDimensions()) {
      const parent = this;
      const interval = setInterval(() => {
        if (parent.setDimensions()) {
          clearInterval(interval);
        }
      }, 200);
    }
  }

  private setDimensions(): boolean {
    if ($(this.detailCard.nativeElement).is(':visible')) {
      const detailCardWidth = $(this.detailCard.nativeElement).width();
      this.headerWidth = detailCardWidth + 'px';
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      return true;
    }
    return false;
  }


  ngOnInit() {
    this._windowSubscribe = this._windowListener.OnResizeAndMenu().subscribe(() => {
      this.setDimensionsListener();
    });

    this._detailChangeSubscribe = this._remoteModuleService.detailModelChangeEvent().subscribe(detail => {
      this.setDetail(detail);
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    });

    this.scrollTop();
  }

  ngOnDestroy() {
    if (this._detailSubscribe) {
      this._detailSubscribe.unsubscribe();
    }

    if (this._detailChangeSubscribe) {
      this._detailChangeSubscribe.unsubscribe();
    }

    if (this._windowSubscribe) {
      this._windowSubscribe.unsubscribe();
    }

    if (this._routerSubscribe) {
      this._routerSubscribe.unsubscribe();
    }

    if (this._locationSubscribe) {
      this._locationSubscribe.unsubscribe();
    }

    if (this._deleteSubscribe) {
      this._deleteSubscribe.unsubscribe();
    }
  }
}
