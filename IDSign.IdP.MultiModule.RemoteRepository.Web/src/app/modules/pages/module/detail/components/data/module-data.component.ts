import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { faPlus, faBolt } from '@fortawesome/free-solid-svg-icons';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { SiteActionControlService } from '@services/site-action-control.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '@http/user/user.service';
import { ModuleDetailModel } from '@models/remote-repository/module/remote-repository-module-detail';
import { ModuleUpdateModel } from '@models/remote-repository/module/remote-repository-module-update';
import { RemoteRepositoryModuleService } from '@http/remote-repository/module/remote-repository-module.service';
import { SlideBoolAnimation } from '@app/sharedanimations/slide-bool-animation';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';

@Component({
  selector: 'app-module-data',
  templateUrl: './module-data.component.html',
  styleUrls: ['./module-data.component.css'],
  animations: [SlideBoolAnimation]
})
export class ModuleDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: ModuleDetailModel;
  form: FormGroup;
  canUpdateData = false;
  canSetpassword = false;
  toggle = false;

  private _updateSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteModuleService: RemoteRepositoryModuleService,
    private _userService: UserService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _userActionControl: SiteActionControlService
  ) {
    this._logger = this._logger.createInstance('userDataComponent');
    this.setDetailModel(<ModuleDetailModel>{});
  }

  public setDetailModel(model: ModuleDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }
  toggleAnimation() {
    this.toggle = !this.toggle;
  }
  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }


  initForm() {
    if (this.form) {
      this.form.get('projectIdentifier').setValue(this.model.identifier);
      this.form.get('moduleCode').setValue(this.model.identifier && this.model.identifier.moduleCode);
      this.form.get('description').setValue(this.model.description);
    } else {
      this.form = this._fb.group({
        projectIdentifier: this._fb.control(this.model.identifier, [Validators.required]),
        moduleCode: this._fb.control(this.model.identifier && this.model.identifier.moduleCode
          , [Validators.required, noWhitespaceValidator]),
        description: this._fb.control(this.model.description, [Validators.required, noWhitespaceValidator])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <ModuleUpdateModel>{};
      model = this.form.value;
      model.newModuleCode = this.form.value.moduleCode;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'Module.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteModuleService.update(model)
        .then(identifier => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
