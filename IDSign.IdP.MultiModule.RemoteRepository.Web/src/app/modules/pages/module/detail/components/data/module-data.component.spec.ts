/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ModuleDataComponent } from './module-data.component';

describe('ModuleDataComponent', () => {
  let component: ModuleDataComponent;
  let fixture: ComponentFixture<ModuleDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModuleDataComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
