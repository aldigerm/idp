import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { RemoteRepositoryModuleService } from '@http/remote-repository/module/remote-repository-module.service';
import { ModuleBasicModel } from '@models/remote-repository/module/remote-repository-module-basic';
import { ModuleIdentifier } from '@models/identifiers/module-identifier';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { ModuleListItemModel } from '@models/remote-repository/module/list/remote-repository-module-list';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class ModulesComponent implements OnInit, AfterViewInit, OnDestroy {
  readonly allColumns: string[] = ['moduleCode', 'projectCode', 'description', 'actions'];
  readonly desktopColumns: string[] = ['moduleCode', 'projectCode', 'description', 'actions'];
  readonly tabletColumns: string[] = ['moduleCode', 'projectCode'];
  readonly mobileColumns: string[] = ['moduleCode', 'projectCode'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode',
    'moduleCode': 'identifier.moduleCode'
  };

  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;
  private _tenantIdentifier = <TenantIdentifier>{};

  listModel: Array<ModuleListItemModel>;

  private _listSubscribe: ISubscription;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _userGroupService: RemoteRepositoryModuleService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('usersComponent');
    this.listModel = new Array<ModuleListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
    this.refreshList();
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true, this._tenantIdentifier);
  }

  getList(changeDisplayStates: boolean, tenantIdentifier: TenantIdentifier) {
    if (!tenantIdentifier) {
      tenantIdentifier = new TenantIdentifier(null, null);
    }
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }

    this._userGroupService.getPageList()
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        }
        this.initTable();
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch((error: HttpErrorResponse) => {
        const errorCode = error && error.error ? error.error.ErrorCode : undefined;
        this._logger.error('Couldnt get user list; ' + errorCode, error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
  }

  create() {
    this._router.navigate(['/modules/create']);
    this.scrollTop();
  }

  closeCreate() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  closeDetail() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  clickItemDetail(event: Event, idAttribute: string, projectCodeAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const moduleCode = button.getAttribute(idAttribute);
    const projectCode = button.getAttribute(projectCodeAttribute);
    const id = new ModuleIdentifier(moduleCode, projectCode);
    this._logger.debug('Id to get is ' + JSON.stringify(id));
    this.getDetailAndShow(id);
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  refreshAndShowDetail(id: ModuleIdentifier) {
    this.getList(false, this._tenantIdentifier);
    this.getDetailAndShow(id);
  }

  private getDetailAndShow(id: ModuleIdentifier) {
    this._router.navigate(['/modules/detail', id.projectCode, id.moduleCode]);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
