import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';
import { TenantSelectorControl } from '@app/sharedcontrols/tenant-selector/tenant-selector.module';
import { PasswordControl } from '@app/sharedcontrols/password/password.module';
import { ModuleCreateComponent } from './pages/module-create.component';
import { ProjectSelectorModule } from '@app/sharedcontrols/project-selector/project-selector.module';

const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'user', url: './users' }]
  },
  component: ModuleCreateComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateTimeFormatPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    InternalTextBoxControl,
    ProjectSelectorModule,
    PasswordControl
  ],
  declarations: [
    ModuleCreateComponent]
})
export class ModuleCreateModule { }
