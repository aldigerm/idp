import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryRoleClaimTypeService } from '@http/remote-repository/role-claim-type/remote-repository-role-claim-type.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { RoleClaimTypeDetailModel } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { RoleClaimTypeSetRoles } from '@models/remote-repository/role/claims/role-claim-type/remote-repository-role-claim-type-set-roles';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { RoleBasicModel } from '@models/remote-repository/role/remote-repository-role-basic';

@Component({
  selector: 'app-role-claim-type-membership',
  templateUrl: './role-claim-type-membership.component.html',
  styleUrls: ['./role-claim-type-membership.component.css']
})
export class RoleClaimTypeMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: RoleClaimTypeDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryRoleClaimTypeService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: RoleClaimTypeDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: RoleIdentifier[] = [];
    if (this.model && this.model.roles) {
      this.model.roles.map(
        u => {
          selected.push(u.identifier);
        }
      );
    }
    this.options = options;
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenants = user.userAccessDetail.tenants
          .filter(u =>
            this.model.identifier
            && u.identifier.projectCode === this.model.identifier.projectCode);

        if (tenants && tenants.length > 0) {
          tenants.forEach(t => {
            t.roles.map(r => {
              const option = <MultiCheckboxOption>{};
              const id = new RoleIdentifier(r, t.identifier.tenantCode, t.identifier.projectCode);
              option.id = id;
              option.name = r + '( ' + t.identifier.tenantCode + ':' + t.identifier.projectCode + ')';
              option.value = id;
              options.push(option);
            });
          });
        }
        this.options = options;
      }
      if (this.form) {
        this.form.get('roles').setValue(selected || []);
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise roles list', error);
    });
    if (this.form) {
      this.form.get('roles').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        roles: this._fb.control(selected || []),
        value: this._fb.control('', [Validators.required, noWhitespaceValidator])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <RoleClaimTypeSetRoles>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'RoleClaimType.Detail.Tabs.Membership.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setRoleMembership(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

}
