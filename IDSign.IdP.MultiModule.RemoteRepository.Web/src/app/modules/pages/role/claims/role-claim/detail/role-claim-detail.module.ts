import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { FeatureControlPipeModule } from '@app/shared/pipes/control/feature-control/feature-control.pipe.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TreeModule } from 'angular-tree-component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatRadioModule } from '@angular/material/radio';
import { TimeFormatPipeModule } from '@app/shared/pipes/calendar/time/time-format.pipe.module';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';
import { InternalCheckboxControl } from '@app/sharedcontrols/checkbox/int-checkbox.module';
import { TenantSelectorControl } from '@app/sharedcontrols/tenant-selector/tenant-selector.module';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';
import { InternalMultiCheckboxControl } from '@app/sharedcontrols/multi-checkbox/int-multi-checkbox.module';
import { RoleClaimMembershipComponent } from './components/membership/role-claim-membership.component';
import { ProjectSelectorModule } from '@app/sharedcontrols/project-selector/project-selector.module';
import { RoleClaimDetailComponent } from './pages/role-claim-detail.component';
import { RoleClaimDataComponent } from './components/data/role-claim-data.component';
import { InternalLabelControl } from '@app/sharedcontrols/label/int-label.module';
import { InternalDropdownControl } from '@app/sharedcontrols/dropdown/int-dropdown.module';
import { ResponsiveTableModule } from '@app/shareddirectives/table/responsive-table.directive.module';
import { PasswordControl } from '@app/sharedcontrols/password/password.module';
import { RoleClaimDetailButtonModule } from '@app/sharedcontrols/buttons/role-claim-detail-button/role-claim-detail-button.module';
import { ProfilePictureControl } from '@app/sharedcontrols/profile-picture/profile-picture-control.module';
import { AccessControlModule } from '@app/shareddirectives/access-control/access-control.directive.module';
const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'user', url: './user' }]
  },
  component: RoleClaimDetailComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    DateTimeFormatPipeModule,
    SpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    MaterialsModule,
    ResponsiveTableModule,
    InternalTextBoxControl,
    TenantSelectorControl,
    PasswordControl,
    InternalMultiCheckboxControl,
    InternalDropdownControl,
    RoleClaimDetailButtonModule,
    InternalCheckboxControl,
    ProfilePictureControl,
    InternalLabelControl,
    AccessControlModule
  ],
  declarations: [
    RoleClaimDetailComponent,
    RoleClaimDataComponent,
    RoleClaimMembershipComponent
  ]
})
export class RoleClaimDetailModule { }
