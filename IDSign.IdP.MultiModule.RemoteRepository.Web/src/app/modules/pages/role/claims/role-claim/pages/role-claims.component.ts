import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import {
  AfterViewInit, ChangeDetectorRef, Component
  , OnDestroy, OnInit, QueryList, ViewChildren, Input, EventEmitter, Output
} from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { RemoteRepositoryRoleClaimService } from '@http/remote-repository/role-claim/remote-repository-role-claim.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { HttpErrorResponse } from '@angular/common/http';
import { RoleClaimListItemModel } from '@models/remote-repository/role/claims/role-claim/list/remote-repository-role-claim-list-item';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
@Component({
  selector: 'app-role-claims',
  templateUrl: './role-claims.component.html',
  styleUrls: ['./role-claims.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class RoleClaimsComponent implements OnInit, AfterViewInit, OnDestroy {


  readonly allColumns: string[] = ['type', 'value', 'projectCode', 'tenantCode', 'roleCode', 'description', 'actions'];
  readonly desktopColumns: string[] = ['type', 'value', 'projectCode', 'tenantCode', 'roleCode', 'actions'];
  readonly tabletColumns: string[] = ['type', 'value', 'roleCode', 'tenantCode'];
  readonly mobileColumns: string[] = ['type', 'roleCode'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'type': 'identifier.type',
    'value': 'identifier.value',
    'projectCode': 'identifier.projectCode',
    'tenantCode': 'identifier.tenantCode',
    'roleCode': 'identifier.roleCode'
  };

  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;
  private _projectIdentifier = <ProjectIdentifier>{};
  private _useCallBacks = false;

  listModel: Array<RoleClaimListItemModel>;
  tableSuffix = '-page-users';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;

  @Input() set roleClaimList(value: Array<RoleClaimListItemModel>) {
    this.listModel = value;
  }
  @Input() set useCallBacks(value: boolean) {
    this._useCallBacks = value;
  } get useCallBacks(): boolean {
    return this._useCallBacks;
  }
  @Output() clickedDetail = new EventEmitter<RoleClaimIdentifier>();
  @Output() clickedDelete = new EventEmitter<RoleClaimIdentifier>();
  @ViewChildren('userRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _remoteService: RemoteRepositoryRoleClaimService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('usersComponent');
    this.listModel = new Array<RoleClaimListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    const parent = this;
    parent.refreshList();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true, this._projectIdentifier);
  }

  getList(changeDisplayStates: boolean, projectIdentifier: ProjectIdentifier) {
    if (!projectIdentifier) {
      projectIdentifier = <ProjectIdentifier>{
        projectCode: null
      };
    }
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }

    this._remoteService.getPageList()
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        } else {
          this.displayState = this.displayStateContent;
        }
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch((error: HttpErrorResponse) => {
        const errorCode = error && error.error ? error.error.ErrorCode : undefined;
        this._logger.error('Couldnt get user list; ' + errorCode, error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/roleclaims/create']);
    this.scrollTop();
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  clickDetail(identifier: RoleClaimIdentifier) {
    this.clickedDetail.emit(identifier);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
