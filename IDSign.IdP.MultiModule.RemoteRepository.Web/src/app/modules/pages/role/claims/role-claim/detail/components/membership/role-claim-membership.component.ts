import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryRoleClaimService } from '@http/remote-repository/role-claim/remote-repository-role-claim.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { RoleClaimDetailModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import * as _ from 'lodash';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { RoleClaimSetRoles } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim-set-roles';
@Component({
  selector: 'app-role-claim-membership',
  templateUrl: './role-claim-membership.component.html',
  styleUrls: ['./role-claim-membership.component.css']
})
export class RoleClaimMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: RoleClaimDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryRoleClaimService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _router: Router,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: RoleClaimDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: RoleIdentifier[] = [];
    if (this.model && this.model.rolesWithSameClaim) {
      this.model.rolesWithSameClaim.map(
        u => {
          selected.push(u);
        }
      );
    }
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenant = user.userAccessDetail.tenants.find(t => this.model
          && t.identifier.projectCode === this.model.identifier.projectCode
          && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenant) {
          tenant.roles.map(r => {
            const option = <MultiCheckboxOption>{};
            const id = new RoleIdentifier(r, tenant.identifier.tenantCode, tenant.identifier.projectCode);
            option.id = id;
            option.name = r + '( ' + tenant.identifier.tenantCode + ':' + tenant.identifier.projectCode + ')';
            option.value = id;
            options.push(option);
          });
        }
        this.options = options;

        if (this.form) {
          this.form.get('roles').setValue(selected || []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise roles list', error);
    });
    if (this.form) {
      this.form.get('roles').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        roles: this._fb.control(selected || [])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const model = <RoleClaimSetRoles>{};
      model.roles = [];
      this.form.value.roles.map(u => {
        const user = <RoleIdentifier>{};
        user.roleCode = u.roleCode;
        user.tenantCode = u.tenantCode;
        user.projectCode = u.projectCode;
        model.roles.push(user);
      });

      // check if the current user is also selected to be removed
      const showWarning = model.roles.find(role => _.isMatch(this.model.identifier, role)) ? false : true;

      model.identifier = <RoleClaimIdentifier>{};
      model.identifier = this.model.identifier;

      if (showWarning) {
        const prefix = 'RoleClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Confirm,
            title: prefix + '.Confirm.Title',
            message: prefix + '.Confirm.Message',
            confirmButton: prefix + '.Confirm.OkButton',
            cancelButton: prefix + '.Confirm.CancelButton',
            successCallback: () => this.performUpdate(model, true),
            cancelCallback: () => this._sweetAlert.close()
          }));
      } else {
        this.performUpdate(model, false);
      }
    }

  }

  private performUpdate(model: RoleClaimSetRoles, closeOnReady: boolean) {
    const current = this;
    this._logger.debug('To update : ' + JSON.stringify(model));

    const prefix = 'RoleClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));


    this._remoteUserService.setRoleMembership(model)
      .then(response => {
        if (closeOnReady) {
          this._router.navigate(['/roleclaims/list']);
        }
        current._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt update user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }
}
