import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { tenantValidator, projectValidator } from '@app/sharedvalidators/identifier.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { RemoteRepositoryRoleClaimService } from '@http/remote-repository/role-claim/remote-repository-role-claim.service';
import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { exactSelectionRequiredValidator } from '@app/sharedvalidators/selection-required.validator';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { RoleClaimCreateModel } from '@models/remote-repository/role/claims/role-claim/remote-repository-role-claim-create';

@Component({
  selector: 'app-role-claim-create',
  templateUrl: './role-claim-create.component.html',
  styleUrls: ['./role-claim-create.component.css']
})
export class RoleClaimCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;
  rolesOptions: MultiCheckboxOption[] = [];
  typesOptions: MultiCheckboxOption[] = [];
  private _roleIdentifier: RoleIdentifier;

  private _createSubscribe: ISubscription;
  readonly = false;

  @Input() set roleIdentifier(value: RoleIdentifier) {
    this._roleIdentifier = value;
    if (value) {
      this.readonly = true;
      this.initForm();
    }
  } get roleIdentifier(): RoleIdentifier {
    return this._roleIdentifier;
  }
  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: true }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryRoleClaimService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this._logger = this._logger.createInstance('UserCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {


    if (this.form) {
      this.form.get('type').setValue([]);
      this.form.get('value').setValue('');
      this.form.get('roleCode').setValue((this.roleIdentifier && this.roleIdentifier.roleCode) || '');
      this.form.get('tenantIdentifier').setValue(this.roleIdentifier || <TenantIdentifier>{});
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    } else {
      this.form = this._fb.group({
        type: this._fb.control([]),
        value: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        roleCode: this._fb.control((this.roleIdentifier && this.roleIdentifier.roleCode) || '', [exactSelectionRequiredValidator(1)]),
        tenantIdentifier: this._fb.control(this.roleIdentifier || <TenantIdentifier>{}, [tenantValidator()])
      });

      this.form.get('tenantIdentifier').valueChanges.subscribe(
        (value: TenantIdentifier) => {
          this.updateList(value);
        });
    }
  }

  updateList(tenantIdentifier: TenantIdentifier) {
    if (tenantIdentifier) {

      this._userService.getUser().then(user => {
        if (user && user.userAccessDetail && user.userAccessDetail.tenants && tenantIdentifier) {
          const tenant = user.userAccessDetail.tenants.find(t => tenantIdentifier
            && t.identifier.projectCode === tenantIdentifier.projectCode
            && t.identifier.tenantCode === tenantIdentifier.tenantCode);

          const rolesOptions: MultiCheckboxOption[] = [];
          if (tenant && tenant.roles.length > 0) {
            tenant.roles.map(u => {
              const option = <MultiCheckboxOption>{};
              option.id = u;
              option.name = u;
              option.value = u;
              rolesOptions.push(option);
            });
          }
          this.rolesOptions = rolesOptions;

          const project = user.userAccessDetail.projects.find(t =>
            t.identifier.projectCode === tenantIdentifier.projectCode);
          const typesOptions = [];
          if (project && project.roleClaimTypesDetail && project.roleClaimTypesDetail.length > 0) {
            project.roleClaimTypesDetail.forEach(claim => {
              const option = <MultiCheckboxOption>{};
              option.id = claim.identifier.type;
              option.name = claim.identifier.type + ' (' + claim.description + ')';
              option.value = claim.identifier;
              typesOptions.push(option);
            });
            this.typesOptions = typesOptions;

            if (this.form) {
              this.form.get('roleCode').setValue([]);
              this.form.get('type').setValue([]);
            }
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      }).catch(error => {
        this._logger.error('Couldnt initialise list', error);
      });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/roleclaims/list']);
  }

  closeAndOpenDetail(id: RoleClaimIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/roleclaims/detail'
      , id.projectCode
      , id.tenantCode
      , id.roleCode
      , id.type
      , id.value]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotherRoleClaim() {
    this.form.reset();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {

    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <RoleClaimCreateModel>{};
      model = this.form.value;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'RoleClaim.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(identifier => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotherRoleClaim()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
