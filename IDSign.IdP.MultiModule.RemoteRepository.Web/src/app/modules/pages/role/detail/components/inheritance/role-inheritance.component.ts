import { Component, OnInit } from '@angular/core';
import { RoleDetailModel } from '@models/remote-repository/role/remote-repository-role-detail';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryRoleService } from '@http/remote-repository/role/remote-repository-role.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import {
  RoleSetInheritingRoles
} from '@models/remote-repository/role/remote-repository-role-set-inheriting-roles';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';

@Component({
  selector: 'app-role-inheritance',
  templateUrl: './role-inheritance.component.html',
  styleUrls: ['./role-inheritance.component.css']
})
export class RoleInheritanceComponent implements OnInit {

  faBolt = faBolt;
  model: RoleDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryRoleService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('RoleInheritanceComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: RoleDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenant = user.userAccessDetail.tenants
          .find(t => this.model.identifier && t.identifier.projectCode === this.model.identifier.projectCode
            && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenant) {
          tenant.roles.map(r => {
            const option = <MultiCheckboxOption>{};
            option.id = r;
            option.name = r;
            option.value = r;
            options.push(option);
          });
        }
        this.options = options;
        if (this.form) {
          this.form.get('inheritsFrom').setValue(this.model ? this.model.inheritsFromRoles : []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise project dropdown', error);
    });
    if (this.form) {
      this.form.get('inheritsFrom').setValue(this.model ? this.model.inheritsFromRoles : []);
    } else {
      this.form = this._fb.group({
        inheritsFrom: this._fb.control(this.model ? this.model.inheritsFromRoles : []),
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <RoleSetInheritingRoles>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'Role.Detail.Tabs.Inheritance.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setInheritingFromRoles(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

}
