import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserCreateModel } from '@models/remote-repository/user/remote-repository-user-create';
import { UserService } from '@http/user/user.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { tenantValidator } from '@app/sharedvalidators/identifier.validator';
import { RemoteRepositoryRoleService } from '@http/remote-repository/role/remote-repository-role.service';
import { RoleCreateModel } from '@models/remote-repository/role/remote-repository-role-create';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;

  private _createSubscribe: ISubscription;

  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: true }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryRoleService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService
  ) {
    this._logger = this._logger.createInstance('UserCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {
    if (this.form) {
      this.form.get('roleCode').setValue('');
      this.form.get('description').setValue('');
      this.form.get('tenantIdentifier').setValue(<TenantIdentifier>{});
    } else {
      this.form = this._fb.group({
        roleCode: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        description: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        tenantIdentifier: this._fb.control(<TenantIdentifier>{}, [tenantValidator()])
      });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/roles/list']);
  }

  closeAndOpenDetail(id: RoleIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/roles/detail', id.projectCode
      , id.tenantCode, id.roleCode]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotherRole() {
    this.form.reset();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <RoleCreateModel>{};
      model = this.form.value;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'Role.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(identifier => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotherRole()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
