import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SectionPreloaderComponent } from '@app/shared/components/section-preloader/components/section-preloader.component';

@Component({
    selector: 'app-layout',
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss']
})
export class PageComponent implements OnInit, AfterViewInit, OnDestroy {

    private _sectionPreloaderMessageSubscribe: ISubscription;
    private _sectionPreloaderStateSubscribe: ISubscription;

    @ViewChild(SectionPreloaderComponent, { static: true }) sectionPreloaderComponent: SectionPreloaderComponent;
    constructor(
        private _router: Router,
        private _sectionPreloaderService: SectionPreloaderService,
        private _logger: LoggerService
    ) {
        this._logger = _logger.createInstance('PageComponent');
    }

    ngAfterViewInit() {
        this._sectionPreloaderStateSubscribe = this._sectionPreloaderService.state().subscribe(state => {
            this._logger.debug('New section preloader state : ' + state);
            this.sectionPreloaderComponent.setLoaderState(state);
        });

        this._sectionPreloaderMessageSubscribe = this._sectionPreloaderService.message().subscribe(message => {
            this._logger.debug('New section preloader message : ' + message);
            this.sectionPreloaderComponent.setLoaderMessage(message);
        });
    }

    ngOnInit() {
        if (this._router.url === '/') {
            this._router.navigate(['/users/detail']);
        }
    }

    ngOnDestroy() {
        if (this._sectionPreloaderMessageSubscribe) {
            this._sectionPreloaderMessageSubscribe.unsubscribe();
        }
        if (this._sectionPreloaderStateSubscribe) {
            this._sectionPreloaderStateSubscribe.unsubscribe();
        }
    }
}
