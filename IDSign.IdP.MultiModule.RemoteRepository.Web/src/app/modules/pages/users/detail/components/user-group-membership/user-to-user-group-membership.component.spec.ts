/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { UserToUserGroupMembershipComponent } from './user-to-user-group-membership.component';


describe('UserToUserGroupMembershipComponent', () => {
  let component: UserToUserGroupMembershipComponent;
  let fixture: ComponentFixture<UserToUserGroupMembershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserToUserGroupMembershipComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserToUserGroupMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
