import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { UserSetUserTenants } from '@models/remote-repository/user/remote-repository-user-set-user-tenants';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-user-tenant-membership',
  templateUrl: './user-tenant-membership.component.html',
  styleUrls: ['./user-tenant-membership.component.css']
})
export class UserTenantMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: UserDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _router: Router,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('UserTenantMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        user.userAccessDetail.tenants.map(t => {
          const option = <MultiCheckboxOption>{};
          option.id = t.identifier;
          option.name = t.name;
          option.value = t.identifier;
          options.push(option);
        });
        this.options = options;
        if (this.form) {
          this.form.get('tenants').setValue(this.model ? this.model.tenants : []);
        } else {
          this.form = this._fb.group({
            tenants: this._fb.control(this.model ? this.model.tenants : []),
          });
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise project dropdown', error);
    });
    if (this.form) {
      this.form.get('tenants').setValue(this.model ? this.model.tenants : []);
    } else {
      this.form = this._fb.group({
        tenants: this._fb.control(this.model ? this.model.tenants : []),
      });
    }
  }

  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserSetUserTenants>{};
      model = this.form.value;
      model.userGuid = this.model.identifier.userGuid;

      // for empty lists
      model.tenants = model.tenants ? model.tenants : [];

      this._logger.debug('To update : ' + JSON.stringify(model));

      // check if the current user is also selected to be removed
      this._userService.getUser()
        .then(user => {

          const tenant = user.identifier.tenantCode === this.model.identifier.tenantCode
            && user.identifier.projectCode === this.model.identifier.projectCode;

          let showWarning = false;
          if (tenant) {
            const totalManagedGroups = model.tenants.length;
            showWarning = totalManagedGroups === 0 && (!user.isSuperUser && !user.isAdmin);
          }

          if (showWarning) {
            const prefix = 'User.Detail.Tabs.UserTenantMembership.Actions.Submit.Popup';

            this._sweetAlert.show(
              new SweetAlertRequest({
                type: SweetAlertType.Confirm,
                title: prefix + '.Confirm.Title',
                message: prefix + '.Confirm.Message',
                confirmButton: prefix + '.Confirm.OkButton',
                cancelButton: prefix + '.Confirm.CancelButton',
                successCallback: () => this.performUpdate(model, true),
                cancelCallback: () => this._sweetAlert.close()
              }));
          } else {
            if (model.tenants && model.tenants.length > 0) {
              this.performUpdate(model, false);
            } else {
              if (this.model.isTenantSpecific) {
                this.performUpdate(model, true);
              } else {
                this.performUpdate(model, false);
              }
            }
          }
        });
    }
  }

  performUpdate(model: UserSetUserTenants, closeOnReady: boolean) {
    const prefix = 'User.Detail.Tabs.UserTenantMembership.Actions.Submit.Popup';
    const current = this;
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));


    this._remoteUserService.setUserTenants(model)
      .then(response => {
        if (closeOnReady) {
          this._router.navigate(['/users/list']);
        }
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt update user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }
}
