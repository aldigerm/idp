/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { UserTenantMembershipComponent } from './user-tenant-membership.component';


describe('UserTenantMembershipComponent', () => {
  let component: UserTenantMembershipComponent;
  let fixture: ComponentFixture<UserTenantMembershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserTenantMembershipComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTenantMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
