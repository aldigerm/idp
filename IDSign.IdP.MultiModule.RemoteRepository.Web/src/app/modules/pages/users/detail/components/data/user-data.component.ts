import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { faPlus, faBolt } from '@fortawesome/free-solid-svg-icons';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { ErrorCode } from '@models/error-code';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { environment } from '@environments/environment';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { SiteActionControlService } from '@services/site-action-control.service';
import { UserUpdateModel } from '@models/remote-repository/user/remote-repository-user-update';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: UserDetailModel;
  form: FormGroup;
  canUpdateData = false;
  canSetpassword = false;

  private _createSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService
  ) {
    this._logger = this._logger.createInstance('userDataComponent');
    this.setDetailModel(<UserDetailModel>{});
  }

  public setDetailModel(model: UserDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }


  initForm() {

    if (this.form) {
      this.form.get('username').setValue(this.model.identifier && this.model.identifier.username);
      this.form.get('firstName').setValue(this.model.firstName || '');
      this.form.get('lastName').setValue(this.model.lastName || '');
      this.form.get('emailAddress').setValue(this.model.emailAddress || '');
      this.form.get('enabled').setValue(this.model.enabled);

      if (this.model.isLoggedInUser) {
        this.form.get('enabled').disable();
      }

    } else {
      this.form = this._fb.group({
        username: this._fb.control(this.model.identifier && this.model.identifier.username, [Validators.required, noWhitespaceValidator]),
        firstName: this._fb.control(this.model.firstName || ''),
        lastName: this._fb.control(this.model.lastName || ''),
        emailAddress: this._fb.control(this.model.emailAddress || ''),
        enabled: this._fb.control({ value: this.model.enabled, disabled: this.model.isLoggedInUser })
      });
    }
  }


  update() {
    const current = this;
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      let model = <UserUpdateModel>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'User.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
