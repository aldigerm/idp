import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimesCircle, faBolt } from '@fortawesome/free-solid-svg-icons';
import { LoggerService } from '@app/core/logging/logger.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SlideBoolAnimation } from '@app/sharedanimations/slide-bool-animation';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { UserClaimDetailModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim';
import { RemoteRepositoryUserClaimService } from '@http/remote-repository/user-claim/remote-repository-user-claim.service';
import { UserClaimUpdateModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-update';
import { UserService } from '@http/user/user.service';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { InternalDropdownComponent } from '@app/sharedcontrols/dropdown/int-dropdown.component';
import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { UserClaimCreateModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-create';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-user-assigned-user-claims',
  templateUrl: './user-assigned-user-claims.component.html',
  styleUrls: ['./user-assigned-user-claims.component.css'],
  animations: [SlideBoolAnimation, DetailExpandAnimation]
})
export class UserAssignedUserClaimsComponent implements OnInit, OnDestroy {

  readonly allColumns: string[] = ['type', 'value', 'actions'];
  readonly desktopColumns: string[] = ['type', 'value', 'actions'];
  readonly tabletColumns: string[] = ['type', 'value'];
  readonly mobileColumns: string[] = ['type', 'value'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'type': 'identifier.type',
    'value': 'identifier.value'
  };
  private _createSubscribe: ISubscription;
  private _updateSubscribe: ISubscription;
  private _deleteSubscribe: ISubscription;
  claimIdentifier: UserClaimIdentifier;

  formAdd: FormGroup;
  formUpdate: FormGroup;
  model: UserDetailModel;
  options = new Array<IMultiSelectOption>();

  faPlus = faPlus;
  faBolt = faBolt;
  faTimesCircle = faTimesCircle;

  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateAdd = 'Add';
  readonly displayStateUpdate = 'Update';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  @ViewChild('type', { static: true }) typeControl: InternalDropdownComponent;
  @ViewChild('newType', { static: true }) newTypeControl: InternalDropdownComponent;

  constructor(
    private _sweetAlert: SweetAlertService,
    private _logger: LoggerService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _remoteService: RemoteRepositoryUserClaimService,
    private _userService: UserService,
    private _scroller: ScrollService,
    private _fb: FormBuilder
  ) {
    this._logger = this._logger.createInstance('UserAssignedUserClaimsComponent');
    this.setDetailModel(<UserDetailModel>{});
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
    if (this._deleteSubscribe) {
      this._deleteSubscribe.unsubscribe();
    }
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  clickDetail(identifier: UserClaimIdentifier) {
    this.claimIdentifier = identifier;
    this.setDisplayState(this.displayStateUpdate);
  }
  showOthers() {

    if (this.model.userClaims && this.model.userClaims.length > 0) {
      this.setDisplayState(this.displayStateContent);
    } else {
      this.setDisplayState(this.displayStateNoneFound);
    }
  }

  showContentIfNotOnform() {

    if (this.displayState !== this.displayStateAdd && this.displayState !== this.displayStateUpdate) {
      this.setDisplayState(this.displayStateContent);
    }
  }

  public setDetailModel(model: UserDetailModel) {
    this.model = model;
    this.showContentIfNotOnform();
    this.initForm();
  }
  initForm() {
    if (this.formAdd) {
      this.formAdd.get('type').setValue('');
      this.formAdd.get('value').setValue('');
    } else {
      this.formAdd = this._fb.group({
        type: this._fb.control('', [Validators.required]),
        value: this._fb.control('')
      });
    }
    if (this.formUpdate) {
      this.formUpdate.get('newType').setValue('');
      this.formUpdate.get('newValue').setValue('');
    } else {
      this.formUpdate = this._fb.group({
        newType: this._fb.control('', [Validators.required]),
        newValue: this._fb.control('')
      });
    }

    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.projects && this.model.identifier) {
        const project = user.userAccessDetail.projects.find(t =>
          t.identifier.projectCode === this.model.identifier.projectCode);
        const options = new Array<IMultiSelectOption>();
        if (project && project.userClaimTypesDetail) {
          project.userClaimTypesDetail.forEach(claim => {
            options.push({ id: claim.identifier.type, name: claim.identifier.type + ' (' + claim.description + ')' });
          });
        }
        this.options = options;
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise tenant dropdown', error);
    });
  }

  setDisplayState(state: string) {
    this.displayState = state;
  }

  add() {
    if (!this.formAdd.valid) {
      validateAllFormFields(this.formAdd);
    } else {
      const current = this;
      let model = <UserClaimCreateModel>{};
      model = this.formAdd.value;
      model.tenantIdentifier = this.model.identifier;
      model.username = this.model.identifier.username;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'User.Detail.Tabs.Claims.Actions.Add.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteService.create(model)
        .then(identifier => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => {

                current._sweetAlert.close();

                current.formAdd.reset();

              }, cancelCallback: () => {
                current._sweetAlert.close();

                current.formAdd.reset();

                current.showOthers();
              }
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

  update() {
    if (!this.formUpdate.valid) {
      validateAllFormFields(this.formUpdate);
    } else {
      const current = this;
      let model = <UserClaimUpdateModel>{};
      model = this.formUpdate.value;

      model.identifier = <UserClaimIdentifier>{};
      model.identifier = this.claimIdentifier;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'User.Detail.Tabs.Claims.Actions.Update.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => {

                current._sweetAlert.close();

                current.formUpdate.reset();

                current.showOthers();

              }
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

  clickDelete(event: Event, identifier: UserClaimIdentifier) {

    this._logger.debug('To delete : ' + JSON.stringify(identifier));


    const prefix = 'User.Detail.Tabs.Claims.Actions.Delete.Popup';

    const current = this;
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));

    this._remoteService.delete(identifier)
      .then(response => {
        // we remove this. the model should be updated automatically soon after
        this.model.userClaims = this.model.userClaims.filter(uc => !(_.isMatch(uc.identifier, identifier)));
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  clickEdit(event: Event, id: UserClaimIdentifier) {
    this._logger.debug('Id to get is ' + id);

    const claim = this.model.userClaims.find(uc => _.isMatch(id, uc.identifier));

    if (claim) {
      this.claimIdentifier = id;
      this.formUpdate.get('newType').setValue(claim.identifier.type);
      this.formUpdate.get('newValue').setValue(claim.identifier.value);
      this.setDisplayState(this.displayStateUpdate);
    }

  }

}
