/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { UserAssignedUserClaimsComponent } from './user-assigned-user-claims.component';

describe('ProjectUserClaimsComponent', () => {
  let component: UserAssignedUserClaimsComponent;
  let fixture: ComponentFixture<UserAssignedUserClaimsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserAssignedUserClaimsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAssignedUserClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
