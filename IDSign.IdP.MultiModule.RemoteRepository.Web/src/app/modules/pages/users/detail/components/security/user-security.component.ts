import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserService } from '@http/user/user.service';
import { ErrorCode } from '@models/error-code';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { UserSetPasswordModel } from '@models/remote-repository/user/remote-repository-user-set-password';
import { UserSetSecuritySettingsViewModel } from '@models/remote-repository/user/remote-repository-user-set-security-settings';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SiteActionControlService } from '@services/site-action-control.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { IsJsonString } from '@app/sharedfunctions/utils.function';


@Component({
  selector: 'app-user-security',
  templateUrl: './user-security.component.html',
  styleUrls: ['./user-security.component.css']
})
export class UserSecurityComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: UserDetailModel;
  formPassword: FormGroup;
  formSettings: FormGroup;
  canUpdateData = false;
  canSetpassword = false;
  passwordPoliciesResult: PasswordPolicyDetailModel[];

  private _createSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService
  ) {
    this._logger = this._logger.createInstance('userDataComponent');
    this.setDetailModel(<UserDetailModel>{});
  }

  public setDetailModel(model: UserDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }


  initForm() {

    if (this.formPassword) {
      this.formPassword.get('password').setValue('');
    } else {
      this.formPassword = this._fb.group({
        password: this._fb.control('', [Validators.required, noWhitespaceValidator])
      });
    }

    if (this.formSettings) {
      this.formSettings.get('setPasswordOnNextLogin').setValue(this.model.setPasswordOnNextLogin);
    } else {
      this.formSettings = this._fb.group({
        setPasswordOnNextLogin: this._fb.control(false)
      });
    }
  }


  updatePassword() {
    const current = this;
    if (!this.formPassword.valid) {
      validateAllFormFields(this.formPassword);
    } else {
      let model = <UserSetPasswordModel>{};
      model = this.formPassword.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'User.Detail.Tabs.Security.Actions.UpdatePassword.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setPassword(model)
        .then(response => {
          this.passwordPoliciesResult = null;
          // reset password
          this.formPassword.get('password').setValue('');
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user password', error);
          if (error && error.error && error.error.ErrorCode === ErrorCode[ErrorCode.USER_PASSWORD_FAILED_VALIDATION]) {

            if (error.error.ExceptionData) {
              const data = error.error.ExceptionData;
              if (IsJsonString(data)) {
                this.passwordPoliciesResult = JSON.parse(data) as PasswordPolicyDetailModel[];
              }
            }
          }
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));

        });
    }
  }

  updatSettings() {
    const current = this;
    if (!this.formSettings.valid) {
      validateAllFormFields(this.formPassword);
    } else {
      let model = <UserSetSecuritySettingsViewModel>{};
      model = this.formSettings.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'User.Detail.Tabs.Security.Actions.UpdateSecuritySettings.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setSecuritySettings(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
