import { Component, OnInit } from '@angular/core';
import { UserGroupDetailModel } from '@models/remote-repository/user-group/remote-repository-user-group-detail';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import {
  UserGroupSetInheritingUserGroups
} from '@models/remote-repository/user-group/remote-repository-user-group-set-inheriting-user-groups';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { UserSetUserGroups } from '@models/remote-repository/user/remote-repository-user-set-user-groups';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';

@Component({
  selector: 'app-user-group-management',
  templateUrl: './user-group-management.component.html',
  styleUrls: ['./user-group-management.component.css']
})
export class UserGroupManagementComponent implements OnInit {

  faBolt = faBolt;
  model: UserDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('UserToUserGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options = [];
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenant = user.userAccessDetail.tenants
          .find(t => t.identifier.projectCode === this.model.identifier.projectCode
            && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenant) {
          tenant.userGroups.map(ug => {
            const option = <MultiCheckboxOption>{};
            option.id = ug;
            option.name = ug;
            option.value = ug;
            options.push(option);
          });
        }
        this.options = options;
        if (this.form) {
          this.form.get('userGroups').setValue(this.model ? this.model.managedUserGroups : []);
        } else {
          this.form = this._fb.group({
            userGroups: this._fb.control(this.model ? this.model.managedUserGroups : []),
          });
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise project dropdown', error);
    });
    if (this.form) {
      this.form.get('userGroups').setValue(this.model ? this.model.userGroups : []);
    } else {
      this.form = this._fb.group({
        userGroups: this._fb.control(this.model ? this.model.userGroups : []),
      });
    }
  }

  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserSetUserGroups>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'User.Detail.Tabs.UserGroupManagement.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setManagedUserGroups(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

}
