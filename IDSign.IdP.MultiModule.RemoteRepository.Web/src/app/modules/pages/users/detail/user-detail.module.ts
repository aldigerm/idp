import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';
import { ReturnToMainSiteButtonModule } from '@app/sharedcontrols/buttons/return-to-main-site-button/return-to-main-site-button.module';
import { UserClaimDetailButtonModule } from '@app/sharedcontrols/buttons/user-claim-detail-button/user-claim-detail-button.module';
import { InternalCheckboxControl } from '@app/sharedcontrols/checkbox/int-checkbox.module';
import { InternalDropdownControl } from '@app/sharedcontrols/dropdown/int-dropdown.module';
import { InternalLabelControl } from '@app/sharedcontrols/label/int-label.module';
import { InternalMultiCheckboxControl } from '@app/sharedcontrols/multi-checkbox/int-multi-checkbox.module';
import {
  PasswordPolicyValidationResultModule
} from '@app/sharedcontrols/password/password-policy-validation-result/password-policy-validation-result.module';
import { PasswordControl } from '@app/sharedcontrols/password/password.module';
import { ProfilePictureControl } from '@app/sharedcontrols/profile-picture/profile-picture-control.module';
import { TenantSelectorControl } from '@app/sharedcontrols/tenant-selector/tenant-selector.module';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';
import { AccessControlModule } from '@app/shareddirectives/access-control/access-control.directive.module';
import { ResponsiveTableModule } from '@app/shareddirectives/table/responsive-table.directive.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { UserAssignedUserClaimsComponent } from './components/claims/user-assigned-user-claims.component';
import { UserDataComponent } from './components/data/user-data.component';
import { UserSecurityComponent } from './components/security/user-security.component';
import { UserTenantMembershipComponent } from './components/tenant-membership/user-tenant-membership.component';
import { UserGroupManagementComponent } from './components/user-group-management/user-group-management.component';
import { UserToUserGroupMembershipComponent } from './components/user-group-membership/user-to-user-group-membership.component';
import { UserDetailComponent } from './pages/user-detail.component';
const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'user', url: './user' }]
  },
  component: UserDetailComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    DateTimeFormatPipeModule,
    SpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    MaterialsModule,
    ResponsiveTableModule,
    InternalTextBoxControl,
    TenantSelectorControl,
    PasswordControl,
    InternalMultiCheckboxControl,
    InternalDropdownControl,
    UserClaimDetailButtonModule,
    InternalCheckboxControl,
    ProfilePictureControl,
    InternalLabelControl,
    AccessControlModule,
    ReturnToMainSiteButtonModule,
    PasswordPolicyValidationResultModule
  ],
  declarations: [
    UserDetailComponent,
    UserDataComponent,
    UserToUserGroupMembershipComponent,
    UserSecurityComponent,
    UserAssignedUserClaimsComponent,
    UserGroupManagementComponent,
    UserTenantMembershipComponent
  ]
})
export class UserDetailModule { }
