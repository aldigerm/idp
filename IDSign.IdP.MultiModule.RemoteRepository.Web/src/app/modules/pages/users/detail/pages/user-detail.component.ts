import { MenuService } from '@services/menu.service';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { LocationService } from '@services/location.service';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { faTimesCircle, faTrash, faHandPointLeft } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserDetailModel } from '@models/remote-repository/user/remote-repository-user-detail';
import { UserDataComponent } from '../components/data/user-data.component';
import { UserService } from '@app/core/http/user/user.service';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { filter } from 'rxjs/operators';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { UserToUserGroupMembershipComponent } from '../components/user-group-membership/user-to-user-group-membership.component';
import { UserSecurityComponent } from '../components/security/user-security.component';
import { UserAssignedUserClaimsComponent } from '../components/claims/user-assigned-user-claims.component';
import { RemoteRepositoryUserClaimService } from '@http/remote-repository/user-claim/remote-repository-user-claim.service';
import { BrowserStorage } from '@services/storage/browser-storage';
import { environment } from '@environments/environment';
import { UserGroupManagementComponent } from '../components/user-group-management/user-group-management.component';
import { UserTenantMembershipComponent } from '../components/tenant-membership/user-tenant-membership.component';
declare const $: any;

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  animations: [SlideBoolAnimation]
})
export class UserDetailComponent implements OnInit, OnDestroy {
  faTimesCircle = faTimesCircle;
  faSync = faSync;
  faTrash = faTrash;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateError = 'Error';

  readonly linkdata = 'data';
  readonly linkSecurity = 'security';
  readonly linkUsergroup = 'usergroups';
  readonly linkClaims = 'claims';
  readonly linkGroupManagement = 'userGroupManagement';
  readonly linkUserTenantMembership = 'userTenants';
  readonly linkDefault = this.linkdata;
  displayState = this.displayStateLoader;

  private _detailChangeSubscribe: ISubscription;
  private _userClaimDetailChangeSubscribe: ISubscription;
  private _windowSubscribe: ISubscription;
  private _locationSubscribe: ISubscription;
  private _routerSubscribe: ISubscription;
  private _isDetailOfCurrentUser = false;
  private readonly _prefixDelete = 'User.Detail.Actions.Delete.Popup';
  returnUrl = '';

  model: UserDetailModel;
  headerWidth = '400px';
  private _id = <UserIdentifier>{};
  @Output() closeComponent = new EventEmitter<any>();
  @ViewChild('detailCard', { static: true }) detailCard: ElementRef;
  @ViewChild(MatTabGroup, { static: true }) tabGroup: MatTabGroup;
  @ViewChild(UserDataComponent, { static: false }) dataComponent: UserDataComponent;
  @ViewChild(UserToUserGroupMembershipComponent, { static: false }) userGroupMembershipComponent: UserToUserGroupMembershipComponent;

  @ViewChild(UserSecurityComponent, { static: false }) securityComponent: UserSecurityComponent;
  @ViewChild(UserAssignedUserClaimsComponent, { static: false }) claimsComponent: UserAssignedUserClaimsComponent;
  @ViewChild(UserGroupManagementComponent, { static: false }) userGroupManagementComponent: UserGroupManagementComponent;
  @ViewChild(UserTenantMembershipComponent, { static: false }) userTenantMembershipComponent: UserTenantMembershipComponent;

  constructor(
    private _remoteUserClaimService: RemoteRepositoryUserClaimService,
    private _remoteUserService: RemoteRepositoryUserService,
    private _userService: UserService,
    private _logger: LoggerService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _location: LocationService,
    private _windowListener: WindowEventListenerService,
    private _scroller: ScrollService,
    private _menuService: MenuService,
    private _sweetAlert: SweetAlertService,
    private _browserStorage: BrowserStorage
  ) {
    this._logger = this._logger.createInstance('userDetailComponent');
    this.displayState = this.displayStateLoader;
    this.model = <UserDetailModel>{};

    this._routerSubscribe = this._router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (event.url.startsWith('/users/')) {
          this._logger.debug('Router navigation for user detail detected.');
          this.init();
        }
      });
  }

  refresh() {
    this.retry();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  private init() {
    const userGuid = this._activatedRoute.snapshot.queryParamMap.get('userIdentifier');
    const username = this._activatedRoute.snapshot.queryParamMap.get('username');
    const tenantCode = this._activatedRoute.snapshot.queryParamMap.get('tenantCode');
    const projectCode = this._activatedRoute.snapshot.queryParamMap.get('projectCode');
    if (username && tenantCode && projectCode) {
      this._logger.debug('query params have "' + username + '" "' + tenantCode + '" "' + projectCode + '"');
      const userIdentifier = new UserIdentifier(
        username, tenantCode, projectCode);
      this.getDetail(userIdentifier);
    } else if (userGuid) {
      this._logger.debug('query params have "' + userGuid + '"');
      const userIdentifier = new UserIdentifier();
      userIdentifier.userGuid = userGuid;
      this.getDetail(userIdentifier);
    } else {
      this._remoteUserService.getLoggedInDetail()
        .then(detail => {
          this._id = detail.identifier;
          this.setDetail(detail);
          const tab = this._activatedRoute.snapshot.paramMap.get('tab');
          if (tab) {
            this._logger.debug('Tab is ' + tab);
            this.showTab(tab);
          } else {
            this.showTab(this.linkDefault);
          }
          this._logger.debug('Detail retrieved successfully');
          this.displayState = this.displayStateContent;
          this.setDimensionsListener();
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        });
    }
  }

  private getDetail(userIdentifier: UserIdentifier, silently?: boolean) {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
    this.scrollTop();
    this._id = userIdentifier;

    if (!silently) {
      this.displayState = this.displayStateLoader;
    }
    this._remoteUserService.getDetail(userIdentifier)
      .then(detail => {

        this.setDetail(detail);
        if (!silently) {
          const tab = this._activatedRoute.snapshot.paramMap.get('tab');
          if (tab) {
            this._logger.debug('Tab is ' + tab);
            this.showTab(tab);
          } else {
            this.showTab(this.linkDefault);
          }
        }
        this._logger.debug('Detail retrieved successfully');
        this.displayState = this.displayStateContent;
        this.setDimensionsListener();
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      }).catch(error => {
        this._logger.error('Error while getting detail ', error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      });
  }

  private setDetail(detail: UserDetailModel) {
    if (detail) {
      this.model = detail;
      this._id = detail.identifier;
      this.setModelInComponents(detail);
      this._menuService.setMenuUserDetail(detail);
    }
  }

  /*

  Buttons

  */


  openDataComponent() {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(this.model);
      this.dataComponent.open();
    }
  }

  openUserGroupComponent() {
    if (this.userGroupMembershipComponent) {
      this.userGroupMembershipComponent.setDetailModel(this.model);
      this.userGroupMembershipComponent.open();
    }
  }

  openSecurityComponent() {
    if (this.securityComponent) {
      this.securityComponent.setDetailModel(this.model);
      this.securityComponent.open();
    }
  }

  openClaimsComponent() {
    if (this.claimsComponent) {
      this.claimsComponent.setDetailModel(this.model);
      this.claimsComponent.open();
    }
  }

  openUserTenantMembershipComponent() {
    if (this.userTenantMembershipComponent) {
      this.userTenantMembershipComponent.setDetailModel(this.model);
      this.userTenantMembershipComponent.open();
    }
  }

  openUserGroupManagmentComponent() {
    if (this.userGroupManagementComponent) {
      this.userGroupManagementComponent.setDetailModel(this.model);
      this.userGroupManagementComponent.open();
    }
  }
  onTabChanged(event: MatTabChangeEvent) {
    const tabName = event.tab.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name');
    this.openTab(tabName);
  }

  setModelInComponents(model: UserDetailModel): void {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(model);
    }
    if (this.userGroupMembershipComponent) {
      this.userGroupMembershipComponent.setDetailModel(model);
    }
    if (this.securityComponent) {
      this.securityComponent.setDetailModel(model);
    }
    if (this.claimsComponent) {
      this.claimsComponent.setDetailModel(model);
    }
    if (this.userGroupManagementComponent) {
      this.userGroupManagementComponent.setDetailModel(model);
    }
  }

  showTab(link: string) {
    const tab = this.tabGroup._tabs.find(t => t.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name') === link);
    if (tab) {
      if (this.tabGroup.selectedIndex === tab.position) {
        this.openTab(link);
      }
      this.tabGroup.selectedIndex = tab.position;
      this.tabGroup.realignInkBar();
    } else {
      this.showTab(this.linkDefault);
    }
  }


  openTab(tabName: string) {
    switch (tabName) {
      case this.linkdata: this.openDataComponent();
        break;
      case this.linkUsergroup: this.openUserGroupComponent();
        break;
      case this.linkSecurity: this.openSecurityComponent();
        break;
      case this.linkClaims: this.openClaimsComponent();
        break;
      case this.linkGroupManagement: this.openUserGroupManagmentComponent();
        break;
      case this.linkUserTenantMembership: this.openUserTenantMembershipComponent();
        break;
      default:
        this._logger.error('Defaulting to "' + this.linkDefault + '" as tabName not recognised : "' + tabName + '"');
        this.showTab(this.linkDefault);
        break;
    }
  }

  updateLocation(tab: string) {
    // const current = this._location.path(false);
    // const root = current.substring(0, current.indexOf(this.model.id) + this.model.id.length);
    // const newlink = root + '/' + tab;
    // if (!current.startsWith(newlink)) {
    //   this._location.replaceState(newlink);
    // }
  }

  retry() {
    this.refreshDetail();
  }

  close() {
    this._sweetAlert.close();
    this._router.navigate(['/users/list']);
  }

  refreshDetail() {
    this.getDetail(this._id);
  }
  delete() {
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Confirm,
        title: this._prefixDelete + '.Confirm.Title',
        message: this._prefixDelete + '.Confirm.Message',
        confirmButton: this._prefixDelete + '.Confirm.OkButton',
        cancelButton: this._prefixDelete + '.Confirm.CancelButton',
        successCallback: () => this.deleteConfirmed(),
        cancelCallback: () => this._sweetAlert.close()
      }));
  }

  returnToSite() {
    this._browserStorage.remove(environment.Settings.browserStorageKeys.returnUrl);
    window.location.href = this.returnUrl;
  }
  private deleteConfirmed() {

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: this._prefixDelete + '.Processing',
        message: this._prefixDelete + '.Processing',
        confirmButton: this._prefixDelete + '.Processing'
      }));

    this._remoteUserService.delete(this._id)
      .then(response => {
        this._logger.debug('Deleted');
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: this._prefixDelete + '.Success.Title',
            message: this._prefixDelete + '.Success.Message',
            confirmButton: this._prefixDelete + '.Success.OkButton',
            cancelButton: this._prefixDelete + '.Success.CancelButton',
            successCallback: () => this.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt delete user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: this._prefixDelete + '.Failed.Title',
            message: this._prefixDelete + '.Failed.Message',
            confirmButton: this._prefixDelete + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  private setDimensionsListener() {
    if (!this.setDimensions()) {
      const parent = this;
      const interval = setInterval(() => {
        if (parent.setDimensions()) {
          clearInterval(interval);
        }
      }, 200);
    }
  }

  private setDimensions(): boolean {
    if ($(this.detailCard.nativeElement).is(':visible')) {
      const detailCardWidth = $(this.detailCard.nativeElement).width();
      this.headerWidth = detailCardWidth + 'px';
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      return true;
    }
    return false;
  }


  ngOnInit() {
    this._windowSubscribe = this._windowListener.OnResizeAndMenu()
      .subscribe(() => {
        this.setDimensionsListener();
      });

    // the only way it is triggered if the user changes the user claims in the tab.
    this._userClaimDetailChangeSubscribe = this._remoteUserClaimService.detailChangeEvent()
      .subscribe(identifier => {
        this.getDetail(this._id, true);
      });

    this._detailChangeSubscribe = this._remoteUserService.detailModelChangeEvent()
      .subscribe(detail => {
        this.setDetail(detail);
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      });
    this.scrollTop();
  }

  ngOnDestroy() {

    if (this._detailChangeSubscribe) {
      this._detailChangeSubscribe.unsubscribe();
    }

    if (this._windowSubscribe) {
      this._windowSubscribe.unsubscribe();
    }

    if (this._routerSubscribe) {
      this._routerSubscribe.unsubscribe();
    }

    if (this._locationSubscribe) {
      this._locationSubscribe.unsubscribe();
    }

    if (this._userClaimDetailChangeSubscribe) {
      this._userClaimDetailChangeSubscribe.unsubscribe();
    }
  }
}
