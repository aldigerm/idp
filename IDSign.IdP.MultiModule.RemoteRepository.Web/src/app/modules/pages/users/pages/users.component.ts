import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { HttpErrorResponse } from '@angular/common/http';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { UserListItemModel } from '@models/remote-repository/user/list/remote-repository-user-list';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {

  readonly allColumns: string[] = ['profileImage', 'repositoryUsername', 'projectCode', 'tenantCode', 'username', 'firstName'
    , 'lastName', 'status', 'actions', 'userIdentifier'];
  readonly desktopColumns: string[] = ['profileImage', 'projectCode', 'tenantCode', 'username'
    , 'firstName', 'lastName', 'status', 'actions'];
  readonly tabletColumns: string[] = ['profileImage', 'projectCode', 'tenantCode', 'username', 'firstName', 'lastName', 'status'];
  readonly mobileColumns: string[] = ['profileImage', 'tenantCode', 'username'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode',
    'tenantCode': 'identifier.tenantCode',
    'username': 'identifier.username'
  };


  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;
  private _tenantIdentifier = <TenantIdentifier>{};

  listModel = new Array<UserListItemModel>();
  tableSuffix = '-page-users';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;

  @ViewChildren('userRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _userService: RemoteRepositoryUserService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('usersComponent');
    this.listModel = new Array<UserListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    const parent = this;
    parent.refreshList();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true, this._tenantIdentifier);
  }

  getList(changeDisplayStates: boolean, tenantIdentifier: TenantIdentifier) {
    if (!tenantIdentifier) {
      tenantIdentifier = new TenantIdentifier(null, null);
    }
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }

    this._userService.getPageList(tenantIdentifier)
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch((error: HttpErrorResponse) => {
        const errorCode = error && error.error ? error.error.ErrorCode : undefined;
        this._logger.error('Couldnt get user list; ' + errorCode, error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/users/create']);
    this.scrollTop();
  }


  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
