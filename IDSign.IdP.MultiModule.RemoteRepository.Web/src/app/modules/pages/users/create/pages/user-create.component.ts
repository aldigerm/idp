import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserCreateModel } from '@models/remote-repository/user/remote-repository-user-create';
import { UserService } from '@http/user/user.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { UserIdentifier } from '@app/shared/models/identifiers/user-identifier';
import { tenantValidator } from '@app/sharedvalidators/identifier.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];

  private _createSubscribe: ISubscription;

  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: true }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryUserService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService
  ) {
    this._logger = this._logger.createInstance('UserCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    if (this.form) {
      this.form.reset();
    } else {
      this.form = this._fb.group({
        username: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        firstName: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        lastName: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        password: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        emailAddress: this._fb.control(''),
        userGroups: this._fb.control([]),
        profileImage: this._fb.control(''),
        setPasswordOnFirstLogin: this._fb.control(false),
        tenantIdentifier: this._fb.control(<TenantIdentifier>{}, [tenantValidator()])
      });

      this.form.get('tenantIdentifier').valueChanges.subscribe(
        (value: TenantIdentifier) => {
          this.updateUserGroups(value);
        });
    }
  }

  updateUserGroups(tenantIdentifier: TenantIdentifier) {
    if (tenantIdentifier) {
      this._userService.getUser().then(user => {
        if (user && user.userAccessDetail && user.userAccessDetail.tenants && tenantIdentifier) {
          const tenant = user.userAccessDetail.tenants
            .find(t => tenantIdentifier && t.identifier.projectCode === tenantIdentifier.projectCode
              && t.identifier.tenantCode === tenantIdentifier.tenantCode);
          if (tenant) {
            const options = new Array<MultiCheckboxOption>();
            tenant.userGroupsInferred.forEach(ug => {
              const option = <MultiCheckboxOption>{};
              option.id = ug;
              option.name = ug;
              option.value = ug;
              options.push(option);
            });
            this.options = options;
          }
          if (this.form) {
            this.form.get('userGroups').setValue([]);
          } else {
            this.form = this._fb.group({
              userGroups: this._fb.control([]),
            });
          }
        }
      }).catch(error => {
        this._logger.error('Couldnt initialise project dropdown', error);
      });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/users/list']);
  }

  closeAndOpenDetail(id: UserIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/users/detail'],
      {
        queryParams: {
          projectCode: id.projectCode,
          tenantCode: id.tenantCode,
          username: id.username
        }
      });
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotheruser() {
    this.initForm();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserCreateModel>{};
      model = this.form.value;
      model.enabled = true;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'User.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(identifier => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotheruser()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
