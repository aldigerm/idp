import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { UsersComponent } from './pages/users.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';
import { ResponsiveTableModule } from '@app/shareddirectives/table/responsive-table.directive.module';
import { UserDetailButtonModule } from '@app/sharedcontrols/buttons/user-detail-button/user-detail-button.module';
import { ProfilePictureControl } from '@app/sharedcontrols/profile-picture/profile-picture-control.module';
import { ProfilePictureModule } from '@app/sharedcomponents/profile-picture/profile-picture.module';

const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'users', url: './users' }]
  },
  component: UsersComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    DateTimeFormatPipeModule,
    SpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    AccessControlPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    MaterialsModule,
    ResponsiveTableModule,
    UserDetailButtonModule,
    ProfilePictureModule
  ],
  declarations: [
    UsersComponent
  ]
})
export class UsersModule { }
