import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, Input, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { tenantValidator, projectValidator } from '@app/sharedvalidators/identifier.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { UserClaimDetailModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { RemoteRepositoryUserClaimService } from '@http/remote-repository/user-claim/remote-repository-user-claim.service';
import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { exactSelectionRequiredValidator } from '@app/sharedvalidators/selection-required.validator';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { UserClaimCreateModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-create';

@Component({
  selector: 'app-user-claim-create',
  templateUrl: './user-claim-create.component.html',
  styleUrls: ['./user-claim-create.component.css']
})
export class UserClaimCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;
  usersOptions: MultiCheckboxOption[] = [];
  typesOptions: MultiCheckboxOption[] = [];
  private _userIdentifier: UserIdentifier;

  private _createSubscribe: ISubscription;
  readonly = false;

  @Input() set userIdentifier(value: UserIdentifier) {
    this._userIdentifier = value;
    if (value) {
      this.readonly = true;
      this.initForm();
    }
  } get userIdentifier(): UserIdentifier {
    return this._userIdentifier;
  }
  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: true }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryUserClaimService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this._logger = this._logger.createInstance('UserCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {


    if (this.form) {
      this.form.get('type').setValue([]);
      this.form.get('value').setValue('');
      this.form.get('username').setValue((this.userIdentifier && this.userIdentifier.username) || '');
      this.form.get('tenantIdentifier').setValue(this.userIdentifier || <TenantIdentifier>{});
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    } else {
      this.form = this._fb.group({
        type: this._fb.control([]),
        value: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        username: this._fb.control((this.userIdentifier && this.userIdentifier.username) || '', [exactSelectionRequiredValidator(1)]),
        tenantIdentifier: this._fb.control(this.userIdentifier || <TenantIdentifier>{}, [tenantValidator()])
      });

      this.form.get('tenantIdentifier').valueChanges.subscribe(
        (value: TenantIdentifier) => {
          this.updateList(value);
        });
    }
  }

  updateList(tenantIdentifier: TenantIdentifier) {
    if (tenantIdentifier) {

      this._userService.getUser().then(user => {
        if (user && user.userAccessDetail && user.userAccessDetail.users && tenantIdentifier) {
          const users = user.userAccessDetail.users.filter(u => tenantIdentifier && u.projectCode === tenantIdentifier.projectCode
            && u.tenantCode === tenantIdentifier.tenantCode);
          this.usersOptions = [];
          if (users && users.length > 0) {
            users.map(u => {
              const option = <MultiCheckboxOption>{};
              option.id = u.username;
              option.name = u.username;
              option.value = u.username;
              this.usersOptions.push(option);
            });
          }

          const project = user.userAccessDetail.projects.find(t =>
            t.identifier.projectCode === tenantIdentifier.projectCode);
          this.typesOptions = [];
          if (project && project.userClaimTypesDetail && project.userClaimTypesDetail.length > 0) {
            project.userClaimTypesDetail.forEach(claim => {
              const option = <MultiCheckboxOption>{};
              option.id = claim.identifier.type;
              option.name = claim.identifier.type + ' (' + claim.description + ')';
              option.value = claim.identifier;
              this.typesOptions.push(option);
            });

            if (this.form) {
              this.form.get('username').setValue([]);
              this.form.get('type').setValue([]);
            }
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      }).catch(error => {
        this._logger.error('Couldnt initialise list', error);
      });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/userclaims/list']);
  }

  closeAndOpenDetail(id: UserClaimIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/userclaims/detail', id.projectCode
      , id.projectCode
      , id.username
      , id.type
      , id.value]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotherUserClaim() {
    this.form.reset();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {

    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserClaimCreateModel>{};
      model = this.form.value;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'UserClaim.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(identifier => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotherUserClaim()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
