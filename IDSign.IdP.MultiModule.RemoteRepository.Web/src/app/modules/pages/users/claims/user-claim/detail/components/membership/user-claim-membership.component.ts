import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryUserClaimService } from '@http/remote-repository/user-claim/remote-repository-user-claim.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { UserClaimDetailModel } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim';
import { UserClaimSetUsers } from '@models/remote-repository/user/claims/user-claim/remote-repository-user-claim-set-users';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import * as _ from 'lodash';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';
@Component({
  selector: 'app-user-claim-membership',
  templateUrl: './user-claim-membership.component.html',
  styleUrls: ['./user-claim-membership.component.css']
})
export class UserClaimMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: UserClaimDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserClaimService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _router: Router,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserClaimDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: UserIdentifier[] = [];
    if (this.model && this.model.usersWithSameClaim) {
      this.model.usersWithSameClaim.map(
        u => {
          selected.push(u);
        }
      );
    }
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.users && this.model) {
        const users = user.userAccessDetail.users.filter(u => this.model && u.projectCode === this.model.identifier.projectCode
          && u.tenantCode === this.model.identifier.tenantCode);
        if (users) {
          users.map(u => {
            const option = <MultiCheckboxOption>{};
            option.id = u;
            option.name = u.username + '(' + u.tenantCode + ' : ' + u.projectCode + ')';
            option.value = u;
            options.push(option);
          });
        }
        this.options = options;

        if (this.form) {
          this.form.get('users').setValue(selected || []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise users list', error);
    });
    if (this.form) {
      this.form.get('users').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        users: this._fb.control(selected || [])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const model = <UserClaimSetUsers>{};
      model.users = [];
      this.form.value.users.map(u => {
        const user = <UserIdentifier>{};
        user.username = u.username;
        user.tenantCode = u.tenantCode;
        user.projectCode = u.projectCode;
        model.users.push(user);
      });

      // check if the current user is also selected to be removed
      const showWarning = model.users.find(user => _.isMatch(this.model.identifier, user)) ? false : true;

      model.identifier = <UserClaimIdentifier>{};
      model.identifier = this.model.identifier;

      if (showWarning) {
        const prefix = 'UserClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Confirm,
            title: prefix + '.Confirm.Title',
            message: prefix + '.Confirm.Message',
            confirmButton: prefix + '.Confirm.OkButton',
            cancelButton: prefix + '.Confirm.CancelButton',
            successCallback: () => this.performUpdate(model, true),
            cancelCallback: () => this._sweetAlert.close()
          }));
      } else {
        this.performUpdate(model, false);
      }
    }

  }

  private performUpdate(model: UserClaimSetUsers, closeOnReady: boolean) {
    const current = this;
    this._logger.debug('To update : ' + JSON.stringify(model));

    const prefix = 'UserClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));


    this._remoteUserService.setUserMembership(model)
      .then(response => {
        if (closeOnReady) {
          this._router.navigate(['/userclaims/list']);
        }
        current._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt update user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }
}
