import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryUserClaimTypeService } from '@http/remote-repository/user-claim-type/remote-repository-user-claim-type.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { UserClaimTypeDetailModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type';
import { UserClaimTypeSetUsers } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-set-users';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { UserIdentifier } from '@models/identifiers/user-identifier';

@Component({
  selector: 'app-user-claim-type-membership',
  templateUrl: './user-claim-type-membership.component.html',
  styleUrls: ['./user-claim-type-membership.component.css']
})
export class UserClaimTypeMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: UserClaimTypeDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserClaimTypeService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserClaimTypeDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: UserIdentifier[] = [];
    if (this.model && this.model.users) {
      this.model.users.map(
        u => {
          selected.push(u.identifier);
        }
      );
    }
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.users && this.model) {
        const users = user.userAccessDetail.users
          .filter(u => this.model.identifier && u.projectCode === this.model.identifier.projectCode);

        if (users) {
          users.map(u => {
            const option = <MultiCheckboxOption>{};
            option.id = u;
            option.name = u.username + '(' + u.tenantCode + ' : '
              + u.projectCode + ')';
            option.value = u;
            options.push(option);
          });
        }

        this.options = options;
        if (this.form) {
          this.form.get('users').setValue(selected || []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise users list', error);
    });
    if (this.form) {
      this.form.get('users').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        users: this._fb.control(selected || []),
        value: this._fb.control('', [Validators.required, noWhitespaceValidator])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserClaimTypeSetUsers>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'UserClaimType.Detail.Tabs.Membership.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.setUserMembership(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

}
