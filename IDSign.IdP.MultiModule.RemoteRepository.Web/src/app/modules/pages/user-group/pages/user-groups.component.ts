import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { UserListPageModel } from '@models/remote-repository/user/list/remote-repository-user-page';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserBasicModel } from '@models/remote-repository/user/remote-repository-user-basic';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { UserGroupBasicModel } from '@models/remote-repository/user-group/remote-repository-user-group-basic';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { UserGroupListItemModel } from '@models/remote-repository/user-group/list/remote-repository-user-group-list';

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class UserGroupsComponent implements OnInit, AfterViewInit, OnDestroy {


  readonly allColumns: string[] = ['userGroupCode', 'projectCode', 'tenantCode', 'description', 'actions'];
  readonly desktopColumns: string[] = ['userGroupCode', 'projectCode', 'tenantCode', 'description', 'actions'];
  readonly tabletColumns: string[] = ['userGroupCode', 'projectCode', 'tenantCode', 'actions'];
  readonly mobileColumns: string[] = ['userGroupCode', 'projectCode'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode',
    'tenantCode': 'identifier.tenantCode',
    'userGroupCode': 'identifier.userGroupCode'
  };

  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;
  private _tenantIdentifier = <TenantIdentifier>{};

  listModel: Array<UserGroupListItemModel>;
  tableSuffix = '-page-users';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;

  @ViewChildren('userRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _userGroupService: RemoteRepositoryUserGroupService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('usersComponent');
    this.listModel = new Array<UserGroupListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    const parent = this;
    parent.refreshList();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true, this._tenantIdentifier);
  }

  getList(changeDisplayStates: boolean, tenantIdentifier: TenantIdentifier) {
    if (!tenantIdentifier) {
      tenantIdentifier = new TenantIdentifier(null, null);
    }
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }
    this._userGroupService.getPageList()
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch((error: HttpErrorResponse) => {
        const errorCode = error && error.error ? error.error.ErrorCode : undefined;
        this._logger.error('Couldnt get user list; ' + errorCode, error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/usergroups/create']);
    this.scrollTop();
  }

  closeCreate() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  closeDetail() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  clickItemDetail(event: Event, idAttribute: string, tenantCodeAttribute: string, projectCodeAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const userGroupCode = button.getAttribute(idAttribute);
    const tenantCode = button.getAttribute(tenantCodeAttribute);
    const projectCode = button.getAttribute(projectCodeAttribute);
    const id = new UserGroupIdentifier(userGroupCode, tenantCode, projectCode);
    this._logger.debug('Id to get is ' + JSON.stringify(id));
    this.getDetailAndShow(id);
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  refreshAndShowDetail(id: UserGroupIdentifier) {
    this.getList(false, this._tenantIdentifier);
    this.getDetailAndShow(id);
  }

  private getDetailAndShow(id: UserGroupIdentifier) {
    this._router.navigate(['/usergroups/detail', id.projectCode,
      id.tenantCode, id.userGroupCode]);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
