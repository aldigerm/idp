import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { UserService } from '@http/user/user.service';
import { UserGroupDetailModel } from '@models/remote-repository/user-group/remote-repository-user-group-detail';
import { UserGroupSetPasswordPolicies } from '@models/remote-repository/user-group/remote-repository-user-group-set-password-polcies';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-user-group-password-policies',
  templateUrl: './user-group-password-policies.component.html',
  styleUrls: ['./user-group-password-policies.component.css']
})
export class UserGroupPasswordPoliciesComponent implements OnInit {

  faBolt = faBolt;
  model: UserGroupDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserGroupService: RemoteRepositoryUserGroupService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('PasswordPolicyInheritanceComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserGroupDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenant = user.userAccessDetail.tenants
          .find(t => t.identifier.projectCode === this.model.identifier
            .projectCode
            && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenant) {
          tenant.passwordPolicies.map(pp => {
            const option = <MultiCheckboxOption>{};
            option.id = pp;
            option.name = pp;
            option.value = pp;
            options.push(option);
          });
        }
        this.options = options;
        if (this.form) {
          this.form.get('passwordPolicies').setValue(this.model ? this.model.passwordPolicies : []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise project dropdown', error);
    });
    if (this.form) {
      this.form.get('passwordPolicies').setValue(this.model ? this.model.passwordPolicies : []);
    } else {
      this.form = this._fb.group({
        passwordPolicies: this._fb.control(this.model ? this.model.passwordPolicies : []),
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserGroupSetPasswordPolicies>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'UserGroup.Detail.Tabs.PasswordPolicies.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserGroupService.setPasswordPolicies(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

}
