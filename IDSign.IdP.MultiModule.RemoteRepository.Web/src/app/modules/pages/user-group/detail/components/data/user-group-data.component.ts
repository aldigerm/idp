import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryUserGroupService } from '@http/remote-repository/user-group/remote-repository-user-group.service';
import { UserService } from '@http/user/user.service';
import { UserGroupDetailModel } from '@models/remote-repository/user-group/remote-repository-user-group-detail';
import { UserGroupUpdateModel } from '@models/remote-repository/user-group/remote-repository-user-group-update';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SiteActionControlService } from '@services/site-action-control.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { SubscriptionLike as ISubscription } from 'rxjs';

@Component({
  selector: 'app-user-group-data',
  templateUrl: './user-group-data.component.html',
  styleUrls: ['./user-group-data.component.css']
})
export class UserGroupDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: UserGroupDetailModel;
  form: FormGroup;
  canUpdateData = false;
  canSetpassword = false;

  private _updateSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryUserGroupService,
    private _userService: UserService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _userActionControl: SiteActionControlService
  ) {
    this._logger = this._logger.createInstance('userDataComponent');
    this.setDetailModel(<UserGroupDetailModel>{});
  }

  public setDetailModel(model: UserGroupDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }


  initForm() {

    if (this.form) {
      this.form.get('tenantIdentifier').setValue(this.model.identifier);
      this.form.get('userGroupCode').setValue(this.model.identifier && this.model.identifier.userGroupCode);
      this.form.get('description').setValue(this.model.description);
    } else {
      this.form = this._fb.group({
        tenantIdentifier: this._fb.control(this.model.identifier, [Validators.required]),
        userGroupCode: this._fb.control({ value: this.model.identifier && this.model.identifier.userGroupCode, disabled: true }),
        description: this._fb.control(this.model.description, [Validators.required, noWhitespaceValidator])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <UserGroupUpdateModel>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'UserGroup.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
