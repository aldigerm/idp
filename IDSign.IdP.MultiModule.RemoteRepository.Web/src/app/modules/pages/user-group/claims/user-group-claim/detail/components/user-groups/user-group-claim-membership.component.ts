import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryUserGroupClaimService } from '@http/remote-repository/user-group-claim/remote-repository-user-group-claim.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { UserGroupClaimDetailModel } from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim';
import {
  UserGroupClaimSetUserGroups
} from '@models/remote-repository/user-group/claims/user-group-claim/remote-repository-user-group-claim-set-user-groups';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import * as _ from 'lodash';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';
@Component({
  selector: 'app-user-group-claim-membership',
  templateUrl: './user-group-claim-membership.component.html',
  styleUrls: ['./user-group-claim-membership.component.css']
})
export class UserGroupClaimMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: UserGroupClaimDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserGroupService: RemoteRepositoryUserGroupClaimService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _router: Router,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: UserGroupClaimDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: UserGroupIdentifier[] = [];
    if (this.model && this.model.userGroupsWithSameClaim) {
      this.model.userGroupsWithSameClaim.map(
        u => {
          selected.push(u);
        }
      );
    }
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenants = user.userAccessDetail.tenants.filter(t => this.model
          && t.identifier.projectCode === this.model.identifier.projectCode
          && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenants) {
          tenants.map(t => {
            t.userGroups.map(ug => {
              const option = <MultiCheckboxOption>{};
              const id = new UserGroupIdentifier(ug, t.identifier.tenantCode, t.identifier.projectCode);
              option.id = id;
              option.name = ug + '(' + t.identifier.tenantCode + ' : ' + t.identifier.projectCode + ')';
              option.value = id;
              options.push(option);
            });
          });
        }
        this.options = options;

        if (this.form) {
          this.form.get('userGroups').setValue(selected || []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise userGroups list', error);
    });
    if (this.form) {
      this.form.get('userGroups').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        userGroups: this._fb.control(selected || [])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const model = <UserGroupClaimSetUserGroups>{};
      model.userGroups = [];
      this.form.value.userGroups.map(u => {
        const user = <UserGroupIdentifier>{};
        user.userGroupCode = u.userGroupCode;
        user.tenantCode = u.tenantCode;
        user.projectCode = u.projectCode;
        model.userGroups.push(user);
      });

      // check if the current user is also selected to be removed
      const showWarning = model.userGroups.find(user => _.isMatch(this.model.identifier, user)) ? false : true;

      model.identifier = <UserGroupClaimIdentifier>{};
      model.identifier = this.model.identifier;

      if (showWarning) {
        const prefix = 'UserGroupClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Confirm,
            title: prefix + '.Confirm.Title',
            message: prefix + '.Confirm.Message',
            confirmButton: prefix + '.Confirm.OkButton',
            cancelButton: prefix + '.Confirm.CancelButton',
            successCallback: () => this.performUpdate(model, true),
            cancelCallback: () => this._sweetAlert.close()
          }));
      } else {
        this.performUpdate(model, false);
      }
    }

  }

  private performUpdate(model: UserGroupClaimSetUserGroups, closeOnReady: boolean) {
    const current = this;
    this._logger.debug('To update : ' + JSON.stringify(model));

    const prefix = 'UserGroupClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));


    this._remoteUserGroupService.setUserGroupMembership(model)
      .then(response => {
        if (closeOnReady) {
          this._router.navigate(['/usergroupclaims/list']);
        }
        current._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt update user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }
}
