import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';
import { PasswordControl } from '@app/sharedcontrols/password/password.module';
import { ProjectSelectorModule } from '@app/sharedcontrols/project-selector/project-selector.module';
import { UserGroupClaimCreateComponent } from './pages/user-group-claim-create.component';
import { TenantSelectorControl } from '@app/sharedcontrols/tenant-selector/tenant-selector.module';
import { InternalMultiCheckboxControl } from '@app/sharedcontrols/multi-checkbox/int-multi-checkbox.module';
import { InternalDropdownControl } from '@app/sharedcontrols/dropdown/int-dropdown.module';
import { SpinnerModule } from '@app/sharedcomponents/spinner/spinner.module';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';
import { ResponsiveTableModule } from '@app/shareddirectives/table/responsive-table.directive.module';
import {
  UserGroupClaimDetailButtonModule
} from '@app/sharedcontrols/buttons/user-group-claim-detail-button/user-group-claim-detail-button.module';
import { InternalCheckboxControl } from '@app/sharedcontrols/checkbox/int-checkbox.module';
import { ProfilePictureControl } from '@app/sharedcontrols/profile-picture/profile-picture-control.module';
import { InternalLabelControl } from '@app/sharedcontrols/label/int-label.module';
import { AccessControlModule } from '@app/shareddirectives/access-control/access-control.directive.module';

const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'user', url: './users' }]
  },
  component: UserGroupClaimCreateComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    DateTimeFormatPipeModule,
    SpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    MaterialsModule,
    ResponsiveTableModule,
    InternalTextBoxControl,
    TenantSelectorControl,
    PasswordControl,
    InternalMultiCheckboxControl,
    InternalDropdownControl,
    UserGroupClaimDetailButtonModule,
    InternalCheckboxControl,
    ProfilePictureControl,
    InternalLabelControl,
    AccessControlModule
  ],
  declarations: [
    UserGroupClaimCreateComponent
  ]
})
export class UserGroupClaimCreateModule { }
