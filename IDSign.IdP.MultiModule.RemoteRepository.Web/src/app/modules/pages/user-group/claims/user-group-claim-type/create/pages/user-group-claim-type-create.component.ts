import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { tenantValidator, projectValidator } from '@app/sharedvalidators/identifier.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import {
  UserGroupClaimTypeDetailModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import {
  RemoteRepositoryUserGroupClaimTypeService
} from '@http/remote-repository/user-group-claim-type/remote-repository-user-group-claim-type.service';
import {
  UserGroupClaimTypeIdentifier
} from '@models/identifiers/user-group-claim-type-identifier';
import {
  UserGroupClaimTypeCreateModel
} from '@models/remote-repository/user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type-create';

@Component({
  selector: 'app-user-group-claim-type-create',
  templateUrl: './user-group-claim-type-create.component.html',
  styleUrls: ['./user-group-claim-type-create.component.css']
})
export class UserGroupClaimTypeCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;

  private _createSubscribe: ISubscription;

  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: false }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteService: RemoteRepositoryUserGroupClaimTypeService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService
  ) {
    this._logger = this._logger.createInstance('UserGroupCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {
    if (this.form) {
      this.form.get('projectIdentifier').setValue(<ProjectIdentifier>{});
      this.form.get('type').setValue('');
      this.form.get('description').setValue('');
    } else {
      this.form = this._fb.group({
        type: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        description: this._fb.control(''),
        projectIdentifier: this._fb.control(<ProjectIdentifier>{})
      });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/userclaimtypes/list']);
  }

  closeAndOpenDetail(id: UserGroupClaimTypeIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/userclaimtypes/detail', id.projectCode, id.type]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotherUserGroupClaimType() {
    this.initForm();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model: UserGroupClaimTypeCreateModel;
      model = this.form.value;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'UserGroupClaimType.Create.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteService.create(model)
        .then(identifier => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotherUserGroupClaimType()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
