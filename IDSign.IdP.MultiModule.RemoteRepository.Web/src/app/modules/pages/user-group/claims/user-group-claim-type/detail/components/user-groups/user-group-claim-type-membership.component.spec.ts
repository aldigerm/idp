/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { UserGroupClaimTypeMembershipComponent } from './user-group-claim-type-membership.component';

describe('UserGroupClaimTypeMembershipComponent', () => {
  let component: UserGroupClaimTypeMembershipComponent;
  let fixture: ComponentFixture<UserGroupClaimTypeMembershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserGroupClaimTypeMembershipComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGroupClaimTypeMembershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
