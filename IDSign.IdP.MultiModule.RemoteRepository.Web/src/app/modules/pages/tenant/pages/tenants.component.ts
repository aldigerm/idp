import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { TenantListPageModel } from '@models/remote-repository/tenant/list/remote-repository-tenant-page';
import { RemoteRepositoryTenantService } from '@http/remote-repository/tenant/remote-repository-tenant.service';
import { TenantBasicModel } from '@models/remote-repository/tenant/remote-repository-tenant-basic';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectIdentifier } from '@app/shared/models/identifiers/project-identifier';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { TenantListItemModel } from '@models/remote-repository/tenant/list/remote-repository-tenant-list';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class TenantsComponent implements OnInit, AfterViewInit, OnDestroy {

  readonly allColumns: string[] = ['name', 'projectCode', 'tenantCode', 'status', 'actions'];
  readonly desktopColumns: string[] = ['name', 'projectCode', 'tenantCode', 'status', 'actions'];
  readonly tabletColumns: string[] = ['name', 'projectCode', 'tenantCode', 'status'];
  readonly mobileColumns: string[] = ['name'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode',
    'tenantCode': 'identifier.tenantCode'
  };

  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  form: FormGroup;
  readonly displayStateNone = '';
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateNone;
  private _changeDisplayStates = true;

  listModel: Array<TenantListItemModel>;
  tableSuffix = '-page-tenants';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;
  private _projectIdentifier = <ProjectIdentifier>{};

  @ViewChildren('tenantRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _tenantService: RemoteRepositoryTenantService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _fb: FormBuilder,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('tenantsComponent');
    this.listModel = new Array<TenantListItemModel>();
  }

  ngOnInit() {
    this.initForm();
  }

  ngAfterViewInit() {

    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    if (this.form.value.projectIdentifier) {
      this.getList(true);
    }
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }
  initForm() {
    this.form = this._fb.group({
      projectIdentifier: this._fb.control(<ProjectIdentifier>{}, [Validators.required])
    });
    this.form.get('projectIdentifier').valueChanges.subscribe((value: ProjectIdentifier) => {
      this.getList(true, value);
    });
    this._sectionPreloaderService.hide();
  }
  refreshList() {
    this.getList(true);
  }

  getList(changeDisplayStates: boolean, projectIdentifier?: ProjectIdentifier) {
    if (!projectIdentifier) {
      projectIdentifier = this.form.value.projectIdentifier;
    }
    if (!projectIdentifier || !projectIdentifier.projectCode) {
      this.displayState = this.displayStateNone;
    } else {
      this._projectIdentifier = projectIdentifier;
      this._logger.debug('Refresh list');
      if (changeDisplayStates) {
        this.displayState = this.displayStateLoader;
        this._changeDisplayStates = true;
      } else {
        this._changeDisplayStates = false;
      }
      this._tenantService.getPageList(projectIdentifier)
        .then(data => {
          this.listModel = data;
          if (this.listModel.length === 0) {
            if (changeDisplayStates) {
              this.displayState = this.displayStateNoneFound;
            }
          }
          this._siteChangeDetector.detectChanges(this._changeDetectorRef);
          this._pagePreloaderService.hide();
        }).catch(error => {
          this.displayState = this.displayStateError;
          this._siteChangeDetector.detectChanges(this._changeDetectorRef);
          this._pagePreloaderService.hide();
        });
    }
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/tenants/create'], { queryParams: { projectCode: this._projectIdentifier.projectCode } });
    this.scrollTop();
  }

  closeCreate() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  closeDetail() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  clickItemDetail(event: Event, idAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const id = button.getAttribute(idAttribute);
    this._logger.debug('Tenant to get is ' + id);
    this.getDetailAndShow(id);
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  refreshAndShowDetail(id: string) {
    this.getList(false);
    this.getDetailAndShow(id);
  }

  private getDetailAndShow(id: string) {
    this._router.navigate(['/tenants/detail', this.form.value.projectIdentifier.projectCode, id]);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
