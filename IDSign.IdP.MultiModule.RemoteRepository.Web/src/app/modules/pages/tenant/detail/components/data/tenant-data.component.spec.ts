/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TenantDataComponent } from './tenant-data.component';

describe('TenantDataComponent', () => {
  let component: TenantDataComponent;
  let fixture: ComponentFixture<TenantDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TenantDataComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
