import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { faPlus, faBolt } from '@fortawesome/free-solid-svg-icons';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { ErrorCode } from '@models/error-code';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { environment } from '@environments/environment';
import { TenantDetailModel } from '@models/remote-repository/tenant/remote-repository-tenant-detail';
import { SiteActionControlService } from '@services/site-action-control.service';
import { TenantUpdateModel } from '@models/remote-repository/tenant/remote-repository-tenant-update';
import { RemoteRepositoryTenantService } from '@http/remote-repository/tenant/remote-repository-tenant.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';

@Component({
  selector: 'app-tenant-data',
  templateUrl: './tenant-data.component.html',
  styleUrls: ['./tenant-data.component.css']
})
export class TenantDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model = <TenantDetailModel>{};
  form: FormGroup;

  private _createSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _tenantService: RemoteRepositoryTenantService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _tenantActionControl: SiteActionControlService
  ) {
    this._logger = this._logger.createInstance('TenantDataComponent');
  }

  public setDetailModel(model: TenantDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }


  initForm() {

    if (this.form) {
      this.form.setValue({
        tenantIdentifier: this.model.identifier,
        tenantName: this.model.name,
        enabled: this.model.enabled
      });
      if (this.model.loggedInUserTenant) {
        this.form.get('enabled').disable();
      } else {
        this.form.get('enabled').enable();
      }
    } else {
      this.form = this._fb.group({
        tenantIdentifier: [this.model.identifier, [Validators.required]],
        tenantName: [this.model.name, [Validators.required, noWhitespaceValidator]],
        enabled: this._fb.control({ value: this.model.enabled, disabled: this.model.loggedInUserTenant })
      });
    }
  }

  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      const model = <TenantUpdateModel>{};
      model.name = this.form.value.tenantName;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'Tenant.Detail.Tabs.Data.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._tenantService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
