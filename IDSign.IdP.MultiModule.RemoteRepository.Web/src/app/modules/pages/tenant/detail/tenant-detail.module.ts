import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { FeatureControlPipeModule } from '@app/shared/pipes/control/feature-control/feature-control.pipe.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TreeModule } from 'angular-tree-component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatRadioModule } from '@angular/material/radio';
import { TimeFormatPipeModule } from '@app/shared/pipes/calendar/time/time-format.pipe.module';
import { TenantDetailComponent } from './pages/tenant-detail.component';
import { TenantDataComponent } from './components/data/tenant-data.component';
import { InternalTextBoxControl } from '@app/shared/controls/text-control/int-text-box.module';
import { TenantSelectorControl } from '@app/shared/controls/tenant-selector/tenant-selector.module';
import { InternalCheckboxControl } from '@app/sharedcontrols/checkbox/int-checkbox.module';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';

const routes: Routes = [{
  path: '',
  data: {
    state: 'tenants',
    title: 'tenants',
    urls: [{ title: 'tenant', url: './tenant' }]
  },
  component: TenantDetailComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    SpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateTimeFormatPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    ReactiveFormsModule,
    MultiselectDropdownModule,
    TreeModule,
    NgScrollbarModule,
    FontAwesomeModule,
    MatRadioModule,
    FeatureControlPipeModule,
    TimeFormatPipeModule,
    InternalTextBoxControl,
    TenantSelectorControl,
    InternalCheckboxControl,
    MaterialsModule
  ],
  declarations: [
    TenantDetailComponent,
    TenantDataComponent
  ]
})
export class TenantDetailModule { }
