import { MenuService } from '@services/menu.service';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { LocationService } from '@services/location.service';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild, Input } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { faTimesCircle, faTrash } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { TenantClaimDataComponent } from '../components/data/tenant-claim-data.component';
import { UserService } from '@http/user/user.service';
import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import { MatTabGroup, MatTabChangeEvent } from '@angular/material';
import { TenantClaimMembershipComponent } from '../components/membership/tenant-claim-membership.component';
import { RemoteRepositoryTenantClaimService } from '@http/remote-repository/tenant-claim/remote-repository-tenant-claim.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { filter } from 'rxjs/operators';
import { TenantClaimDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim';
import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';
declare const $: any;

@Component({
  selector: 'app-tenant-claim-detail',
  templateUrl: './tenant-claim-detail.component.html',
  styleUrls: ['./tenant-claim-detail.component.css'],
  animations: [SlideBoolAnimation]
})
export class TenantClaimDetailComponent implements OnInit, OnDestroy {
  faTimesCircle = faTimesCircle;
  faSync = faSync;
  faTrash = faTrash;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateError = 'Error';

  readonly linkinheritance = 'inheritance';
  readonly linkdata = 'data';
  readonly linkMembership = 'membership';
  readonly linkDefault = this.linkdata;

  displayState = this.displayStateLoader;

  private _detailSubscribe: ISubscription;
  private _detailChangeSubscribe: ISubscription;
  private _windowSubscribe: ISubscription;
  private _locationSubscribe: ISubscription;
  private _routerSubscribe: ISubscription;
  private _deleteSubscribe: ISubscription;
  private _isDetailOfCurrentUser = false;
  private readonly _prefixDelete = 'TenantClaim.Detail.Actions.Delete.Popup';

  model: TenantClaimDetailModel;
  headerWidth = '400px';
  private _id = <TenantClaimIdentifier>{};
  @Input() set tenantClaimIdentifier(value: TenantClaimIdentifier) {
    if (value) {
      this.getDetail(value);
    }
  }
  @Output() closeComponent = new EventEmitter<any>();
  @ViewChild('detailCard', { static: true }) detailCard: ElementRef;
  @ViewChild(MatTabGroup, { static: true }) tabGroup: MatTabGroup;
  @ViewChild(TenantClaimDataComponent, { static: false }) dataComponent: TenantClaimDataComponent;
  @ViewChild(TenantClaimMembershipComponent, { static: false }) membershipComponent: TenantClaimMembershipComponent;
  constructor(
    private _remoteTenantClaimService: RemoteRepositoryTenantClaimService,
    private _userService: UserService,
    private _logger: LoggerService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _location: LocationService,
    private _windowListener: WindowEventListenerService,
    private _scroller: ScrollService,
    private _menuService: MenuService,
    private _sweetAlert: SweetAlertService
  ) {
    this._logger = this._logger.createInstance('TenantClaimDetailComponent');
    this.displayState = this.displayStateLoader;
    this.model = <TenantClaimDetailModel>{};

    this._routerSubscribe = this._router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (event.url.startsWith('/tenantclaims/')) {
          this._logger.debug('Router navigation for user group detail detected.');
          this.init();
        }
      });
  }

  refresh() {
    this.retry();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }


  private init() {
    const projectCode = this._activatedRoute.snapshot.paramMap.get('projectCode');
    const type = this._activatedRoute.snapshot.paramMap.get('type');
    const value = this._activatedRoute.snapshot.paramMap.get('value');
    const tenantCode = this._activatedRoute.snapshot.paramMap.get('tenantCode');
    if (type && projectCode) {
      this._logger.debug('query params have "' + projectCode + '" "' + type + '"');
      const id = new TenantClaimIdentifier(tenantCode, projectCode, type, value);
      this.getDetail(id);
    } else {
      this.displayState = this.displayStateError;
      this._pagePreloader.hide();
      this._sectionPreloader.hide();
    }
  }

  private getDetail(tenantClaimTypeIdentifier: TenantClaimIdentifier) {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
    this.scrollTop();
    this._id = tenantClaimTypeIdentifier;
    if (!(tenantClaimTypeIdentifier
      && tenantClaimTypeIdentifier.type && tenantClaimTypeIdentifier.projectCode)) {
      this.displayState = this.displayStateError;
    } else {

      // we unsubscribe from any current events
      if (this._detailSubscribe) {
        this._detailSubscribe.unsubscribe();
      }

      if (this._detailChangeSubscribe) {
        this._detailChangeSubscribe.unsubscribe();
      }

      this.displayState = this.displayStateLoader;
      this._remoteTenantClaimService.getDetail(tenantClaimTypeIdentifier)
        .then(detail => {

          this.setDetail(detail);
          const tab = this._activatedRoute.snapshot.paramMap.get('tab');
          if (tab) {
            this._logger.debug('Tab is ' + tab);
            this.showTab(tab);
          } else {
            // we go to overview by default
            this._logger.debug('Tab match not found.');
            this.showTab(this.linkDefault);
          }
          this._logger.debug('Detail retrieved successfully');
          this.displayState = this.displayStateContent;
          this.setDimensionsListener();
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        }).catch(error => {
          this._logger.error('Error while getting detail ', error);
          this.displayState = this.displayStateError;
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        });
    }
  }

  private setDetail(detail: TenantClaimDetailModel) {
    if (detail) {
      this.model = detail;
      this._id = detail.identifier;
      this._menuService.setMenuTenantClaimDetail(detail);
    }
  }

  /*

  Buttons

  */

  openDataComponent() {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(this.model);
      this.dataComponent.open();
    }
  }

  openMembership() {
    if (this.membershipComponent) {
      this.membershipComponent.setDetailModel(this.model);
      this.membershipComponent.open();
    }
  }
  onTabChanged(event: MatTabChangeEvent) {
    const tabName = event.tab.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name');
    this.openTab(tabName);
  }

  setModelInComponents(model: TenantClaimDetailModel): void {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(model);
    }
    if (this.membershipComponent) {
      this.membershipComponent.setDetailModel(model);
    }
  }

  showTab(link: string) {
    const tab = this.tabGroup._tabs.find(t => t.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name') === link);
    if (tab) {
      if (this.tabGroup.selectedIndex === tab.position) {
        this.openTab(link);
      }
      this.tabGroup.selectedIndex = tab.position;
      this.tabGroup.realignInkBar();
    } else {
      this.showTab(this.linkDefault);
    }
  }


  openTab(tabName: string) {
    switch (tabName) {
      case this.linkdata: this.openDataComponent();
        break;
      case this.linkMembership: this.openMembership();
        break;
      default:
        this._logger.error('Defaulting to "' + this.linkDefault + '" as tabName not recognised : "' + tabName + '"');
        this.showTab(this.linkDefault);
        break;
    }
  }


  updateLocation(tab: string) {
    // const current = this._location.path(false);
    // const root = current.substring(0, current.indexOf(this.model.id) + this.model.id.length);
    // const newlink = root + '/' + tab;
    // if (!current.startsWith(newlink)) {
    //   this._location.replaceState(newlink);
    // }
  }

  retry() {
    this.refreshDetail();
  }

  close() {
    this._sweetAlert.close();
    this._router.navigate(['/tenantclaims/list']);
  }

  refreshDetail() {
    this.getDetail(this._id);
  }
  delete() {
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Confirm,
        title: this._prefixDelete + '.Confirm.Title',
        message: this._prefixDelete + '.Confirm.Message',
        confirmButton: this._prefixDelete + '.Confirm.OkButton',
        cancelButton: this._prefixDelete + '.Confirm.CancelButton',
        successCallback: () => this.deleteConfirmed(),
        cancelCallback: () => this._sweetAlert.close()
      }));
  }

  private deleteConfirmed() {

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: this._prefixDelete + '.Processing',
        message: this._prefixDelete + '.Processing',
        confirmButton: this._prefixDelete + '.Processing'
      }));

    this._remoteTenantClaimService.delete(this._id)
      .then(response => {
        this._logger.debug('Deleted');
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: this._prefixDelete + '.Success.Title',
            message: this._prefixDelete + '.Success.Message',
            confirmButton: this._prefixDelete + '.Success.OkButton',
            cancelButton: this._prefixDelete + '.Success.CancelButton',
            successCallback: () => this.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt delete user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: this._prefixDelete + '.Failed.Title',
            message: this._prefixDelete + '.Failed.Message',
            confirmButton: this._prefixDelete + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  private setDimensionsListener() {
    if (!this.setDimensions()) {
      const parent = this;
      const interval = setInterval(() => {
        if (parent.setDimensions()) {
          clearInterval(interval);
        }
      }, 200);
    }
  }

  private setDimensions(): boolean {
    if ($(this.detailCard.nativeElement).is(':visible')) {
      const detailCardWidth = $(this.detailCard.nativeElement).width();
      this.headerWidth = detailCardWidth + 'px';
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      return true;
    }
    return false;
  }


  ngOnInit() {
    this._windowSubscribe = this._windowListener.OnResizeAndMenu().subscribe(() => {
      this.setDimensionsListener();
    });
    this.scrollTop();

    this._detailChangeSubscribe = this._remoteTenantClaimService.detailModelChangeEvent().subscribe(detail => {
      this.setDetail(detail);
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    });
  }

  ngOnDestroy() {
    if (this._detailSubscribe) {
      this._detailSubscribe.unsubscribe();
    }

    if (this._detailChangeSubscribe) {
      this._detailChangeSubscribe.unsubscribe();
    }

    if (this._windowSubscribe) {
      this._windowSubscribe.unsubscribe();
    }

    if (this._routerSubscribe) {
      this._routerSubscribe.unsubscribe();
    }

    if (this._locationSubscribe) {
      this._locationSubscribe.unsubscribe();
    }

    if (this._deleteSubscribe) {
      this._deleteSubscribe.unsubscribe();
    }
  }
}
