/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { TenantClaimDataComponent } from './tenant-claim-data.component';


describe('TenantClaimDataComponent', () => {
  let component: TenantClaimDataComponent;
  let fixture: ComponentFixture<TenantClaimDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TenantClaimDataComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantClaimDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
