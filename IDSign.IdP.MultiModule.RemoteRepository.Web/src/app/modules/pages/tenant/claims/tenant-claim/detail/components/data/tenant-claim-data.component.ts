import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { faPlus, faBolt } from '@fortawesome/free-solid-svg-icons';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { TenantClaimDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim';
import { RemoteRepositoryTenantClaimService } from '@http/remote-repository/tenant-claim/remote-repository-tenant-claim.service';
import { UserService } from '@http/user/user.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { SiteActionControlService } from '@services/site-action-control.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { TenantClaimUpdateModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim-update';
import { HttpErrorResponse } from '@angular/common/http';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import { tenantValidator } from '@app/sharedvalidators/identifier.validator';


@Component({
  selector: 'app-tenant-claim-data',
  templateUrl: './tenant-claim-data.component.html',
  styleUrls: ['./tenant-claim-data.component.css']
})
export class TenantClaimDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: TenantClaimDetailModel;
  form: FormGroup;
  typesOptions: MultiCheckboxOption[] = [];

  private _updateSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryTenantClaimService,
    private _userService: UserService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _userActionControl: SiteActionControlService
  ) {
    this._logger = this._logger.createInstance('userDataComponent');
    this.setDetailModel(<TenantClaimDetailModel>{});
  }

  public setDetailModel(model: TenantClaimDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }


  initForm() {

    if (this.form) {
      this.form.get('tenantIdentifier').setValue(this.model.identifier);
      this.form.get('newValue').setValue(this.model.identifier && this.model.identifier.value);
    } else {
      this.form = this._fb.group({
        tenantIdentifier: this._fb.control(this.model.identifier, [tenantValidator]),
        newValue: this._fb.control(this.model.identifier && this.model.identifier.value, [Validators.required, noWhitespaceValidator])
      });
    }
  }

  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <TenantClaimUpdateModel>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'TenantClaim.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
