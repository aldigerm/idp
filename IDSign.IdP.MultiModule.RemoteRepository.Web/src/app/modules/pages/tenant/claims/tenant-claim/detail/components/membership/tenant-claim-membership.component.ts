import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryTenantClaimService } from '@http/remote-repository/tenant-claim/remote-repository-tenant-claim.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserService } from '@http/user/user.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { TenantClaimDetailModel } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim';
import { MultiCheckboxOption } from '@app/sharedcontrols/multi-checkbox/multi-checkbox-option';
import * as _ from 'lodash';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { TenantClaimSetTenants } from '@models/remote-repository/tenant/claims/tenant-claim/remote-repository-tenant-claim-set-tenants';
@Component({
  selector: 'app-tenant-claim-membership',
  templateUrl: './tenant-claim-membership.component.html',
  styleUrls: ['./tenant-claim-membership.component.css']
})
export class TenantClaimMembershipComponent implements OnInit {

  faBolt = faBolt;
  model: TenantClaimDetailModel;
  form: FormGroup;
  options: MultiCheckboxOption[] = [];
  private _updateSubscribe: ISubscription;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryTenantClaimService,
    private _pagePreloader: PagePreloaderService,
    private _userService: UserService,
    private _router: Router,
    private _sectionPreloader: SectionPreloaderService) {
    this._logger = this._logger.createInstance('userGroupMembershipComponent');
  }

  ngOnInit() {
    this.initForm();
  }

  public setDetailModel(model: TenantClaimDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  initForm() {
    const options: MultiCheckboxOption[] = [];
    const selected: TenantIdentifier[] = [];
    if (this.model && this.model.tenantsWithSameClaim) {
      this.model.tenantsWithSameClaim.map(
        u => {
          selected.push(u);
        }
      );
    }
    this._userService.getUser().then(user => {
      if (user && user.userAccessDetail && user.userAccessDetail.tenants && this.model) {
        const tenants = user.userAccessDetail.tenants.filter(t => this.model
          && t.identifier.tenantCode === this.model.identifier.tenantCode);
        if (tenants) {
          tenants.map(tenant => {
            const option = <MultiCheckboxOption>{};
            const id = tenant.identifier;
            option.id = id;
            option.name = tenant.name + '( ' + tenant.identifier.tenantCode + ':' + tenant.identifier.projectCode + ')';
            option.value = id;
            options.push(option);
          });
        }
        this.options = options;

        if (this.form) {
          this.form.get('tenants').setValue(selected || []);
        }
      }
    }).catch(error => {
      this._logger.error('Couldnt initialise tenants list', error);
    });
    if (this.form) {
      this.form.get('tenants').setValue(selected || []);
    } else {
      this.form = this._fb.group({
        tenants: this._fb.control(selected || [])
      });
    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const model = <TenantClaimSetTenants>{};
      model.tenants = [];
      this.form.value.tenants.map(u => {
        const user = <TenantIdentifier>{};
        user.tenantCode = u.tenantCode;
        user.tenantCode = u.tenantCode;
        user.projectCode = u.projectCode;
        model.tenants.push(user);
      });

      // check if the current user is also selected to be removed
      const showWarning = model.tenants.find(tenant => _.isMatch(this.model.identifier, tenant)) ? false : true;

      model.identifier = <TenantClaimIdentifier>{};
      model.identifier = this.model.identifier;

      if (showWarning) {
        const prefix = 'TenantClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Confirm,
            title: prefix + '.Confirm.Title',
            message: prefix + '.Confirm.Message',
            confirmButton: prefix + '.Confirm.OkButton',
            cancelButton: prefix + '.Confirm.CancelButton',
            successCallback: () => this.performUpdate(model, true),
            cancelCallback: () => this._sweetAlert.close()
          }));
      } else {
        this.performUpdate(model, false);
      }
    }

  }

  private performUpdate(model: TenantClaimSetTenants, closeOnReady: boolean) {
    const current = this;
    this._logger.debug('To update : ' + JSON.stringify(model));

    const prefix = 'TenantClaim.Detail.Tabs.Membership.Actions.Submit.Popup';

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));


    this._remoteUserService.setTenantMembership(model)
      .then(response => {
        if (closeOnReady) {
          this._router.navigate(['/tenantclaims/list']);
        }
        current._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt update user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }
}
