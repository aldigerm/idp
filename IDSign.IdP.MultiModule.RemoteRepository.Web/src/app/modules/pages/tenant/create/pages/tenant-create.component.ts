import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { ErrorCode } from '@models/error-code';
import { RemoteRepositoryTenantService } from '@http/remote-repository/tenant/remote-repository-tenant.service';
import { TenantCreateModel } from '@models/remote-repository/tenant/remote-repository-tenant-create';
import { TenantIdentifier } from '@app/shared/models/identifiers/tenant-identifier';
import { HttpErrorResponse } from '@angular/common/http';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { UserService } from '@http/user/user.service';
import { ProjectSelectorComponent } from '@app/sharedcontrols/project-selector/project-selector.component';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { projectValidator } from '@app/sharedvalidators/identifier.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';

@Component({
  selector: 'app-tenant-create',
  templateUrl: './tenant-create.component.html',
  styleUrls: ['./tenant-create.component.css']
})
export class TenantCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  hasAMLDocument = false;
  form: FormGroup;
  countriesOptions: IMultiSelectOption[];

  private _createSubscribe: ISubscription;

  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(ProjectSelectorComponent, { static: true }) projectSelectorComponent: ProjectSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryTenantService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _activatedRoute: ActivatedRoute,
    private _user: UserService
  ) {
    this._logger = this._logger.createInstance('tenantCreateComponent');
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {

    const projectCode = this._activatedRoute.snapshot.queryParams['projectCode'];
    const projectIdentifier = <ProjectIdentifier>{ projectCode: projectCode || '' };
    if (this.form) {
      this.form.get('projectIdentifier').setValue(projectIdentifier);
      this.form.get('tenantCode').setValue('');
      this.form.get('tenantName').setValue('');
    } else {
      this.form = this._fb.group({
        projectIdentifier: this._fb.control(projectIdentifier, [projectValidator()]),
        tenantCode: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        tenantName: this._fb.control('', [Validators.required, noWhitespaceValidator])
      });
    }

    this.projectSelectorComponent.readonly = projectCode ? true : false;

  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/tenants/list']);
  }

  closeAndOpenDetail(identifier: TenantIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/tenants/detail', identifier.projectCode, identifier.tenantCode]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnothertenant() {
    this.form.reset();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <TenantCreateModel>{};
      model = this.form.value;
      model.name = this.form.value.tenantName;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'Tenant.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(tenantCreateResult => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(new TenantIdentifier(
                model.tenantCode, model.projectIdentifier.projectCode
              )),
              cancelCallback: () => current.addAnothertenant()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
