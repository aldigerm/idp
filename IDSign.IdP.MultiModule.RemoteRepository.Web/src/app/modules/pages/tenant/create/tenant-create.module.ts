import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { DateTimeFormatPipeModule } from '@app/shared/pipes/calendar/date-time/date-time-format.pipe.module';
import { DateFormatPipeModule } from '@app/shared/pipes/calendar/date/date-format.pipe.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TenantCreateComponent } from './pages/tenant-create.component';
import { ProjectSelectorModule } from '@app/shared/controls/project-selector/project-selector.module';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';

const routes: Routes = [{
  path: '',
  data: {
    state: 'tenants',
    title: 'tenants',
    urls: [{ title: 'tenant', url: './tenants' }]
  },
  component: TenantCreateComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    AccessControlPipeModule,
    DateTimeFormatPipeModule,
    DateFormatPipeModule,
    ShowErrorsModule,
    MultiselectDropdownModule,
    FontAwesomeModule,
    ProjectSelectorModule,
    InternalTextBoxControl
  ],
  declarations: [
    TenantCreateComponent]
})
export class TenantCreateModule { }
