import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { faPlus, faBolt } from '@fortawesome/free-solid-svg-icons';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { ErrorCode } from '@models/error-code';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { environment } from '@environments/environment';
import { ProjectDetailModel } from '@models/remote-repository/project/remote-repository-project-detail';
import { SiteActionControlService } from '@services/site-action-control.service';
import { ProjectUpdateModel } from '@models/remote-repository/project/remote-repository-project-update';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { RemoteRepositoryProjectService } from '@http/remote-repository/project/remote-repository-project.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';

@Component({
  selector: 'app-project-data',
  templateUrl: './project-data.component.html',
  styleUrls: ['./project-data.component.css']
})
export class ProjectDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: ProjectDetailModel;
  form: FormGroup;
  hasAMLDocument = false;
  hasRiskDocument = false;
  canUpdateMerchantIds = false;
  canSetpassword = false;

  private _createSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _projectService: RemoteRepositoryProjectService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _projectActionControl: SiteActionControlService
  ) {
    this._logger = this._logger.createInstance('projectDataComponent');
    this.setDetailModel(<ProjectDetailModel>{});
  }

  public setDetailModel(model: ProjectDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }


  initForm() {
    if (this.form) {
      this.form.get('projectCode').setValue(this.model.identifier && this.model.identifier.projectCode);
      this.form.get('projectName').setValue(this.model.name);
      this.form.get('enabled').setValue(this.model.enabled);

      if (this.model.loggedInUserProject) {
        this.form.get('enabled').disable();
      } else {
        this.form.get('enabled').enable();
      }

    } else {
      this.form = this._fb.group({
        projectCode: this._fb.control(this.model.identifier && this.model.identifier.projectCode
          , [Validators.required, noWhitespaceValidator]),
        projectName: this._fb.control(this.model.name, [Validators.required, noWhitespaceValidator]),
        enabled: this._fb.control({ value: this.model.enabled, disabled: this.model.loggedInUserProject })
      });
    }
  }

  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <ProjectUpdateModel>{};
      model = this.form.value;
      model.identifier = this.model.identifier;
      model.name = this.form.value.projectName;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'Project.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._projectService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
