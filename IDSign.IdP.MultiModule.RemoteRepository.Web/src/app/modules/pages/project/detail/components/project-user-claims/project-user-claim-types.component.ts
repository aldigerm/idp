import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { faPlus, faTimesCircle, faBolt } from '@fortawesome/free-solid-svg-icons';
import { ProjectDetailModel } from '@models/remote-repository/project/remote-repository-project-detail';
import { LoggerService } from '@app/core/logging/logger.service';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { ProjectUpdateModel } from '@models/remote-repository/project/remote-repository-project-update';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { RemoteRepositoryUserClaimTypeService } from '@http/remote-repository/user-claim-type/remote-repository-user-claim-type.service';
import { ISubscription } from 'rxjs/Subscription';
import { UserClaimTypeDetailModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SlideBoolAnimation } from '@app/sharedanimations/slide-bool-animation';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { UserClaimTypeUpdateModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-update';
import { UserClaimTypeCreateModel } from '@models/remote-repository/user/claims/user-claim-type/remote-repository-user-claim-type-create';

@Component({
  selector: 'app-project-user-claim-types',
  templateUrl: './project-user-claim-types.component.html',
  styleUrls: ['./project-user-claim-types.component.css'],
  animations: [SlideBoolAnimation, DetailExpandAnimation]
})
export class ProjectUserClaimTypesComponent implements OnInit, OnDestroy {

  readonly allColumns: string[] = ['type', 'description', 'actions'];
  readonly desktopColumns: string[] = ['type', 'description', 'actions'];
  readonly tabletColumns: string[] = ['type', 'description'];
  readonly mobileColumns: string[] = ['type', 'description'];

  private _createSubscribe: ISubscription;
  private _updateSubscribe: ISubscription;
  private _deleteSubscribe: ISubscription;
  private _claimUpdate: UserClaimTypeDetailModel;

  formAdd: FormGroup;
  formUpdate: FormGroup;
  model: ProjectDetailModel;

  faPlus = faPlus;
  faBolt = faBolt;
  faTimesCircle = faTimesCircle;

  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateAdd = 'Add';
  readonly displayStateUpdate = 'Update';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;

  constructor(
    private _sweetAlert: SweetAlertService,
    private _logger: LoggerService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _remoteService: RemoteRepositoryUserClaimTypeService,
    private _scroller: ScrollService,
    private _fb: FormBuilder
  ) {
    this._logger = this._logger.createInstance('ProjectUserClaimTypesComponent');
    this.setDetailModel(<ProjectDetailModel>{});
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
    if (this._deleteSubscribe) {
      this._deleteSubscribe.unsubscribe();
    }
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  showOthers() {

    if (this.model.userClaimTypesDetail && this.model.userClaimTypesDetail.length > 0) {
      this.displayState = this.displayStateContent;
    } else {
      this.displayState = this.displayStateNoneFound;
    }
  }

  public setDetailModel(model: ProjectDetailModel) {
    this.model = model;
    this.showOthers();
    this.initForm();
  }
  initForm() {
    if (this.formAdd) {
      this.formAdd.get('type').setValue('');
      this.formAdd.get('description').setValue('');
    } else {
      this.formAdd = this._fb.group({
        type: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        description: this._fb.control('')
      });
    }
    if (this.formUpdate) {
      this.formUpdate.get('newType').setValue('');
      this.formUpdate.get('description').setValue('');
    } else {
      this.formUpdate = this._fb.group({
        newType: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        description: this._fb.control('')
      });
    }
  }

  add() {
    if (!this.formAdd.valid) {
      validateAllFormFields(this.formAdd);
    } else {
      const current = this;
      let model: UserClaimTypeCreateModel;
      model = this.formAdd.value;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'Project.Detail.Tabs.UserClaimTypes.Actions.Add.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteService.create(model)
        .then(identifier => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => {

                current._sweetAlert.close();

                current.formAdd.reset();

              }, cancelCallback: () => {
                current._sweetAlert.close();

                current.formAdd.reset();

                current.showOthers();
              }
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

  update() {
    if (!this.formUpdate.valid) {
      validateAllFormFields(this.formUpdate);
    } else {
      const current = this;
      let model = <UserClaimTypeUpdateModel>{};
      model = this.formUpdate.value;
      model.identifier = this._claimUpdate.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));


      const prefix = 'Project.Detail.Tabs.UserClaimTypes.Actions.Update.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }

  clickDelete(event: Event, idAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const id = button.getAttribute(idAttribute);
    this._logger.debug('Id to get is ' + id);
    const current = this;

    const model = <UserClaimTypeIdentifier>{};
    model.type = id;

    this._logger.debug('To delete : ' + JSON.stringify(model));


    const prefix = 'Project.Detail.Tabs.UserClaimTypes.Actions.Delete.Popup';

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: prefix + '.Processing',
        message: prefix + '.Processing',
        confirmButton: prefix + '.Processing'
      }));

    this._remoteService.delete(model)
      .then(response => {
        current._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: prefix + '.Success.Title',
            message: prefix + '.Success.Message',
            confirmButton: prefix + '.Success.OkButton',
            cancelButton: prefix + '.Success.CancelButton',
            successCallback: () => current._sweetAlert.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: prefix + '.Failed.Title',
            message: prefix + '.Failed.Message',
            confirmButton: prefix + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  clickEdit(event: Event, idAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const id = button.getAttribute(idAttribute);
    this._logger.debug('Id to get is ' + id);

    const claim = this.model.userClaimTypesDetail.find(uct => uct.identifier.type === id);

    if (claim) {
      this.formUpdate.get('newType').setValue(claim.identifier.type);
      this.formUpdate.get('description').setValue(claim.description);
      this.displayState = this.displayStateUpdate;
      this._claimUpdate = claim;
    }

  }

}
