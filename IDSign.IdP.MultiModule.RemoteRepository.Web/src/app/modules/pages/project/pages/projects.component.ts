import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { LoggerService } from '@app/core/logging/logger.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { TableService } from '@services/html/table/table.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { ProjectListPageModel } from '@models/remote-repository/project/list/remote-repository-project-page';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { ProjectBasicModel } from '@models/remote-repository/project/remote-repository-project-basic';
import { RemoteRepositoryProjectService } from '@http/remote-repository/project/remote-repository-project.service';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { ProjectListItemModel } from '@models/remote-repository/project/list/remote-repository-project-list';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class ProjectsComponent implements OnInit, AfterViewInit, OnDestroy {

  readonly allColumns: string[] = ['name', 'projectCode', 'status', 'actions'];
  readonly desktopColumns: string[] = ['name', 'projectCode', 'status', 'actions'];
  readonly tabletColumns: string[] = ['name', 'projectCode', 'status'];
  readonly mobileColumns: string[] = ['name'];
  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode'
  };

  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;

  listModel: Array<ProjectListItemModel>;
  tableSuffix = '-page-projects';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;

  @ViewChildren('projectRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _projectService: RemoteRepositoryProjectService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('projectsComponent');
    this.listModel = new Array<ProjectListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    const parent = this;
    parent.refreshList();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true);
  }

  getList(changeDisplayStates: boolean) {
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }
    this._projectService.getPageList()
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch(error => {
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/projects/create']);
    this.scrollTop();
  }

  closeCreate() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  closeDetail() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  clickItemDetail(event: Event, idAttribute: string) {
    const button = (event.target as HTMLButtonElement);
    const id = button.getAttribute(idAttribute);
    this._logger.debug('Id to get is ' + id);
    this.getDetailAndShow(id);
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  refreshAndShowDetail(id: string) {
    this.getList(false);
    this.getDetailAndShow(id);
  }

  private getDetailAndShow(id: string) {
    this._router.navigate(['/projects/detail', id]);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
