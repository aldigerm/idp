import { ShowSectionPreLoaderGuard } from './show-section-preloader.guard';
import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';


describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [ShowSectionPreLoaderGuard]
    });
  });

  it('should ...', inject([ShowSectionPreLoaderGuard], (guard: ShowSectionPreLoaderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
