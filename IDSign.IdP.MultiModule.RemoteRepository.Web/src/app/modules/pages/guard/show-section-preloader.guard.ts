import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';

import { Observable ,  from } from 'rxjs';

/**
 * Decides if a route can be activated.
 */
@Injectable() export class ShowSectionPreLoaderGuard implements CanDeactivate<any> {

    constructor(private _sectionPreloaderService: SectionPreloaderService) {
    }

    canDeactivate(component: any): boolean | Observable<boolean> {
        this._sectionPreloaderService.setLocalisedMessage('PagePreloader.RedirectingSection');
        this._sectionPreloaderService.show();
        return from(new Promise<boolean>((resolve) => {
            setTimeout(() => {
                resolve(true);
            }, 1000);
        }));
    }
}
