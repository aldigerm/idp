import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserService } from '@http/user/user.service';
import { AccessControlService } from '@services/access-control.service';
import { UserCacheService } from '@services/storage/user-cache.service';

import { Observable ,  from } from 'rxjs';
import { ModuleCode } from '@app/shared/models/module-code';

/**
 * Decides if a route can be activated.
 */
@Injectable() export class PageGuard implements CanActivate {

    constructor(
        private authenticationService: AuthenticationService
        , private router: Router
        , private userService: UserService
        , private logger: LoggerService
        , private userCacheService: UserCacheService
        , private accessControlService: AccessControlService) {

        // create logging instance
        this.logger = this.logger.createInstance('AuthGuard');
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const url: string = state.url;
        return from(
            this.authenticationService.isSignedIn()
                .then(username => {
                    return username;
                })
                .then(username => {
                    return this.userService.isRecognised(username)
                        .then(recognised => {
                            if (recognised) {
                                this.logger.debug('Route is ' + url);

                                const modules = route.data['requiredModule'] as ModuleCode;
                                this.accessControlService.setAllowedModules(recognised.allowedModules);
                                let hasAccess = this.accessControlService.checkAccessForSite();
                                if (!hasAccess) {
                                    // user can't see any tab
                                    return this.redirectToLogin(url);
                                }

                                hasAccess = this.accessControlService.checkAccessForUrl(url);
                                if (!hasAccess) {
                                    // we redirect to the first accessible url
                                    const newUrl = this.accessControlService.getFirstAccessibleUrl();
                                    return this.redirectToUrl(newUrl);
                                }
                            }
                            if (username && recognised) {
                                this.logger.debug('User is signed in and is recognised, let him through.');
                                return this.userCacheService.getUser()
                                    .then(u => {
                                        this.accessControlService.setAllowedModules(u.allowedModules);
                                        return true;
                                    });
                            } else {
                                // he is not signed in, so no need to send the user to login
                                return this.redirectToLogin(url);
                            }
                        })
                        .catch(e => {
                            return this.redirectToLogin(url);
                        });
                })
                .catch(error => {
                    return this.redirectToLogin(url);
                }));
    }

    private redirectToLogin(url: string): boolean {
        // Stores the attempted URL for redirecting.
        this.authenticationService.setRedirectUri(url);
        return this.redirectToUrl('/login');
    }

    private redirectToUrl(url: string): boolean {
        this.logger.debug('Guard : Redirecting to ' + url);
        this.router.navigate([url]);
        return false;
    }

}
