import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PageGuard } from './page.guard';

describe('PageGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [PageGuard]
    });
  });

  it('should ...', inject([PageGuard], (guard: PageGuard) => {
    expect(guard).toBeTruthy();
  }));
});
