import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { TenantSelectorComponent } from '@app/sharedcontrols/tenant-selector/tenant-selector.component';
import { addUpdateFormControl, removeControlFromForm } from '@app/sharedfunctions/form.function';
import { passwordPolicyTypeValidator, projectValidator } from '@app/sharedvalidators/identifier.validator';
import { noWhitespaceValidator } from '@app/sharedvalidators/no-whitespace.validator';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { faPlus, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';
import { UserService } from '@http/user/user.service';
import { PasswordPolicyIdentifier } from '@models/identifiers/password-policy-identifier';
import { PasswordPolicyTypeIdentifier } from '@models/identifiers/password-policy-type-identifier';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { PasswordPolicyCreateModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-create';
import { RemoteRepositoryPasswordPolicyType } from '@models/remote-repository/password-policy/remote-repository-password-policy-type.enum';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { TranslateService } from '@ngx-translate/core';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { numberValidator } from '@app/sharedvalidators/number.validator';

@Component({
  selector: 'app-password-policy-create',
  templateUrl: './password-policy-create.component.html',
  styleUrls: ['./password-policy-create.component.css']
})
export class PasswordPolicyCreateComponent implements OnInit, OnDestroy {

  faPlus = faPlus;
  faTimesCircle = faTimesCircle;
  form: FormGroup;
  regexValue = RemoteRepositoryPasswordPolicyType.Regex;
  minimumLengthValue = RemoteRepositoryPasswordPolicyType.MinimumLength;
  maximumLengthValue = RemoteRepositoryPasswordPolicyType.MaximumLength;
  showRegexSection = false;
  showLengthSection = false;
  showHistorySection = false;

  private _createSubscribe: ISubscription;

  @Output() closeComponent = new EventEmitter<any>();
  @Output() refreshAndShowDetail = new EventEmitter<string>();
  @ViewChild(TenantSelectorComponent, { static: true }) tenantSelectorComponent: TenantSelectorComponent;

  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _translate: TranslateService,
    private _remoteRepositoryService: RemoteRepositoryPasswordPolicyService,
    private _sectionPreloader: SectionPreloaderService,
    private _pagePreloader: PagePreloaderService,
    private _router: Router,
    private _scroller: ScrollService,
    private _userService: UserService
  ) {
    this._logger = this._logger.createInstance('UserCreateComponent');
    this.initForm();
  }

  ngOnInit() {
    this.initForm();
    this.closeLoaders();
  }

  ngOnDestroy(): void {
    if (this._createSubscribe) {
      this._createSubscribe.unsubscribe();
    }
  }

  initForm() {
    if (this.form) {
      this.form.get('description').setValue('');
      this.form.get('defaultErrorMessage').setValue('');
      this.form.get('passwordPolicyCode').setValue('');
      this.form.get('passwordPolicyTypeIdentifier').setValue(<PasswordPolicyTypeIdentifier>{});
      this.form.get('tenantIdentifier').setValue(<TenantIdentifier>{});
    } else {
      this.form = this._fb.group({
        passwordPolicyCode: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        description: this._fb.control('', [noWhitespaceValidator]),
        defaultErrorMessage: this._fb.control('', [Validators.required, noWhitespaceValidator]),
        passwordPolicyTypeIdentifier: this._fb.control(<PasswordPolicyTypeIdentifier>{}, [passwordPolicyTypeValidator()]),
        tenantIdentifier: this._fb.control(<TenantIdentifier>{}, [projectValidator()]),
      });

      this.form.get('passwordPolicyTypeIdentifier').valueChanges.subscribe(
        (value: PasswordPolicyTypeIdentifier) => {
          if (value && value.passwordPolicyTypeCode === this.regexValue) {
            this.showRegexSection = true;
            this.showLengthSection = false;
            this.showHistorySection = false;
            addUpdateFormControl(this.form, this._fb, 'regex', '', [Validators.required, noWhitespaceValidator]);
            removeControlFromForm(this.form, 'length');
            removeControlFromForm(this.form, 'historyLimit');
          } else if (value &&
            (value.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MaximumLength
              || value.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MinimumLength)) {
            this.showRegexSection = false;
            this.showLengthSection = true;
            this.showHistorySection = false;
            removeControlFromForm(this.form, 'regex');
            removeControlFromForm(this.form, 'historyLimit');
            addUpdateFormControl(this.form, this._fb, 'length', '', [Validators.required, numberValidator]);
          } else if (value &&
            value.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.History) {
            this.showRegexSection = false;
            this.showLengthSection = false;
            this.showHistorySection = true;
            removeControlFromForm(this.form, 'length');
            removeControlFromForm(this.form, 'regex');
            addUpdateFormControl(this.form, this._fb, 'historyLimit', '', [Validators.required, numberValidator]);
          }
        });
    }
  }

  closeLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  close() {
    this._sweetAlert.close();
    this.closeComponent.emit(true);
    this._router.navigate(['/password-policys/list']);
  }

  closeAndOpenDetail(id: PasswordPolicyIdentifier) {
    this._sweetAlert.close();
    this._router.navigate(['/password-policy/detail', id.projectCode, id.tenantCode, id.passwordPolicyCode]);
    this.scrollTop();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }

  addAnotherPasswordPolicy() {
    this.form.reset();
    this.closeLoaders();
    this._sweetAlert.close();
  }

  add() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <PasswordPolicyCreateModel>{};
      model = this.form.value;

      this._logger.debug('To save : ' + JSON.stringify(model));

      const prefix = 'PasswordPolicy.Create.Actions.Submit.Popup';
      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));

      this._remoteRepositoryService.create(model)
        .then(identifier => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.SuccessWithOption,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current.closeAndOpenDetail(identifier),
              cancelCallback: () => current.addAnotherPasswordPolicy()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
