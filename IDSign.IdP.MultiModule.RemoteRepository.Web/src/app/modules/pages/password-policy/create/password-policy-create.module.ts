import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { PasswordPolicyTypeSelectorModule } from '@app/sharedcontrols/password-policy-type-selector/password-policy-type-selector.module';
import { RegexTesterModule } from '@app/sharedcontrols/regex/regex.module';
import { TenantSelectorControl } from '@app/sharedcontrols/tenant-selector/tenant-selector.module';
import { InternalTextAreaControl } from '@app/sharedcontrols/text-area/int-text-area.module';
import { InternalTextBoxControl } from '@app/sharedcontrols/text-control/int-text-box.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { PasswordPolicyCreateComponent } from './pages/password-policy-create.component';

const routes: Routes = [{
  path: '',
  data: {
    state: 'users',
    title: 'users',
    urls: [{ title: 'user', url: './users' }]
  },
  component: PasswordPolicyCreateComponent
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    InternalTextBoxControl,
    PasswordPolicyTypeSelectorModule,
    RegexTesterModule,
    InternalTextAreaControl,
    TenantSelectorControl
  ],
  declarations: [
    PasswordPolicyCreateComponent]
})
export class PasswordPolicyCreateModule { }
