import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { faSync, faTimesCircle, faTrash } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { PasswordPolicyIdentifier } from '@models/identifiers/password-policy-identifier';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { ScrollService } from '@services/html/scrolling/scroll.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { LocationService } from '@services/location.service';
import { MenuService } from '@services/menu.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { PasswordPolicyDataComponent } from '../components/data/password-policy-data.component';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';
declare const $: any;

@Component({
  selector: 'app-password-policy-detail',
  templateUrl: './password-policy-detail.component.html',
  styleUrls: ['./password-policy-detail.component.css'],
  animations: [SlideBoolAnimation]
})
export class PasswordPolicyDetailComponent implements OnInit, OnDestroy {
  faTimesCircle = faTimesCircle;
  faSync = faSync;
  faTrash = faTrash;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateError = 'Error';

  readonly linkdata = 'data';
  readonly linkDefault = this.linkdata;

  displayState = this.displayStateLoader;

  private _detailSubscribe: ISubscription;
  private _detailChangeSubscribe: ISubscription;
  private _windowSubscribe: ISubscription;
  private _locationSubscribe: ISubscription;
  private _routerSubscribe: ISubscription;
  private _deleteSubscribe: ISubscription;
  private _isDetailOfCurrentUser = false;
  private readonly _prefixDelete = 'PasswordPolicy.Detail.Actions.Delete.Popup';

  model: PasswordPolicyDetailModel;
  headerWidth = '400px';
  private _id = <PasswordPolicyIdentifier>{};
  @Output() closeComponent = new EventEmitter<any>();
  @ViewChild('detailCard', { static: true }) detailCard: ElementRef;
  @ViewChild(MatTabGroup, { static: true }) tabGroup: MatTabGroup;
  @ViewChild(PasswordPolicyDataComponent, { static: false }) dataComponent: PasswordPolicyDataComponent;
  constructor(
    private _remoteUserService: RemoteRepositoryPasswordPolicyService,
    private _logger: LoggerService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService,
    private _windowListener: WindowEventListenerService,
    private _scroller: ScrollService,
    private _menuService: MenuService,
    private _sweetAlert: SweetAlertService
  ) {
    this._logger = this._logger.createInstance('userDetailComponent');
    this.displayState = this.displayStateLoader;
    this.model = <PasswordPolicyDetailModel>{};

    this._routerSubscribe = this._router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (event.url.startsWith('/password-policy/')) {
          this._logger.debug('Router navigation for password policy detected.');
          this.init();
        }
      });
  }

  refresh() {
    this.retry();
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }


  private init() {
    const tenantCode = this._activatedRoute.snapshot.paramMap.get('tenantCode');
    const projectCode = this._activatedRoute.snapshot.paramMap.get('projectCode');
    const passwordPolicyCode = this._activatedRoute.snapshot.paramMap.get('passwordPolicyCode');
    if (tenantCode && projectCode && passwordPolicyCode) {
      this._logger.debug('query params have "' + tenantCode + '" "' + projectCode
        + '" "' + passwordPolicyCode + '"');
      const id = new PasswordPolicyIdentifier(passwordPolicyCode, tenantCode, projectCode);
      this.getDetail(id);
    } else {
      this.displayState = this.displayStateError;
      this._pagePreloader.hide();
      this._sectionPreloader.hide();
    }
  }

  private getDetail(identifier: PasswordPolicyIdentifier) {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
    this.scrollTop();
    this._id = identifier;
    if (!(identifier
      && identifier.passwordPolicyCode
      && identifier.tenantCode
      && identifier.projectCode)) {
      this.displayState = this.displayStateError;
    } else {

      // we unsubscribe from any current events
      if (this._detailSubscribe) {
        this._detailSubscribe.unsubscribe();
      }

      if (this._detailChangeSubscribe) {
        this._detailChangeSubscribe.unsubscribe();
      }

      this.displayState = this.displayStateLoader;
      this._remoteUserService.getDetail(identifier)
        .then(detail => {

          this.setDetail(detail);
          const tab = this._activatedRoute.snapshot.paramMap.get('tab');
          if (tab) {
            this._logger.debug('Tab is ' + tab);
            this.showTab(tab);
          } else {
            this.showTab(this.linkDefault);
          }
          this._logger.debug('Detail retrieved successfully');
          this.displayState = this.displayStateContent;
          this.setDimensionsListener();
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        }).catch(error => {
          this._logger.error('Error while getting detail ', error);
          this.displayState = this.displayStateError;
          this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        });
    }
  }

  private setDetail(detail: PasswordPolicyDetailModel) {
    if (detail) {
      this.model = detail;
      this._id = detail.identifier;
      this.setModelInComponents(detail);
      this._menuService.setMenuPasswordPolicyDetail(detail);
    }
  }

  /*

  Buttons

  */


  openDataComponent() {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(this.model);
      this.dataComponent.open();
    }
  }

  onTabChanged(event: MatTabChangeEvent) {
    const tabName = event.tab.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name');
    this.openTab(tabName);
  }

  goToNextAvailableTab() {
    this.showTab(this.linkDefault);
  }

  setModelInComponents(model: PasswordPolicyDetailModel): void {
    if (this.dataComponent) {
      this.dataComponent.setDetailModel(model);
    }
  }

  updateLocation(tab: string) {
    // const current = this._location.path(false);
    // const root = current.substring(0, current.indexOf(this.model.id) + this.model.id.length);
    // const newlink = root + '/' + tab;
    // if (!current.startsWith(newlink)) {
    //   this._location.replaceState(newlink);
    // }
  }

  retry() {
    this.refreshDetail();
  }

  close() {
    this._sweetAlert.close();
    this._router.navigate(['/password-policy/list']);
  }

  showTab(link: string) {
    const tab = this.tabGroup._tabs.find(t => t.content.viewContainerRef.element.nativeElement.getAttribute('data-tab-name') === link);
    if (tab) {
      if (this.tabGroup.selectedIndex === tab.position) {
        this.openTab(link);
      }
      this.tabGroup.selectedIndex = tab.position;
      this.tabGroup.realignInkBar();
    } else {
      this.showTab(this.linkDefault);
    }
  }


  openTab(tabName: string) {
    switch (tabName) {
      case this.linkdata: this.openDataComponent();
        break;
      default:
        this._logger.error('Defaulting to "' + this.linkDefault + '" as tabName not recognised : "' + tabName + '"');
        this.showTab(this.linkDefault);
        break;
    }
  }


  refreshDetail() {
    this.getDetail(this._id);
  }
  delete() {
    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Confirm,
        title: this._prefixDelete + '.Confirm.Title',
        message: this._prefixDelete + '.Confirm.Message',
        confirmButton: this._prefixDelete + '.Confirm.OkButton',
        cancelButton: this._prefixDelete + '.Confirm.CancelButton',
        successCallback: () => this.deleteConfirmed(),
        cancelCallback: () => this._sweetAlert.close()
      }));
  }

  private deleteConfirmed() {

    this._sweetAlert.show(
      new SweetAlertRequest({
        type: SweetAlertType.Processing,
        title: this._prefixDelete + '.Processing',
        message: this._prefixDelete + '.Processing',
        confirmButton: this._prefixDelete + '.Processing'
      }));

    this._remoteUserService.delete(this._id)
      .then(response => {
        this._logger.debug('Deleted');
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Success,
            title: this._prefixDelete + '.Success.Title',
            message: this._prefixDelete + '.Success.Message',
            confirmButton: this._prefixDelete + '.Success.OkButton',
            cancelButton: this._prefixDelete + '.Success.CancelButton',
            successCallback: () => this.close()
          }));
      }).catch((error: HttpErrorResponse) => {
        this._logger.error('Couldnt delete user', error);
        this._sweetAlert.show(
          new SweetAlertRequest({
            type: SweetAlertType.Error,
            title: this._prefixDelete + '.Failed.Title',
            message: this._prefixDelete + '.Failed.Message',
            confirmButton: this._prefixDelete + '.Failed.OkButton',
            errorCode: error && error.error ? error.error.ErrorCode : undefined
          }));
      });
  }

  private setDimensionsListener() {
    if (!this.setDimensions()) {
      const parent = this;
      const interval = setInterval(() => {
        if (parent.setDimensions()) {
          clearInterval(interval);
        }
      }, 200);
    }
  }

  private setDimensions(): boolean {
    if ($(this.detailCard.nativeElement).is(':visible')) {
      const detailCardWidth = $(this.detailCard.nativeElement).width();
      this.headerWidth = detailCardWidth + 'px';
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
      return true;
    }
    return false;
  }


  ngOnInit() {
    this._windowSubscribe = this._windowListener.OnResizeAndMenu().subscribe(() => {
      this.setDimensionsListener();
    });

    this._detailChangeSubscribe = this._remoteUserService.detailModelChangeEvent().subscribe(detail => {
      this.setDetail(detail);
      this._siteChangeDetector.detectChanges(this._changeDetectionRef);
    });

    this.scrollTop();
  }

  ngOnDestroy() {
    if (this._detailSubscribe) {
      this._detailSubscribe.unsubscribe();
    }

    if (this._detailChangeSubscribe) {
      this._detailChangeSubscribe.unsubscribe();
    }

    if (this._windowSubscribe) {
      this._windowSubscribe.unsubscribe();
    }

    if (this._routerSubscribe) {
      this._routerSubscribe.unsubscribe();
    }

    if (this._locationSubscribe) {
      this._locationSubscribe.unsubscribe();
    }

    if (this._deleteSubscribe) {
      this._deleteSubscribe.unsubscribe();
    }
  }
}
