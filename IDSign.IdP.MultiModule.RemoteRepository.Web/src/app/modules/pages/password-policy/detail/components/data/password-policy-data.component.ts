import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { noWhitespaceValidator } from '@app/shared/validators/no-whitespace.validator';
import { addUpdateFormControl, removeControlFromForm } from '@app/sharedfunctions/form.function';
import { validateAllFormFields } from '@app/sharedvalidators/validate-form-fields';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { PasswordPolicyUpdateModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-update';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { RemoteRepositoryPasswordPolicyType } from '@models/remote-repository/password-policy/remote-repository-password-policy-type.enum';
import { numberValidator } from '@app/sharedvalidators/number.validator';


@Component({
  selector: 'app-password-policy-data',
  templateUrl: './password-policy-data.component.html',
  styleUrls: ['./password-policy-data.component.css']
})
export class PasswordPolicyDataComponent implements OnInit, OnDestroy {

  faBolt = faBolt;
  model: PasswordPolicyDetailModel;
  form: FormGroup;
  canUpdateData = false;
  canSetpassword = false;
  showRegexSection = false;
  showLengthSection = false;
  showHistorySection = false;

  private _updateSubscribe: ISubscription;
  constructor(
    private _logger: LoggerService,
    private _fb: FormBuilder,
    private _sweetAlert: SweetAlertService,
    private _remoteUserService: RemoteRepositoryPasswordPolicyService,
    private _pagePreloader: PagePreloaderService,
    private _sectionPreloader: SectionPreloaderService
  ) {
    this._logger = this._logger.createInstance('PrivacyPolicyDataComponent');
    this.setDetailModel(<PasswordPolicyDetailModel>{});
  }

  public setDetailModel(model: PasswordPolicyDetailModel) {
    this.model = model;
    this.initForm();
  }

  public open() {
    this.hideLoaders();
  }

  hideLoaders() {
    this._pagePreloader.hide();
    this._sectionPreloader.hide();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this._updateSubscribe) {
      this._updateSubscribe.unsubscribe();
    }
  }


  initForm() {
    if (this.form) {
      this.form.get('tenantIdentifier').setValue(this.model.identifier);
      this.form.get('passwordPolicyTypeIdentifier').setValue(this.model.passwordPolicyTypeIdentifier);
      this.form.get('passwordPolicyCode').setValue(this.model.identifier.passwordPolicyCode);
      this.form.get('description').setValue(this.model.description ? this.model.description : '');
      this.form.get('defaultErrorMessage').setValue(this.model.defaultErrorMessage ? this.model.defaultErrorMessage : '');
    } else {
      this.form = this._fb.group({
        tenantIdentifier: this._fb.control(this.model.identifier, [Validators.required]),
        passwordPolicyTypeIdentifier: this._fb.control(this.model.passwordPolicyTypeIdentifier, [Validators.required]),
        passwordPolicyCode: this._fb.control({ value: this.model.identifier && this.model.identifier.passwordPolicyCode, disabled: true }),
        description: this._fb.control(this.model.description ? this.model.description : '', []),
        defaultErrorMessage: this._fb.control(this.model.defaultErrorMessage ? this.model.defaultErrorMessage : '',
          [Validators.required, noWhitespaceValidator])
      });
    }

    if (this.model && this.model.passwordPolicyTypeIdentifier && this.model.passwordPolicyTypeIdentifier.passwordPolicyTypeCode) {
      if (this.model.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.Regex) {
        this.showRegexSection = true;
        this.showLengthSection = false;
        this.showHistorySection = false;
        addUpdateFormControl(this.form, this._fb, 'regex', this.model.regex, [Validators.required, noWhitespaceValidator]);
        removeControlFromForm(this.form, 'length');
        removeControlFromForm(this.form, 'historyLimit');
      } else if ((this.model.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MaximumLength
        || this.model.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MinimumLength)) {
        this.showRegexSection = false;
        this.showLengthSection = true;
        this.showHistorySection = false;
        removeControlFromForm(this.form, 'regex');
        removeControlFromForm(this.form, 'historyLimit');
        addUpdateFormControl(this.form, this._fb, 'length', this.model.length, [Validators.required, numberValidator]);
      } else if (this.model.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.History) {
        this.showRegexSection = false;
        this.showLengthSection = false;
        this.showHistorySection = true;
        removeControlFromForm(this.form, 'length');
        removeControlFromForm(this.form, 'regex');
        addUpdateFormControl(this.form, this._fb, 'historyLimit', this.model.historyLimit, [Validators.required, numberValidator]);
      }

    }
  }


  update() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    } else {
      const current = this;
      let model = <PasswordPolicyUpdateModel>{};
      model = this.form.value;
      model.identifier = this.model.identifier;

      this._logger.debug('To update : ' + JSON.stringify(model));

      const prefix = 'PasswordPolicy.Detail.Tabs.Data.Actions.Submit.Popup';

      this._sweetAlert.show(
        new SweetAlertRequest({
          type: SweetAlertType.Processing,
          title: prefix + '.Processing',
          message: prefix + '.Processing',
          confirmButton: prefix + '.Processing'
        }));


      this._remoteUserService.update(model)
        .then(response => {
          current._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Success,
              title: prefix + '.Success.Title',
              message: prefix + '.Success.Message',
              confirmButton: prefix + '.Success.OkButton',
              cancelButton: prefix + '.Success.CancelButton',
              successCallback: () => current._sweetAlert.close()
            }));
        }).catch((error: HttpErrorResponse) => {
          this._logger.error('Couldnt update user', error);
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: prefix + '.Failed.Title',
              message: prefix + '.Failed.Message',
              confirmButton: prefix + '.Failed.OkButton',
              errorCode: error && error.error ? error.error.ErrorCode : undefined
            }));
        });
    }
  }
}
