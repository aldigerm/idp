import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from '@app/core/logging/logger.service';
import { ScrollService } from '@app/core/services/html/scrolling/scroll.service';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { SlideBoolAnimation } from '@app/shared/animations/slide-bool-animation';
import { DetailExpandAnimation } from '@app/sharedanimations/detail-expand.animation';
import { faExclamation, faPlus, faSync } from '@fortawesome/free-solid-svg-icons';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { TableService } from '@services/html/table/table.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { SectionPreloaderService } from '@services/preloaders/section/section-preloader.service';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { PasswordPolicyListItemModel } from '@models/remote-repository/password-policy/list/remote-repository-password-policy-list';
import { RemoteRepositoryPasswordPolicyService } from '@http/remote-repository/password-policy/password-policy.service';
import { PasswordPolicyIdentifier } from '@models/identifiers/password-policy-identifier';

@Component({
  selector: 'app-password-policy',
  templateUrl: './password-policy.component.html',
  styleUrls: ['./password-policy.component.css'],
  animations: [SlideBoolAnimation, FadeBoolAnimation, DetailExpandAnimation]
})
export class PasswordPolicyComponent implements OnInit, AfterViewInit, OnDestroy {

  readonly allColumns: string[] = ['passwordPolicyCode', 'passwordPolicyTypeCode', 'projectCode', 'description', 'actions'];
  readonly desktopColumns: string[] = ['passwordPolicyCode', 'passwordPolicyTypeCode', 'projectCode', 'description', 'actions'];
  readonly tabletColumns: string[] = ['passwordPolicyCode', 'passwordPolicyTypeCode', 'projectCode', 'actions'];
  readonly mobileColumns: string[] = ['passwordPolicyCode', 'passwordPolicyTypeCode', 'projectCode'];

  readonly columnValuesMapper: { [id: string]: string } = {
    'projectCode': 'identifier.projectCode',
    'passwordPolicyCode': 'identifier.passwordPolicyCode',
    'passwordPolicyTypeCode': 'passwordPolicyTypeIdentifier.passwordPolicyTypeCode'
  };
  faPlus = faPlus;
  faSync = faSync;
  faExclamation = faExclamation;
  readonly displayStateLoader = 'Loader';
  readonly displayStateContent = 'Content';
  readonly displayStateNoneFound = 'NoneFound';
  readonly displayStateError = 'Error';
  displayState = this.displayStateLoader;
  private _changeDisplayStates = true;
  private _tenantIdentifier = <TenantIdentifier>{};

  listModel: Array<PasswordPolicyListItemModel>;
  tableSuffix = '-page-users';

  private _listSubscribe: ISubscription;
  private _rowsSubscribe: ISubscription;

  @ViewChildren('userRow') public rows: QueryList<any>;

  constructor(
    private _pagePreloaderService: PagePreloaderService,
    private _sectionPreloaderService: SectionPreloaderService,
    private _logger: LoggerService,
    private _passwordPolicyService: RemoteRepositoryPasswordPolicyService,
    private _tableService: TableService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _scroller: ScrollService
  ) {
    this._logger = this._logger.createInstance('usersComponent');
    this.listModel = new Array<PasswordPolicyListItemModel>();
    this.displayState = this.displayStateLoader;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._pagePreloaderService.hide();
    this._rowsSubscribe = this.rows.changes.subscribe(t => {
      this._logger.debug('Changes triggered ' + t);
      this.initTable();
    });
    const parent = this;
    parent.refreshList();
  }

  ngOnDestroy(): void {
    if (this._listSubscribe) {
      this._listSubscribe.unsubscribe();
    }
    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
  }

  refreshList() {
    this.getList(true, this._tenantIdentifier);
  }

  getList(changeDisplayStates: boolean, tenantIdentifier: TenantIdentifier) {
    if (!tenantIdentifier) {
      tenantIdentifier = new TenantIdentifier(null, null);
    }
    this._logger.debug('Refresh list');
    if (changeDisplayStates) {
      this.displayState = this.displayStateLoader;
      this._changeDisplayStates = true;
    } else {
      this._changeDisplayStates = false;
    }

    this._passwordPolicyService.getPageList()
      .then(data => {
        this.listModel = data;
        if (this.listModel.length === 0) {
          if (changeDisplayStates) {
            this.displayState = this.displayStateNoneFound;
          }
        }
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      }).catch((error: HttpErrorResponse) => {
        const errorCode = error && error.error ? error.error.ErrorCode : undefined;
        this._logger.error('Couldnt get user list; ' + errorCode, error);
        this.displayState = this.displayStateError;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
        this._sectionPreloaderService.hide();
        this._pagePreloaderService.hide();
      });
  }

  chatSearchKeyUp(value: string) {
    this._tableService.search(value, this.tableSuffix, 500);
  }

  getDaysOpenClass(days: number): string {
    if (days < 15) {
      return 'level1';
    } else if (days < 29) {
      return 'level2';
    } else {
      return 'level3';
    }
  }

  private initTable() {
    if (this._changeDisplayStates) {
      if (this.listModel.length > 0) {
        this.displayState = this.displayStateContent;
      } else {
        this.displayState = this.displayStateNoneFound;
      }
    } else {
      // we reset the display state for future retrievals
      this._changeDisplayStates = true;
    }
    this._tableService.initTable(this.tableSuffix);
    this._tableService.restartPagination(this.tableSuffix);

    this._siteChangeDetector.detectChanges(this._changeDetectorRef);
  }

  create() {
    this._router.navigate(['/password-policy/create']);
    this.scrollTop();
  }

  closeCreate() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  closeDetail() {
    this.displayState = this.displayStateContent;
    this.scrollTop();
  }

  clickItemDetail(id: PasswordPolicyIdentifier) {
    this._logger.debug('Id to get is ' + JSON.stringify(id));
    this.getDetailAndShow(id);
  }

  getUnixtime(date: any): Number {
    return new Date(date).getTime() / 1000;
  }

  refreshAndShowDetail(id: PasswordPolicyIdentifier) {
    this.getList(false, this._tenantIdentifier);
    this.getDetailAndShow(id);
  }

  private getDetailAndShow(id: PasswordPolicyIdentifier) {
    this._router.navigate(['/password-policy/detail',
      id.projectCode, id.tenantCode, id.passwordPolicyCode]);
  }

  private scrollTop() {
    this._scroller.scrollToTop();
  }
}
