import { ShowSectionPreLoaderGuard } from './guard/show-section-preloader.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './pages.component';
import { PageGuard } from './guard/page.guard';
import { ModuleCode } from '@models/module-code';
import { MenuId } from '@app/shared/models/menu';

const routes: Routes = [
    {
        path: '', component: PageComponent,
        children: [
            // users
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'users/create',
                loadChildren: './users/create/user-create.module#UserCreateModule',
                data: {
                    menuId: MenuId.UserCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'users/detail',
                loadChildren: './users/detail/user-detail.module#UserDetailModule',
                data: {
                    menuId: MenuId.UserDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_VIEW
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'users/list',
                loadChildren: './users/users.module#UsersModule',
                data: {
                    menuId: MenuId.UserList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },

            // tenants
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenants/create',
                loadChildren: './tenant/create/tenant-create.module#TenantCreateModule',
                data: {
                    menuId: MenuId.TenantCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenants/detail/:projectCode/:tenantCode',
                loadChildren: './tenant/detail/tenant-detail.module#TenantDetailModule',
                data: {
                    menuId: MenuId.TenantDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenants/list',
                loadChildren: './tenant/tenant.module#TenantsModule',
                data: {
                    menuId: MenuId.TenantList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // projects
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'projects/create',
                loadChildren: './project/create/project-create.module#ProjectCreateModule',
                data: {
                    menuId: MenuId.ProjectCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'projects/detail/:projectCode',
                loadChildren: './project/detail/project-detail.module#ProjectDetailModule',
                data: {
                    menuId: MenuId.ProjectDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'projects/list',
                loadChildren: './project/projects.module#ProjectsModule',
                data: {
                    menuId: MenuId.ProjectList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN,
                    isMenuDefaultLink: true
                }
            },

            // user groups
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroups/create',
                loadChildren: './user-group/create/user-group-create.module#UserGroupCreateModule',
                data: {
                    menuId: MenuId.UserGroupCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroups/detail/:projectCode/:tenantCode/:userGroupCode',
                loadChildren: './user-group/detail/user-group-detail.module#UserGroupDetailModule',
                data: {
                    menuId: MenuId.UserGroupDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroups/list',
                loadChildren: './user-group/user-groups.module#UserGroupsModule',
                data: {
                    menuId: MenuId.UserGroupList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // roles
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roles/create',
                loadChildren: './role/create/role-create.module#RoleCreateModule',
                data: {
                    menuId: MenuId.RoleCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roles/detail/:projectCode/:tenantCode/:roleCode',
                loadChildren: './role/detail/role-detail.module#RoleDetailModule',
                data: {
                    menuId: MenuId.RoleDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roles/list',
                loadChildren: './role/roles.module#RolesModule',
                data: {
                    menuId: MenuId.RoleList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // modules
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'modules/create',
                loadChildren: './module/create/module-create.module#ModuleCreateModule',
                data: {
                    menuId: MenuId.ModuleCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'modules/detail/:projectCode/:moduleCode',
                loadChildren: './module/detail/module-detail.module#ModuleDetailModule',
                data: {
                    menuId: MenuId.ModuleDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'modules/list',
                loadChildren: './module/modules.module#ModulesModule',
                data: {
                    menuId: MenuId.ModuleList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },


            // user claim type
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaimtypes/create',
                loadChildren: './users/claims/user-claim-type/create/user-claim-type-create.module#UserClaimTypeCreateModule',
                data: {
                    menuId: MenuId.UserClaimTypeCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaimtypes/detail/:projectCode/:type',
                loadChildren: './users/claims/user-claim-type/detail/user-claim-type-detail.module#UserClaimTypeDetailModule',
                data: {
                    menuId: MenuId.UserClaimTypeDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaimtypes/list',
                loadChildren: './users/claims/user-claim-type/user-claim-types.module#UserClaimTypesModule',
                data: {
                    menuId: MenuId.UserClaimTypeList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // user claim
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaims/create',
                loadChildren: './users/claims/user-claim/create/user-claim-create.module#UserClaimCreateModule',
                data: {
                    menuId: MenuId.UserClaimCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaims/detail/:projectCode/:tenantCode/:username/:type/:value',
                loadChildren: './users/claims/user-claim/detail/user-claim-detail.module#UserClaimDetailModule',
                data: {
                    menuId: MenuId.UserClaimDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'userclaims/list',
                loadChildren: './users/claims/user-claim/user-claims.module#UserClaimsModule',
                data: {
                    menuId: MenuId.UserClaimList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },


            // role claim type
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaimtypes/create',
                loadChildren: './role/claims/role-claim-type/create/role-claim-type-create.module#RoleClaimTypeCreateModule',
                data: {
                    menuId: MenuId.RoleClaimTypeCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaimtypes/detail/:projectCode/:type',
                loadChildren: './role/claims/role-claim-type/detail/role-claim-type-detail.module#RoleClaimTypeDetailModule',
                data: {
                    menuId: MenuId.RoleClaimTypeDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaimtypes/list',
                loadChildren: './role/claims/role-claim-type/role-claim-types.module#RoleClaimTypesModule',
                data: {
                    menuId: MenuId.RoleClaimTypeList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // role claim
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaims/create',
                loadChildren: './role/claims/role-claim/create/role-claim-create.module#RoleClaimCreateModule',
                data: {
                    menuId: MenuId.RoleClaimCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaims/detail/:projectCode/:tenantCode/:roleCode/:type/:value',
                loadChildren: './role/claims/role-claim/detail/role-claim-detail.module#RoleClaimDetailModule',
                data: {
                    menuId: MenuId.RoleClaimDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'roleclaims/list',
                loadChildren: './role/claims/role-claim/role-claims.module#RoleClaimsModule',
                data: {
                    menuId: MenuId.RoleClaimList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },


            // user-group claim type
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaimtypes/create',
                loadChildren: './user-group/claims/user-group-claim-type/create/user-group-claim-type-create.module' +
                    '#UserGroupClaimTypeCreateModule',
                data: {
                    menuId: MenuId.UserGroupClaimTypeCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaimtypes/detail/:projectCode/:type',
                loadChildren: './user-group/claims/user-group-claim-type/detail/user-group-claim-type-detail.module'
                    + '#UserGroupClaimTypeDetailModule',
                data: {
                    menuId: MenuId.UserGroupClaimTypeDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaimtypes/list',
                loadChildren: './user-group/claims/user-group-claim-type/user-group-claim-types.module#UserGroupClaimTypesModule',
                data: {
                    menuId: MenuId.UserGroupClaimTypeList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // user-group claim
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaims/create',
                loadChildren: './user-group/claims/user-group-claim/create/user-group-claim-create.module#UserGroupClaimCreateModule',
                data: {
                    menuId: MenuId.UserGroupClaimCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaims/detail/:projectCode/:tenantCode/:userGroupCode/:type/:value',
                loadChildren: './user-group/claims/user-group-claim/detail/user-group-claim-detail.module#UserGroupClaimDetailModule',
                data: {
                    menuId: MenuId.UserGroupClaimDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'usergroupclaims/list',
                loadChildren: './user-group/claims/user-group-claim/user-group-claims.module#UserGroupClaimsModule',
                data: {
                    menuId: MenuId.UserGroupClaimList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },


            // tenant claim type
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaimtypes/create',
                loadChildren: './tenant/claims/tenant-claim-type/create/tenant-claim-type-create.module#TenantClaimTypeCreateModule',
                data: {
                    menuId: MenuId.TenantClaimTypeCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaimtypes/detail/:projectCode/:type',
                loadChildren: './tenant/claims/tenant-claim-type/detail/tenant-claim-type-detail.module#TenantClaimTypeDetailModule',
                data: {
                    menuId: MenuId.TenantClaimTypeDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaimtypes/list',
                loadChildren: './tenant/claims/tenant-claim-type/tenant-claim-types.module#TenantClaimTypesModule',
                data: {
                    menuId: MenuId.TenantClaimTypeList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // tenant claim
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaims/create',
                loadChildren: './tenant/claims/tenant-claim/create/tenant-claim-create.module#TenantClaimCreateModule',
                data: {
                    menuId: MenuId.TenantClaimCreate,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaims/detail/:projectCode/:tenantCode/:type/:value',
                loadChildren: './tenant/claims/tenant-claim/detail/tenant-claim-detail.module#TenantClaimDetailModule',
                data: {
                    menuId: MenuId.TenantClaimDetail,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'tenantclaims/list',
                loadChildren: './tenant/claims/tenant-claim/tenant-claims.module#TenantClaimsModule',
                data: {
                    menuId: MenuId.TenantClaimList,
                    requiredModule: ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN
                }
            },

            // password-policy
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'password-policy/create',
                loadChildren: './password-policy/create/password-policy-create.module#PasswordPolicyCreateModule',
                data: {
                    menuId: MenuId.PasswordPolicyCreate
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'password-policy/detail/:projectCode/:tenantCode/:passwordPolicyCode',
                loadChildren: './password-policy/detail/password-policy-detail.module#PasswordPolicyDetailModule',
                data: {
                    menuId: MenuId.PasswordPolicyDetail
                }
            },
            {
                canActivate: [PageGuard],
                canDeactivate: [ShowSectionPreLoaderGuard],
                path: 'password-policy/list',
                loadChildren: './password-policy/password-policy.module#PasswordPoliciesModule',
                data: {
                    menuId: MenuId.PasswordPolicyList
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
