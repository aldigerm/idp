import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './pages/login.component';
import { PagePreloaderModule } from '@app/shared/components/page-preloader/page-preloader.module';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        FormsModule,
        TranslateModule,
        PagePreloaderModule,
        FontAwesomeModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {
}
