import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { faArrowCircleRight, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '@http/user/user.service';
import { AccessControlService } from '@services/access-control.service';
import { PagePreloaderService } from '@services/preloaders/page/page-preloader.service';
import { BrowserStorage } from '@services/storage/browser-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  attempts = 0;
  isCallback = false;
  userProfile: Object;
  failedLogin = false;
  idpError = false;
  noAccess = false;
  environmentErrorType = '';
  environmentError = false;
  isInitiateAutomaticKey = 'isInitiateAutomatic';
  tokenReceived = false;
  initialCheckDone: boolean;
  faTimesCircle = faTimesCircle;
  faArrowCircleRight = faArrowCircleRight;
  error: any;

  constructor(
    private authenticationService: AuthenticationService,
    private storage: BrowserStorage,
    private logger: LoggerService,
    private userService: UserService,
    private router: Router,
    private pagePreloaderService: PagePreloaderService,
    private browserStorage: BrowserStorage,
    private accessControlService: AccessControlService) {

    // create logging instance
    this.logger = this.logger.createInstance('LoginComponent');
    this.initialCheckDone = false;

    this.failedLogin = false;
    this.logger.debug('Starting login component');
    this.tokenReceived = false;

  }

  ngOnInit() {
    this.pagePreloaderService.show();
    if (this.isCallBack()) {
      this.pagePreloaderService.setLocalisedMessage('PagePreloader.ProcessingCallbackLogin');
      this.logger.debug('Is Callback');
      this.removeIsCallBackFlag();
      this.isCallback = true;
      // if its a callback and we have a valid access token
      // then we try to login
      if (this.authenticationService.hasValidToken()) {
        this.checkLoginStatus();
      } else {
        this.automaticLogin();
      }

    } else {
      this.isCallback = false;
      this.logger.debug('Is not callback. we check at once');
      this.initialCheck();
    }
  }

  initialCheck() {
    if (!this.initialCheckDone) {
      this.initialCheckDone = true;
      this.logger.debug('Performing initial check.');
      this.checkLoginStatus();
    }
  }

  checkLoginStatus(forced?: boolean): void {
    this.pagePreloaderService.setLocalisedMessage('PagePreloader.CheckLoginStatus');
    if (forced) {
      this.logger.debug('Forced check');
    }
    this.logger.debug('Checking login status.');
    this.removeIsCallBackFlag();
    if (this.isIdpErrored()) {
      this.processIdpFailed();
    } else if (this.isUserLoginErrored()) {
      this.processFailedLogin();
    } else if (this.isSettingsErrored()) {
      this.processSettingsError();
    } else {
      this.authenticationService.isSignedIn()
        .then(username => {
          if (username) {
            this.logger.debug('User is signed in.');
            this.pagePreloaderService.setLocalisedMessage('PagePreloader.GettingUserData');

            // refresh user and reload
            this.userService.isRecognised(username)
              .then(recognised => {

                if (recognised) {


                  // user returned successfully
                  this.failedLogin = false;
                  this.tokenReceived = true;

                  this.logger.debug('Success! Authenticated and recognised!');
                  this.userService.getUser()
                    .then(u => {
                      this.accessControlService.setAllowedModules(u.allowedModules);
                      const hasAnyAccess = this.accessControlService.checkAccessForSite();

                      if (hasAnyAccess) {
                        let url = this.authenticationService.getRedirectUri();
                        // check if there is a url from a redirect
                        if (url) {
                          this.logger.debug('There is a redirect Url stored. Using it : ' + url);
                          this.router.navigateByUrl(url);
                          this.authenticationService.removeRedirectUri();
                        } else {
                          url = this.accessControlService.getFirstAccessibleUrl();
                          if (url) {
                            // go to route and let router decide were to go
                            this.router.navigate([url]);
                          } else {
                            // something went wrong, user has no access
                            this.processFailedLoginNoAccess();
                          }
                        }
                      } else {
                        this.processFailedLoginNoAccess();
                      }
                    });
                } else {
                  this.logger.debug('the user is not recognised');
                  this.processFailedLogin();
                }

              })
              .catch(error => {
                // parent is a callback, but the user does not exist
                this.processFailedLogin();
              });
          } else {
            this.logger.debug('not signed in yet so ...');
            if (this.isCallback) {

              this.logger.debug('Its a callback so we show an error.');
              // parent.storage.remove(parent.isInitiateAutomaticKey);
              // parent is a callback, but the user does not exist
              this.processFailedLogin();

            } else {
              this.pagePreloaderService.setLocalisedMessage('PagePreloader.RedirectToIDP');
              this.logger.debug('It not a call back. So we initiate the login.');
              this.removeUserLoadErrorFlag();
              // we automatically send the user to the idp
              // if its not a callback
              this.automaticLogin();

            }
          }
        }).catch(err => {
          this.logger.debug('the user is not signed in');
          this.processFailedLogin();
        });
    }
  }

  processSettingsError() {
    this.failedLogin = false;
    this.idpError = false;
    this.noAccess = false;
    this.environmentError = true;
    this.error = undefined;
    this.logger.error('failed in settings');
    this.environmentErrorType = this.getSettingsError();
    this.pagePreloaderService.hide();
    this.tokenReceived = false;
    this.removeAllFlags();
    this.showForm(this);
  }

  processFailedLogin() {
    this.failedLogin = true;
    this.idpError = false;
    this.noAccess = false;
    this.environmentError = false;
    this.error = undefined;
    this.logger.error('failed to login');
    this.pagePreloaderService.hide();
    this.tokenReceived = false;
    this.removeAllFlags();
    this.showForm(this);
  }

  processFailedLoginNoAccess() {
    this.failedLogin = false;
    this.idpError = false;
    this.noAccess = true;
    this.environmentError = false;
    this.error = undefined;
    this.logger.error('user signed and recognised, but without features assigned.');
    this.pagePreloaderService.hide();
    this.tokenReceived = false;
    this.removeAllFlags();
    this.showForm(this);
  }

  processIdpFailed() {
    this.failedLogin = false;
    this.idpError = true;
    this.environmentError = false;
    this.noAccess = false;
    this.error = this.getIdpError();
    this.logger.error('failed to login due to idp', this.error);
    this.pagePreloaderService.hide();
    this.tokenReceived = false;
    this.removeAllFlags();
    this.showForm(this);
  }

  showForm(current: any) {
    current.pagePreloaderService.hide();
    $(function () {
      (<any>$('[data-toggle="tooltip"]')).tooltip();
    });
    $('#to-recover').on('click', function () {
      $('#loginform').slideUp();
      $('#recoverform').fadeIn();
    });
  }

  login() {
    this.removeAllFlags();
    window.location.href = './login';
  }

  automaticLogin() {
    this.removeAllFlags();
    this.authenticationService.automaticLogin()
      .then(isLoggedIn => {
        if (isLoggedIn) {
          this.checkLoginStatus(true);
        } else {
          this.logger.debug('User not logged in. Redirect required...');
        }
      })
      .catch(error => {
        // if for some reason the discovery document gave an error, then we log out
        this.logger.error('Discovery document load failed on automatic login attempt.', error);
        this.browserStorage.set(environment.Settings.browserStorageKeys.userLoadErrorKey, 'true');
        window.location.reload();
      });
  }

  logout() {
    this.authenticationService.signout();
  }

  getSettingsError(): string {
    return (this.browserStorage.get(environment.Settings.browserStorageKeys.environmentLoadingError));
  }

  isSettingsErrored(): boolean {
    return this.getSettingsError() ? true : false;
  }

  isUserLoginErrored(): boolean {
    return (this.browserStorage.get(environment.Settings.browserStorageKeys.userLoadErrorKey)) ? true : false;
  }

  removeIsCallBackFlag() {
    this.storage.remove(environment.Settings.browserStorageKeys.isIdpCallBack);
  }

  isCallBack(): boolean {
    return (this.storage.get(environment.Settings.browserStorageKeys.isIdpCallBack)) ? true : false;
  }

  removeUserLoadErrorFlag() {
    this.browserStorage.remove(environment.Settings.browserStorageKeys.userLoadErrorKey);
  }

  isIdpErrored(): boolean {
    return (this.browserStorage.get(environment.Settings.browserStorageKeys.idpError)) ? true : false;
  }

  getIdpError(): any {
    return this.browserStorage.getObject(environment.Settings.browserStorageKeys.idpError);
  }

  removeIdpErrorFlag() {
    this.browserStorage.remove(environment.Settings.browserStorageKeys.idpError);
  }

  removeEnvSettingsErrorFlag() {
    this.browserStorage.remove(environment.Settings.browserStorageKeys.environmentLoadingError);
  }

  removeAllFlags() {
    this.removeIdpErrorFlag();
    this.removeUserLoadErrorFlag();
    this.removeIsCallBackFlag();
    this.removeEnvSettingsErrorFlag();
  }
}
