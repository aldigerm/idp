import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './core/guards/auth.guard';
import { AuthLoginGuard } from './core/guards/auth.guard.login';
import { IdpCallbackComponent } from './modules/idp-callback/pages/idp-callback.component';
import { UserProfileComponent } from './modules/user-profile/pages/user-profile.component';
import { ProfileGuard } from './core/guards/profile.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: './modules/pages/pages.module#PagesModule',
        canActivate: [AuthGuard]
    },
    { path: 'user/profile', component: UserProfileComponent },
    { path: 'session/:sid', loadChildren: './modules/session/session.module#SessionModule' },
    { path: 'callback', component: IdpCallbackComponent },
    { path: 'login', loadChildren: './modules/login/login.module#LoginModule', canActivate: [AuthLoginGuard] },
    { path: '404', loadChildren: './modules/404/not-found.module#NotFoundModule' },
    { path: 'error', loadChildren: './modules/error/error.module#ErrorModule' },
    { path: '**', redirectTo: '404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes), NgbModule.forRoot()],
    exports: [RouterModule]
})
export class AppRoutingModule { }
