import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AccessControlService } from '@services/access-control.service';

@Directive({
  selector: '[appAccessControl]'
})
export class AccessControlDirective {

  private _setting = '';
  @Input() set appAccessControl(value: string) {
    this._setting = value;
    this.process(value);

  } get appAccessControl(): string {
    return this._setting;
  }

  constructor(
    private templateRef: TemplateRef<any>
    , private viewContainer: ViewContainerRef
    , private _accessControlService: AccessControlService) { }

  process(type: string): void {
    if (this.appAccessControl) {
      if (this._accessControlService.checkAccess(type)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        // Else remove template from DOM
        this.viewContainer.clear();
      }
    }
  }

}
