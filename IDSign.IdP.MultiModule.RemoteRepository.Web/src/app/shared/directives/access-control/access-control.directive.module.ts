import { NgModule } from '@angular/core';
import { AccessControlDirective } from './access-control.directive';
@NgModule({
   declarations: [
      AccessControlDirective
   ],
   exports: [
      AccessControlDirective
   ]
})
export class AccessControlModule { }
