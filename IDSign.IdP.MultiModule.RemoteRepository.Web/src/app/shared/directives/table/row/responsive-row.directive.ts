import { Directive, Input, ElementRef, Renderer2, OnInit } from '@angular/core';
import { ResponsiveTableDirective } from '../responsive-table.directive';
import { LoggerService } from '../../../../core/logging/logger.service';

@Directive({
  selector: '[appResponsiveRow]'
})
export class ResponsiveRowDirective {

  private _isMobile = false;
  private _isTablet = false;
  private _isDesktop = false;
  private _columnName = '';

  @Input() set columnName(value: string) {
    this._columnName = value;
    if (value) {
      this.logger = this.logger.createInstance('ResponsiveRowDirective:' + value);
      this.processDisplay();
    }
  } get columnName(): string {
    return this._columnName;
  }

  constructor(
    private ef: ElementRef
    , private renderer: Renderer2
    , private responsiveTable: ResponsiveTableDirective
    , private logger: LoggerService
  ) {
    this.processDisplay();
    this.subscribe();
  }

  hide() {
    this.logger.debug('Hiding');
    this.ef.nativeElement.hidden = true;
  }

  show() {
    this.logger.debug('Showing');
    this.ef.nativeElement.hidden = false;
  }

  processDisplay() {
    if (this.responsiveTable && this.columnName) {

      this._isMobile = !this.responsiveTable.appMobileColumns.includes(this.columnName);
      this._isTablet = !this.responsiveTable.appTabletColumns.includes(this.columnName);
      this._isDesktop = !this.responsiveTable.appDesktopColumns.includes(this.columnName);

      // show/hide on first load
      if ((this.responsiveTable.isMobile && this._isMobile)
        || (this.responsiveTable.isTablet && this._isTablet)
        || (this.responsiveTable.isDesktop && this._isDesktop)) {
        this.show();
      } else {
        this.hide();
      }

    }
  }

  subscribe() {
    if (this.responsiveTable) {

      this.responsiveTable.isMobileEmitter.subscribe((flag: boolean) => {
        if (flag && this._isMobile) {
          this.show();
        } else {
          this.hide();
        }
      });
      this.responsiveTable.isTabletEmitter.subscribe((flag: boolean) => {
        if (flag && this._isTablet) {
          this.show();
        } else {
          this.hide();
        }
      });
    }
  }
}
