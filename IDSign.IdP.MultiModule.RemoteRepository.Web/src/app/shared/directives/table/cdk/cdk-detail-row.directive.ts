import { Directive, HostBinding, HostListener, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { ResponsiveTableDirective } from '../responsive-table.directive';
import { LoggerService } from '@app/core/logging/logger.service';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[cdkDetailRow]'
})
export class CdkDetailRowDirective {
  private row: any;
  private tRef: TemplateRef<any>;
  private opened: boolean;
  private enabled: boolean;

  @HostBinding('class.expanded')
  get expended(): boolean {
    return this.opened;
  }

  @Input()
  set cdkDetailRow(value: any) {
    if (value !== this.row) {
      this.row = value;
      // this.render();
    }
  }

  @Input('cdkDetailRowTpl')
  set template(value: TemplateRef<any>) {
    if (value !== this.tRef) {
      this.tRef = value;
      // this.render();
    }
  }

  constructor(public vcRef: ViewContainerRef
    , private responsiveTable: ResponsiveTableDirective
    , private logger: LoggerService) {
    this.logger = logger.createInstance('CdkDetailRowDirective');
    this.processDisplay();
    this.subscribe();
  }

  @HostListener('click')
  onClick(): void {
    this.toggle();
  }

  toggle(): void {
    if (this.enabled) {
      if (this.opened) {
        this.close();
      } else {
        this.open();
      }
    }
  }

  private close() {
    this.opened = false;
    this.vcRef.clear();
  }

  private open() {
    this.opened = true;
    this.render();
  }

  private render(): void {
    this.vcRef.clear();
    if (this.tRef && this.row) {
      this.vcRef.createEmbeddedView(this.tRef, { $implicit: this.row });
    }
  }


  disableAndClose() {
    this.logger.debug('Disabling section');
    this.enabled = false;
    this.close();
  }

  enable() {
    this.logger.debug('Enablind section');
    this.enabled = true;
  }

  processDisplay() {
    if (this.responsiveTable) {
      // if there are hidden columns, then we enable the opening
      // of the detail row. otherwise we close if its open and we
      // disable it
      if (this.responsiveTable.hiddenColumns.length > 0) {
        this.enable();
      } else {
        this.disableAndClose();
      }
    }
  }

  subscribe() {
    if (this.responsiveTable) {

      this.responsiveTable.isMobileEmitter.subscribe((flag: boolean) => {
        this.processDisplay();
      });
      this.responsiveTable.isTabletEmitter.subscribe((flag: boolean) => {
        this.processDisplay();
      });
    }
  }
}
