import { Directive, ElementRef, Input, Output, ChangeDetectorRef, OnDestroy, EventEmitter, OnInit } from '@angular/core';
import { MatTable, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { LoggerService } from '@app/core/logging/logger.service';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, tap } from 'rxjs/operators';
import { ISubscription } from 'rxjs/Subscription';
import { detectChangesWithCheck } from '@app/sharedfunctions/change-detector.function';

@Directive({
  exportAs: 'responsiveTable',
  selector: '[appResponsiveTable]'
})
export class ResponsiveTableDirective implements OnInit, OnDestroy {

  private table: MatTable<any>;
  private _rowsSubscribe: ISubscription;
  private _smallLayoutSubscribe: ISubscription;
  private _mediumLayoutSubscribe: ISubscription;
  private _displayedColumns: string[];
  private _hiddenColuns: string[];
  private _columnValuesMapper: { [id: string]: string };
  private _filter: string;
  private _dataSource: MatTableDataSource<any>;
  private _rawDataSource: any;
  private _sort: MatSort;
  private _paginator: MatPaginator;
  private searchTimeout = setTimeout(function () { }, 0);

  private _isMobile = false;
  private _isTablet = false;
  private _appTabletColumns: string[];
  private _appMobileColumns: string[];

  public get isMobile(): boolean {
    return this._isMobile;
  }

  public get isTablet(): boolean {
    return this._isTablet;
  }

  public get isDesktop(): boolean {
    return !(this.isMobile && this.isTablet);
  }

  public get hiddenColumns(): string[] {
    return this._hiddenColuns;
  }

  @Input() formDataField = '';
  @Input() appInitialPageSize = 20;
  @Input() appSearchDelay = 500;
  @Input() appAllColumns: string[];
  @Input() appDesktopColumns: string[];
  @Input() set appTabletColumns(value: string[]) {
    this._appTabletColumns = value;
  } get appTabletColumns(): string[] {
    return this._appTabletColumns;
  }
  @Input() set appMobileColumns(value: string[]) {
    this._appMobileColumns = value;
  } get appMobileColumns(): string[] {
    return this._appMobileColumns;
  }

  @Input() set columnValuesMapper(value: { [id: string]: string }) {
    this._columnValuesMapper = value;
    this.setDataSource();
  } get columnValuesMapper(): { [id: string]: string } {
    return this._columnValuesMapper;
  }

  @Input() set appDataSource(value: any) {
    this._rawDataSource = value;
    this.setDataSource();
  }

  private set dataSource(value: MatTableDataSource<any>) {
    this._dataSource = value;
  } private get dataSource(): MatTableDataSource<any> {
    return this._dataSource;
  }

  @Input() set appSort(value: MatSort) {
    this._sort = value;
    this.setDataSource();
  } get appSort(): MatSort {
    return this._sort;
  }
  @Input() set appPaginator(value: MatPaginator) {
    this._paginator = value;
    this.setDataSource();
  } get appPaginator(): MatPaginator {
    return this._paginator;
  }
  @Input() set appFilter(value: string) {
    this._filter = value;
    this.applyFilter(this._filter);
  }
  @Output() appDisplayedColumns = new EventEmitter<string[]>();
  @Output() isMobileEmitter = new EventEmitter<boolean>();
  @Output() isTabletEmitter = new EventEmitter<boolean>();


  smallScreen$: Observable<boolean> = this.breakpointObserver
    .observe([Breakpoints.XSmall])
    .pipe(map(result => result.matches), tap(() => detectChangesWithCheck(this._changeDetectorRef)));

  mediumScreen$: Observable<boolean> = this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.Medium]) // medium to cover ipad pro
    .pipe(map(result => result.matches), tap(() => detectChangesWithCheck(this._changeDetectorRef)));

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  constructor(
    private el: MatTable<any>
    , private _logger: LoggerService
    , private breakpointObserver: BreakpointObserver
    , private _changeDetectorRef: ChangeDetectorRef) {
    this._logger = _logger.createInstance('AppResponsiveTableDirective');
    this.table = el;
    this.table.dataSource = [];
  }

  setDataSource() {
    if (this._rawDataSource) {
      if (this.columnValuesMapper) {
        this._rawDataSource.forEach(item => {
          Object.keys(this.columnValuesMapper).forEach(property => {
            item[property] = this.getPropertyValue(item, property);
          });
        });
      }
      this.dataSource = new MatTableDataSourceWithCustomSort(this._rawDataSource, this.formDataField);
      this.dataSource.paginator = this.appPaginator;
      this.dataSource.sort = this.appSort;
      this.dataSource.filterPredicate = this.filterPredicate;
      this.table.dataSource = this.dataSource;
      if (this.dataSource.paginator) {
        this.dataSource.paginator.pageSize = this.appInitialPageSize;
      }
      this.setupColumnSubs();
    }
  }

  filterPredicate(item: any, filterValue: string): boolean {
    let match = Object.keys(item).some(k => item[k] != null &&
      item[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase()));

    if (!match && this.formDataField) {
      const formDataString = item[this.formDataField];
      if (formDataString) {
        const formDataParsed = JSON.parse(item[this.formDataField]);
        match = Object.keys(formDataParsed).some(k => formDataParsed[k] != null &&
          formDataParsed[k].toString().toLowerCase()
            .includes(filterValue.toLowerCase()));
      }
    }
    return match;
  }


  getPropertyValue(item: any, property: string): any {
    // check if item is initialised
    if (!item || !property) {
      return '';
    }

    // check if we have mappings specified
    if (this.columnValuesMapper) {

      // check if we have a map for this property
      const mapper = this.columnValuesMapper[property];
      if (mapper) {

        // we do therefore we follow the path
        let value = item;
        for (let i = 0, path = mapper.split('.'), len = path.length; i < len; i++) {
          value = value[path[i]];
          if (!value) {
            this._logger.warn('property wasnt found for map:' + mapper + ' -> ' + path[i]);
            return '';
          }
        }

        // returning the value
        return value;
      } else {
        // we don't so we fall to default
        return item[property];
      }
    } else {
      return item[property];
    }
  }

  setupColumnSubs() {
    this._smallLayoutSubscribe = this.smallScreen$.subscribe(isMobile => {
      this._logger.debug('Mobile screen triggered.');
      this._isMobile = isMobile;
      this.isMobileEmitter.emit(isMobile);
      this.updateColums();
    });
    this._mediumLayoutSubscribe = this.mediumScreen$.subscribe(isTablet => {
      this._logger.debug('Tablet screen triggered.');
      this._isTablet = isTablet;
      this.isTabletEmitter.emit(isTablet);
      this.updateColums();
    });
  }

  hidePaginator() {
    if (this.appPaginator) {
      this.appPaginator.hidePageSize = true;
    }
  }

  showPaginator() {
    if (this.appPaginator) {
      this.appPaginator.hidePageSize = false;
    }
  }


  updateColums() {
    if (this._isMobile) {
      this._displayedColumns = this.appMobileColumns;
      this.hidePaginator();
    } else if (this._isTablet) {
      this._displayedColumns = this.appTabletColumns;
      this.showPaginator();
    } else {
      this._displayedColumns = this.appDesktopColumns;
      this.showPaginator();
    }

    // get hidden columns
    this._hiddenColuns = this.appAllColumns.filter(item =>
      this._displayedColumns.indexOf(item) === -1);

    this.appDisplayedColumns.emit(this._displayedColumns);
    detectChangesWithCheck(this._changeDetectorRef);
  }

  applyFilter(filterValue: string) {
    if (this.dataSource) {
      clearTimeout(this.searchTimeout);
      const parent = this;
      this.searchTimeout = setTimeout(function () {
        parent._logger.debug('Searching for ' + filterValue);
        parent.dataSource.filter = filterValue.trim().toLowerCase();
      }, this.appSearchDelay);
    }
  }

  ngOnInit(): void {
    this.setDataSource();
  }

  ngOnDestroy(): void {

    this._changeDetectorRef.detach();


    if (this._rowsSubscribe) {
      this._rowsSubscribe.unsubscribe();
    }
    if (this._smallLayoutSubscribe) {
      this._smallLayoutSubscribe.unsubscribe();
    }
    if (this._mediumLayoutSubscribe) {
      this._mediumLayoutSubscribe.unsubscribe();
    }
  }
}

export class MatTableDataSourceWithCustomSort<T> extends MatTableDataSource<T> {

  constructor(
    private initial: T[],
    private formDataField: string
  ) {
    super(initial);
  }

  sortData: ((data: T[], sort: MatSort) => T[]) = (data: T[], sort: MatSort): T[] => {
    const active = sort.active;
    const direction = sort.direction;
    if (!active || direction === '') { return data; }

    return data.sort((a, b) => {
      const valueA = this.customSortingDataAccessor(a, active);
      const valueB = this.customSortingDataAccessor(b, active);

      // If both valueA and valueB exist (truthy), then compare the two. Otherwise, check if
      // one value exists while the other doesn't. In this case, existing value should come first.
      // This avoids inconsistent results when comparing values to undefined/null.
      // If neither value exists, return 0 (equal).
      let comparatorResult = 0;
      if (valueA != null && valueB != null) {
        // Check if one value is greater than the other; if equal, comparatorResult should remain 0.
        if (valueA > valueB) {
          comparatorResult = 1;
        } else if (valueA < valueB) {
          comparatorResult = -1;
        }
      } else if (valueA != null) {
        comparatorResult = 1;
      } else if (valueB != null) {
        comparatorResult = -1;
      }

      return comparatorResult * (direction === 'asc' ? 1 : -1);
    });
  }


  customSortingDataAccessor(item: any, property: string, skipFormData?: boolean): any {
    // properties which end with "date" or "time" will be considered as
    // strings with a valid date
    const propertyName = property.toLowerCase();
    let value: any;
    if (item[property] !== undefined && (propertyName.endsWith('date') || propertyName.endsWith('time'))) {
      value = new Date(item[property]);
    } else {
      value = item[property];
    }

    // if we haven't found a match
    // and we in the item there is some more data
    // and this is the first try of the recursion
    // then we search in the form data
    if (!value && this.formDataField && !skipFormData) {
      const formDataString = item[this.formDataField];
      if (formDataString) {
        const formDataParsed = JSON.parse(item[this.formDataField]);
        return this.customSortingDataAccessor(formDataParsed, property, true);
      }
    } else {
      return value;
    }
  }
}
