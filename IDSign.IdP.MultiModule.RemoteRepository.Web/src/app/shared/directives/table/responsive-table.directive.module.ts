import { NgModule } from '@angular/core';
import { ResponsiveTableDirective } from './responsive-table.directive';
import { ResponsiveRowDirective } from './row/responsive-row.directive';
import { CdkDetailRowDirective } from './cdk/cdk-detail-row.directive';

@NgModule({
   declarations: [
      ResponsiveTableDirective,
      ResponsiveRowDirective,
      CdkDetailRowDirective
   ],
   exports: [
      ResponsiveTableDirective,
      ResponsiveRowDirective,
      CdkDetailRowDirective
   ]
})
export class ResponsiveTableModule { }
