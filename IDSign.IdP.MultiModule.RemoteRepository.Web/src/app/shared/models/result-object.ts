import { ErrorCode } from './error-code';

export interface ResultObject {
    errorCode: ErrorCode;
}
