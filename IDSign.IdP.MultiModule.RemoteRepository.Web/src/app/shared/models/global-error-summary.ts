export class GlobalErrorSummary {
    url: string;
    connectionId: string;
    time: Date;
    error: GlobalError;
}
export class GlobalError {
    message: string;
    name: string;
    stack: string;
}
