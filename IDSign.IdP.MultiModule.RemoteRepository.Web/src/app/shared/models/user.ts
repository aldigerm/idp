import { UserIdentifier } from './identifiers/user-identifier';
import { TenantIdentifier } from './identifiers/tenant-identifier';
import { ProjectIdentifier } from './identifiers/project-identifier';
import { ProjectBasicModel } from './remote-repository/project/remote-repository-project-basic';
import { TenantBasicModel } from './remote-repository/tenant/remote-repository-tenant-basic';
import { ProjectDetailModel } from './remote-repository/project/remote-repository-project-detail';
import { PasswordPolicyTypeBasicModel } from './remote-repository/password-policy/type/remote-repository-password-policy-type-basic';
import { PasswordPolicyBasicModel } from './remote-repository/password-policy/remote-repository-password-policy-basic';

export interface Claim {
    type: string;
    value: string;
    code: string;
}

export interface UserAccessDetail {
    projects: ProjectDetailModel[];
    tenants: TenantBasicModel[];
    users: UserIdentifier[];
    passwordPolicyTypes: PasswordPolicyTypeBasicModel[];
}
export interface User {
    identifier: UserIdentifier;
    oldUsername: string;
    title: string;
    firstName: string;
    MiddleName: string;
    lastName: string;
    dateOfBirth: Date;
    occupation: string;
    customAttributes: string;
    cityOfBirth: string;
    nins: string;
    emailAddress: string;
    mobileNumber: string;
    userProfile: UserProfile;
    enabled: boolean;
    repositoryUsername: string;
    userIdentifier: string;
    allowedModules: string[];
    claims: Claim[];
    userAccessDetail: UserAccessDetail;
    profileImage: string;
    isSuperUser: boolean;
    isAdmin: boolean;
}

export class UserProfile {
    public iss: string;
    public aud: string;
    public exp: number;
    public nbf: number;
    public nonce: string;
    public iat: number;
    public at_hash: string;
    public sid: string;
    public sub: string;
    public auth_time: number;
    public idp: string;
    public amr: string[];
    public name: string;
}
