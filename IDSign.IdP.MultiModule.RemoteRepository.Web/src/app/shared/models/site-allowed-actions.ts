export interface AllowedSiteActions {
    actions: Array<SiteAction>;
}

export enum SiteAction {
    NotSet = 0,
    AddPlaceholder = 1,
    DeletePlaceholder = 2,
    UploadDocument = 3,
    ActionDocument = 4,
    UpdateMerchantIds = 5,
    SetMCCCode = 6,
    DownloadRiskReport = 7,
    DownloadChecklistDocuments = 8
}

