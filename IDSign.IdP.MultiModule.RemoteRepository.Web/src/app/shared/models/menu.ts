export enum MenuId {
    Parent,
    UserList,
    UserDetail,
    UserCreate,
    UserProfileData,
    ProjectDetail,
    TenantDetail,
    TenantCreate,
    TenantList,
    ProjectList,
    ProjectCreate,
    ProjectParent,
    TenantParent,
    UserParent,
    ProjectData,
    TenantData,
    UserCurrent,
    Data,
    UserGroupCreate,
    UserGroupDetail,
    UserGroupList,
    UserGroupParent,
    UserGroupData,
    RoleCreate,
    RoleDetail,
    RoleList,
    RoleParent,
    RoleData,
    ModuleDetail,
    ModuleCreate,
    ModuleList,
    ModuleParent,
    ModuleData,
    UserClaimTypeCreate,
    UserClaimTypeDetail,
    UserClaimTypeList,
    UserClaimTypeParent,
    UserClaimTypeData,
    UserClaimCreate,
    UserClaimDetail,
    UserClaimList,
    UserClaimParent,
    UserClaimData,
    UserGroupClaimParent,
    UserGroupClaimCreate,
    UserGroupClaimList,
    UserGroupClaimTypeParent,
    UserGroupClaimTypeList,
    UserGroupClaimTypeCreate,
    RoleClaimTypeCreate,
    RoleClaimTypeParent,
    RoleClaimList,
    RoleClaimParent,
    RoleClaimCreate,
    RoleClaimTypeList,
    UserGroupClaimDetail,
    UserGroupClaimTypeDetail,
    RoleClaimDetail,
    RoleClaimTypeDetail,
    TenantClaimTypeCreate,
    TenantClaimCreate,
    TenantClaimParent,
    TenantClaimTypeDetail,
    TenantClaimDetail,
    TenantClaimTypeParent,
    TenantClaimTypeList,
    TenantClaimList,
    PasswordPolicyCreate,
    PasswordPolicyDetail,
    PasswordPolicyList,
    PasswordPolicyParent,
    PasswordPolicyData,
}
