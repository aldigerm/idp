import { ModuleBasicModel } from './remote-repository-module-basic';

export interface ModuleDetailModel extends ModuleBasicModel {
    moduleRoles: any[];
}
