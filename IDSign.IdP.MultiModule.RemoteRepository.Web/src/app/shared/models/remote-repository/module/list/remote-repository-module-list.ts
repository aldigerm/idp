import { ModuleBasicModel } from '../remote-repository-module-basic';
import { ModuleIdentifier } from '../../../identifiers/module-identifier';

export interface ModuleListModel {
    list: ModuleListItemModel[];
}
export class ModuleListItemModel implements ModuleBasicModel, ModuleIdentifier {
    identifier: ModuleIdentifier;
    get projectCode(): string { return this.identifier.projectCode; }
    get moduleCode(): string { return this.identifier.moduleCode; }
    description: string;
}
