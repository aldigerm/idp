import { ModuleIdentifier } from '../../identifiers/module-identifier';
export interface ModuleBasicModel {
    identifier: ModuleIdentifier;
    description: string;
}
