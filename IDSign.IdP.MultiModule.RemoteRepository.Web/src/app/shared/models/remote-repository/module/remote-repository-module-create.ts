import { ProjectIdentifier } from '@models/identifiers/project-identifier';

export interface ModuleCreateModel {
    projectIdentifier: ProjectIdentifier;
    moduleCode: string;
    description: string;
}
