import { ModuleBasicModel } from './remote-repository-module-basic';

export interface ModuleUpdateModel extends ModuleBasicModel {
    newModuleCode: string;
}
