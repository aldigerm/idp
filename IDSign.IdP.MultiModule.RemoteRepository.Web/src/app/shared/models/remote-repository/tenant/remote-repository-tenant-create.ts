import { TenantDetailModel } from './remote-repository-tenant-detail';
import { ErrorCode } from '@models/error-code';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
export interface TenantCreateModel {
    name: string;
    tenantCode: string;
    projectIdentifier: ProjectIdentifier;
}

export interface TenantCreateResult extends TenantDetailModel {
    status: ErrorCode;
}
