import { TenantBasicModel } from '../remote-repository-tenant-basic';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface TenantListModel {
    list: TenantListItemModel[];
}
export interface TenantListItemModel extends TenantBasicModel {

}
