import { TenantListModel } from './remote-repository-tenant-list';
export interface TenantListPageModel extends TenantListModel {
    total: number;
}
