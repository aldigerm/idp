import { TenantIdentifier } from '../../identifiers/tenant-identifier';
export interface TenantBasicModel {
    identifier: TenantIdentifier;
    id: string;
    name: string;
    enabled: boolean;
    loggedInUserTenant: boolean;
    userGroups: string[];
    userGroupsInferred: string[];
    roles: string[];
    usernames: string[];
    passwordPolicies: string[];
}
