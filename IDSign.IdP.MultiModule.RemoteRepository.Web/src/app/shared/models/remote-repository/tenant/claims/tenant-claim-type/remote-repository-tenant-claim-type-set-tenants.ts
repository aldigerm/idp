import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface TenantClaimTypeSetTenants {
    identifier: TenantClaimTypeIdentifier;
    tenants: TenantIdentifier[];
    value: string;
}
