import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { TenantBasicModel } from '../../remote-repository-tenant-basic';

export interface TenantClaimTypeCreateModel {
    projectIdentifier: ProjectIdentifier;
    type: string;
    description: string;
    tenants: TenantBasicModel[];
}
