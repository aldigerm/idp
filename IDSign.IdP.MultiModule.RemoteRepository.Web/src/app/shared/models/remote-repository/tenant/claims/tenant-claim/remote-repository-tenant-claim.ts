import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface TenantClaimDetailModel {
    identifier: TenantClaimIdentifier;
    tenantsWithSameClaim: TenantIdentifier[];
    description: string;
}
