import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';

export interface TenantClaimUpdateModel {
    identifier: TenantClaimIdentifier;
    newValue: string;
}
