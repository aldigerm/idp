import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';
import { TenantBasicModel } from '../../remote-repository-tenant-basic';

export interface TenantClaimTypeDetailModel {
    identifier: TenantClaimTypeIdentifier;
    description: string;
    tenants: TenantBasicModel[];
}
