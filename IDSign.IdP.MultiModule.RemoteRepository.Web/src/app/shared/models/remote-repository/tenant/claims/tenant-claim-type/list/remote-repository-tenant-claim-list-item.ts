import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';
import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';

export class TenantClaimTypeListItemModel implements TenantClaimTypeIdentifier {
    get projectCode(): string { return this.identifier.projectCode; }
    get type(): string { return this.identifier.type; }
    identifier: TenantClaimTypeIdentifier;
    description: string;
}
