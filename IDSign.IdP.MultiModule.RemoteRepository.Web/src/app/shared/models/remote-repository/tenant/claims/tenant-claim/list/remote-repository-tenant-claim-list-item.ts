import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';

export interface TenantClaimListItemModel {
    identifier: TenantClaimIdentifier;
    description: string;
}
