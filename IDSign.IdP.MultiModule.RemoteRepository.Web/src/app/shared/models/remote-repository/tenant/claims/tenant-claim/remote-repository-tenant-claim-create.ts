import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface TenantClaimCreateModel {
    tenantIdentifier: TenantIdentifier;
    tenantname: string;
    value: string;
}
