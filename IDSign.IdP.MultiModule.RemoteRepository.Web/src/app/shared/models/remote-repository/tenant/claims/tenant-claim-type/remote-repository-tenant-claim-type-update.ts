import { TenantClaimTypeDetailModel } from './remote-repository-tenant-claim-type';

export interface TenantClaimTypeUpdateModel extends TenantClaimTypeDetailModel {
    newType: string;
    newProjectIdentifier: string;
}
