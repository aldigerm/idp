import { TenantBasicModel } from './remote-repository-tenant-basic';

export interface TenantDetailModel extends TenantBasicModel {
    tenantGroups: any[];
    tenantRoles: any[];
}
