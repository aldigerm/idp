import { PasswordPolicyUserGroupIdentifier } from '@models/identifiers/password-policy-user-group-identifier';

export interface PasswordPolicyUserGroupBasicModel {
    identifier: PasswordPolicyUserGroupIdentifier;
}
