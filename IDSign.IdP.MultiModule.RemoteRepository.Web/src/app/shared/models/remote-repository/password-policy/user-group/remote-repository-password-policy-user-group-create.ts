import { ProjectIdentifier } from '@models/identifiers/project-identifier';

export interface PasswordPolicyUserGroupCreateModel {
    projectIdentifier: ProjectIdentifier;
    userGroups: string[];
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    profileImage: string;
    password: string;
    enabled: boolean;
    setPasswordOnFirstLogin: boolean;
}
