
import { PasswordPolicyUserGroupBasicModel } from './remote-repository-password-policy-user-group-basic';

export interface PasswordPolicyUserGroupDetailModel extends PasswordPolicyUserGroupBasicModel {
    data: string;
    defaultErrorMessage: string;
    description: string;

}
