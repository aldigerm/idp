import { PasswordPolicyTypeIdentifier } from '@models/identifiers/password-policy-type-identifier';

export interface PasswordPolicyTypeBasicModel {
    identifier: PasswordPolicyTypeIdentifier;
    description: string;
}
