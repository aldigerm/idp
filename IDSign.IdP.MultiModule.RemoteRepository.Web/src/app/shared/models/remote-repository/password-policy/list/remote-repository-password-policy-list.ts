import { PasswordPolicyBasicModel } from '../remote-repository-password-policy-basic';

export interface PasswordPolicyListModel {
    list: PasswordPolicyListItemModel[];
}

export interface PasswordPolicyListItemModel extends PasswordPolicyBasicModel {

}
