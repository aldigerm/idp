export enum RemoteRepositoryPasswordPolicyType {
    Regex = 'RegexMatch',
    MinimumLength = 'MinimumLength',
    MaximumLength = 'MaximumLength',
    History = 'History'
}
