import { PasswordPolicyBasicModel } from './remote-repository-password-policy-basic';

export interface PasswordPolicyDetailModel extends PasswordPolicyBasicModel {
    regex: string;
    length: number;
    historyLimit: number;
    defaultErrorMessage: string;
    description: string;
    data: string;
    passed: boolean;
}
