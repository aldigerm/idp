import { PasswordPolicyTypeIdentifier } from '@models/identifiers/password-policy-type-identifier';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';

export interface PasswordPolicyCreateModel {
    passwordPolicyTypeIdentifier: PasswordPolicyTypeIdentifier;
    projectIdentifier: ProjectIdentifier;
    code: string;
    data: string;
    defaultErrorMessage: string;
    description: string;
    regex: string;
    length: number;

}
