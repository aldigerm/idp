import { PasswordPolicyIdentifier } from '../../identifiers/password-policy-identifier';
import { PasswordPolicyTypeIdentifier } from '@models/identifiers/password-policy-type-identifier';
export interface PasswordPolicyBasicModel {
    identifier: PasswordPolicyIdentifier;
    passwordPolicyTypeIdentifier: PasswordPolicyTypeIdentifier;
    description: string;
}
