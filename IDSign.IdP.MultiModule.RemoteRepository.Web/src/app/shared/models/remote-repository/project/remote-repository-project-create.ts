import { ProjectDetailModel } from './remote-repository-project-detail';
import { ErrorCode } from '@models/error-code';
export interface ProjectCreateModel {
    name: string;
    projectCode: string;
}

export interface ProjectCreateResult extends ProjectDetailModel {
    status: ErrorCode;
}
