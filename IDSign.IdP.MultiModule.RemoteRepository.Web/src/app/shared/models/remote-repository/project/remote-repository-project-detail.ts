import { ProjectBasicModel } from './remote-repository-project-basic';
import { TenantBasicModel } from '../tenant/remote-repository-tenant-basic';
import { UserClaimTypeDetailModel } from '../user/claims/user-claim-type/remote-repository-user-claim-type';
import { UserGroupClaimTypeDetailModel } from '../user-group/claims/user-group-claim-type/remote-repository-user-group-claim-type';
import { RoleClaimTypeDetailModel } from '../role/claims/role-claim-type/remote-repository-role-claim-type';
import { TenantClaimTypeDetailModel } from '../tenant/claims/tenant-claim-type/remote-repository-tenant-claim-type';
export interface ProjectDetailModel extends ProjectBasicModel {
    tenants: TenantBasicModel[];
    userClaimTypes: string[];
    userClaimTypesDetail: UserClaimTypeDetailModel[];
    userGroupClaimTypesDetail: UserGroupClaimTypeDetailModel[];
    roleClaimTypesDetail: RoleClaimTypeDetailModel[];
    tenantClaimTypesDetail: TenantClaimTypeDetailModel[];
}
