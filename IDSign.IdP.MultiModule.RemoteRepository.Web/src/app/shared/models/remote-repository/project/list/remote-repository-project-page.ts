import { ProjectListModel } from './remote-repository-project-list';
export interface ProjectListPageModel extends ProjectListModel {
    total: number;
}
