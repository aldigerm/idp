import { ProjectBasicModel } from '../remote-repository-project-basic';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';

export interface ProjectListModel {
    list: ProjectListItemModel[];
}
export class ProjectListItemModel implements ProjectIdentifier {

    name: string;
    enabled: boolean;
    loggedInUserProject: boolean;
    identifier: ProjectIdentifier;
    get projectCode(): string { return this.identifier.projectCode; }
    description: string;
}
