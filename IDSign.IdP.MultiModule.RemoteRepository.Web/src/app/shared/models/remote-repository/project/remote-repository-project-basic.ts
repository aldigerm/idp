import { ProjectIdentifier } from '../../identifiers/project-identifier';
export interface ProjectBasicModel {
    identifier: ProjectIdentifier;
    id: string;
    name: string;
    enabled: boolean;
    loggedInUserProject: boolean;
    moduleCodes: string[];
}
