import { TenantIdentifier } from '../../identifiers/tenant-identifier';

export interface UserSetUserTenants {
    userGuid: string;
    tenants: TenantIdentifier[];
}
