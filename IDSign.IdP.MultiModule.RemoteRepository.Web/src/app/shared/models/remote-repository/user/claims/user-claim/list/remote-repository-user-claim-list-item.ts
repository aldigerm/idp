import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';

export interface UserClaimListItemModel {
    identifier: UserClaimIdentifier;
    description: string;
}
