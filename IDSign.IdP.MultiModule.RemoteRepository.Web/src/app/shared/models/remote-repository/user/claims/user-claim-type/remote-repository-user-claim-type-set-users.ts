import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';

export interface UserClaimTypeSetUsers {
    identifier: UserClaimTypeIdentifier;
    users: UserIdentifier[];
    value: string;
}
