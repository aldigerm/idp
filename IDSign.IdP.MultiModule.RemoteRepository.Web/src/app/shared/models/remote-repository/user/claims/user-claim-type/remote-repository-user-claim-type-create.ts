import { User } from '@models/user';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';

export interface UserClaimTypeCreateModel {
    projectIdentifier: ProjectIdentifier;
    type: string;
    description: string;
    users: User[];
}
