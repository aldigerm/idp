import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';

export class UserClaimTypeListItemModel implements UserClaimTypeIdentifier {
    get projectCode(): string { return this.identifier.projectCode; }
    get type(): string { return this.identifier.type; }
    identifier: UserClaimTypeIdentifier;
    description: string;
}
