import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface UserClaimCreateModel {
    tenantIdentifier: TenantIdentifier;
    username: string;
    value: string;
}
