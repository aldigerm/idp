import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';

export interface UserClaimSetUsers {
    users: UserIdentifier[];
    identifier: UserClaimIdentifier;
}
