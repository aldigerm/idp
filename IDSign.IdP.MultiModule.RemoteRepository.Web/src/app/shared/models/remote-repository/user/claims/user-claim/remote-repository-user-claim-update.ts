import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';

export interface UserClaimUpdateModel {
    identifier: UserClaimIdentifier;
    newValue: string;
}
