import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';
import { User } from '@models/user';

export interface UserClaimTypeDetailModel {
    identifier: UserClaimTypeIdentifier;
    description: string;
    users: User[];
}
