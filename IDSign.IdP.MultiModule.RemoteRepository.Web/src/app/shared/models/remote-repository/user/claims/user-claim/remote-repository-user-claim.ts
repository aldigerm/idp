import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';
import { UserIdentifier } from '@models/identifiers/user-identifier';

export interface UserClaimDetailModel {
    identifier: UserClaimIdentifier;
    usersWithSameClaim: UserIdentifier[];
    description: string;
}
