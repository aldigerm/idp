import { UserClaimTypeDetailModel } from './remote-repository-user-claim-type';

export interface UserClaimTypeUpdateModel extends UserClaimTypeDetailModel {
    newType: string;
    newProjectIdentifier: string;
}
