import { UserBasicModel } from './remote-repository-user-basic';
import { UserClaimListItemModel } from './claims/user-claim/list/remote-repository-user-claim-list-item';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { PasswordPolicyDetailModel } from '../password-policy/remote-repository-password-policy-detail';

export interface UserDetailModel extends UserBasicModel {
    isTenantSpecific: boolean;
    userGroups: string[];
    managedUserGroups: string[];
    userClaims: UserClaimListItemModel[];
    tenants: TenantIdentifier[];
    passwordPolicies: PasswordPolicyDetailModel[];
}
