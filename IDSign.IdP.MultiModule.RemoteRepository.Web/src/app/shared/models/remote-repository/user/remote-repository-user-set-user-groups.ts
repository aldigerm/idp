import { UserIdentifier } from '@models/identifiers/user-identifier';

export interface UserSetUserGroups extends UserIdentifier {
    identifier: UserIdentifier;
    userGroups: string[];
}
