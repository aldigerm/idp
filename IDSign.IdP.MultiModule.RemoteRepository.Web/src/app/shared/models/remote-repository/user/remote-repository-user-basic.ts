import { UserIdentifier } from '../../identifiers/user-identifier';
export interface UserBasicModel {
    identifier: UserIdentifier;
    id: string;
    firstName: string;
    lastName: string;
    enabled: boolean;
    isLoggedInUser: boolean;
    repositoryUsername: string;
    emailAddress: string;
    tenantUsername: string;
    setPasswordOnNextLogin: boolean;
}
