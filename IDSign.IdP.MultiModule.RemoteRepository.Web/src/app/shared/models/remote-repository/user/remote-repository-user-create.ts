import { UserDetailModel } from './remote-repository-user-detail';
import { ErrorCode } from '@models/error-code';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
export interface UserCreateModel {
    tenantIdentifier: TenantIdentifier;
    userGroups: string[];
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    profileImage: string;
    password: string;
    enabled: boolean;
    setPasswordOnFirstLogin: boolean;
}

export interface UserCreateResult extends UserDetailModel {
    status: ErrorCode;
}
