import { UserIdentifier } from '../../identifiers/user-identifier';

export interface UserSetPasswordModel {
    identifier: UserIdentifier;
    password: string;
}
