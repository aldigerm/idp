import { UserListModel } from './remote-repository-user-list';
export interface UserListPageModel extends UserListModel {
    total: number;
}
