import { UserBasicModel } from '../remote-repository-user-basic';
import { UserIdentifier } from '../../../identifiers/user-identifier';

export interface UserListModel {
    list: UserListItemModel[];
}

export interface UserListItemModel extends UserBasicModel {

}
