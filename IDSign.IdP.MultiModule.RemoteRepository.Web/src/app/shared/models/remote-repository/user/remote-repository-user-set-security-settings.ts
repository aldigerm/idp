import { UserIdentifier } from '../../identifiers/user-identifier';

export interface UserSetSecuritySettingsViewModel {
    identifier: UserIdentifier;
    setPasswordOnNextLogin: boolean;
}
