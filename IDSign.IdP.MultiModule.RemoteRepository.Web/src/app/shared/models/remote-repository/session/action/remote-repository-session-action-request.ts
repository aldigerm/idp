export interface UserTenantSessionActionRequestModel {
    identifier: string;
    data: string;
}
