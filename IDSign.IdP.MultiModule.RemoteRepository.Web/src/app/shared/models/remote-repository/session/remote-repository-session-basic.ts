export interface UserTenantSessionBasicModel {
    identifier: string;
    expiryDate: Date;
    userTenantSessionTypeCode: string;
}
