import { UserTenantSessionBasicModel } from './remote-repository-session-basic';
import { UserTenantIdentifier } from '@models/identifiers/user-tenant-identifier';

export interface UserTenantSessionDetailModel extends UserTenantSessionBasicModel {
    userTenantIdentifier: UserTenantIdentifier;
    data: string;
}
