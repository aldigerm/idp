import { RoleBasicModel } from './remote-repository-role-basic';

export interface RoleSetUserGroups extends RoleBasicModel {
    userGroups: string[];
}
