import { RoleBasicModel } from './remote-repository-role-basic';

export interface RoleDetailModel extends RoleBasicModel {
    userGroups: string[];
    inheritsFromRoles: string[];
    moduleCodes: string[];
}
