import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface RoleClaimCreateModel {
    tenantIdentifier: TenantIdentifier;
    rolename: string;
    value: string;
}
