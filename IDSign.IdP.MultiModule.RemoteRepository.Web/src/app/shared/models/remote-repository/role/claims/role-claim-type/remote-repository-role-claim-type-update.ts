import { RoleClaimTypeDetailModel } from './remote-repository-role-claim-type';

export interface RoleClaimTypeUpdateModel extends RoleClaimTypeDetailModel {
    newType: string;
    newProjectIdentifier: string;
}
