import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';
import { RoleIdentifier } from '@models/identifiers/role-identifier';

export interface RoleClaimTypeSetRoles {
    identifier: RoleClaimTypeIdentifier;
    roles: RoleIdentifier[];
    value: string;
}
