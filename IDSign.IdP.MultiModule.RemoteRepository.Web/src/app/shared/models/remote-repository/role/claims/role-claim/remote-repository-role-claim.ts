import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { RoleIdentifier } from '@models/identifiers/role-identifier';

export interface RoleClaimDetailModel {
    identifier: RoleClaimIdentifier;
    rolesWithSameClaim: RoleIdentifier[];
    description: string;
}
