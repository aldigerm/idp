import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';

export class RoleClaimTypeListItemModel implements RoleClaimTypeIdentifier {
    get projectCode(): string { return this.identifier.projectCode; }
    get type(): string { return this.identifier.type; }
    identifier: RoleClaimTypeIdentifier;
    description: string;
}
