import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';
import { RoleBasicModel } from '../../remote-repository-role-basic';

export interface RoleClaimTypeDetailModel {
    identifier: RoleClaimTypeIdentifier;
    description: string;
    roles: RoleBasicModel[];
}
