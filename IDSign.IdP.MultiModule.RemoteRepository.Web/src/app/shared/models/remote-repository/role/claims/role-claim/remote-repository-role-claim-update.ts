import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';

export interface RoleClaimUpdateModel {
    identifier: RoleClaimIdentifier;
    newValue: string;
}
