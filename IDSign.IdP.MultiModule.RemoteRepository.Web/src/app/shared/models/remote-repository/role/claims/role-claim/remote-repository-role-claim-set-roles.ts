import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';
import { RoleIdentifier } from '@models/identifiers/role-identifier';

export interface RoleClaimSetRoles {
    roles: RoleIdentifier[];
    identifier: RoleClaimIdentifier;
}
