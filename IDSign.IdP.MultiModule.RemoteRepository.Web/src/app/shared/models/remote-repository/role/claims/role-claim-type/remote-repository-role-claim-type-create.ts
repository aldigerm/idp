import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { RoleBasicModel } from '../../remote-repository-role-basic';

export interface RoleClaimTypeCreateModel {
    projectIdentifier: ProjectIdentifier;
    type: string;
    description: string;
    roles: RoleBasicModel[];
}
