import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';

export interface RoleClaimListItemModel {
    identifier: RoleClaimIdentifier;
    description: string;
}
