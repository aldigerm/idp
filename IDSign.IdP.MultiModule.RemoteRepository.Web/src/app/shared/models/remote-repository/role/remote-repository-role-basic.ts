import { RoleIdentifier } from '@models/identifiers/role-identifier';

export interface RoleBasicModel {
    identifier: RoleIdentifier;
    description: string;
}
