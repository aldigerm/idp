import { RoleBasicModel } from '../remote-repository-role-basic';
import { RoleIdentifier } from '@models/identifiers/role-identifier';

export interface RoleListModel {
    list: RoleListItemModel[];
}

export interface RoleListItemModel extends RoleBasicModel {
}
