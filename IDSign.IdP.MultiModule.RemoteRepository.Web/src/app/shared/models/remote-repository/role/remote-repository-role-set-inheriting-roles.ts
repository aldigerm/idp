import { RoleBasicModel } from './remote-repository-role-basic';

export interface RoleSetInheritingRoles extends RoleBasicModel {
    roles: string[];
}
