import { RoleBasicModel } from './remote-repository-role-basic';

export interface RoleSetModules extends RoleBasicModel {
    moduleCodes: string[];
}
