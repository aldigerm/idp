import { RoleBasicModel } from './remote-repository-role-basic';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface RoleCreateModel {
    roleCode: string;
    tenantIdentifier: TenantIdentifier;
}
