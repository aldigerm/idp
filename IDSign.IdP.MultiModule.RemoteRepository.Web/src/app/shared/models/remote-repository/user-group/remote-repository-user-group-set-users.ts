import { UserGroupBasicModel } from './remote-repository-user-group-basic';

export interface UserGroupSetUsers extends UserGroupBasicModel {
    usernames: string[];
}
