import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';

export interface UserGroupBasicModel {
    identifier: UserGroupIdentifier;
    description: string;
}
