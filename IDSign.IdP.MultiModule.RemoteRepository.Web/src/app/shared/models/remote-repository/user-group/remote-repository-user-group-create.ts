import { ErrorCode } from '@models/error-code';
import { UserGroupBasicModel } from './remote-repository-user-group-basic';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
export interface UserGroupCreateModel extends UserGroupBasicModel {
    tenantIdentifier: TenantIdentifier;
    userGroupCode: string;
    description: string;
}
