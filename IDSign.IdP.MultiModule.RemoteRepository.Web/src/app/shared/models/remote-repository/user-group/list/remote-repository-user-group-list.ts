import { UserGroupBasicModel } from '../remote-repository-user-group-basic';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';

export interface UserGroupListModel {
    list: UserGroupListItemModel[];
}

export interface UserGroupListItemModel extends UserGroupBasicModel {
}
