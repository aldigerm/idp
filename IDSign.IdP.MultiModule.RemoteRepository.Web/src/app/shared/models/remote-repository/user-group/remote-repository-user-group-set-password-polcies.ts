import { UserGroupBasicModel } from './remote-repository-user-group-basic';

export interface UserGroupSetPasswordPolicies extends UserGroupBasicModel {
    passwordPolicies: string[];
}
