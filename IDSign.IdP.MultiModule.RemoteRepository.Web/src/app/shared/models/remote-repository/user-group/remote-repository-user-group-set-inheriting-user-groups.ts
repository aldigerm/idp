import { UserGroupBasicModel } from './remote-repository-user-group-basic';

export interface UserGroupSetInheritingUserGroups extends UserGroupBasicModel {
    userGroups: string[];
}
