import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';

export interface UserGroupClaimTypeSetUserGroups {
    identifier: UserGroupClaimTypeIdentifier;
    UserGroups: UserGroupIdentifier[];
    value: string;
}
