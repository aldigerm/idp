import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';

export interface UserGroupClaimListItemModel {
    identifier: UserGroupClaimIdentifier;
    description: string;
}
