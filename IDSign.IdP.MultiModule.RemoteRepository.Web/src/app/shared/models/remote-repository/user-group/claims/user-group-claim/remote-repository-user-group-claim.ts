import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';

export interface UserGroupClaimDetailModel {
    identifier: UserGroupClaimIdentifier;
    userGroupsWithSameClaim: UserGroupIdentifier[];
    description: string;
}
