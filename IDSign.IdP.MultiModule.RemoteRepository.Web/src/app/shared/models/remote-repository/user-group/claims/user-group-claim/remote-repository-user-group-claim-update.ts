import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';

export interface UserGroupClaimUpdateModel {
    identifier: UserGroupClaimIdentifier;
    newValue: string;
}
