import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { UserGroupBasicModel } from '../../remote-repository-user-group-basic';

export interface UserGroupClaimTypeCreateModel {
    projectIdentifier: ProjectIdentifier;
    type: string;
    description: string;
    UserGroups: UserGroupBasicModel[];
}
