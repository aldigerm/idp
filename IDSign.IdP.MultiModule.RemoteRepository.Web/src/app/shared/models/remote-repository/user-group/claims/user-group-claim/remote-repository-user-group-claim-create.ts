import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';
import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';

export interface UserGroupClaimCreateModel {
    tenantIdentifier: TenantIdentifier;
    UserGroupCode: string;
    value: string;
}
