import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';
import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';

export class UserGroupClaimTypeListItemModel implements UserGroupClaimTypeIdentifier {
    get projectCode(): string { return this.identifier.projectCode; }
    get type(): string { return this.identifier.type; }
    identifier: UserGroupClaimTypeIdentifier;
    description: string;
}
