import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';
import { UserGroupBasicModel } from '../../remote-repository-user-group-basic';

export interface UserGroupClaimTypeDetailModel {
    identifier: UserGroupClaimTypeIdentifier;
    description: string;
    userGroups: UserGroupBasicModel[];
}
