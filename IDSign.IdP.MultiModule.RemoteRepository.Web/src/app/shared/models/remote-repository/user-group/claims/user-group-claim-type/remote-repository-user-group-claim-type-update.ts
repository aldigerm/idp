import { UserGroupClaimTypeDetailModel } from './remote-repository-user-group-claim-type';

export interface UserGroupClaimTypeUpdateModel extends UserGroupClaimTypeDetailModel {
    newType: string;
    newProjectIdentifier: string;
}
