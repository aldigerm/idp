import { UserGroupBasicModel } from './remote-repository-user-group-basic';

export interface UserGroupDetailModel extends UserGroupBasicModel {
    inheritsFromUserGroups: string[];
    usernames: string[];
    passwordPolicies: string[];
}
