export class IdSignConfig {
    public signingMode: string;
    public internationalUser: string;
    public readonly isInternationalUser: boolean = (this.internationalUser === 'true');
    public fiscalCode: string;
    public userInterface = new UserInterface();
}

export class UserInterface {
    public viewSuite: string;
    public viewTheme: string;
}
