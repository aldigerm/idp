import { ProjectIdentifier } from './project-identifier';

export class TenantIdentifier {
    projectCode: string;
    tenantCode: string;
    constructor(
        tenantCode: string,
        projectCode: string) {
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
