import { TenantIdentifier } from './tenant-identifier';

export class UserGroupIdentifier {
    tenantCode: string;
    projectCode: string;
    userGroupCode: string;
    constructor(
        userGroupCode: string,
        tenantCode: string,
        projectCode: string
    ) {
        this.userGroupCode = userGroupCode;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
