import { ProjectIdentifier } from './project-identifier';

export class ModuleIdentifier {
    projectCode: string;
    moduleCode: string;

    constructor(
        moduleCode: string,
        projectCode: string) {
        this.moduleCode = moduleCode;
        this.projectCode = projectCode;
    }
}
