export class PasswordPolicyIdentifier {
    passwordPolicyCode: string;
    tenantCode: string;
    projectCode: string;
    constructor(
        passwordPolicyCode?: string,
        tenantCode?: string,
        projectCode?: string) {
        this.passwordPolicyCode = passwordPolicyCode;
        this.projectCode = projectCode;
        this.tenantCode = tenantCode;
    }
}
