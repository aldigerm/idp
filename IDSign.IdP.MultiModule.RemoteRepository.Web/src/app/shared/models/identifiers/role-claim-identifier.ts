export class RoleClaimIdentifier {
    roleCode: string;
    tenantCode: string;
    projectCode: string;
    type: string;
    value: string;
    constructor(
        roleCode: string,
        tenantCode: string,
        projectCode: string,
        type: string,
        value: string) {
        this.roleCode = roleCode;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
        this.type = type;
        this.value = value;
    }
}
