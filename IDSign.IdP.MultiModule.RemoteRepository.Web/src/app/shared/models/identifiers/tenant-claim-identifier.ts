export class TenantClaimIdentifier {
    tenantCode: string;
    projectCode: string;
    type: string;
    value: string;
    constructor(
        tenantCode: string,
        projectCode: string,
        type: string,
        value: string) {
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
        this.type = type;
        this.value = value;
    }
}
