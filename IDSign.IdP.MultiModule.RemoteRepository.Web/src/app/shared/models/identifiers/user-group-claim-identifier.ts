export class UserGroupClaimIdentifier {
    userGroupCode: string;
    tenantCode: string;
    projectCode: string;
    type: string;
    value: string;
    constructor(
        userGroupCode: string,
        tenantCode: string,
        projectCode: string,
        type: string,
        value: string) {
        this.userGroupCode = userGroupCode;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
        this.type = type;
        this.value = value;
    }
}
