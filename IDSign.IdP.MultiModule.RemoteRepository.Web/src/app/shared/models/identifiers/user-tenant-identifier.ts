export class UserTenantIdentifier {
    tenantCode: string;
    projectCode: string;
    username: string;
    constructor(
        username?: string,
        tenantCode?: string,
        projectCode?: string
    ) {
        this.username = username;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
