import { PasswordPolicyIdentifier } from './password-policy-identifier';
import { UserGroupIdentifier } from './user-group-identifier';

export class PasswordPolicyUserGroupIdentifier {

    passwordPolicyCode: string;
    passwordPolicyTypeCode: string;
    userGroupCode: string;
    tenantCode: string;
    projectCode: string;

    constructor(
        passwordPolicyCode?: string,
        passwordPolicyTypeCode?: string,
        userGroupCode?: string,
        tenantCode?: string,
        projectCode?: string
    ) {
        this.passwordPolicyCode = passwordPolicyCode;
        this.passwordPolicyTypeCode = passwordPolicyTypeCode;
        this.userGroupCode = userGroupCode;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
