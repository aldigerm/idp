import { TenantIdentifier } from './tenant-identifier';

export class RoleIdentifier {
    tenantCode: string;
    roleCode: string;
    projectCode: string;
    constructor(
        roleCode?: string,
        tenantCode?: string,
        projectCode?: string) {
        this.roleCode = roleCode;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
