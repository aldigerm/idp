export class PasswordPolicyTypeIdentifier {
    passwordPolicyTypeCode: string;
    constructor(
        passwordPolicyTypeCode?: string
    ) {
        this.passwordPolicyTypeCode = passwordPolicyTypeCode;
    }
}
