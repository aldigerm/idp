import { ProjectIdentifier } from './project-identifier';

export class UserClaimTypeIdentifier {
    projectCode: string;
    type: string;
    constructor(
        type: string,
        projectCode: string
    ) {
        this.type = type;
        this.projectCode = projectCode;
    }
}
