import { TenantIdentifier } from './tenant-identifier';

export class UserIdentifier {
    userGuid: string;
    tenantCode: string;
    projectCode: string;
    username: string;
    constructor(
        username?: string,
        tenantCode?: string,
        projectCode?: string
    ) {
        this.username = username;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
    }
}
