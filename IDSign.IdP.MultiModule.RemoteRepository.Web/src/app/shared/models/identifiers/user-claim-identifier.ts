import { UserIdentifier } from './user-identifier';
import { UserClaimTypeIdentifier } from './user-claim-type-identifier';

export class UserClaimIdentifier {
    username: string;
    tenantCode: string;
    projectCode: string;
    type: string;
    value: string;
    constructor(
        username: string,
        tenantCode: string,
        projectCode: string,
        type: string,
        value: string) {
        this.username = username;
        this.tenantCode = tenantCode;
        this.projectCode = projectCode;
        this.type = type;
        this.value = value;
    }
}
