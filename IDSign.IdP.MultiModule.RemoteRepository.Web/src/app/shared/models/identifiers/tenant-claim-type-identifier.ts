export class TenantClaimTypeIdentifier {
    projectCode: string;
    type: string;
    constructor(
        type: string,
        projectCode: string
    ) {
        this.type = type;
        this.projectCode = projectCode;
    }
}
