export class UserGroupClaimTypeIdentifier {
    projectCode: string;
    type: string;
    constructor(
        type: string,
        projectCode: string
    ) {
        this.type = type;
        this.projectCode = projectCode;
    }
}
