
export enum DisplayState {
  Loading,
  NoneFound,
  Show,
  Forbidden
}

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37
}

export enum FileUploadLocation {
  UserProfileImage = 'userProfileImage'
}
