export enum SweetAlertType {
    Error,
    Success,
    Confirm,
    Processing,
    Close,
    SuccessWithOption,
    ErrorWithOption
}

export class SweetAlertRequest {

    public type: SweetAlertType;
    public title: string;
    public errorCode: string;
    public message: string;
    public messageParams: any;
    public cancelButton: string;
    public confirmButton: string;
    public confirmButtonColor: string;
    public cancelButtonColor: string;
    public successCallback: () => any;
    public cancelCallback: () => any;

    constructor(options: {
        type?: SweetAlertType,
        title?: string,
        message?: string,
        messageParams?: any,
        cancelButton?: string,
        confirmButton?: string,
        confirmButtonColor?: string,
        cancelButtonColor?: string,
        errorCode?: string,
        successCallback?: () => any,
        cancelCallback?: () => any
    }) {
        this.type = options.type;
        this.title = options.title ? options.title : 'notprovided';
        this.message = options.message ? options.message : 'notprovided';
        this.cancelButton = options.cancelButton ? options.cancelButton : 'notprovided';
        this.confirmButton = options.confirmButton ? options.confirmButton : 'notprovided';
        this.messageParams = options.messageParams ? options.messageParams : {};
        this.cancelButtonColor = options.cancelButtonColor ? options.cancelButtonColor : undefined;
        this.confirmButtonColor = options.confirmButtonColor ? options.confirmButtonColor : undefined;
        this.errorCode = options.errorCode ? options.errorCode : '';
        this.successCallback = options.successCallback ? options.successCallback : () => this.noCallBack();
        this.cancelCallback = options.cancelCallback ? options.cancelCallback : () => this.noCallBack();
    }

    noCallBack() {

    }
}
