import { MultilevelNodes, Configuration } from '@app/shared/components/ng-material-multilevel-menu/app.model';
import { Router } from '@angular/router';
import { Component, AfterViewInit, ViewChild, ElementRef, OnDestroy, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { MenuService } from '@services/menu.service';
import { MenuId } from '@models/menu';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
declare const $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements AfterViewInit, OnDestroy {
    config: Configuration = {
        paddingAtStart: true,
        interfaceWithRoute: true,
        classname: 'custom-material-theme',
        listBackgroundColor: `#ffffff`,
        fontColor: `#000000`,
        defaultListClass: `sidenav-listitem`,
        selectedListClass: `sidenav-listitem active`,
    };
    menuData: MultilevelNodes;
    menuSubscriber: ISubscription;

    constructor(
        private _logger: LoggerService,
        private _menuService: MenuService,
        private _siteChangeDetector: SiteChangeDetectorService,
        private _changeDetectionRef: ChangeDetectorRef
    ) {
        this._logger = _logger.createInstance('SidebarMobileComponent');
        this.menuData = <MultilevelNodes>{};
        this.menuData.items = new Array<MultilevelNodes>();
    }

    ngOnDestroy() {
        if (this.menuSubscriber) {
            this.menuSubscriber.unsubscribe();
        }
    }

    ngAfterViewInit() {
        this.menuData = this._menuService.getMenu();
        this._siteChangeDetector.detectChanges(this._changeDetectionRef);
        this.menuSubscriber = this._menuService.menuChange().subscribe(result => {
            if (result) {
                this._logger.debug('Menu is set ' + JSON.stringify(result));
                this.menuData = result;
            }
        });
    }
}
