import { NgMaterialMultilevelMenuModule } from './../ng-material-multilevel-menu/ng-material-multilevel-menu.module';
import { environment } from '@environments/environment';
import { RouterModule } from '@angular/router';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './sidebar.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule(
  {
    imports: [
      CommonModule,
      AccessControlPipeModule,
      TranslateModule,
      RouterModule,
      NgMaterialMultilevelMenuModule
    ],
    declarations: [
      SidebarComponent
    ],
    exports: [
      SidebarComponent
    ]
  }
)
export class SidebarModule {
}
