import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { FadeBoolAnimation } from '@app/shared/animations/fade-bool-animation';
import { detectChangesWithCheck } from '@app/sharedfunctions/change-detector.function';
import { timer } from 'rxjs';

@Component({
  selector: 'app-page-preloader',
  templateUrl: './page-preloader.component.html',
  styleUrls: ['./page-preloader.component.scss'],
  animations: [FadeBoolAnimation]
})
export class PagePreloaderComponent implements OnInit, AfterViewInit {
  changeLog: string[] = [];
  canChange = false;
  actionAffected = false;
  message = '';
  animationState = 'in';

  @Input() public shown: boolean;

  constructor(
    private logger: LoggerService
    , private _changeDetectorRef: ChangeDetectorRef
  ) {
    this.logger = this.logger.createInstance('PagePreloaderComponent');
  }

  ngOnInit() {
    this.shown = false;
  }

  ngAfterViewInit() {
    this.canChange = true;
  }

  setLoaderState(state: boolean) {
    this.shown = state;
    this.checkLoaderState();
    timer(0, 5000)
      .subscribe(() => {
        this.checkLoaderState();
      });
  }

  setLoaderMessage(message: string) {
    this.message = message;
    detectChangesWithCheck(this._changeDetectorRef);
  }

  checkLoaderState() {
    if (this.shown === true) {
      this.fadeInPreloader();
    } else {
      this.fadeOutPreLoader();
    }
  }

  fadeInPreloader() {
    if (this.canChange) {
      // if it is visible already, do not show it
      if ((+($('#pagepreloader').css('opacity')) <= 1)) {
        this.logger.debug('showing preloader.');
        $('.preloader.page').stop().hide();
        $('#pagepreloader').stop().css('opacity', '1');
        $('#pagepreloader').stop().css('pointer-events', 'all');
        this.actionAffected = true;
      }
    } else {
      this.logger.debug('cant show');
    }
  }

  fadeOutPreLoader() {
    if (this.canChange) {
      // if it is visible, then hide it
      if (+($('#pagepreloader').css('opacity')) !== 0) {
        this.logger.debug('fading out preloader');
        $('.preloader.page').stop().hide();
        $('#pagepreloader').stop().css('opacity', '0');
        $('#pagepreloader').stop().css('pointer-events', 'none');
        this.actionAffected = false;
      }
    } else {
      this.logger.debug('cant fade out');
    }
  }
}
