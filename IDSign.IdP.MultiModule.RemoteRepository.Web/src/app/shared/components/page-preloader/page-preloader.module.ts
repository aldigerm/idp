import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { PagePreloaderComponent } from './page-preloader.component';


@NgModule({
    imports: [
        CommonModule,
        SpinnerModule
    ],
    declarations: [PagePreloaderComponent],
    exports: [PagePreloaderComponent]
})
export class PagePreloaderModule {
}
