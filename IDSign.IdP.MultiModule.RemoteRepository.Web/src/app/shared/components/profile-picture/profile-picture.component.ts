import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { RemoteRepositoryUserService } from '@http/remote-repository/user/remote-repository-user.service';
import { UserIdentifier } from '@models/identifiers/user-identifier';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.css']
})
export class ProfilePictureComponent implements OnInit {

  private _username: UserIdentifier;
  private _data = '';
  defaultUrl = 'assets/images/users/default.png';
  @Input() asIcon = false;
  @Input() asBox = false;
  @Input() set userIdentifier(value: UserIdentifier) {
    if (value !== this._username) {
      this._username = value;
      if (value) {
        this.loadImage(value);
      }
    }
  }
  get userIdentifier(): UserIdentifier {
    return this._username;
  }
  @ViewChild('image', { static: true }) image: ElementRef;

  @Input() set url(value: string) {
    if (this.image && value) {
      const image = this.image.nativeElement as HTMLImageElement;
      image.src = value;
    }
  }
  @Input() set data(value: string) {
    this._data = value;
    if (this.image && value) {
      const image = this.image.nativeElement as HTMLImageElement;
      image.src = value.startsWith('data:image') ? value : 'data:image/jpeg;base64,' + value;
    }
  } get data(): string {
    return this._data;
  }
  @Output() imageLoaded = new EventEmitter<any>();

  constructor(
    private _remoteUserRepositoryService: RemoteRepositoryUserService,
    private _logger: LoggerService) {
    this._logger = this._logger.createInstance('ProfilePictureComponent');
  }

  ngOnInit() {
  }

  public loadImage(identifier?: UserIdentifier) {
    identifier = identifier ? identifier : this.userIdentifier;
    if (identifier) {
      // we hide any current url
      this.url = this.defaultUrl;
      this._remoteUserRepositoryService.getUserProfileImage(identifier)
        .then(profile => {
          if (profile) {
            this.data = profile;
            this.imageLoaded.emit(profile);
          }
        })
        .catch(error => {
          this._logger.warn('Couldnt get profile picture', error);
        });
    }
  }

}
