import { LoggerService } from '@app/core/logging/logger.service';
import { Observable ,  timer } from 'rxjs';
import {
  Component,
  OnInit,
  Input,
  AfterViewInit
} from '@angular/core';
import { FadeBoolAnimation } from 'app/shared/animations/fade-bool-animation';

@Component({
  selector: 'app-section-preloader',
  templateUrl: './section-preloader.component.html',
  styleUrls: ['./section-preloader.component.scss'],
  animations: [FadeBoolAnimation]
})
export class SectionPreloaderComponent implements OnInit, AfterViewInit {
  changeLog: string[] = [];
  canChange = false;
  actionAffected = false;
  message = '';
  animationState = 'in';

  @Input() public shown: boolean;

  constructor(
    private logger: LoggerService
  ) {
    this.logger = this.logger.createInstance('SectionPreloaderComponent');
  }

  ngOnInit() {
    this.shown = false;
  }

  ngAfterViewInit() {
    this.canChange = true;
  }

  setLoaderState(state: boolean) {
    this.shown = state;
    this.checkLoaderState();
    timer(0, 5000)
      .subscribe(() => {
        this.checkLoaderState();
      });
  }

  setLoaderMessage(message: string) {
    this.message = message;
  }

  checkLoaderState() {
    if (this.shown === true) {
      this.fadeInPreloader();
    } else {
      this.fadeOutPreLoader();
    }
  }

  fadeInPreloader() {
    if (this.canChange) {
      // if it is visible already, do not show it
      if ((+($('#sectionpreloader').css('opacity')) <= 1)) {
        this.logger.debug('showing preloader.');
        $('#sectionpreloader').stop().css('opacity', '1');
        $('#sectionpreloader').stop().css('pointer-events', 'all');
        this.actionAffected = true;
      }
    } else {
      this.logger.debug('cant show');
    }
  }

  fadeOutPreLoader() {
    if (this.canChange) {
      // if it is visible, then hide it
      if (+($('#sectionpreloader').css('opacity')) !== 0) {
        this.logger.debug('fading out preloader');
        $('#sectionpreloader').stop().css('opacity', '0');
        $('#sectionpreloader').stop().css('pointer-events', 'none');
        this.actionAffected = false;
      }
    } else {
      this.logger.debug('cant fade out');
    }
  }
}
