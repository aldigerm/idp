import { SectionPreloaderComponent } from './components/section-preloader.component';
import { SpinnerModule } from '@app/shared/components/spinner/spinner.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


@NgModule({
    imports: [
        CommonModule,
        SpinnerModule
    ],
    declarations: [SectionPreloaderComponent],
    exports: [SectionPreloaderComponent]
})
export class SectionPreloaderModule {
}
