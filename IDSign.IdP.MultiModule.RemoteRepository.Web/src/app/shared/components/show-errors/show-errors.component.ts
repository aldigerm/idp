import { Component, Input } from '@angular/core';
import { AbstractControl, AbstractControlDirective } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { LoggerService } from '@app/core/logging/logger.service';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-show-errors',
  template: `
  <div class='form-group has-danger' *ngIf='shouldShowErrors'>
    <div class='form-control-feedback' style='color: red' *ngFor='let error of errorList'>{{error}}</div>
  </div>
  `,
})
export class ShowErrorsComponent {

  private _control: AbstractControlDirective | AbstractControl;
  private _shouldShowErrors = false;
  private _errors: string[];
  private _touched: boolean;

  @Input() errorMessage = '';
  @Input() localizationParams: any = {};

  @Input()
  private set control(value: AbstractControlDirective | AbstractControl) {
    this._control = value;
    if (this._control) {
      this._control.statusChanges.subscribe(status => {
        // we update the error list if the status changed
        this.listOfErrors();
      });
    }
  } private get control(): AbstractControlDirective | AbstractControl {
    return this._control;
  }

  set errorList(value: string[]) {
    this._errors = value;
  } get errorList(): string[] {
    return this._errors;
  }

  constructor(
    private translate: TranslateService,
    private logger: LoggerService) {

    this.logger = this.logger.createInstance('ShowErrorsComponent');
    this.translate.onLangChange.subscribe(() => {
      // we update the errors if there is a change in language
      this.listOfErrors();
    });
  }

  get shouldShowErrors(): boolean {
    // we see if it should be shown
    const show: boolean = this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched);

    // we only get the translated errors if
    // there was a change in the current show status
    // and that the errors should be shown
    if (show !== this._shouldShowErrors && show) {
      // this will be called once every change
      // in show status
      this.listOfErrors();
    }

    // we assign the show status as the current value
    this._shouldShowErrors = show;
    return this._shouldShowErrors;
  }

  listOfErrors(): void {
    this.errorList = new Array<string>();
    if (this.control && this.control.errors != null) {
      Object.keys(this.control.errors)
        .map(field => {
          this.getMessage(field, this.control.errors[field])
            .subscribe(translation => {
              this.errorList.push(translation);
            });
        });
    }
    this._errors = this.errorList;
  }

  private getMessage(type: string, params: any): Observable<string> {
    let message = '';
    let param1Value = '';
    let typeName = type;
    switch (type) {
      case 'minlength': param1Value = params.requiredLength;
        break;
      case 'maxlength': param1Value = params.requiredLength;
        break;
      case 'pattern':
        let patternName = 'generic';
        if ((this.control as any).name) {
          switch ((this.control as any).name.toLowerCase()) {
            case 'companycode':
            case 'companyname':
              patternName = 'alphabetonly';
              break;
            case 'firstname':
            case 'lastname':
              patternName = 'alphabetonly'; // 'unicode';
              break;
            case 'email':
            case 'emailaddress':
              patternName = 'email';
              break;
            default:
              patternName = 'generic';
              break;
          }
        } else {
          patternName = 'generic';
        }
        typeName = type + '.' + patternName;
        param1Value = params.requiredPattern;
        break;
      default:
        param1Value = null;
        break;
    }
    const variables = { 'param1': param1Value };
    Object.assign(variables, this.localizationParams);

    if (this.errorMessage) {
      message = this.errorMessage;
    } else {
      this.translate.get('ValidationErrorMessages.' + typeName, variables)
        .subscribe(translation => {
          message = translation;
        });
    }
    return of(message);
  }
}
