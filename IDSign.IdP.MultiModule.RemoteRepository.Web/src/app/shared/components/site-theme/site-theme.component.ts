import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { DOCUMENT } from "@angular/common";
import { LoggerService } from '@app/core/logging/logger.service';
import { environment } from '@environments/environment';
import { IdSignConfig } from '@models/idsign-config';
import { QueryStringUtilitiesService } from '@services/query-string-utilities.service';
import { SiteConfiguratorService } from '@services/site-configurator.service';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-site-theme',
  template: '',
})
/******************************************************************


      All bundled css for this theme must be set in the
      angular.json and in the "styles" array

        {
          "input": "src/companies/others/default/styles/styles.scss",
          "bundleName": "default.company-styles",
          "inject": false
        },

      Where "default" is the name of the theme for the theme usually
      donated by the company code given to the user and the styles
      are at to be loaded are stored at
      the location "src/companies/others/default/styles/styles.scss"

      We use "inject": false to not have the bundle included by default
      when the site loads.


**********************************************************************/
export class SiteThemeComponent implements OnInit, OnDestroy {

  loadAPI: Promise<any>;
  idSignConfigChangeEventSubscribtion: ISubscription;

  readonly styleElementId = 'client-theme';

  constructor(
    private siteConfiguratorService: SiteConfiguratorService
    , private logger: LoggerService
    , private queryStringUtilitiesService: QueryStringUtilitiesService
    , @Inject(DOCUMENT) private document) {
    this.logger = this.logger.createInstance('SiteThemeComponent');
  }

  public loadThemeResourcesFromEnvironment() {
    this.loadThemeResources(environment.Settings.siteConfig);
  }

  loadThemeResources(siteConfig: IdSignConfig) {
    const companyCode = siteConfig.userInterface.viewTheme;
    this.logger.debug('%cSite theme to load is ' + companyCode, 'color:blue');
    const style = this.getThemeStyleHref(companyCode);

    if (style) {
      this.loadStylesUsingHref(style);
    } else {
      const theme = this.getThemeElement();
      if (theme) {
        theme.remove();
      }
    }
  }

  public getThemeStyleHref(themeName: string): string {
    if (themeName) {

      return themeName.toLowerCase() + '.company-styles' + '.css';
    } else {
      return '';
    }
  }

  loadStylesUsingHref(styleName: string) {

    // check if we have generated the element already
    const themeLink = this.getThemeElement();

    if (themeLink) {

      // there is an element already,
      // so we simply update the href.
      // the old css will be unloaded
      // and new one loaded instead
      themeLink.href = styleName;

    } else {

      // we create a new link element
      // so we tell the DOM to load the theme
      // which was bundled
      const style = this.document.createElement('link');
      style.id = this.styleElementId;
      style.rel = 'stylesheet';
      style.href = `${styleName}`;

      // we add the element to the head.
      // this should be added as the last
      // element before the angular auto-generated
      // <style> elements
      this.document.head.appendChild(style);
    }
  }

  private getThemeElement(): HTMLLinkElement {

    return this.document.getElementById(
      this.styleElementId
    ) as HTMLLinkElement;

  }

  removeConfigurationParameter(): string {
    const newUrl = this.queryStringUtilitiesService
      .removeURLParameter(window.location.href, environment.Settings.defaultValues.siteConfigParameterName);
    if (newUrl !== window.location.href) {
      this.logger.debug('Parameter removed. Refreshing page without parameter.');
      window.location.href = newUrl;
      return newUrl;
    } else {
      this.logger.debug('Parameter wasnt found or was not removed. No refreshing page.');
      return newUrl;
    }
  }


  ngOnInit() {
    this.siteConfiguratorService.configure()
      .then(siteConfig => {
        this.loadThemeResourcesFromEnvironment();
        this.removeConfigurationParameter();
      });

    this.idSignConfigChangeEventSubscribtion = this.siteConfiguratorService.idSignConfigChangeEvent()
      .subscribe(() => {
        this.loadThemeResourcesFromEnvironment();
        this.removeConfigurationParameter();
      });
  }

  ngOnDestroy() {
    if (this.idSignConfigChangeEventSubscribtion) {
      this.idSignConfigChangeEventSubscribtion.unsubscribe();
    }
  }

}
