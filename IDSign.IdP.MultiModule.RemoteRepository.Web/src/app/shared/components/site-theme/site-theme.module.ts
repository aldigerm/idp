import { SiteThemeComponent } from './site-theme.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        CommonModule
    ]
    , declarations: [SiteThemeComponent]
    , exports: [SiteThemeComponent]
})
export class SiteThemeModule {
}
