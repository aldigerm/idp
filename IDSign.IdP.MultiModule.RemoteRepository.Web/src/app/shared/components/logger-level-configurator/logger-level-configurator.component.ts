import { Component, OnInit } from '@angular/core';
// import { LoggingLevel } from '@models/logging-level.enum';

@Component({
  selector: 'app-logger-level-configurator',
  template: ''
})
export class LoggerLevelConfiguratorComponent implements OnInit {

  // cookieName = environment.Settings.defaultValues.siteConfigCookieName
  // + environment.Settings.defaultValues.loggerLevelCookieName;

  constructor(
    // private logger: LoggerService,
    // private loggerConfigurator: LoggerConfiguratorService,
    // private queryString: QueryStringUtilitiesService,
    // private cookieService: CookieService
  ) {
    // this.logger = this.logger.createInstance('LoggerLevelConfiguratorComponent');
    this.setLevel();
  }

  ngOnInit() {

  }

  setLevel() {
    // let level = this.queryString.getParameterByName(window.location.href, environment.Settings.defaultValues.loggerLevelParameterName);

    // // if the level is not in the query string, look in the cookie
    // if (!level) {
    //   level = this.cookieService.get(this.cookieName);
    // }

    // // if we have a value, then we use it
    // if (level) {
    //   this.logger.info('Logger level is in query string : ' + level);
    //   const loggerLevel: LoggingLevel = LoggingLevel[level];
    //   this.cookieService.set(environment.Settings.defaultValues.siteConfigCookieName
    //     + environment.Settings.defaultValues.loggerLevelCookieName
    //     , LoggingLevel[loggerLevel]);
    //   this.loggerConfigurator.setLevel(loggerLevel);
    // }
  }

}
