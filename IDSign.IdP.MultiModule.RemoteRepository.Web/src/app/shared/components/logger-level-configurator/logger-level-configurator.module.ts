import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoggerLevelConfiguratorComponent } from './logger-level-configurator.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LoggerLevelConfiguratorComponent
  ],
  exports: [
    LoggerLevelConfiguratorComponent
  ]
})
export class LoggerLevelConfiguratorModule { }
