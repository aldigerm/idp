import { Injectable } from '@angular/core';
import { MultilevelNodes } from './app.model';

@Injectable()
export class MultilevelMenuService {
  foundLinkObject: MultilevelNodes;
  generateId(): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 20; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
  addRandomId(nodes: MultilevelNodes[]): void {
    nodes.forEach((node: MultilevelNodes) => {
      if (!node.id) {
        node.id = this.generateId();
      }
      if (node.items !== undefined) {
        this.addRandomId(node.items);
      }
    });
  }
  recursiveCheckId(node: MultilevelNodes, nodeId: string): boolean {
    if (node.id === nodeId) {
      return true;
    } else {
      if (node.items !== undefined) {
        return node.items.some((nestedNode: MultilevelNodes) => {
          return this.recursiveCheckId(nestedNode, nodeId);
        });
      }
    }
  }
  recursiveCheckLink(nodes: MultilevelNodes[], link: string): MultilevelNodes {
    let found: MultilevelNodes = null;
    for (let i = 0; i < nodes.length; i++) {
      const node = nodes[i];
      if (node) {
        if (node.link === link) {
          found = node;
          break;
        } else {
          if (node.items !== undefined && node.items.length !== 0) {
            found = this.recursiveCheckLink(node.items, link);
            if (found) {
              break;
            }
          }
        }
      }
    }
    return found;
  }
  getMatchedObjectByUrl(node: MultilevelNodes[], link: string): MultilevelNodes {
    this.foundLinkObject = this.recursiveCheckLink(node, link);
    return this.foundLinkObject;
  }
}
