export interface MultilevelNodes {
    linkQueryParams: any;
    id: string;
    label: string;
    faIcon?: string;
    icon?: string;
    hidden?: boolean;
    link?: string;
    externalRedirect?: boolean;
    data: any;
    items?: MultilevelNodes[];
    onSelected?: Function;

    isTop: boolean;
    menuId: string;
    localisation: string;
    localisationParam: any;
    order: number;
}

export interface Configuration {
    classname?: string;
    paddingAtStart?: boolean;
    defaultListClass?: string;
    listBackgroundColor?: string;
    fontColor?: string;
    selectedListClass?: string;
    interfaceWithRoute?: boolean;
}

export interface BackgroundStyle {
    background: string;
}

export interface ListStyle {
    background: string;
    color: string;
}
