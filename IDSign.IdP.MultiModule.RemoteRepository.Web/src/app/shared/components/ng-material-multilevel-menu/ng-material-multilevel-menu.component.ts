import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LocationService } from '@services/location.service';
import { MultilevelMenuService } from './multilevel-menu.service';

import { Configuration, MultilevelNodes, BackgroundStyle } from './app.model';
import { CONSTANT } from './constants';
import { LoggerService } from '@app/core/logging/logger.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'ng-material-multilevel-menu',
  templateUrl: './ng-material-multilevel-menu.component.html',
  styleUrls: ['./ng-material-multilevel-menu.component.css'],
})
export class NgMaterialMultilevelMenuComponent implements OnInit, OnChanges {

  private _items: MultilevelNodes[];

  @Input() set items(value: MultilevelNodes[]) {

    value.sort(function (a, b) {
      return a.order - b.order;
    });

    this._items = value;

    this.updateNodeByURL(this.router.url);
  }

  get items(): MultilevelNodes[] {
    return this._items;
  }

  @Input() configuration: Configuration = null;
  @Output() selectedItem = new EventEmitter<MultilevelNodes>();
  currentNode: MultilevelNodes;
  nodeConfig: Configuration = {
    paddingAtStart: true,
    listBackgroundColor: null,
    fontColor: null,
    selectedListClass: null,
    interfaceWithRoute: null
  };
  isInvalidConfig = true;
  constructor(
    private router: Router,
    private location: LocationService,
    private _logger: LoggerService,
    private multilevelMenuService: MultilevelMenuService
  ) {
    this._logger = _logger.createInstance('NgMaterialMultilevelMenuComponent');
  }

  ngOnChanges() {
    this.checkValiddata();
    this.detectInvalidConfig();
  }

  ngOnInit() {
    if (
      this.configuration !== null && this.configuration !== undefined && this.configuration !== '' &&
      this.configuration.interfaceWithRoute !== null && this.configuration.interfaceWithRoute) {
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.updateNodeByURL(event.url);
        });

      this.location.replaceStateChange().subscribe((value: string) => {
        this._logger.debug('Location changed to ' + value);
        this.updateNodeByURL(value);
      });

      this.updateNodeByURL(this.router.url);
    }
  }
  updateNodeByURL(url: string): void {
    const foundNode = this.multilevelMenuService.getMatchedObjectByUrl(this.items, url);
    if (
      foundNode &&
      foundNode.link &&
      foundNode.link !== ''
    ) {
      this.currentNode = foundNode;
      this.selectedListItem(foundNode);
    }
  }
  checkValiddata(): void {
    if (this.items.length === 0) {
      // no warning, the list might be empty at start
    } else {
      this.items = this.items.filter(n => !n.hidden);
      this.multilevelMenuService.addRandomId(this.items);
    }
  }
  detectInvalidConfig(): void {
    if (this.configuration === null || this.configuration === undefined || this.configuration === '') {
      this.isInvalidConfig = true;
    } else {
      this.isInvalidConfig = false;
      const config = this.configuration;
      if (config.paddingAtStart !== undefined && config.paddingAtStart !== null && typeof config.paddingAtStart === 'boolean') {
        this.nodeConfig.paddingAtStart = config.paddingAtStart;
      }
      if (config.listBackgroundColor !== '' &&
        config.listBackgroundColor !== null &&
        config.listBackgroundColor !== undefined) {
        this.nodeConfig.listBackgroundColor = config.listBackgroundColor;
      }
      if (config.fontColor !== '' &&
        config.fontColor !== null &&
        config.fontColor !== undefined) {
        this.nodeConfig.fontColor = config.fontColor;
      }
      if (config.interfaceWithRoute !== null &&
        config.interfaceWithRoute !== undefined &&
        typeof config.interfaceWithRoute === 'boolean') {
        this.nodeConfig.interfaceWithRoute = config.interfaceWithRoute;
      }
      if (config.defaultListClass !== null &&
        config.defaultListClass !== undefined &&
        typeof config.defaultListClass === 'string') {
        this.nodeConfig.defaultListClass = config.defaultListClass;
      }
      if (config.selectedListClass !== null &&
        config.selectedListClass !== undefined &&
        typeof config.selectedListClass === 'string') {
        this.nodeConfig.selectedListClass = config.selectedListClass;
      }
    }
  }
  getClassName(): string {
    if (this.isInvalidConfig) {
      return CONSTANT.DEFAULT_CLASS_NAME;
    } else {
      if (this.configuration.classname !== '' && this.configuration.classname !== null && this.configuration.classname !== undefined) {
        return `${CONSTANT.DEFAULT_CLASS_NAME} ${this.configuration.classname}`;
      } else {
        return CONSTANT.DEFAULT_CLASS_NAME;
      }
    }
  }
  getGlobalStyle(): BackgroundStyle {
    if (!this.isInvalidConfig) {
      const styles = {
        background: null
      };
      if (this.configuration.defaultListClass !== '' &&
        this.configuration.defaultListClass !== null &&
        this.configuration.defaultListClass !== undefined) {
        styles.background = this.configuration.defaultListClass;
      }
      return styles;
    }
  }
  selectedListItem(event: MultilevelNodes): void {
    this.currentNode = event;
    this.selectedItem.emit(event);
  }
}
