import {
  Component, OnChanges, Input, Output, EventEmitter
  , ChangeDetectorRef, ViewChild, ElementRef, AfterViewInit, OnDestroy
} from '@angular/core';
import { Router } from '@angular/router';
import { SubscriptionLike as ISubscription } from 'rxjs';
import { trigger, style, transition, animate, state, group } from '@angular/animations';
import { WindowEventListenerService } from '@services/window-listener.service';
import { MultilevelMenuService } from './../multilevel-menu.service';
import { Configuration, MultilevelNodes, ListStyle } from './../app.model';
import { CONSTANT } from './../constants';
import { NgScrollbar } from 'ngx-scrollbar';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { AutoHightAdjustComponent } from '../../auto-hight-adjust/auto-hight-adjust.component';

@Component({
  selector: 'ng-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ height: '*', opacity: 0 })),
      transition(':leave', [
        style({ height: '*', opacity: 0.2 }),
        group([
          animate(300, style({ height: 0 })),
          animate('200ms ease-out', style({ opacity: 0 }))
        ])
      ]),
      transition(':enter', [
        style({ height: '0', opacity: 0 }),
        group([
          animate(200, style({ height: '*' })),
          animate('400ms ease-out', style({ opacity: 1 }))
        ])
      ])
    ]),
    trigger('isExpanded', [
      state('no', style({ transform: 'rotate(-90deg)' })),
      state('yes', style({ transform: 'rotate(0deg)', })),

      transition('no => yes',
        animate(300)
      ),
      transition('yes => no',
        animate(300)
      )
    ])
  ]
})
export class ListItemComponent implements OnChanges, AfterViewInit, OnDestroy {
  @Input() set node(value: MultilevelNodes) {
    this._node = value;
  } get node(): MultilevelNodes {
    return this._node;
  }
  @Input() level = 1;
  @Input() set selectedNode(value: MultilevelNodes) {
    this._selectedNode = value;
  } get selectedNode(): MultilevelNodes {
    return this._selectedNode;
  }
  @Input() nodeConfiguration: Configuration = null;
  @Output() selectedItem = new EventEmitter<MultilevelNodes>();
  @ViewChild(AutoHightAdjustComponent, { static: false }) autoHightAdjustComponent: AutoHightAdjustComponent;
  @ViewChild(NgScrollbar, { static: false }) listScrollRef: NgScrollbar;
  private _selectedNode: MultilevelNodes;
  private _node: MultilevelNodes;
  isSelected = false;
  isMouseOver = false;
  nodeChildren: MultilevelNodes[];
  classes: { [index: string]: boolean; };
  selectedListClasses: string;
  expanded = false;
  listHeight = 'auto';
  mouseOverTimeout = setTimeout(function () { }, 0);

  constructor(
    private router: Router,
    private multilevelMenuService: MultilevelMenuService
  ) {
    this.selectedListClasses = '';
    this.router.onSameUrlNavigation = 'reload';
  }
  ngOnChanges() {
    this.nodeChildren = this.node && this.node.items ? this.node.items.filter(n => !n.hidden) : [];
    if (this.selectedNode !== undefined && this.selectedNode !== null) {
      this.setSelectedClass(this.multilevelMenuService.recursiveCheckId(this.node, this.selectedNode.id));
    }
  }

  ngAfterViewInit() {
    const parent = this;
  }

  ngOnDestroy() {
  }

  setSelectedClass(isFound: boolean): void {
    if (isFound) {
      this.isSelected = true;
      this.expanded = true;
    } else {
      this.isSelected = false;
    }
    this.selectedListClasses = this.isSelected ? this.nodeConfiguration.selectedListClass : '';
    this.setClasses();
  }
  getPaddingAtStart(): boolean {
    return this.nodeConfiguration && this.nodeConfiguration.paddingAtStart ? true : false;
  }

  getListClass(): string {
    return this.nodeConfiguration ? this.nodeConfiguration.defaultListClass : '';
  }
  get hasItems(): boolean {
    return this.nodeChildren && this.nodeChildren.length > 0 ? true : false;
  }
  setClasses(): void {
    this.classes = {
      ['level-' + this.level]: true,
      'amml-submenu': this.hasItems && this.expanded && this.getPaddingAtStart()
    };
  }

  expand(node: MultilevelNodes): void {

    // the user might have hovered over already
    clearTimeout(this.mouseOverTimeout);

    this.expanded = !this.expanded;
    this.setClasses();
    if (this.nodeConfiguration.interfaceWithRoute !== null
      && this.nodeConfiguration.interfaceWithRoute
      && node.link !== undefined
      && node.items.length === 0
    ) {
      if (node.externalRedirect !== undefined && node.externalRedirect) {
        window.location.href = node.link;
      } else {
        this.router.navigate([node.link]);
      }
    } else if (node.items === undefined) {
      this.selectedListItem(node);
    }
    const parent = this;
    setTimeout(() => {
      if (parent.autoHightAdjustComponent) {
        parent.autoHightAdjustComponent.fixDimensions();
      }
    }, 500);
  }
  selectedListItem(node: MultilevelNodes): void {
    this.selectedItem.emit(node);
  }
}
