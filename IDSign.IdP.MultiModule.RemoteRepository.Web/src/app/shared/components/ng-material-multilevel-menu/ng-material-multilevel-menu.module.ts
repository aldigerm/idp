import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialsModule } from './materials.module';

import { NgMaterialMultilevelMenuComponent } from './ng-material-multilevel-menu.component';
import { ListItemComponent } from './list-item/list-item.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { AutoHightAdjustModule } from '../auto-hight-adjust/auto-hight-adjust.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialsModule,
    TranslateModule,
    NgScrollbarModule,
    AutoHightAdjustModule
  ],
  declarations: [NgMaterialMultilevelMenuComponent, ListItemComponent],
  exports: [NgMaterialMultilevelMenuComponent]
})
export class NgMaterialMultilevelMenuModule { }
