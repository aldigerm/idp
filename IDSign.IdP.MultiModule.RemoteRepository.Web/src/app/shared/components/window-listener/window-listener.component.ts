import { Component, HostListener, OnInit } from '@angular/core';
import { WindowEventListenerService } from '@services/window-listener.service';

@Component({
  selector: 'app-window-listener',
  templateUrl: './window-listener.component.html',
  styleUrls: ['./window-listener.component.css']
})
export class WindowListenerComponent implements OnInit {

  constructor(
    private _windowListener: WindowEventListenerService
  ) { }

  ngOnInit() {
  }

  @HostListener('window:resize', [])
  private triggerOnResize() {
    this._windowListener.TriggerOnMenu();
  }
  @HostListener('window:idonboard-resized-menu', [])
  private triggerOnMenu() {
    this._windowListener.TriggerOnMenu();
  }
}
