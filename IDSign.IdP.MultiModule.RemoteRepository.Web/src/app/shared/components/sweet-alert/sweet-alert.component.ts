import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SubscriptionLike as ISubscription } from 'rxjs';
declare const jquery: any;
declare const swal: any;
declare const $: any;

@Component({
  selector: 'app-sweet-alert',
  template: '',
  styleUrls: ['./sweet-alert.component.scss']
})
export class SweetAlertComponent implements OnInit, OnDestroy {

  public readonly colourGreen = '#A5DC86';
  public readonly colourBlue = '#8CD4F5';
  public readonly colourRed = '#DD6B55';
  private sweetalertSubscriber: ISubscription;

  constructor(
    private logger: LoggerService,
    private translate: TranslateService,
    private sweeatAlertService: SweetAlertService) {
    this.logger = this.logger.createInstance('SweetAlertComponent');
  }

  ngOnInit() {
    this.sweetalertSubscriber = this.sweeatAlertService.state().subscribe(request => {
      this.showSweetAlert(request);
    });
  }

  ngOnDestroy() {
    if (this.sweetalertSubscriber) {
      this.sweetalertSubscriber.unsubscribe();
    }
  }
  public showSweetAlert(request: SweetAlertRequest) {

    if (request) {
      const hasErrorCode = request.errorCode ? true : false;
      // get the translated messages
      Promise.all([
        this.translate.get(request.title)
          .subscribe(value => {
            request.title = value;
          }),
        this.translate.get(request.message, request.messageParams)
          .subscribe(value => {
            request.message = value;
          }),
        this.translate.get(request.cancelButton)
          .subscribe(value => {
            request.cancelButton = value;
          }),
        this.translate.get(request.confirmButton)
          .subscribe(value => {
            request.confirmButton = value;
          }),
        this.translate.get('Errors.' + request.errorCode)
          .subscribe(value => {
            request.errorCode = value;
          }),
      ]).then(r => {
        const requestType = request.type;
        switch (request.type) {
          case SweetAlertType.Processing: {
            this.logger.debug('Showing processing...');
            swal({
              title: request.title,
              text: '<div class="loader">'
                + '<div class="loader__figure"></div>'
                + '</div>',
              html: true,
              showCancelButton: false,
              showConfirmButton: false
            });
            break;
          }
          case SweetAlertType.Error:
            this.logger.debug('Showing error...');
            swal({
              title: request.title,
              text: request.message + (hasErrorCode ? ' ' + request.errorCode : ''),
              type: 'warning',
              confirmButtonText: request.confirmButton,
              confirmButtonColor: request.confirmButtonColor
            },
              function () {
                request.successCallback();
              });
            break;
          case SweetAlertType.Success:
            this.logger.debug('Showing success...');
            swal({
              title: request.title,
              text: request.message + (hasErrorCode ? ' ' + request.errorCode : ''),
              type: 'success',
              confirmButtonText: request.confirmButton
            }, function () {
              request.successCallback();
            });
            break;
          case SweetAlertType.SuccessWithOption:
            this.logger.debug('Showing success with confirm...');
            swal({
              title: request.title,
              text: request.message + (hasErrorCode ? ' ' + request.errorCode : ''),
              type: 'success',
              showCancelButton: true,
              confirmButtonText: request.confirmButton,
              confirmButtonColor: request.confirmButtonColor ? request.confirmButtonColor : this.colourGreen,
              cancelButtonText: request.cancelButton,
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
              if (isConfirm) {
                request.successCallback();
              } else {
                request.cancelCallback();
              }
            });
            break;
          case SweetAlertType.ErrorWithOption:
            this.logger.debug('Showing error with option...');
            swal({
              title: request.title,
              text: request.message + (hasErrorCode ? ' ' + request.errorCode : ''),
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: request.confirmButton,
              confirmButtonColor: request.confirmButtonColor ? request.confirmButtonColor : this.colourGreen,
              cancelButtonText: request.cancelButton,
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
              if (isConfirm) {
                request.successCallback();
              } else {
                request.cancelCallback();
              }
            });
            break;
          case SweetAlertType.Confirm:
            this.logger.debug('Showing confirm...');
            swal({
              title: request.title,
              text: request.message + (hasErrorCode ? ' ' + request.errorCode : ''),
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: request.confirmButtonColor ? request.confirmButtonColor : this.colourRed,
              confirmButtonText: request.confirmButton,
              cancelButtonText: request.cancelButton,
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
              if (isConfirm) {
                request.successCallback();
              } else {
                request.cancelCallback();
              }
            });
            break;
          case SweetAlertType.Close:
            this.logger.debug('Closing swal.');
            swal.close();
            break;
          default:
            this.logger.error('Swal request not recognised! ' + SweetAlertType[requestType]);
            swal.close();
            break;
        }
      });
    }
  }
}
