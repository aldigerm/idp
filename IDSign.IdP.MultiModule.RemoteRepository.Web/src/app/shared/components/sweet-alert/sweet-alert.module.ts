import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SweetAlertComponent } from './sweet-alert.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SweetAlertComponent],
  exports: [SweetAlertComponent]
})
export class SweetAlertModule { }
