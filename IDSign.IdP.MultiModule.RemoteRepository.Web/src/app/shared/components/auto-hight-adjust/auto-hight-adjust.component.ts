import {
  Component, OnInit, ChangeDetectorRef
  , ElementRef, AfterViewInit, OnDestroy, Output, EventEmitter, AfterContentInit
} from '@angular/core';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { ISubscription } from 'rxjs/Subscription';

import * as $ from 'jquery';

@Component({
  selector: 'app-auto-hight-adjust',
  templateUrl: './auto-hight-adjust.component.html',
  styleUrls: ['./auto-hight-adjust.component.css']
})
export class AutoHightAdjustComponent implements OnInit, AfterViewInit, OnDestroy, AfterContentInit {

  @Output() heightAdjust = new EventEmitter<string>();
  private _windowSubscribe: ISubscription;
  constructor(
    private _windowListenerService: WindowEventListenerService,
    private _elementRef: ElementRef,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this._windowSubscribe = this._windowListenerService.OnResizeAndMenu().subscribe(() => {
      this.fixDimensions();
    });
  }

  ngAfterContentInit() {
    this.fixDimensions();
  }
  ngOnDestroy(): void {
    if (this._windowSubscribe) {
      this._windowSubscribe.unsubscribe();
    }
  }

  public fixDimensions(): boolean {
    if (this._elementRef !== undefined) {
      const listItemElement = $(this._elementRef.nativeElement);

      if (listItemElement && $(listItemElement)[0]) {
        // menu
        const heightPx = this.distanceFromTopOfDivToBottomOfScreen(this._elementRef.nativeElement);
        let ngContentHeight = 0;
        const ngContent = this._elementRef.nativeElement.childNodes.forEach(node => {
          if (node && $(node.parentElement)) {
            ngContentHeight += $(node.parentElement).outerHeight(true);
          }
        });
        let leastHeight = heightPx;
        if (ngContent && ngContent.nativeElement) {
          leastHeight = ngContentHeight > heightPx ? heightPx : ngContentHeight;
        }
        const height = leastHeight + 'px';
        this._elementRef.nativeElement.style.height = height;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);

        this.heightAdjust.emit(height);
        return true;
      }
    }
    return false;
  }

  private distanceFromTopOfDivToBottomOfScreen(element: any): number {
    const scrollTop = $(window).scrollTop(),
      elementOffset = $(element).offset().top,
      distance = (elementOffset - scrollTop);
    return $(window).height() - distance;
  }
}
