import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoHightAdjustComponent } from './auto-hight-adjust.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AutoHightAdjustComponent],
  exports: [AutoHightAdjustComponent]
})
export class AutoHightAdjustModule { }
