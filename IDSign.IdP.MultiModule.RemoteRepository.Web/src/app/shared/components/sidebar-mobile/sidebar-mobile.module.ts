import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { environment } from '@environments/environment';
import { AccessControlPipeModule } from '@app/shared/pipes/control/access-control/access-control.pipe.module';
import { SidebarMobileComponent } from './sidebar-mobile.component';
import { NgMaterialMultilevelMenuModule } from './../ng-material-multilevel-menu/ng-material-multilevel-menu.module';

@NgModule(
  {
    imports: [
      CommonModule,
      AccessControlPipeModule,
      TranslateModule,
      RouterModule,
      NgMaterialMultilevelMenuModule
    ],
    declarations: [
      SidebarMobileComponent
    ],
    exports: [
      SidebarMobileComponent
    ]
  }
)
export class SidebarMobileModule {
}
