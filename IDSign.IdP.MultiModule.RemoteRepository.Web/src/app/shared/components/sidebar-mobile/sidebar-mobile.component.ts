import { SubscriptionLike as ISubscription } from 'rxjs';
import { MenuService } from '@app/core/services/menu.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { Component, AfterViewInit, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { MultilevelNodes, Configuration } from '@app/shared/components/ng-material-multilevel-menu/app.model';
import { Router, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';

declare const $: any;

@Component({
    selector: 'app-sidebar-mobile',
    templateUrl: './sidebar-mobile.component.html',
    styleUrls: ['./sidebar-mobile.component.css']
})
export class SidebarMobileComponent implements OnInit, AfterViewInit, OnDestroy {
    body: any;
    wrapper: any;
    mask: any;
    menu: any;
    closeBtn: any;
    menuOpeners: any;
    readonly options = {
        wrapper: '.body-content',
        type: 'push-left',
        menuOpenerClass: '.c-button',
        maskId: '#c-mask'
    };
    config: Configuration = {
        paddingAtStart: true,
        interfaceWithRoute: true,
        classname: 'custom-material-theme',
        listBackgroundColor: `#ffffff`,
        fontColor: `#000000`,
        defaultListClass: `sidenav-listitem`,
        selectedListClass: `sidenav-listitem active`,
    };
    menuData: MultilevelNodes;
    menuSubscriber: ISubscription;
    routerSubscriber: ISubscription;

    constructor(
        private logger: LoggerService,
        private menuService: MenuService,
        private router: Router
    ) {
        this.logger = logger.createInstance('SidebarMobileComponent');
        this.menuSubscriber = this.menuService.menuChange().subscribe(result => {
            if (result) {
                this.logger.debug('Menu is set ' + JSON.stringify(result));
                this.menuData = result;
            }
        });
        this.routerSubscriber = this.router.events
            .pipe(filter(event => event instanceof NavigationStart))
            .subscribe((event: NavigationStart) => {
                this.close();
            });
    }


    ngOnInit() {
        $('.page-wrapper').addClass('no-transitions');
        $('app-sidebar-mobile').addClass('sidebar-nav c-menu c-menu--push-left side-nav mobile no-transitions');
    }

    ngOnDestroy() {
        if (this.menuSubscriber) {
            this.menuSubscriber.unsubscribe();
        }
        if (this.routerSubscriber) {
            this.routerSubscriber.unsubscribe();
        }
    }

    ngAfterViewInit() {
        this.body = document.body;
        this.wrapper = document.querySelector(this.options.wrapper);
        this.mask = document.querySelector(this.options.maskId);
        this.menu = document.querySelector('app-sidebar-mobile');
        this.closeBtn = this.menu.querySelector('.c-menu__close');
        this.menuOpeners = document.querySelectorAll(this.options.menuOpenerClass);

        const parent = this;

        this.closeBtn.addEventListener('click', function (e) {
            e.preventDefault();
            parent.close();
        }.bind(this));


        // Event for clicks on the mask.
        this.mask.addEventListener('click', function (e) {
            e.preventDefault();
            parent.close();
        }.bind(this));


        const pushLeftBtn = document.querySelector('.c-button--push-left');

        pushLeftBtn.addEventListener('click', function (e) {
            e.preventDefault();
            if ($(document.body).hasClass('has-active-menu')) {
                $(this).removeClass('open');
                parent.close();
            } else {
                $(this).addClass('open');
                parent.open();
            }
        });
        $('.page-wrapper').removeClass('no-transitions');
        $('app-sidebar-mobile').removeClass('no-transitions');
    }

    open() {
        $('html, body').css('overflow-x', 'hidden');
        this.body.classList.add('has-active-menu');
        this.wrapper.classList.add('has-' + this.options.type);
        this.menu.classList.add('is-active');
        this.mask.classList.add('is-active');
        this.disableMenuOpeners();
    }

    close() {
        if (this.body && this.wrapper && this.menu && this.mask) {
            this.body.classList.remove('has-active-menu');
            this.wrapper.classList.remove('has-' + this.options.type);
            this.menu.classList.remove('is-active');
            this.mask.classList.remove('is-active');
            $('html, body').css('overflow-x', 'initial');
            this.enableMenuOpeners();
        }
    }

    disableMenuOpeners() {
        this.menuOpeners.forEach(function (item) {
            item.disabled = true;
        });
    }

    enableMenuOpeners() {
        this.menuOpeners.forEach(function (item) {
            item.disabled = false;
        });
    }
}
