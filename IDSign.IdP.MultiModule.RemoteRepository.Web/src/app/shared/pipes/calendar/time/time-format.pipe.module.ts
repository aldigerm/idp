import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TimeFormatPipe } from './time-format.pipe';

@NgModule(
  {
    imports: [
      CommonModule
    ],
    declarations: [
      TimeFormatPipe
    ],
    exports: [
      TimeFormatPipe
    ]
  }
)
export class TimeFormatPipeModule {
}
