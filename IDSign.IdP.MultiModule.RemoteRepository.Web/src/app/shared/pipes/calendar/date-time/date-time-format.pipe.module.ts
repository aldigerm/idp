import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateTimeFormatPipe } from './date-time-format.pipe';

@NgModule(
  {
    imports: [
      CommonModule
    ],
    declarations: [
      DateTimeFormatPipe
    ],
    exports: [
      DateTimeFormatPipe
    ]
  }
)
export class DateTimeFormatPipeModule {
}
