import { CommonModule } from '@angular/common';
import { DateFormatPipe } from './date-format.pipe';
import { NgModule } from '@angular/core';

@NgModule(
  {
    imports: [
      CommonModule
    ],
    declarations: [
      DateFormatPipe
    ],
    exports: [
      DateFormatPipe
    ]
  }
)
export class DateFormatPipeModule {
}
