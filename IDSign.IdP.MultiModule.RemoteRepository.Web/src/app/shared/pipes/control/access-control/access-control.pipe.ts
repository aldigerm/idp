import { Pipe, PipeTransform } from '@angular/core';
import { AccessControlService } from '@services/access-control.service';

@Pipe({
  name: 'accessControl'
})
export class AccessControlPipe implements PipeTransform {

  hasAccess = false;

  constructor(
    private accessControlService: AccessControlService
  ) {
  }

  transform(value: any, args?: any): any {
    return this.accessControlService.checkAccess(value);
  }

}
