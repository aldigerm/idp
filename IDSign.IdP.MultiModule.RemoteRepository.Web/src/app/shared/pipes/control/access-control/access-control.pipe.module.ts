import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AccessControlPipe } from './access-control.pipe';

@NgModule(
  {
    imports: [
      CommonModule
    ],
    declarations: [
      AccessControlPipe
    ],
    exports: [
      AccessControlPipe
    ]
  }
)
export class AccessControlPipeModule {
}
