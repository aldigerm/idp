import { Pipe, PipeTransform } from '@angular/core';
import { FeatureControlService } from '@services/feature-control.service';

@Pipe({
  name: 'featureControl'
})
export class FeatureControlPipe implements PipeTransform {

  constructor(
    private featureControlService: FeatureControlService
  ) {
  }

  transform(value: any, args?: any): any {
    return this.featureControlService.isFeatureEnabled(value);
  }

}
