import { FeatureControlPipe } from './feature-control.pipe';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule(
  {
    imports: [
      CommonModule
    ],
    declarations: [
      FeatureControlPipe
    ],
    exports: [
      FeatureControlPipe
    ]
  }
)
export class FeatureControlPipeModule {
}
