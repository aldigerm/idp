import { FormGroup, ValidatorFn, FormBuilder } from '@angular/forms';

export function addUpdateFormControl(form: FormGroup, fb: FormBuilder, controlName: string, value: any, validators: ValidatorFn[]) {
    if (form.get(controlName)) {
        form.get(controlName).setValue(value);
    } else {
        form.addControl(controlName, fb.control(value, validators));
    }
}

export function removeControlFromForm(form: FormGroup, controlName: string): boolean {
    if (form.get(controlName)) {
        form.removeControl(controlName);
        return true;
    } else {
        return false;
    }
}
