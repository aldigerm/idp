
export function IsJsonString(str: any): boolean {
    if (!str) {
        return false;
    }
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
