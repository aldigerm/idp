import * as _ from 'lodash';
export function hasEqualProperties(object, value) {
    return _.isEqual(Object.assign({}, object), Object.assign({}, value));
}
