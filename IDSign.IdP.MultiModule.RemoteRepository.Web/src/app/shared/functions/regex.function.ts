export function isRegexMatch(regex: string, value: string): boolean {
    const regExp = new RegExp(regex);
    return regExp.test(value);
}
