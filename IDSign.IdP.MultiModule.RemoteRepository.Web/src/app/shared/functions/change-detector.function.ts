import { ChangeDetectorRef } from '@angular/core';

export function detectChangesWithCheck(changeDetector: ChangeDetectorRef): void {
    if (!changeDetector['destroyed']) {
        changeDetector.detectChanges();
    }
}
