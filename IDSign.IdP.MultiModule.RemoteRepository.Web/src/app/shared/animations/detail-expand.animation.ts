import { trigger, state, style, transition, animate } from '@angular/animations';
export const DetailExpandAnimation = [
    trigger('detailExpand', [
        state('void', style({ height: '0px', minHeight: '0', visibility: 'hidden', opacity: 0 })),
        state('*', style({ height: '*', visibility: 'visible', opacity: 1 })),
        transition('void <=> *', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
];
