import { trigger, state, style, transition, animate, group, keyframes } from '@angular/animations';
export const FadeBoolAnimation = [
    trigger('fadeBool', [
        state('true', style({
            opacity: 1,
            padding: '*'
        })),
        state('false', style({
            opacity: 0,
            display: 'none',
            overflow: 'hidden'
        })),
        transition('true <=> false', animate('500ms ease-in-out'))
    ])
];
