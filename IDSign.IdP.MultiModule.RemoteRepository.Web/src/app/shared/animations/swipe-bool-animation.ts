import { trigger, state, style, transition, animate, group } from '@angular/animations';
export const SwipeBoolAnimation = [
    trigger('swipeBool', [
        state('true', style({ position: 'absolute', left: 0, right: 0, transform: 'translate3d(0%,0,0)' })),
        state('false', style({ position: 'absolute', left: 0, right: 0, transform: 'translate3d(-100%,0,0)' })),
        transition('true => false', [group([
            animate('500ms cubic-bezier(.35,0,.25,1)', style({ transform: 'translate3d(100%,0,0)' })) // y: '-100%'
        ]
        )]),
        transition('false => true', [group([
            animate('500ms cubic-bezier(.35,0,.25,1)', style({ transform: 'translate3d(0%,0,0)' }))
        ]
        )])
    ]),
];
