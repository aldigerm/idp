import { trigger, state, style, transition, animate, group } from '@angular/animations';
export const SlideBoolAnimation = [
    trigger('slideBool', [
        state('true', style({
            'height': '*', 'opacity': '1', 'display': 'block'
        })),
        state('false', style({
            'height': '0', 'opacity': '0', 'display': 'none'
        })),
        transition('true => false', [group([
            animate('200ms ease-in-out', style({
                'opacity': '0'
            })),
            animate('200ms ease-in-out', style({
                'height': '0px'
            })),
            animate('200ms ease-in-out', style({
                'display': 'none'
            }))
        ]
        )]),
        transition('false => true', [group([
            animate('200ms ease-in-out', style({
                'display': 'block'
            })),
            animate('200ms ease-in-out', style({
                'height': '*'
            })),
            animate('200ms ease-in-out', style({
                'opacity': '1'
            }))
        ]
        )])
    ]),
];
