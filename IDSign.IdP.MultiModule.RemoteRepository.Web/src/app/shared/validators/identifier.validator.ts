import { AbstractControl, ValidatorFn } from '@angular/forms';
export function tenantValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (!control.value || !control.value.tenantCode || !control.value.projectCode) {
            return { tenantIdentifier: true };
        } else {
            return null;
        }
    };
}
export function projectValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (!control.value || !control.value.projectCode) {
            return { projectIdentifier: true };
        } else {
            return null;
        }
    };
}

export function passwordPolicyTypeValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        if (!(control.value && control.value.passwordPolicyTypeCode)) {
            return { passwordPolicyTypeCode: true };
        } else {
            return null;
        }
    };
}
