
import { FormControl } from '@angular/forms';
export function numberValidator(control: FormControl) {
    const isValid = !isNaN(Number(control.value));
    return isValid ? null : { 'number': true };
}
