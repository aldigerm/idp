
import { FormControl } from '@angular/forms';
export function exactSelectionRequiredValidator(exact: number) {
    return (control: FormControl): { [key: string]: boolean } | null => {
        const value = control.value || [];
        let length = 0;
        if (value instanceof Array) {
            length = value.length;
        } else {
            length = 1;
        }
        const isSelected = length === exact;
        return isSelected ? null : { 'exactSelection': true };
    };
}
