
import { FormControl } from '@angular/forms';
export function noWhitespaceValidator(control: FormControl) {
    const isEmpty = (control.value || '').length === 0;
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = isEmpty || !isWhitespace;
    return isValid ? null : { 'whitespace': true };
}
