import { Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-int-text-box',
  templateUrl: './int-text-box.component.html',
  styleUrls: ['./int-text-box.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InternalTextBoxComponent),
      multi: true
    }
  ]
})

export class InternalTextBoxComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;
  isValid = false;

  form: FormGroup;
  private _model = '';
  type = 'text';

  @Input() minimumNumber: number;
  @Input() maximumNumber: number;
  @Input() set numbersOnly(value: boolean) {
    if (value) {
      this.type = 'number';
    } else {
      this.type = 'text';
    }
  }

  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() readonly = false;
  @Input() translatePrefix = '';
  @Input() set model(value: string) {
    this._model = value;
  } get model() {
    return this._model;
  }

  constructor() {
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.propagateChange(obj);
    this.propagateTouch(obj);
  }

  set value(val: string) {
    this._model = val;
    this.propagateChange(val);
    this.propagateTouch(val);
  } get value(): string {
    return this._model;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public getModel(): string {
    return this.model;
  }

  ngOnInit() {
  }
}
