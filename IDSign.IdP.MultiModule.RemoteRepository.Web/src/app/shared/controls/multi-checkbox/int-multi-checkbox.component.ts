import { Component, OnInit, Input, EventEmitter, Output, forwardRef, Optional, Host, SkipSelf, ViewChild } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import {
  FormBuilder, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR,
  AbstractControl, ControlContainer, FormControl
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MultiCheckboxOption } from './multi-checkbox-option';
import * as _ from 'lodash';
import { JsonPipe } from '@angular/common';
import { hasEqualProperties } from '@app/sharedfunctions/hasEqualProperties';

@Component({
  selector: 'app-int-multi-checkbox',
  templateUrl: './int-multi-checkbox.component.html',
  styleUrls: ['./int-multi-checkbox.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InternalMultiCheckboxComponent),
      multi: true
    }
  ]
})

export class InternalMultiCheckboxComponent implements OnInit, ControlValueAccessor {

  disabled = false;
  isValid = false;

  form: FormGroup;
  private _model: any[] = [];
  private _options: MultiCheckboxOption[] = [];
  private _optionsSorted: MultiCheckboxOption[] = [];
  private _sorted = true;
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() set sorted(value: boolean) {
    this._sorted = value;
    this.initForm();
  } get sorted(): boolean {
    return this._sorted;
  }
  @Input() readonly = false;
  @Input() translatePrefix = '';
  @Input() set options(value: MultiCheckboxOption[]) {
    if (!value) {
      value = [];
    }
    this._options = value;
    let index = 0;
    this.options.forEach(o => {
      o.index = index;
      index++;
    });

    value.sort(function (a, b) {
      if (a.name < b.name) { return -1; }
      if (a.name > b.name) { return 1; }
      return 0;
    });
    this._optionsSorted = value;

    this.updateOptionsWithModel();
    this.initForm();
  } get options(): MultiCheckboxOption[] {
    return this._options;
  }
  @Input() localizationString = '';
  @Input() set model(value: any[]) {
    if (!value) {
      value = [];
    }
    this._model = value;
    this.initForm();
  } get model(): any[] {
    return this._model;
  }

  constructor(
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
    private _logger: LoggerService,
    private _fb: FormBuilder
  ) {
    this._logger = _logger.createInstance('InternalTextBox');
  }

  propagateChange = (__: any) => { };
  propagateTouch = (__: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.updateOptionsWithModel();

    this.initForm();
    this.propagateChange(obj);
    this.propagateTouch(obj);
  }

  set value(val: any[]) {
    this._model = val;
    this.propagateChange(val);
    this.propagateTouch(val);
  } get value(): any[] {
    return this._model;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  checkBoxChanged(checkbox: MatCheckboxChange): void {
    const value = +checkbox.source.value;
    const checkedValue = this.options.find(o => o.index === value);
    checkedValue.selected = checkbox.checked;
    this._model = [];
    this.options.filter(v => v.selected).map(o => {
      this._model.push(o.value);
    });
    this.propagateChange(this._model);
  }
  updateOptionsWithModel() {
    this._options.map(c => {
      if (this.model.find(o => hasEqualProperties(o, c.value))) {
        c.selected = true;
      } else {
        c.selected = false;
      }
    });
  }
  initForm() {
    let valuesControls: FormControl[];
    if (this.sorted) {
      valuesControls = this._options.map(c =>
        this.model && this.model.find(o => hasEqualProperties(o, c.value)) ?
          this._fb.control(true) : this._fb.control(false));
    } else {
      valuesControls = this._optionsSorted.map(c =>
        this.model && this.model.find(o => hasEqualProperties(o, c.value)) ?
          this._fb.control(true) : this._fb.control(false));
    }
    if (this.form) {
      this.form.reset();
      this.form.setControl('value', this._fb.array(valuesControls));
    } else {
      this.form = this._fb.group({
        value: this._fb.array(valuesControls)
      });
    }
  }

  public getModel(): string[] {
    return this.model;
  }

  ngOnInit() {
    this.initForm();
  }
}
