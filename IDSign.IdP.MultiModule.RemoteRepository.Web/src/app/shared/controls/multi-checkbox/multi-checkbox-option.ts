import { IMultiSelectOption } from 'angular-2-dropdown-multiselect/dropdown/types';

export interface MultiCheckboxOption extends IMultiSelectOption {
    index: number;
    value: any;
    selected: boolean;
}
