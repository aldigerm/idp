import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InternalMultiCheckboxComponent } from './int-multi-checkbox.component';
import { MaterialsModule } from '@app/sharedcomponents/ng-material-multilevel-menu/materials.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    MaterialsModule
  ],
  declarations: [InternalMultiCheckboxComponent],
  exports: [InternalMultiCheckboxComponent]
})
export class InternalMultiCheckboxControl { }
