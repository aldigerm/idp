import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ProfilePictureModule } from '@app/sharedcomponents/profile-picture/profile-picture.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { ProfilePictureControlComponent } from './profile-picture-control.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FontAwesomeModule,
    FileUploadModule,
    ProfilePictureModule
  ],
  declarations: [ProfilePictureControlComponent],
  exports: [ProfilePictureControlComponent],
})
export class ProfilePictureControl { }
