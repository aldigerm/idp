import {
  Component, ElementRef, EventEmitter, forwardRef
  , Host, Input, OnInit, Optional, Output, Renderer, SkipSelf, ViewChild
} from '@angular/core';
import { AbstractControl, ControlContainer, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LoggerService } from '@app/core/logging/logger.service';
import { ProfilePictureComponent } from '@app/sharedcomponents/profile-picture/profile-picture.component';
import { environment } from '@environments/environment';
import { faCheck, faClone, faExclamationTriangle, faTimes, faTrash, faUpload } from '@fortawesome/free-solid-svg-icons';
import { FileUploadLocation } from '@models/enums';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { SweetAlertRequest, SweetAlertType } from '@models/sweet-alert/sweet-alert';
import { SweetAlertService } from '@services/html/sweet-alert/sweet-alert.service';
import { WindowEventListenerService } from '@services/window-listener.service';
import { FileItem, FileUploader, ParsedResponseHeaders } from 'ng2-file-upload';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-profile-picture-control',
  templateUrl: './profile-picture-control.component.html',
  styleUrls: ['./profile-picture-control.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProfilePictureControlComponent),
      multi: true
    }
  ]
})
export class ProfilePictureControlComponent implements OnInit, ControlValueAccessor {

  private _uploadedStatus = new BehaviorSubject<boolean>(false);
  // The internal data model
  private innerValue = '';
  @Input() formControlName: string;
  @Input() public uploadType = FileUploadLocation.UserProfileImage;
  @Input() readAsBase64 = false;
  @Input() auto = true;
  @Input() set userIdentifier(input: UserIdentifier) {
    this._userIdentifier = input;
  }
  get userIdentifier(): UserIdentifier {
    return this._userIdentifier;
  }
  private _userIdentifier: UserIdentifier;
  @ViewChild(ProfilePictureComponent, { static: true }) image: ProfilePictureComponent;
  @ViewChild('input', { static: false }) fileInput: ElementRef;
  formControl: AbstractControl;
  src = '';

  faTimes = faTimes;
  faUpload = faUpload;
  faCheck = faCheck;
  faExclamationTriangle = faExclamationTriangle;
  faTrash = faTrash;
  faClone = faClone;
  defaultImage = 'assets/images/users/default.png';
  displayState = '';

  uploadUrl = '';
  percentDone = 0;
  uuid = '';
  maxUploadSize = 100 * 1024;
  params = {};
  allowMultiple = false;
  id = '';
  readonly displayStateUpload = 'Upload';
  readonly displayStateUploading = 'Uploading';
  readonly displayStateUploaded = 'Uploaded';
  readonly displayStateError = 'Error';
  readonly displayStateCancelled = 'Cancelled';
  readonly prefix = 'Common.FileUpload.Actions.Upload.PopUp';

  @ViewChild('progressbar', { static: false }) progressbar: ElementRef;

  // custom uploader properties
  public uploader: FileUploader = new FileUploader(
    {
      allowedMimeType: ['image/jpeg', 'image/png', 'image/gif']
      , maxFileSize: this.maxUploadSize
    }
  );
  public hasBaseDropZoneOver = false;
  @Output() uploaded = new EventEmitter<boolean>();
  @Output() cancelled = new EventEmitter();
  @Output() error = new EventEmitter<string>();

  // Placeholders for the callbacks which are later providesd
  // by the Control Value Accessor
  private onTouchedCallback = () => { };
  private onChangeCallback = (_: any) => { };

  set value(v: string) {
    if (v !== this.innerValue) {
      this.innerValue = v;

      this.onChangeCallback(v);
    }
  }
  get value(): string {
    return this.innerValue;
  }

  constructor(
    private renderer: Renderer,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
    private _sweetAlert: SweetAlertService,
    private _logger: LoggerService,
    private _auth: AuthenticationService,
    private _windowEventListenerService: WindowEventListenerService) {
    this._logger = _logger.createInstance('ProfileImageComponent');
    this.displayState = this.displayStateUpload;
    this.uuid = '' + Math.floor(Math.random() * (999999 - 100000)) + 100000;
  }

  // Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  // From ControlValueAccessor interface
  writeValue(value: any) {
    if (!value) {
      if (this.image) {
        this.image.data = this.defaultImage;
      }
    } else {
      this.innerValue = value;
      if (this.image && value) {
        this.image.data = this.readAsBase64 ? value : 'data:image/gif;base64,' + value;
      }
      this.getFormControl();
    }

    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
    }
  }

  getFormControl() {
    if (this.controlContainer) {
      if (this.formControlName) {
        this.formControl = this.controlContainer.control.get(this.formControlName);
        this._logger.debug('Custom image for ' + this.formControlName);
        this.setDisabledState(this.formControl.disabled);
      } else {
        this._logger.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      this._logger.warn('Can\'t find parent FormGroup directive');
    }
  }
  // From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean) {
    // disable other components here
  }

  ngOnInit() {

    const parent = this;

    this.params = {
      fileSize: this.getFileSizeInKB(this.maxUploadSize)
    };

    this.uploader.onWhenAddingFileFailed = (item, filter) => {
      this.displayState = this.displayStateError;
      setTimeout(function () {
        parent.displayState = parent.displayStateUpload;
      }, 2000);
    };

    this.uploader.onBeforeUploadItem = (item: FileItem): any => {
      // logic of connecting url with the file
      item.withCredentials = false;
      item.url = this.getUploadUrl();
      item.headers = this.addHeader({ name: 'Authorization', value: 'Bearer ' + this._auth.getBearerToken() }, item.headers);
      item.headers = this.addHeader({ name: 'username', value: this.userIdentifier.username }, item.headers);
      item.headers = this.addHeader({ name: 'tenantCode', value: this.userIdentifier.tenantCode }, item.headers);
      item.headers = this.addHeader({ name: 'projectCode', value: this.userIdentifier.projectCode }, item.headers);

      this.displayState = this.displayStateUploading;
      return { fileItem: item };
    };

    this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {

      // in case we need to expand the parent div
      this._windowEventListenerService.TriggerOnResize();

      // we upload immediately if we configured automatic uploading
      if (this.auto) {
        this.uploader.uploadItem(fileItem);
      } else if (this.readAsBase64) {
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this.writeValue(myReader.result);
          this.onChangeCallback(myReader.result);
          fileItem.remove();
        };

        myReader.readAsDataURL(fileItem._file);
      }
    };

    this.uploader.onCompleteItem = (item: FileItem, response: string, status: number, header: ParsedResponseHeaders) => {
      this._logger.debug('File completed: ' + item.file.name);
      if (this.IsJsonString(response)) {
        const errorMessage = JSON.parse(response);
        if (errorMessage.ErrorCode) {
          this._sweetAlert.show(
            new SweetAlertRequest({
              type: SweetAlertType.Error,
              title: this.prefix + '.Failed.Title',
              message: this.prefix + '.Failed.Message',
              confirmButton: this.prefix + '.Failed.OkButton',
              errorCode: errorMessage && errorMessage.ErrorCode ? errorMessage.ErrorCode : undefined
            }));
        }
      }
      // single uploads would use the main section to show statuses
      if (!this.allowMultiple) {
        if (item.isSuccess) {
          this.displayState = this.displayStateUploaded;

          // single files are removed immediately
          this.uploader.removeFromQueue(item);
        }
      } else {
        // in multiple uploads, we show the status before removing
        // the status is handled by the other listeners
        if (item.isSuccess) {
          setTimeout(function () {
            parent.uploader.removeFromQueue(item);
          }, 2000);
        }
      }
    };

    this.uploader.onCancelItem = (item: FileItem, response: string, status: number, header: ParsedResponseHeaders) => {
      this._logger.debug('Cancelled item: ' + item.file.name);
      if (!this.allowMultiple) {
        this.displayState = this.displayStateCancelled;
      }
    };

    this.uploader.onErrorItem = (item: FileItem, response: string, status: number, header: ParsedResponseHeaders) => {
      this.displayState = this.displayStateError;
      if (this.IsJsonString(response)) {
        const error = JSON.parse(response);
        if (error.ErrorCode) {
        }
      }
      setTimeout(function () {
        parent.uploader.removeFromQueue(item);
      }, 2000);
      this.error.emit(response);
    };

    this.uploader.onCompleteAll = () => {

      // fetch image on server
      parent.image.loadImage();

      if (this.isUserProfileImageFile) {

        setTimeout(function () {
          parent.displayState = parent.displayStateUpload;
        }, 2000);
      }
      this.uploader.clearQueue();
    };
  }
  getFileSizeInKB(sizeInBytes): string {
    return (sizeInBytes / 1024).toFixed(2) + ' KB';
  }

  showUploadOptionIfChecklist() {
    if (this.isUserProfileImageFile) {
      this.displayState = this.displayStateUpload;
    }
  }

  addHeader(header: Header, headers: Header[]): any {
    if (headers === undefined) {
      headers = [];
    }
    const index = headers.findIndex(h => h.name === header.name);
    if (index >= 0) {
      headers[index].value = header.value;
    } else {
      headers.push(header);
    }
    return headers;
  }
  removeHeader(header: Header, headers: Header[]): any {
    if (headers === undefined) {
      headers = [];
    }
    const index = headers.findIndex(h => h.name === header.name);
    if (index >= 0) {
      headers.splice(index, 1);
    }
    return headers;
  }

  hasHeader(header: Header, headers: Header[]): boolean {
    return headers.findIndex(h => h.name === header.name && h.value === header.value) >= 0;
  }

  public reset() {
    this.uploader.clearQueue();
  }

  get isUserProfileImageFile(): boolean {
    return this.uploadType === FileUploadLocation.UserProfileImage;
  }

  getUploadUrl(): string {
    if (this.isUserProfileImageFile) {
      return environment.Settings.externalUrls.apiEndpoint + environment.Settings.apiKeys.idP.UserFileUpload;
    }
    this._logger.debug('Upload url for fileType:' + this.uploadType + ' is ' + this.uploadUrl);
    return '';
  }

  public setItemModel(dossierId: string, model: UserIdentifier) {
    this.userIdentifier = model;
    this.displayState = this.displayStateUpload;
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  cancelAllAndRemoveIfAuto(uploader: FileUploader) {
    const parent = this;

    // if files are not ready and are cancelled
    // then we show an error
    if (uploader.queue.filter(f => !f.isSuccess).length > 0) {
      this.cancelled.emit();
      this.displayState = this.displayStateCancelled;
      setTimeout(function () {
        parent.displayState = parent.displayStateUpload;
      }, 2000);
    } else {
      // if everything was uploaded, then we show the
      // option to upload again
      this.displayState = this.displayStateUpload;
    }

    uploader.cancelAll();
    uploader.clearQueue();
  }

  public UploadedStatus(): Observable<boolean> {
    return this._uploadedStatus;
  }

  onClear(event) {
    if (this.displayState !== this.displayStateUploaded
      && this.displayState !== this.displayStateError) {
      const parent = this;
      this.displayState = this.displayStateCancelled;
      setTimeout(function () {
        parent.displayState = parent.displayStateUpload;
      }, 2000);
    }
  }

  IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
}

export interface Header {
  name: string;
  value: string;
}
