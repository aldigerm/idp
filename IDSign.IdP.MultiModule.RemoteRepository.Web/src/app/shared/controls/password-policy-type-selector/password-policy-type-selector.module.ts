import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InternalDropdownControl } from '../dropdown/int-dropdown.module';
import { InternalTextBoxControl } from '../text-control/int-text-box.module';
import { PasswordPolicyTypeSelectorComponent } from './password-policy-type-selector.component';
import { InternalLabelControl } from '../label/int-label.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    InternalDropdownControl,
    InternalTextBoxControl,
    InternalLabelControl
  ],
  declarations: [PasswordPolicyTypeSelectorComponent],
  exports: [PasswordPolicyTypeSelectorComponent]
})
export class PasswordPolicyTypeSelectorModule { }
