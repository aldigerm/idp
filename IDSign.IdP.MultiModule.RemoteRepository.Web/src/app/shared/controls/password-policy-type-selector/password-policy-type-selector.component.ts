import { ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserService } from '@http/user/user.service';
import { PasswordPolicyTypeIdentifier } from '@models/identifiers/password-policy-type-identifier';
import { PasswordPolicyTypeBasicModel } from '@models/remote-repository/password-policy/type/remote-repository-password-policy-type-basic';
import { TranslateService } from '@ngx-translate/core';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { PasswordPolicyTypeSelectorModule } from './password-policy-type-selector.module';

@Component({
  selector: 'app-password-policy-type-selector',
  templateUrl: './password-policy-type-selector.component.html',
  styleUrls: ['./password-policy-type-selector.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordPolicyTypeSelectorComponent),
      multi: true
    }
  ]
})
export class PasswordPolicyTypeSelectorComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;
  isSinglePasswordPolicyType = false;
  singlePasswordPolicyTypeCode = '';
  passwordPolicyTypeName = '';
  passwordPolicyTypeCodeFound = true;
  showLabel = false;
  _readonly = false;

  form: FormGroup;
  private _model = <PasswordPolicyTypeIdentifier>{};

  set model(value: PasswordPolicyTypeIdentifier) {
    this._model = value;
    if (value) {
      this._model = value;
    } else {
      this._model = <PasswordPolicyTypeIdentifier>{};
      this.propagateChange(this._model);
    }
    this.initForm();
  }
  get model(): PasswordPolicyTypeIdentifier {
    return this._model;
  }

  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() set readonly(value: boolean) {
    this._readonly = value;
  } get readonly(): boolean {
    return this._readonly;
  }
  @Input() initialiseWithCurrentlyAccessible = false;
  passwordPolicyTypeOptions: IMultiSelectOption[];
  private _passwordPolicyTypes: PasswordPolicyTypeBasicModel[];

  constructor(
    private _logger: LoggerService,
    private _userService: UserService,
    private _fb: FormBuilder,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this._logger = this._logger.createInstance('PasswordPolicyTypeSelectorComponent');
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    if (obj) {
      this.model = obj;
    } else {
      this.model = <PasswordPolicyTypeIdentifier>{};
    }
    this.initForm();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  initForm() {
    if (this.form) {
      this.form.get('passwordPolicyTypeCode').setValue((this.model && this.model.passwordPolicyTypeCode)
        || this.singlePasswordPolicyTypeCode || '');
    } else {
      this.form = this._fb.group({
        passwordPolicyTypeCode: [(this.model && this.model.passwordPolicyTypeCode) || this.singlePasswordPolicyTypeCode || '']
      });


      this.form.get('passwordPolicyTypeCode').valueChanges.subscribe(
        (value: string) => {
          if (!this.model) {
            this.model = <PasswordPolicyTypeIdentifier>{};
          }
          if (this.model.passwordPolicyTypeCode !== value) {
            this.model.passwordPolicyTypeCode = value;
            this.propagateChange(this.model);
          }
        });

      if (this.initialiseWithCurrentlyAccessible) {
        this.initialiseWithAcessible();
      }
    }
    this.updateLabel();
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  public initialiseWithAcessible() {
    this._userService.getUser().then(user => {
      this.setSelection(user.userAccessDetail.passwordPolicyTypes);
    }).catch(error => {
      this._logger.error('Couldnt initialise passwordPolicyType dropdown', error);
    });
  }

  public setSelection(passwordPolicyTypes: PasswordPolicyTypeBasicModel[]): void {
    this._passwordPolicyTypes = passwordPolicyTypes;
    this.passwordPolicyTypeOptions = new Array<IMultiSelectOption>();
    if (passwordPolicyTypes) {

      // we build the options
      passwordPolicyTypes.forEach(passwordPolicyType => {
        this.passwordPolicyTypeOptions.push({
          id: passwordPolicyType.identifier.passwordPolicyTypeCode,
          name: passwordPolicyType.description + ' (' + passwordPolicyType.identifier.passwordPolicyTypeCode + ')'
        });
      });
      this.updateLabel();
    }
  }

  updateLabel() {

    // we check if there is a specific passwordPolicyType ot show
    if (this.readonly && this.model.passwordPolicyTypeCode) {
      const passwordPolicyType = this.passwordPolicyTypeOptions.find(p => p.id === this.model.passwordPolicyTypeCode);
      if (passwordPolicyType) {
        // passwordPolicyType found so we're ok
        this.isSinglePasswordPolicyType = true;
        this.passwordPolicyTypeName = passwordPolicyType.name;
        this.passwordPolicyTypeCodeFound = true;
        this.showLabel = true;
      } else {
        // passwordPolicyType somehow not found
        this.isSinglePasswordPolicyType = false;
        this.passwordPolicyTypeCodeFound = false;

        // the form is reset on model change
        this.model = <PasswordPolicyTypeIdentifier>{};
      }
      // if we have 1 passwordPolicyType, then we just show that one
    } else if (this.passwordPolicyTypeOptions && this.passwordPolicyTypeOptions.length === 1) {
      const passwordPolicyType = this.passwordPolicyTypeOptions[0];
      this.singlePasswordPolicyTypeCode = passwordPolicyType.id;
      this.passwordPolicyTypeName = passwordPolicyType.name;
      this.model.passwordPolicyTypeCode = this.singlePasswordPolicyTypeCode;
      this.isSinglePasswordPolicyType = true;
      this.passwordPolicyTypeCodeFound = true;
      this.showLabel = true;
      this.propagateChange(this.model);
    } else {
      this.isSinglePasswordPolicyType = false;
      this.passwordPolicyTypeCodeFound = false;
      this.singlePasswordPolicyTypeCode = '';
      this.showLabel = false;
      this.propagateChange(this.model);
    }
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  getDropDownValue(value: any): string {
    if (value) {
      if (this.isString(value)) {
        return value;
      } else if (this.isArray(value)) {
        return value[0];
      }
    }
    return '';
  }

  // Returns if a value is an array
  isArray(value) {
    return value && typeof value === 'object' && value.constructor === Array;
  }

  // Returns if a value is a string
  isString(value) {
    return typeof value === 'string' || value instanceof String;
  }
  public getModel(): PasswordPolicyTypeIdentifier {
    return this.model;
  }

  ngOnInit() {
  }
}
