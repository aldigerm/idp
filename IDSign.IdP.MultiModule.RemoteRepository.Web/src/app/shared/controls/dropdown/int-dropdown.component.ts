import {
  Component, OnInit, Input, forwardRef
  , Optional, Host, SkipSelf, ViewChild, ChangeDetectorRef
} from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import {
  FormBuilder, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR,
  AbstractControl, ControlContainer
} from '@angular/forms';
import { IMultiSelectTexts, IMultiSelectSettings, IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { TranslateService } from '@ngx-translate/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';

@Component({
  selector: 'int-dropdown',
  templateUrl: './int-dropdown.component.html',
  styleUrls: ['./int-dropdown.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InternalDropdownComponent),
      multi: true
    }
  ]
})

export class InternalDropdownComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;

  form: FormGroup;
  private _model = '';
  private _options: IMultiSelectOption[];
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() translate = false;
  @Input() set model(value: string) {
    this._model = value;
    this.initForm();
  } get model() {
    return this._model;
  }
  @Input() set options(value: IMultiSelectOption[]) {
    this._options = value;
    this.initDropDown(value);
  } get options(): IMultiSelectOption[] {
    return this._options;
  }
  @Input() set enableSearch(value: boolean) {
    this.singleSelectSettings.enableSearch = value;
  }

  translatedOptions = new Array<IMultiSelectOption>();

  singleSelectSettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    dynamicTitleMaxItems: 1,
    displayAllSelectedText: false,
    showCheckAll: false,
    buttonClasses: 'btn btn-info btn-block',
    showUncheckAll: false,
    ignoreLabels: true,
    selectionLimit: 1,
    minSelectionLimit: 1,
    closeOnSelect: true,
    autoUnselect: true
  };

  multiSelectTexts: IMultiSelectTexts = {
    checkAll: 'Select all users',
    uncheckAll: 'Unselect all users',
    checked: 'user selected',
    checkedPlural: 'users selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All users selected',
  };
  constructor(
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
    private _logger: LoggerService,
    private _translate: TranslateService,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _fb: FormBuilder
  ) {
    this._logger = this._logger.createInstance('CustomDropdownComponent');
    this._translate.onLangChange.subscribe(x => {
      this._logger.debug('Lang changed. Updating dropdown.');
      this.initDropDown();
    });
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.initForm();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  initForm(options?: IMultiSelectOption[]) {

    if (this.form) {
      this.form.get('selection').setValue([this.model || undefined]);
    } else {
      this.form = this._fb.group({
        selection: this._fb.control([this.model || undefined], this.formControl.validator)
      });
    }
  }
  public initDropDown(options?: IMultiSelectOption[]) {

    if (options) {
      this._options = options;
    }
    if (this.formControlName) {
      const prefix = 'DropdownValues.selectTexts.' + this.formControlName + '.';
      const multiSelectTexts: IMultiSelectTexts = {};
      Promise.all([
        this._translate.get(prefix + 'checked')
          .subscribe(text => {
            multiSelectTexts.checked = text;
          }),
        this._translate.get(prefix + 'checkAll')
          .subscribe(text => {
            multiSelectTexts.checkAll = text;
          }),
        this._translate.get(prefix + 'uncheckAll')
          .subscribe(text => {
            multiSelectTexts.uncheckAll = text;
          }),
        this._translate.get(prefix + 'checkedPlural')
          .subscribe(text => {
            multiSelectTexts.checkedPlural = text;
          }),
        this._translate.get(prefix + 'searchPlaceholder')
          .subscribe(text => {
            multiSelectTexts.searchPlaceholder = text;
          }),
        this._translate.get(prefix + 'searchEmptyResult')
          .subscribe(text => {
            multiSelectTexts.searchEmptyResult = text;
          }),
        this._translate.get(prefix + 'searchNoRenderText')
          .subscribe(text => {
            multiSelectTexts.checked = text;
          }),
        this._translate.get(prefix + 'defaultTitle')
          .subscribe(text => {
            multiSelectTexts.defaultTitle = text;
          }),
        this._translate.get(prefix + 'allSelected')
          .subscribe(text => {
            multiSelectTexts.allSelected = text;
          }),
        this.translateOptions()
      ]).then(ready => {
        this.multiSelectTexts = multiSelectTexts;
        this._siteChangeDetector.detectChanges(this._changeDetectorRef);
      }).catch((error: Error) => {
        this._logger.warn(error.message);
      });
    }
  }

  public translateOptions(): Promise<any> {
    const translateOptionsTranslationsQueue = new Array<Observable<any>>();
    const translatedOptions = new Array<IMultiSelectOption>();
    if (this.options && this.formControlName) {
      this.options.forEach(element => {
        if (this.translate) {
          const path = 'Common.Form.' + this.formControlName + '.translations.' + element.name;
          translateOptionsTranslationsQueue.push(
            this._translate.get(path)
              .pipe(map(text => {
                translatedOptions.push({ id: element.id, name: text });
              }))
          );
        } else {
          translatedOptions.push({ id: element.id, name: element.name });
        }
      });
    }
    return new Promise<any>((resolve, reject) => {
      if (this.translate) {
        forkJoin(translateOptionsTranslationsQueue).subscribe(
          result => {
            this.translatedOptions = translatedOptions;
            this._siteChangeDetector.detectChanges(this._changeDetectorRef);
            resolve(true);
          }
        );
      } else {
        this.translatedOptions = translatedOptions;
        resolve(true);
      }
    });
  }
  onChange(event: string) {
    const value = this.getDropDownValue(event);
    if (value !== this.model) {
      this._logger.debug('New value is ' + value);
      this.propagateChange(value);
      this.model = value;
    }
  }

  getDropDownValue(value: any): string {
    if (value) {
      if (this.isString(value)) {
        return value;
      } else if (this.isArray(value)) {
        return value[0];
      }
    }
    return '';
  }

  // Returns if a value is an array
  isArray(value) {
    return value && typeof value === 'object' && value.constructor === Array;
  }

  // Returns if a value is a string
  isString(value) {
    return typeof value === 'string' || value instanceof String;
  }
  public getModel(): string {
    return this.model;
  }

  ngOnInit() {

    if (this.controlContainer) {
      if (this.formControlName) {
        this.formControl = this.controlContainer.control.get(this.formControlName);
        this._logger.debug('Custom text box for ' + this.formControlName);
      } else {
        this._logger.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      this._logger.warn('Can\'t find parent FormGroup directive');
    }
  }
}
