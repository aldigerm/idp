import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InternalDropdownControl } from '../dropdown/int-dropdown.module';
import { InternalTextBoxControl } from '../text-control/int-text-box.module';
import { ProjectSelectorComponent } from './project-selector.component';
import { InternalLabelControl } from '../label/int-label.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    InternalDropdownControl,
    InternalTextBoxControl,
    InternalLabelControl
  ],
  declarations: [ProjectSelectorComponent],
  exports: [ProjectSelectorComponent]
})
export class ProjectSelectorModule { }
