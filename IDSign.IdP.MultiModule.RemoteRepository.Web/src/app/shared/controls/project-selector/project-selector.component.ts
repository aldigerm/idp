import { ChangeDetectorRef, Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { UserService } from '@http/user/user.service';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { ProjectBasicModel } from '@models/remote-repository/project/remote-repository-project-basic';
import { TranslateService } from '@ngx-translate/core';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Component({
  selector: 'app-project-selector',
  templateUrl: './project-selector.component.html',
  styleUrls: ['./project-selector.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProjectSelectorComponent),
      multi: true
    }
  ]
})
export class ProjectSelectorComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;
  isSingleProject = false;
  singleProjectCode = '';
  projectName = '';
  projectCodeFound = true;
  showLabel = false;
  _readonly = false;

  form: FormGroup;
  private _model = <ProjectIdentifier>{};

  set model(value: ProjectIdentifier) {
    this._model = value;
    if (value) {
      this._model = value;
    } else {
      this._model = <TenantIdentifier>{};
      this.propagateChange(this._model);
    }
    this.initForm();
  }
  get model(): ProjectIdentifier {
    return this._model;
  }

  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() set readonly(value: boolean) {
    this._readonly = value;
  } get readonly(): boolean {
    return this._readonly;
  }
  @Input() initialiseWithCurrentlyAccessible = false;
  projectOptions: IMultiSelectOption[];
  private _projects: ProjectBasicModel[];

  constructor(
    private _logger: LoggerService,
    private _translate: TranslateService,
    private _userService: UserService,
    private _fb: FormBuilder,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this._logger = this._logger.createInstance('ProjectSelectorComponent');
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    if (obj) {
      this.model = obj;
    } else {
      this.model = <ProjectIdentifier>{};
    }
    this.initForm();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  initForm() {
    if (this.form) {
      this.form.get('project').setValue(this.model.projectCode || this.singleProjectCode || '');
    } else {
      this.form = this._fb.group({
        project: [this.model.projectCode || this.singleProjectCode || '']
      });


      this.form.get('project').valueChanges.subscribe(
        (value: string) => {
          if (this.model.projectCode !== value) {
            this.model.projectCode = value;
            this.propagateChange(this.model);
          }
        });

      if (this.initialiseWithCurrentlyAccessible) {
        this.initialiseWithAcessible();
      }
    }
    this.updateLabel();
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  public initialiseWithAcessible() {
    this._userService.getUser().then(user => {
      this.setSelection(user.userAccessDetail.projects);
    }).catch(error => {
      this._logger.error('Couldnt initialise project dropdown', error);
    });
  }

  public setSelection(projects: ProjectBasicModel[]): void {
    this._projects = projects;
    this.projectOptions = new Array<IMultiSelectOption>();
    if (projects) {

      // we build the options
      projects.forEach(project => {
        this.projectOptions.push({ id: project.identifier.projectCode, name: project.name + ' (' + project.identifier.projectCode + ')' });
      });
      this.updateLabel();
    }
  }

  updateLabel() {

    // we check if there is a specific project ot show
    if (this.readonly && this.model.projectCode) {
      const project = this.projectOptions.find(p => p.id === this.model.projectCode);
      if (project) {
        // project found so we're ok
        this.isSingleProject = true;
        this.projectName = project.name;
        this.projectCodeFound = true;
        this.showLabel = true;
      } else {
        // project somehow not found
        this.isSingleProject = false;
        this.projectCodeFound = false;

        // the form is reset on model change
        this.model = <ProjectIdentifier>{};
      }
      // if we have 1 project, then we just show that one
    } else if (this.projectOptions && this.projectOptions.length === 1) {
      const project = this.projectOptions[0];
      this.singleProjectCode = project.id;
      this.projectName = project.name;
      this.model.projectCode = this.singleProjectCode;
      this.isSingleProject = true;
      this.projectCodeFound = true;
      this.showLabel = true;
      this.propagateChange(this.model);
    } else {
      this.isSingleProject = false;
      this.projectCodeFound = false;
      this.singleProjectCode = '';
      this.showLabel = false;
      this.propagateChange(this.model);
    }
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  getDropDownValue(value: any): string {
    if (value) {
      if (this.isString(value)) {
        return value;
      } else if (this.isArray(value)) {
        return value[0];
      }
    }
    return '';
  }

  // Returns if a value is an array
  isArray(value) {
    return value && typeof value === 'object' && value.constructor === Array;
  }

  // Returns if a value is a string
  isString(value) {
    return typeof value === 'string' || value instanceof String;
  }
  public getModel(): ProjectIdentifier {
    return this.model;
  }

  ngOnInit() {
  }
}
