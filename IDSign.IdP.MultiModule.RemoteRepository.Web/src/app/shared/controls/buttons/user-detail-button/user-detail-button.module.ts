import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailButtonComponent } from './user-detail-button.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    UserDetailButtonComponent
  ],
  exports: [
    UserDetailButtonComponent
  ]
})
export class UserDetailButtonModule { }
