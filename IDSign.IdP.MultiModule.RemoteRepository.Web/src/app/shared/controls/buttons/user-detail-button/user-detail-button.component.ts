import { Component, OnInit, Input } from '@angular/core';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-detail-button',
  templateUrl: './user-detail-button.component.html',
  styleUrls: ['./user-detail-button.component.css']
})
export class UserDetailButtonComponent implements OnInit {

  @Input() userIdentifier: UserIdentifier;

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('UserDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.userIdentifier));
    this.getDetailAndShow(this.userIdentifier);
  }

  private getDetailAndShow(id: UserIdentifier) {
    if (id.projectCode && id.tenantCode && id.username) {
      this._router.navigate(['/users/detail'],
        {
          queryParams: {
            projectCode: id.projectCode,
            tenantCode: id.tenantCode,
            username: id.username
          }
        });
    } else {
      this._router.navigate(['/users/detail'],
        {
          queryParams: {
            userIdentifier: id.userGuid
          }
        });
    }
  }
}
