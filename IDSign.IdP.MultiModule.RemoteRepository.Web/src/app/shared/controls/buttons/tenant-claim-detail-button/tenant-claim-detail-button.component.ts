import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { TenantClaimIdentifier } from '@models/identifiers/tenant-claim-identifier';

@Component({
  selector: 'app-tenant-claim-detail-button',
  templateUrl: './tenant-claim-detail-button.component.html',
  styleUrls: ['./tenant-claim-detail-button.component.css']
})
export class TenantClaimDetailButtonComponent implements OnInit {

  private _useCallBacks = false;
  @Input() tenantClaimIdentifier: TenantClaimIdentifier;
  @Input() set useCallBacks(value: boolean) {
    this._useCallBacks = value;
  } get useCallBacks(): boolean {
    return this._useCallBacks;
  }
  @Output() click = new EventEmitter<TenantClaimIdentifier>();

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('TenantClaimDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.tenantClaimIdentifier));
    if (this.useCallBacks) {
      this.click.emit(this.tenantClaimIdentifier);
    } else {
      this.getDetailAndShow(this.tenantClaimIdentifier);
    }
  }

  private getDetailAndShow(id: TenantClaimIdentifier) {
    this._router.navigate(['/tenantclaims/detail', id.projectCode,
      id.tenantCode, id.type, id.value]);
  }
}
