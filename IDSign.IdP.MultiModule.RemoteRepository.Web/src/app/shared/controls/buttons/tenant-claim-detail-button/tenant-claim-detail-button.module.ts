import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TenantClaimDetailButtonComponent } from './tenant-claim-detail-button.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    TenantClaimDetailButtonComponent
  ],
  exports: [
    TenantClaimDetailButtonComponent
  ]
})
export class TenantClaimDetailButtonModule { }
