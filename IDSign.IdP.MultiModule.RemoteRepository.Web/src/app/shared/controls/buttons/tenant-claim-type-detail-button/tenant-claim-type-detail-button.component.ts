import { Component, OnInit, Input } from '@angular/core';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { TenantClaimTypeIdentifier } from '@models/identifiers/tenant-claim-type-identifier';

@Component({
  selector: 'app-tenant-claim-type-detail-button',
  templateUrl: './tenant-claim-type-detail-button.component.html',
  styleUrls: ['./tenant-claim-type-detail-button.component.css']
})
export class TenantClaimTypeDetailButtonComponent implements OnInit {

  @Input() tenantClaimTypeIdentifier: TenantClaimTypeIdentifier;

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('TenantClaimTypeDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.tenantClaimTypeIdentifier));
    this.getDetailAndShow(this.tenantClaimTypeIdentifier);
  }

  private getDetailAndShow(id: TenantClaimTypeIdentifier) {
    this._router.navigate(['/tenantclaimtypes/detail', id.projectCode, id.type]);
  }
}
