import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TenantClaimTypeDetailButtonComponent } from './tenant-claim-type-detail-button.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    TenantClaimTypeDetailButtonComponent
  ],
  exports: [
    TenantClaimTypeDetailButtonComponent
  ]
})
export class TenantClaimTypeDetailButtonModule { }
