import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { UserClaimIdentifier } from '@models/identifiers/user-claim-identifier';

@Component({
  selector: 'app-user-claim-detail-button',
  templateUrl: './user-claim-detail-button.component.html',
  styleUrls: ['./user-claim-detail-button.component.css']
})
export class UserClaimDetailButtonComponent implements OnInit {

  private _useCallBacks = false;
  @Input() userClaimIdentifier: UserClaimIdentifier;
  @Input() set useCallBacks(value: boolean) {
    this._useCallBacks = value;
  } get useCallBacks(): boolean {
    return this._useCallBacks;
  }
  @Output() click = new EventEmitter<UserClaimIdentifier>();

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('UserClaimDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.userClaimIdentifier));
    if (this.useCallBacks) {
      this.click.emit(this.userClaimIdentifier);
    } else {
      this.getDetailAndShow(this.userClaimIdentifier);
    }
  }

  private getDetailAndShow(id: UserClaimIdentifier) {
    this._router.navigate(['/userclaims/detail', id.projectCode,
      id.tenantCode, id.username, id.type, id.value]);
  }
}
