import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { UserClaimDetailButtonComponent } from './user-claim-detail-button.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    UserClaimDetailButtonComponent
  ],
  exports: [
    UserClaimDetailButtonComponent
  ]
})
export class UserClaimDetailButtonModule { }
