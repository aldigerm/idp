import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnToMainSiteButtonComponent } from './return-to-main-site-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FontAwesomeModule
  ],
  declarations: [ReturnToMainSiteButtonComponent],
  exports: [ReturnToMainSiteButtonComponent]
})
export class ReturnToMainSiteButtonModule { }
