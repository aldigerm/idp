import { Component, OnInit, Output, Input } from '@angular/core';
import { faHandPointLeft } from '@fortawesome/free-solid-svg-icons';
import { BrowserStorage } from '@services/storage/browser-storage';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-return-to-main-site-button',
  templateUrl: './return-to-main-site-button.component.html',
  styleUrls: ['./return-to-main-site-button.component.css']
})
export class ReturnToMainSiteButtonComponent implements OnInit {

  returnUrl = '';

  @Input() asButton = false;

  public get hasReturnUrl(): boolean {
    return this.returnUrl ? true : false;
  }

  constructor(
    private _browserStorage: BrowserStorage) { }

  ngOnInit() {
    this.returnUrl = this._browserStorage.get(environment.Settings.browserStorageKeys.returnUrl);
  }

  returnToSite() {
    this._browserStorage.remove(environment.Settings.browserStorageKeys.returnUrl);
    window.location.href = this.returnUrl;
  }

}
