import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserGroupClaimTypeDetailButtonComponent } from './user-group-claim-type-detail-button.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    UserGroupClaimTypeDetailButtonComponent
  ],
  exports: [
    UserGroupClaimTypeDetailButtonComponent
  ]
})
export class UserGroupClaimTypeDetailButtonModule { }
