import { Component, OnInit, Input } from '@angular/core';
import { UserGroupIdentifier } from '@models/identifiers/user-group-identifier';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { UserGroupClaimTypeIdentifier } from '@models/identifiers/user-group-claim-type-identifier';

@Component({
  selector: 'app-user-group-claim-type-detail-button',
  templateUrl: './user-group-claim-type-detail-button.component.html',
  styleUrls: ['./user-group-claim-type-detail-button.component.css']
})
export class UserGroupClaimTypeDetailButtonComponent implements OnInit {

  @Input() userGroupClaimTypeIdentifier: UserGroupClaimTypeIdentifier;

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('UserGroupClaimTypeDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.userGroupClaimTypeIdentifier));
    this.getDetailAndShow(this.userGroupClaimTypeIdentifier);
  }

  private getDetailAndShow(id: UserGroupClaimTypeIdentifier) {
    this._router.navigate(['/usergroupclaimtypes/detail', id.projectCode, id.type]);
  }
}
