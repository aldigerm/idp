import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserClaimTypeDetailButtonComponent } from './user-claim-type-detail-button.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    UserClaimTypeDetailButtonComponent
  ],
  exports: [
    UserClaimTypeDetailButtonComponent
  ]
})
export class UserClaimTypeDetailButtonModule { }
