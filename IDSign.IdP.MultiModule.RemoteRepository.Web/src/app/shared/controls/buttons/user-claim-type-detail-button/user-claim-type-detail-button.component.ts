import { Component, OnInit, Input } from '@angular/core';
import { UserIdentifier } from '@models/identifiers/user-identifier';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { UserClaimTypeIdentifier } from '@models/identifiers/user-claim-type-identifier';

@Component({
  selector: 'app-user-claim-type-detail-button',
  templateUrl: './user-claim-type-detail-button.component.html',
  styleUrls: ['./user-claim-type-detail-button.component.css']
})
export class UserClaimTypeDetailButtonComponent implements OnInit {

  @Input() userClaimTypeIdentifier: UserClaimTypeIdentifier;

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('UserClaimTypeDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.userClaimTypeIdentifier));
    this.getDetailAndShow(this.userClaimTypeIdentifier);
  }

  private getDetailAndShow(id: UserClaimTypeIdentifier) {
    this._router.navigate(['/userclaimtypes/detail', id.projectCode, id.type]);
  }
}
