import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { UserGroupClaimDetailButtonComponent } from './user-group-claim-detail-button.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    UserGroupClaimDetailButtonComponent
  ],
  exports: [
    UserGroupClaimDetailButtonComponent
  ]
})
export class UserGroupClaimDetailButtonModule { }
