import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { UserGroupClaimIdentifier } from '@models/identifiers/user-group-claim-identifier';

@Component({
  selector: 'app-user-group-claim-detail-button',
  templateUrl: './user-group-claim-detail-button.component.html',
  styleUrls: ['./user-group-claim-detail-button.component.css']
})
export class UserGroupClaimDetailButtonComponent implements OnInit {

  private _useCallBacks = false;
  @Input() userGroupClaimIdentifier: UserGroupClaimIdentifier;
  @Input() set useCallBacks(value: boolean) {
    this._useCallBacks = value;
  } get useCallBacks(): boolean {
    return this._useCallBacks;
  }
  @Output() click = new EventEmitter<UserGroupClaimIdentifier>();

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('UserGroupClaimDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.userGroupClaimIdentifier));
    if (this.useCallBacks) {
      this.click.emit(this.userGroupClaimIdentifier);
    } else {
      this.getDetailAndShow(this.userGroupClaimIdentifier);
    }
  }

  private getDetailAndShow(id: UserGroupClaimIdentifier) {
    this._router.navigate(['/usergroupclaims/detail', id.projectCode,
      id.tenantCode, id.userGroupCode, id.type, id.value]);
  }
}
