import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleClaimTypeDetailButtonComponent } from './role-claim-type-detail-button.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    RoleClaimTypeDetailButtonComponent
  ],
  exports: [
    RoleClaimTypeDetailButtonComponent
  ]
})
export class RoleClaimTypeDetailButtonModule { }
