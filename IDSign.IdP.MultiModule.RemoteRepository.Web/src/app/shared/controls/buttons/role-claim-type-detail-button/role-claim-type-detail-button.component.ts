import { Component, OnInit, Input } from '@angular/core';
import { RoleIdentifier } from '@models/identifiers/role-identifier';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { RoleClaimTypeIdentifier } from '@models/identifiers/role-claim-type-identifier';

@Component({
  selector: 'app-role-claim-type-detail-button',
  templateUrl: './role-claim-type-detail-button.component.html',
  styleUrls: ['./role-claim-type-detail-button.component.css']
})
export class RoleClaimTypeDetailButtonComponent implements OnInit {

  @Input() roleClaimTypeIdentifier: RoleClaimTypeIdentifier;

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('RoleClaimTypeDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.roleClaimTypeIdentifier));
    this.getDetailAndShow(this.roleClaimTypeIdentifier);
  }

  private getDetailAndShow(id: RoleClaimTypeIdentifier) {
    this._router.navigate(['/roleclaimtypes/detail', id.projectCode, id.type]);
  }
}
