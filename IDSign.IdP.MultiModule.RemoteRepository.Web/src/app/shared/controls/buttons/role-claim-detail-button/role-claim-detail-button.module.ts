import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RoleClaimDetailButtonComponent } from './role-claim-detail-button.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    RoleClaimDetailButtonComponent
  ],
  exports: [
    RoleClaimDetailButtonComponent
  ]
})
export class RoleClaimDetailButtonModule { }
