import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import { Router } from '@angular/router';
import { RoleClaimIdentifier } from '@models/identifiers/role-claim-identifier';

@Component({
  selector: 'app-role-claim-detail-button',
  templateUrl: './role-claim-detail-button.component.html',
  styleUrls: ['./role-claim-detail-button.component.css']
})
export class RoleClaimDetailButtonComponent implements OnInit {

  private _useCallBacks = false;
  @Input() roleClaimIdentifier: RoleClaimIdentifier;
  @Input() set useCallBacks(value: boolean) {
    this._useCallBacks = value;
  } get useCallBacks(): boolean {
    return this._useCallBacks;
  }
  @Output() click = new EventEmitter<RoleClaimIdentifier>();

  constructor(
    private _logger: LoggerService,
    private _router: Router) {
    this._logger = this._logger.createInstance('RoleClaimDetailButtonComponent');
  }

  ngOnInit() {
  }

  clickItemDetail() {
    this._logger.debug('Id to get is ' + JSON.stringify(this.roleClaimIdentifier));
    if (this.useCallBacks) {
      this.click.emit(this.roleClaimIdentifier);
    } else {
      this.getDetailAndShow(this.roleClaimIdentifier);
    }
  }

  private getDetailAndShow(id: RoleClaimIdentifier) {
    this._router.navigate(['/roleclaims/detail', id.projectCode,
      id.tenantCode, id.roleCode, id.type, id.value]);
  }
}
