import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { InternalTextAreaComponent } from './int-text-area.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    MultiselectDropdownModule
  ],
  declarations: [
    InternalTextAreaComponent
  ],
  exports: [InternalTextAreaComponent]
})
export class InternalTextAreaControl { }
