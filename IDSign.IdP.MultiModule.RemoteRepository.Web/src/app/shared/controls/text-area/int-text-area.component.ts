import { Component, OnInit, Input, forwardRef, ViewChild, Output, EventEmitter } from '@angular/core';
import {
  FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR,
  AbstractControl
} from '@angular/forms';

@Component({
  selector: 'app-int-text-area',
  templateUrl: './int-text-area.component.html',
  styleUrls: ['./int-text-area.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InternalTextAreaComponent),
      multi: true
    }
  ]
})

export class InternalTextAreaComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;
  isValid = false;

  form: FormGroup;
  private _model = '';
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() placeHolderText: string;
  @Input() readonly = false;
  @Input() translatePrefix = '';
  @Output() inputFocusChanged = new EventEmitter<boolean>();
  @Input() set model(value: string) {
    this._model = value;
  } get model() {
    return this._model;
  }

  constructor() {
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.propagateChange(obj);
    this.propagateTouch(obj);
  }

  set value(val: string) {
    this._model = val;
    this.propagateChange(val);
    this.propagateTouch(val);
  } get value(): string {
    return this._model;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public getModel(): string {
    return this.model;
  }

  ngOnInit() {
  }

  // to block the arrow keys interaction in dossier overview
  onFocus(value: boolean) {
    this.inputFocusChanged.emit(value);
  }

}
