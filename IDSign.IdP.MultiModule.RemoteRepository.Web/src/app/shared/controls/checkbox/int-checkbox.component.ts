import { Component, ElementRef, forwardRef, Host, Input, Optional, Renderer, SkipSelf, ViewChild } from '@angular/core';
import { AbstractControl, ControlContainer, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';

@Component({
  selector: 'app-int-checkbox',
  templateUrl: './int-checkbox.component.html',
  styleUrls: ['./int-checkbox.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InternalCheckboxComponent),
      multi: true
    }
  ]
})
export class InternalCheckboxComponent implements ControlValueAccessor {

  // The internal data model
  private innerValue = false;
  @Input() formControlName: string;
  @ViewChild('chk', { static: true }) chk: ElementRef;
  formControl: AbstractControl;

  id = '';
  // Placeholders for the callbacks which are later providesd
  // by the Control Value Accessor
  private onTouchedCallback = () => { };
  private onChangeCallback = (_: any) => { };

  // get accessor
  get value(): boolean {
    return this.innerValue;
  }

  // set accessor including call the onchange callback
  set value(v: boolean) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }
  setDisabledState(isDisabled: boolean) {
    this.renderer.setElementProperty(this.chk.nativeElement, 'disabled', isDisabled);
    // disable other components here
  }
  constructor(
    private _logger: LoggerService,
    private renderer: Renderer,
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer) {
    this._logger = _logger.createInstance('InternalCheckbox');
    this.id = 'chk-' + Math.random() * 16;
  }

  // Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }

  // From ControlValueAccessor interface
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
      this.getFormControl();
    }
  }

  getFormControl() {
    if (this.controlContainer) {
      if (this.formControlName) {
        this.formControl = this.controlContainer.control.get(this.formControlName);
        this._logger.debug('Custom checknox for ' + this.formControlName);
        this.setDisabledState(this.formControl.disabled);
      } else {
        this._logger.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      this._logger.warn('Can\'t find parent FormGroup directive');
    }
  }
  // From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  // From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

}
