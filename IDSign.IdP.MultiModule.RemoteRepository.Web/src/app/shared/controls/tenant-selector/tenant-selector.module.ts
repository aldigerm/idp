import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TenantSelectorComponent } from './tenant-selector.component';
import { InternalDropdownControl } from '../dropdown/int-dropdown.module';
import { InternalTextBoxControl } from '../text-control/int-text-box.module';
import { ProjectSelectorModule } from '../project-selector/project-selector.module';
import { InternalLabelControl } from '../label/int-label.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    InternalDropdownControl,
    InternalTextBoxControl,
    ProjectSelectorModule,
    InternalLabelControl
  ],
  declarations: [
    TenantSelectorComponent
  ],
  exports: [TenantSelectorComponent]
})
export class TenantSelectorControl { }
