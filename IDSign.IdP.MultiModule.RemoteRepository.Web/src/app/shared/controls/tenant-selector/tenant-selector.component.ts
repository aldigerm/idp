import { Component, OnInit, Input, forwardRef, Optional, Host, SkipSelf, ViewChild, ChangeDetectorRef } from '@angular/core';
import { LoggerService } from '@app/core/logging/logger.service';
import {
  FormBuilder, FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR,
  AbstractControl, Validators, ControlContainer, NG_VALIDATORS
} from '@angular/forms';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { IMultiSelectTexts, IMultiSelectSettings, IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { TranslateService } from '@ngx-translate/core';
import { TenantIdentifier } from '@models/identifiers/tenant-identifier';
import { InternalDropdownComponent } from '../dropdown/int-dropdown.component';
import { ProjectSelectorComponent } from '../project-selector/project-selector.component';
import { ProjectIdentifier } from '@models/identifiers/project-identifier';
import { UserService } from '@http/user/user.service';
import { ProjectBasicModel } from '@models/remote-repository/project/remote-repository-project-basic';
import { TenantBasicModel } from '@models/remote-repository/tenant/remote-repository-tenant-basic';
import { SiteChangeDetectorService } from '@services/site-change-detector.service';

@Component({
  selector: 'app-tenant-selector',
  templateUrl: './tenant-selector.component.html',
  styleUrls: ['./tenant-selector.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TenantSelectorComponent),
      multi: true
    }
  ]
})

export class TenantSelectorComponent implements OnInit, ControlValueAccessor {

  faMinus = faMinus;
  faPlus = faPlus;
  disabled = false;
  isValid = false;
  removed = false;
  isSingleProject = false;
  isSingleTenant = false;
  singleProject = '';
  tenantCode = '';
  tenantName = '';
  showLabel = false;
  _readonly = false;

  form: FormGroup;
  private _model = <TenantIdentifier>{};
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @ViewChild(ProjectSelectorComponent, { static: true }) projectControl: ProjectSelectorComponent;
  @Input() formControlName: string;
  @Input() set readonly(value: boolean) {
    this._readonly = value;
  } get readonly(): boolean {
    return this._readonly;
  }
  @Input() initialiseWithCurrentlyAccessible = false;
  @Input() set model(value: TenantIdentifier) {
    this._model = value;
    if (value) {
      this._model = value;
    } else {
      this._model = <TenantIdentifier>{};
      this.updateTenantsForProject('');
      this.propagateChange(this._model);
    }
    this.initForm();
  } get model() {
    return this._model;
  }
  projectOptions: IMultiSelectOption[];
  tenantOptions: IMultiSelectOption[];
  private _projects: ProjectBasicModel[];
  private _tenants: TenantBasicModel[];

  constructor(
    private _logger: LoggerService,
    private _userService: UserService,
    private _fb: FormBuilder,
    private _siteChangeDetector: SiteChangeDetectorService,
    private _changeDetectionRef: ChangeDetectorRef
  ) {
    this._logger = this._logger.createInstance('TenantSelectorComponent');
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    if (obj) {
      this.model = obj;
    } else {
      this.model = <TenantIdentifier>{};
    }

    this.initForm();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  initForm() {
    const projectCode = this.model.projectCode || this.singleProject;
    const projectIdentifier = projectCode ? <ProjectIdentifier>{ projectCode: projectCode } : <ProjectIdentifier>{};

    if (this.form) {
      this.form.get('project').setValue(projectIdentifier);
      this.form.get('tenant').setValue(this.model.tenantCode || this.tenantCode || '');
    } else {
      this.form = this._fb.group({
        project: [projectIdentifier],
        tenant: [this.model.tenantCode || this.tenantCode || '']
      });

      this.form.get('project').valueChanges.subscribe(
        (value: ProjectIdentifier) => {
          if (value.projectCode !== this.model.projectCode) {
            this.model.projectCode = value.projectCode;
            this.updateTenantsForProject(value.projectCode);
            this.propagateChange(this.model);
          }
        });


      this.form.get('tenant').valueChanges.subscribe(
        (value: string) => {
          if (value !== this._model.tenantCode) {
            this.model.tenantCode = value;
            this.propagateChange(this.model);
          }
        });
    }

    if (this.model.projectCode) {
      this.updateTenantsForProject(this.model.projectCode);
    }
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  public initialiseWithAcessible() {
    this._userService.getUser().then(user => {
      this.setSelection(user.userAccessDetail.projects, user.userAccessDetail.tenants);
    }).catch(error => {
      this._logger.error('Couldnt initialise tenant dropdown', error);
    });
  }

  public setSelection(projects: ProjectBasicModel[], tenants: TenantBasicModel[]): void {
    this._projects = projects;
    this._tenants = tenants;
    this.projectOptions = new Array<IMultiSelectOption>();
    this.tenantOptions = new Array<IMultiSelectOption>();
    if (projects) {
      this.projectControl.setSelection(projects);
      if (this.projectControl.isSingleProject) {
        this.singleProject = this.projectControl.singleProjectCode;
        this.updateTenantsForProject(this.singleProject);
        this.propagateChange(this.model);
      }
    }
  }

  updateTenantsForProject(projectCode: string) {
    this.tenantOptions = new Array<IMultiSelectOption>();
    if (this._tenants) {
      this._tenants.filter(t => t.identifier.projectCode === projectCode).forEach(tenant => {
        this.tenantOptions.push(
          {
            id: tenant.identifier.tenantCode,
            name: tenant.name + ' (' + tenant.identifier.tenantCode + ')'
          });
      });
    }
    if (this.readonly && this.model.tenantCode && this.tenantOptions) {
      const tenant = this.tenantOptions.find(p => p.id === this.model.tenantCode);
      // look for the tenant
      if (tenant) {
        this.isSingleTenant = true;
        this.tenantName = tenant.name;
        this.showLabel = true;
      } else {
        // it wasn't found, so we reset
        this.model.tenantCode = '';
        this.initForm();
      }
      // check if there is only 1 option to select from
    } else if (this.tenantOptions.length === 1) {
      this.isSingleTenant = true;
      this.tenantCode = this.tenantOptions[0].id;
      this.tenantName = this.tenantOptions[0].name;
      this.model.tenantCode = this.tenantCode;
      this.showLabel = true;
      // check if the list has the current model option
    } else {
      const existingTenant = this.tenantOptions.find(t => t.id === this.model.tenantCode);
      if (existingTenant) {
        this.tenantCode = existingTenant.id;
        this.tenantName = existingTenant.name;
        this.model.tenantCode = this.tenantCode;
      } else {
        // tenant is new so we show the list to select from
        this.isSingleTenant = false;
        this.showLabel = false;
        this.model.tenantCode = '';
      }
    }
    this._siteChangeDetector.detectChanges(this._changeDetectionRef);
  }

  getDropDownValue(value: any): string {
    if (value) {
      if (this.isString(value)) {
        return value;
      } else if (this.isArray(value)) {
        return value[0];
      }
    }
    return '';
  }

  // Returns if a value is an array
  isArray(value) {
    return value && typeof value === 'object' && value.constructor === Array;
  }

  // Returns if a value is a string
  isString(value) {
    return typeof value === 'string' || value instanceof String;
  }
  public getModel(): TenantIdentifier {
    return this.model;
  }

  ngOnInit() {

    if (this.initialiseWithCurrentlyAccessible) {
      this.initialiseWithAcessible();
    }

    this.initForm();
  }
}
