import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegexTesterComponent } from './regex.component';
import { TranslateModule } from '@ngx-translate/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FontAwesomeModule
  ],
  declarations: [RegexTesterComponent],
  exports: [RegexTesterComponent]
})
export class RegexTesterModule { }
