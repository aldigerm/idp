import { Component, ElementRef, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { faCheck, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { isRegexMatch } from '@app/sharedfunctions/regex.function';

@Component({
  selector: 'app-regex',
  templateUrl: './regex.component.html',
  styleUrls: ['./regex.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RegexTesterComponent),
      multi: true
    }
  ]
})

export class RegexTesterComponent implements OnInit, ControlValueAccessor {

  disabled = false;
  isValid = false;
  isMatch = false;
  faTimesCircle = faTimesCircle;
  faCheck = faCheck;
  private _model = '';
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @ViewChild('testInput', { static: true }) testInput: ElementRef;
  @Input() formControlName: string;
  @Input() readonly = false;
  @Input() translatePrefix = '';
  @Input() set model(value: string) {
    this._model = value;
  } get model() {
    return this._model;
  }

  constructor() {
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.propagateChange(obj);
    this.propagateTouch(obj);
    this.testRegex();
  }

  set value(val: string) {
    this._model = val;
    this.propagateChange(val);
    this.propagateTouch(val);
  } get value(): string {
    return this._model;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  testRegex() {
    const input = this.testInput && this.testInput.nativeElement ? this.testInput.nativeElement.value : '';
    if (input) {
      this.isMatch = isRegexMatch(this.model, input);
    }
  }

  public getModel(): string {
    return this.model;
  }

  ngOnInit() {
  }
}
