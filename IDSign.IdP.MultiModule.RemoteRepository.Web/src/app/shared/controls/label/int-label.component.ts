import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'int-label',
  templateUrl: './int-label.component.html',
  styleUrls: ['./int-label.component.css']
})
export class LabelComponent implements OnInit {

  @Input() value = '';
  @Input() controlName = '';
  @Input() translatePrefix = '';
  constructor() { }

  ngOnInit() {
  }

}
