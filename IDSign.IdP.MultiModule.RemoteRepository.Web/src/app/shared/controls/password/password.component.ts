import { Component, forwardRef, Host, Input, OnInit, Optional, SkipSelf, ViewChild } from '@angular/core';
import {
  AbstractControl, ControlContainer
  , ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators
} from '@angular/forms';
import { LoggerService } from '@app/core/logging/logger.service';
import { isRegexMatch } from '@app/sharedfunctions/regex.function';
import { matchPassword } from '@app/sharedvalidators/password.validator';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { RemoteRepositoryPasswordPolicyType } from '@models/remote-repository/password-policy/remote-repository-password-policy-type.enum';

@Component({
  selector: 'int-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordComponent),
      multi: true
    }
  ]
})
export class PasswordComponent implements OnInit, ControlValueAccessor {

  formControl: AbstractControl;
  disabled = false;
  isValid = false;

  form: FormGroup;
  private _model = '';
  @ViewChild('value', { static: false }) input: HTMLInputElement;
  @Input() formControlName: string;
  @Input() readonly = false;
  @Input() translatePrefix = '';
  @Input() set passwordPolicies(value: PasswordPolicyDetailModel[]) {
    const passwordPoliciesMatches = new Array<PasswordPolicyDetailModel>();
    if (value) {
      value.forEach(pp => {
        if (pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode !== RemoteRepositoryPasswordPolicyType.History) {
          passwordPoliciesMatches.push(pp);
        }
      });
      this.passwordPoliciesMatches = passwordPoliciesMatches;
    }
  }

  passwordPoliciesMatches = new Array<PasswordPolicyDetailModel>();

  @Input() set model(value: string) {
    this._model = value;
    this.initForm();
  } get model() {
    return this._model;
  }

  constructor(
    @Optional() @Host() @SkipSelf()
    private controlContainer: ControlContainer,
    private _logger: LoggerService,
    private _fb: FormBuilder
  ) {
    this._logger = _logger.createInstance('PasswordComponent');
  }

  propagateChange = (_: any) => { };
  propagateTouch = (_: any) => { };

  writeValue(obj: any): void {
    this.model = obj;
    this.initForm();
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  initForm() {
    if (this.form) {
      this.form.get('password').setValue('');
      this.form.get('confirmPassword').setValue('');
    } else {
      this.form = this._fb.group({
        password: [this.model || '', Validators.required],
        confirmPassword: ['', Validators.required]
      }, {
        validator: matchPassword // your validation method
      });

      this.form.get('password').valueChanges.subscribe(
        (value: string) => {
          if (this.passwordPoliciesMatches) {
            const passwordPoliciesMatches = new Array<PasswordPolicyDetailModel>();

            this.passwordPoliciesMatches.forEach(pp => {
              if (pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.Regex) {
                pp.passed = isRegexMatch(pp.regex, value);
              } else if (pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MinimumLength) {
                pp.passed = value.length >= pp.length;
              } else if (pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MaximumLength) {
                pp.passed = value.length <= pp.length;
              } else {
                pp.passed = true;
              }
              passwordPoliciesMatches.push(pp);
            });

            this.passwordPoliciesMatches = passwordPoliciesMatches;
          }
        });

      this.form.get('confirmPassword').valueChanges.subscribe(
        (value: string) => {
          let passed = true;
          if (!this.form.validator(this.form)) {
            if (this.passwordPoliciesMatches) {
              this.passwordPoliciesMatches.forEach(ppm => {
                if (!ppm.passed) {
                  passed = false;
                }
              });
            }
            if (passed) {
              this.propagateChange(value);
            } else {
              this.propagateChange('');
            }
          }
        });
    }
  }

  public getModel(): string {
    return this.model;
  }

  ngOnInit() {

    if (this.controlContainer) {
      if (this.formControlName) {
        this.formControl = this.controlContainer.control.get(this.formControlName);
        this._logger.debug('Custom text box for ' + this.formControlName);
      } else {
        this._logger.warn('Missing FormControlName directive from host element of the component');
      }
    } else {
      this._logger.warn('Can\'t find parent FormGroup directive');
    }

  }

}
