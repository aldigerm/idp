import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShowErrorsModule } from '@app/shared/components/show-errors/show-errors.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { PasswordPolicyValidationResultModule } from './password-policy-validation-result/password-policy-validation-result.module';
import { PasswordComponent } from './password.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShowErrorsModule,
    FontAwesomeModule,
    TranslateModule,
    MultiselectDropdownModule,
    PasswordPolicyValidationResultModule
  ],
  declarations: [PasswordComponent]
  , exports: [PasswordComponent]
})
export class PasswordControl { }
