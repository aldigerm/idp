import { Component, Input, OnInit } from '@angular/core';
import { faCheck, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { PasswordPolicyDetailModel } from '@models/remote-repository/password-policy/remote-repository-password-policy-detail';
import { RemoteRepositoryPasswordPolicyType } from '@models/remote-repository/password-policy/remote-repository-password-policy-type.enum';

@Component({
  selector: 'app-password-policy-validation-result',
  templateUrl: './password-policy-validation-result.component.html',
  styleUrls: ['./password-policy-validation-result.component.css']
})
export class PasswordPolicyValidationResultComponent implements OnInit {

  faCheck = faCheck;
  faTimesCircle = faTimesCircle;
  passwordPoliciesDisplay: Array<PasswordPolicyDisplayModel>;

  @Input() set passwordPolicies(value: Array<PasswordPolicyDetailModel>) {
    if (value) {
      const passwordPoliciesDisplay = new Array<PasswordPolicyDisplayModel>();
      value.forEach(pp => {
        const ppd = <PasswordPolicyDisplayModel>{
          ...pp
        };
        ppd.code = pp.identifier.passwordPolicyCode;
        ppd.isMaximumLength = pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MaximumLength;
        ppd.isMinimumLength = pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.MinimumLength;
        ppd.isRegex = pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.Regex;
        ppd.isHistory = pp.passwordPolicyTypeIdentifier.passwordPolicyTypeCode === RemoteRepositoryPasswordPolicyType.History;
        ppd.params = { length: pp.length, historyLimit: pp.historyLimit };
        passwordPoliciesDisplay.push(ppd);
      });
      this.passwordPoliciesDisplay = passwordPoliciesDisplay;
    } else {
      this.passwordPoliciesDisplay = null;
    }
  }

  constructor() { }

  ngOnInit() {
  }

}

export interface PasswordPolicyDisplayModel extends PasswordPolicyDetailModel {
  code: string;
  isRegex: boolean;
  isMinimumLength: boolean;
  isMaximumLength: boolean;
  isHistory: boolean;
  params: any;
}
