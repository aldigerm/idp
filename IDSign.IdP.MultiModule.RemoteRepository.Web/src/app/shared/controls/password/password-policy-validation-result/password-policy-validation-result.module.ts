import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { PasswordPolicyValidationResultComponent } from './password-policy-validation-result.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    TranslateModule
  ],
  declarations: [PasswordPolicyValidationResultComponent],
  exports: [PasswordPolicyValidationResultComponent]
})
export class PasswordPolicyValidationResultModule { }
