import {
  AccessControlModel, ApiKeys, AppSettings
  , BrowserStorageKeys, DateFormats, DefaultValues, EnvironmentModel
  , ExternalUrls, FeatureControlModel, IdpClientCredentials
  , IdpClientCredentialsList,
  RoutesKeys
} from '@app/core/configs/global';
import { IdSignConfig } from '@models/idsign-config';
import { LoggingLevel } from '@models/logging/logging-level.enum';

export const idpClientCredentialsList = new IdpClientCredentialsList(
  new Array<IdpClientCredentials>(
    { companyCode: 'default', projectCode: 'idonboard' })
);
export const RouteLinks = RoutesKeys;
export const environment = new EnvironmentModel(
  new AppSettings(
    'default-unloaded',
    'https://localhost.idonboard.co.uk/IDOnBoard.Web/settings',
    false,
    new ExternalUrls(),
    undefined,
    new DateFormats(),
    LoggingLevel.TRACE,
    new IdSignConfig(),
    new DefaultValues(),
    new ApiKeys(),
    new AccessControlModel(),
    new FeatureControlModel(),
    new BrowserStorageKeys(),
    undefined,
    undefined
  )
);
