﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.IO;

namespace IDSign.IdP.MultiModule.RemoteRepository.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddCors();
            services.Configure<AngularSettings>(Configuration.GetSection("AngularSettings"));

            services
              .AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            IConfigurationSection sectionData = Configuration.GetSection("AngularSettings");
            var appSettings = new AngularSettings();
            sectionData.Bind(appSettings);

            string onlineUrlCheck = Configuration["OnlineUrlCheck"];
            string angularSettingsUrl = Configuration["AngularSettingsUrl"];

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            #region CORS Configuration

            string allowedOrigins = Configuration["allowedOrigins"];
            if (!String.IsNullOrWhiteSpace(allowedOrigins))
            {
                string[] origins = allowedOrigins.Split(";");
                app.UseCors(builder =>
                  builder.WithOrigins(origins)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        );
            }

            #endregion

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    context.Request.Path.Value.ToLower().EndsWith(onlineUrlCheck))
                {
                    context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync("true");
                }
                else if (context.Response.StatusCode == 404 &&
                  !Path.HasExtension(context.Request.Path.Value) &&
                  context.Request.Path.Value.ToLower().EndsWith(angularSettingsUrl))
                {
                    context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(appSettings));
                }
                else if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });
            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
