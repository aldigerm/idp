call ng build --configuration=prod --base-href /idsign.idp.remoteusers.web/ --prod

for /f "tokens=2 delims==" %%I in ('wmic os get localdatetime /format:list') do set datetime=%%I

set datetime=%datetime:~0,8%_%datetime:~8,6%
set fileName=wwwroot_basehref_idonboard.web_%datetime%.zip

rem zip
echo outputting to %fileName%
tar.exe -a -c -f %fileName% wwwroot

rem Sound is made when the proess finished. 
rem This is Optional and can be removed.
rundll32 user32.dll,MessageBeep

rem Allows user to see the result
pause
