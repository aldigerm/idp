using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Web
{
    public class AngularSettings
    {
        public string version { get; set; }

        public string envName { get; set; }

        public bool production { get; set; }

        public ExternalUrls externalUrls { get; set; }

        public IdsignAuthConfig idPConfig { get; set; }

        public string loggingLevelString { get; set; }

        public IdpClientCredentialsList idpClientCredentialsList { get; set; }

        public string companyCode { get; set; }

        public FeatureControlModel featureControl { get; set; }
    }

    public class ExternalUrls
    {
        public string apiEndpoint { get; set; }
    }

    public class IdsignAuthConfig
    {
        public string issuer { get; set; }
        public string redirectUri { get; set; }
        public string silentRefreshRedirectUri { get; set; }
        public string clientId { get; set; }
        public string scope { get; set; }
        public bool showDebugInformation { get; set; }
        public bool sessionChecksEnabled { get; set; }
        public bool requireHttps { get; set; }
        public string dummyClientSecret { get; set; }
    }
    public class IdpClientCredentialsList
    {
        public List<IdpClientCredentials> list { get; set; }
    }
    public class IdpClientCredentials
    {
        public string settingsCode { get; set; }
        public string projectCode { get; set; }
        public string companyCode { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public bool isDefault { get; set; }
        public string lang { get; set; }
    }

    public class FeatureControlModel
    {
        public List<string> allowedFeatures { get; set; }
    }
}
