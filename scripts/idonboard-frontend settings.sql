/****** Script for SelectTopNRows command from SSMS  ******/

DECLARE @clientIdCode VARCHAR(1000) =  'id-onboard'; ---- this is the clientId
DECLARE @baseServer VARCHAR(1000) = 'https://mb/idonboard.web'; ---- this is the base url of the local website
DECLARE @successfulLoginRelativeRedirectPath VARCHAR(1000) = '/callback';
DECLARE @postLogoutRelativeRedirectPath VARCHAR(1000) = '/login';
DECLARE @silentRefreshRelativePath VARCHAR(1000) = '/silent-refresh.html';
 

DECLARE @clientId VARCHAR(1000) = (SELECT ID FROM dbo.Clients c WHERE c.ClientId = @clientIdCode);

DECLARE  @BaseUrls TABLE ( Url NVARCHAR(500));
			
INSERT @BaseUrls (Url)
SELECT  Value as Unit FROM STRING_SPLIT(@baseServer , ',')  

-- redirect after login page with token
INSERT [dbo].[ClientRedirectUris] ([ClientId], [RedirectUri]) 
		SELECT @clientId, url 
		+ @successfulLoginRelativeRedirectPath	
		from @BaseUrls url 
		WHERE NOT EXISTS (SELECT ID FROM [dbo].[ClientRedirectUris] 
							WHERE [ClientId] = @clientId AND [RedirectUri] = (url 
							+ @successfulLoginRelativeRedirectPath
							)) 

UNION	SELECT @clientId, url 
		+ @silentRefreshRelativePath				
		from @BaseUrls url
		WHERE NOT EXISTS (SELECT ID FROM [dbo].[ClientRedirectUris] 
							WHERE [ClientId] = @clientId AND [RedirectUri] = (url 
							+ @silentRefreshRelativePath
							)) 

UNION	SELECT @clientId, url 
		from @BaseUrls url
		WHERE NOT EXISTS (SELECT ID FROM [dbo].[ClientRedirectUris] 
							WHERE [ClientId] = @clientId AND [RedirectUri] = (url 
							)) 
		
INSERT [dbo].[ClientPostLogoutRedirectUris] ([ClientId], [PostLogoutRedirectUri]) 
		SELECT @clientId, url + @postLogoutRelativeRedirectPath			
		from @BaseUrls url
		WHERE NOT EXISTS (SELECT ID FROM [dbo].[ClientPostLogoutRedirectUris] 
							WHERE [ClientId] = @clientId AND [PostLogoutRedirectUri] = (url 
							+ @postLogoutRelativeRedirectPath
							))  
    