-- !!run these in IDSign.Idp site folder (where the Startup.cs class is)!!

-- to create a migration. 
dotnet ef migrations add initialcreate --project ../IDSign.Idp.Model.EF

-- to update migration
dotnet ef database update --project ../IDSign.Idp.Model.EF

dotnet ef migrations add --project ../IDSign.Idp.Model.EF InitialIdentityServerPersistedGrantDbMigration -c PersistedGrantDbContext -o Migrations/IdentityServer/PersistedGrantDb
dotnet ef migrations add --project ../IDSign.Idp.Model.EF InitialIdentityServerConfigurationDbMigration -c ConfigurationDbContext -o Migrations/IdentityServer/ConfigurationDb

dotnet ef database update --project ../IDSign.Idp.Model.EF --context ConfigurationDbContext
dotnet ef database update --project ../IDSign.Idp.Model.EF --context PersistedGrantDbContext