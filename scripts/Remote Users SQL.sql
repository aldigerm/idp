DECLARE @apiResourceId VARCHAR(50) =  'idonboard-webhub';

DELETE ac
FROM [dbo].[ApiResources] ar 
inner join dbo.ApiClaims ac on ac.ApiResourceId = ar.Id
where ar.Name = @apiResourceId and ac.Type in ('remote-repository-tenant','remote-repository-project')

INSERT INTO dbo.ApiClaims(ApiResourceId,Type)
		SELECT ar.id, 'remote-repository-tenant'
		FROM [dbo].[ApiResources] ar 
		inner join dbo.ApiClaims ac on ac.ApiResourceId = ar.Id
		where ar.Name = @apiResourceId
union	SELECT ar.id, 'remote-repository-project'
		FROM [dbo].[ApiResources] ar 
		inner join dbo.ApiClaims ac on ac.ApiResourceId = ar.Id
		where ar.Name = @apiResourceId

update c
	set c.ClientId = 'id-onboard'
from dbo.Clients c where c.ClientId = 'id-onboard-fendi'

delete cc -- these claims will be added in the code 
from dbo.clients c
inner join dbo.ClientClaims cc on cc.ClientId = c.id
where cc.Type in ('remote-repository-tenant','remote-repository-project') and c.ClientId = 'id-onboard' -- set name here

