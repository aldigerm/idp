
DECLARE @apiResource VARCHAR(128) = 'remoteuser-webhub'; 
DECLARE @apiSecret VARCHAR(128) = 'remoteuser-webhub'; -- this is the secret in plain text to validate reference tokens
DECLARE @apiScopeShowInDiscoveryDocument BIT = 0; -- show or NOT show in discovery document (extra security) (0 don't show, 1 show)
DECLARE @apiResourceClaims VARCHAR(MAX) = 'remote-repository-tenant,remote-repository-project,remote-repository-repo-username'

INSERT [dbo].[ApiResources] ([Description], [DisplayName], [Enabled], [Name], [Created], [LastAccessed], [NonEditable], [Updated]) 
        SELECT TOP 1 @apiResource + ' Resource', @apiResource, 1, @apiResource, GETDATE(), NULL, 0, NULL
        FROM [dbo].[ApiResources] api WHERE  NOT EXISTS (SELECT * FROM dbo.ApiResources apis WHERE apis.Name = @apiResource)
        -- add mandatory scope with same name as resource
        INSERT INTO [dbo].[ApiScopes]
                    ([ApiResourceId]
                    ,[Description]
                    ,[DisplayName]
                    ,[Emphasize]
                    ,[Name]
                    ,[Required]
                    ,[ShowInDiscoveryDocument])
        SELECT TOP 1 api.Id, @apiResource + ' Resource Scope', @apiResource, 0, @apiResource, 0, @apiScopeShowInDiscoveryDocument
        FROM [dbo].[ApiResources] api WHERE api.Name = @apiResource AND NOT EXISTS (SELECT * FROM dbo.[ApiScopes] apisc
                                                        INNER JOIN dbo.ApiResources apis ON apisc.ApiResourceId = apis.id
                                                                WHERE apis.Name = @apiResource AND apisc.Name = @apiResource)
        -- add secret
        DECLARE @APIHASHBYTES VARBINARY(128) = hashbytes('sha2_256', @apiSecret)
        DECLARE @APISecretHash VARCHAR(128) = (SELECT cast(N'' as xml).value('xs:base64Binary(sql:variable("@APIHASHBYTES"))', 'varchar(128)'));
        INSERT INTO [dbo].[ApiSecrets]
                    ([ApiResourceId]
                    ,[Description]
                    ,[Expiration]
                    ,[Type]
                    ,[Value]
                    ,[Created])
        SELECT
                    api.Id
                    ,@apiResource + ' secret'
                    ,NULL
                    ,'SharedSecret'
                    ,@APISecretHash
                    ,GETDATE()
                FROM dbo.ApiResources api 
                WHERE api.Name = @apiResource and NOT EXISTS(SELECT * FROM [dbo].[ApiSecrets] aps 
                                                            INNER JOIN dbo.ApiResources apis ON apis.Id = aps.ApiResourceId
                                                            WHERE apis.Name = @apiResource AND aps.ApiResourceId = api.Id)
   
        IF(@apiResourceClaims is not null)
		BEGIN
			SELECT Value
			INTO #Temp
			FROM STRING_SPLIT(@apiResourceClaims , ',')
			

			INSERT [dbo].[ApiClaims] (ApiResourceId, [Type])
			SELECT api.Id, t.Value as Unit 
			FROM #Temp t 
			INNER JOIN [dbo].[ApiResources] api ON api.Name = @apiResource
			LEFT JOIN dbo.ApiClaims apic ON apic.ApiResourceId = api.Id AND apic.Type = t.value
			WHERE apic.Id is null

			DROP TABLE #TEMP
		END