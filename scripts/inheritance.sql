/****** Script for SelectTopNRows command from SSMS  ******/
SELECT 'module',m.Code,' is for role ', r.Code, t.Code
  FROM [dbo].[RoleModule] rm
  inner join Role r on r.Id = rm.RoleId
  inner join Module m on m.Id = rm.ModuleId
  inner join Tenant t on t.Id = r.TenantId
  where t.Code = 'dof' 
  order by m.Code

  select 'role ', r.Code, 'is also',ri.Code
  FROM dbo.RoleInheritance rh
  inner join Role r on rh.RoleId = r.ID
  inner join Role ri on rh.InheritsFromId = ri.Id
  
  select 'group ', ug.Code, 'is in role',r.Code, t.Code
  FROM dbo.UserGroupRole ugr
  inner join Role r on ugr.RoleId = r.ID
  inner join UserGroup ug on ugr.UserGroupId = ug.Id
  inner join Tenant t on t.Id = r.TenantId
  where t.Code = 'dof'
  order by ug.Code

  select 'group',ug.Code, ' is in group',ugi.Code, t.Code
  FROM dbo.UserGroupInheritance ugh
  inner join UserGroup ug on ugh.UserGroupId = ug.ID
  inner join UserGroup ugi on ugh.InheritsFromId = ugi.Id
  inner join Tenant t on t.Id = ug.TenantId
  where t.Code = 'dof'
  
  select 'user',u.TenantUsername, ' is in group',ug.Code, t.Code
  FROM dbo.UserToUserGroup uug
  inner join UserGroup ug on uug.UserGroupId = ug.ID
  inner join [UserTenant] ut on ut.Id = uug.UserTenantId
  inner join [User] u on ut.UserId = u.Id
  inner join Tenant t on t.Id = ug.TenantId
  where t.Code = 'dof'
  order by u.id, ug.Code

  select 'group', ug.Id, ug.Code, 'has value', ugc.Value, 'of type', ugct.Id, ugct.Type,ugct.Description
  FROM UserGroup ug 
  INNER JOIN UserGroupClaim ugc on ug.Id = ugc.UserGroupId
  INNER JOIN UserGroupClaimType ugct on ugc.UserGroupClaimTypeId = ugct.Id
