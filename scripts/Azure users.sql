-- ======================================================================================
-- Create SQL Login template for Azure SQL Database and Azure SQL Data Warehouse Database
-- ======================================================================================
--USE master
CREATE LOGIN sa_wexflow_hangfire
	WITH PASSWORD = '' 
GO

--USE [Idp.Core]

CREATE USER sa_wexflow_hangfire
	FOR LOGIN sa_wexflow_hangfire
GO

CREATE SCHEMA hgfw
AUTHORIZATION sa_wexflow_hangfire
GO

GRANT CREATE TABLE TO sa_id_onboard_hangfire;

GRANT CREATE TABLE TO sa_wexflow_hangfire;

EXEC sp_addrolemember N'db_owner','sa_idsign_idp_core'
GO

--use master

CREATE LOGIN sa_idsign_idp_users
	WITH PASSWORD = '' 
GO

CREATE LOGIN sa_idsign_idp_users_log
	WITH PASSWORD = '' 
GO

--use [Idp.RemoteRepository]


CREATE USER sa_idsign_idp_users
	FOR LOGIN sa_idsign_idp_users
GO

CREATE USER sa_idsign_idp_users_log
	FOR LOGIN sa_idsign_idp_users_log
GO

EXEC sp_addrolemember N'db_owner','sa_idsign_idp_users'
GO
EXEC sp_addrolemember N'db_owner','sa_idsign_idp_users_log'
GO