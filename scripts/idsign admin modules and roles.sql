USE [Idp.RemoteRepository]
GO

declare @projectId int = (SELECT top 1 id from Project p where p.Code = 'idonboard');

INSERT INTO [dbo].[Module]
           ([Description]
           ,[Code]
           ,[ProjectId])

      SELECT 'IDSign Admin PROVIDER_USERS'										,'PROVIDER_USERS',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_LIST'									,'PROVIDER_USERS_LIST',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_DETAIL'								,'PROVIDER_USERS_DETAIL',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_DETAIL_VIEW'							,'PROVIDER_USERS_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_DETAIL_APPROVE'						,'PROVIDER_USERS_DETAIL_APPROVE',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_DETAIL_REJECT'						,'PROVIDER_USERS_DETAIL_REJECT',@projectId
union SELECT 'IDSign Admin PROVIDER_USERS_LIST_VIEW'							,'PROVIDER_USERS_LIST_VIEW',@projectId	
union SELECT 'IDSign Admin IDENTITY_LIST'										,'IDENTITY_LIST',@projectId
union SELECT 'IDSign Admin IDENTITY_DETAIL'										,'IDENTITY_DETAIL',@projectId
union SELECT 'IDSign Admin IDENTITY_DETAIL_VIEW'								,'IDENTITY_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin IDENTITY_LIST_VIEW'									,'IDENTITY_LIST_VIEW',@projectId
union SELECT 'IDSign Admin EVIDENCE_VIEW'										,'EVIDENCE_VIEW',@projectId
union SELECT 'IDSign Admin EVIDENCE_ACTIVATE'									,'EVIDENCE_ACTIVATE',@projectId
union SELECT 'IDSign Admin EVIDENCE_REVOKE'										,'EVIDENCE_REVOKE',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_VIEW'							,'BLOCKCHAIN_IDENTITY_VIEW',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_ACTIVATE'						,'BLOCKCHAIN_IDENTITY_ACTIVATE',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_REVOKE'							,'BLOCKCHAIN_IDENTITY_REVOKE',@projectId
union SELECT 'IDSign Admin SESSION_DETAIL'										,'SESSION_DETAIL',@projectId
union SELECT 'IDSign Admin SESSION_LIST'										,'SESSION_LIST',@projectId
union SELECT 'IDSign Admin SESSION_LIST_VIEW'									,'SESSION_LIST_VIEW',@projectId
union SELECT 'IDSign Admin SESSION_DETAIL_COMPLETE'								,'SESSION_DETAIL_COMPLETE',@projectId
union SELECT 'IDSign Admin SESSION_DETAIL_DOWNLOAD'								,'SESSION_DETAIL_DOWNLOAD',@projectId
union SELECT 'IDSign Admin SESSION_DETAIL_SIGN'									,'SESSION_DETAIL_SIGN',@projectId
union SELECT 'IDSign Admin SESSION_DETAIL_VIEW'									,'SESSION_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin CERTIFICATE_ACTIVATE'								,'CERTIFICATE_ACTIVATE',@projectId
union SELECT 'IDSign Admin CERTIFICATE_REVOKE'									,'CERTIFICATE_REVOKE',@projectId
union SELECT 'IDSign Admin CERTIFICATE_VIEW'									,'CERTIFICATE_VIEW',@projectId
union SELECT 'IDSign Admin IDENTITY_LIST'										,'IDENTITY_LIST',@projectId
union SELECT 'IDSign Admin IDENTITY_DETAIL'										,'IDENTITY_DETAIL',@projectId
union SELECT 'IDSign Admin IDENTITY_DETAIL_VIEW'								,'IDENTITY_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin IDENTITY_LIST_VIEW'									,'IDENTITY_LIST_VIEW',@projectId
union SELECT 'IDSign Admin EVIDENCE_VIEW'										,'EVIDENCE_VIEW',@projectId
union SELECT 'IDSign Admin EVIDENCE_ACTIVATE'									,'EVIDENCE_ACTIVATE',@projectId
union SELECT 'IDSign Admin EVIDENCE_REVOKE'										,'EVIDENCE_REVOKE',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_VIEW'							,'BLOCKCHAIN_IDENTITY_VIEW',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_ACTIVATE'						,'BLOCKCHAIN_IDENTITY_ACTIVATE',@projectId
union SELECT 'IDSign Admin BLOCKCHAIN_IDENTITY_REVOKE'							,'BLOCKCHAIN_IDENTITY_REVOKE',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_LIST'							,'VIDEO_IDENTIFICATION_LIST',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_DETAIL'							,'VIDEO_IDENTIFICATION_DETAIL',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_DETAIL_VIEW'					,'VIDEO_IDENTIFICATION_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_LIST_VIEW'						,'VIDEO_IDENTIFICATION_LIST_VIEW',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_DETAIL_APPROVE'					,'VIDEO_IDENTIFICATION_DETAIL_APPROVE',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_DETAIL_REJECT'					,'VIDEO_IDENTIFICATION_DETAIL_REJECT',@projectId
union SELECT 'IDSign Admin VIDEO_IDENTIFICATION_DETAIL_REQUEST_CERTIFICATE'		,'VIDEO_IDENTIFICATION_DETAIL_REQUEST_CERTIFICATE',@projectId
union SELECT 'IDSign Admin IDP_ADMIN_LIST'										,'IDP_ADMIN_LIST',@projectId
union SELECT 'IDSign Admin IDP_ADMIN_DETAIL'									,'IDP_ADMIN_DETAIL',@projectId
union SELECT 'IDSign Admin IDP_ADMIN_DETAIL_VIEW'								,'IDP_ADMIN_DETAIL_VIEW',@projectId
union SELECT 'IDSign Admin IDP_ADMIN_LIST_VIEW'									,'IDP_ADMIN_LIST_VIEW',@projectId


declare @idsignAdminUserRole VARCHAR(50) ='idsign-admin-all'

INSERT INTO [dbo].[Role]
           ([Code]
           ,[Description]
           ,[TenantId]
           ,[Enabled])
SELECT
           @idsignAdminUserRole
           ,'Role for users who can use idsign admin'
           ,t.Id
           , 1 FROM Tenant t

INSERT INTO [dbo].[RoleModule]
           ([RoleId]
           ,[ModuleId])
SELECT
           r.Id
           ,m.Id FROM Module m INNER JOIN Role r ON r.Code =@idsignAdminUserRole WHERE m.Description like 'IDSign%'


declare @supportUserRole VARCHAR(50) ='support'

INSERT INTO [dbo].[RoleInheritance]
           ([RoleId]
           ,[InheritsFromId])
SELECT rsup.Id , radmin.Id FROM Role radmin INNER JOIN Role rsup ON radmin.Code = @idsignAdminUserRole AND rsup.Code = @supportUserRole
GO



