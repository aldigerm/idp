/****** Script for SelectTopNRows command from SSMS  ******/


DECLARE @loginToSearch NVARCHAR(1024) = 'https://signchain-demo.azurewebsites.net/idsign.idp.remoteusers.web/callback';
DECLARE @postLogoutRedirectToSearch NVARCHAR(1024) = 'https://signchain-demo.azurewebsites.net/idsign.idp.remoteusers.web/login';

INSERT [ClientPostLogoutRedirectUris]
( PostLogoutRedirectUri,ClientId)
SELECT @postLogoutRedirectToSearch, cplr.Id
  FROM dbo.Clients cplr
  left join  [dbo].ClientRedirectUris cru  on cru.ClientId = cplr.Id and cru.RedirectUri = @loginToSearch 
  left join [dbo].[ClientPostLogoutRedirectUris] plr on plr.ClientId = cplr.id AND plr.PostLogoutRedirectUri = @postLogoutRedirectToSearch
  WHERE cru.id is not null and plr.id is null
  