
DECLARE @loginToSearch NVARCHAR(1024) = 'https://localhost:4201/callback';
DECLARE @loginToAdd NVARCHAR(1024) = 'https://localhost:4202/callback';

INSERT [ClientRedirectUris]
( RedirectUri,ClientId)
SELECT @loginToAdd, cplr.Id
  FROM dbo.Clients cplr
  left join  [dbo].ClientRedirectUris cru  on cru.ClientId = cplr.Id and cru.RedirectUri = @loginToSearch 
  left join [dbo].[ClientRedirectUris] plr on plr.ClientId = cplr.id AND plr.RedirectUri = @loginToAdd
  WHERE cru.id is not null and plr.id is null