CREATE ROLE [LoggerAutoCreate];
GRANT SELECT ON sys.tables TO [LoggerAutoCreate];
GRANT SELECT ON sys.schemas TO [LoggerAutoCreate];
GRANT ALTER ON SCHEMA::[log] TO [LoggerAutoCreate]
GRANT CREATE TABLE TO [LoggerAutoCreate];

CREATE ROLE [LoggerWriter];
GRANT SELECT TO [LoggerWriter];
GRANT INSERT TO [LoggerWriter];

CREATE LOGIN [idp_logger] WITH PASSWORD = 'Logger-password';

CREATE USER [idp_logger] FOR LOGIN [idp_logger] WITH DEFAULT_SCHEMA = [log];
GRANT CONNECT TO [idp_logger];

ALTER ROLE [LoggerAutoCreate] ADD MEMBER [idp_logger];
ALTER ROLE [LoggerWriter] ADD MEMBER [idp_logger];