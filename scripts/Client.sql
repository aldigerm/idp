﻿/*

	Script to create client through SQL

	Explanation of timeouts: https://github.com/IdentityServer/IdentityServer3/issues/2411
	Explanation of properties: https://identityserver4.readthedocs.io/en/latest/reference/client.html

*/

--	Scope (yours)
--	Username (clientid it for it)
--	Password (to get token) to get a token with the scope

-- 0 : view current data already in the database for this configurations
-- 1 : save the configuration to the db
DECLARE @SaveDataInDBForClientCode BIT = 0;


-- 0 : Remove any other objects related to the Client and Add Again
-- 1 : Keep any other items and only update/insert the Client table.
DECLARE @UpdateInsertClientTableOnly BIT = 0;
-- client id (also username to get token)
----------------------------------

-- Vaco WebHub

----------------------------------

-- client id (also username to get token)
DECLARE @clientCode VARCHAR(100) = 'vaco'; 
 -- scopes to be possible to request for token, set to null if not required.
 -- Use commas (,) to separate the scope names.
DECLARE @clientScopes VARCHAR(MAX) = 'workflow-webhub,vaco-webhub,user-repository-webhub,remoteuser-webhub';
DECLARE @clientSecret VARCHAR(MAX) = null; 
DECLARE  @clientClaims TABLE (ClaimType VARCHAR(100), ClaimValue VARCHAR(100));

DECLARE @apiResource VARCHAR(200) = 'vaco-webhub';
DECLARE @apiScopeShowInDiscoveryDocument BIT = 0;
DECLARE @apiSecret VARCHAR(200) = 'vaco-webhub-secret';
DECLARE @apiClaims VARCHAR(MAX) = null;

 -- If it is this an api then set to 1. Otherwise set to 0.
DECLARE @isApi BIT = 0;
-- let clientid/clientpassword be given to get a token
DECLARE @allowClientCredentials BIT = 0;

/* If it is not an API, then set the following */
DECLARE @baseServer VARCHAR(1000) = 'https://vaco-web.azurewebsites.net'; -- base domain for redirect urls used further on. helpful to not repeat, otherwise add your own
DECLARE @successfulLoginRelativeRedirectPath VARCHAR(1000) = '/callback';
DECLARE @postLogoutRelativeRedirectPath VARCHAR(1000) = '/login';
DECLARE @silentRefreshRelativePath VARCHAR(1000) = '/silent-refresh.html';
DECLARE @devUrl VARCHAR(200) = null; 

;

--	Absolute	0 : the refresh token will expire on a fixed point in time (specified by the AbsoluteRefreshTokenLifetime)
--	Sliding		1 : when refreshing the token, the lifetime of the refresh token will be renewed (by the amount specified in SlidingRefreshTokenLifetime). 
--					The lifetime will not exceed AbsoluteRefreshTokenLifetime.
DECLARE @refreshTokenExpirationType INT = 0;

DECLARE @maximumTimeForTokenToBeValidInMinutes INT = 20160; -- 14 days * 24 hours per day * 60 minutes per hour = 20160

------------------------------------------------------------------------------------------------------
-- if @refreshTokenExpirationType is sliding ( 1 ),
-- how much is the token's lifetime extended when it is being refreshed
------------------------------------------------------------------------------------------------------

-- add how much longer is the token valid for before it needs to be refreshed. 
-- ie. after x minutes, the token needs to be refreshed, otherwise the token would stop being valid.
DECLARE @timeAddedToRefreshTokenExpiry INT = 20160; 

-- how long it would take for a token to be invalid if it wasn't refreshed. 
-- ie. so after x minutes of its refresh lifetime, the token cannot be refreshed and is definitley expired.
DECLARE @timeForTokenToExpireBeforeARefreshIsNeededMinutes INT = 20160; 

-- ReUse	0 : the refresh token handle will stay the same when refreshing tokens
-- OneTime	1 : the refresh token handle will be updated when refreshing tokens. This is the default.
DECLARE @refreshTokenUsageType INT = 1;

DECLARE @accessTokenType INT = 1; -- 0 for JWT tokens, 1 for Reference tokens 


-- check if we have the client already
DECLARE @clientId INT = (SELECT ID FROM dbo.Clients c WHERE c.ClientId = @clientCode);

IF @SaveDataInDBForClientCode = 0 -- view the values currently saved in the database 
BEGIN
    SELECT CASE WHEN c.AccessTokenType = 0 THEN 'JWT' WHEN c.AccessTokenType = 1 THEN 'Reference' ELSE 'Unknown' END, c.ClientId, c.*
    FROM dbo.Clients c 
    WHERE c.Id = @clientId OR @clientId is null
    ORDER BY 2

    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientSecrets] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientScopes] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientRedirectUris] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientProperties] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null

    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientPostLogoutRedirectUris] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientIdPRestrictions] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientGrantTypes] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId 
    FROM [dbo].[ClientCorsOrigins] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null
    
    SELECT cc.* , c.ClientId
    FROM [dbo].[ClientClaims] cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.ClientId = @clientId OR @clientId is null

    SELECT cc.Scope as ScopeForApiResource, c.ClientId as ClientId
    FROM dbo.ClientScopes cc
    INNER JOIN dbo.Clients c ON c.id = cc.ClientId
    WHERE cc.Scope in (@apiResource) OR @clientId is null
    ORDER BY 2,1

    SELECT c.Name as ApiResourceName
    FROM dbo.ApiResources c 
    WHERE c.Name = @apiResource OR @clientId is null
    ORDER BY 1

    SELECT cs.Name as ApiScope, c.Name as ApiResourceName
    FROM dbo.ApiScopes cs
    INNER JOIN dbo.ApiResources c ON c.id = cs.ApiResourceId
    WHERE c.Name = @apiResource OR @clientId is null
    ORDER BY 2,1

    SELECT acs.Type as ApiClaimType, c.Name as ApiResourceName
    FROM dbo.ApiClaims acs
    INNER JOIN dbo.ApiResources c ON c.id = acs.ApiResourceId
    WHERE c.Name = @apiResource OR @clientId is null
    ORDER BY 2,1
	
    SELECT cc.* , c.Name 
    FROM [dbo].[ApiSecrets] cc
    INNER JOIN dbo.ApiResources c ON c.id = cc.ApiResourceId
    WHERE c.Name = @apiResource OR @clientId is null
END
ELSE
BEGIN
    -- remove all client info if found
    IF(@clientId IS NOT NULL AND @UpdateInsertClientTableOnly = 0)
    BEGIN
        DELETE FROM [dbo].[ClientSecrets]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientScopes]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientRedirectUris]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientProperties]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientPostLogoutRedirectUris]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientIdPRestrictions]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientGrantTypes]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientCorsOrigins]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[ClientClaims]
        WHERE ClientId = @clientId
        DELETE FROM [dbo].[Clients]
        WHERE Id = @clientId
    END 

	DECLARE @secondsInAMinute INT = 60;
	DECLARE @maxTokenLifeInSeconds INT = @maximumTimeForTokenToBeValidInMinutes * @secondsInAMinute;
	DECLARE @timeForTokenToExpire INT = @maxTokenLifeInSeconds;
	DECLARE @timeAddedToRefreshTokenExpirySeconds INT = @timeAddedToRefreshTokenExpiry * @secondsInAMinute;
	
	-- if the token is a sliding token
	-- then use the sliding token refresh time as the token time
	IF(@refreshTokenExpirationType = 1)
	BEGIN 
		SET @timeForTokenToExpire = @timeForTokenToExpireBeforeARefreshIsNeededMinutes * @secondsInAMinute;
	END
    
	-- create client it (change token times and other times accordingly)
    DECLARE @ClientTempTable TABLE (
		[AbsoluteRefreshTokenLifetime] [int] NOT NULL,
		[AccessTokenLifetime] [int] NOT NULL,
		[AccessTokenType] [int] NOT NULL,
		[AllowAccessTokensViaBrowser] [bit] NOT NULL,
		[AllowOfflineAccess] [bit] NOT NULL,
		[AllowPlainTextPkce] [bit] NOT NULL,
		[AllowRememberConsent] [bit] NOT NULL,
		[AlwaysIncludeUserClaimsInIdToken] [bit] NOT NULL,
		[AlwaysSendClientClaims] [bit] NOT NULL,
		[AuthorizationCodeLifetime] [int] NOT NULL,
		[BackChannelLogoutSessionRequired] [bit] NOT NULL,
		[BackChannelLogoutUri] [nvarchar](2000) NULL,
		[ClientClaimsPrefix] [nvarchar](200) NULL,
		[ClientId] [nvarchar](200) NOT NULL,
		[ClientName] [nvarchar](200) NULL,
		[ClientUri] [nvarchar](2000) NULL,
		[ConsentLifetime] [int] NULL,
		[Description] [nvarchar](1000) NULL,
		[EnableLocalLogin] [bit] NOT NULL,
		[Enabled] [bit] NOT NULL,
		[FrontChannelLogoutSessionRequired] [bit] NOT NULL,
		[FrontChannelLogoutUri] [nvarchar](2000) NULL,
		[IdentityTokenLifetime] [int] NOT NULL,
		[IncludeJwtId] [bit] NOT NULL,
		[LogoUri] [nvarchar](2000) NULL,
		[PairWiseSubjectSalt] [nvarchar](200) NULL,
		[ProtocolType] [nvarchar](200) NOT NULL,
		[RefreshTokenExpiration] [int] NOT NULL,
		[RefreshTokenUsage] [int] NOT NULL,
		[RequireClientSecret] [bit] NOT NULL,
		[RequireConsent] [bit] NOT NULL,
		[RequirePkce] [bit] NOT NULL,
		[SlidingRefreshTokenLifetime] [int] NOT NULL,
		[UpdateAccessTokenClaimsOnRefresh] [bit] NOT NULL,
		[Created] [datetime2](7) NOT NULL,
		[DeviceCodeLifetime] [int] NOT NULL,
		[LastAccessed] [datetime2](7) NULL,
		[NonEditable] [bit] NOT NULL,
		[Updated] [datetime2](7) NULL,
		[UserCodeType] [nvarchar](100) NULL,
		[UserSsoLifetime] [int] NULL
	)
	
	 INSERT INTO @ClientTempTable
	 (	
			[AbsoluteRefreshTokenLifetime]
			, [AccessTokenLifetime]
			, [AccessTokenType]
			, [AllowAccessTokensViaBrowser]
			, [AllowOfflineAccess]
			, [AllowPlainTextPkce]
			, [AllowRememberConsent]
			, [AlwaysIncludeUserClaimsInIdToken]
			, [AlwaysSendClientClaims]
			, [AuthorizationCodeLifetime]
			, [BackChannelLogoutSessionRequired]
			, [BackChannelLogoutUri]
			, [ClientClaimsPrefix]
			, [ClientId]
			, [ClientName]
			, [ClientUri]
			, [ConsentLifetime]
			, [Description]
			, [EnableLocalLogin]
			, [Enabled]
			, [FrontChannelLogoutSessionRequired]
			, [FrontChannelLogoutUri]
			, [IdentityTokenLifetime]
			, [IncludeJwtId]
			, [LogoUri]
			, [PairWiseSubjectSalt]
			, [ProtocolType]
			, [RefreshTokenExpiration]
			, [RefreshTokenUsage]
			, [RequireClientSecret]
			, [RequireConsent]
			, [RequirePkce]
			, [SlidingRefreshTokenLifetime]
			, [UpdateAccessTokenClaimsOnRefresh]
			, [Created]
			, [DeviceCodeLifetime]
			, [LastAccessed]
			, [NonEditable]
			, [Updated]
			, [UserCodeType]
			, [UserSsoLifetime]
			)
     SELECT
				@maxTokenLifeInSeconds					--	[AbsoluteRefreshTokenLifetime] : Maximum lifetime of a refresh token in seconds. 
			,	@timeForTokenToExpire					--	[AccessTokenLifetime]
			,	@accessTokenType						--	[AccessTokenType]
			,	1										--	[AllowAccessTokensViaBrowser]
			,	1										--	[AllowOfflineAccess]
			,	0										--	[AllowPlainTextPkce]
			,	1										--	[AllowRememberConsent]
			,	1										--	[AlwaysIncludeUserClaimsInIdToken]
			,	1										--	[AlwaysSendClientClaims]
			,	@timeForTokenToExpire					--	[AuthorizationCodeLifetime]
			,	1										--	[BackChannelLogoutSessionRequired]
			,	N''										--	[BackChannelLogoutUri]
			,	N''										--	[ClientClaimsPrefix]
			,	@clientCode								--	[ClientId]
			,	@clientCode								--	[ClientName]
			,	N''										--	[ClientUri]
			,	NULL									--	[ConsentLifetime]
			,	NULL									--	[Description]
			,	1										--	[EnableLocalLogin]
			,	1										--	[Enabled]
			,	1										--	[FrontChannelLogoutSessionRequired]
			,	N''										--	[FrontChannelLogoutUri]
			,	@timeForTokenToExpire					--	[IdentityTokenLifetime]
			,	0										--	[IncludeJwtId]
			,	N''										--	[LogoUri]
			,	N''										--	[PairWiseSubjectSalt]
			,	N'oidc'									--	[ProtocolType]
			,	@refreshTokenExpirationType				--	[RefreshTokenExpiration] 
			,	@refreshTokenUsageType					--	[RefreshTokenUsage]
			,	0										--	[RequireClientSecret]
			,	0										--	[RequireConsent]
			,	0										--	[RequirePkce]
			,	@timeAddedToRefreshTokenExpirySeconds	--	[SlidingRefreshTokenLifetime]
			,	0										--	[UpdateAccessTokenClaimsOnRefresh]
			,	GETDATE()								--	[Created]
			,	@timeForTokenToExpire					--	[DeviceCodeLifetime]
			,	NULL									--	[LastAccessed]
			,	0										--	[NonEditable]
			,	NULL									--	[Updated]
			,	NULL									--	[UserCodeType]
			,	NULL									--	[UserSsoLifetime]
			
			
			MERGE dbo.Clients AS [Target]  
			USING @ClientTempTable AS [Source]
			ON [Target].ClientId = [Source].ClientId
			WHEN MATCHED THEN
				UPDATE 
				SET	[AbsoluteRefreshTokenLifetime]		= [Source].[AbsoluteRefreshTokenLifetime]
					, [AccessTokenLifetime]					= [Source].[AccessTokenLifetime]
					, [AccessTokenType]						= [Source].[AccessTokenType]
					, [AllowAccessTokensViaBrowser]			= [Source].[AllowAccessTokensViaBrowser]
					, [AllowOfflineAccess]					= [Source].[AllowOfflineAccess]
					, [AllowPlainTextPkce]					= [Source].[AllowPlainTextPkce]
					, [AllowRememberConsent]				= [Source].[AllowRememberConsent]
					, [AlwaysIncludeUserClaimsInIdToken]	= [Source].[AlwaysIncludeUserClaimsInIdToken]
					, [AlwaysSendClientClaims]				= [Source].[AlwaysSendClientClaims]
					, [AuthorizationCodeLifetime]			= [Source].[AuthorizationCodeLifetime]
					, [BackChannelLogoutSessionRequired]	= [Source].[BackChannelLogoutSessionRequired]
					, [BackChannelLogoutUri]				= [Source].[BackChannelLogoutUri]
					, [ClientClaimsPrefix]					= [Source].[ClientClaimsPrefix]
					, [ClientId]							= [Source].[ClientId]
					, [ClientName]							= [Source].[ClientName]
					, [ClientUri]							= [Source].[ClientUri]
					, [ConsentLifetime]						= [Source].[ConsentLifetime]
					, [Description]							= [Source].[Description]
					, [EnableLocalLogin]					= [Source].[EnableLocalLogin]
					, [Enabled]								= [Source].[Enabled]
					, [FrontChannelLogoutSessionRequired]	= [Source].[FrontChannelLogoutSessionRequired]
					, [FrontChannelLogoutUri]				= [Source].[FrontChannelLogoutUri]
					, [IdentityTokenLifetime]				= [Source].[IdentityTokenLifetime]
					, [IncludeJwtId]						= [Source].[IncludeJwtId]
					, [LogoUri]								= [Source].[LogoUri]
					, [PairWiseSubjectSalt]					= [Source].[PairWiseSubjectSalt]
					, [ProtocolType]						= [Source].[ProtocolType]
					, [RefreshTokenExpiration]				= [Source].[RefreshTokenExpiration]
					, [RefreshTokenUsage]					= [Source].[RefreshTokenUsage]
					, [RequireClientSecret]					= [Source].[RequireClientSecret]
					, [RequireConsent]						= [Source].[RequireConsent]
					, [RequirePkce]							= [Source].[RequirePkce]
					, [SlidingRefreshTokenLifetime]			= [Source].[SlidingRefreshTokenLifetime]
					, [UpdateAccessTokenClaimsOnRefresh]	= [Source].[UpdateAccessTokenClaimsOnRefresh]
					, [Created]								= [Source].[Created]
					, [DeviceCodeLifetime]					= [Source].[DeviceCodeLifetime]
					, [LastAccessed]						= [Source].[LastAccessed]
					, [NonEditable]							= [Source].[NonEditable]
					, [Updated]								= [Source].[Updated]
					, [UserCodeType]						= [Source].[UserCodeType]
					, [UserSsoLifetime]						= [Source].[UserSsoLifetime]
			
			WHEN NOT MATCHED THEN  	
			-- insert if there are no matches			
				INSERT (	
						[AbsoluteRefreshTokenLifetime]
					, [AccessTokenLifetime]
					, [AccessTokenType]
					, [AllowAccessTokensViaBrowser]
					, [AllowOfflineAccess]
					, [AllowPlainTextPkce]
					, [AllowRememberConsent]
					, [AlwaysIncludeUserClaimsInIdToken]
					, [AlwaysSendClientClaims]
					, [AuthorizationCodeLifetime]
					, [BackChannelLogoutSessionRequired]
					, [BackChannelLogoutUri]
					, [ClientClaimsPrefix]
					, [ClientId]
					, [ClientName]
					, [ClientUri]
					, [ConsentLifetime]
					, [Description]
					, [EnableLocalLogin]
					, [Enabled]
					, [FrontChannelLogoutSessionRequired]
					, [FrontChannelLogoutUri]
					, [IdentityTokenLifetime]
					, [IncludeJwtId]
					, [LogoUri]
					, [PairWiseSubjectSalt]
					, [ProtocolType]
					, [RefreshTokenExpiration]
					, [RefreshTokenUsage]
					, [RequireClientSecret]
					, [RequireConsent]
					, [RequirePkce]
					, [SlidingRefreshTokenLifetime]
					, [UpdateAccessTokenClaimsOnRefresh]
					, [Created]
					, [DeviceCodeLifetime]
					, [LastAccessed]
					, [NonEditable]
					, [Updated]
					, [UserCodeType]
					, [UserSsoLifetime]) 						 
				VALUES(
					[Source].[AbsoluteRefreshTokenLifetime]
					, [Source].[AccessTokenLifetime]
					, [Source].[AccessTokenType]
					, [Source].[AllowAccessTokensViaBrowser]
					, [Source].[AllowOfflineAccess]
					, [Source].[AllowPlainTextPkce]
					, [Source].[AllowRememberConsent]
					, [Source].[AlwaysIncludeUserClaimsInIdToken]
					, [Source].[AlwaysSendClientClaims]
					, [Source].[AuthorizationCodeLifetime]
					, [Source].[BackChannelLogoutSessionRequired]
					, [Source].[BackChannelLogoutUri]
					, [Source].[ClientClaimsPrefix]
					, [Source].[ClientId]
					, [Source].[ClientName]
					, [Source].[ClientUri]
					, [Source].[ConsentLifetime]
					, [Source].[Description]
					, [Source].[EnableLocalLogin]
					, [Source].[Enabled]
					, [Source].[FrontChannelLogoutSessionRequired]
					, [Source].[FrontChannelLogoutUri]
					, [Source].[IdentityTokenLifetime]
					, [Source].[IncludeJwtId]
					, [Source].[LogoUri]
					, [Source].[PairWiseSubjectSalt]
					, [Source].[ProtocolType]
					, [Source].[RefreshTokenExpiration]
					, [Source].[RefreshTokenUsage]
					, [Source].[RequireClientSecret]
					, [Source].[RequireConsent]
					, [Source].[RequirePkce]
					, [Source].[SlidingRefreshTokenLifetime]
					, [Source].[UpdateAccessTokenClaimsOnRefresh]
					, [Source].[Created]
					, [Source].[DeviceCodeLifetime]
					, [Source].[LastAccessed]
					, [Source].[NonEditable]
					, [Source].[Updated]
					, [Source].[UserCodeType]
					, [Source].[UserSsoLifetime]
				);

	IF(@UpdateInsertClientTableOnly = 0)
	BEGIN 
		-- get the primary key for the client
		SET @clientId = (SELECT ID FROM dbo.Clients c WHERE c.ClientId = @clientCode);
    
		-- add api scope claim
		IF(@apiResource IS NOT NULL)
		BEGIN
			INSERT [dbo].[ClientScopes] ([ClientId], [Scope])
			SELECT @clientId, @apiResource
			WHERE NOT EXISTS (SELECT * FROM [dbo].[ClientScopes] WHERE ClientId = @clientId AND Scope = @apiResource)
		END
		-- put in more scopes here e.g. openid
		IF(@clientSecret IS NOT NULL)
		BEGIN
			-- add secret
			DECLARE @clientHASHBYTES VARBINARY(128) = hashbytes('sha2_256', @clientSecret)
			DECLARE @clientSecretHash VARCHAR(128) = (SELECT cast(N'' as xml).value('xs:base64Binary(sql:variable("@clientHASHBYTES"))', 'varchar(128)'));
       

			INSERT INTO [dbo].[ClientSecrets]
				([ClientId]
				,[Description]
				,[Expiration]
				,[Type]
				,[Value]
				,[Created])
			SELECT
				@clientId
				,@clientCode + ' secret'
				,NULL
				,'SharedSecret'
				,@clientSecretHash
				,GETDATE()
			FROM dbo.Clients c 
			WHERE c.Id = @clientId and NOT EXISTS(SELECT * FROM [dbo].[ClientSecrets] css 
														INNER JOIN dbo.Clients cs ON cs.Id = css.ClientId
														WHERE cs.Id = @clientId AND css.Value = @clientSecretHash)
  
		END
    
		-- insert any other scopes is required
		IF(@clientScopes is not null)
		BEGIN
			INSERT [dbo].[ClientScopes] ([ClientId], [Scope])
			SELECT @clientId, Value as Unit FROM STRING_SPLIT(@clientScopes , ',') 
			WHERE NOT EXISTS (SELECT * FROM [dbo].[ClientScopes] WHERE ClientId = @clientId AND Scope = Value)
		END 

		INSERT [dbo].[ClientClaims] ([ClientId], [Type],[Value])
		SELECT @clientId, ClaimType, ClaimValue FROM @clientClaims cc
		WHERE NOT EXISTS (SELECT * FROM [dbo].[ClientClaims] WHERE ClientId = @clientId AND [Type] = cc.ClaimType and [Value] = cc.ClaimValue)
				
		IF(@isApi = 0)
		BEGIN
    
			-- implicit means that users are redirected to the login page
			INSERT [dbo].[ClientGrantTypes] ([ClientId], [GrantType]) VALUES (@clientId, N'implicit')
			-- put in more scopes here e.g. openid  
    
			INSERT [dbo].[ClientScopes] ([ClientId], [Scope])
			SELECT @clientId, 'openid'
			WHERE NOT EXISTS (SELECT * FROM [dbo].[ClientScopes] WHERE ClientId = @clientId AND Scope = 'openid')

			-- allowed redirects after a successful logout
			IF(@devUrl is not null)
			BEGIN
				INSERT [dbo].[ClientPostLogoutRedirectUris] ([ClientId], [PostLogoutRedirectUri]) VALUES (@clientId,@devUrl + @postLogoutRelativeRedirectPath)
			END
			-- your site login page
        
			IF(@devUrl is not null)
			BEGIN
				INSERT [dbo].[ClientRedirectUris] ([ClientId], [RedirectUri]) VALUES (@clientId,@devUrl + @successfulLoginRelativeRedirectPath)           
				INSERT [dbo].[ClientRedirectUris] ([ClientId], [RedirectUri]) VALUES (@clientId,@devUrl + @silentRefreshRelativePath)
			END

			DECLARE  @BaseUrls TABLE ( Url NVARCHAR(500));
			
			INSERT @BaseUrls (Url)
			SELECT  Value as Unit FROM STRING_SPLIT(@baseServer , ',')  

			-- redirect after login page with token
			INSERT [dbo].[ClientRedirectUris] ([ClientId], [RedirectUri]) 
					SELECT @clientId, url + @successfulLoginRelativeRedirectPath	from @BaseUrls url
			UNION	SELECT @clientId, url + @silentRefreshRelativePath				from @BaseUrls url
			
			INSERT [dbo].[ClientPostLogoutRedirectUris] ([ClientId], [PostLogoutRedirectUri]) 
				SELECT @clientId, url + @postLogoutRelativeRedirectPath			from @BaseUrls url 
    
		END
		

		IF(@allowClientCredentials = 1)
		BEGIN

			-- client credentials means that a username/password as the clientId/secret are sent for the token
			INSERT [dbo].[ClientGrantTypes] ([ClientId], [GrantType]) VALUES (@clientId, N'client_credentials')
        
		END

		IF(@apiResource IS NOT NULL)
		BEGIN
			INSERT [dbo].[ApiResources] ([Description], [DisplayName], [Enabled], [Name], [Created], [LastAccessed], [NonEditable], [Updated]) 
			SELECT TOP 1 @apiResource + ' Resource', @apiResource, 1, @apiResource, GETDATE(), NULL, 0, NULL
			FROM [dbo].[ApiResources] api WHERE  NOT EXISTS (SELECT * FROM dbo.ApiResources apis WHERE apis.Name = @apiResource)
			-- add mandatory scope with same name as resource
			INSERT INTO [dbo].[ApiScopes]
						([ApiResourceId]
						,[Description]
						,[DisplayName]
						,[Emphasize]
						,[Name]
						,[Required]
						,[ShowInDiscoveryDocument])
			SELECT TOP 1 api.Id, @apiResource + ' Resource Scope', @apiResource, 0, @apiResource, 0, @apiScopeShowInDiscoveryDocument
			FROM [dbo].[ApiResources] api WHERE api.Name = @apiResource AND NOT EXISTS (SELECT * FROM dbo.[ApiScopes] apisc
															INNER JOIN dbo.ApiResources apis ON apisc.ApiResourceId = apis.id WHERE apisc.Name = @apiResource AND apis.Name = @apiResource)
	
			DECLARE @apiResourceId INT = (SELECT TOP 1 ID FROM [dbo].[ApiResources] WHERE Name = @apiResource)

			INSERT [dbo].[ApiClaims] ([ApiResourceId], [Type])
			SELECT @apiResourceId, Value as Unit FROM STRING_SPLIT(@apiClaims , ',')  
			WHERE NOT EXISTS (SELECT * FROM [dbo].[ApiClaims] ac WHERE ac.[ApiResourceId] = @apiResourceId AND ac.Type = Value)

			-- add secret
			DECLARE @APIHASHBYTES VARBINARY(128) = hashbytes('sha2_256', @apiSecret)
			DECLARE @APISecretHash VARCHAR(128) = (SELECT cast(N'' as xml).value('xs:base64Binary(sql:variable("@APIHASHBYTES"))', 'varchar(128)'));
			INSERT INTO [dbo].[ApiSecrets]
						([ApiResourceId]
						,[Description]
						,[Expiration]
						,[Type]
						,[Value]
						,[Created])
			SELECT
						api.Id
						,@apiResource + ' secret'
						,NULL
						,'SharedSecret'
						,@APISecretHash
						,GETDATE()
					FROM dbo.ApiResources api 
					WHERE api.Name = @apiResource and NOT EXISTS(SELECT * FROM [dbo].[ApiSecrets] aps 
																INNER JOIN dbo.ApiResources apis ON apis.Id = aps.ApiResourceId
																WHERE apis.Name = @apiResource AND aps.ApiResourceId = api.Id)
   
			-- use reference tokens for this client and NOT JWT
			UPDATE c
				SET c.AccessTokenType = 1
			FROM Clients c
			WHERE c.Id = @clientId
			-- add api resource scope to client
			INSERT INTO dbo.ClientScopes (ClientId, Scope)
			SELECT TOP 1 c.id, @apiResource
			FROM  [dbo].[ClientScopes] cs
			INNER JOIN dbo.Clients c on cs.ClientId = c.id
			WHERE c.Id = @clientId and NOT EXISTS(SELECT * FROM [dbo].[ClientScopes] css 
															INNER JOIN dbo.Clients cc on css.ClientId = cc.id
															WHERE cc.Id = @clientId AND css.Scope = @apiResource)
		END
	END
END