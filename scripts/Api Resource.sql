
DECLARE @apiResource VARCHAR(128) = 'remoteuser-webhub'; -- this is the name of the api
DECLARE @apiSecret VARCHAR(128) = 'remoteuser-webhub' -- this is the secret to validate reference tokens
DECLARE @apiScopeShowInDiscoveryDocument BIT = 0; -- show or not show in discovery document (extra security) (0 don't show, 1 show)
DECLARE @clientId VARCHAR(128) = 'id-onboard-fendi'; -- the name of an existent clientId (user name)
DECLARE @view BIT = 1; -- view current data for this configurations


IF @view = 1
BEGIN

	SELECT CASE WHEN c.AccessTokenType = 0 THEN 'JWT' WHEN c.AccessTokenType = 1 THEN 'Reference' ELSE 'Unknown' END, c.ClientId as ClientId
	FROM dbo.Clients c 
	WHERE c.ClientId = @clientId
	ORDER BY 2

	SELECT cs.Scope as ClientScope, c.ClientId as ClientId
	FROM dbo.ClientScopes cs
	INNER JOIN dbo.Clients c ON c.id = cs.ClientId
	WHERE cs.Scope in (@apiResource)
	ORDER BY 2,1

	SELECT c.Name as ApiResourceName
	FROM dbo.ApiResources c 
	WHERE c.Name in (@apiResource)
	ORDER BY 1

	SELECT cs.Name as ApiScope, c.Name as ApiResourceName
	FROM dbo.ApiScopes cs
	INNER JOIN dbo.ApiResources c ON c.id = cs.ApiResourceId
	WHERE cs.Name in (@apiResource)
	ORDER BY 2,1

END
ELSE
BEGIN

	INSERT [dbo].[ApiResources] ([Description], [DisplayName], [Enabled], [Name], [Created], [LastAccessed], [NonEditable], [Updated]) 
	SELECT TOP 1 @apiResource + ' Resource', @apiResource, 1, @apiResource, GETDATE(), NULL, 0, NULL
	FROM [dbo].[ApiResources] api WHERE  NOT EXISTS (SELECT * FROM dbo.ApiResources apis WHERE apis.Name = @apiResource)

	-- add mandatory scope with same name as resource
	INSERT INTO [dbo].[ApiScopes]
			   ([ApiResourceId]
			   ,[Description]
			   ,[DisplayName]
			   ,[Emphasize]
			   ,[Name]
			   ,[Required]
			   ,[ShowInDiscoveryDocument])
	SELECT TOP 1 api.Id, @apiResource + ' Resource Scope', @apiResource, 0, @apiResource, 0, @apiScopeShowInDiscoveryDocument
	FROM [dbo].[ApiResources] api WHERE api.Name = @apiResource AND  NOT EXISTS (SELECT * FROM dbo.[ApiScopes] apisc
													INNER JOIN dbo.ApiResources apis ON apisc.ApiResourceId = apis.id
														 WHERE apis.Name = @apiResource AND apisc.Name = @apiResource)

	-- add secret
	DECLARE @HASHBYTES VARBINARY(128) = hashbytes('sha2_256', @apiSecret)
	DECLARE @Secret VARCHAR(128) = (SELECT cast(N'' as xml).value('xs:base64Binary(sql:variable("@HASHBYTES"))', 'varchar(128)'));
	INSERT INTO [dbo].[ApiSecrets]
			   ([ApiResourceId]
			   ,[Description]
			   ,[Expiration]
			   ,[Type]
			   ,[Value]
			   ,[Created])
	SELECT
			   api.Id
			   ,@apiResource + ' secret'
			   ,null
			   ,'SharedSecret'
			   ,@Secret
			   ,GETDATE()
			FROM dbo.ApiResources api 
			where api.Name = @apiResource and NOT EXISTS(SELECT * FROM [dbo].[ApiSecrets] aps 
														INNER JOIN dbo.ApiResources apis ON apis.Id = aps.ApiResourceId
														WHERE apis.Name = @apiResource AND aps.ApiResourceId = api.Id)

   
	-- use reference tokens for this client and not JWT
	UPDATE c
		SET c.AccessTokenType = 1
	FROM Clients c
	WHERE c.ClientId = @clientId

	-- add api resource scope to client
	INSERT INTO dbo.ClientScopes (ClientId, Scope)
	SELECT TOP 1 c.id, @apiResource
	FROM  [dbo].[ClientScopes] cs
	inner join dbo.Clients c on cs.ClientId = c.id
	where c.ClientId = @clientId and NOT EXISTS(SELECT * FROM [dbo].[ClientScopes] css 
													inner join dbo.Clients cc on css.ClientId = cc.id
													WHERE cc.ClientId = @clientId AND css.Scope = @apiResource)

END												