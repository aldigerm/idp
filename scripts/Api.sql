

DECLARE @apiResource VARCHAR(200) = 'remote-user-repository';
DECLARE @apiScopeShowInDiscoveryDocument BIT = 0;
DECLARE @apiSecret VARCHAR(200) = null;
DECLARE @apiResourceId INT = null;
DECLARE @apiClaims VARCHAR(MAX) = 'remote-repository-project,remote-repository-tenant';

IF(@apiResource IS NOT NULL)
BEGIN
	INSERT [dbo].[ApiResources] ([Description], [DisplayName], [Enabled], [Name], [Created], [LastAccessed], [NonEditable], [Updated]) 
	SELECT TOP 1 @apiResource + ' Resource', @apiResource, 1, @apiResource, GETDATE(), NULL, 0, NULL
	FROM [dbo].[ApiResources] api WHERE  NOT EXISTS (SELECT * FROM dbo.ApiResources apis WHERE apis.Name = @apiResource)

	SET @apiResourceId = (SELECT TOP 1 ID FROM [dbo].[ApiResources] api WHERE api.Name = @apiResource)

	-- add mandatory scope with same name as resource
	INSERT INTO [dbo].[ApiScopes]
				([ApiResourceId]
				,[Description]
				,[DisplayName]
				,[Emphasize]
				,[Name]
				,[Required]
				,[ShowInDiscoveryDocument])
	SELECT TOP 1 api.Id, @apiResource + ' Resource Scope', @apiResource, 0, @apiResource, 0, @apiScopeShowInDiscoveryDocument
	FROM [dbo].[ApiResources] api WHERE api.Name = @apiResource AND NOT EXISTS (SELECT * FROM dbo.[ApiScopes] apisc
													INNER JOIN dbo.ApiResources apis ON apisc.ApiResourceId = apis.id
															WHERE apis.Name = @apiResource AND apisc.Name = @apiResource)
																	 


	INSERT [dbo].[ApiClaims] ([ApiResourceId], [Type])
	SELECT @apiResourceId, Value as Unit FROM STRING_SPLIT(@apiClaims , ',')  
	WHERE NOT EXISTS (SELECT * FROM [dbo].[ApiClaims] ac WHERE ac.[ApiResourceId] = @apiResourceId AND ac.Type = Value)

	IF(@apiSecret is not null)
	BEGIN

		-- add secret
		DECLARE @APIHASHBYTES VARBINARY(128) = hashbytes('sha2_256', @apiSecret)
		DECLARE @APISecretHash VARCHAR(128) = (SELECT cast(N'' as xml).value('xs:base64Binary(sql:variable("@APIHASHBYTES"))', 'varchar(128)'));
		INSERT INTO [dbo].[ApiSecrets]
					([ApiResourceId]
					,[Description]
					,[Expiration]
					,[Type]
					,[Value]
					,[Created])
		SELECT
					api.Id
					,@apiResource + ' secret'
					,NULL
					,'SharedSecret'
					,@APISecretHash
					,GETDATE()
				FROM dbo.ApiResources api 
				WHERE api.Name = @apiResource and NOT EXISTS(SELECT * FROM [dbo].[ApiSecrets] aps 
															INNER JOIN dbo.ApiResources apis ON apis.Id = aps.ApiResourceId
															WHERE apis.Name = @apiResource AND aps.ApiResourceId = api.Id)
	END
END