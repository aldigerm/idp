
DECLARE @tenantCode NVARCHAR(50) = 'TENANT_CODE'

-- delete all orphaned modules !!!!
DELETE m FROM [dbo].[Module] m
left JOIN dbo.RoleModule rm on rm.ModuleId = m.Id
WHERE rm.id is null

DELETE ugi 
FROM [dbo].[UserGroupInheritance] ugi
INNER JOIN [dbo].UserGroup ug on ugi.UserGroupId = ug.Id
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode

DELETE ugc 
FROM [dbo].[UserGroupClaim] ugc
INNER JOIN [dbo].[UserGroup] ug on ugc.UserGroupId = ug.Id
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode

DELETE tc 
FROM [dbo].[TenantClaim] tc
INNER JOIN [dbo].Tenant t on tc.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE ugr 
FROM [dbo].[UserGroupRole] ugr
INNER JOIN [dbo].[UserGroup] ug on ugr.UserGroupId = ug.Id
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE rm
FROM [dbo].[RoleModule] rm
INNER JOIN [dbo].[Role] r on rm.RoleId = r.id
INNER JOIN [dbo].Tenant t on r.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE ri
FROM [dbo].[RoleInheritance] ri
INNER JOIN [dbo].[Role] r on ri.RoleId = r.id
INNER JOIN [dbo].Tenant t on r.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE rc 
FROM [dbo].[RoleClaim] rc
INNER JOIN [dbo].[Role] r on rc.RoleId = r.id
INNER JOIN [dbo].Tenant t on r.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE r 
FROM [dbo].[Role] r
INNER JOIN [dbo].Tenant t on r.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE utugm
FROM [dbo].[UserToUserGroupManagement] utugm
INNER JOIN [dbo].[UserGroup] ug on utugm.UserGroupId = ug.Id
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE utug
FROM [dbo].[UserToUserGroup] utug
INNER JOIN [dbo].[UserGroup] ug on utug.UserGroupId = ug.Id
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE ug
FROM [dbo].[UserGroup] ug
INNER JOIN [dbo].Tenant t on ug.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE uc
FROM [dbo].[UserClaim] uc
INNER JOIN [dbo].[UserTenant] ut on uc.UserTenantId = ut.Id
INNER JOIN [dbo].Tenant t on ut.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE ut
FROM [dbo].[UserTenant] ut
INNER JOIN [dbo].Tenant t on ut.TenantId = t.Id
WHERE t.Code = @tenantCode


DELETE t
FROM [dbo].[Tenant] t
WHERE t.Code = @tenantCode
