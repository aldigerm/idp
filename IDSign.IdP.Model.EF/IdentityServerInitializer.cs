﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IDSign.IdP.Model;
using Microsoft.Extensions.Logging;

namespace IDSign.IdP.Model.EF
{
    public class IdentityServerInitializer
    {
        public static void InitializeDatabase(ConfigurationDbContext configurationDbContext
            , PersistedGrantDbContext persistedGrantDbContext
            , InternalModelContext internalModelContext
            , ILogger<IdentityServerInitializer> logger)
        {
            try
            {
                configurationDbContext.Database.Migrate();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                logger.LogError($"configurationDbContext migration threw an error -> {e.Message}");
            }
            try
            {
                persistedGrantDbContext.Database.Migrate();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                logger.LogError($"persistedGrantDbContext migration threw an error -> {e.Message}");
            }

            try
            {
                internalModelContext.Database.Migrate();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                logger.LogError($"internalModelContext migration threw an error -> {e.Message}");
            }

            foreach (var client in Config.GetClients())
            {
                if (!configurationDbContext.Clients.Any(c => c.ClientId == client.ClientId))
                {
                    logger.LogInformation($"Adding client -> {client.ClientId}");
                    configurationDbContext.Clients.Add(client.ToEntity());
                    configurationDbContext.SaveChanges();
                }
            }
            foreach (var resource in Config.GetIdentityResources())
            {
                if (!configurationDbContext.IdentityResources.Any(c => c.Name == resource.Name))
                {
                    logger.LogInformation($"Adding identity resource -> {resource.Name}");
                    configurationDbContext.IdentityResources.Add(resource.ToEntity());
                    configurationDbContext.SaveChanges();
                }
            }
            foreach (var resource in Config.GetApiResources())
            {
                if (!configurationDbContext.ApiResources.Any(r => r.Name == resource.Name))
                {
                    logger.LogInformation($"Adding api resource -> {resource.Name}");
                    configurationDbContext.ApiResources.Add(resource.ToEntity());
                    configurationDbContext.SaveChanges();
                }
            }

        }
    }
}
