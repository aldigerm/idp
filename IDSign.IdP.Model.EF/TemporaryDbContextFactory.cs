﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IDSign.IdP.Model.EF
{
    public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<InternalModelContext>
    { 
        public InternalModelContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<InternalModelContext>();
            builder.UseSqlServer("Server=localhost;Database=IDSign.IdP.RemoteRepository;User ID=sa_idsign_idp_users;Password=sa_idsign_idp_users;MultipleActiveResultSets=true",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(ModelContext).GetTypeInfo().Assembly.GetName().Name));
            return new InternalModelContext(builder.Options);
        }
    }
}
