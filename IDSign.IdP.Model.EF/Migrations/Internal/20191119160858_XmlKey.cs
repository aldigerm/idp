﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.Model.EF.Migrations.Internal
{
    public partial class XmlKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "XmlKey",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Xml = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_XmlKey", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "XmlKey");
        }
    }
}
