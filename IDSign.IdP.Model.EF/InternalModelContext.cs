﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Options;
using IDSign.IdP.Model.EF.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;

namespace IDSign.IdP.Model.EF
{
    public class InternalModelContext : DbContext
    {
        public InternalModelContext(
            DbContextOptions<InternalModelContext> nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public DbSet<XmlKey> XmlKeys { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var entConfigType = typeof(IEntityConfig);

            // Init types
            Assembly.GetAssembly(entConfigType)
                    .GetTypes()
                    .Where(type => entConfigType.IsAssignableFrom(type) && type != entConfigType)
                    .ToList()
                    .ForEach(type => ((IEntityConfig)Activator.CreateInstance(type)).MapEntity(modelBuilder));

        }
    }
}
