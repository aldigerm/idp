﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.EF.Mapping
{
    public interface IEntityConfig
    {
        void MapEntity(ModelBuilder builder);
    }
}
