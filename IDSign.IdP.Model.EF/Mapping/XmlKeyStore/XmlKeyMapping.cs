﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.Model.EF.Mapping
{
    public class XmlKeyMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<XmlKey>());
        }
        private void Map(EntityTypeBuilder<XmlKey> builder)
        {
            builder.ToTable("XmlKey");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).IsRequired();
            builder.Property(x => x.Xml).IsRequired();
        }
    }
}
