﻿using IDSign.IdP.Core;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class PasswordPolicyService : IPasswordPolicyServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IPasswordPolicyDataProvider _PasswordPolicyDataProvider;

        public PasswordPolicyService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IPasswordPolicyDataProvider passwordPolicyDataProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _PasswordPolicyDataProvider = passwordPolicyDataProvider;
        }


        public async Task<PasswordPolicyIdentifier> CreateAsync(PasswordPolicyCreateModel passwordPolicy)
        {
            var result = await _PasswordPolicyDataProvider.GetSimpleAsync(passwordPolicy.GetPasswordPolicyIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_EXISTS, passwordPolicy.GetPasswordPolicyIdentifier().ToString());
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(passwordPolicy.Regex))
                {
                    var dataModel = new PasswordPolicyRegexModel()
                    {
                        Regex = passwordPolicy.Regex
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                else if (passwordPolicy.Length.HasValue)
                {
                    var dataModel = new PasswordPolicyLengthModel()
                    {
                        Length = passwordPolicy.Length.Value
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                else if (passwordPolicy.HistoryLimit.HasValue)
                {
                    var dataModel = new PasswordPolicyHistoryModel()
                    {
                        Limit = passwordPolicy.HistoryLimit.Value
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                return await _PasswordPolicyDataProvider.CreateAsync(passwordPolicy);
            }
        }

        public async Task<PasswordPolicyIdentifier> UpdateAsync(PasswordPolicyUpdateModel passwordPolicy)
        {
            var result = await _PasswordPolicyDataProvider.GetSimpleAsync(passwordPolicy.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND, passwordPolicy.ToString());
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(passwordPolicy.Regex))
                {
                    var dataModel = new PasswordPolicyRegexModel()
                    {
                        Regex = passwordPolicy.Regex
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                else if (passwordPolicy.Length.HasValue)
                {
                    var dataModel = new PasswordPolicyLengthModel()
                    {
                        Length = passwordPolicy.Length.Value
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                else if (passwordPolicy.HistoryLimit.HasValue)
                {
                    var dataModel = new PasswordPolicyHistoryModel()
                    {
                        Limit = passwordPolicy.HistoryLimit.Value
                    };
                    passwordPolicy.Data = HelpersService.DescribeObject(dataModel);
                }
                return await _PasswordPolicyDataProvider.UpdateAsync(passwordPolicy);
            }
        }

        public async Task<bool> DeleteAsync(PasswordPolicyIdentifier passwordPolicy)
        {
            var result = await _PasswordPolicyDataProvider.DeleteAsync(passwordPolicy);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND);
            }
            // get updated user
            return true;
        }

        public async Task<IList<PasswordPolicyModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var passwordPolicies = await _PasswordPolicyDataProvider.GetListAsync(tenantIdentifier);
            IList<PasswordPolicyModel> list = new List<PasswordPolicyModel>();
            foreach (var entity in passwordPolicies)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<PasswordPolicyModel> GetViewModelAsync(PasswordPolicyIdentifier passwordPolicyIdentifier)
        {
            var result = await _PasswordPolicyDataProvider.GetAsync(passwordPolicyIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND);
            }
            return GetViewModel(result);
        }


        public PasswordPolicyModel GetViewModel(PasswordPolicy entity)
        {
            var model = new PasswordPolicyModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.PasswordPolicyTypeIdentifier = entity.PasswordPolicyType.GetIdentifier();
            model.Description = entity.Description;
            model.DefaultErrorMessage = entity.DefaultErrorMessage;
            model.Data = entity.Data;
            model.Regex = entity.GetRegex();
            model.Length = entity.GetLength();
            model.HistoryLimit = entity.GetHistoryLimit();
            model.UserGroups = new List<UserGroupIdentifier>();

            if (entity.PasswordPolicyUserGroup != null && entity.PasswordPolicyUserGroup.Any())
            {
                foreach (var ug in entity.PasswordPolicyUserGroup.Select(uggp => uggp.UserGroup))
                {
                    model.UserGroups.Add(ug.GetIdentifier());
                }
            }

            return model;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var list = await _PasswordPolicyDataProvider.GetListAsync(tenantIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }

        public async Task<IList<PasswordPolicyModel>> GetAccessibleListAsync()
        {
            if (_HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return await GetListAsync(null);
            }

            if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN))
            {
                return await GetListAsync(new TenantIdentifier(null, _HttpContextAccessor.ProjectCode()));
            }
            else if (_HttpContextAccessor.HasAnyAllowedModules(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_USER_MANAGEMENT))
            {
                return await GetListAsync(new TenantIdentifier(_HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode()));
            }
            else
            {
                return new List<PasswordPolicyModel>();
            }
        }
    }
}
