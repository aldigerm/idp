﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IPasswordPolicyUserGroupServiceProvider
    {
        Task<PasswordPolicyUserGroupModel> GetAsync(PasswordPolicyUserGroupIdentifier passwordPolicyUserGroupIdentifier);
        Task<int> DeleteUserGroupAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier);
        Task<PasswordPolicyUserGroupIdentifier> AddUserGroupAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier);
        Task<PasswordPolicyUserGroupIdentifier> UpdateAsync(PasswordPolicyUserGroupModel model);
        PasswordPolicyUserGroupModel GetViewModel(PasswordPolicyUserGroup passwordPolicyUserGroup);
    }
}
