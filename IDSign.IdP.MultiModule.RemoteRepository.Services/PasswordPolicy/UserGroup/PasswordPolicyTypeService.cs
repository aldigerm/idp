﻿using IDSign.IdP.Core;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class PasswordPolicyUserGroupService : IPasswordPolicyUserGroupServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IPasswordPolicyUserGroupDataProvider _PasswordPolicyUserGroupDataProvider;

        public PasswordPolicyUserGroupService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IPasswordPolicyUserGroupDataProvider passwordPolicyUserGroupDataProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _PasswordPolicyUserGroupDataProvider = passwordPolicyUserGroupDataProvider;
        }

        public async Task<PasswordPolicyUserGroupModel> GetAsync(PasswordPolicyUserGroupIdentifier passwordPolicyUserGroupIdentifier)
        {
            var passwordPolicyUserGroup = await _PasswordPolicyUserGroupDataProvider.GetAsync(passwordPolicyUserGroupIdentifier);
            if (passwordPolicyUserGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_USERGROUP_NOT_FOUND);
            }
            return GetViewModel(passwordPolicyUserGroup);
        }

        public async Task<List<PasswordPolicyUserGroupModel>> GetListAsync(UserGroupIdentifier passwordPolicyUserGroupIdentifier)
        {
            var passwordPolicyUserGroups = await _PasswordPolicyUserGroupDataProvider.GetListAsync(passwordPolicyUserGroupIdentifier);
            var list = new List<PasswordPolicyUserGroupModel>();
            if (passwordPolicyUserGroups != null)
            {
                foreach (var ppug in passwordPolicyUserGroups)
                {
                    var model = GetViewModel(ppug);
                    if (model != null)
                    {
                        list.Add(model);
                    }
                }
            }
            return list;
        }

        public async Task<int> DeleteUserGroupAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier)
        {
            return await _PasswordPolicyUserGroupDataProvider.DeleteAsync(passwordPolicyIdentifier, userGroupIdentifier);
        }

        public async Task<PasswordPolicyUserGroupIdentifier> AddUserGroupAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier)
        {
            return await _PasswordPolicyUserGroupDataProvider.AddAsync(passwordPolicyIdentifier, userGroupIdentifier);
        }

        public async Task<PasswordPolicyUserGroupIdentifier> UpdateAsync(PasswordPolicyUserGroupModel passwordPolicyUserGroupModel)
        {
            return await _PasswordPolicyUserGroupDataProvider.UpdateAsync(passwordPolicyUserGroupModel);
        }

        public PasswordPolicyUserGroupModel GetViewModel(PasswordPolicyUserGroup passwordPolicyUserGroup)
        {
            PasswordPolicyUserGroupModel viewModel = new PasswordPolicyUserGroupModel();
            if (passwordPolicyUserGroup != null)
            {
                viewModel.Identifier = passwordPolicyUserGroup.GetIdentifier();
            }
            return viewModel;
        }


    }
}
