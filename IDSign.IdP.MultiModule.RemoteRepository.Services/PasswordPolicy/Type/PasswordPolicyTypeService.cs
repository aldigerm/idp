﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class PasswordPolicyTypeService : IPasswordPolicyTypeServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IPasswordPolicyTypeDataProvider _PasswordPolicyTypeDataProvider;

        public PasswordPolicyTypeService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IPasswordPolicyTypeDataProvider passwordPolicyTypeDataProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _PasswordPolicyTypeDataProvider = passwordPolicyTypeDataProvider;
        }


        public async Task<PasswordPolicyTypeIdentifier> CreateAsync(PasswordPolicyTypeCreateModel passwordPolicy)
        {
            var result = await _PasswordPolicyTypeDataProvider.GetSimpleAsync(passwordPolicy.GetIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_EXISTS, passwordPolicy.GetIdentifier().ToString());
            }
            else
            {
                return await _PasswordPolicyTypeDataProvider.CreateAsync(passwordPolicy);
            }
        }

        public async Task<PasswordPolicyTypeIdentifier> UpdateAsync(PasswordPolicyTypeUpdateModel passwordPolicy)
        {
            var result = await _PasswordPolicyTypeDataProvider.GetSimpleAsync(passwordPolicy.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND, passwordPolicy.ToString());
            }
            else
            {
                return await _PasswordPolicyTypeDataProvider.UpdateAsync(passwordPolicy);
            }
        }

        public async Task<bool> DeleteAsync(PasswordPolicyTypeIdentifier passwordPolicy)
        {
            var result = await _PasswordPolicyTypeDataProvider.DeleteAsync(passwordPolicy);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND);
            }
            // get updated user
            return true;
        }

        public async Task<IList<PasswordPolicyTypeBasicModel>> GetAccessibleListAsync()
        {
            IList<PasswordPolicyTypeBasicModel> list = new List<PasswordPolicyTypeBasicModel>();
            var all = await GetListAsync();
            foreach(var type in all)
            {
                list.Add(type);
            }
            return list;
        }

        public async Task<IList<PasswordPolicyTypeModel>> GetListAsync()
        {
            var passwordPolicies = await _PasswordPolicyTypeDataProvider.GetListAsync();
            IList<PasswordPolicyTypeModel> list = new List<PasswordPolicyTypeModel>();
            foreach (var entity in passwordPolicies)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<PasswordPolicyTypeModel> GetViewModelAsync(PasswordPolicyTypeIdentifier passwordPolicyIdentifier)
        {
            var result = await _PasswordPolicyTypeDataProvider.GetAsync(passwordPolicyIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND);
            }
            return GetViewModel(result);
        }


        private PasswordPolicyTypeModel GetViewModel(PasswordPolicyType entity)
        {
            var model = new PasswordPolicyTypeModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.Description = entity.Description;

            if (entity.PasswordPolicies != null && entity.PasswordPolicies.Any())
            {
                model.PasswordPolicies = new List<PasswordPolicyIdentifier>();
                foreach (var pp in entity.PasswordPolicies)
                {
                    model.PasswordPolicies.Add(pp.GetIdentifier());
                }
            }

            return model;
        }

        public async Task DeleteAllAsync()
        {
            var list = await _PasswordPolicyTypeDataProvider.GetListAsync();
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
