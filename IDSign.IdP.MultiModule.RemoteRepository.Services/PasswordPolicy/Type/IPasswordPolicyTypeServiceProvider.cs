﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IPasswordPolicyTypeServiceProvider
    {
        Task<PasswordPolicyTypeModel> GetViewModelAsync(PasswordPolicyTypeIdentifier passwordPolicyIdentifier);
        Task<PasswordPolicyTypeIdentifier> CreateAsync(PasswordPolicyTypeCreateModel passwordPolicy);
        Task<PasswordPolicyTypeIdentifier> UpdateAsync(PasswordPolicyTypeUpdateModel passwordPolicy);
        Task<bool> DeleteAsync(PasswordPolicyTypeIdentifier passwordPolicy);
        Task<IList<PasswordPolicyTypeModel>> GetListAsync();
        Task DeleteAllAsync();
        Task<IList<PasswordPolicyTypeBasicModel>> GetAccessibleListAsync();
    }
}
