﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IPermissionsServiceProvider
    {
        Task InitPermissionsAsync();
        Task InitPermissionsSupportAsync();
        Task InitPermissionsSupportAsync(ProjectIdentifier projectIdentifier);
        Task<IList<Module>> GetInferredModulesAsync(List<Role> roles);

        IRoleServiceProvider RoleServiceProvider { get; }
        IModuleServiceProvider ModuleServiceProvider { get; }
        IUserGroupServiceProvider UserGroupServiceProvider { get; }

        Task InitPermissionsSupportAsync(TenantIdentifier tenantIdentifier);
    }
}
