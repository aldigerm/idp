﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class ModuleService : IModuleServiceProvider
    {
        protected readonly ILogger<ModuleService> _logger;
        protected readonly IModuleDataProvider _ModuleDataProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor ;
        public ModuleService(
            IModuleDataProvider moduleDataProvider
            , ILogger<ModuleService> logger
            , IHttpContextAccessor httpContextAccessor 
            )
        {
            _HttpContextAccessor = httpContextAccessor  ;
            _ModuleDataProvider = moduleDataProvider;
            _logger = logger;
        }

        public async Task InitSupportAsync(ProjectIdentifier projectIdentifier = null)
        {
            projectIdentifier = projectIdentifier ?? SupportConstants.GetSupportProjectIdentifier();
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Superuser", SupportConstants.SupportModuleCode, projectIdentifier));

            // repository                                                                                                                                                                        
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Repository Profile Tenant Administrator", ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Repository Profile Project Administrator", ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Repository Profile Any Administrator", ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN, projectIdentifier));

        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            // modules
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Parent", ModuleCode.PARENT, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dashboard management", ModuleCode.DASHBOARD_MANAGEMENT, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier management", ModuleCode.DOSSIER_MANAGEMENT, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dashboard view", ModuleCode.DASHBOARD_VIEW, projectIdentifier));

            // country                                                                                                                                                                                                                  
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Country View", ModuleCode.COUNTRY_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Country File Download", ModuleCode.COUNTRY_FILE_DOWNLOAD, projectIdentifier));

            // detail                                                                                                                                                                                                                   
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Create", ModuleCode.DOSSIER_DETAIL_CREATE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Create", ModuleCode.DOSSIER_DETAIL_UPDATE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail EDIT", ModuleCode.DOSSIER_DETAIL_EDIT, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossiers Personal View", ModuleCode.DOSSIER_DETAIL_VIEW_PERSONAL, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier List View All dossiers", ModuleCode.DOSSIER_VIEW_ALL, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier List View", ModuleCode.DOSSIER_LIST_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail View", ModuleCode.DOSSIER_DETAIL_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail History View", ModuleCode.DOSSIER_DETAIL_HISTORY_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Data View", ModuleCode.DOSSIER_DETAIL_DATA_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Collect Docs Basic", ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_BASIC, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Collect Docs Extended", ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_EXTENDED, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Action on Collect Docs", ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_ACTION, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Reject Reason View", ModuleCode.DOSSIER_DETAIL_REJECT_REASON_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Internal Steps View", ModuleCode.DOSSIER_DETAIL_INTERNAL_STEPS_VIEW, projectIdentifier));

            // actions                                                                                                                                                                                                                  
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Send Back to Partner", ModuleCode.DOSSIER_DETAIL_SEND_BACK_TO_PARTNER, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Detail Send Back to Risk", ModuleCode.DOSSIER_DETAIL_SEND_TO_RISK, projectIdentifier));

            // psp                                                                                                                                                                                                                      
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier PSP View", ModuleCode.DOSSIER_DETAIL_PSP_VIEW, projectIdentifier));

            // risk                                                                                                                                                                                                                     
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Risk View", ModuleCode.DOSSIER_DETAIL_RISK_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Risk Verify", ModuleCode.DOSSIER_DETAIL_RISK_VERIFY, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Risk Create Merchant Ids", ModuleCode.DOSSIER_DETAIL_RISK_CREATE_MERCHANT_IDS, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Risk File Upload", ModuleCode.DOSSIER_DETAIL_RISK_UPLOAD_FILE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Risk Permanent Reject", ModuleCode.DOSSIER_DETAIL_RISK_PERMANENT_REJECT, projectIdentifier));

            // post complete                                                                                                                                                                                                            
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Post Complete View", ModuleCode.DOSSIER_DETAIL_POST_COMPLETE_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Dossier Post Complete Action", ModuleCode.DOSSIER_DETAIL_POST_COMPLETE_ACTION, projectIdentifier));

            // user                                                                                                                                                                                                                     
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Basic User", ModuleCode.USER, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Internal User", ModuleCode.USER_INTERNAL, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Partner User", ModuleCode.PARTNER, projectIdentifier));

            // task                                                                                                                                                                                                                     
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task Personal Create", ModuleCode.TASK_PERSONAL_CREATE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task Personal View", ModuleCode.TASK_PERSONAL_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task Personal Close", ModuleCode.TASK_PERSONAL_CLOSE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task All Create", ModuleCode.TASK_ALL_CREATE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task All View", ModuleCode.TASK_ALL_VIEW, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task All Close", ModuleCode.TASK_ALL_CLOSE, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Task All Reassign", ModuleCode.TASK_ALL_REASSIGN, projectIdentifier));

            // reporting                                                                                                                                                                                                                     
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Reporting", ModuleCode.REPORTING, projectIdentifier));
            await CreateOrUpdateAsync(GetModuleUpdateViewModel("Configuration", ModuleCode.CONFIGURATION, projectIdentifier));
        }

        private ModuleUpdateViewModel GetModuleUpdateViewModel(string description, ModuleCode code, ProjectIdentifier projectIdentifier)
        {
            return GetModuleUpdateViewModel(description, code.ToString(), projectIdentifier);
        }

        private ModuleUpdateViewModel GetModuleUpdateViewModel(string description, string code, ProjectIdentifier projectIdentifier)
        {
            return new ModuleUpdateViewModel()
            {
                Description = description,
                Identifier = new ModuleIdentifier(code, projectIdentifier)
            };
        }

        public async Task<Module> GetAsync(ModuleIdentifier moduleIdentifier)
        {
            return await _ModuleDataProvider.GetAsync(moduleIdentifier);
        }

        public async Task<int> DeleteAsync(ModuleIdentifier moduleIdentifier)
        {
            return await _ModuleDataProvider.DeleteAsync(moduleIdentifier);
        }

        public async Task<IList<Module>> GetListAsync()
        {
            return await _ModuleDataProvider.GetAllModulesAsync();
        }

        public async Task<IList<ModuleViewModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var modules = await _ModuleDataProvider.GetListAsync(projectIdentifier);
            return GetListViewModel(modules);
        }

        private IList<ModuleViewModel> GetListViewModel(IList<Module> modules)
        {
            List<ModuleViewModel> list = new List<ModuleViewModel>();
            foreach (var module in modules)
            {
                var model = GetViewModel(module);
                if (model != null)
                    list.Add(model);
            }
            list = list.OrderBy(m => m.Identifier.ModuleCode).ToList();
            return list;
        }

        private ModuleViewModel GetViewModel(Module module)
        {
            if (module == null)
                return null;

            var model = new ModuleViewModel();
            model.Description = module.Description;
            model.Identifier = new ModuleIdentifier(module.Code, module.Project.Code);
            return model;
        }

        public async Task<IList<ModuleViewModel>> GetLoggedInModuleAsync()
        {
            return await GetListAsync(new ProjectIdentifier(_HttpContextAccessor.ProjectCode()));
        }

        public async Task<ModuleViewModel> GetModuleViewModelAsync(ModuleIdentifier moduleIdentifier)
        {
            var module = await _ModuleDataProvider.GetAsync(moduleIdentifier);
            if (module == null)
            {
                throw new IdPNotFoundException(ErrorCode.MODULE_NOT_FOUND, moduleIdentifier.ToString());
            }
            return GetViewModel(module);
        }

        public async Task<ModuleIdentifier> CreateAsync(ModuleCreateModel model)
        {
            var module = await _ModuleDataProvider.CreateAsync(model);
            return model.GetModuleIdentifier();
        }

        public async Task<ModuleIdentifier> UpdateAsync(ModuleUpdateViewModel model)
        {
            return await _ModuleDataProvider.UpdateAsync(model);
        }

        public async Task<ModuleIdentifier> CreateOrUpdateAsync(ModuleUpdateViewModel model)
        {
            var moduleIdentifier = await _ModuleDataProvider.CreateOrUpdateAsync(model);
            if (moduleIdentifier == null)
            {
                throw new IdPException();
            }
            return moduleIdentifier;
        }

        private IList<Module> GetDistinct(IList<Module> modules)
        {
            modules = modules.GroupBy(elem => elem.Code).Select(group => group.First()).ToList();
            return modules;
        }

        public async Task<IList<Module>> GetModulesForRoles(IList<Role> allRoles)
        {
            if (allRoles.Any(r => r.Code == SupportConstants.SupportRoleCode))
            {
                return await GetListAsync();
            }
            else if (allRoles.Any(r => r.Code == SupportConstants.TenantAdminRoleCode))
            {
                var list = await GetListAsync();
                list = list.Where(m => m.Code != SupportConstants.SupportModuleCode).ToList();
                return list;
            }
            else
            {
                return allRoles.SelectMany(r => r.RoleModules).Select(rm => rm.Module).ToList();
            }
        }

        public async Task<IList<Module>> GetModulesForRoleAsync(Role role)
        {
            return await GetModulesForRolesAsync(new List<Role>() { role });
        }

        public async Task<IList<Module>> GetModulesForRolesAsync(IList<Role> allRoles)
        {
            var modules = await GetModulesForRoles(allRoles);

            if (modules == null || !modules.Any())
                return new List<Module>();

            return GetDistinct(modules);
        }

        public async Task<IList<ModuleViewModel>> GetAccessibleListAsync(ProjectIdentifier projectIdentifier)
        {
            if (_HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return GetListViewModel(await GetListAsync());
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN) && _HttpContextAccessor.LoggedInUserIdentifier().GetProjectIdentifier().IsEquals(projectIdentifier))
            {
                return await GetListAsync(projectIdentifier);
            }
            else
            {
                return new List<ModuleViewModel>();
            }
        }
    }
}
