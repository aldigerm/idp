﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IModuleServiceProvider
    {
        Task<Module> GetAsync(ModuleIdentifier moduleIdentifier);
        Task<IList<ModuleViewModel>> GetLoggedInModuleAsync();
        Task<ModuleViewModel> GetModuleViewModelAsync(ModuleIdentifier moduleIdentifier);
        Task<IList<ModuleViewModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<ModuleIdentifier> CreateAsync(ModuleCreateModel model);
        Task<ModuleIdentifier> CreateOrUpdateAsync(ModuleUpdateViewModel model);
        Task<ModuleIdentifier> UpdateAsync(ModuleUpdateViewModel model);
        Task<int> DeleteAsync(ModuleIdentifier moduleIdentifier);
        Task<IList<Module>> GetListAsync();
        Task InitAsync(ProjectIdentifier projectIdentifier);
        Task InitSupportAsync(ProjectIdentifier projectIdentifier = null);
        Task<IList<Module>> GetModulesForRoles(IList<Role> allRoles);
        Task<IList<Module>> GetModulesForRoleAsync(Role role);
        Task<IList<Module>> GetModulesForRolesAsync(IList<Role> allRoles);
        Task<IList<ModuleViewModel>> GetAccessibleListAsync(ProjectIdentifier projectIdentifier);
    }
}
