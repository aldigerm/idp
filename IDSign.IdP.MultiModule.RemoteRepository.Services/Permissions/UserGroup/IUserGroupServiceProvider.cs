﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserGroupServiceProvider
    {
        Task<IList<UserGroupViewModel>> GetLoggedInUserGroupsAsync();
        Task<UserGroupViewModel> GetViewModelAsync(UserGroupIdentifier userGroupIdentifier);
        Task<List<UserGroupViewModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<UserGroupViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier);
        Task<UserGroupIdentifier> CreateAsync(UserGroupCreateModel model);
        Task<UserGroupViewModel> CreateOrUpdateAsync(UserGroupUpdateViewModel model);
        Task<UserGroupIdentifier> UpdateAsync(UserGroupUpdateViewModel model);
        Task<int> DeleteAsync(UserGroupIdentifier userGroupIdentifier);
        Task<UserGroupViewModel> SetInheritingUserGroups(UserGroupIdentifier userGroupIdentifier, IList<string> inheritesFromUserGroups);
        Task<UserGroupViewModel> SetUsers(UserGroupIdentifier userGroupIdentifier, IList<string> usernames, IList<string> accessibleUsernames);
        Task<AppUser> RemoveUserFromGroup(UserIdentifier userIdentifier, string code);
        Task<AppUser> AddUserToGroup(UserIdentifier userIdentifier, string userGroupCode);
        Task<List<UserGroup>> GetForClaimsAsync(List<string> claims, TenantIdentifier tenantIdentifier);
        Task<UserGroup> GetAsync(int id);
        Task InitAsync(TenantIdentifier tenantIdentifier);
        Task<List<UserGroup>> GetInferredUserGroupsAsync(AppUser user, TenantIdentifier tenantIdentifier);
        Task<List<UserGroup>> GetUserGroupsRecursivelyAsync(List<UserGroup> userGroups);
        Task InitSupportAsync(TenantIdentifier tenantIdentifier = null);
        Task<List<UserGroup>> GetUserGroupsRecursivelyAsync(UserGroup userGroup);
        Task<List<UserGroup>> GetUserGroupsIncludedAsync(UserGroup userGroup);
        Task<List<UserGroup>> GetUserGroupsIncludedAsync(List<UserGroup> includedUserGroups);
        Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier, params string[] userGroupsNames);
        Task<bool> ExistsAsync(UserGroupIdentifier userGroupIdentifier);
        Task<AppUser> AddUserToGroups(UserIdentifier user, params string[] userGroupCodes);
        Task<AppUser> RemoveUserFromGroups(UserIdentifier user, params string[] userGroupCodes);
        Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier);
        UserGroupViewModel GetViewModel(UserGroup userGroup);
        Task<UserGroup> GetAsync(UserGroupIdentifier userGroupIdentifier);
        Task<List<UserGroup>> GetAccessibleGroupsIncluded(UserIdentifier userIdentifier);
        Task<UserGroupViewModel> SetPasswordPolicies(UserGroupIdentifier identifier, IList<string> passwordPolicies);
    }
}
