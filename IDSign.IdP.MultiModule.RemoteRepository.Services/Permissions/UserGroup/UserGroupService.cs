﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserGroupService : IUserGroupServiceProvider
    {
        protected readonly ILogger<UserGroupService> _logger;
        protected readonly IUserGroupDataProvider _UserGroupDataProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserGroupManagementServiceProvider _UserGroupManagementServiceProvider;
        protected readonly IPasswordPolicyUserGroupServiceProvider _PasswordPolicyUserGroupServiceProvider;

        public UserGroupService(
            IUserGroupDataProvider userGroupDataProvider
            , ILogger<UserGroupService> logger
            , IHttpContextAccessor httpContextAccessor
            , IUserGroupManagementServiceProvider userGroupManagementServiceProvider
            , IPasswordPolicyUserGroupServiceProvider passwordPolicyUserGroupServiceProvider
            )
        {
            _UserGroupDataProvider = userGroupDataProvider;
            _HttpContextAccessor = httpContextAccessor;
            _logger = logger;
            _UserGroupManagementServiceProvider = userGroupManagementServiceProvider;
            _PasswordPolicyUserGroupServiceProvider = passwordPolicyUserGroupServiceProvider;
        }

        public async Task InitSupportAsync(TenantIdentifier tenantIdentifier = null)
        {
            tenantIdentifier = tenantIdentifier ?? SupportConstants.GetSupportTenantIdentifier();

            // super users / developers
            UserGroupViewModel supportGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Support Group", SupportConstants.SupportUserGroupCode, null, tenantIdentifier));

            // admins
            UserGroupViewModel tenantAdminGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Tenant Admin", SupportConstants.TenantAdminUserGroupCode, null, tenantIdentifier));
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {

            _logger.LogInformation("Creating groups and roles for {0} ", tenantIdentifier);

            // every user
            UserGroupViewModel dossierUserGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Dossier User", InitialGroupCodes.User, null, tenantIdentifier));

            // partners
            UserGroupViewModel partner1Group = await CreateOrUpdateAsync(GetUserGroupViewModel("Partner " + InitialGroupCodes.Partner1GroupCode, InitialGroupCodes.Partner1GroupCode, null, tenantIdentifier));

            // partners
            UserGroupViewModel partner2Group = await CreateOrUpdateAsync(GetUserGroupViewModel("Partner " + InitialGroupCodes.Partner2GroupCode, InitialGroupCodes.Partner2GroupCode, null, tenantIdentifier));

            // internal users (team members)
            UserGroupViewModel dossierInternalUserGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Dossier Internal User", InitialGroupCodes.InternalUser, new List<UserGroupViewModel>() { dossierUserGroup }, tenantIdentifier));

            // risk users
            UserGroupViewModel tenantRiskGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Tenant Risk", InitialGroupCodes.TenantRisk, new List<UserGroupViewModel>() { dossierInternalUserGroup }, tenantIdentifier));
            // risk supervisors
            UserGroupViewModel riskSupervisorGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Tenant Risk Supervisor", InitialGroupCodes.TenantRiskSupervisor, new List<UserGroupViewModel>() { tenantRiskGroup }, tenantIdentifier));

            // psp users
            UserGroupViewModel tenanatPSPGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Tenant PSP", InitialGroupCodes.TenantPSP, new List<UserGroupViewModel>() { dossierInternalUserGroup }, tenantIdentifier));
            // psp supervisors
            UserGroupViewModel pspSupervisorGroup = await CreateOrUpdateAsync(GetUserGroupViewModel("Tenant PSP Supervisors", InitialGroupCodes.TenantPSPSupervisors, new List<UserGroupViewModel>() { tenanatPSPGroup }, tenantIdentifier));


            _logger.LogInformation("Initializing of permission finished.");
        }

        private UserGroupUpdateViewModel GetUserGroupViewModel(string description, string code, List<UserGroupViewModel> inheritsFrom, TenantIdentifier tenantIdentifier)
        {
            return new UserGroupUpdateViewModel()
            {
                Identifier = new UserGroupIdentifier(code, tenantIdentifier),
                Description = description,
                InheritsFromUserGroups = inheritsFrom?.Select(ug => ug.Identifier.UserGroupCode).ToList()
            };
        }

        public async Task<int> DeleteAsync(UserGroupIdentifier userGroupIdentifier)
        {
            return await _UserGroupDataProvider.DeleteAsync(userGroupIdentifier);
        }

        public async Task<List<UserGroup>> GetListAsync()
        {
            return await _UserGroupDataProvider.GetListAsync();
        }

        public async Task<List<UserGroupViewModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var userGroups = await _UserGroupDataProvider.GetListAsync(tenantIdentifier);
            return await GetListViewModelAsync(userGroups);
        }

        public async Task<List<UserGroupViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier)
        {
            if (_HttpContextAccessor.IsAdmin() || _HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return await GetListViewModelAsync(await GetListAsync());
            }

            List<UserGroupViewModel> list = new List<UserGroupViewModel>();
            if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN) && _HttpContextAccessor.LoggedInUserIdentifier().GetProjectIdentifier().IsEquals(tenantIdentifier.GetProjectIdentifier()))
            {
                list = await GetListAsync(new ProjectIdentifier(_HttpContextAccessor.ProjectCode()));
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN)
                && (tenantIdentifier == null || tenantIdentifier.IsUndefined() || _HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier().IsEquals(tenantIdentifier)))
            {
                list = await GetListAsync(_HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier());
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_USER_MANAGEMENT))
            {
                var managedUsers = await _UserGroupManagementServiceProvider.GetListAsync(_HttpContextAccessor.LoggedInUserIdentifier());
                var includedUserGroups = await GetUserGroupsIncludedAsync(managedUsers.Select(ug => ug.UserGroup).ToList());
                foreach (var ug in includedUserGroups)
                {
                    list.Add(GetViewModel(ug));
                }
            }
            else
            {
                return new List<UserGroupViewModel>();
            }

            list.RemoveAll(ug => ug.Identifier.UserGroupCode == SupportConstants.GetSupportUserGroupIdentifier().UserGroupCode);

            return list;
        }

        private async Task<List<UserGroupViewModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var userGroups = await _UserGroupDataProvider.GetListAsync(projectIdentifier);
            return await GetListViewModelAsync(userGroups);
        }

        private List<UserGroupViewModel> GetListViewModel(List<UserGroup> userGroups)
        {
            var list = new List<UserGroupViewModel>();
            foreach (var ug in userGroups)
            {
                list.Add(GetViewModel(ug));
            }
            return list;
        }

        private async Task<List<UserGroupViewModel>> GetListViewModelAsync(List<UserGroup> userGroups)
        {
            List<UserGroupViewModel> list = new List<UserGroupViewModel>();
            foreach (var id in userGroups.Select(ug => ug.Id))
            {
                var userGroup = await GetAsync(id);
                var model = GetViewModel(userGroup);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<List<UserGroup>> GetAccessibleGroupsIncluded(UserIdentifier userIdentifier)
        {
            var accessibleList = await GetAccessibleListAsync(userIdentifier.GetTenantIdentifier());
            var list = new List<UserGroup>();
            foreach (var accessibleUg in accessibleList)
            {
                var ug = await GetAsync(accessibleUg.Identifier);
                if (ug != null)
                {
                    list.Add(ug);
                }
            }
            var includedUserGroups = await GetUserGroupsIncludedAsync(list);
            return includedUserGroups?.ToList() ?? new List<UserGroup>();
        }

        public async Task<List<string>> GetFilteredListForLoggedInUser(IList<string> userGroupCodes)
        {
            var recursively = await GetAccessibleGroupsIncluded(_HttpContextAccessor.LoggedInUserIdentifier());
            return userGroupCodes.Common(recursively.Select(ug => ug.Code), ug => ug).ToList();
        }
        public UserGroupViewModel GetViewModel(UserGroup userGroup)
        {
            if (userGroup == null)
                return null;

            var model = new UserGroupViewModel();
            model.Description = userGroup.Description;
            model.Identifier = userGroup.GetIdentifier();
            model.InheritsFromUserGroups = userGroup.InheritingFromTheseUserGroups?.Select(inh => inh.InheritsFrom.Code).ToList();
            model.Usernames = userGroup.ApplicationUserGroups?.Select(aug => aug.UserTenant.User.TenantUsername).ToList();

            model.PasswordPolicies = new List<string>();
            if (userGroup.PasswordPolicyUserGroup != null && userGroup.PasswordPolicyUserGroup.Any())
            {
                foreach (var ppug in userGroup.PasswordPolicyUserGroup)
                {
                    var ppugModel = _PasswordPolicyUserGroupServiceProvider.GetViewModel(ppug);
                    if (ppugModel != null)
                    {
                        model.PasswordPolicies.Add(ppugModel.Identifier.PasswordPolicyCode);
                    }
                }
            }
            return model;
        }

        public async Task<IList<UserGroupViewModel>> GetLoggedInModuleAsync()
        {
            return await GetListAsync(new TenantIdentifier(_HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode()));
        }

        public async Task<UserGroupViewModel> GetViewModelAsync(UserGroupIdentifier userGroupIdentifier)
        {
            var userGroup = await _UserGroupDataProvider.GetAsync(userGroupIdentifier);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifier.ToString());
            }
            return GetViewModel(userGroup);
        }

        public async Task<UserGroupIdentifier> CreateAsync(UserGroupCreateModel model)
        {
            var userGroup = await _UserGroupDataProvider.CreateAsync(model);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR);
            }

            await _UserGroupDataProvider.SetInheritingUserGroups(model.GetUserGroupIdentifier(), model.InheritsFromUserGroups);

            return model.GetUserGroupIdentifier();
        }

        public async Task<UserGroupIdentifier> UpdateAsync(UserGroupUpdateViewModel model)
        {
            var identifier = await _UserGroupDataProvider.UpdateAsync(model);
            if (identifier == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR);
            }

            // we only update if there is an empty or populated list
            // a null list does NOT get processed
            if (model.InheritsFromUserGroups != null)
            {
                await _UserGroupDataProvider.SetInheritingUserGroups(identifier, model.InheritsFromUserGroups);
            }
            return identifier;
        }

        public async Task<UserGroupViewModel> CreateOrUpdateAsync(UserGroupUpdateViewModel model)
        {
            var userGroup = await _UserGroupDataProvider.CreateOrUpdateAsync(model);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR);
            }

            await _UserGroupDataProvider.SetInheritingUserGroups(model.Identifier, model.InheritsFromUserGroups);

            return await GetViewModelAsync(model.Identifier);
        }

        public async Task<UserGroupViewModel> SetInheritingUserGroups(UserGroupIdentifier userGroupIdentifier, IList<string> inheritesFromUserGroups)
        {
            var userGroup = await _UserGroupDataProvider.SetInheritingUserGroups(userGroupIdentifier, inheritesFromUserGroups);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR);
            }
            return await GetViewModelAsync(userGroupIdentifier);
        }

        public async Task<UserGroupViewModel> SetPasswordPolicies(UserGroupIdentifier userGroupIdentifier, IList<string> passwordPolicies)
        {
            var userGroup = await GetViewModelAsync(userGroupIdentifier);
            var current = userGroup.PasswordPolicies;
            var newList = passwordPolicies;

            var toRemove = current.Except(passwordPolicies);
            var toAdd = passwordPolicies.Except(current);


            foreach (var passwordPolicy in toRemove)
            {
                await _PasswordPolicyUserGroupServiceProvider.DeleteUserGroupAsync(new PasswordPolicyIdentifier(passwordPolicy, userGroupIdentifier.GetTenantIdentifier()), userGroupIdentifier);
            }

            foreach (var passwordPolicy in toAdd)
            {
                await _PasswordPolicyUserGroupServiceProvider.AddUserGroupAsync(new PasswordPolicyIdentifier(passwordPolicy, userGroupIdentifier.GetTenantIdentifier()), userGroupIdentifier);
            }

            return await GetViewModelAsync(userGroupIdentifier);
        }


        public async Task<UserGroupViewModel> SetUsers(UserGroupIdentifier userGroupIdentifier, IList<string> usernames, IList<string> accessibleUsernames)
        {
            var userGroup = await GetViewModelAsync(userGroupIdentifier);
            var current = userGroup.Usernames;
            var newList = usernames;

            var toRemove = current.Except(usernames).Common(accessibleUsernames, u => u);
            var toAdd = usernames.Except(current).Common(accessibleUsernames, u => u);


            foreach (var username in toRemove)
            {
                await _UserGroupDataProvider.RemoveUserFromGroup(new UserIdentifier(username, userGroupIdentifier.GetTenantIdentifier()), userGroupIdentifier.UserGroupCode);
            }

            foreach (var username in toAdd)
            {
                await _UserGroupDataProvider.AddUserToGroup(new UserIdentifier(username, userGroupIdentifier.GetTenantIdentifier()), userGroupIdentifier.UserGroupCode);
            }

            return await GetViewModelAsync(userGroupIdentifier);
        }


        public async Task<IList<UserGroupViewModel>> GetLoggedInUserGroupsAsync()
        {
            return await this.GetListAsync(new TenantIdentifier(_HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode()));
        }

        public async Task<AppUser> RemoveUserFromGroup(UserIdentifier userIdentifier, string code)
        {
            return await _UserGroupDataProvider.RemoveUserFromGroup(userIdentifier, code);
        }

        public async Task<AppUser> AddUserToGroup(UserIdentifier userIdentifier, string userGroupCode)
        {
            return await _UserGroupDataProvider.AddUserToGroup(userIdentifier, userGroupCode);
        }

        public async Task<List<UserGroup>> GetForClaimsAsync(List<string> claims, TenantIdentifier tenantIdentifier)
        {
            return await _UserGroupDataProvider.GetForClaimsAsync(claims, tenantIdentifier);
        }

        public async Task<UserGroup> GetAsync(int id)
        {
            return await _UserGroupDataProvider.GetAsync(id);
        }

        public async Task<UserGroup> GetAsync(UserGroupIdentifier identifier)
        {
            return await _UserGroupDataProvider.GetAsync(identifier);
        }

        public async Task<List<UserGroup>> GetInferredUserGroupsAsync(AppUser user, TenantIdentifier tenantIdentifier)
        {
            _logger.LogDebug("Getting user group inheritance for :" + user.GetIdentifier(tenantIdentifier).ToString());
            List<UserGroup> tenantUserGroups = await _UserGroupDataProvider.GetListAsync(tenantIdentifier);
            var allUserGroups = await GetUserGroupsRecursivelyAsync(user.UserTenants.Where(ut => ut.Tenant.GetIdentifier().IsEquals(tenantIdentifier))
                .SelectMany(ut => ut.UserTenantUserGroups)
                .Where(aug => tenantUserGroups.Any(tug => tug.Id == aug.UserGroupId))
                .Select(aug => aug.UserGroup).ToList());
            return allUserGroups;
        }

        public async Task<List<UserGroup>> GetUserGroupsRecursivelyAsync(UserGroup userGroup)
        {
            return await GetUserGroupsRecursivelyAsync(new List<UserGroup>() { userGroup });
        }

        public async Task<List<UserGroup>> GetUserGroupsRecursivelyAsync(List<UserGroup> userGroups)
        {
            List<UserGroup> allUserGroups = new List<UserGroup>();
            foreach (var userGroup in userGroups)
            {
                _logger.LogDebug("Getting inheritance for usergroup :" + userGroup.Code);
                allUserGroups = await GetUserGroupsRecursivelyAsync(userGroup, allUserGroups);
            }
            _logger.LogDebug("Roles derived by inheritance are :" + string.Join(",", allUserGroups.Select(r => r?.Code)));
            return allUserGroups;
        }

        public async Task<List<UserGroup>> GetUserGroupsRecursivelyAsync(UserGroup userGroup, List<UserGroup> inferredUserGroups, string path = "")
        {
            if (inferredUserGroups == null)
            {
                inferredUserGroups = new List<UserGroup>();
            }
            if (userGroup != null)
            {
                path += ((path == "") ? "" : " -> ") + userGroup.Code;
                if (inferredUserGroups.Any(r => r.Id == userGroup.Id))
                {
                    _logger.LogWarning("UserGroup is part of a cyclic loop: " + userGroup.Code + ". Path: " + path);
                }
                else
                {
                    userGroup = await GetAsync(userGroup.Id);
                    inferredUserGroups.Add(userGroup);
                    if (userGroup.InheritingFromTheseUserGroups != null && userGroup.InheritingFromTheseUserGroups.Any())
                    {
                        foreach (var cUg in userGroup.InheritingFromTheseUserGroups.Select(r => r.InheritsFrom))
                        {
                            inferredUserGroups = await GetUserGroupsRecursivelyAsync(cUg, inferredUserGroups, path);
                        }
                    }
                }
            }
            return inferredUserGroups;
        }

        public async Task<List<UserGroup>> GetUserGroupsIncludedAsync(UserGroup userGroup)
        {
            return await GetUserGroupsIncludedAsync(new List<UserGroup>() { userGroup });
        }

        public async Task<List<UserGroup>> GetUserGroupsIncludedAsync(List<UserGroup> userGroups)
        {
            List<UserGroup> includedUserGroups = new List<UserGroup>();
            foreach (var userGroup in userGroups)
            {
                includedUserGroups = await GetUserGroupsIncludedAsync(userGroup, includedUserGroups);
            }
            return includedUserGroups;
        }

        public async Task<List<UserGroup>> GetUserGroupsIncludedAsync(UserGroup userGroup, List<UserGroup> includedUserGroups)
        {
            if (includedUserGroups == null)
            {
                includedUserGroups = new List<UserGroup>();
            }
            if (userGroup != null && !includedUserGroups.Any(ug => ug.Code == userGroup.Code))
            {
                userGroup = await GetAsync(userGroup.Id);
                includedUserGroups.Add(userGroup);
                if (userGroup.UserGroupsWhichInheritFromCurrent != null && userGroup.UserGroupsWhichInheritFromCurrent.Any())
                {
                    foreach (var cUg in userGroup.UserGroupsWhichInheritFromCurrent.Select(r => r.UserGroup))
                    {
                        includedUserGroups = await GetUserGroupsIncludedAsync(cUg, includedUserGroups);
                    }
                }
            }
            return includedUserGroups;
        }

        public async Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier, params string[] userGroupCodes)
        {
            foreach (var code in userGroupCodes.Where(ug => !String.IsNullOrWhiteSpace(ug)))
            {
                var exists = await ExistsAsync(new UserGroupIdentifier(code, tenantIdentifier));
                if (!exists)
                {
                    _logger.LogError("Group not found (code, tenantcode, projectcode) '{0}' '{1}' '{2}'", code, tenantIdentifier.TenantCode, tenantIdentifier.ProjectCode);
                    return false;
                }
            }
            return true;
        }

        public async Task<bool> ExistsAsync(UserGroupIdentifier userGroupIdentifier)
        {
            return await _UserGroupDataProvider.ExistsAsync(userGroupIdentifier);
        }

        public async Task<AppUser> AddUserToGroups(UserIdentifier userIdentifier, params string[] userGroupCodes)
        {
            AppUser user = null;
            foreach (var userGroupCode in userGroupCodes)
            {
                user = await AddUserToGroup(userIdentifier, userGroupCode);
            }
            return user;
        }


        public async Task<AppUser> RemoveUserFromGroups(UserIdentifier userIdentifier, params string[] userGroupCodes)
        {
            AppUser user = null;
            foreach (var code in userGroupCodes.Where(ug => !String.IsNullOrWhiteSpace(ug)))
            {
                user = await RemoveUserFromGroup(userIdentifier, code);
            }
            return user;
        }

        public async Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            return await _UserGroupDataProvider.DeleteAllAsync(tenantIdentifier);
        }
    }
}
