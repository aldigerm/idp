﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IRoleServiceProvider
    {
        Task InitAsync(TenantIdentifier tenantIdentifier);
        Task<Role> GetRoleAsync(int id);
        Task<Role> GetRoleAsync(RoleIdentifier roleIdentifier);
        Task<IList<RoleViewModel>> GetLoggedInRolesAsync();
        Task<RoleViewModel> GetViewModelAsync(RoleIdentifier roleIdentifier);
        Task<RoleViewModel> GetViewModelAsync(Role role, bool getRecursiveValues);
        Task<IList<RoleViewModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<RoleIdentifier> CreateAsync(RoleCreateModel model);
        Task<RoleIdentifier> UpdateAsync(RoleUpdateViewModel model);
        Task<int> DeleteAsync(RoleIdentifier roleIdentifier);
        Task<RoleViewModel> SetModules(RoleIdentifier roleIdentifier, IList<string> moduleCodes);
        Task<RoleViewModel> SetInheritingRoles(RoleIdentifier roleIdentifier, IList<string> roles);
        Task<RoleViewModel> SetUserGroups(RoleIdentifier roleIdentifier, IList<string> userGroups);
        Task<IList<Role>> GetRolesRecursivelyAsync(AppUser applicationUser, TenantIdentifier tenantIdentifier);
        Task<List<Role>> GetRolesAsync(List<string> roleCodes);
        Task<List<Role>> GetRolesRecursivelyAsync(Role role);
        Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task<IList<Role>> GetRolesRecursivelyAsync(List<Role> roles);
        Task InitSupportAsync(TenantIdentifier tenantIdentifier = null);
        Task<IList<RoleViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier);
        Task<IList<RoleViewModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<IList<RoleViewModel>> GetListAsync();
    }
}
