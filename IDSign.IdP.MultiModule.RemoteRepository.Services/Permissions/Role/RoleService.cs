﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class RoleService : IRoleServiceProvider
    {
        private readonly IRoleDataProvider _RoleDataProvider;
        private readonly IModuleServiceProvider _ModuleServiceProvider;
        private readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly ILogger<PermissionsService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;

        public RoleService(
            IRoleDataProvider roleDataProvider
            , IModuleServiceProvider moduleServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , ILogger<PermissionsService> logger
            , IHttpContextAccessor httpContextAccessor
            )
        {
            _UserGroupServiceProvider = userGroupServiceProvider;
            _ModuleServiceProvider = moduleServiceProvider;
            _RoleDataProvider = roleDataProvider;
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
        }

        public async Task InitSupportAsync(TenantIdentifier tenantIdentifier = null)
        {
            tenantIdentifier = tenantIdentifier ?? SupportConstants.GetSupportTenantIdentifier();

            // superusers
            Role superRoles = await CreateOrUpdateAsync(GetRoleUpdateViewModel(SupportConstants.SupportRoleCode, "Superusers", new List<string>() { SupportConstants.SupportUserGroupCode }, null, new List<string>() { SupportConstants.SupportModuleCode }, tenantIdentifier));

            // administrators
            Role tenantAdminRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(SupportConstants.TenantAdminRoleCode, "Administrators", new List<string>() { SupportConstants.TenantAdminUserGroupCode }, new List<string>() { }, new List<string>() { ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN.ToString() }, tenantIdentifier));

        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {
            _logger.LogInformation("Creating roles for companies .");

            _logger.LogInformation("Creating roles for {0} ", tenantIdentifier);

            #region Roles

            // all users
            Role dossierUserRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.User, "Any Role", new List<string>() { InitialGroupCodes.User }, null
                , new List<string>() { ModuleCode.DASHBOARD_VIEW.ToString().ToString()
                        , ModuleCode.DOSSIER_DETAIL_VIEW_PERSONAL.ToString().ToString()
                        , ModuleCode.DOSSIER_DETAIL_VIEW.ToString()
                        , ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_BASIC.ToString()
                        , ModuleCode.TASK_PERSONAL_CREATE.ToString()
                        , ModuleCode.TASK_PERSONAL_VIEW.ToString()
                        , ModuleCode.TASK_PERSONAL_CLOSE.ToString()
                        , ModuleCode.DOSSIER_DETAIL_CREATE.ToString()
                        , ModuleCode.USER.ToString()
                        , ModuleCode.DOSSIER_LIST_VIEW.ToString()
                        , ModuleCode.COUNTRY_FILE_DOWNLOAD.ToString()
                        , ModuleCode.COUNTRY_VIEW.ToString() }
                , tenantIdentifier));

            // internal users
            Role dossierInternalUserRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.InternalUser, "Internal Role", new List<string>() { InitialGroupCodes.InternalUser }, new List<string>() { dossierUserRole.Code }, new List<string>() { ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_EXTENDED.ToString(), ModuleCode.DOSSIER_DETAIL_REJECT_REASON_VIEW.ToString(), ModuleCode.DOSSIER_DETAIL_INTERNAL_STEPS_VIEW.ToString(), ModuleCode.DOSSIER_VIEW_ALL.ToString(), ModuleCode.USER_INTERNAL.ToString(), ModuleCode.DOSSIER_DETAIL_POST_COMPLETE_ACTION.ToString() }, tenantIdentifier));

            // partner users
            Role dossierPartnerUserRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.Partner, "Partner Roles", new List<string>() { InitialGroupCodes.Partner1GroupCode, InitialGroupCodes.Partner2GroupCode }, new List<string>() { dossierUserRole.Code }, new List<string>() { ModuleCode.PARTNER.ToString() }, tenantIdentifier));

            // psp users
            Role pspRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.Psp, "PSP Roles", new List<string>() { InitialGroupCodes.TenantPSP }, new List<string>() { dossierInternalUserRole.Code, dossierUserRole.Code }, new List<string>() { ModuleCode.DOSSIER_DETAIL_PSP_VIEW.ToString(), ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_ACTION.ToString(), ModuleCode.DOSSIER_DETAIL_SEND_BACK_TO_PARTNER.ToString(), ModuleCode.DOSSIER_DETAIL_SEND_TO_RISK.ToString() }, tenantIdentifier));

            // risk users
            Role riskRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.Risk, "Risk Roles", new List<string>() { InitialGroupCodes.TenantRisk }, new List<string>() { dossierInternalUserRole.Code, dossierUserRole.Code }, new List<string>() { ModuleCode.DOSSIER_DETAIL_RISK_VIEW.ToString(), ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_ACTION.ToString(), ModuleCode.DOSSIER_DETAIL_RISK_VERIFY.ToString(), ModuleCode.DOSSIER_DETAIL_RISK_CREATE_MERCHANT_IDS.ToString(), ModuleCode.DOSSIER_DETAIL_RISK_UPLOAD_FILE.ToString(), ModuleCode.DOSSIER_DETAIL_RISK_PERMANENT_REJECT.ToString(), ModuleCode.DOSSIER_DETAIL_SEND_BACK_TO_PARTNER.ToString() }, tenantIdentifier));

            // all supervisors
            Role supervisorRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.Supervisor, "Any Supervisor", new List<string>() { InitialGroupCodes.TenantPSPSupervisors, InitialGroupCodes.TenantRiskSupervisor }, new List<string>() { dossierInternalUserRole.Code }, new List<string>() { ModuleCode.DOSSIER_DETAIL_EDIT.ToString(), ModuleCode.DOSSIER_DETAIL_HISTORY_VIEW.ToString(), ModuleCode.DOSSIER_DETAIL_DATA_VIEW.ToString(), ModuleCode.TASK_ALL_CREATE.ToString(), ModuleCode.TASK_ALL_VIEW.ToString(), ModuleCode.TASK_ALL_CLOSE.ToString(), ModuleCode.TASK_ALL_REASSIGN.ToString(), ModuleCode.DOSSIER_DETAIL_UPDATE.ToString(), ModuleCode.REPORTING.ToString() }, tenantIdentifier));

            // psp supervisors
            Role pspSupervisorRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.PspSupervisor, "PSP Supervisors", new List<string>() { InitialGroupCodes.TenantPSPSupervisors }, new List<string>() { supervisorRole.Code, pspRole.Code }, null, tenantIdentifier));

            // risk supervisors
            Role riskSupervisorRole = await CreateOrUpdateAsync(GetRoleUpdateViewModel(RoleCode.RiskSupervisor, "Risk Supervisors", new List<string>() { InitialGroupCodes.TenantRiskSupervisor }, new List<string>() { supervisorRole.Code, riskRole.Code }, null, tenantIdentifier));

            #endregion

            _logger.LogInformation("Initializing of permission finished.");
        }

        private RoleUpdateViewModel GetRoleUpdateViewModel(string code, string description, List<string> groupCodes, List<string> inheritsFromRolesCodes, List<string> moduleCodes, TenantIdentifier tenantIdentifier)
        {
            return new RoleUpdateViewModel()
            {
                Description = description,
                Identifier = new RoleIdentifier(code, tenantIdentifier),
                InheritsFromRoles = inheritsFromRolesCodes,
                UserGroups = groupCodes,
                ModuleCodes = moduleCodes
            };
        }

        private async Task<Role> CreateOrUpdateAsync(RoleUpdateViewModel model)
        {
            var role = await _RoleDataProvider.CreateOrUpdateAsync(model);
            if (role != null)
            {
                await SetModules(model.Identifier, model.ModuleCodes);

                await SetInheritingRoles(model.Identifier, model.InheritsFromRoles);

                await SetUserGroups(model.Identifier, model.UserGroups);

                role = await _RoleDataProvider.GetAsync(model.Identifier);
            }
            return role;
        }

        public async Task<List<Role>> GetRolesAsync(List<string> roleCodes)
        {
            List<Role> roles = new List<Role>();
            foreach (var roleCode in roleCodes)
            {
                var role = await _RoleDataProvider.GetAsync(new RoleIdentifier(roleCode, _HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode()));
                if (role != null)
                {
                    roles.Add(role);
                }
            }
            return roles;
        }
        public async Task<List<Role>> GetRolesRecursivelyAsync(Role role)
        {
            return await GetRolesRecursivelyAsync(role, new List<Role>());
        }

        public async Task<List<Role>> GetRolesRecursivelyAsync(Role role, List<Role> inferredRoles, string path = "")
        {
            if (inferredRoles == null)
            {
                inferredRoles = new List<Role>();
            }
            if (role != null)
            {
                path += ((path == "") ? "" : " -> ") + role.Code;
                if (inferredRoles.Any(r => r.Id == role.Id))
                {
                    _logger.LogWarning("Role is part of a cyclic loop: " + role.Code + ". Path: " + path);
                }
                else
                {
                    role = await _RoleDataProvider.GetAsync(role.Id);
                    inferredRoles.Add(role);
                    if (role.InheritingFromTheseRoles != null && role.InheritingFromTheseRoles.Any())
                    {
                        foreach (var cRole in role.InheritingFromTheseRoles.Select(r => r.InheritsFrom))
                        {
                            inferredRoles = await GetRolesRecursivelyAsync(cRole, inferredRoles, path);
                        }
                    }
                }
            }
            return inferredRoles;
        }

        public async Task<Role> GetRoleAsync(int id)
        {
            return await _RoleDataProvider.GetAsync(id);
        }

        public async Task<Role> GetRoleAsync(RoleIdentifier roleIdentifier)
        {
            return await _RoleDataProvider.GetAsync(roleIdentifier);
        }

        public async Task<IList<RoleViewModel>> GetLoggedInRolesAsync()
        {
            var roles = await GetRolesAsync(_HttpContextAccessor.Roles());
            IList<RoleViewModel> models = new List<RoleViewModel>();
            foreach (var role in roles)
            {
                RoleViewModel model = await GetViewModelAsync(role, false);
                if (model != null)
                    models.Add(model);
            }
            return models;
        }

        public async Task<RoleViewModel> GetViewModelAsync(Role role, bool getRecursiveValues)
        {
            if (role == null)
                return null;

            var model = new RoleViewModel();
            model.Description = role.Description;
            model.InheritsFromRoles = role.InheritingFromTheseRoles?.Select(inh => inh.InheritsFrom.Code).ToList();
            model.ModuleCodes = role.RoleModules?.Select(rm => rm.Module.Code).ToList();
            model.Identifier = new RoleIdentifier(role.Code, role.Tenant.Code, role.Tenant.Project.Code);
            model.UserGroups = role.UserGroupsRoles?.Select(ugr => ugr.UserGroup.Code).ToList();

            var recursiveRoles = getRecursiveValues ? await GetRolesRecursivelyAsync(role) : null;
            var inheritedModules = getRecursiveValues ? await _ModuleServiceProvider.GetModulesForRolesAsync(recursiveRoles) : null;

            model.InheritedModuleCodes = model.ModuleCodes == null || inheritedModules == null ? null : inheritedModules?.Select(m => m.Code).Except(model.ModuleCodes).ToList();
            return model;
        }

        public async Task<IList<Role>> GetRolesRecursivelyAsync(AppUser applicationUser, TenantIdentifier tenantIdentifier)
        {
            _logger.LogDebug("Getting role inheritance for :" + applicationUser.GetIdentifier(tenantIdentifier).ToString());
            List<Role> allRoles = new List<Role>();
            var groups = await _UserGroupServiceProvider.GetInferredUserGroupsAsync(applicationUser, tenantIdentifier);
            var roles = groups.SelectMany(ugs => ugs.UserGroupRoles).Select(ugr => ugr.Role).ToList();
            return await GetRolesRecursivelyAsync(roles);
        }


        public async Task<IList<Role>> GetRolesRecursivelyAsync(List<Role> roles)
        {
            List<Role> allRoles = new List<Role>();
            foreach (var role in roles)
            {
                _logger.LogDebug("Getting inheritance for role :" + role.Code);

                // add user's roles and all parent roles recursively
                allRoles = await GetRolesRecursivelyAsync(role, allRoles);
            }
            _logger.LogDebug("Roles derived by inheritance are :" + string.Join(",", allRoles.Select(r => r?.Code)));
            return allRoles;
        }

        public async Task<RoleViewModel> GetViewModelAsync(RoleIdentifier roleIdentifier)
        {
            var role = await _RoleDataProvider.GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }
            return await GetViewModelAsync(role, true);
        }

        public async Task<IList<RoleViewModel>> GetListAsync()
        {
            IList<Role> roles = await _RoleDataProvider.GetListAsync();

            return await GetListViewModelAsync(roles);
        }

        public async Task<IList<RoleViewModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await GetListAsync(new TenantIdentifier("", projectIdentifier));
        }

        public async Task<IList<RoleViewModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<Role> roles = await _RoleDataProvider.GetListAsync(tenantIdentifier);

            return await GetListViewModelAsync(roles);
        }

        private async Task<IList<RoleViewModel>> GetListViewModelAsync(IList<Role> roles)
        {
            IList<RoleViewModel> models = new List<RoleViewModel>();
            foreach (var role in roles)
            {
                RoleViewModel model = await GetViewModelAsync(role, false);
                if (model != null)
                    models.Add(model);
            }
            return models;
        }

        public async Task<IList<RoleViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier)
        {
            if (_HttpContextAccessor.IsAdmin() || _HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return await GetListAsync();
            }

            IList<RoleViewModel> list = new List<RoleViewModel>();
            if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN)
                && (tenantIdentifier == null || tenantIdentifier.IsUndefined() || _HttpContextAccessor.LoggedInUserIdentifier().GetProjectIdentifier().IsEquals(tenantIdentifier.GetProjectIdentifier())))
            {
                list = await GetListAsync(tenantIdentifier.GetProjectIdentifier());
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN) && _HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier().IsEquals(tenantIdentifier))
            {
                list = await GetListAsync(_HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier());
            }
            else
            {
                return new List<RoleViewModel>();
            }

            var resultList = list.ToList();
            resultList.RemoveAll(r => r.Identifier.RoleCode == SupportConstants.GetSupportRoleIdentifier().RoleCode);
            return resultList;
        }

        public async Task<RoleIdentifier> CreateAsync(RoleCreateModel model)
        {
            var role = await _RoleDataProvider.CreateAsync(model);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR, model.ToString());
            }

            // set modules
            await SetModules(model.GetRoleIdentifier(), model.ModuleCodes);

            // set user groups
            await SetUserGroups(model.GetRoleIdentifier(), model.UserGroups);

            // set roles which this new role inherits from
            await SetInheritingRoles(model.GetRoleIdentifier(), model.InheritsFromRoles);

            return model.GetRoleIdentifier();
        }

        public async Task<RoleIdentifier> UpdateAsync(RoleUpdateViewModel model)
        {
            var identifier = await _RoleDataProvider.UpdateAsync(model);
            if (identifier == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, model.ToString());
            }

            // we only process lists which are not null. 
            // if one wants to remove all, then passa an instantiated empty list

            #region Modules, UserGroups and Roles

            if (model.ModuleCodes != null)
            {
                // set modules
                await SetModules(identifier, model.ModuleCodes);
            }

            if (model.UserGroups != null)
            {
                // set user groups
                await SetUserGroups(identifier, model.UserGroups);
            }

            if (model.InheritsFromRoles != null)
            {
                // set roles which this new role inherits from
                await SetInheritingRoles(identifier, model.InheritsFromRoles);
            }

            #endregion

            return identifier;
        }

        public async Task<int> DeleteAsync(RoleIdentifier roleIdentifier)
        {
            int deleted = await _RoleDataProvider.DeleteAsync(roleIdentifier);
            if (deleted == 0)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }
            return deleted;
        }

        public async Task<RoleViewModel> SetModules(RoleIdentifier roleIdentifier, IList<string> moduleCodes)
        {
            var role = await _RoleDataProvider.SetModules(roleIdentifier, moduleCodes);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR, roleIdentifier.ToString());
            }
            return await GetViewModelAsync(roleIdentifier);
        }

        public async Task<RoleViewModel> SetInheritingRoles(RoleIdentifier roleIdentifier, IList<string> roles)
        {
            var role = await _RoleDataProvider.SetRoleInheritances(roleIdentifier, roles);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR, roleIdentifier.ToString());
            }
            return await GetViewModelAsync(roleIdentifier);
        }

        public async Task<RoleViewModel> SetUserGroups(RoleIdentifier roleIdentifier, IList<string> userGroups)
        {
            var role = await _RoleDataProvider.SetUserGroups(roleIdentifier, userGroups);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.GENERIC_ERROR, roleIdentifier.ToString());
            }
            return await GetViewModelAsync(roleIdentifier);
        }

        public async Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            int deleted = await _RoleDataProvider.DeleteAllAsync(tenantIdentifier);
            return deleted;
        }
    }
}
