﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class PermissionsService : IPermissionsServiceProvider
    {
        private readonly ITenantServiceProvider _TenantServiceProvider;
        private readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly ILogger<PermissionsService> _logger;
        protected readonly IClaimsCollectionServiceProvider _ClaimsCollectionServiceProvider;


        public PermissionsService(
            IRoleServiceProvider permissionDataProvider
            , ITenantServiceProvider tenantDataProvider
            , IProjectServiceProvider projectServiceProvider
            , IModuleServiceProvider moduleServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , ILogger<PermissionsService> logger
            , IClaimsCollectionServiceProvider claimsCollectionServiceProvider
            )
        {
            UserGroupServiceProvider = userGroupServiceProvider;
            ModuleServiceProvider = moduleServiceProvider;
            _ProjectServiceProvider = projectServiceProvider;
            RoleServiceProvider = permissionDataProvider;
            _TenantServiceProvider = tenantDataProvider;
            _logger = logger;
            _ClaimsCollectionServiceProvider = claimsCollectionServiceProvider;
        }

        public IRoleServiceProvider RoleServiceProvider { get; }
        public IModuleServiceProvider ModuleServiceProvider { get; }
        public IUserGroupServiceProvider UserGroupServiceProvider { get; }

        public async Task InitPermissionsSupportAsync()
        {
            await ModuleServiceProvider.InitSupportAsync();
            await UserGroupServiceProvider.InitSupportAsync();
            await RoleServiceProvider.InitSupportAsync();
            foreach (var p in await _ProjectServiceProvider.GetListAsync())
            {
                await InitPermissionsSupportAsync(p.Identifier);
            }
        }

        public async Task InitPermissionsSupportAsync(ProjectIdentifier projectIdentifier)
        {
            var project = await _ProjectServiceProvider.GetAsync(projectIdentifier);
            await ModuleServiceProvider.InitSupportAsync(projectIdentifier);
            foreach (var tenantIdentifier in project.Tenants.Select(t => t.Identifier))
            {
                await InitPermissionsSupportAsync(tenantIdentifier);
            }
        }

        public async Task InitPermissionsSupportAsync(TenantIdentifier tenantIdentifier)
        {
            await UserGroupServiceProvider.InitSupportAsync(tenantIdentifier);
            await RoleServiceProvider.InitSupportAsync(tenantIdentifier);
        }

        public async Task InitPermissionsAsync()
        {
            _logger.LogInformation("Initialising permission.");

            var tenantIdentifier = Constants.GetInitialTenantIdentifier();

            foreach (var project in await _ProjectServiceProvider.GetListAsync())
            {
                _logger.LogInformation($"Creating support modules for project  {project.Identifier}.");
                await ModuleServiceProvider.InitSupportAsync(project.Identifier);
            }

            foreach (var tenant in await _TenantServiceProvider.GetListAsync())
            {
                _logger.LogInformation($"Creating permissions for tenant  {tenant.Identifier}.");
                await InitPermissionsSupportAsync(tenant.Identifier);
            }

            _logger.LogInformation($"Creating user groups for default project {tenantIdentifier}.");
            await UserGroupServiceProvider.InitAsync(tenantIdentifier);
            _logger.LogInformation($"Creating roles for default project {tenantIdentifier}.");
            await RoleServiceProvider.InitAsync(tenantIdentifier);
            _logger.LogInformation($"Creating claims for default project {tenantIdentifier}.");
            await _ClaimsCollectionServiceProvider.InitAsync(tenantIdentifier);

            _logger.LogInformation("Initializing of permission finished.");
        }

        public async Task<IList<Module>> GetInferredModulesAsync(List<Role> roles)
        {
            IList<Role> recursiveRoles = await RoleServiceProvider.GetRolesRecursivelyAsync(roles);
            var modules = await ModuleServiceProvider.GetModulesForRolesAsync(recursiveRoles);
            return modules;
        }

    }
}
