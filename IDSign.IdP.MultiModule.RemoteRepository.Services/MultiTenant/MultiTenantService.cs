﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class MultiTenantService : IMultiTenantServiceProvider
    {
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly IPermissionsServiceProvider _PermissionsServiceProvider;
        protected readonly IUserServiceProvider _UserServiceProvider;
        protected readonly IClaimsCollectionServiceProvider _ClaimsCollectionServiceProvider;
        public MultiTenantService
            (
            ITenantServiceProvider tenantServiceProvider
            , IProjectServiceProvider projectServiceProvider
            , IPermissionsServiceProvider permissionsServiceProvider
            , IUserServiceProvider userServiceProvider
            , IClaimsCollectionServiceProvider claimsCollectionServiceProvider
            )
        {
            _UserServiceProvider = userServiceProvider;
            _TenantServiceProvider = tenantServiceProvider;
            _ProjectServiceProvider = projectServiceProvider;
            _PermissionsServiceProvider = permissionsServiceProvider;
            _ClaimsCollectionServiceProvider = claimsCollectionServiceProvider;
        }

        public async Task InitAsync()
        {
            await _ProjectServiceProvider.InitAsync(Constants.GetInitialTenantIdentifier().GetProjectIdentifier());
            await _TenantServiceProvider.InitAsync(Constants.GetInitialTenantIdentifier());
        }

        public async Task InitSupportAsync()
        {
            await _ProjectServiceProvider.InitSupportAsync();
            await _TenantServiceProvider.InitSupportAsync();
        }
        
        public async Task<int> DeleteAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantServiceProvider.GetAsync(tenantIdentifier);

            // remove all user groups for tenant
            await _PermissionsServiceProvider.UserGroupServiceProvider.DeleteAllAsync(tenantIdentifier);

            // remove all roles for tenant
            await _PermissionsServiceProvider.RoleServiceProvider.DeleteAllAsync(tenantIdentifier);

            // remove all claims
            await _ClaimsCollectionServiceProvider.DeleteAllAsync(tenantIdentifier);

            foreach (var username in tenant.Usernames)
            {
                var userIdentifier = new UserIdentifier(username, tenantIdentifier);

                // remove user
                await _UserServiceProvider.DeleteAsync(userIdentifier);
            }


            // remove tenant
            await _TenantServiceProvider.DeleteAsync(tenantIdentifier);

            return 1;
        }

        public async Task<int> DeleteAsync(ProjectIdentifier projectIdentifier)
        {
            var project = await _ProjectServiceProvider.GetAsync(projectIdentifier);
            foreach (var tenantListItem in project.Tenants)
            {
                var tenantIdentifier = new TenantIdentifier(tenantListItem.Identifier.TenantCode, projectIdentifier);
                await DeleteAsync(tenantIdentifier);
            }

            // remove all claims types
            await _ClaimsCollectionServiceProvider.DeleteAllAsync(projectIdentifier);

            // remove project
            return await _ProjectServiceProvider.DeleteAsync(projectIdentifier);
        }

        public async Task<TenantIdentifier> CreateAsync(TenantCreateModel model)
        {
            var tenantIdentifier = await _TenantServiceProvider.CreateAsync(model);
            if (tenantIdentifier == null)
            {
                throw new IdPException();
            }

            await _PermissionsServiceProvider.InitPermissionsSupportAsync(tenantIdentifier);

            return tenantIdentifier;
        }

        public async Task<ProjectIdentifier> CreateAsync(ProjectCreateModel model)
        {
            var projectIdentifier = await _ProjectServiceProvider.CreateAsync(model);
            if (projectIdentifier == null)
            {
                throw new IdPException();
            }

            await _PermissionsServiceProvider.InitPermissionsSupportAsync(projectIdentifier);

            return projectIdentifier;
        }

    }
}
