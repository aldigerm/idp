﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserTenantServiceProvider
    {
        Task<UserTenantViewModel> GetAsync(UserTenantIdentifier userTenantIdentifier);
        Task<IList<UserTenantViewModel>> GetListAsync();
        Task<IList<UserTenantViewModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<UserTenantViewModel> GetLoggedInUserTenantAsync();
        Task<UserTenantViewModel> GetViewModelAsync(UserTenantIdentifier userTenantIdentifier);
        Task<UserTenantIdentifier> CreateAsync(UserTenantCreateModel model);
        Task<UserTenantIdentifier> UpdateAsync(UserTenantUpdateViewModel model);
        Task<bool> ExistsAsync(UserTenantIdentifier userTenantIdentifier, bool throwIfInexisting);
        Task<int> DeleteAsync(UserTenantIdentifier userTenantIdentifier);
        Task<IList<UserTenantViewModel>> GetAccessibleListAsync();
        UserTenantViewModel GetViewModel(UserTenant userTenant);
        Task<bool> SetUserTenantsForUser(UserIdentifier user, params TenantIdentifier[] tenantIdentifiers);
    }
}
