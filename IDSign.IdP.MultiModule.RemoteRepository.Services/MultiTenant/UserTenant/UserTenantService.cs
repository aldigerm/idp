﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserTenantService : IUserTenantServiceProvider
    {
        protected readonly IUserTenantDataProvider _UserTenantDataProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor ;
        protected readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        public UserTenantService
            (
            IUserTenantDataProvider UserTenantDataProvider
            , ITenantServiceProvider tenantServiceProvider
            , IHttpContextAccessor httpContextAccessor 
            , IProjectServiceProvider projectServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            )
        {
            _ProjectServiceProvider = projectServiceProvider;
            _HttpContextAccessor = httpContextAccessor  ;
            _UserTenantDataProvider = UserTenantDataProvider;
            _TenantServiceProvider = tenantServiceProvider;
            _ProjectServiceProvider = projectServiceProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
        }

        public async Task<UserTenantViewModel> GetAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var userTenant = await _UserTenantDataProvider.GetAsync(userTenantIdentifier);
            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, userTenantIdentifier.ToString());
            }
            return GetViewModel(userTenant);
        }
        public async Task<IList<UserTenantViewModel>> GetListAsync()
        {
            IList<UserTenant> userTenants = await _UserTenantDataProvider.GetListAsync();
            if (userTenants == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND);
            }
            IList<UserTenantViewModel> models = new List<UserTenantViewModel>();
            foreach (var userTenant in userTenants)
            {
                var model = GetViewModel(userTenant);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return new List<UserTenantViewModel>(models);

        }

        public async Task<IList<UserTenantViewModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            IList<UserTenant> userTenants = await _UserTenantDataProvider.GetListAsync(projectIdentifier);
            if (userTenants == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND);
            }
            IList<UserTenantViewModel> models = new List<UserTenantViewModel>();
            foreach (var userTenant in userTenants)
            {
                var model = GetViewModel(userTenant);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return new List<UserTenantViewModel>(models);

        }

        public async Task<IList<UserTenantViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<UserTenantViewModel> allUsers = null;
            if (tenantIdentifier == null)
            {
                allUsers = await GetListAsync();
            }
            else
            {
                allUsers = await GetListAsync(tenantIdentifier);
            }
            var accessibleUsers = await GetAccessibleListAsync();

            var viewableUsers = allUsers.Common(accessibleUsers, u => u.RepositoryUsername);

            return viewableUsers.ToList();
        }

        private async Task<IList<UserTenantViewModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<UserTenant> userTenants = await _UserTenantDataProvider.GetListAsync(tenantIdentifier);
            return GetListViewModel(userTenants);
        }

        private IList<UserTenantViewModel> GetListViewModel(IList<UserTenant> userTenants)
        {
            IList<UserTenantViewModel> list = new List<UserTenantViewModel>();
            foreach (var userTenant in userTenants)
            {
                var model = GetViewModel(userTenant);
                if (model != null)
                {
                    list.Add(model);
                }
            }
            return list;
        }
        public async Task<UserTenantViewModel> GetLoggedInUserTenantAsync()
        {
            UserTenantIdentifier identifier = new UserTenantIdentifier(_HttpContextAccessor.Username(), _HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode());
            return await GetViewModelAsync(identifier);
        }

        public async Task<UserTenantViewModel> GetViewModelAsync(UserTenantIdentifier model)
        {
            var userTenant = await _UserTenantDataProvider.GetAsync(model);
            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND);
            }
            return GetViewModel(userTenant);
        }

        public async Task<UserTenantIdentifier> CreateAsync(UserTenantCreateModel model)
        {
            var userTenant = await _UserTenantDataProvider.CreateAsync(model);
            if (userTenant == null)
            {
                throw new IdPException();
            }
            return model.GetUserTenantIdentifier();
        }

        public async Task<UserTenantIdentifier> UpdateAsync(UserTenantUpdateViewModel model)
        {
            var userTenant = await _UserTenantDataProvider.UpdateAsync(model);
            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND);
            }
            return userTenant;
        }


        public UserTenantViewModel GetViewModel(UserTenant userTenant)
        {
            UserTenantViewModel viewModel = new UserTenantViewModel();
            if (userTenant != null)
            {
                viewModel.RepositoryUsername = userTenant.UserName;
                viewModel.Identifier = userTenant.GetIdentifier();
                if (userTenant.UserClaims != null)
                {
                    foreach (var claim in userTenant.UserClaims)
                    {
                        viewModel.UserClaims.Add(new BasicClaim(claim.UserClaimType.Type, claim.Value));
                    }
                }
            }
            return viewModel;
        }

        public async Task<bool> ExistsAsync(UserTenantIdentifier userTenantIdentifier, bool throwIfInexisting)
        {
            bool exists = await _UserTenantDataProvider.ExistsAsync(userTenantIdentifier);
            if (!exists && throwIfInexisting)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, userTenantIdentifier.ToString());
            }
            return exists;
        }

        public async Task<int> DeleteAsync(UserTenantIdentifier userTenantIdentifier)
        {
            return await _UserTenantDataProvider.DeleteAsync(userTenantIdentifier);
        }


        public async Task<IList<UserTenantViewModel>> GetAccessibleListAsync()
        {

            var accessibleUsers = await _UserSharedServiceProvider.GetAccessibleListAsync();
            var list = new List<UserTenantViewModel>();
            foreach (var accessibleUser in accessibleUsers)
            {
                var userTenant = await GetAsync(new UserTenantIdentifier(accessibleUser.Identifier));
                if (userTenant != null)
                {
                    list.Add(userTenant);
                }
            }
            // user can only manage their own data
            return list;
        }

        public async Task<IList<UserTenantViewModel>> GetAccessibleListAsync(ProjectIdentifier projectIdentifier)
        {
            IList<UserTenantViewModel> allUsers = null;
            if (projectIdentifier == null)
            {
                allUsers = await GetListAsync();
            }
            else
            {
                allUsers = await GetListAsync(projectIdentifier);
            }
            var accessibleUsers = await GetAccessibleListAsync();

            var viewableUsers = allUsers.Common(accessibleUsers, u => u.RepositoryUsername);

            return viewableUsers.ToList();
        }

        public async Task<bool> SetUserTenantsForUser(UserIdentifier userIdentifier, params TenantIdentifier[] tenantIdentifiers)
        {
            var user = await _UserSharedServiceProvider.GetAsync(userIdentifier);
            var currentTenants = new List<TenantIdentifier>();
            foreach (var ut in user.UserTenants)
            {
                currentTenants.Add(ut.Tenant.GetIdentifier());
            }

            var submitted = tenantIdentifiers;
            var toRemove = new List<UserTenantIdentifier>();
            foreach (var currentTenant in currentTenants)
            {
                if (!submitted.Any(cg => cg.IsEquals(currentTenant)))
                {
                    toRemove.Add(new UserTenantIdentifier(user.TenantUsername, currentTenant));
                }
            }
            var toAdd = new List<UserTenantCreateModel>();
            foreach (var submittedTenant in submitted)
            {
                if (!currentTenants.Any(t => t.IsEquals(submittedTenant)))
                {
                    var model = new UserTenantCreateModel() {
                        UserIdentifier = userIdentifier,
                        TenantIdentifier = submittedTenant
                    };
                    toAdd.Add(model);
                }
            }
            foreach (var userGroupManagementIdentifier in toRemove)
            {
                await DeleteAsync(userGroupManagementIdentifier);
            }
            foreach (var userGroupManagementIdentifier in toAdd)
            {
                await CreateAsync(userGroupManagementIdentifier);
            }
            return true;
        }
    }
}
