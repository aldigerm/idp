﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IProjectServiceProvider
    {
        Task InitAsync(ProjectIdentifier projectIdentifier);

        Task<ProjectViewModel> GetAsync(ProjectIdentifier projectIdentifier);
        Task<IList<ProjectViewModel>> GetListAsync();
        Task<ProjectViewModel> GetLoggedInProjectAsync();
        Task<ProjectIdentifier> CreateAsync(ProjectCreateModel model);
        Task<ProjectIdentifier> UpdateAsync(ProjectUpdateViewModel model);
        Task<int> DeleteAsync(ProjectIdentifier projectIdentifier);
        Task<IList<ProjectViewModel>> GetAccessibleListAsync();
        Task InitSupportAsync();
    }
}
