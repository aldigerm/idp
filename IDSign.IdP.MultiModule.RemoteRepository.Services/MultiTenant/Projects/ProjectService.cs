﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class ProjectService : IProjectServiceProvider
    {
        protected readonly IProjectDataProvider _ProjectDataProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IModuleServiceProvider _ModuleServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        public ProjectService
            (
            IProjectDataProvider ProjectDataProvider
            , ITenantServiceProvider tenantServiceProvider
            , IHttpContextAccessor httpContextAccessor
            , IModuleServiceProvider moduleServiceProvider
            )
        {
            _HttpContextAccessor = httpContextAccessor;
            _TenantServiceProvider = tenantServiceProvider;
            _ProjectDataProvider = ProjectDataProvider;
            _ModuleServiceProvider = moduleServiceProvider;
        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            await _ProjectDataProvider.CreateOrUpdateAndGetProjectAsync(new ProjectUpdateViewModel() { Identifier = projectIdentifier, Name = "Initial Project" });
        }

        public async Task InitSupportAsync()
        {
            await _ProjectDataProvider.CreateOrUpdateAndGetProjectAsync(new ProjectUpdateViewModel() { Identifier = SupportConstants.GetSupportProjectIdentifier(), Name = "Support Project" });
        }

        public async Task<ProjectViewModel> GetAsync(ProjectIdentifier projectIdentifier)
        {
            var project = await _ProjectDataProvider.GetAsync(projectIdentifier);
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND);
            }
            return GetProjectViewModel(project);
        }

        public async Task<IList<ProjectViewModel>> GetListAsync()
        {
            var projects = await _ProjectDataProvider.GetListAsync();
            IList<ProjectViewModel> models = new List<ProjectViewModel>();
            foreach (var project in projects)
            {
                var model = GetProjectViewModel(project);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return models;

        }

        public async Task<ProjectViewModel> GetLoggedInProjectAsync()
        {
            return await GetAsync(new ProjectIdentifier(_HttpContextAccessor.ProjectCode()));
        }

        public async Task<ProjectIdentifier> CreateAsync(ProjectCreateModel model)
        {
            Project project = await _ProjectDataProvider.CreateAsync(model);
            if (project == null)
            {
                throw new IdPException();
            }

            return model.GetProjectIdentifier();
        }

        public async Task<ProjectIdentifier> UpdateAsync(ProjectUpdateViewModel model)
        {
            var identifier = await _ProjectDataProvider.UpdateAsync(model);
            if (identifier == null)
            {
                throw new IdPException();
            }
            return identifier;
        }

        private ProjectViewModel GetProjectViewModel(Project project)
        {
            ProjectViewModel viewModel = new ProjectViewModel();
            if (project != null)
            {
                viewModel.Name = project.Name;
                viewModel.Identifier = new ProjectIdentifier(project.Code);
                viewModel.Enabled = project.Enabled;
                viewModel.LoggedInUserProject = viewModel.Identifier.IsEquals(_HttpContextAccessor.LoggedInUserIdentifier()?.GetProjectIdentifier());
                foreach (var t in project.Tenants)
                {
                    viewModel.Tenants.Add(new TenantListElementModel(t.Name, t.Enabled, new TenantIdentifier(t.Code, project.Code)));
                }

                if (project.Modules != null)
                {
                    foreach (var module in project.Modules)
                    {
                        viewModel.ModulesCodes.Add(module.Code);
                    }
                }

                if (project.UserClaimTypes != null)
                {
                    foreach (var userClaimType in project.UserClaimTypes)
                    {
                        viewModel.UserClaimTypes.Add(userClaimType.Type);
                        viewModel.UserClaimTypesDetail.Add(new UserClaimTypeModel(userClaimType.Type, userClaimType.Description, viewModel.Identifier));
                    }
                }

                if (project.UserGroupClaimTypes != null)
                {
                    foreach (var userGroupClaimType in project.UserGroupClaimTypes)
                    {
                        viewModel.UserGroupClaimTypes.Add(userGroupClaimType.Type);
                        viewModel.UserGroupClaimTypesDetail.Add(new UserGroupClaimTypeModel(userGroupClaimType.Type, userGroupClaimType.Description, viewModel.Identifier));
                    }
                }

                if (project.RoleClaimTypes != null)
                {
                    foreach (var roleClaimType in project.RoleClaimTypes)
                    {
                        viewModel.RoleClaimTypes.Add(roleClaimType.Type);
                        viewModel.RoleClaimTypesDetail.Add(new RoleClaimTypeModel(roleClaimType.Type, roleClaimType.Description, viewModel.Identifier));
                    }
                }

                if (project.TenantClaimTypes != null)
                {
                    foreach (var tenantClaimType in project.TenantClaimTypes)
                    {
                        viewModel.TenantClaimTypes.Add(tenantClaimType.Type);
                        viewModel.TenantClaimTypesDetail.Add(new TenantClaimTypeModel(tenantClaimType.Type, tenantClaimType.Description, viewModel.Identifier));
                    }
                }
            }
            return viewModel;
        }

        public async Task<int> DeleteAsync(ProjectIdentifier projectIdentifier)
        {
            return await _ProjectDataProvider.DeleteAsync(projectIdentifier);
        }

        public async Task<IList<ProjectViewModel>> GetAccessibleListAsync()
        {
            if (_HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return await GetListAsync();
            }

            IList<ProjectViewModel> list = new List<ProjectViewModel>();
            if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN))
            {
                list = new List<ProjectViewModel>() { await GetLoggedInProjectAsync() };
            }
            else if (_HttpContextAccessor.HasAnyAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN))
            {
                var project = await GetLoggedInProjectAsync();
                var tenants = await _TenantServiceProvider.GetAccessibleListAsync();
                project.Tenants = new List<TenantListElementModel>();
                var modules = await _ModuleServiceProvider.GetAccessibleListAsync(_HttpContextAccessor.LoggedInUserIdentifier().GetProjectIdentifier());
                project.ModulesCodes = modules?.Where(m => m.Identifier.GetProjectIdentifier().IsEquals(project.Identifier)).Select(m => m.Identifier?.ModuleCode).ToList();
                foreach (var tenant in tenants)
                {
                    var listItemModel = new TenantListElementModel(tenant.Name, tenant.Enabled.GetValueOrDefault(false), tenant.Identifier);
                    project.Tenants.Add(listItemModel);
                }
                list = new List<ProjectViewModel>() { project };
            }
            else
            {
                return new List<ProjectViewModel>();
            }

            var resultList = list.ToList();
            resultList.RemoveAll(p => p.Identifier.IsEquals(SupportConstants.GetSupportProjectIdentifier()));
            return resultList;
        }

    }
}
