﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface ITenantServiceProvider
    {
        Task InitAsync(TenantIdentifier tenantIdentifier);
        Task InitSupportAsync();
        Task<TenantViewModel> GetAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true);
        Task<IList<TenantViewModel>> GetListAsync(bool includeTemporaryUsers = true);
        Task<IList<TenantViewModel>> GetListAsync(ProjectIdentifier projectIdentifier, bool includeTemporaryUsers = true);
        Task<TenantViewModel> GetLoggedInTenantAsync();
        Task<TenantViewModel> GetViewModelAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true);
        Task<TenantIdentifier> CreateAsync(TenantCreateModel model);
        Task<TenantIdentifier> UpdateAsync(TenantUpdateViewModel model);
        Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier, bool throwIfInexisting);
        Task<int> DeleteAsync(TenantIdentifier tenantIdentifier);
        Task<IList<TenantViewModel>> GetAccessibleListAsync();
        TenantViewModel GetViewModel(Tenant tenant, bool includeTemporaryUsers = true);
    }
}
