﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class TenantService : ITenantServiceProvider
    {
        protected readonly ITenantDataProvider _TenantDataProvider;
        protected readonly IUserGroupManagementServiceProvider _UserGroupManagementServiceProvider;
        protected readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor ;
        public TenantService
            (
            ITenantDataProvider TenantDataProvider
            , IHttpContextAccessor httpContextAccessor 
            , IUserGroupManagementServiceProvider userGroupManagementServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            )
        {
            _HttpContextAccessor = httpContextAccessor  ;
            _TenantDataProvider = TenantDataProvider;
            _UserGroupManagementServiceProvider = userGroupManagementServiceProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
        }
        public async Task InitSupportAsync()
        {
            await _TenantDataProvider.CreateOrUpdateAndGetTenantAsync(new TenantUpdateViewModel() { Identifier = SupportConstants.GetSupportTenantIdentifier(), Name = "Support" });
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {
            await _TenantDataProvider.CreateOrUpdateAndGetTenantAsync(new TenantUpdateViewModel() { Identifier = tenantIdentifier, Name = "BSPay" });
        }

        public async Task<TenantViewModel> GetAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, tenantIdentifier.ToString());
            }
            return GetViewModel(tenant, includeTemporaryUsers);
        }

        public async Task<IList<TenantViewModel>> GetListAsync(bool includeTemporaryUsers = true)
        {
            IList<Tenant> tenants = await _TenantDataProvider.GetListAsync();
            if (tenants == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND);
            }
            IList<TenantViewModel> models = new List<TenantViewModel>();
            foreach (var tenant in tenants)
            {
                var model = GetViewModel(tenant, includeTemporaryUsers);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return new List<TenantViewModel>(models);

        }
        public async Task<IList<TenantViewModel>> GetListAsync(ProjectIdentifier projectIdentifier, bool includeTemporaryUsers = true)
        {
            IList<Tenant> tenants = await _TenantDataProvider.GetListAsync(projectIdentifier);
            if (tenants == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND);
            }
            IList<TenantViewModel> models = new List<TenantViewModel>();
            foreach (var tenant in tenants)
            {
                var model = GetViewModel(tenant, includeTemporaryUsers);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return new List<TenantViewModel>(models);

        }

        public async Task<TenantViewModel> GetLoggedInTenantAsync()
        {
            TenantIdentifier identifier = new TenantIdentifier(_HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode());
            return await GetViewModelAsync(identifier);
        }

        public async Task<TenantViewModel> GetViewModelAsync(TenantIdentifier model, bool includeTemporaryUsers = true)
        {
            var tenant = await _TenantDataProvider.GetAsync(model);
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND);
            }
            return GetViewModel(tenant, includeTemporaryUsers);
        }

        public async Task<TenantIdentifier> CreateAsync(TenantCreateModel model)
        {
            var tenant = await _TenantDataProvider.CreateAsync(model);
            if (tenant == null)
            {
                throw new IdPException();
            }
            return model.GetTenantIdentifier();
        }

        public async Task<TenantIdentifier> UpdateAsync(TenantUpdateViewModel model)
        {
            var tenant = await _TenantDataProvider.UpdateAsync(model);
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND);
            }
            return tenant;
        }


        public TenantViewModel GetViewModel(Tenant tenant, bool includeTemporaryUsers)
        {
            TenantViewModel viewModel = new TenantViewModel();
            if (tenant != null)
            {
                viewModel.Id = tenant.Id;
                viewModel.Name = tenant.Name;
                viewModel.Identifier = new TenantIdentifier(tenant.Code, tenant.Project.Code);
                viewModel.LoggedInUserTenant = viewModel.Identifier.IsEquals(_HttpContextAccessor.LoggedInUserIdentifier()?.GetTenantIdentifier());
                viewModel.Enabled = tenant.Enabled;
                if (tenant.UserTenants != null)
                {
                    foreach (var userTenant in tenant.UserTenants)
                    {
                        if (includeTemporaryUsers || !userTenant.User.IsTemporaryUser)
                        {
                            viewModel.Usernames.Add(userTenant.User.TenantUsername);
                        }
                    }
                }
                if (tenant.UserGroups != null)
                {
                    foreach (var userGroup in tenant.UserGroups)
                    {
                        viewModel.UserGroups.Add(userGroup.Code);
                        viewModel.UserGroupsInferred.Add(userGroup.Code);
                    }
                }
                if (tenant.Roles != null)
                {
                    foreach (var role in tenant.Roles)
                    {
                        viewModel.Roles.Add(role.Code);
                    }
                }
                if (tenant.PasswordPolicies != null)
                {
                    foreach (var passwordPolicy in tenant.PasswordPolicies)
                    {
                        viewModel.PasswordPolicies.Add(passwordPolicy.Code);
                    }
                }
                if (tenant.TenantClaims != null)
                {
                    foreach (var claim in tenant.TenantClaims)
                    {
                        viewModel.BasicClaims.Add(new BasicClaim(claim.TenantClaimType.Type, claim.Value));
                    }
                }
            }
            return viewModel;
        }

        public async Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier, bool throwIfInexisting)
        {
            bool exists = await _TenantDataProvider.ExistsAsync(tenantIdentifier);
            if (!exists && throwIfInexisting)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, tenantIdentifier.ToString());
            }
            return exists;
        }

        public async Task<int> DeleteAsync(TenantIdentifier tenantIdentifier)
        {
            return await _TenantDataProvider.DeleteAsync(tenantIdentifier);
        }


        public async Task<IList<TenantViewModel>> GetAccessibleListAsync()
        {
            if (_HttpContextAccessor.IsAdmin() || _HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()))
            {
                return await GetListAsync();
            }

            IList<TenantViewModel> list = new List<TenantViewModel>();

            if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN))
            {
                list = await GetListAsync(new ProjectIdentifier(_HttpContextAccessor.ProjectCode()));
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN))
            {
                list = new List<TenantViewModel>() { await GetLoggedInTenantAsync() };
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_USER_MANAGEMENT))
            {
                var tenant = await GetLoggedInTenantAsync();
                List<UserGroup> accessibleGroups = await _UserGroupManagementServiceProvider.GetAccessibleGroups(_HttpContextAccessor.LoggedInUserIdentifier());
                List<UserGroup> accessibleGroupsInferred = await _UserGroupServiceProvider.GetAccessibleGroupsIncluded(_HttpContextAccessor.LoggedInUserIdentifier());
                tenant.UserGroups = accessibleGroups.Select(ug => ug.Code).ToList();
                tenant.UserGroupsInferred = accessibleGroupsInferred.Select(ug => ug.Code).ToList();
                tenant.Usernames = accessibleGroupsInferred.SelectMany(ug => ug.ApplicationUserGroups).Select(aug => aug.UserTenant.User.TenantUsername).Distinct().ToList();
                list = new List<TenantViewModel>() { tenant };
            }
            else
            {
                return new List<TenantViewModel>();
            }

            foreach (var tenant in list)
            {
                tenant.UserGroups = tenant.UserGroups.Where(ug => ug != SupportConstants.SupportUserGroupCode).ToList();
                tenant.UserGroupsInferred = tenant.UserGroupsInferred.Where(ug => ug != SupportConstants.SupportUserGroupCode).ToList();
            }

            return list;
        }

    }
}
