﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IMultiTenantServiceProvider
    {
        Task InitAsync();
        Task InitSupportAsync();
        Task<int> DeleteAsync(TenantIdentifier tenantIdentifier);
        Task<int> DeleteAsync(ProjectIdentifier projectIdentifier);
        Task<TenantIdentifier> CreateAsync(TenantCreateModel model);
        Task<ProjectIdentifier> CreateAsync(ProjectCreateModel model);
    }
}
