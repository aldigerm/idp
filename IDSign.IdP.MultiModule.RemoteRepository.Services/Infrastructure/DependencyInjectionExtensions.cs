﻿using IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddServicesLayerServices(this IServiceCollection services)
        {
            services.AddTransient<IPermissionsServiceProvider, PermissionsService>();
            services.AddTransient<ITenantServiceProvider, TenantService>();
            services.AddTransient<IProjectServiceProvider, ProjectService>();
            services.AddTransient<IMultiTenantServiceProvider, MultiTenantService>();
            services.AddTransient<IUserServiceProvider, UserService>();
            services.AddTransient<IModuleServiceProvider, ModuleService>();
            services.AddTransient<IUserGroupServiceProvider, UserGroupService>();
            services.AddTransient<IRoleServiceProvider, RoleService>();
            services.AddTransient<IUserClaimServiceProvider, UserClaimService>();
            services.AddTransient<IUserClaimTypeServiceProvider, UserClaimTypeService>();
            services.AddTransient<IUserGroupClaimServiceProvider, UserGroupClaimService>();
            services.AddTransient<IUserGroupClaimTypeServiceProvider, UserGroupClaimTypeService>();
            services.AddTransient<IRoleClaimServiceProvider, RoleClaimService>();
            services.AddTransient<IRoleClaimTypeServiceProvider, RoleClaimTypeService>();
            services.AddTransient<ITenantClaimServiceProvider, TenantClaimService>();
            services.AddTransient<ITenantClaimTypeServiceProvider, TenantClaimTypeService>();
            services.AddTransient<IUserGroupManagementServiceProvider, UserGroupManagementService>();
            services.AddTransient<IManagementServiceProvider, ManagementService>();
            services.AddTransient<IClaimsCollectionServiceProvider, ClaimsCollectionService>();
            services.AddTransient<IUserSharedServiceProvider, UserSharedService>();
            services.AddTransient<IUserTenantServiceProvider, UserTenantService>();

            services.AddTransient<IPasswordPolicyServiceProvider, PasswordPolicyService>();
            services.AddTransient<IPasswordPolicyTypeServiceProvider, PasswordPolicyTypeService>();
            services.AddTransient<IPasswordPolicyUserGroupServiceProvider, PasswordPolicyUserGroupService>();
            services.AddTransient<IUserPreviousPasswordServiceProvider, UserPreviousPasswordService>();


            services.AddTransient<IUserTenantSessionServiceProvider, UserTenantSessionService>();
            services.AddTransient<IUserTenantSessionActionServiceProvider, UserTenantSessionActionService>();
            services.AddTransient<IEmailServiceProvider, EmailService>();

            services.AddTransient<ITemporaryUserServiceProvider, TemporaryUserService>();
            services.AddTransient<IOTPServiceProvider, OTPService>();

            return services;
        }
    }
}