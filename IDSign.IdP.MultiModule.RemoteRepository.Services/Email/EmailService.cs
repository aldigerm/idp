﻿using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WCF.EmailService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class EmailService : IEmailServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        public EmailService(
             ILogger<UserService> logger
            )
        {
            _logger = logger;
        }


        private async Task<bool> SendEmailAsync(string subject, string emailBody, string to, List<AttachmentFile> attachments)
        {
            EmailServiceClient emailServiceClient = new EmailServiceClient(EmailServiceClient.EndpointConfiguration.BasicHttpBinding_IEmailService);
            return await emailServiceClient.SendEmailAsync(subject, emailBody, to, attachments);
        }


    }
}
