﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserGroupManagementServiceProvider
    {
        Task<List<UserGroup>> GetAccessibleGroups(UserIdentifier userIdentifier);
        Task<IList<UserToUserGroupManagement>> GetListAsync(UserIdentifier userIdentifier);
        Task<bool> SetUserGroups(UserIdentifier userIdentifier, IList<string> userGroupCodes);

    }
}
