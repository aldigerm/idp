﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserGroupManagementService : IUserGroupManagementServiceProvider
    {
        protected readonly ILogger<UserGroupService> _logger;
        protected readonly IUserGroupManagementDataProvider _UserGroupManagementDataProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor ;

        public UserGroupManagementService(
            IUserGroupManagementDataProvider userGroupManagementDataProvider
            , ILogger<UserGroupService> logger
            , IHttpContextAccessor httpContextAccessor 
            )
        {
            _UserGroupManagementDataProvider = userGroupManagementDataProvider;
            _HttpContextAccessor = httpContextAccessor  ;
            _logger = logger;
        }

        public async Task<List<UserGroup>> GetAccessibleGroups(UserIdentifier userIdentifier)
        {
            var list = await _UserGroupManagementDataProvider.GetListAsync(userIdentifier);
            return list?.Select(ugm => ugm.UserGroup)?.ToList() ?? new List<UserGroup>();
        }

        public async Task<IList<UserToUserGroupManagement>> GetListAsync(UserIdentifier userIdentifier)
        {
            return await _UserGroupManagementDataProvider.GetListAsync(userIdentifier);
        }

        public async Task<bool> SetUserGroups(UserIdentifier userIdentifier, IList<string> userGroupCodes)
        {
            var list = await _UserGroupManagementDataProvider.GetListAsync(userIdentifier);
            var currentGroups = new List<UserGroupIdentifier>();
            foreach (var ug in list)
            {
                currentGroups.Add(ug.GetIdentifier().GetUserGroupIdentifier());
            }

            var submitted = userGroupCodes;
            var toRemove = new List<UserGroupManagementIdentifier>();
            foreach (var currentGroup in currentGroups)
            {
                if (!submitted.Any(cg => cg == currentGroup.UserGroupCode))
                {
                    toRemove.Add(new UserGroupManagementIdentifier(userIdentifier, currentGroup));
                }
            }
            var toAdd = new List<UserGroupManagementIdentifier>();
            foreach (var submittedUserGroup in submitted)
            {
                var id = new UserGroupIdentifier(submittedUserGroup, userIdentifier.GetTenantIdentifier());
                if (!currentGroups.Any(u => u.IsEquals(id)))
                {
                    toAdd.Add(new UserGroupManagementIdentifier(userIdentifier, id));
                }
            }
            foreach (var userGroupManagementIdentifier in toRemove)
            {
                await DeleteAsync(userGroupManagementIdentifier);
            }
            foreach (var userGroupManagementIdentifier in toAdd)
            {
                await CreateAsync(userGroupManagementIdentifier);
            }
            return true;
        }

        private async Task<UserGroupManagementIdentifier> CreateAsync(UserGroupManagementIdentifier userGroupIdentifier)
        {
            return await _UserGroupManagementDataProvider.CreateAsync(userGroupIdentifier);
        }

        private async Task<int> DeleteAsync(UserGroupManagementIdentifier userGroupIdentifier)
        {
            return await _UserGroupManagementDataProvider.DeleteAsync(userGroupIdentifier);
        }


    }
}
