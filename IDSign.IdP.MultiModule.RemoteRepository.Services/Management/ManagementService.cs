﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class ManagementService : IManagementServiceProvider
    {
        protected readonly ILogger<ManagementService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor ;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IModuleServiceProvider _ModuleServiceProvider;
        protected readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly IRoleServiceProvider _RoleServiceProvider;
        protected readonly IUserSharedServiceProvider _UserSharedServiceProvider;

        private readonly string[] SystemModules = { ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN.ToString(), ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN.ToString(), ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString() };

        public ManagementService(
              ILogger<ManagementService> logger
            , IHttpContextAccessor httpContextAccessor
            , ITenantServiceProvider tenantDataProvider
            , IModuleServiceProvider moduleServiceProvider
            , IProjectServiceProvider projectServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , IRoleServiceProvider roleServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            )
        {
            _logger = logger;
            _RoleServiceProvider = roleServiceProvider;
            _ModuleServiceProvider = moduleServiceProvider;
            _TenantServiceProvider = tenantDataProvider;
            _ProjectServiceProvider = projectServiceProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
            _HttpContextAccessor = httpContextAccessor;
            _UserSharedServiceProvider = userSharedServiceProvider;
        }

        public async Task EnforceAccessibiltyAsync(TenantIdentifier tenantIdentifier)
        {
            var accessible = await _TenantServiceProvider.GetAccessibleListAsync();
            if (!accessible.Any(t => t.Identifier.IsEquals(tenantIdentifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.TENANT_NOT_ACCESSIBLE);
            }
        }

        public async Task EnforceNotRelatedAsync(TenantIdentifier tenantIdentifier)
        {
            var notPossibleToUpdate = await _TenantServiceProvider.GetLoggedInTenantAsync();
            if (notPossibleToUpdate.Identifier.IsEquals(tenantIdentifier))
            {
                throw new IdPUnauthorizedException(ErrorCode.TENANT_NOT_ACCESSIBLE_IS_RELATED, tenantIdentifier.ToString());
            }
        }

        public void EnforceNotChangeSupportAsync(RoleIdentifier roleIdentifier)
        {
            if (roleIdentifier.RoleCode == SupportConstants.GetSupportRoleIdentifier().RoleCode)
            {
                throw new IdPUnauthorizedException(ErrorCode.ROLE_NOT_ACCESSIBLE_IS_SUPPORT, roleIdentifier.ToString());
            }
        }

        public async Task EnforceNotRelatedAsync(RoleIdentifier roleIdentifier)
        {
            var user = await _UserSharedServiceProvider.GetAsync(_HttpContextAccessor.LoggedInUserIdentifier());
            var notPossibleToUpdate = await _RoleServiceProvider.GetRolesRecursivelyAsync(user, _HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier());
            if (notPossibleToUpdate.Any(t => t.GetIdentifier().IsEquals(roleIdentifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.ROLE_NOT_ACCESSIBLE_IS_RELATED, roleIdentifier.ToString());
            }
        }

        public async Task EnforceAccessibiltyAsync(UserIdentifier userIdentifier)
        {
            var accessible = await _UserSharedServiceProvider.GetAccessibleListAsync();
            if (!accessible.Any(t => t.Identifier.IsEquals(userIdentifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.USER_NOT_ACCESSIBLE, userIdentifier.ToString());
            }
        }

        public async Task EnforceAccessibiltyAsync(TenantIdentifier tenantIdentifier, List<string> usernames)
        {
            var accessible = await _UserSharedServiceProvider.GetAccessibleListAsync();
            foreach (var username in usernames)
            {
                var userIdentifier = new UserIdentifier(username, tenantIdentifier);
                if (!accessible.Any(t => t.Identifier.IsEquals(userIdentifier)))
                {
                    throw new IdPUnauthorizedException(ErrorCode.USER_NOT_ACCESSIBLE, userIdentifier.ToString());
                }
            }
        }

        public async Task EnforceNotRelatedAsync(UserGroupIdentifier userGroupIdentifier)
        {
            var user = await _UserSharedServiceProvider.GetAsync(_HttpContextAccessor.LoggedInUserIdentifier(), false, false);
            var notPossibleToUpdate = await _UserGroupServiceProvider.GetInferredUserGroupsAsync(user, _HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier());
            if (notPossibleToUpdate.Any(t => t.GetIdentifier().IsEquals(userGroupIdentifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.USERGROUP_NOT_ACCESSIBLE_IS_RELATED, userGroupIdentifier.ToString());
            }
        }

        public void EnforceNotSameUser(UserIdentifier userIdentifier)
        {
            if (_HttpContextAccessor.LoggedInUserIdentifier().IsEquals(userIdentifier))
            {
                throw new IdPUnauthorizedException(ErrorCode.USER_NOT_ACCESSIBLE_AS_SAME, "Cannot set your own user groups.");
            }
        }

        public void EnforceNotChangeSupportAsync(ProjectIdentifier projectIdentifier)
        {
            if (projectIdentifier.ProjectCode == SupportConstants.GetSupportProjectIdentifier().ProjectCode)
            {
                throw new IdPUnauthorizedException(ErrorCode.PROJECT_NOT_ACCESSIBLE_IS_SUPPORT, projectIdentifier.ToString());
            }
        }

        public void EnforceNotChangeSupportAsync(UserGroupIdentifier userGroupIdentifier)
        {
            if (userGroupIdentifier.UserGroupCode == SupportConstants.GetSupportUserGroupIdentifier().UserGroupCode)
            {
                throw new IdPUnauthorizedException(ErrorCode.USERGROUP_NOT_ACCESSIBLE_IS_SUPPORT, userGroupIdentifier.ToString());
            }
        }
        public void EnforceNotChangeSupportAsync(TenantIdentifier tenantIdentifier)
        {
            if (tenantIdentifier.TenantCode == SupportConstants.GetSupportTenantIdentifier().TenantCode)
            {
                throw new IdPUnauthorizedException(ErrorCode.TENANT_NOT_ACCESSIBLE_IS_SUPPORT, tenantIdentifier.ToString());
            }
        }
        public async Task EnforceNotRelatedAsync(ProjectIdentifier projectIdentifier)
        {
            var notPossibleToUpdate = await _ProjectServiceProvider.GetLoggedInProjectAsync();
            if (notPossibleToUpdate.Identifier.IsEquals(projectIdentifier))
            {
                throw new IdPUnauthorizedException(ErrorCode.PROJECT_NOT_ACCESSIBLE_IS_RELATED, projectIdentifier.ToString());
            }
        }

        public void EnforceNotChangeSupportAsync(ModuleIdentifier moduleIdentifier)
        {
            if (moduleIdentifier.ModuleCode == SupportConstants.GetSupportModuleIdentifier().ModuleCode)
            {
                throw new IdPUnauthorizedException(ErrorCode.MODULE_NOT_ACCESSIBLE_IS_SUPPORT, moduleIdentifier.ToString());
            }
        }

        public void EnforceNotChangeSystemModulesAsync(ModuleIdentifier moduleIdentifier)
        {
            if (SystemModules.Any(m => m == moduleIdentifier.ModuleCode))
            {
                throw new IdPUnauthorizedException(ErrorCode.MODULE_NOT_ACCESSIBLE_IS_SYSTEM, moduleIdentifier.ToString());
            }
        }

        public async Task EnforceNeedingSupportCredentials(UserGroupIdentifier userGroupIdentifier)
        {
            var user = await _UserSharedServiceProvider.GetLoggedInUser(false);
            if (user.UserGroups.Any(ug => SupportConstants.GetSupportUserGroupIdentifier().UserGroupCode == ug) && !IsSupportUser(user))
            {
                throw new IdPUnauthorizedException(ErrorCode.USERGROUP_NOT_ACCESSIBLE_REQUIRES_SUPPORT, userGroupIdentifier.ToString());
            }
        }

        public async Task<bool> IsSupportUser(UserIdentifier userIdentifier)
        {
            var user = await _UserSharedServiceProvider.GetUserViewModelAsync(userIdentifier);
            return IsSupportUser(user);
        }

        public bool IsSupportUser(UserViewModel user)
        {
            var result = user?.UserGroups?.Any(ug => SupportConstants.GetSupportUserGroupIdentifier().UserGroupCode == ug);
            return result.GetValueOrDefault(false);
        }


        public async Task EnforceAccessibilty(UserGroupIdentifier userGroupIdentifier)
        {
            var accessible = await _UserGroupServiceProvider.GetAccessibleListAsync(userGroupIdentifier.GetTenantIdentifier());
            if (!accessible.Any(t => t.Identifier.IsEquals(userGroupIdentifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.USERGROUP_NOT_ACCESSIBLE_IS_RELATED);
            }
        }

        public async Task EnforceAccessibilty(TenantIdentifier tenantIdentifier, List<string> userGroupCodes)
        {
            var accessible = await _UserGroupServiceProvider.GetAccessibleListAsync(tenantIdentifier);
            foreach (var accessibleUg in accessible)
            {
                if (!userGroupCodes.Select(ugc => new UserGroupIdentifier(ugc, tenantIdentifier)).Any(id => id.IsEquals(accessibleUg.Identifier)))
                    throw new IdPUnauthorizedException(ErrorCode.USERGROUP_NOT_ACCESSIBLE_IS_RELATED);
            }
        }

        public async Task EnforceAccessibilty(RoleIdentifier identifier)
        {
            var accessible = await _RoleServiceProvider.GetAccessibleListAsync(identifier.GetTenantIdentifier());
            if (!accessible.Any(r => r.Identifier.IsEquals(identifier)))
            {
                throw new IdPUnauthorizedException(ErrorCode.ROLE_NOT_ACCESSIBLE);
            }
        }
    }
}
