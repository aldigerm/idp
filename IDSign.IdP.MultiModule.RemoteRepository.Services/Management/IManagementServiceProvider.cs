﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IManagementServiceProvider
    {

        Task EnforceAccessibiltyAsync(UserIdentifier userIdentifier);
        Task EnforceAccessibiltyAsync(TenantIdentifier tenantIdentifier, List<string> usernames);
        Task EnforceNotRelatedAsync(UserGroupIdentifier userGroupIdentifier);
        void EnforceNotSameUser(UserIdentifier userIdentifier);
        Task EnforceAccessibiltyAsync(TenantIdentifier tenantIdentifier);
        Task EnforceNotRelatedAsync(TenantIdentifier roleIdentifier);
        Task EnforceNotRelatedAsync(RoleIdentifier roleIdentifier);
        void EnforceNotChangeSupportAsync(RoleIdentifier roleIdentifier);
        void EnforceNotChangeSupportAsync(ProjectIdentifier projectIdentifier);
        void EnforceNotChangeSupportAsync(TenantIdentifier tenantIdentifier);
        Task EnforceNotRelatedAsync(ProjectIdentifier roleIdentifier);
        void EnforceNotChangeSupportAsync(ModuleIdentifier moduleIdentifier);
        Task EnforceAccessibilty(UserGroupIdentifier userGroupIdentifier);
        Task EnforceAccessibilty(TenantIdentifier tenantIdentifier, List<string> userGroupCodes);
        void EnforceNotChangeSupportAsync(UserGroupIdentifier identifier);
        Task EnforceNeedingSupportCredentials(UserGroupIdentifier userGroupIdentifier);
        bool IsSupportUser(UserViewModel user);
        void EnforceNotChangeSystemModulesAsync(ModuleIdentifier moduleIdentifier);
        Task EnforceAccessibilty(RoleIdentifier identifier);
        Task<bool> IsSupportUser(UserIdentifier userIdentifier);
    }
}
