﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IOTPServiceProvider
    {
        bool Send(string companyName, string phoneNumber, string otpValue, string recipientFullName = null);
        string GenerateOTP();
        bool IsOTPMatch(AppUser user, string storedValue, string submittedValue);
    }
}
