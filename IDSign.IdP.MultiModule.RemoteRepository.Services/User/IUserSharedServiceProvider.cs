﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserSharedServiceProvider
    {
        Task<AppUser> GetSimpleAsync(UserIdentifier userIdentifier);
        Task<AppUser> GetAsync(int id);
        Task<AppUser> GetAsync(UserIdentifier request);
        Task<AppUser> GetAsync(UserIdentifier request, bool setAsLoggedInUser, bool getModules);
        Task<AppUser> GetUserAsync(UserIdentifier request, bool setAsLoggedInUser, bool getModules, UserRegisterViewModel registerModel);
        Task<UserViewModel> GetUserViewModelAsync(UserIdentifier request);
        UserViewModel GetUserViewModel(AppUser user, TenantIdentifier tenantIdentifier);
        UserViewModel GetUserViewModel(AppUser user, int? tenantId);
        Task<UserViewModel> GetLoggedInUser(bool getAccessDetail);
        Task<IList<UserViewModel>> GetListAsync(bool includeTemporaryUsers = true);
        ClaimsPrincipal SetAsLoggedInUser(AppUser user, UserIdentifier userIdentifier);
        ClaimsPrincipal SetApiAdminAsLoggedInUser(TenantIdentifier tenantIdentifier);
        Task<bool> ExistsAsync(UserIdentifier userIdentifier);
        Task<IList<UserViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true);
        Task<IList<UserViewModel>> GetAccessibleListAsync(bool includeTemporaryUsers = true);
        Task<IList<UserViewModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<UserProfileModel> GetUserProfileAsync(string username = null, string tenantCode = null, string projectCode = null);
        Task<byte[]> GetUserProfileImageAsync(string username = null, string tenantCode = null, string projectCode = null);
        Task<UserIdentifier> CreateAsync(UserRegisterViewModel request);
    }
}
