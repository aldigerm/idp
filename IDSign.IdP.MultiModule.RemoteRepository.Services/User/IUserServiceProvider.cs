﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserServiceProvider
    {
        Task InitAsync();
        Task InitSupportAsync(); 
        Task<UserIdentifier> UpdateAsync(UserUpdateViewModel request);
        Task<UserViewModel> UpdatePasswordAsync(UpdatePasswordViewModel request);
        Task<UserViewModel> UpdateSecuritySettingsAsync(UpdateSecuritySettingsViewModel request);
        Task<UserViewModel> LoginUserAsync(LoginViewModel login);
        Task<UserViewModel> SetGroupsForUser(UserIdentifier user, params string[] userGroupCodes);
        Task<bool> SetManagedGroupsForUser(UserIdentifier user, params string[] userGroupCodes);
        Task<int> DeleteAsync(UserIdentifier user);
        Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, string mimeType, Stream stream);
        Task<ClaimsPrincipal> InitialiseUserFromPrincipal(ClaimsPrincipal principal);
    }
}
