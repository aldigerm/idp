﻿using IDSign.IdP.Core;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserSharedService : IUserSharedServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IUserDataProvider _UserDataProvider;
        protected readonly IUserTenantDataProvider _UserTenantDataProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly IModuleServiceProvider _ModuleServiceProvider;
        protected readonly IRoleServiceProvider _RoleServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserClaimDataProvider _UserClaimDataProvider;
        protected readonly IUserGroupManagementServiceProvider _UserGroupManagementServiceProvider;
        protected readonly IPasswordPolicyServiceProvider _PasswordPolicyServiceProvider;
        protected readonly IPasswordPolicyTypeServiceProvider _PasswordPolicyTypeServiceProvider;
        protected readonly IPasswordPolicyUserGroupServiceProvider _PasswordPolicyUserGroupServiceProvider;
        protected readonly IUserTenantSessionServiceProvider _UserTenantSessionServiceProvider;

        public UserSharedService(
              IUserDataProvider userDataProvider
            , ITenantServiceProvider tenantServiceProvider
            , IModuleServiceProvider moduleServiceProvider
            , IHttpContextAccessor httpContextAccessor
            , IProjectServiceProvider projectServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , IRoleServiceProvider roleServiceProvider
            , ILogger<UserService> logger
            , IUserClaimDataProvider userClaimDataProvider
            , IUserGroupManagementServiceProvider userGroupManagementServiceProvider
            , IUserTenantDataProvider userTenantDataProvider
            , IPasswordPolicyUserGroupServiceProvider passwordPolicyUserGroupServiceProvider
            , IPasswordPolicyServiceProvider passwordPolicyServiceProvider
            , IPasswordPolicyTypeServiceProvider passwordPolicyTypeServiceProvider
            , IUserTenantSessionServiceProvider userTenantSessionServiceProvider
            )
        {
            _logger = logger;
            _RoleServiceProvider = roleServiceProvider;
            _ModuleServiceProvider = moduleServiceProvider;
            _TenantServiceProvider = tenantServiceProvider;
            _UserDataProvider = userDataProvider;
            _ProjectServiceProvider = projectServiceProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
            _HttpContextAccessor = httpContextAccessor;
            _UserClaimDataProvider = userClaimDataProvider;
            _UserGroupManagementServiceProvider = userGroupManagementServiceProvider;
            _UserTenantDataProvider = userTenantDataProvider;
            _PasswordPolicyUserGroupServiceProvider = passwordPolicyUserGroupServiceProvider;
            _PasswordPolicyServiceProvider = passwordPolicyServiceProvider;
            _PasswordPolicyTypeServiceProvider = passwordPolicyTypeServiceProvider;
            _UserTenantSessionServiceProvider = userTenantSessionServiceProvider;
        }

        public async Task<AppUser> GetAsync(UserIdentifier userIdentifier)
        {
            var user = await _UserDataProvider.GetAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userIdentifier?.ToString());
            }
            return user;
        }

        public async Task<AppUser> GetSimpleAsync(UserIdentifier userIdentifier)
        {
            var user = await _UserDataProvider.GetSimpleAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userIdentifier?.ToString());
            }
            return user;
        }

        public async Task<UserViewModel> GetUserViewModelAsync(UserIdentifier userIdentifier)
        {
            var user = await GetAsync(userIdentifier, false, true);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERNAME_EXISTS);
            }
            return GetUserViewModel(user, userIdentifier.GetTenantIdentifier());
        }

        public async Task<AppUser> GetAsync(int id)
        {
            var user = await _UserDataProvider.GetAsync(id);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, id.ToString());
            }
            return user;
        }

        public async Task<bool> ExistsAsync(UserIdentifier userIdentifier)
        {
            return await _UserDataProvider.ExistsAsync(userIdentifier);
        }


        public UserViewModel GetUserViewModel(AppUser user, TenantIdentifier tenantIdentifier)
        {
            int? tenantId = null;
            if (tenantIdentifier != null)
            {
                tenantId = AsyncHelpers.RunSync(() => _TenantServiceProvider.GetAsync(tenantIdentifier))?.Id;
            }
            return GetUserViewModel(user, tenantId);
        }

        public UserViewModel GetUserViewModel(AppUser user, int? tenantId)
        {
            if (user == null)
            {
                return null;
            }
            UserViewModel viewModel = new UserViewModel();
            viewModel = user.UserDetails == null ? viewModel : HelpersService.CopyProperties<UserViewModel>(user?.UserDetails);

            if (tenantId.HasValue)
            {
                UserTenant userTenant = user.UserTenants.FirstOrDefault(ut => ut.TenantId == tenantId.Value);
                var userIdentifer = new UserIdentifier(user.ObjectIdentifier, user.TenantUsername, userTenant?.Tenant.Code, userTenant?.Tenant.Project.Code);
                if (userTenant?.UserClaims != null)
                {
                    foreach (var claim in userTenant.UserClaims.Where(c => c != null))
                    {
                        viewModel.UserClaims.Add(new UserClaimModel(claim.Value, claim.UserClaimType.Type, claim.UserClaimType.Description, userIdentifer));
                    }
                }
                viewModel.Identifier = userIdentifer;
                viewModel.RepositoryUsername = userTenant?.UserName;
                viewModel.IsTenantSpecific = true;
                var userGroups = userTenant.UserTenantUserGroups?.Where(aug => aug.UserGroup.TenantId == tenantId.Value);
                viewModel.UserGroups = userGroups?.Select(aug => aug.UserGroup.Code).ToList();
                if (userTenant.UserGroupManagement != null)
                {
                    foreach (var ug in userTenant.UserGroupManagement.Select(um => um.UserGroup))
                    {
                        viewModel.ManagedUserGroups.Add(ug.Code);
                    }
                }

                viewModel.PasswordPolicies = new List<PasswordPolicyModel>();
                if (user.PasswordPolicies != null && user.PasswordPolicies.Any())
                {
                    var tenantInferredUserGroups = user.InferredUserGroups.Where(ug => ug.TenantId == tenantId.Value);
                    foreach (var passwordPolicy in user.PasswordPolicies.Where(pp => tenantInferredUserGroups.Any(ug => ug.GetIdentifier().IsEquals(pp.UserGroup.GetIdentifier()))).Select(ppug => ppug.PasswordPolicy))
                    {
                        var model = _PasswordPolicyServiceProvider.GetViewModel(passwordPolicy);
                        viewModel.PasswordPolicies.Add(model);
                    }
                }
                viewModel.Sessions = new List<UserTenantSessionModel>();
                if(userTenant.UserTenantSessions != null && userTenant.UserTenantSessions.Any())
                {
                    foreach (var userTenantSession in userTenant.UserTenantSessions)
                    {
                        var userTenantSessionModel = _UserTenantSessionServiceProvider.GetViewModel(userTenantSession);
                        if (userTenantSessionModel != null)
                        {
                            viewModel.Sessions.Add(userTenantSessionModel);
                        }
                    }
                }
            }
            else
            {
                viewModel.Identifier = new UserIdentifier(user.ObjectIdentifier);
                viewModel.IsTenantSpecific = false;
            }
            viewModel.TenantUsername = user.TenantUsername;
            viewModel.AllowedModules = user.AllowedModulesCodes;
            viewModel.UserIdentifier = user.ObjectIdentifier;
            viewModel.Enabled = !user.LockoutEnabled; ;
            viewModel.IsNewPasswordRequired = user.NewPasswordRequired;
            viewModel.AllClaims = user.BasicClaims;
            viewModel.IsLoggedInUser = _HttpContextAccessor.IsSameAs(viewModel.Identifier);
            viewModel.IsSuperUser = viewModel.AllowedModules.IsSuperUser();
            viewModel.IsAdmin = viewModel.AllowedModules.IsAdmin();
            viewModel.IsTemporaryUser = user.IsTemporaryUser;


            if (user.UserTenants != null)
            {
                foreach (var userTenantItem in user.UserTenants)
                {
                    viewModel.UserTenants.Add(userTenantItem.Tenant.GetIdentifier());
                }
            }

            return viewModel;
        }


        public async Task<IList<UserViewModel>> GetListAsync(bool includeTemporaryUsers = true)
        {
            var tenants = await _TenantServiceProvider.GetListAsync();
            var appUsers = await _UserDataProvider.GetListAsync(includeTemporaryUsers);
            List<UserViewModel> models = new List<UserViewModel>();
            foreach (var tenant in tenants)
            {
                foreach (var appUser in appUsers.Where(au => au.UserTenants.Any(ut => ut.TenantId == tenant.Id)).FilterTemporaryUsers(includeTemporaryUsers))
                {
                    var model = GetUserViewModel(appUser, tenant.Id);
                    if (model != null)
                    {
                        models.Add(model);
                    }
                }
            }
            int? nullTenant = null;
            foreach (var appUser in appUsers.Where(au => au.UserTenants == null || !au.UserTenants.Any()).FilterTemporaryUsers(includeTemporaryUsers))
            {
                var model = GetUserViewModel(appUser, nullTenant);
                if (model != null)
                {
                    models.Add(model);
                }
            }
            return models;
        }

        public async Task<UserViewModel> GetLoggedInUser(bool getAccessDetail)
        {
            var appUser = await GetAsync(_HttpContextAccessor.LoggedInUserIdentifier(), false, true);
            var user = GetUserViewModel(appUser, _HttpContextAccessor.LoggedInUserIdentifier().GetTenantIdentifier());
            if (getAccessDetail)
            {
                var userAccessDetail = new UserAccessDetail();
                userAccessDetail.Projects = await _ProjectServiceProvider.GetAccessibleListAsync();
                userAccessDetail.Tenants = await _TenantServiceProvider.GetAccessibleListAsync();
                userAccessDetail.Users = new List<UserIdentifier>();
                var accessibleUsers = await GetAccessibleListAsync();
                userAccessDetail.Users = accessibleUsers.Select(au => au.Identifier).ToList();
                userAccessDetail.PasswordPolicyTypes = await _PasswordPolicyTypeServiceProvider.GetAccessibleListAsync();
                user.UserAccessDetail = userAccessDetail;
            }
            return user;
        }
        public async Task<IList<UserViewModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<AppUser> appUsers = await _UserDataProvider.GetListAsync(tenantIdentifier);
            return GetListViewModel(appUsers, tenantIdentifier);
        }

        public async Task<IList<UserViewModel>> GetAccessibleListAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true)
        {
            IList<UserViewModel> allUsers = null;
            IEnumerable<UserViewModel> viewableUsers = null;
            IList<UserViewModel> accessibleUsers = await GetAccessibleListAsync();
            if (tenantIdentifier == null)
            {
                allUsers = await GetListAsync();
                viewableUsers = allUsers.Where(u => accessibleUsers.Any(au => u.Identifier.IsEquals(au.Identifier)));
            }
            else
            {
                allUsers = await GetListAsync(tenantIdentifier);
                viewableUsers = allUsers.Common(accessibleUsers, u => u.RepositoryUsername);
            }


            return viewableUsers
                .Where(u => includeTemporaryUsers || !u.IsTemporaryUser)
                .ToList();
        }

        private IList<UserViewModel> GetListViewModel(IList<AppUser> appUsers, TenantIdentifier tenantIdentifier)
        {
            IList<UserViewModel> list = new List<UserViewModel>();
            var tenantId = AsyncHelpers.RunSync(() => _TenantServiceProvider.GetAsync(tenantIdentifier))?.Id;
            foreach (var appUser in appUsers)
            {
                var model = GetUserViewModel(appUser, tenantId);
                if (model != null)
                {
                    list.Add(model);
                }
            }
            return list;
        }

        public async Task<int> DeleteAsync(UserIdentifier userIdentifier)
        {
            // check if the user is the current logged in user
            // a user cannot delete itself
            if (userIdentifier.IsEquals(_HttpContextAccessor.LoggedInUserIdentifier()))
            {
                throw new IdPNotFoundException(ErrorCode.USER_CANNOT_BE_DELETED_SAME, "User cannot delete itself");
            }
            return await _UserDataProvider.DeleteAsync(userIdentifier);
        }

        public async Task<IList<UserViewModel>> GetAccessibleListAsync(bool includeTemporaryUsers = true)
        {
            if (_HttpContextAccessor.IsAdmin() || _HttpContextAccessor.HasAnyAllowedModules(SupportConstants.SupportModuleCode, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN.ToString()) || _HttpContextAccessor.IsAdmin())
            {
                // get everyone
                return await GetListAsync();
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN))
            {
                // get the ones which the user has access to as a project admin
                var projects = await _ProjectServiceProvider.GetAccessibleListAsync();
                List<UserViewModel> users = new List<UserViewModel>();
                foreach (var project in projects)
                {
                    foreach (var tenant in project.Tenants)
                    {
                        var us = await GetAccessibleListAsync(tenant.Identifier);
                        if (us != null && us.Any())
                        {
                            users.AddRange(us);
                        }
                    }
                }
                return users;
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN))
            {
                // user is not a super user or a project admin but has tenant admin access
                // so check which are given access and get their users
                var tenants = await _TenantServiceProvider.GetAccessibleListAsync();
                List<UserViewModel> users = new List<UserViewModel>();
                foreach (var tenantIdentifier in tenants.Select(t => t.Identifier))
                {
                    var tenantUsers = await GetListAsync(tenantIdentifier);
                    users.AddRange(tenantUsers);
                }
                return users;
            }
            else if (_HttpContextAccessor.HasAllowedModule(ModuleCode.REPOSITORY_USER_MANAGEMENT))
            {
                // user can manage themself and all the users in the groups which they are assigned
                var loggedInUser = await GetLoggedInUser(false);
                List<UserViewModel> users = new List<UserViewModel>();
                users.Add(loggedInUser);
                foreach (var userGroupCode in loggedInUser.ManagedUserGroups)
                {
                    UserGroup userGroup = await _UserGroupServiceProvider.GetAsync(new UserGroupIdentifier(userGroupCode, _HttpContextAccessor.TenantCode(), _HttpContextAccessor.ProjectCode()));
                    List<UserGroup> userGroups = await _UserGroupServiceProvider.GetUserGroupsIncludedAsync(userGroup);
                    var appUsers = userGroups.SelectMany(ug => ug.ApplicationUserGroups).Select(aug => aug.UserTenant.User);
                    var tenant = await _TenantServiceProvider.GetAsync(userGroup.GetIdentifier().GetTenantIdentifier());
                    foreach (var appUser in appUsers)
                    {
                        var appUserDetail = await GetAsync(appUser.Id);
                        var user = GetUserViewModel(appUserDetail, tenant?.Id);
                        if (user != null && !user.Identifier.IsEquals(loggedInUser.Identifier) && !users.Any(u => u.Identifier.IsEquals(user.Identifier)))
                        {
                            users.Add(user);
                        }
                    }
                }
                return users;
            }
            else
            {
                // user can only manage their own data
                return new List<UserViewModel>() { await GetLoggedInUser(false) };
            }
        }

        public async Task<AppUser> GetAsync(UserIdentifier request, bool setAsLoggedInUser, bool getModules)
        {
            return await GetUserAsync(request, setAsLoggedInUser, getModules, null);
        }

        public async Task<UserIdentifier> CreateAsync(UserRegisterViewModel request)
        {
            var userGroups = request.UserGroups?.ToArray() ?? new string[0];
            if (!await _UserGroupServiceProvider.ExistsAsync(request.TenantIdentifier, userGroups))
            {
                _logger.LogError("Not all usergroups were found.");
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND);
            }

            string errors = "";
            string displayError = "Something went wrong";
            // check if a user exists
            AppUser user = null;

            if (await ExistsAsync(request.GetUserIdentifier()))
            {
                displayError = "Username " + request.GetUserIdentifier() + " already exists.";
                errors = "Username " + request.GetUserIdentifier() + " was found in the system.";
                _logger.LogError(errors);
                throw new IdPException(ErrorCode.USERNAME_EXISTS);
            }

            // get tenant
            var tenant = await _TenantServiceProvider.GetAsync(request.TenantIdentifier);
            if (tenant == null)
            {
                displayError = "Tenant " + request.TenantIdentifier + " not found.";
                errors = "Tenant " + request.TenantIdentifier + " was not found in the system.";
                _logger.LogError(errors);
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, request.TenantIdentifier.ToString());
            }

            // create user
            user = await _UserDataProvider.CreateAsync(request);

            // relate user to tenant
            var userTenantCreateModel = new UserTenantCreateModel();
            userTenantCreateModel.TenantIdentifier = request.TenantIdentifier;
            userTenantCreateModel.UserId = user.Id;
            await _UserTenantDataProvider.CreateAsync(userTenantCreateModel);

            // upload file

            if (!string.IsNullOrWhiteSpace(request.ProfileImage))
            {
                try
                {
                    byte[] profileImage = HelpersService.ConvertFromBase64ToBytes(request.ProfileImage.CleanBase64());
                    await UploadProfilePictureAsync(request.GetUserIdentifier(), profileImage);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Couldn't upload the given profile image on create.");
                }
            }

            if (!request.IsTemporaryUser)
            {
                // create hash
                string passwordHash = HashPassword(user, request.Password);

                // set password
                user = await _UserDataProvider.SetPasswordHashAsync(request.GetUserIdentifier(), passwordHash);
            }

            await _UserGroupServiceProvider.AddUserToGroups(request.GetUserIdentifier(), userGroups);

            // get newly created user
            user = await GetAsync(user.Id);

            //user.RemoveCyclicReferences();
            _logger.LogDebug("User created for tenant : " + user.TenantCode);

            return request.GetUserIdentifier();

        }

        private string HashPassword(AppUser user, string password)
        {
            return new PasswordHasher().HashPassword(user, password);
        }

        public async Task<AppUser> GetUserAsync(UserIdentifier userIdentifier, bool setAsLoggedInUser, bool getModules, UserRegisterViewModel registerModel)
        {
            AppUser user = null;
            try
            {
                user = await GetAsync(userIdentifier);
            }
            catch (IdPNotFoundException ex)
            {
                if (ex.ErrorCode == ErrorCode.USER_NOT_FOUND)
                {
                    _logger.LogDebug("User wasn't found. This can happen if its a new user.");
                }
                else
                {
                    throw;
                }
            }
            UserViewModel result = null;

            // if the user wasn't found then we try to register it if we are told to
            if (user == null && registerModel != null)
            {
                // create user
                _logger.LogDebug($"ApplicationUser not found. Creating for '{userIdentifier.Username}' '{userIdentifier.TenantCode}' '{userIdentifier.ProjectCode}'");
                userIdentifier = await CreateAsync(registerModel);
                if (userIdentifier == null)
                {
                    throw new IdPException(ErrorCode.USER_CREATION_ERROR, "The registration on update did not return the expected identifier");
                }

                user = await GetAsync(userIdentifier);

                if (user == null)
                {
                    throw new IdPException(ErrorCode.GENERIC_ERROR, "The newly created user was attempted to be retrieved but it wasn't found.");
                }
            }
            else if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

            var tenantIdentifier = userIdentifier.GetTenantIdentifier();

            if (getModules && tenantIdentifier != null)
            {
                var tenant = await _TenantServiceProvider.GetAsync(tenantIdentifier);
                var userTenant = user.UserTenants.FirstOrDefault(ut => ut.TenantId == tenant.Id);
                List<UserGroup> userGroups = await _UserGroupServiceProvider.GetInferredUserGroupsAsync(user, tenantIdentifier);

                user.InferredUserGroups = userGroups;

                var roles = await _RoleServiceProvider.GetRolesRecursivelyAsync(user, tenantIdentifier);
                var modules = await _ModuleServiceProvider.GetModulesForRolesAsync(roles);
                user.AllowedModulesCodes = modules.Select(m => m.Code).Distinct().ToList();

                // anyone who has been appointed some users to manage will see this tab
                if ((userTenant.UserGroupManagement != null && userTenant.UserGroupManagement.Any()) || user.AllowedModulesCodes.IsAdmin())
                {
                    if (!user.AllowedModulesCodes.Any(m => m == ModuleCode.REPOSITORY_USER_MANAGEMENT.ToString()))
                    {
                        user.AllowedModulesCodes.Add(ModuleCode.REPOSITORY_USER_MANAGEMENT.ToString());
                    }
                }

                user.BasicClaims = new List<BasicClaim>();
                user.Roles = roles.Select(r => r.Code).ToList();

                // modules
                foreach (var value in modules.Where(m => m.Code != null && !m.Code.Trim().Equals(String.Empty)))
                {
                    user.BasicClaims.Add(new BasicClaim(ClaimName.Module, value.Code));
                }

                // user groups
                user.PasswordPolicies = new List<PasswordPolicyUserGroup>();
                foreach (var value in userGroups.Where(ug => ug.Code != null && !ug.Code.Trim().Equals(String.Empty)))
                {
                    user.BasicClaims.Add(new BasicClaim(ClaimName.UserGroup, value.Code));
                    if (value.UserGroupClaims != null)
                    {
                        foreach (var claim in value.UserGroupClaims)
                        {
                            user.BasicClaims.Add(new BasicClaim(claim.UserGroupClaimType.Type, claim.Value));
                        }
                    }

                    if (value.PasswordPolicyUserGroup != null && value.PasswordPolicyUserGroup.Any())
                    {
                        foreach (var passwordPolicyUserGroup in value.PasswordPolicyUserGroup)
                        {
                            if (!user.PasswordPolicies.Any(pp => pp.GetIdentifier().IsEquals(passwordPolicyUserGroup.GetIdentifier())))
                            {
                                user.PasswordPolicies.Add(passwordPolicyUserGroup);
                            }
                        }
                    }
                }

                // roles
                foreach (var value in roles.Where(r => r.Code != null && !r.Code.Trim().Equals(String.Empty)))
                {
                    user.BasicClaims.Add(new BasicClaim(ClaimName.Role, value.Code));
                    if (value.RoleClaims != null)
                    {
                        foreach (var claim in value.RoleClaims)
                        {
                            user.BasicClaims.Add(new BasicClaim(claim.RoleClaimType.Type, claim.Value));
                        }
                    }
                }

                // claims assigned at user level
                if (userTenant.UserClaims != null)
                {
                    foreach (var claim in userTenant.UserClaims)
                    {
                        user.BasicClaims.Add(new BasicClaim(claim.UserClaimType.Type, claim.Value));
                    }
                }

                // claims assigned at tenant level
                if (tenant.BasicClaims != null)
                {
                    foreach (var claim in tenant.BasicClaims)
                    {
                        user.BasicClaims.Add(claim);
                    }
                }

            }

            // place the user in the thread or httpcontext
            if (setAsLoggedInUser)
            {
                SetAsLoggedInUser(user, userIdentifier);
            }

            return user;
        }

        public ClaimsPrincipal SetAsLoggedInUser(AppUser user, UserIdentifier userIdentifier)
        {
            user.TenantCode = userIdentifier.TenantCode;
            user.ProjectCode = userIdentifier.ProjectCode;

            UserTenant appUserTenant = user.UserTenants.FirstOrDefault(ut => ut.Tenant.GetIdentifier().IsEquals(userIdentifier.GetTenantIdentifier()));
            if (appUserTenant == null)
            {
                throw new IdPException(ErrorCode.USERTENANT_NOT_FOUND, "In user:" + user.ObjectIdentifier + " " + userIdentifier.GetTenantIdentifier().ToString());
            }

            // we set the user as the current user
            var identity = new GenericIdentity(user.TenantUsername);

            if (_HttpContextAccessor.HttpContext?.User?.Identity != null)
            {
                identity.AddClaims(((ClaimsIdentity)_HttpContextAccessor.HttpContext.User.Identity).Claims);
            }

            // add internal claims if missing
            if (!identity.HasClaim(c => c.Type == "sub"))
            {
                identity.AddClaim(new Claim("sub", user.TenantUsername));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.TenantUsername));

                _HttpContextAccessor.HttpContext.SetUsername(user.TenantUsername);
            }
            if (!identity.HasClaim(c => c.Type == ClaimName.RepositoryUsername))
            {
                identity.AddClaim(new Claim(ClaimName.RepositoryUsername, appUserTenant.UserName));

                _HttpContextAccessor.HttpContext.SetRepositoryUsername(appUserTenant.UserName);
            }
            if (!identity.HasClaim(c => c.Type == ClaimName.Tenant))
            {
                identity.AddClaim(new Claim(ClaimName.Tenant, user.TenantCode));

                _HttpContextAccessor.HttpContext.SetTenantCode(user.TenantCode);
            }
            if (!identity.HasClaim(c => c.Type == ClaimName.Project))
            {
                identity.AddClaim(new Claim(ClaimName.Project, user.ProjectCode));

                _HttpContextAccessor.HttpContext.SetProjectCode(user.ProjectCode);
            }
            if (!identity.HasClaim(c => c.Type == ClaimName.UserGuid))
            {
                identity.AddClaim(new Claim(ClaimName.UserGuid, user.ObjectIdentifier));

                _HttpContextAccessor.HttpContext.SetUserGuid(user.ObjectIdentifier);
            }

            // add modules to claims
            if (user.AllowedModulesCodes != null)
            {
                foreach (var module in user.AllowedModulesCodes)
                {
                    if (!identity.HasClaim(c => c.Type == ClaimName.Module && c.Value == module))
                    {
                        identity.AddClaim(new Claim(ClaimName.Module, module));
                    }
                }

                _HttpContextAccessor.HttpContext.SetAllowedModules(user.AllowedModulesCodes.ToList());
            }

            // add any roles to claims
            if (user.Roles != null)
            {
                foreach (var roleCode in user.Roles)
                {
                    identity.AddClaim(new Claim(ClaimName.Role, roleCode));
                }

                _HttpContextAccessor.HttpContext.SetRoles(user.Roles.ToList());
            }

            // initialise the principal to store in the thread
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // done! return success
            _logger.LogDebug($"User '{appUserTenant.UserName}' from '{user.TenantCode}' '{user.ProjectCode}' given access");
            return principal;
        }

        public async Task<UserProfileModel> GetUserProfileAsync(string username = null, string tenantCode = null, string projectCode = null)
        {
            UserIdentifier userIdentifier = null;
            // check if all parameters were given and create an identifier if provided
            if (!(string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(tenantCode) || string.IsNullOrWhiteSpace(projectCode)))
            {
                userIdentifier = new UserIdentifier(username, tenantCode, projectCode);
            }
            else
            {
                // the flag is set so we get the profile of the logged in user
                userIdentifier = _HttpContextAccessor.LoggedInUserIdentifier();
            }

            var user = await GetAsync(userIdentifier, false, true);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }
            return GetUserProfileModel(user, userIdentifier);
        }

        public async Task<byte[]> GetUserProfileImageAsync(string username = null, string tenantCode = null, string projectCode = null)
        {
            UserIdentifier userIdentifier = null;
            // check if all parameters were given and create an identifier if provided
            if (!(string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(tenantCode) && string.IsNullOrWhiteSpace(projectCode)))
            {
                userIdentifier = new UserIdentifier(username, tenantCode, projectCode);

                // should we enforce accessibility to the user image?
            }
            else
            {                // the flag is set so we get the profile of the logged in user
                userIdentifier = _HttpContextAccessor.LoggedInUserIdentifier();
            }

            return await _UserDataProvider.GetProfileImageAsync(userIdentifier);
        }

        private UserProfileModel GetUserProfileModel(AppUser user, UserIdentifier userIdentifier)
        {
            var userViewModel = GetUserViewModel(user, userIdentifier.GetTenantIdentifier());
            if (userViewModel == null)
            {
                return null;
            }
            var profileModel = HelpersService.CopyProperties<UserProfileModel>(userViewModel);
            if (userViewModel.AllClaims != null)
            {
                profileModel.Properties = new List<BasicClaim>();
                foreach (var uc in userViewModel.AllClaims)
                {
                    profileModel.Properties.Add(new BasicClaim(uc.Type, uc.Value));
                }
            }
            profileModel.Roles = user.Roles;
            profileModel.AllowedModules = user.AllowedModulesCodes;
            profileModel.UserGroups = userViewModel.UserGroups;
            return profileModel;
        }
        public ClaimsPrincipal SetApiAdminAsLoggedInUser(TenantIdentifier tenantIdentifier)
        {
            // check if tenant exists
            bool exists = AsyncHelpers.RunSync<bool>(() => _TenantServiceProvider.ExistsAsync(tenantIdentifier, false));

            if (!exists)
            {
                return null;
            }
            var codes = AsyncHelpers.RunSync(() => _ModuleServiceProvider.GetListAsync(tenantIdentifier.GetProjectIdentifier()));
            return SetAsLoggedInUser(new AppUser()
            {
                TenantUsername = "api",
                UserTenants = new List<UserTenant>() {
                    new UserTenant(){
                        UserName= "api",
                        Tenant = new Tenant()
                        {
                            Code = tenantIdentifier.TenantCode,
                            Project = new Project()
                            {
                                Code = tenantIdentifier.ProjectCode
                            }
                        }
                    }
                },
                AllowedModulesCodes = codes.Select(m => m.Identifier.ModuleCode).ToList()
            }, new UserIdentifier()
            {
                Username = "api",
                TenantCode = tenantIdentifier.TenantCode,
                ProjectCode = tenantIdentifier.ProjectCode
            });
        }

        private async Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, byte[] data)
        {
            return await _UserDataProvider.UploadProfilePictureAsync(userIdentifier, data);
        }
    }
}
