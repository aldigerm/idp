﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface ITemporaryUserServiceProvider
    {
        Task<CreateTemporaryUserResponseModel> CreateTemporaryUserAsync(CreateTemporaryUserRequestModel model);
        Task<TemporaryUserViewModel> LoginAsync(OTPTemporaryUserLoginRequestModel model);
        Task<TemporaryUserViewModel> RequestDeliveryOTPValueDAsync(BaseTemporaryUserLoginRequestModel model);
        Task<TemporaryUserViewModel> SubmitOTPAsync(OTPTemporaryUserLoginRequestModel model);
    }
}
