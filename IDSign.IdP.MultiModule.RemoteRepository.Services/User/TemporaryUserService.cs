﻿using IDSign.IdP.Core;
using IDSign.IdP.Core.Extensions;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Constants;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class TemporaryUserService : ITemporaryUserServiceProvider
    {
        private readonly IUserTenantSessionServiceProvider _UserTenantSessionServiceProvider;
        private readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        private readonly IUserClaimServiceProvider _UserClaimServiceProvider;
        private readonly IUserClaimTypeServiceProvider _UserClaimTypeServiceProvider;
        private readonly IUserTenantSessionActionServiceProvider _UserTenantSessionActionServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly ILogger<TemporaryUserService> _logger;
        private readonly IOTPServiceProvider _OTPServiceProvider;
        public TemporaryUserService(
              IUserTenantSessionServiceProvider userTenantServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IUserClaimServiceProvider userClaimServiceProvider
            , IUserClaimTypeServiceProvider userClaimTypeServiceProvider
            , IUserTenantSessionActionServiceProvider userTenantSessionActionServiceProvider
            , IHttpContextAccessor httpContextAccessor
            , ILogger<TemporaryUserService> logger
            , IOTPServiceProvider oTPServiceProvider
            )
        {
            _UserTenantSessionServiceProvider = userTenantServiceProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _HttpContextAccessor = httpContextAccessor;
            _UserTenantSessionActionServiceProvider = userTenantSessionActionServiceProvider;
            _logger = logger;
            _UserClaimServiceProvider = userClaimServiceProvider;
            _UserClaimTypeServiceProvider = userClaimTypeServiceProvider;
            _OTPServiceProvider = oTPServiceProvider;
        }

        public async Task<CreateTemporaryUserResponseModel> CreateTemporaryUserAsync(CreateTemporaryUserRequestModel model)
        {
            var temporaryUserIdentifier = await _UserSharedServiceProvider.CreateAsync(new Model.DTO.UserRegisterViewModel()
            {
                Username = model.TenantCode + "_" + GenerateStringSuffix(),
                TenantIdentifier = model.GetTenantIdentifier(),
                FirstName = model.FirstName ?? "Test",
                LastName = model.LastName ?? "User",
                Title = model.Title?.ToTitleCase() ?? "",
                UserGroups = model.UserGroups,
                IsTemporaryUser = true,
                MobileNumber = model.MobileNumber,
                UseMobileOTP = model.SendOTP
            });

            if (model?.Properties.Any() == true)
            {
                // we create the types if they are missing
                var types = model.Properties.Select(p => p.Type).Distinct();
                foreach (var type in types)
                {
                    var userClaimTypeIdentifier = new UserClaimTypeIdentifier(type, model.GetProjectIdentifier());

                    // check if the type exists
                    if (await _UserClaimTypeServiceProvider.GetSimpleAsync(userClaimTypeIdentifier) == null)
                    {

                        _logger.LogDebug($"Creating type {type}");
                        // it doesn't so we create it
                        await _UserClaimTypeServiceProvider.CreateAsync(new UserClaimTypeCreateModel()
                        {
                            ProjectIdentifier = model.GetProjectIdentifier(),
                            Type = type,
                            Description = "Automatically created " + type
                        });
                    }
                }

                // we now create the properties
                foreach (var property in model.Properties)
                {
                    _logger.LogDebug($"Creating claim: '{property.Type}' -> '{property.Value}'");

                    await _UserClaimServiceProvider.CreateAsync(new UserClaimCreateModel()
                    {
                        TenantIdentifier = temporaryUserIdentifier.GetTenantIdentifier(),
                        Type = property.Type,
                        Value = property.Value,
                        Username = temporaryUserIdentifier.Username
                    });
                }
            }

            var temporaryUserTenantIdentifier = new UserTenantIdentifier(temporaryUserIdentifier);

            var temporarySessionInformation = await _UserTenantSessionServiceProvider.CreateAccessCodeForUserAsync(temporaryUserTenantIdentifier, model.ExpiryTimeoutInMinutes);

            var user = await _UserSharedServiceProvider.GetUserAsync(temporaryUserIdentifier, false, false, null);
            var userViewModel = _UserSharedServiceProvider.GetUserViewModel(user, temporaryUserTenantIdentifier.GetTenantIdentifier());
            var key = SerializeEasidModel(temporarySessionInformation.UserTenantSessionIdentifier, temporarySessionInformation.ExpiryDate.Value, userViewModel);

            return new CreateTemporaryUserResponseModel()
            {
                ExpiryDate = temporarySessionInformation.ExpiryDate,
                Key = key,
                Username = temporaryUserIdentifier.Username
            };
        }

        private async Task<UserTenantSessionModel> GetTemporaryAccessUserTenantSessionAsync(BaseTemporaryUserLoginRequestModel model)
        {
            var easidModel = DeserializeEasid(model?.ExternalAccessSessionId);

            if (string.IsNullOrWhiteSpace(easidModel?.AccessIdentifier?.SessionId))
            {
                throw new IdPException(ErrorCode.USER_SESSION_NOT_DESERIALIZABLE, $"Couldn't find sessionId in the following string='{model.ExternalAccessSessionId}'");
            }
            var userTenantSession = await _UserTenantSessionServiceProvider.GetAsync(easidModel.AccessIdentifier.SessionId);
            if (userTenantSession == null || !userTenantSession.UserTenantSessionTypeCode.EqualsIgnoreCase(UserTenantSessionTypes.AccessCode))
            {
                throw new IdPException(ErrorCode.USER_SESSION_NOT_FOUND, $"Session not found for '{easidModel.AccessIdentifier.SessionId}' {(userTenantSession?.UserTenantSessionTypeCode != null ? ("Type expected was '" + UserTenantSessionTypes.AccessCode + "' but actually is '" + userTenantSession.UserTenantSessionTypeCode + "'") : "")}");
            }
            return userTenantSession;
        }
        public async Task<TemporaryUserViewModel> LoginAsync(OTPTemporaryUserLoginRequestModel model)
        {
            var easidModel = DeserializeEasid(model?.ExternalAccessSessionId);
            var userTenantSession = await GetTemporaryAccessUserTenantSessionAsync(model);
            if (_UserTenantSessionActionServiceProvider.IsSessionExpired(userTenantSession))
            {
                _logger.LogWarning($"Session is expired {easidModel.AccessIdentifier.SessionId}");
                return BuildTemporaryUserViewModel(userTenantSession?.ExpiryDate, null, false, null, false, easidModel.TenantIdentifier.TenantCode, easidModel.TenantIdentifier.ProjectCode, null, IdpErrorCodes.USER_SESSION_NOT_FOUND_OR_EXPIRED);
            }
            else if (!easidModel.AccessIdentifier.ExpiryDateTime.Equals(userTenantSession.ExpiryDate))
            {
                _logger.LogWarning($"Session is not expired but the dates in the token and the one stored in the db do not match. SessionId='{userTenantSession.UserTenantSessionIdentifier}' Found='{easidModel.AccessIdentifier.ExpiryDateTime}' Expected='{userTenantSession.ExpiryDate}''");
                return BuildTemporaryUserViewModel(userTenantSession?.ExpiryDate, null, false, null, false, easidModel.TenantIdentifier.TenantCode, easidModel.TenantIdentifier.ProjectCode, null, IdpErrorCodes.USER_SESSION_NOT_FOUND_OR_EXPIRED);
            }
            else
            {
                var user = await _UserSharedServiceProvider.GetAsync(userTenantSession.UserTenantIdentifier?.GetUserIdentifier());
                if (user == null)
                {
                    _logger.LogError($"User for session wasn't found for user tenant session '{userTenantSession.UserTenantSessionIdentifier}' with '{userTenantSession.UserTenantIdentifier?.GetUserIdentifier()}'");
                    return null;
                }


                var userViewModel = _UserSharedServiceProvider.GetUserViewModel(user, userTenantSession.UserTenantIdentifier?.GetTenantIdentifier());

                bool otpSubmissionRequired = false;
                var otpValue = "";

                // check if we need to send an OTP
                if (user.TwoFactorEnabled)
                {
                    if (!string.IsNullOrWhiteSpace(model.OTP))
                    {
                        return await SubmitOTPAsync(model);
                    }
                    else
                    {
                        var activeOTP = GetActiveOTP(userViewModel.Sessions);
                        if (activeOTP == null)
                        {
                            _logger.LogDebug("No active OTP. A new one will be automatically generated.");
                            return await RequestDeliveryOTPValueDAsync(model);
                        }
                        else
                        {
                            _logger.LogDebug("An OTP is already being expected. No new ones will be sent.");
                            otpValue = activeOTP.Data;
                            otpSubmissionRequired = true;
                        }
                    }
                }
                return BuildTemporaryUserViewModel(userTenantSession.ExpiryDate, userViewModel.MobileNumber, otpSubmissionRequired, userViewModel, false, otpValue);
            }
        }

        public async Task<TemporaryUserViewModel> SubmitOTPAsync(OTPTemporaryUserLoginRequestModel model)
        {
            var easidModel = DeserializeEasid(model?.ExternalAccessSessionId);
            var userTenantSession = await GetTemporaryAccessUserTenantSessionAsync(model);
            var user = await _UserSharedServiceProvider.GetAsync(userTenantSession.UserTenantIdentifier?.GetUserIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_SESSION_RELATED_USER_NOT_FOUND_EXCEPTION, $"User for session wasn't found for user tenant session '{userTenantSession.UserTenantSessionIdentifier}' with '{userTenantSession.UserTenantIdentifier?.GetUserIdentifier()}'");
            }
            var userViewModel = _UserSharedServiceProvider.GetUserViewModel(user, userTenantSession.UserTenantIdentifier?.GetTenantIdentifier());

            var recipientPhoneNumber = GetUserOTPRecipientPhoneNumber(userViewModel);

            // we check if there is an OTP for this users 
            var otpSessions = userViewModel.Sessions.Where(s => s.UserTenantSessionTypeCode == UserTenantSessionTypes.OTP);

            _logger.LogDebug($"Easid has {otpSessions?.Count()} total active/incactive otp sessions.");

            var isOTPValid = false;
            var createNewOTP = false;
            var otpValue = "";
            if (!otpSessions?.Any() ?? true)
            {
                _logger.LogDebug($"There are no session. OTP cannot be valid.");
                createNewOTP = true;
            }
            else
            {
                var activeOTP = GetActiveOTP(otpSessions);

                if (activeOTP == null)
                {
                    createNewOTP = true;
                    _logger.LogDebug($"No OTP submitted and no active OTP exists. Sending one by default.");
                }
                else
                {
                    _logger.LogDebug($"OTP submitted '{model.OTP}'. Checking if there is a valid active OTP.");

                    otpValue = activeOTP.Data;
                    isOTPValid = _OTPServiceProvider.IsOTPMatch(user, otpValue, model.OTP);
                }
            }

            #region Create OTP

            if (createNewOTP)
            {
                return await RequestDeliveryOTPValueDAsync(model);
            }
            #endregion

            if (isOTPValid)
            {
                // user is validated
                // logging in
                _logger.LogDebug("OTP Validated successfully.");

                _logger.LogDebug("Deleting old OTPs...");
                var deleted = 0;
                foreach (var session in otpSessions)
                {
                    deleted += await _UserTenantSessionServiceProvider.DeleteAsync(session.UserTenantSessionIdentifier);
                }
                _logger.LogDebug($"Deleted {deleted} old OTP sessions.");

                return BuildTemporaryUserViewModel(userTenantSession.ExpiryDate, userViewModel.MobileNumber, false, userViewModel, false, otpValue);
            }
            else
            {
                return BuildTemporaryUserViewModel(userTenantSession.ExpiryDate, recipientPhoneNumber, true, userViewModel, false, otpValue, IdpErrorCodes.USER_NOT_AUTHENTICATED_INVALID_OTP_ERROR);
            }

        }

        private UserTenantSessionModel GetActiveOTP(IEnumerable<UserTenantSessionModel> sessions)
        {
            if (sessions == null || !sessions.Any())
            {
                _logger.LogDebug($"OTP Sessions passed is null or empty.");
                return null;
            }

            var otpSession = sessions?.Where(s => s != null && s.UserTenantSessionTypeCode == UserTenantSessionTypes.OTP).OrderByDescending(s => s.CreationDate).FirstOrDefault();

            _logger.LogDebug($"Validating {otpSession}");

            if (_UserTenantSessionActionServiceProvider.IsSessionExpired(otpSession))
            {
                _logger.LogDebug($"Last created OTP session is expired.");
                return null;

            }
            else if (string.IsNullOrWhiteSpace(otpSession?.Data))
            {
                _logger.LogDebug($"Last created OTP session has no OTP data");
                return null;
            }
            else
            {
                _logger.LogDebug($"Valid active OTP found is {otpSession}");
                return otpSession;
            }
        }

        private DateTimeOffset? GetNextAllowedOTPAttempt(IEnumerable<UserTenantSessionModel> sessions)
        {
            if (!sessions?.Any() ?? true)
            {
                return null;
            }
            var maxCount = Settings.OTPMaxCountInTimeFrame;
            var timeFrame = Settings.OTPTimeFrameInMinutes;
            var timeFrameStart = DateTimeOffset.Now.AddMinutes(-timeFrame);
            var otpSessions = sessions
                .Where(s => s != null && s.UserTenantSessionTypeCode == UserTenantSessionTypes.OTP);

            var otpSessionsInTimeFrame = otpSessions
                .Where(s => s.CreationDate > timeFrameStart);

            var totalSessionsSentInCurrentTimeFrame = otpSessionsInTimeFrame.Count();
            _logger.LogDebug($"There are {totalSessionsSentInCurrentTimeFrame} and the maximum number is {maxCount} from a total {otpSessions.Count()} created since {timeFrameStart}");

            var firstRequestOfSet = otpSessionsInTimeFrame.OrderBy(s => s.CreationDate).FirstOrDefault();

            return firstRequestOfSet != null && totalSessionsSentInCurrentTimeFrame > maxCount ? firstRequestOfSet.CreationDate.AddMinutes(timeFrame) : (DateTimeOffset?)null;
        }

        private string GetUserOTPRecipientPhoneNumber(UserViewModel userViewModel)
        {
            return userViewModel.MobileNumber;
        }

        public async Task<TemporaryUserViewModel> RequestDeliveryOTPValueDAsync(BaseTemporaryUserLoginRequestModel model)
        {
            var easidModel = DeserializeEasid(model?.ExternalAccessSessionId);
            var userTenantSession = await GetTemporaryAccessUserTenantSessionAsync(model);
            var projectIdentifier = userTenantSession.UserTenantIdentifier.GetTenantIdentifier().GetProjectIdentifier();

            var userViewModel = await _UserSharedServiceProvider.GetUserViewModelAsync(userTenantSession.UserTenantIdentifier?.GetUserIdentifier());
            // we check if there is an OTP for this users 
            var otpSessions = userViewModel.Sessions.OrderByDescending(s => s.ExpiryDate).Where(s => s.UserTenantSessionTypeCode == UserTenantSessionTypes.OTP);

            var recipientPhoneNumber = GetUserOTPRecipientPhoneNumber(userViewModel);

            bool success = false;

            var nextOTPRequestAllowedAt = GetNextAllowedOTPAttempt(userViewModel.Sessions);
            if (nextOTPRequestAllowedAt.HasValue)
            {
                var activeOTP = GetActiveOTP(userViewModel.Sessions);
                return BuildTemporaryUserViewModel(userTenantSession.ExpiryDate, recipientPhoneNumber, true, userViewModel, true, activeOTP?.Data, IdpErrorCodes.OTP_REQUEST_THRESHOLD_REACHED_ERROR);
            }

            var smsOTPValue = _OTPServiceProvider.GenerateOTP();

            var title = userViewModel.Title;

            try
            {
                success = _OTPServiceProvider.Send(userTenantSession.TenantName, recipientPhoneNumber, smsOTPValue, userViewModel.Title?.ToTitleCase() + " " + userViewModel.FirstName + " " + userViewModel.MiddleName + " " + userViewModel.LastName);
            }
            catch (Exception e)
            {
                _logger.LogError($"OTP threw an exception while attempted to send: {e.Message}");
            }

            if (success || Settings.Debug)
            {
                if (!success)
                {
                    _logger.LogWarning("Sms failed to send but the Debug setting allows to continue. Saving data in DB.");
                }
                else
                {
                    _logger.LogDebug("Sms sent successfully. Saving data in DB.");
                }

                var storedDBotpValue = smsOTPValue;
                if (Settings.OTPEncryptionEnabled)
                {
                    _logger.LogDebug(Settings.Debug ? $"Encrypting the OTP value is '{smsOTPValue}'" : $"Encrypting the OTP starting with the character '{storedDBotpValue[0]}'");
                    var user = await _UserSharedServiceProvider.GetAsync(userTenantSession.UserTenantIdentifier.GetUserIdentifier());
                    storedDBotpValue = EncryptionHelper.EncryptValue(user, smsOTPValue);
                }

                UserTenantSessionModel otpSession = await _UserTenantSessionServiceProvider.CreateOTPSessionForUserAsync(userTenantSession.UserTenantIdentifier, storedDBotpValue);
                return BuildTemporaryUserViewModel(otpSession.ExpiryDate, recipientPhoneNumber, true, userViewModel, true, smsOTPValue, success ? null : IdpErrorCodes.OTP_FAILED_TO_SEND_ERROR);
            }
            else
            {
                return BuildTemporaryUserViewModel(null, recipientPhoneNumber, true, userViewModel, false, null, IdpErrorCodes.OTP_FAILED_TO_SEND_ERROR);
            }
        }

        private TemporaryUserViewModel BuildTemporaryUserViewModel(DateTimeOffset? expiryDateTimeOffset, string recipientPhoneNumber, bool requireOTP, UserViewModel userViewModel, bool createdOTP, string expectedOTP, SessionError error = null)
        {
            return BuildTemporaryUserViewModel(expiryDateTimeOffset, recipientPhoneNumber, requireOTP, userViewModel, createdOTP, expectedOTP, userViewModel.Identifier.TenantCode, userViewModel.Identifier.ProjectCode, error);
        }

        private TemporaryUserViewModel BuildTemporaryUserViewModel(DateTimeOffset? expiryDateTimeOffset, string recipientPhoneNumber, bool requireOTP, UserViewModel userViewModel, bool createdOTP, string expectedOTP, string tenantCode, string projectCode, SessionError error = null)
        {
            return new TemporaryUserViewModel()
            {
                User = requireOTP ? null : userViewModel,
                PhoneNumber = recipientPhoneNumber,
                RequireOTP = requireOTP,
                ExpiryDateTime = expiryDateTimeOffset,
                NextOTPRequestAllowedAt = GetNextAllowedOTPAttempt(userViewModel?.Sessions),
                ProjectCode = projectCode,
                TenantCode = tenantCode,
                Error = error,
                CreatedOTP = createdOTP,
                // we are assuming that anything less than 10 characters is in fact no encrypted
                ExpectedOTP = Settings.OTPReturnIfNotEncrypted && expectedOTP?.Length < 10 ? expectedOTP : null
            };
        }

        private string SerializeEasidModel(string sessionId, DateTimeOffset expiryDateTime, UserViewModel userViewModel)
        {

            var identifierModel = new ExternalAccessSessionIdModel()
            {
                ExpiryDateTime = expiryDateTime,
                SessionId = sessionId
            };

            var tenantIdentifier = userViewModel.Identifier.GetTenantIdentifier();

            var model = new ExternalAccessModel()
            {
                AccessIdentifier = identifierModel,
                TenantIdentifier = tenantIdentifier
            };

            return HelpersService.ObjectToBase64String(model);
        }

        private ExternalAccessModel DeserializeEasid(string easid)
        {
            return HelpersService.Base64StringToObject<ExternalAccessModel>(easid);
        }

        private string GenerateStringSuffix(int length = 8)
        {
            // creating a StringBuilder object()
            StringBuilder str_build = new StringBuilder();
            Random random = new Random();

            char letter;

            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(25 * flt));
                letter = Convert.ToChar(shift + 65);
                str_build.Append(letter);
            }
            return str_build.ToString();
        }
    }
}
