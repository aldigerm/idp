﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserTenantSessionActionServiceProvider
    {
        Task<UserTenantSessionModel> GetAsync(string userTenantSessionIdentifier);
        Task<UserTenantSessionActionResponseModel> ActionAsync(UserTenantSessionActionRequestModel model);
        bool IsSessionExpired(UserTenantSessionBasicModel session);
    }
}
