﻿using IDSign.IdP.Core;
using IDSign.IdP.Model.Constants;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserTenantSessionActionService : IUserTenantSessionActionServiceProvider
    {
        protected readonly IUserTenantSessionServiceProvider _UserTenantSessionServiceProvider;
        protected readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        protected readonly IUserServiceProvider _UserServiceProvider;
        private ILogger<UserTenantSessionService> _logger;
        public UserTenantSessionActionService(
             ILogger<UserTenantSessionService> logger
            , IUserTenantSessionServiceProvider userTenantSessionServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IUserServiceProvider userServiceProvider
            )
        {
            _logger = logger;
            _UserTenantSessionServiceProvider = userTenantSessionServiceProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _UserServiceProvider = userServiceProvider;
        }

        public async Task<UserTenantSessionModel> GetAsync(string userTenantSessionIdentifier)
        {
            var session = await _UserTenantSessionServiceProvider.GetAsync(userTenantSessionIdentifier);
            if (session == null)
                throw new IdPNotFoundException(ErrorCode.USER_SESSION_NOT_FOUND, userTenantSessionIdentifier);
            var model = new UserTenantSessionModel();
            switch (session.UserTenantSessionTypeCode)
            {
                case UserTenantSessionTypes.ForceNewPassword:
                case UserTenantSessionTypes.ResetPassword:
                    model = await BuildPasswordDataAsync(session);
                    break;
                default:
                    throw new IdPException(ErrorCode.USER_SESSION_TYPE_CODE_NOT_SUPPORTED, session.UserTenantSessionTypeCode);
            }

            // we do not expose this information to the front
            model.UserTenantIdentifier = null;

            return model;
        }

        public bool IsSessionExpired(UserTenantSessionBasicModel session)
        {
            return (session == null || (session.ExpiryDate.HasValue && session.ExpiryDate.Value < DateTimeOffset.Now));
        }

        public async Task<UserTenantSessionActionResponseModel> ActionAsync(UserTenantSessionActionRequestModel model)
        {
            var session = await _UserTenantSessionServiceProvider.GetAsync(model.UserTenantSessionIdentifier);

            // if session wasn't found or the session has not expired
            if (IsSessionExpired(session))
            {
                throw new IdPException(ErrorCode.USER_SESSION_NOT_FOUND_OR_EXPIRED, model.UserTenantSessionIdentifier);
            }

            switch (session.UserTenantSessionTypeCode)
            {
                case UserTenantSessionTypes.AccessCode:
                    return await ActionAccessCodeAsync(session, model);
                case UserTenantSessionTypes.ForceNewPassword:
                case UserTenantSessionTypes.ResetPassword:
                    return await SetPasswordAsync(session, model);
                default:
                    throw new IdPException(ErrorCode.USER_SESSION_TYPE_CODE_NOT_SUPPORTED, session.UserTenantSessionTypeCode);
            }
        }

        private async Task<UserTenantSessionActionResponseModel> ActionAccessCodeAsync(UserTenantSessionModel session, UserTenantSessionActionRequestModel request)
        {
            var userIdentifier = session.UserTenantIdentifier.GetUserIdentifier();
            var user = await _UserSharedServiceProvider.GetAsync(userIdentifier, false, true);
            var model = new UserTenantSessionActionResponseModel();
            model.Data = HelpersService.DescribeObject(user);
            return model;
        }

        private async Task<UserTenantSessionModel> BuildPasswordDataAsync(UserTenantSessionModel model)
        {
            var userIdentifier = model.UserTenantIdentifier.GetUserIdentifier();
            var user = await _UserSharedServiceProvider.GetUserViewModelAsync(userIdentifier);
            model.Data = HelpersService.DescribeObject(user.PasswordPolicies);
            return model;
        }

        private async Task<UserTenantSessionActionResponseModel> SetPasswordAsync(UserTenantSessionModel session, UserTenantSessionActionRequestModel request)
        {
            var passwordModel = HelpersService.DeserializeObject<UserTenantSessionActionPasswordRequestModel>(request.Data);
            var userIdentifier = session.UserTenantIdentifier.GetUserIdentifier();
            var updatPasswordRequest = new UpdatePasswordViewModel()
            {
                Identifier = userIdentifier,
                Password = passwordModel?.Password,
                IsLoginReset = true
            };
            var passwordResult = await _UserServiceProvider.UpdatePasswordAsync(updatPasswordRequest);
            if (passwordResult != null)
            {
                _logger.LogDebug($"Reset password done successfully. Removing the session {session.UserTenantSessionIdentifier}");
                await _UserTenantSessionServiceProvider.DeleteAsync(session.UserTenantSessionIdentifier);
            }
            return new UserTenantSessionActionResponseModel();
        }
    }
}
