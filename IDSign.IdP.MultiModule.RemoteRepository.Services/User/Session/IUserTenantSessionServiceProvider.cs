﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserTenantSessionServiceProvider
    {
        Task<IList<UserTenantSessionModel>> GetListAsync(UserTenantIdentifier userTenantIdentifier);
        Task<string> CreateAsync(UserTenantSessionModel model);
        Task<string> UpdateAsync(UserTenantSessionModel model);
        Task<int> DeleteAsync(string identifier);
        Task<UserTenantSessionModel> GetAsync(string userTenantSessionIdentifier);
        UserTenantSessionModel GetViewModel(UserTenantSession userTenantSession);
        Task<UserTenantSessionModel> CreateForceNewPasswordSessionForUserAsync(UserTenantIdentifier userTenantIdentifier);
        Task<UserTenantSessionModel> CreateAccessCodeForUserAsync(UserTenantIdentifier userTenantIdentifier, int expiryInMinutes);
        Task<UserTenantSessionModel> CreateOTPSessionForUserAsync(UserTenantIdentifier userTenantIdentifier, string otpValue);
    }
}
