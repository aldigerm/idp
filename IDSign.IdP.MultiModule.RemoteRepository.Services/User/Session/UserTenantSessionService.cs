﻿using IDSign.IdP.Model.Constants;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserTenantSessionService : IUserTenantSessionServiceProvider
    {
        
        protected readonly IUserTenantSessionDataProvider _UserTenantSessionDataProvider;
        private ILogger<UserTenantSessionService> _logger;
        public UserTenantSessionService(
              ILogger<UserTenantSessionService> logger
            , IUserTenantSessionDataProvider userTenantSessionDataProvider
            )
        {
            _logger = logger;
            _UserTenantSessionDataProvider = userTenantSessionDataProvider;
        }

        public async Task<string> CreateAsync(UserTenantSessionModel model)
        {
            if (model == null)
                throw new IdPException(ErrorCode.INCORRECT_FORM, "Model is null");

            // setting the identifier which the public user would see
            model.UserTenantSessionIdentifier = Guid.NewGuid().ToString();

            var entity = await _UserTenantSessionDataProvider.CreateAsync(model);

            if (entity == null)
                return null;

            return model.UserTenantSessionIdentifier;
        }


        public async Task<IList<UserTenantSessionModel>> GetListAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var entityList = await _UserTenantSessionDataProvider.GetListAsync(userTenantIdentifier);
            var list = new List<UserTenantSessionModel>();
            foreach (var entity in entityList)
            {
                var model = GetViewModel(entity);
                if (model != null)
                {
                    model.UserTenantIdentifier = userTenantIdentifier;
                    list.Add(model);
                }
            }
            return list;
        }

        public async Task<UserTenantSessionModel> GetAsync(string userTenantSessionIdentifier)
        {
            var entity = await _UserTenantSessionDataProvider.GetAsync(userTenantSessionIdentifier);
            if (entity == null)
                throw new IdPNotFoundException(ErrorCode.USER_SESSION_NOT_FOUND, userTenantSessionIdentifier);

            return GetViewModel(entity);
        }

        public async Task<UserTenantSessionModel> CreateOTPSessionForUserAsync(UserTenantIdentifier userTenantIdentifier, string otpValue)
        {
            var createNewLoginRequest = new UserTenantSessionModel();
            createNewLoginRequest.ExpiryDate = DateTimeOffset.Now.AddMinutes(Settings.OTPExpiryMinutes);
            createNewLoginRequest.UserTenantIdentifier = userTenantIdentifier;
            createNewLoginRequest.UserTenantSessionTypeCode = UserTenantSessionTypes.OTP;
            createNewLoginRequest.Data = otpValue;
            var sessionId = await CreateAsync(createNewLoginRequest);
            return await GetAsync(sessionId);
        }
        public async Task<UserTenantSessionModel> CreateForceNewPasswordSessionForUserAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var createNewLoginRequest = new UserTenantSessionModel();
            createNewLoginRequest.ExpiryDate = DateTimeOffset.Now.AddMinutes(Settings.ForcedNewPasswordExpiryMinutes);
            createNewLoginRequest.UserTenantIdentifier = userTenantIdentifier;
            createNewLoginRequest.UserTenantSessionTypeCode = UserTenantSessionTypes.ForceNewPassword;
            var sessionId = await CreateAsync(createNewLoginRequest);
            return await GetAsync(sessionId);
        }

        public async Task<UserTenantSessionModel> CreateAccessCodeForUserAsync(UserTenantIdentifier userTenantIdentifier, int expiryInMinutes)
        {
            var createNewLoginRequest = new UserTenantSessionModel();
            createNewLoginRequest.ExpiryDate = DateTimeOffset.Now.AddMinutes(expiryInMinutes);
            createNewLoginRequest.UserTenantIdentifier = userTenantIdentifier;
            createNewLoginRequest.UserTenantSessionTypeCode = UserTenantSessionTypes.AccessCode;
            var sessionId = await CreateAsync(createNewLoginRequest);
            return await GetAsync(sessionId);
        }


        public async Task<string> UpdateAsync(UserTenantSessionModel model)
        {
            return await _UserTenantSessionDataProvider.UpdateAsync(model);
        }
        public async Task<int> DeleteAsync(string identifier)
        {
            return await _UserTenantSessionDataProvider.DeleteAsync(identifier);
        }

        public UserTenantSessionModel GetViewModel(UserTenantSession userTenantSession)
        {
            if (userTenantSession == null)
                return null;

            var model = new UserTenantSessionModel();
            model.ExpiryDate = userTenantSession.ExpiryDate;
            model.UserTenantSessionIdentifier = userTenantSession.Identifier;
            model.UserTenantSessionTypeCode = userTenantSession.UserTenantSessionType.Code;
            model.UserTenantIdentifier = userTenantSession.UserTenant?.GetIdentifier();
            model.TenantName = userTenantSession.UserTenant.Tenant.Name;
            model.CreationDate = userTenantSession.CreationDate;
            model.Data = userTenantSession.Data;
            return model;
        }

        public Task<UserTenantSessionActionResponseModel> ActionAsync(UserTenantSessionActionRequestModel model)
        {
            throw new NotImplementedException();
        }
    }
}
