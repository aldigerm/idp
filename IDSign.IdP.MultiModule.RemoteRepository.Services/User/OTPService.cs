﻿using IDSign.IdP.MultiModule.RemoteRepository.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.Extensions.Logging;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using SMSApi.Api;
using IDSign.IdP.Core;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class OTPService : IOTPServiceProvider
    {
        protected readonly ILogger<OTPService> _logger;
        public OTPService(
            ILogger<OTPService> logger
            )
        {
            _logger = logger;
        }

        public string GenerateOTP()
        {
            var rnd = new Random();
            return rnd.Next(100000, 999999).ToString();
        }

        public bool IsOTPMatch(AppUser user, string storedValue, string submittedValue)
        {
            bool valid = false;
            if (Settings.OTPEncryptionEnabled)
            {
                _logger.LogDebug($"Comparing OTP '{submittedValue}' with '{storedValue}' with encryption check");
                valid = EncryptionHelper.VerifyEncryptedValue(user, storedValue, submittedValue);
            }
            else
            {
                _logger.LogDebug($"Comparing OTP '{submittedValue}' directly with '{storedValue}'");
                valid = storedValue.EqualsTrimIgnoreCase(submittedValue);
            }
            if (valid)
            {
                _logger.LogDebug($"OTP '{submittedValue}' matched with '{storedValue}'");
            }
            else
            {
                _logger.LogError($"OTP '{submittedValue}' failed to match with '{storedValue}'");
            }
            return valid;
        }

        public bool Send(string companyName, string phoneNumber, string otpValue, string recipientFullName = null)
        {
            if (FeatureHelper.EnableOTPSms)
            {


                try
                {
                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        throw new IdPException(ErrorCode.OTP_FIELDS_PROVIDED_INVALID_EXCEPTION, "The phone number was not provided");
                    }
                    if (string.IsNullOrWhiteSpace(otpValue))
                    {
                        throw new IdPException(ErrorCode.OTP_FIELDS_PROVIDED_INVALID_EXCEPTION, "The otp value was not provided");
                    }

                    companyName = companyName?.Trim() ?? "";
                    phoneNumber = phoneNumber.Replace(" ", "")
                        .Replace("+", "00")
                        .Trim();

                    var message = OTPServiceConstants.SmsMessage
                        .Replace("{{value}}", otpValue)
                        .Replace("{{recipientFullName}}", string.IsNullOrWhiteSpace(recipientFullName) ? "" : " " + recipientFullName.Trim())
                        .Replace("{{companyName}}", companyName)
                        .Trim();

                    var senderName = OTPServiceConstants.SmsSender
                        .Replace("{{companyName}}", companyName)
                        .Trim();

                    SMSApi.Api.IClient client = new SMSApi.Api.ClientOAuth(OTPServiceConstants.Token);

                    //for SMSAPI.com clients:
                    var smsApi = new SMSApi.Api.SMSFactory(client, ProxyAddress.SmsApiCom);

                    _logger.LogDebug($"Sending OTP: \nphone number:'{phoneNumber}' \nmessage:'{message}' \nfrom sender:'{senderName}'");

                    var result =
                        smsApi.ActionSend()
                            .SetText(message)
                            .SetTo(phoneNumber)
                            .SetSender(senderName) //Sender name
                            .SetIDx(Guid.NewGuid().ToString())
                            .Execute();

                    _logger.LogDebug("Send: " + result.Count);

                }
                catch (SMSApi.Api.ActionException e)
                {
                    /**
                     * Action error
                     */
                    throw new IdPException(ErrorCode.OTP_EXTERNAL_API_EXCEPTION, e.Message);
                }
                catch (SMSApi.Api.ClientException e)
                {
                    /**
                     * Error codes (list available in smsapi docs). Example:
                     * 101 	Invalid authorization info
                     * 102 	Invalid username or password
                     * 103 	Insufficient credits on Your account
                     * 104 	No such template
                     * 105 	Wrong IP address (for IP filter turned on)
                     * 110	Action not allowed for your account
                     */
                    throw new IdPException(ErrorCode.OTP_EXTERNAL_API_EXCEPTION, e.Message);
                }
                catch (SMSApi.Api.HostException e)
                {
                    /* 
                     * Server errors
                     * SMSApi.Api.HostException.E_JSON_DECODE - problem with parsing data
                     */
                    throw new IdPException(ErrorCode.OTP_EXTERNAL_API_EXCEPTION, e.Message);
                }
                catch (SMSApi.Api.ProxyException e)
                {
                    // communication problem between client and sever 
                    throw new IdPException(ErrorCode.OTP_EXCEPTION, e.Message);
                }

                return true;
            }
            else
            {
                _logger.LogWarning($"Feature setting 'EnableOTPSms' is not enabled. Sms was not sent but marking itself as successful.");
                return true;
            }
        }
        public async Task VerifyAsync(AppUser user, string otpValue)
        {

        }
    }
}
