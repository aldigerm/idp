﻿using IDSign.IdP.Model.Constants;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserService : IUserServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IUserDataProvider _UserDataProvider;
        protected readonly IUserTenantDataProvider _UserTenantDataProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly IProjectServiceProvider _ProjectServiceProvider;
        protected readonly IModuleServiceProvider _ModuleServiceProvider;
        protected readonly IRoleServiceProvider _RoleServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserClaimDataProvider _UserClaimDataProvider;
        protected readonly IUserGroupManagementServiceProvider _UserGroupManagementServiceProvider;
        protected readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        protected readonly IPasswordPolicyServiceProvider _PasswordPolicyServiceProvider;
        protected readonly IUserPreviousPasswordServiceProvider _UserPreviousPasswordServiceProvider;
        protected readonly IUserTenantSessionServiceProvider _UserTenantSessionServiceProvider;

        public UserService(
              IUserDataProvider userDataProvider
            , ITenantServiceProvider tenantServiceProvider
            , IModuleServiceProvider moduleServiceProvider
            , IHttpContextAccessor httpContextAccessor
            , IProjectServiceProvider projectServiceProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , IRoleServiceProvider roleServiceProvider
            , ILogger<UserService> logger
            , IUserClaimDataProvider userClaimDataProvider
            , IUserGroupManagementServiceProvider userGroupManagementServiceProvider
            , IUserTenantDataProvider userTenantDataProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IPasswordPolicyServiceProvider passwordPolicyServiceProvider
            , IUserPreviousPasswordServiceProvider userPreviousPasswordServiceProvider
            , IUserTenantSessionServiceProvider userTenantSessionServiceProvider
            )
        {
            _logger = logger;
            _RoleServiceProvider = roleServiceProvider;
            _ModuleServiceProvider = moduleServiceProvider;
            _TenantServiceProvider = tenantServiceProvider;
            _UserDataProvider = userDataProvider;
            _ProjectServiceProvider = projectServiceProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
            _HttpContextAccessor = httpContextAccessor;
            _UserClaimDataProvider = userClaimDataProvider;
            _UserGroupManagementServiceProvider = userGroupManagementServiceProvider;
            _UserTenantDataProvider = userTenantDataProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _PasswordPolicyServiceProvider = passwordPolicyServiceProvider;
            _UserPreviousPasswordServiceProvider = userPreviousPasswordServiceProvider;
            _UserTenantSessionServiceProvider = userTenantSessionServiceProvider;
        }

        public async Task InitAsync()
        {
            var tenantIdentifier = Constants.GetInitialTenantIdentifier();
            await InitUser("partner_1", tenantIdentifier, new Dictionary<string, string>() { }, new string[] { InitialGroupCodes.Partner1GroupCode });
            await InitUser("partner_2", tenantIdentifier, new Dictionary<string, string>() { }, new string[] { InitialGroupCodes.Partner1GroupCode });
            await InitUser("partner_3", tenantIdentifier, new Dictionary<string, string>() { }, new string[] { InitialGroupCodes.Partner2GroupCode });
            await InitUser("partner_4", tenantIdentifier, new Dictionary<string, string>() { }, new string[] { InitialGroupCodes.Partner2GroupCode });
            await InitUser("psp_user_1", tenantIdentifier, null, new string[] { InitialGroupCodes.TenantPSP });
            await InitUser("risk_user_1", tenantIdentifier, null, new string[] { InitialGroupCodes.TenantRisk });
            await InitUser("bspay_admin", tenantIdentifier, null, new string[] { SupportConstants.TenantAdminUserGroupCode });
        }

        public async Task InitSupportAsync()
        {
            await InitUser(SupportConstants.SupportUsername, SupportConstants.GetSupportTenantIdentifier(), null, new string[] { SupportConstants.SupportUserGroupCode });
            foreach (var p in await _ProjectServiceProvider.GetListAsync())
            {
                foreach (var tenantIdentifier in p.Tenants.Select(t => t.Identifier))
                {
                    await InitUser(SupportConstants.SupportUsername, tenantIdentifier, null, new string[] { SupportConstants.SupportUserGroupCode });
                }
            }
        }

        private async Task InitUser(string username, TenantIdentifier tenantIdentifier, Dictionary<string, string> userclaims, string[] groupCodes)
        {
            var registerRequest = new UserRegisterViewModel()
            {
                Username = username,
                TenantIdentifier = tenantIdentifier,
                Password = username // password same as username
            };
            var user = await _UserSharedServiceProvider.GetUserAsync(registerRequest.GetUserIdentifier(), false, false, registerRequest);
            await _UserGroupServiceProvider.AddUserToGroups(registerRequest.GetUserIdentifier(), groupCodes);
            if (userclaims != null)
            {
                foreach (var keyvalue in userclaims)
                {
                    try
                    {
                        await _UserClaimDataProvider.CreateAsync(new UserClaimCreateModel()
                        {
                            TenantIdentifier = tenantIdentifier,
                            Type = keyvalue.Key,
                            Username = username,
                            Value = keyvalue.Value
                        });
                    }
                    catch (IdPException e)
                    {
                        if (!(e.ErrorCode == ErrorCode.USER_CLAIM_EXISTS))
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public async Task<UserIdentifier> UpdateAsync(UserUpdateViewModel request)
        {
            UserIdentifier userIdentifier = request.Identifier;

            if (await _UserSharedServiceProvider.ExistsAsync(userIdentifier))
            {
                try
                {
                    // a user cannot enabled/disable their own user
                    if (_HttpContextAccessor.IsSameAs(userIdentifier))
                    {
                        request.Enabled = null;
                    }

                    userIdentifier = await _UserDataProvider.UpdateAsync(request);

                    _logger.LogDebug("User updated.");
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "User update threw an error.");
                    throw new IdPException(ErrorCode.GENERIC_ERROR, e.Message);
                }

                return userIdentifier;
            }
            else
            {
                // user not found for updating
                string error = $"User does not exist. Username search for is '{request.Identifier}'";
                _logger.LogError(error);
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

        }

        public async Task<UserViewModel> UpdatePasswordAsync(UpdatePasswordViewModel request)
        {
            // check if a user exists

            UserIdentifier userIdentifier = request.Identifier;

            AppUser user = await _UserSharedServiceProvider.GetAsync(userIdentifier, false, true);
            var userViewModel = _UserSharedServiceProvider.GetUserViewModel(user, userIdentifier.GetTenantIdentifier());

            if (userViewModel != null)
            {


                var results = await ValidatePasswordAsync(request.Password, userViewModel.PasswordPolicies, userIdentifier);


                if (results.Any(p => !p.Passed))
                {
                    _logger.LogError("User password cannot be set as it failed validation.");
                    throw new IdPDescriptiveException(ErrorCode.USER_PASSWORD_FAILED_VALIDATION)
                            .SetObject(results);
                }

                try
                {

                    // generate the hash
                    string passwordHash = null;
                    if (!String.IsNullOrWhiteSpace(request.Password))
                    {
                        passwordHash = HashPassword(user, request.Password);
                        request.Password = passwordHash;
                    }

                    // set the password
                    user = await _UserDataProvider.UpdatePasswordAsync(request);

                    if (!string.IsNullOrWhiteSpace(user.PasswordHash))
                    {
                        // we add to the history the password just added
                        await _UserPreviousPasswordServiceProvider.CreateAsync(userIdentifier, passwordHash);
                    }

                    user = await _UserDataProvider.GetAsync(user.Id);

                    _logger.LogDebug("User updated.");
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "User update threw an error.");
                    throw new IdPException(ErrorCode.GENERIC_ERROR, e.Message);
                }

                return _UserSharedServiceProvider.GetUserViewModel(user, userIdentifier.GetTenantIdentifier());
            }
            else
            {
                // user not found for updating
                string error = $"User does not exist. Username search for is '{request.Identifier}'";
                _logger.LogError(error);
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

        }


        public async Task<IList<PasswordPolicyModel>> ValidatePasswordAsync(string password, IList<PasswordPolicyModel> passwordPolicies, UserIdentifier userIdentifier)
        {
            if (passwordPolicies == null)
                return new List<PasswordPolicyModel>();

            AppUser user = null;

            foreach (var passwordPolicy in passwordPolicies)
            {
                if (passwordPolicy.PasswordPolicyTypeIdentifier.PasswordPolicyTypeCode == PasswordPolicyTypes.RegexMatch)
                {
                    Match match = Regex.Match(password, passwordPolicy.Regex);
                    passwordPolicy.Passed = match.Success;
                }
                else if (passwordPolicy.PasswordPolicyTypeIdentifier.PasswordPolicyTypeCode == PasswordPolicyTypes.MinimumLength)
                {
                    passwordPolicy.Passed = password.Length >= passwordPolicy.Length.Value;
                }
                else if (passwordPolicy.PasswordPolicyTypeIdentifier.PasswordPolicyTypeCode == PasswordPolicyTypes.MaximumLength)
                {
                    passwordPolicy.Passed = password.Length <= passwordPolicy.Length.Value;
                }
                else if (passwordPolicy.PasswordPolicyTypeIdentifier.PasswordPolicyTypeCode == PasswordPolicyTypes.History)
                {
                    var previousPassword = await _UserPreviousPasswordServiceProvider.GetPreviousPasswords(userIdentifier, passwordPolicy.HistoryLimit.Value);
                    if (user == null)
                    {
                        user = await _UserSharedServiceProvider.GetSimpleAsync(userIdentifier);
                    }
                    // if there all passwords hashes fail, then we do not have a match, therefore succeeded check
                    passwordPolicy.Passed = previousPassword.All(pr => VerifyHashPassword(user, pr.PasswordHash, password));
                }
            }
            return passwordPolicies;
        }
        public async Task<UserViewModel> UpdateSecuritySettingsAsync(UpdateSecuritySettingsViewModel request)
        {
            AppUser user = null;

            UserIdentifier userIdentifier = request.Identifier;

            if (await _UserSharedServiceProvider.ExistsAsync(userIdentifier))
            {
                try
                {
                    user = await _UserDataProvider.UpdateSecuritySettingsAsync(request);

                    user = await _UserDataProvider.GetAsync(user.Id);

                    return _UserSharedServiceProvider.GetUserViewModel(user, userIdentifier.GetTenantIdentifier());
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "User update threw an error.");
                    throw new IdPException(ErrorCode.GENERIC_ERROR, e.Message);
                }
            }
            else
            {
                // user not found for updating
                string error = $"User does not exist. Username search for is '{request.Identifier}'";
                _logger.LogError(error);
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }
        }

        public async Task<UserViewModel> LoginUserAsync(LoginViewModel login)
        {
            var user = await _UserSharedServiceProvider.GetAsync(login.GetUserIdentifier(), false, true);

            // check if it all went well
            if (user == null)
                throw new IdPUnauthorizedException(ErrorCode.USER_NOT_FOUND);

            if (user.LockoutEnabled)
            {
                throw new IdPUnauthorizedException(ErrorCode.USER_DISABLED);
            }

            // check password
            var result = VerifyHashWithStatusPassword(user, login.Password);

            // see if password didn't match
            if (result == PasswordVerificationResult.Failed)
                throw new IdPUnauthorizedException(ErrorCode.USER_NOT_AUTHENTICATED);

            // succeeded. see if a rehash is needed
            if (result == PasswordVerificationResult.SuccessRehashNeeded)
            {
                string passwordHash = HashPassword(user, login.Password);
                user = await _UserDataProvider.SetPasswordHashAsync(login.GetUserIdentifier(), login.Password);
            }

            // succeeded, return user
            var model = _UserSharedServiceProvider.GetUserViewModel(user, login.GetUserIdentifier().GetTenantIdentifier());

            if (model.IsNewPasswordRequired)
            {
                if (model.Sessions == null)
                {
                    model.Sessions = new List<UserTenantSessionModel>();
                }

                foreach (var sessionToDelete in model.Sessions.Where(s => s.UserTenantSessionTypeCode == UserTenantSessionTypes.ForceNewPassword))
                {
                    // delete all currently stored sessions to reset the password
                    // so we only have 1 at a time
                    await _UserTenantSessionServiceProvider.DeleteAsync(sessionToDelete.UserTenantSessionIdentifier);
                }

                // remove sessions just deleted for user
                model.Sessions = model.Sessions.Where(s => s.UserTenantSessionTypeCode != UserTenantSessionTypes.ForceNewPassword).ToList();

                // create
                var session = await _UserTenantSessionServiceProvider.CreateForceNewPasswordSessionForUserAsync(new UserTenantIdentifier(model.Identifier));
                
                // add newly created session
                model.Sessions.Add(session);
            }

            return model;
        }

        public async Task<UserViewModel> SetGroupsForUser(UserIdentifier userIdentifier, params string[] userGroupCodes)
        {
            var user = await _UserSharedServiceProvider.GetAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, $"User not found for {userIdentifier.Username} {userIdentifier.TenantCode} {userIdentifier.ProjectCode}");
            }

            var currentCodes = user.UserTenants.FirstOrDefault(ut => ut.Tenant.GetIdentifier().IsEquals(userIdentifier.GetTenantIdentifier()))?.UserTenantUserGroups.Select(ug => ug.UserGroup.Code);
            var toRemove = currentCodes.Except(userGroupCodes.ToList(), c => c);
            var toAdd = userGroupCodes.Except(currentCodes, c => c);

            // we only process what is accessible to the current user
            var accessibleUserGroups = await _UserGroupServiceProvider.GetAccessibleGroupsIncluded(_HttpContextAccessor.LoggedInUserIdentifier());
            toRemove = toRemove.Common(accessibleUserGroups.Select(ug => ug.Code), ug => ug);
            toAdd = toAdd.Common(accessibleUserGroups.Select(ug => ug.Code), ug => ug);

            if (toRemove.Any())
            {
                foreach (var code in toRemove)
                {
                    user = await _UserGroupServiceProvider.RemoveUserFromGroup(userIdentifier, code);
                }
            }

            if (toAdd.Any())
            {
                foreach (var code in toAdd)
                {
                    user = await _UserGroupServiceProvider.AddUserToGroup(userIdentifier, code);
                }
            }

            // get updated user
            return await _UserSharedServiceProvider.GetUserViewModelAsync(userIdentifier);
        }

        public async Task<bool> SetManagedGroupsForUser(UserIdentifier userIdentifier, params string[] userGroupCodes)
        {
            var result = await _UserGroupManagementServiceProvider.SetUserGroups(userIdentifier, userGroupCodes);
            if (!result)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Method returned false");
            }
            // get updated user
            return true;
        }


        private string HashPassword(AppUser user, string password)
        {
            return EncryptionHelper.EncryptValue(user, password);
        }

        private bool VerifyHashPassword(AppUser user, string password)
        {
            return EncryptionHelper.VerifyEncryptedValue(user, user.PasswordHash, password);
        }

        private PasswordVerificationResult VerifyHashWithStatusPassword(AppUser user, string password)
        {
            return EncryptionHelper.VerifyHashWithStatusPassword(user, user.PasswordHash, password);
        }

        private bool VerifyHashPassword(AppUser user, string passwordHash, string password)
        {
            return EncryptionHelper.VerifyEncryptedValue(user, passwordHash, password);
        }
        private bool VerifyHashWithStatusPassword(AppUser user, string passwordHash, string password)
        {
            return EncryptionHelper.VerifyEncryptedValue(user, passwordHash, password);
        }

        public async Task<int> DeleteAsync(UserIdentifier userIdentifier)
        {
            // check if the user is the current logged in user
            // a user cannot delete itself
            if (userIdentifier.IsEquals(_HttpContextAccessor.LoggedInUserIdentifier()))
            {
                throw new IdPNotFoundException(ErrorCode.USER_CANNOT_BE_DELETED_SAME, "User cannot delete itself");
            }
            return await _UserDataProvider.DeleteAsync(userIdentifier);
        }


        public async Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, string mimeType, Stream stream)
        {
            if (!MimeTypes.Images.Contains(mimeType))
            {
                _logger.LogError("File with mimeType {0}, is not an image {1}", mimeType, userIdentifier.ToString());
                return false;
            }

            byte[] data = null;
            using (BinaryReader reader = new BinaryReader(stream))
            {
                data = reader.ReadBytes((int)stream.Length);
            }
            return await UploadProfilePictureAsync(userIdentifier, data);
        }

        public async Task<ClaimsPrincipal> InitialiseUserFromPrincipal(ClaimsPrincipal principal)
        {
            Claim initialised = new Claim(ClaimName.InitialisedKey, "true");

            string username = principal.Username();
            string tenantCode = principal.TenantCode();
            string projectCode = principal.ProjectCode();
            string userGuid = principal.UserGuid();
            bool adminScope = principal.HasAdminScope();

            if (adminScope)
            {
                _logger.LogDebug("Admin scope was not found.");
            }
            else if (string.IsNullOrWhiteSpace(username))
            {
                _logger.LogWarning("User name was not found. Include 'name' claim in token.");
            }

            if (string.IsNullOrWhiteSpace(tenantCode))
            {
                _logger.LogWarning("Tenant claim was not found {0}", ClaimName.Tenant);
            }
            if (string.IsNullOrWhiteSpace(projectCode))
            {
                _logger.LogWarning("Project claim was not found {0}", ClaimName.Project);
            }

            _logger.LogDebug($"Checking authorization of user {username} {tenantCode} {projectCode}");

            ClaimsPrincipal user = null;
            try
            {
                if (adminScope)
                {
                    user = _UserSharedServiceProvider.SetApiAdminAsLoggedInUser(new TenantIdentifier(tenantCode, projectCode));
                }
                else
                {
                    var userIdentifier = new UserIdentifier(userGuid, username, tenantCode, projectCode);
                    var appUser = await _UserSharedServiceProvider.GetAsync(new UserIdentifier(userGuid, username, tenantCode, projectCode), false, true);
                    user = _UserSharedServiceProvider.SetAsLoggedInUser(appUser, userIdentifier);
                }
            }
            catch (IdPNotFoundException e)
            {
                _logger.LogWarning(e, "Some entity was not found : {0} {1}", e.ErrorCode.ToString(), e.Message);
            }
            catch (IdPException e)
            {
                _logger.LogError(e, "IdPException thrown while getting user: {0}", e.ErrorCode.ToString());
                throw e;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Exception thrown while getting user: {0}", e.Message);
                throw e;
            }

            if (user != null)
            {
                _logger.LogDebug($"User found.");
                var claims = user.Claims.ToList();
                claims.AddRange(principal.Claims);
                claims.Add(initialised);
                claims = claims.Distinct().ToList();
                var newClaimsIdentity = new ClaimsIdentity(claims, ((ClaimsIdentity)principal.Identity).AuthenticationType);
                return new ClaimsPrincipal(newClaimsIdentity);
            }
            else
            {
                _logger.LogDebug("User or api not found. Denying access.");
                var claims = principal.Claims.ToList();
                claims.Add(initialised);
                var newClaimsIdentity = new ClaimsIdentity(claims, ((ClaimsIdentity)principal.Identity).AuthenticationType);
                return new ClaimsPrincipal(newClaimsIdentity);
            }
        }

        private async Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, byte[] data)
        {
            return await _UserDataProvider.UploadProfilePictureAsync(userIdentifier, data);
        }
    }
}
