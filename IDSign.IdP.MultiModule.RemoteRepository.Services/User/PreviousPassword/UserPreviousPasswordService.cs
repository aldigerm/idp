﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserPreviousPasswordService : IUserPreviousPasswordServiceProvider
    {
        protected readonly IUserPreviousPasswordDataProvider _UserPreviousPasswordDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserPreviousPasswordService> _logger;
        public UserPreviousPasswordService(
             RemoteRepositoryDbContext ctx
            , ILogger<UserPreviousPasswordService> logger
            , IUserPreviousPasswordDataProvider userPreviousPasswordDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserPreviousPasswordDataProvider = userPreviousPasswordDataProvider;
        }

        public async Task<UserPreviousPassword> CreateAsync(UserIdentifier userIdentifier, string hash)
        {
            return await _UserPreviousPasswordDataProvider.CreateAsync(userIdentifier, hash);
        }


        public async Task<IList<UserPreviousPassword>> GetPreviousPasswords(UserIdentifier userIdentifier, int limit)
        {
            return await _UserPreviousPasswordDataProvider.GetPreviousPasswords(userIdentifier, limit);
        }

    }
}
