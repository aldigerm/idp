﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface ITenantClaimServiceProvider
    {
        Task<TenantClaimModel> GetViewModelAsync(TenantClaimIdentifier claimIdentifier);
        Task<TenantClaimIdentifier> CreateAsync(TenantClaimCreateModel claim);
        Task<TenantClaimIdentifier> UpdateAsync(TenantClaimUpdateModel claim);
        Task<bool> DeleteAsync(TenantClaimIdentifier claim);
        Task<bool> SetTenants(TenantClaimSetTenantsModel model);
        Task<IList<TenantClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task InitAsync(TenantIdentifier tenantIdentifier);
    }
}
