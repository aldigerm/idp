﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface ITenantClaimTypeServiceProvider
    {
        Task InitAsync(ProjectIdentifier projectIdentifier);
        Task<TenantClaimTypeIdentifier> CreateAsync(TenantClaimTypeCreateModel claim);
        Task<TenantClaimTypeIdentifier> UpdateAsync(TenantClaimTypeUpdateModel claim);
        Task<bool> DeleteAsync(TenantClaimTypeIdentifier claim);
        Task<List<TenantClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<TenantClaimTypeModel> GetViewModelAsync(TenantClaimTypeIdentifier tenantGroupIdentifier);
        Task<bool> SetTenants(TenantClaimTypeSetTenantsModel model);
        Task DeleteAllAsync(ProjectIdentifier projectIdentifier);
    }
}
