﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class TenantClaimService : ITenantClaimServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly ITenantClaimDataProvider _TenantClaimDataProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;

        public TenantClaimService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , ITenantClaimDataProvider tenantClaimDataProvider
            , ITenantServiceProvider tenantServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _TenantClaimDataProvider = tenantClaimDataProvider;
            _TenantServiceProvider = tenantServiceProvider;
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {
            await TryCreateAsync(new TenantClaimCreateModel()
            {
                TenantIdentifier = tenantIdentifier,
                Type = InitialClaimName.TenantClaimType,
                Value = tenantIdentifier.TenantCode
            });
        }

        private async Task TryCreateAsync(TenantClaimCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.TENANT_CLAIM_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<TenantClaimIdentifier> CreateAsync(TenantClaimCreateModel claim)
        {
            var result = await _TenantClaimDataProvider.GetSimpleAsync(claim.GetTenantClaimIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_EXISTS, claim.GetTenantClaimIdentifier().ToString());
            }
            else
            {
                return await _TenantClaimDataProvider.CreateAsync(claim);
            }
        }

        public async Task<TenantClaimIdentifier> UpdateAsync(TenantClaimUpdateModel claim)
        {
            var result = await _TenantClaimDataProvider.GetSimpleAsync(claim.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_NOT_FOUND, claim.ToString());
            }
            else
            {
                return await _TenantClaimDataProvider.UpdateAsync(claim);
            }
        }

        public async Task<bool> DeleteAsync(TenantClaimIdentifier claim)
        {
            var result = await _TenantClaimDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_NOT_FOUND);
            }
            // get updated tenant
            return true;
        }

        public async Task<IList<TenantClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var tenantClaims = await _TenantClaimDataProvider.GetListAsync(tenantIdentifier);
            IList<TenantClaimModel> list = new List<TenantClaimModel>();
            foreach (var entity in tenantClaims)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<TenantClaimModel> GetViewModelAsync(TenantClaimIdentifier claimIdentifier)
        {
            var result = await _TenantClaimDataProvider.GetAsync(claimIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_NOT_FOUND);
            }


            var similarTenantClaims = await _TenantClaimDataProvider.GetListWithSameTypeAndValueAsync(claimIdentifier);

            var model = GetViewModel(result);
            if (similarTenantClaims != null && similarTenantClaims.Any())
            {
                model.TenantsWithSameClaim = new List<TenantIdentifier>();
                foreach (var tenantClaim in similarTenantClaims)
                {
                    var tenant = tenantClaim.Tenant.GetIdentifier();
                    model.TenantsWithSameClaim.Add(tenant);
                }
            }

            return model;
        }


        private TenantClaimModel GetViewModel(TenantClaim entity)
        {
            var model = new TenantClaimModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.Description = entity.TenantClaimType.Description;

            return model;
        }

        public async Task<bool> SetTenants(TenantClaimSetTenantsModel model)
        {
            var result = await _TenantClaimDataProvider.GetListWithSameTypeAndValueAsync(model.Identifier);
            var current = new List<TenantIdentifier>();
            foreach (var tenantClaim in result)
            {
                current.Add(tenantClaim.Tenant.GetIdentifier());
            }

            var submitted = model.Tenants;
            var toRemove = new List<TenantIdentifier>();
            foreach (var currentUser in current)
            {
                if (!submitted.Any(u => u.IsEquals(currentUser)))
                {
                    toRemove.Add(currentUser);
                }
            }
            var toAdd = new List<TenantIdentifier>();
            foreach (var submittedUser in submitted)
            {
                if (!current.Any(u => u.IsEquals(submittedUser)))
                {
                    toAdd.Add(submittedUser);
                }
            }
            foreach (var tenant in toRemove)
            {
                model.Identifier.TenantCode = tenant.TenantCode;
                await DeleteAsync(model.Identifier);
            }
            foreach (var tenant in toAdd)
            {
                var claimModel = new TenantClaimCreateModel();
                claimModel.TenantIdentifier = model.Identifier.GetTenantIdentifier();
                claimModel.Type = model.Identifier.Type;
                claimModel.Value = model.Identifier.Value;
                await CreateAsync(claimModel);
            }
            return true;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var list = await _TenantClaimDataProvider.GetListAsync(tenantIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
