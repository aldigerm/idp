﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class TenantClaimTypeService : ITenantClaimTypeServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly ITenantClaimTypeDataProvider _TenantClaimTypeDataProvider;
        protected readonly ITenantServiceProvider _TenantServiceProvider;
        protected readonly ITenantClaimServiceProvider _TenantClaimServiceProvider;
        public TenantClaimTypeService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , ITenantClaimTypeDataProvider tenantClaimTypeDataProvider
            , ITenantServiceProvider tenantServiceProvider
            , ITenantClaimServiceProvider tenantClaimServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _TenantClaimTypeDataProvider = tenantClaimTypeDataProvider;
            _TenantServiceProvider = tenantServiceProvider;
            _TenantClaimServiceProvider = tenantClaimServiceProvider;
        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            await TryCreateAsync(new TenantClaimTypeCreateModel()
            {
                Description = "OnBoard Company Code",
                ProjectIdentifier = projectIdentifier,
                Type = InitialClaimName.TenantClaimType
            });
        }

        private async Task TryCreateAsync(TenantClaimTypeCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.TENANT_CLAIM_TYPE_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<TenantClaimTypeIdentifier> CreateAsync(TenantClaimTypeCreateModel claim)
        {
            var result = await _TenantClaimTypeDataProvider.GetSimpleAsync(claim.GetClaimTypeIdentifier());

            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_EXISTS);
            }
            else
            {
                result = await _TenantClaimTypeDataProvider.CreateAsync(claim);
            }

            if (result == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't add update");
            }

            // get updated tenant
            return claim.GetClaimTypeIdentifier();
        }

        public async Task<TenantClaimTypeIdentifier> UpdateAsync(TenantClaimTypeUpdateModel claim)
        {
            TenantClaimType result;
            if (!claim.NewType.Equals(claim.Identifier.Type, System.StringComparison.InvariantCultureIgnoreCase))
            {
                result = await _TenantClaimTypeDataProvider.GetSimpleAsync(new TenantClaimTypeIdentifier(claim.NewType, claim.Identifier.GetProjectIdentifier()));

                // check if new claim exists
                if (result != null)
                {
                    throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_EXISTS);
                }
            }

            // if type doesnt exist then an error will be thrown
            var identifier = await _TenantClaimTypeDataProvider.UpdateAsync(claim);

            if (identifier == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't update");
            }
            // get updated tenant
            return identifier;
        }

        public async Task<bool> DeleteAsync(TenantClaimTypeIdentifier claim)
        {
            var result = await _TenantClaimTypeDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND);
            }
            // get updated tenant
            return true;
        }

        public async Task<List<TenantClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var tenantClaimTypes = await _TenantClaimTypeDataProvider.GetListAsync(projectIdentifier);
            List<TenantClaimTypeModel> list = new List<TenantClaimTypeModel>();
            foreach (var entity in tenantClaimTypes)
            {
                var model = await GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<TenantClaimTypeModel> GetViewModelAsync(TenantClaimTypeIdentifier claimTypeIdentifier)
        {
            var result = await _TenantClaimTypeDataProvider.GetAsync(claimTypeIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND);
            }
            return await GetViewModel(result);
        }

        public async Task<bool> SetTenants(TenantClaimTypeSetTenantsModel model)
        {
            var result = await GetViewModelAsync(model.Identifier);
            var submittedTenants = new List<TenantIdentifier>();
            foreach (var u in model.Tenants)
            {
                var claimModel = new TenantClaimCreateModel(model.Value, u, model);
                await _TenantClaimServiceProvider.CreateAsync(claimModel);
            }

            return true;
        }

        private async Task<TenantClaimTypeModel> GetViewModel(TenantClaimType entity)
        {
            var model = new TenantClaimTypeModel();
            if (entity == null)
                return model;

            model.Identifier = new TenantClaimTypeIdentifier(entity.Type, entity.Project.GetIdentifier());
            model.Description = entity.Description;

            if (entity.TenantClaims != null)
            {
                foreach (var tenant in entity.TenantClaims.Select(uc => uc.Tenant))
                {
                    model.Tenants.Add(_TenantServiceProvider.GetViewModel(tenant));
                }
            }

            return model;
        }

        public async Task DeleteAllAsync(ProjectIdentifier projectIdentifier)
        {
            var list = await _TenantClaimTypeDataProvider.GetListAsync(projectIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
