﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class RoleClaimTypeService : IRoleClaimTypeServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IRoleClaimTypeDataProvider _RoleClaimTypeDataProvider;
        protected readonly IRoleServiceProvider _RoleServiceProvider;
        protected readonly IRoleClaimServiceProvider _RoleClaimServiceProvider;
        public RoleClaimTypeService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IRoleClaimTypeDataProvider roleClaimTypeDataProvider
            , IRoleServiceProvider roleServiceProvider
            , IRoleClaimServiceProvider roleClaimServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _RoleClaimTypeDataProvider = roleClaimTypeDataProvider;
            _RoleServiceProvider = roleServiceProvider;
            _RoleClaimServiceProvider = roleClaimServiceProvider;
        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            await TryCreateAsync(new RoleClaimTypeCreateModel()
            {
                Description = "OnBoard Partner Code",
                ProjectIdentifier = projectIdentifier,
                Type = ClaimName.Partner
            });

            await TryCreateAsync(new RoleClaimTypeCreateModel()
            {
                Description = "OnBoard User code",
                ProjectIdentifier = projectIdentifier,
                Type = InitialClaimName.User
            });
        }

        private async Task TryCreateAsync(RoleClaimTypeCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.ROLE_CLAIM_TYPE_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<RoleClaimTypeIdentifier> CreateAsync(RoleClaimTypeCreateModel claim)
        {
            var result = await _RoleClaimTypeDataProvider.GetSimpleAsync(claim.GetClaimTypeIdentifier());

            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_EXISTS);
            }
            else
            {
                result = await _RoleClaimTypeDataProvider.CreateAsync(claim);
            }

            if (result == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't add update");
            }

            // get updated role
            return claim.GetClaimTypeIdentifier();
        }

        public async Task<RoleClaimTypeIdentifier> UpdateAsync(RoleClaimTypeUpdateModel claim)
        {
            RoleClaimType result;
            if (!claim.NewType.Equals(claim.Identifier.Type, System.StringComparison.InvariantCultureIgnoreCase))
            {
                result = await _RoleClaimTypeDataProvider.GetSimpleAsync(new RoleClaimTypeIdentifier(claim.NewType, claim.Identifier.GetProjectIdentifier()));

                // check if new claim exists
                if (result != null)
                {
                    throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_EXISTS);
                }
            }

            // if type doesnt exist then an error will be thrown
            var identifier = await _RoleClaimTypeDataProvider.UpdateAsync(claim);

            if (identifier == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't update");
            }
            // get updated role
            return identifier;
        }

        public async Task<bool> DeleteAsync(RoleClaimTypeIdentifier claim)
        {
            var result = await _RoleClaimTypeDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND);
            }
            // get updated role
            return true;
        }

        public async Task<List<RoleClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var roleClaimTypes = await _RoleClaimTypeDataProvider.GetListAsync(projectIdentifier);
            List<RoleClaimTypeModel> list = new List<RoleClaimTypeModel>();
            foreach (var entity in roleClaimTypes)
            {
                var model = await GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<RoleClaimTypeModel> GetViewModelAsync(RoleClaimTypeIdentifier claimTypeIdentifier)
        {
            var result = await _RoleClaimTypeDataProvider.GetAsync(claimTypeIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND);
            }
            return await GetViewModel(result);
        }

        public async Task<bool> SetRoles(RoleClaimTypeSetRolesModel model)
        {
            var result = await GetViewModelAsync(model.Identifier);
            var submittedRoles = new List<RoleIdentifier>();
            foreach (var u in model.Roles)
            {
                var claimModel = new RoleClaimCreateModel(model.Value, u, model);
                await _RoleClaimServiceProvider.CreateAsync(claimModel);
            }

            return true;
        }

        private async Task<RoleClaimTypeModel> GetViewModel(RoleClaimType entity)
        {
            var model = new RoleClaimTypeModel();
            if (entity == null)
                return model;

            model.Identifier = new RoleClaimTypeIdentifier(entity.Type, entity.Project.GetIdentifier());
            model.Description = entity.Description;

            if (entity.RoleClaims != null)
            {
                foreach (var role in entity.RoleClaims.Select(uc => uc.Role))
                {
                    model.Roles.Add(await _RoleServiceProvider.GetViewModelAsync(role, false));
                }
            }

            return model;
        }

        public async Task DeleteAllAsync(ProjectIdentifier projectIdentifier)
        {
            var list = await _RoleClaimTypeDataProvider.GetListAsync(projectIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
