﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IRoleClaimTypeServiceProvider
    {
        Task InitAsync(ProjectIdentifier projectIdentifier);
        Task<RoleClaimTypeIdentifier> CreateAsync(RoleClaimTypeCreateModel claim);
        Task<RoleClaimTypeIdentifier> UpdateAsync(RoleClaimTypeUpdateModel claim);
        Task<bool> DeleteAsync(RoleClaimTypeIdentifier claim);
        Task<List<RoleClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<RoleClaimTypeModel> GetViewModelAsync(RoleClaimTypeIdentifier roleGroupIdentifier);
        Task<bool> SetRoles(RoleClaimTypeSetRolesModel model);
        Task DeleteAllAsync(ProjectIdentifier projectIdentifier);
    }
}
