﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class RoleClaimService : IRoleClaimServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IRoleClaimDataProvider _RoleClaimDataProvider;
        protected readonly IRoleServiceProvider _RoleServiceProvider;

        public RoleClaimService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IRoleClaimDataProvider roleClaimDataProvider
            , IRoleServiceProvider roleServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor  ;
            _RoleClaimDataProvider = roleClaimDataProvider;
            _RoleServiceProvider = roleServiceProvider;
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {

            await TryCreateForRole(RoleCode.Psp, tenantIdentifier);
            await TryCreateForRole(RoleCode.Risk, tenantIdentifier);
            await TryCreateForRole(RoleCode.User, tenantIdentifier);
            await TryCreateForRole(RoleCode.InternalUser, tenantIdentifier);
            await TryCreateForRole(RoleCode.Partner, tenantIdentifier);
            await TryCreateForRole(RoleCode.Supervisor, tenantIdentifier);
            await TryCreateForRole(RoleCode.PspSupervisor, tenantIdentifier);
            await TryCreateForRole(RoleCode.RiskSupervisor, tenantIdentifier);
            await TryCreateForRole(SupportConstants.SupportRoleCode, tenantIdentifier);
        }

        private async Task TryCreateForRole(string roleCode, TenantIdentifier tenantIdentifier)
        {
            await TryCreateAsync(new RoleClaimCreateModel()
            {
                TenantIdentifier = tenantIdentifier,
                RoleCode = roleCode,
                Type = InitialClaimName.User,
                Value = roleCode
            });
        }

        private async Task TryCreateAsync(RoleClaimCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.ROLE_CLAIM_EXISTS))
                {
                    throw;
                }
            }
        }
        public async Task<RoleClaimIdentifier> CreateAsync(RoleClaimCreateModel claim)
        {
            var result = await _RoleClaimDataProvider.GetSimpleAsync(claim.GetRoleClaimIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_EXISTS, claim.GetRoleClaimIdentifier().ToString());
            }
            else
            {
                return await _RoleClaimDataProvider.CreateAsync(claim);
            }
        }

        public async Task<RoleClaimIdentifier> UpdateAsync(RoleClaimUpdateModel claim)
        {
            var result = await _RoleClaimDataProvider.GetSimpleAsync(claim.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_NOT_FOUND, claim.ToString());
            }
            else
            {
                return await _RoleClaimDataProvider.UpdateAsync(claim);
            }
        }

        public async Task<bool> DeleteAsync(RoleClaimIdentifier claim)
        {
            var result = await _RoleClaimDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_NOT_FOUND);
            }
            // get updated role
            return true;
        }

        public async Task<IList<RoleClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var roleClaims = await _RoleClaimDataProvider.GetListAsync(tenantIdentifier);
            IList<RoleClaimModel> list = new List<RoleClaimModel>();
            foreach (var entity in roleClaims)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<RoleClaimModel> GetViewModelAsync(RoleClaimIdentifier claimIdentifier)
        {
            var result = await _RoleClaimDataProvider.GetAsync(claimIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_NOT_FOUND);
            }


            var similarRoleClaims = await _RoleClaimDataProvider.GetListWithSameTypeAndValueAsync(claimIdentifier);

            var model = GetViewModel(result);
            if (similarRoleClaims != null && similarRoleClaims.Any())
            {
                model.RolesWithSameClaim = new List<RoleIdentifier>();
                foreach (var roleClaim in similarRoleClaims)
                {
                    var role = roleClaim.Role.GetIdentifier();
                    model.RolesWithSameClaim.Add(role);
                }
            }

            return model;
        }


        private RoleClaimModel GetViewModel(RoleClaim entity)
        {
            var model = new RoleClaimModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.Description = entity.RoleClaimType.Description;

            return model;
        }

        public async Task<bool> SetRoles(RoleClaimSetRolesModel model)
        {
            var result = await _RoleClaimDataProvider.GetListWithSameTypeAndValueAsync(model.Identifier);
            var current = new List<RoleIdentifier>();
            foreach (var roleClaim in result)
            {
                current.Add(roleClaim.Role.GetIdentifier());
            }

            var submitted = model.Roles;
            var toRemove = new List<RoleIdentifier>();
            foreach (var currentUser in current)
            {
                if (!submitted.Any(u => u.IsEquals(currentUser)))
                {
                    toRemove.Add(currentUser);
                }
            }
            var toAdd = new List<RoleIdentifier>();
            foreach (var submittedUser in submitted)
            {
                if (!current.Any(u => u.IsEquals(submittedUser)))
                {
                    toAdd.Add(submittedUser);
                }
            }
            foreach (var role in toRemove)
            {
                model.Identifier.RoleCode = role.RoleCode;
                await DeleteAsync(model.Identifier);
            }
            foreach (var role in toAdd)
            {
                var claimModel = new RoleClaimCreateModel();
                claimModel.TenantIdentifier = model.Identifier.GetTenantIdentifier();
                claimModel.RoleCode = role.RoleCode;
                claimModel.Type = model.Identifier.Type;
                claimModel.Value = model.Identifier.Value;
                await CreateAsync(claimModel);
            }
            return true;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var list = await _RoleClaimDataProvider.GetListAsync(tenantIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
