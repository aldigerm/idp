﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IRoleClaimServiceProvider
    {
        Task<RoleClaimModel> GetViewModelAsync(RoleClaimIdentifier claimIdentifier);
        Task<RoleClaimIdentifier> CreateAsync(RoleClaimCreateModel claim);
        Task<RoleClaimIdentifier> UpdateAsync(RoleClaimUpdateModel claim);
        Task<bool> DeleteAsync(RoleClaimIdentifier claim);
        Task<bool> SetRoles(RoleClaimSetRolesModel model);
        Task<IList<RoleClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task InitAsync(TenantIdentifier tenantIdentifier);
    }
}
