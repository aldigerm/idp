﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserClaimTypeServiceProvider
    {
        Task<UserClaimType> GetSimpleAsync(UserClaimTypeIdentifier identifier);
        Task<UserClaimTypeIdentifier> CreateAsync(UserClaimTypeCreateModel claim);
        Task<UserClaimTypeIdentifier> UpdateAsync(UserClaimTypeUpdateModel claim);
        Task<bool> DeleteAsync(UserClaimTypeIdentifier claim);
        Task<List<UserClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<UserClaimTypeModel> GetViewModelAsync(UserClaimTypeIdentifier userGroupIdentifier);
        Task<bool> SetUsers(UserClaimTypeSetUsersModel model);
        Task InitAsync(ProjectIdentifier projectIdentifier);
        Task DeleteAllAsync(ProjectIdentifier projectIdentifier);
    }
}
