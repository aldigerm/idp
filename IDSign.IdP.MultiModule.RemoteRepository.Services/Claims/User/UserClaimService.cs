﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserClaimService : IUserClaimServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserClaimDataProvider _UserClaimDataProvider;
        protected readonly IUserServiceProvider _UserServiceProvider;

        public UserClaimService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IUserClaimDataProvider userClaimDataProvider
            , IUserServiceProvider userServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor  ;
            _UserClaimDataProvider = userClaimDataProvider;
            _UserServiceProvider = userServiceProvider;
        }


        public async Task<UserClaimIdentifier> CreateAsync(UserClaimCreateModel claim)
        {
            var result = await _UserClaimDataProvider.GetSimpleAsync(claim.GetUserClaimIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_EXISTS, claim.GetUserClaimIdentifier().ToString());
            }
            else
            {
                return await _UserClaimDataProvider.CreateAsync(claim);
            }
        }

        public async Task<UserClaimIdentifier> UpdateAsync(UserClaimUpdateModel claim)
        {
            var result = await _UserClaimDataProvider.GetSimpleAsync(claim.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_NOT_FOUND, claim.ToString());
            }
            else
            {
                return await _UserClaimDataProvider.UpdateAsync(claim);
            }
        }

        public async Task<bool> DeleteAsync(UserClaimIdentifier claim)
        {
            var result = await _UserClaimDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_NOT_FOUND);
            }
            // get updated user
            return true;
        }

        public async Task<IList<UserClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var userClaims = await _UserClaimDataProvider.GetListAsync(tenantIdentifier);
            IList<UserClaimModel> list = new List<UserClaimModel>();
            foreach (var entity in userClaims)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<UserClaimModel> GetViewModelAsync(UserClaimIdentifier claimIdentifier)
        {
            var result = await _UserClaimDataProvider.GetAsync(claimIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_NOT_FOUND);
            }


            var similarUserClaims = await _UserClaimDataProvider.GetListWithSameTypeAndValueAsync(claimIdentifier);

            var model = GetViewModel(result);
            if (similarUserClaims != null && similarUserClaims.Any())
            {
                model.UsersWithSameClaim = new List<UserIdentifier>();
                foreach (var userClaim in similarUserClaims)
                {
                    var user = userClaim.UserTenant.User.GetIdentifier(claimIdentifier.GetTenantIdentifier());
                    model.UsersWithSameClaim.Add(new UserIdentifier(user));
                }
            }

            return model;
        }


        private UserClaimModel GetViewModel(UserClaim entity)
        {
            var model = new UserClaimModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.Description = entity.UserClaimType.Description;

            return model;
        }

        public async Task<bool> SetUsers(UserClaimSetUsersModel model)
        {
            var result = await _UserClaimDataProvider.GetListWithSameTypeAndValueAsync(model.Identifier);
            var current = new List<UserIdentifier>();
            foreach (var userClaim in result)
            {
                current.Add(userClaim.UserTenant.User.GetIdentifier(model.Identifier.GetTenantIdentifier()));
            }

            var submitted = model.Users;
            var toRemove = new List<UserIdentifier>();
            foreach (var currentUser in current)
            {
                if (!submitted.Any(u => u.IsEquals(currentUser)))
                {
                    toRemove.Add(currentUser);
                }
            }
            var toAdd = new List<UserIdentifier>();
            foreach (var submittedUser in submitted)
            {
                if (!current.Any(u => u.IsEquals(submittedUser)))
                {
                    toAdd.Add(submittedUser);
                }
            }
            foreach (var user in toRemove)
            {
                model.Identifier.Username = user.Username;
                await DeleteAsync(model.Identifier);
            }
            foreach (var user in toAdd)
            {
                var claimModel = new UserClaimCreateModel();
                claimModel.TenantIdentifier = model.Identifier.GetTenantIdentifier();
                claimModel.Username = user.Username;
                claimModel.Type = model.Identifier.Type;
                claimModel.Value = model.Identifier.Value;
                await CreateAsync(claimModel);
            }
            return true;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var list = await _UserClaimDataProvider.GetListAsync(tenantIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
