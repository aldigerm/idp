﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserClaimTypeService : IUserClaimTypeServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserClaimTypeDataProvider _UserClaimTypeDataProvider;
        protected readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        protected readonly IUserClaimServiceProvider _UserClaimServiceProvider;
        public UserClaimTypeService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IUserClaimTypeDataProvider userClaimTypeDataProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IUserClaimServiceProvider userClaimServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _UserClaimTypeDataProvider = userClaimTypeDataProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _UserClaimServiceProvider = userClaimServiceProvider;
        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            await TryCreateAsync(new UserClaimTypeCreateModel()
            {
                Description = "OnBoard Partner Code",
                ProjectIdentifier = projectIdentifier,
                Type = ClaimName.Partner
            });
        }

        private async Task TryCreateAsync(UserClaimTypeCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.USER_CLAIM_TYPE_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<UserClaimType> GetSimpleAsync(UserClaimTypeIdentifier identifier)
        {
            return await _UserClaimTypeDataProvider.GetSimpleAsync(identifier);
        }

        public async Task<UserClaimTypeIdentifier> CreateAsync(UserClaimTypeCreateModel claim)
        {
            var result = await _UserClaimTypeDataProvider.GetSimpleAsync(claim.GetClaimTypeIdentifier());

            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_EXISTS);
            }
            else
            {
                result = await _UserClaimTypeDataProvider.CreateAsync(claim);
            }

            if (result == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't add update");
            }

            // get updated user
            return claim.GetClaimTypeIdentifier();
        }

        public async Task<UserClaimTypeIdentifier> UpdateAsync(UserClaimTypeUpdateModel claim)
        {
            UserClaimType result;
            if (!claim.NewType.Equals(claim.Identifier.Type, System.StringComparison.InvariantCultureIgnoreCase))
            {
                result = await _UserClaimTypeDataProvider.GetSimpleAsync(new UserClaimTypeIdentifier(claim.NewType, claim.Identifier.ProjectCode));

                // check if new claim exists
                if (result != null)
                {
                    throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_EXISTS);
                }
            }

            // if type doesnt exist then an error will be thrown
            var identifier = await _UserClaimTypeDataProvider.UpdateAsync(claim);

            if (identifier == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't update");
            }
            // get updated user
            return identifier;
        }

        public async Task<bool> DeleteAsync(UserClaimTypeIdentifier claim)
        {
            var result = await _UserClaimTypeDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND);
            }
            // get updated user
            return true;
        }

        public async Task<List<UserClaimTypeModel>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var userClaimTypes = await _UserClaimTypeDataProvider.GetListAsync(projectIdentifier);
            List<UserClaimTypeModel> list = new List<UserClaimTypeModel>();
            foreach (var entity in userClaimTypes)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<UserClaimTypeModel> GetViewModelAsync(UserClaimTypeIdentifier claimTypeIdentifier)
        {
            var result = await _UserClaimTypeDataProvider.GetAsync(claimTypeIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND);
            }
            return GetViewModel(result);
        }

        public async Task<bool> SetUsers(UserClaimTypeSetUsersModel model)
        {
            var result = await GetViewModelAsync(model.Identifier);
            var submittedUsers = new List<UserIdentifier>();
            foreach (var u in model.Users)
            {
                var claimModel = new UserClaimCreateModel(model.Value, u, model);
                await _UserClaimServiceProvider.CreateAsync(claimModel);
            }

            return true;
        }

        private UserClaimTypeModel GetViewModel(UserClaimType entity)
        {
            var model = new UserClaimTypeModel();
            if (entity == null)
                return model;

            model.Identifier = new UserClaimTypeIdentifier(entity.Type, entity.Project.Code);
            model.Description = entity.Description;

            if (entity.UserClaims != null)
            {
                foreach (var userClaims in entity.UserClaims)
                {
                    model.Users.Add(_UserSharedServiceProvider.GetUserViewModel(userClaims.UserTenant.User, userClaims.UserTenant.TenantId));
                }
            }

            return model;
        }

        public async Task DeleteAllAsync(ProjectIdentifier projectIdentifier)
        {
            var list = await _UserClaimTypeDataProvider.GetListAsync(projectIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
