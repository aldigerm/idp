﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserClaimServiceProvider
    {
        Task<UserClaimModel> GetViewModelAsync(UserClaimIdentifier claimIdentifier);
        Task<UserClaimIdentifier> CreateAsync(UserClaimCreateModel claim);
        Task<UserClaimIdentifier> UpdateAsync(UserClaimUpdateModel claim);
        Task<bool> DeleteAsync(UserClaimIdentifier claim);
        Task<bool> SetUsers(UserClaimSetUsersModel model);
        Task<IList<UserClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(TenantIdentifier tenantIdentifier);
    }
}
