﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IClaimsCollectionServiceProvider
    {
        Task InitAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(ProjectIdentifier projectIdentifier);
    }
}
