﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class ClaimsCollectionService : IClaimsCollectionServiceProvider
    {
        protected readonly IUserGroupClaimServiceProvider _UserGroupClaimServiceProvider;
        protected readonly IUserGroupClaimTypeServiceProvider _UserGroupClaimTypeServiceProvider;
        protected readonly IRoleClaimServiceProvider _RoleClaimServiceProvider;
        protected readonly IRoleClaimTypeServiceProvider _RoleClaimTypeServiceProvider;
        protected readonly IUserClaimServiceProvider _UserClaimServiceProvider;
        protected readonly IUserClaimTypeServiceProvider _UserClaimTypeServiceProvider;
        protected readonly ITenantClaimServiceProvider _TenantClaimServiceProvider;
        protected readonly ITenantClaimTypeServiceProvider _TenantClaimTypeServiceProvider;
        public ClaimsCollectionService
            (
              IUserGroupClaimServiceProvider userGroupClaimServiceProvider
            , IUserGroupClaimTypeServiceProvider userGroupClaimTypeServiceProvider
            , IRoleClaimServiceProvider roleClaimServiceProvider
            , IRoleClaimTypeServiceProvider roleClaimTypeServiceProvider
            , IUserClaimServiceProvider userClaimServiceProvider
            , IUserClaimTypeServiceProvider userClaimTypeServiceProvider
            , ITenantClaimServiceProvider tenantClaimServiceProvider
            , ITenantClaimTypeServiceProvider tenantClaimTypeServiceProvider
            )
        {
            _UserGroupClaimServiceProvider = userGroupClaimServiceProvider;
            _UserGroupClaimTypeServiceProvider = userGroupClaimTypeServiceProvider;
            _RoleClaimServiceProvider = roleClaimServiceProvider;
            _RoleClaimTypeServiceProvider = roleClaimTypeServiceProvider;
            _UserClaimServiceProvider = userClaimServiceProvider;
            _UserClaimTypeServiceProvider = userClaimTypeServiceProvider;
            _TenantClaimServiceProvider = tenantClaimServiceProvider;
            _TenantClaimTypeServiceProvider = tenantClaimTypeServiceProvider;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            await _RoleClaimServiceProvider.DeleteAllAsync(tenantIdentifier);
            await _UserGroupClaimServiceProvider.DeleteAllAsync(tenantIdentifier);
            await _UserClaimServiceProvider.DeleteAllAsync(tenantIdentifier);
            await _TenantClaimServiceProvider.DeleteAllAsync(tenantIdentifier);
        }

        public async Task DeleteAllAsync(ProjectIdentifier projectIdentifier)
        {
            await _RoleClaimTypeServiceProvider.DeleteAllAsync(projectIdentifier);
            await _UserGroupClaimTypeServiceProvider.DeleteAllAsync(projectIdentifier);
            await _UserClaimTypeServiceProvider.DeleteAllAsync(projectIdentifier);
            await _TenantClaimTypeServiceProvider.DeleteAllAsync(projectIdentifier);
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {
            await _UserGroupClaimTypeServiceProvider.InitAsync(tenantIdentifier.GetProjectIdentifier());
            await _UserGroupClaimServiceProvider.InitAsync(tenantIdentifier);
            await _RoleClaimTypeServiceProvider.InitAsync(tenantIdentifier.GetProjectIdentifier());
            await _RoleClaimServiceProvider.InitAsync(tenantIdentifier);
            await _TenantClaimTypeServiceProvider.InitAsync(tenantIdentifier.GetProjectIdentifier());
            await _TenantClaimServiceProvider.InitAsync(tenantIdentifier);
        }

    }
}
