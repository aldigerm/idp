﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserGroupClaimService : IUserGroupClaimServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserGroupClaimDataProvider _UserGroupClaimDataProvider;
        protected readonly IUserServiceProvider _UserGroupServiceProvider;

        public UserGroupClaimService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IUserGroupClaimDataProvider userGroupClaimDataProvider
            , IUserServiceProvider userGroupServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _UserGroupClaimDataProvider = userGroupClaimDataProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
        }

        public async Task InitAsync(TenantIdentifier tenantIdentifier)
        {
            await TryCreateAsync(new UserGroupClaimCreateModel()
            {
                TenantIdentifier = tenantIdentifier,
                UserGroupCode = InitialGroupCodes.Partner1GroupCode,
                Type = ClaimName.Partner,
                Value = InitialClaimValues.Partner1UserGroupClaimValue
            });

            await TryCreateAsync(new UserGroupClaimCreateModel()
            {
                TenantIdentifier = tenantIdentifier,
                UserGroupCode = InitialGroupCodes.Partner2GroupCode,
                Type = ClaimName.Partner,
                Value = InitialClaimValues.Partner2UserGroupClaimValue
            });
        }

        private async Task TryCreateAsync(UserGroupClaimCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.USER_GROUP_CLAIM_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<UserGroupClaimIdentifier> CreateAsync(UserGroupClaimCreateModel claim)
        {
            var result = await _UserGroupClaimDataProvider.GetSimpleAsync(claim.GetUserGroupClaimIdentifier());
            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_GROUP_CLAIM_EXISTS, claim.GetUserGroupClaimIdentifier().ToString());
            }
            else
            {
                return await _UserGroupClaimDataProvider.CreateAsync(claim);
            }
        }

        public async Task<UserGroupClaimIdentifier> UpdateAsync(UserGroupClaimUpdateModel claim)
        {
            var result = await _UserGroupClaimDataProvider.GetSimpleAsync(claim.Identifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_GROUP_CLAIM_NOT_FOUND, claim.ToString());
            }
            else
            {
                return await _UserGroupClaimDataProvider.UpdateAsync(claim);
            }
        }

        public async Task<bool> DeleteAsync(UserGroupClaimIdentifier claim)
        {
            var result = await _UserGroupClaimDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.USER_GROUP_CLAIM_NOT_FOUND);
            }
            // get updated userGroup
            return true;
        }

        public async Task<IList<UserGroupClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var userGroupClaims = await _UserGroupClaimDataProvider.GetListAsync(tenantIdentifier);
            IList<UserGroupClaimModel> list = new List<UserGroupClaimModel>();
            foreach (var entity in userGroupClaims)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<UserGroupClaimModel> GetViewModelAsync(UserGroupClaimIdentifier claimIdentifier)
        {
            var result = await _UserGroupClaimDataProvider.GetAsync(claimIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_GROUP_CLAIM_NOT_FOUND);
            }


            var similarUserGroupClaims = await _UserGroupClaimDataProvider.GetListWithSameTypeAndValueAsync(claimIdentifier);

            var model = GetViewModel(result);
            if (similarUserGroupClaims != null && similarUserGroupClaims.Any())
            {
                model.UserGroupsWithSameClaim = new List<UserGroupIdentifier>();
                foreach (var userGroupClaim in similarUserGroupClaims)
                {
                    var userGroup = userGroupClaim.UserGroup.GetIdentifier();
                    model.UserGroupsWithSameClaim.Add(userGroup);
                }
            }

            return model;
        }


        private UserGroupClaimModel GetViewModel(UserGroupClaim entity)
        {
            var model = new UserGroupClaimModel();
            if (entity == null)
                return model;
            model.Identifier = entity.GetIdentifier();
            model.Description = entity.UserGroupClaimType.Description;

            return model;
        }

        public async Task<bool> SetUserGroups(UserGroupClaimSetUserGroupsModel model)
        {
            var result = await _UserGroupClaimDataProvider.GetListWithSameTypeAndValueAsync(model.Identifier);
            var current = new List<UserGroupIdentifier>();
            foreach (var userGroupClaim in result)
            {
                current.Add(userGroupClaim.UserGroup.GetIdentifier());
            }

            var submitted = model.UserGroups;
            var toRemove = new List<UserGroupIdentifier>();
            foreach (var currentUser in current)
            {
                if (!submitted.Any(u => u.IsEquals(currentUser)))
                {
                    toRemove.Add(currentUser);
                }
            }
            var toAdd = new List<UserGroupIdentifier>();
            foreach (var submittedUser in submitted)
            {
                if (!current.Any(u => u.IsEquals(submittedUser)))
                {
                    toAdd.Add(submittedUser);
                }
            }
            foreach (var userGroup in toRemove)
            {
                model.Identifier.UserGroupCode = userGroup.UserGroupCode;
                await DeleteAsync(model.Identifier);
            }
            foreach (var userGroup in toAdd)
            {
                var claimModel = new UserGroupClaimCreateModel();
                claimModel.TenantIdentifier = model.Identifier.GetTenantIdentifier();
                claimModel.UserGroupCode = userGroup.UserGroupCode;
                claimModel.Type = model.Identifier.Type;
                claimModel.Value = model.Identifier.Value;
                await CreateAsync(claimModel);
            }
            return true;
        }

        public async Task DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var list = await _UserGroupClaimDataProvider.GetListAsync(tenantIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
