﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserGroupClaimServiceProvider
    {
        Task InitAsync(TenantIdentifier tenantIdentifier);
        Task<UserGroupClaimModel> GetViewModelAsync(UserGroupClaimIdentifier claimIdentifier);
        Task<UserGroupClaimIdentifier> CreateAsync(UserGroupClaimCreateModel claim);
        Task<UserGroupClaimIdentifier> UpdateAsync(UserGroupClaimUpdateModel claim);
        Task<bool> DeleteAsync(UserGroupClaimIdentifier claim);
        Task<bool> SetUserGroups(UserGroupClaimSetUserGroupsModel model);
        Task<IList<UserGroupClaimModel>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task DeleteAllAsync(TenantIdentifier tenantIdentifier);
    }
}
