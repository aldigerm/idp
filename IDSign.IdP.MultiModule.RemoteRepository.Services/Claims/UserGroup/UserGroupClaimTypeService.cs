﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Implementation
{
    public class UserGroupClaimTypeService : IUserGroupClaimTypeServiceProvider
    {
        protected readonly ILogger<UserService> _logger;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly IUserGroupClaimTypeDataProvider _UserGroupClaimTypeDataProvider;
        protected readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        protected readonly IUserGroupClaimServiceProvider _UserGroupClaimServiceProvider;
        public UserGroupClaimTypeService(
             IHttpContextAccessor httpContextAccessor
            , ILogger<UserService> logger
            , IUserGroupClaimTypeDataProvider userGroupClaimTypeDataProvider
            , IUserGroupServiceProvider userGroupServiceProvider
            , IUserGroupClaimServiceProvider userGroupClaimServiceProvider
            )
        {
            _logger = logger;
            _HttpContextAccessor = httpContextAccessor;
            _UserGroupClaimTypeDataProvider = userGroupClaimTypeDataProvider;
            _UserGroupServiceProvider = userGroupServiceProvider;
            _UserGroupClaimServiceProvider = userGroupClaimServiceProvider;
        }

        public async Task InitAsync(ProjectIdentifier projectIdentifier)
        {
            await TryCreateAsync(new UserGroupClaimTypeCreateModel()
            {
                Description = "OnBoard Partner Code",
                ProjectIdentifier = projectIdentifier,
                Type = ClaimName.Partner
            });
        }

        private async Task TryCreateAsync(UserGroupClaimTypeCreateModel claim)
        {
            try
            {
                await CreateAsync(claim);
            }
            catch (IdPNotFoundException ex)
            {
                // we expect that it might exists, otherwise its an error
                if (!(ex.ErrorCode == ErrorCode.USER_CLAIM_TYPE_EXISTS))
                {
                    throw;
                }
            }
        }

        public async Task<UserGroupClaimTypeIdentifier> CreateAsync(UserGroupClaimTypeCreateModel claim)
        {
            var result = await _UserGroupClaimTypeDataProvider.GetSimpleAsync(claim.GetClaimTypeIdentifier());

            if (result != null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_EXISTS);
            }
            else
            {
                result = await _UserGroupClaimTypeDataProvider.CreateAsync(claim);
            }

            if (result == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't add update");
            }

            // get updated userGroup
            return claim.GetClaimTypeIdentifier();
        }

        public async Task<UserGroupClaimTypeIdentifier> UpdateAsync(UserGroupClaimTypeUpdateModel claim)
        {
            UserGroupClaimType result;
            if (!claim.NewType.Equals(claim.Identifier.Type, System.StringComparison.InvariantCultureIgnoreCase))
            {
                result = await _UserGroupClaimTypeDataProvider.GetSimpleAsync(new UserGroupClaimTypeIdentifier(claim.NewType, claim.Identifier.GetProjectIdentifier()));

                // check if new claim exists
                if (result != null)
                {
                    throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_EXISTS);
                }
            }

            // if type doesnt exist then an error will be thrown
            var identifier = await _UserGroupClaimTypeDataProvider.UpdateAsync(claim);

            if (identifier == null)
            {
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Couldn't update");
            }
            // get updated userGroup
            return identifier;
        }

        public async Task<bool> DeleteAsync(UserGroupClaimTypeIdentifier claim)
        {
            var result = await _UserGroupClaimTypeDataProvider.DeleteAsync(claim);
            if (result == 0)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND);
            }
            // get updated userGroup
            return true;
        }

        public async Task<List<UserGroupClaimTypeModel>> GetListAsync(ProjectIdentifier userGroupIdentifier)
        {
            var userGroupClaimTypes = await _UserGroupClaimTypeDataProvider.GetListAsync(userGroupIdentifier);
            List<UserGroupClaimTypeModel> list = new List<UserGroupClaimTypeModel>();
            foreach (var entity in userGroupClaimTypes)
            {
                var model = GetViewModel(entity);
                if (model != null)
                    list.Add(model);
            }
            return list;
        }

        public async Task<UserGroupClaimTypeModel> GetViewModelAsync(UserGroupClaimTypeIdentifier claimTypeIdentifier)
        {
            var result = await _UserGroupClaimTypeDataProvider.GetAsync(claimTypeIdentifier);
            if (result == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND);
            }
            return GetViewModel(result);
        }

        public async Task<bool> SetUserGroups(UserGroupClaimTypeSetUserGroupsModel model)
        {
            var result = await GetViewModelAsync(model.Identifier);
            var submittedUserGroups = new List<UserGroupIdentifier>();
            foreach (var u in model.UserGroups)
            {
                var claimModel = new UserGroupClaimCreateModel(model.Value, u, model);
                await _UserGroupClaimServiceProvider.CreateAsync(claimModel);
            }

            return true;
        }

        private UserGroupClaimTypeModel GetViewModel(UserGroupClaimType entity)
        {
            var model = new UserGroupClaimTypeModel();
            if (entity == null)
                return model;

            model.Identifier = new UserGroupClaimTypeIdentifier(entity.Type, entity.Project.GetIdentifier());
            model.Description = entity.Description;

            if (entity.UserGroupClaims != null)
            {
                foreach (var userGroup in entity.UserGroupClaims.Select(uc => uc.UserGroup))
                {
                    model.UserGroups.Add(_UserGroupServiceProvider.GetViewModel(userGroup));
                }
            }

            return model;
        }

        public async Task DeleteAllAsync(ProjectIdentifier projectIdentifier)
        {
            var list = await _UserGroupClaimTypeDataProvider.GetListAsync(projectIdentifier);
            foreach (var item in list)
            {
                await DeleteAsync(item.GetIdentifier());
            }
        }
    }
}
