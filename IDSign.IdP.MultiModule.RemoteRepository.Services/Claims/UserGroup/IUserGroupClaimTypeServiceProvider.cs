﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Providers
{
    public interface IUserGroupClaimTypeServiceProvider
    {
        Task<UserGroupClaimTypeIdentifier> CreateAsync(UserGroupClaimTypeCreateModel claim);
        Task<UserGroupClaimTypeIdentifier> UpdateAsync(UserGroupClaimTypeUpdateModel claim);
        Task<bool> DeleteAsync(UserGroupClaimTypeIdentifier claim);
        Task<List<UserGroupClaimTypeModel>> GetListAsync(ProjectIdentifier userGroupIdentifier);
        Task<UserGroupClaimTypeModel> GetViewModelAsync(UserGroupClaimTypeIdentifier userGroupIdentifier);
        Task<bool> SetUserGroups(UserGroupClaimTypeSetUserGroupsModel model);
        Task InitAsync(ProjectIdentifier projectIdentifier);
        Task DeleteAllAsync(ProjectIdentifier projectIdentifier);
    }
}
