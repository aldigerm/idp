﻿using IDSign.IdP.MultiModule.RemoteRepository.Model;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services
{
    public static class EncryptionHelper
    {
        public static string EncryptValue(AppUser user, string password)
        {
            return new PasswordHasher().HashPassword(user, password);
        }

        public static bool VerifyEncryptedValue(AppUser user, string hashedValue, string value)
        {
            var result = VerifyHashWithStatusPassword(user, hashedValue, value);
            return result  == PasswordVerificationResult.Success || result == PasswordVerificationResult.SuccessRehashNeeded;
        }

        public static PasswordVerificationResult VerifyHashWithStatusPassword(AppUser user, string hashedValue, string value)
        {
            return new PasswordHasher().VerifyHashedPassword(user, hashedValue, value); 
        }
    }
}
