using IDSign.IdP.Core;
using IDSign.IdP.Core.Config;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Data;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;
using Module = IDSign.IdP.MultiModule.RemoteRepository.Model.Module;

namespace IDSign.IdP.MultiModule.RemoteRepository.Services.Tests
{
    public class UserServiceTest
    {
        IPermissionsServiceProvider _PermissionServiceProvider;
        IRoleServiceProvider _RoleServiceProvider;
        IUserSharedServiceProvider _UserServiceProvider;
        IModuleServiceProvider _ModuleServiceProvider;
        private Role supportRole;
        private Role pspRole;
        private Role riskRole;
        private Role userRole;
        private Role internalUserRole;
        private Role tenantAdminRole;
        private Role partnerRole;
        private Role pspSupervisorRole;
        private Role riskSupervisorRole;
        public IServiceProvider serviceProvider;

        async Task Init()
        {
            #region Dependency Injection initialisation
            var serviceCollection = new ServiceCollection();

            string connectionString = "Server=localhost;Database=IDSign.IdP.RemoteRepository;User ID=sa_idsign_idp_users;Password=sa_idsign_idp_users;MultipleActiveResultSets=true";
            string efMigrationAssemblies = typeof(RemoteRepositoryDbContextInitializer).GetTypeInfo().Assembly.GetName().Name;

            serviceCollection
                .AddServicesLayerServices()
                .AddDataLayerServices()
                .AddAutoMapper()
                .AddDbContext<RemoteRepositoryDbContext>(options =>
                    options.UseSqlServer(connectionString));

            
            serviceCollection.AddTransient<IClaimsTransformation, RemoteRepositoryClaimsTransformer>();
            serviceCollection.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // claims transformation to get custom user
            serviceCollection.AddTransient<ClaimsPrincipal>(
                provider =>
                {
                    return new ClaimsPrincipal();
                });
            serviceProvider = serviceCollection.BuildServiceProvider();
            #endregion

            AppSettingsConfigValue.Init("", null);


            _PermissionServiceProvider = serviceProvider.GetService<IPermissionsServiceProvider>();
            _RoleServiceProvider = serviceProvider.GetService<IRoleServiceProvider>();
            _UserServiceProvider = serviceProvider.GetService<IUserSharedServiceProvider>();
            _ModuleServiceProvider = serviceProvider.GetService<IModuleServiceProvider>();
            
            // initialise database
            var ctx = serviceProvider.GetService<RemoteRepositoryDbContext>();
            var initialiseDatabase = serviceProvider.GetService<ILogger<RemoteRepositoryDbContextInitializer>>();
            RemoteRepositoryDbContextInitializer.InitializeDatabase(ctx, initialiseDatabase);

            var roleIdentifier = new RoleIdentifier(SupportConstants.SupportRoleCode, Model.Constants.GetInitialTenantIdentifier());

            supportRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.Psp;

            pspRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.Risk;
            riskRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.User;
            userRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.InternalUser;
            internalUserRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = SupportConstants.TenantAdminRoleCode;
            tenantAdminRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.Partner;
            partnerRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.PspSupervisor;
            pspSupervisorRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);

            roleIdentifier.RoleCode = RoleCode.RiskSupervisor;
            riskSupervisorRole = await _RoleServiceProvider.GetRoleAsync(roleIdentifier);
        }

        [Fact]
        public async Task Test_UserPermissionsRecursively()
        {
            await Init();

            // superusers
            List<Role> roles = new List<Role>() { supportRole };
            var modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.True(modules.HasModule(SupportConstants.SupportModuleCode));
            // these are admin functions
            SupervisorModules(modules, true);
            // psp view
            RiskModules(modules, true);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // psp user
            PSPModules(modules, true);

            // psp users
            roles = new List<Role>() { pspRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // psp do not have admin functions
            SupervisorModules(modules, false);
            // psp can't view risk
            RiskModules(modules, false);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // psp user
            PSPModules(modules, true);

            // psp super visors users
            roles = new List<Role>() { pspSupervisorRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // supervisors have admin functions
            SupervisorModules(modules, true);
            // psp can't view risk
            RiskModules(modules, false);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // psp user
            PSPModules(modules, true);

            // risk super visors users
            roles = new List<Role>() { riskSupervisorRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // supervisors have admin functions
            SupervisorModules(modules, true);
            // psp can't view risk
            RiskModules(modules, true);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // psp user
            PSPModules(modules, false);

            // risk users
            roles = new List<Role>() { riskRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // risk do not have admin functions
            SupervisorModules(modules, false);
            // risk can't view psp
            PSPModules(modules, false);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // risk user
            RiskModules(modules, true);

            // risk and psp
            roles = new List<Role>() { riskRole, pspRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // risk/psp do not have admin functions
            SupervisorModules(modules, false);
            // psp
            PSPModules(modules, true);
            // basic user
            BasicUserModules(modules, true);
            // risk user
            RiskModules(modules, true);

            // admin users
            roles = new List<Role>() { tenantAdminRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // basic user
            BasicUserModules(modules, true);
            // these are admin functions
            SupervisorModules(modules, true);
            // psp
            PSPModules(modules, true);
            // risk user
            RiskModules(modules, true);

            // partner users
            roles = new List<Role>() { partnerRole };
            modules = await _PermissionServiceProvider.GetInferredModulesAsync(roles);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // these are admin functions
            SupervisorModules(modules, false);
            // psp
            PSPModules(modules, false);
            // basic user
            BasicUserModules(modules, true);
            // risk user
            RiskModules(modules, false);
            // partner user
            PartnerModules(modules, true);
        }

        [Fact]
        public async Task Test_Users()
        {
            await Init();

            // superusers
            var userRequest = new UserIdentifier("risk_user_1", "bspay", "idonboard");
            var user = await _UserServiceProvider.GetUserAsync(userRequest, false, true, null);
            var modules = await GetModules(user.AllowedModulesCodes, userRequest);
            Assert.False(modules.HasModule(SupportConstants.SupportModuleCode));
            // risk do not have admin functions
            SupervisorModules(modules, false);
            // risk can't view psp
            PSPModules(modules, false);
            // basic user
            BasicUserModules(modules, true);
            // internal user
            InternalUserModules(modules, true);
            // risk user
            RiskModules(modules, true);
        }

        async Task<IList<Module>> GetModules(IList<string> modules, UserIdentifier userIdentifier)
        {
            var parsed = new List<Module>();
            foreach (var moduleCode in modules)
            {
                var module = await _ModuleServiceProvider.GetAsync(new ModuleIdentifier(moduleCode, userIdentifier.GetProjectIdentifier()));
                if (module != null)
                {
                    parsed.Add(module);
                }
            }
            return parsed;
        }

        void PartnerModules(IList<Module> modules, bool shouldBeFound)
        {
            BasicUserModules(modules, shouldBeFound);
        }

        void BasicUserModules(IList<Module> modules, bool shouldBeFound)
        {
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.USER));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DASHBOARD_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_VIEW_PERSONAL));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_LIST_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_CREATE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_BASIC));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.COUNTRY_FILE_DOWNLOAD));

            // tasks
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_PERSONAL_CREATE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_PERSONAL_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_PERSONAL_CLOSE));
        }

        void InternalUserModules(IList<Module> modules, bool shouldBeFound)
        {
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_EXTENDED));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_REJECT_REASON_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_INTERNAL_STEPS_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_VIEW_ALL));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.USER_INTERNAL));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_POST_COMPLETE_ACTION));
        }
        void SupervisorModules(IList<Module> modules, bool shouldBeFound)
        {
            // dossier related
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_HISTORY_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_DATA_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_UPDATE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_EDIT));

            // tasks
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_ALL_CREATE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_ALL_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_ALL_CLOSE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.TASK_ALL_REASSIGN));

            // supervisors are internal users
            if (shouldBeFound)
            {
                InternalUserModules(modules, true);
            }
        }

        void PSPModules(IList<Module> modules, bool shouldBeFound)
        {
            // psp modules
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_PSP_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_SEND_TO_RISK));

            // psp are internal users
            if (shouldBeFound)
            {
                InternalUserModules(modules, true);
                CommonModules(modules, true);
            }
        }

        void CommonModules(IList<Module> modules, bool shouldBeFound)
        {
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_COLLECT_DOCS_ACTION));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_SEND_BACK_TO_PARTNER));
        }

        void RiskModules(IList<Module> modules, bool shouldBeFound)
        {
            // risk modules
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_RISK_VIEW));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_RISK_VERIFY));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_RISK_CREATE_MERCHANT_IDS));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_RISK_UPLOAD_FILE));
            Assert.Equal<bool>(shouldBeFound, modules.HasModule(ModuleCode.DOSSIER_DETAIL_RISK_PERMANENT_REJECT));

            // risk are internal users
            if (shouldBeFound)
            {
                InternalUserModules(modules, true);
                CommonModules(modules, true);
            }
        }
    }
}
