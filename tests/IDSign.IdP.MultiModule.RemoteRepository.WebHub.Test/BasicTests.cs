using System;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using IdentityModel.Client;
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Test
{
    public class BasicTests : IClassFixture<StartupStateApiFactory>
    {
        private readonly StartupStateApiFactory _factory;
        private readonly TokenClient _tokenClient;

        public BasicTests(StartupStateApiFactory factory)
        {
            _factory = factory;
            
            var disco = DiscoveryClient.GetAsync("https://vaa-idp.azurewebsites.net/.well-known/openid-configuration").Result;
            _tokenClient = new TokenClient(disco.TokenEndpoint, "user-repository", "user-repository-secret");
            var result = _tokenClient.RequestClientCredentialsAsync("remote-user-repository-admin").Result;
            var bearerToken = result.AccessToken;
        }

        [Theory]
        [InlineData("/api/User/get")]
        [InlineData("/api/Tenant/get")]
        [InlineData("/api/Project/get")]
        [InlineData("/api/Module/get")]
        [InlineData("/api/UserGroup/get")]
        [InlineData("/api/Role/get")]
        public async Task Get_EndpointsForbidden(string url)
        {
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            Assert.Equal(StatusCodes.Status403Forbidden, (int)response.StatusCode); // 403 as we haven't authenticated
        }

        [Theory]
        [InlineData("/api/User/get")]
        [InlineData("/api/Tenant/get")]
        [InlineData("/api/Project/get")]
        [InlineData("/api/Module/get")]
        [InlineData("/api/UserGroup/get")]
        [InlineData("/api/Role/get")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {

            var client = _factory.CreateClient();

            // Arrange
            var tokenResponse = await _tokenClient.RequestResourceOwnerPasswordAsync("bspay_admin", "bspay_admin", "openid idonboard-webhub remote-user-repository-basic");
            client.SetBearerToken(tokenResponse.AccessToken);

            // Act
            var response = await client.GetAsync(url);

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode); // should allow access
        }
    }
}
