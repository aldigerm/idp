﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model
{
    public class HttpContextKey
    {
        public static readonly string ConnectionId = "ConnectionId";
    }
}
