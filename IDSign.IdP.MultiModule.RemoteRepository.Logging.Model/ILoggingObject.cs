﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model
{
    public interface ILoggingObject
    {
        int Id { get; set; }
        string ConnectionId { get; set; }
        string CompanyCode { get; set; }
        string User { get; set; }
        string RequestIpAddress { get; set; }        // The IP address that made the request.
        string RequestContentType { get; set; }      // The request content type.
        string RequestContentBody { get; set; }      // The request content body.
        string RequestScheme { get; set; }           // The request http/https.
        string RequestAuthority { get; set; }        // The domain.
        string RequestAbsolutePath { get; set; }     // The url.
        string RequestQuery { get; set; }            // The request query string.
        string RequestMethod { get; set; }           // The request method (GET, POST, etc).
        string RequestHeaders { get; set; }          // The request headers

        [XmlElement("LogTimestamp")]
        [NotMapped]
        string LogTimestampString // format: 2011-11-11T15:05:46.4733406+01:00
        {
            get; set;        }

        [XmlIgnore]
        DateTimeOffset? LogTimestamp { get; set; }
        string Server { get; set; }
        string RequestMachineName { get; set; }
        string Application { get; set; }             // The application that made the request.
    }
}
