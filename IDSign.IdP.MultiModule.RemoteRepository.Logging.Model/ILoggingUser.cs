﻿namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model
{
    public interface ILoggingUser
    {
        string UserName { get; set; }
        string CompanyCode { get; set; }
    }

    public class LoggingUser : ILoggingUser
    {
        public LoggingUser(string username, string companyCode)
        {
            this.UserName = username;
            this.CompanyCode = companyCode;
        }

        public string UserName { get; set; }
        public string CompanyCode { get; set; }
    }
}
