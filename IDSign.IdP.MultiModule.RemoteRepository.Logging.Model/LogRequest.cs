﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model
{
    public class LogRequest
    {
        public string Source { get; set; }
        public string Message { get; set; }
    }
}
