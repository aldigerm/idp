﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model
{
    public class AuditLog : ILoggingObject
    {

        #region BaseLog
        public int Id { get; set; }
        public string ConnectionId { get; set; }
        public string CompanyCode { get; set; }
        public string User { get; set; }
        public string RequestIpAddress { get; set; }        // The IP address that made the request.
        public string RequestContentType { get; set; }      // The request content type.
        public string RequestContentBody { get; set; }      // The request content body.
        public string RequestScheme { get; set; }           // The request http/https.
        public string RequestAuthority { get; set; }        // The domain.
        public string RequestAbsolutePath { get; set; }     // The url.
        public string RequestQuery { get; set; }            // The request query string.
        public string RequestMethod { get; set; }           // The request method (GET, POST, etc).
        public string RequestHeaders { get; set; }          // The request headers

        [XmlElement("LogTimestamp")]
        [NotMapped]
        public string LogTimestampString // format: 2011-11-11T15:05:46.4733406+01:00
        {
            get { return LogTimestamp.HasValue ? LogTimestamp.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz") : null; }
            set { LogTimestamp = DateTimeOffset.Parse(value); }
        }

        [XmlIgnore]
        public DateTimeOffset? LogTimestamp { get; set; }
        public string Server { get; set; }
        public string RequestMachineName { get; set; }
        public string Application { get; set; }             // The application that made the request.
        #endregion

        /// <summary>
        /// Should be an Enums.AuditEventType
        /// </summary>
        public string AuditEventType { get; set; }
        public string ClientUserAgent { get; set; }

        [XmlElement("ResponseTimestamp")]
        [NotMapped]
        public string ResponseTimestampString // format: 2011-11-11T15:05:46.4733406+01:00
        {
            get { return ResponseTimestamp.HasValue ? ResponseTimestamp.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz") : null; }
            set { ResponseTimestamp = DateTimeOffset.Parse(value); }
        }

        [XmlIgnore]
        public DateTimeOffset? ResponseTimestamp { get; set; }

        [XmlElement("ErrorTimestamp")]
        [NotMapped]
        public string ErrorTimestampString // format: 2011-11-11T15:05:46.4733406+01:00
        {
            get { return ErrorTimestamp.HasValue ? ErrorTimestamp.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz") : null; }
            set { ErrorTimestamp = DateTimeOffset.Parse(value); }
        }

        [XmlIgnore]
        public DateTimeOffset? ErrorTimestamp { get; set; }
    }
}
