﻿using System.Collections.Generic;
using System.Linq;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Data.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddDataLayerServices(this IServiceCollection services)
        {
            services.AddTransient<IRoleDataProvider, RoleData>();
            services.AddTransient<ITenantDataProvider, TenantData>();
            services.AddTransient<IProjectDataProvider, ProjectData>();
            services.AddTransient<IUserDataProvider, UserData>();
            services.AddTransient<IModuleDataProvider, ModuleData>();
            services.AddTransient<IUserGroupDataProvider, UserGroupData>();
            services.AddTransient<IUserClaimDataProvider, UserClaimData>();
            services.AddTransient<IUserClaimTypeDataProvider, UserClaimTypeData>();

            services.AddTransient<IUserGroupClaimDataProvider, UserGroupClaimData>();
            services.AddTransient<IUserGroupClaimTypeDataProvider, UserGroupClaimTypeData>();

            services.AddTransient<IRoleClaimDataProvider, RoleClaimData>();
            services.AddTransient<IRoleClaimTypeDataProvider, RoleClaimTypeData>();

            services.AddTransient<ITenantClaimDataProvider, TenantClaimData>();
            services.AddTransient<ITenantClaimTypeDataProvider, TenantClaimTypeData>();

            services.AddTransient<IUserGroupManagementDataProvider, UserGroupManagementData>();
            services.AddTransient<IUserTenantDataProvider, UserTenantData>();

            services.AddTransient<IPasswordPolicyDataProvider, PasswordPolicyData>();
            services.AddTransient<IPasswordPolicyTypeDataProvider, PasswordPolicyTypeData>();
            services.AddTransient<IPasswordPolicyUserGroupDataProvider, PasswordPolicyUserGroupData>();

            services.AddTransient<IUserPreviousPasswordDataProvider, UserPreviousPasswordData>();
            services.AddTransient<IUserTenantSessionDataProvider, UserTenantSessionData>();

            return services;
        }
    }
}