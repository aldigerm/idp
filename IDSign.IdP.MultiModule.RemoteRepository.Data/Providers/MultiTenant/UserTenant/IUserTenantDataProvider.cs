﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserTenantDataProvider
    {
        Task<UserTenant> GetAsync(UserTenantIdentifier tenantIdentifier);
        Task<UserTenant> GetSimpleAsync(UserTenantIdentifier tenantIdentifier);
        Task<IList<UserTenant>> GetListAsync();
        Task<IList<UserTenant>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<IList<UserTenant>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<UserTenantIdentifier> CreateAsync(UserTenantCreateModel model);
        Task<UserTenantIdentifier> UpdateAsync(UserTenantUpdateViewModel model);
        Task<bool> ExistsAsync(UserTenantIdentifier tenantIdentifier);
        Task<int> DeleteAsync(UserTenantIdentifier tenantIdentifier);
    }
}
