﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserTenantData : IUserTenantDataProvider
    {
        private RemoteRepositoryDbContext ctx;
        private IProjectDataProvider _ProjectDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private IUserDataProvider _UserDataProvider;
        private ILogger<UserTenantData> _logger;
        public UserTenantData
            (RemoteRepositoryDbContext _ctx
            , IProjectDataProvider projectDataProvider
            , ITenantDataProvider tenantDataProvider
            , IUserDataProvider userDataProvider
            , ILogger<UserTenantData> logger)
        {
            _logger = logger;
            ctx = _ctx;
            _ProjectDataProvider = projectDataProvider;
            _TenantDataProvider = tenantDataProvider;
            _UserDataProvider = userDataProvider;
        }

        public async Task<UserTenant> GetSimpleAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var tenant = await ctx.UserTenants
                    .AsNoTracking()
                    .SingleOrDefaultAsync(c => c.Tenant.Code == userTenantIdentifier.TenantCode && c.Tenant.Project.Code == userTenantIdentifier.ProjectCode
                    && c.User.TenantUsername == userTenantIdentifier.Username);

            if (tenant == null)
            {
                _logger.LogWarning("Tenant not found {0}", userTenantIdentifier);
            }
            return tenant;
        }

        public async Task<UserTenant> GetAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var tenant = await ctx.UserTenants
                    .Include(ut => ut.User)
                    .Include(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                    .AsNoTracking()
                    .SingleOrDefaultAsync(c => c.Tenant.Code == userTenantIdentifier.TenantCode && c.Tenant.Project.Code == userTenantIdentifier.ProjectCode
                    && c.User.TenantUsername == userTenantIdentifier.Username);

            if (tenant == null)
            {
                _logger.LogWarning("Tenant not found {0}", userTenantIdentifier);
            }
            return tenant;
        }

        public async Task<IList<UserTenant>> GetListAsync()
        {
            IList<UserTenant> list;
            list = await ctx.UserTenants
                .Include(ut => ut.User)
                .Include(ut => ut.Tenant)
                    .ThenInclude(t => t.Project)
                .AsNoTracking()
                .ToListAsync();
            return list;
        }

        public async Task<IList<UserTenant>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var project = await _ProjectDataProvider.GetAsync(projectIdentifier);
            if (project == null)
            {
                return null;
            }

            return await ctx.UserTenants
                    .Include(ut => ut.User)
                    .Include(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                    .AsNoTracking()
                    .Where(t => t.Tenant.Project.Id == project.Id)
                    .ToListAsync();
        }

        public async Task<IList<UserTenant>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<UserTenant> appUsers = new List<UserTenant>();

            // if there is a tenant code, then we need a project code
            if (!string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode) && string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                throw new IdPException(ErrorCode.GENERIC_ERROR, "A project code is needed when supplying a tenant code");

            appUsers = await ctx.UserTenants
                    .Include(ut => ut.User)
                    .Include(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                // if there is a project code then get for the project, otherwise get all
                // if there isn't a tenant code, then get for the given project
                .Where(ut => (ut.Tenant.Project.Code == tenantIdentifier.ProjectCode || string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                && (ut.Tenant.Code == tenantIdentifier.TenantCode || string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode)))
                .AsNoTracking()
                .ToListAsync();

            return appUsers;
        }

        public async Task<UserTenantIdentifier> UpdateAsync(UserTenantUpdateViewModel model)
        {
            UserTenant userTenant = await GetSimpleAsync(model.Identifier);

            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND);
            }

            var tenant = await _TenantDataProvider.GetAsync(model.Identifier.GetTenantIdentifier());

            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.Identifier.GetTenantIdentifier().ToString());
            }

            userTenant.UserName = model.RepositoryUsername ?? userTenant.UserName;

            ctx.UserTenants.Update(userTenant);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userTenant);

            return await GetIdentifier(userTenant);
        }

        private async Task<UserTenantIdentifier> GetIdentifier(UserTenant entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<UserTenant> GetAsync(int id)
        {
            return await ctx.UserTenants
                     .Include(ut => ut.User)
                     .Include(ut => ut.Tenant)
                         .ThenInclude(t => t.Project)
                     .AsNoTracking()
                     .SingleOrDefaultAsync(c => c.Id == id);
        }

        public async Task<UserTenantIdentifier> CreateAsync(UserTenantCreateModel model)
        {
            
            var tenant = await _TenantDataProvider.GetAsync(model.TenantIdentifier);

            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.TenantIdentifier.ToString());
            }

            AppUser user = null;
            if (model.UserIdentifier != null)
            {
                user = await _UserDataProvider.GetAsync(model.UserIdentifier);
            }
            else
            {
                user = await _UserDataProvider.GetAsync(model.UserId);
            }

            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, model.UserIdentifier.ToString());
            }

            UserTenant userTenant = await ctx.UserTenants.FirstOrDefaultAsync(ut => ut.TenantId == tenant.Id && ut.UserId == user.Id);

            if (userTenant != null)
            {
                throw new IdPException(ErrorCode.USERTENANT_ALREADY_EXISTS);
            }

            userTenant = new UserTenant();
            userTenant.TenantId = tenant.Id;
            userTenant.UserId = user.Id;
            userTenant.UserName = BuildUserName(user.GetIdentifier(tenant.GetIdentifier()));

            ctx.UserTenants.Add(userTenant);

            await ctx.SaveChangesAsync();

            return await GetIdentifier(userTenant);
        }

        public async Task<bool> ExistsAsync(UserTenantIdentifier userTenantIdentifier)
        {
            return await ctx.UserTenants.AnyAsync(t => t.Tenant.Code == userTenantIdentifier.TenantCode
            && t.Tenant.Project.Code == userTenantIdentifier.ProjectCode
            && t.User.TenantUsername == userTenantIdentifier.Username);
        }

        public async Task<UserTenant> GetToDeleteAsync(UserTenantIdentifier userTenantIdentifier)
        {
            UserTenant userTenant = await ctx.UserTenants
                .Include(ut => ut.UserTenantUserGroups)
                .Include(ut => ut.UserGroupManagement)
                .Include(ut => ut.UserClaims)
                .AsNoTracking()
                .SingleOrDefaultAsync(t => t.Tenant.Code == userTenantIdentifier.TenantCode
                    && t.Tenant.Project.Code == userTenantIdentifier.ProjectCode
                    && t.User.TenantUsername == userTenantIdentifier.Username);

            if (userTenant == null)
            {
                _logger.LogWarning("UserTenant not found {0}", userTenantIdentifier.ToString());
            }
            return userTenant;
        }

        public async Task<int> DeleteAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var entity = await GetToDeleteAsync(userTenantIdentifier);
            if (entity == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND, userTenantIdentifier.ToString());
            }

            if (entity.UserClaims != null)
            {
                ctx.UserClaims.RemoveRange(entity.UserClaims);
            }
            if (entity.UserTenantUserGroups != null)
            {
                ctx.ApplicationUserToUserGroups.RemoveRange(entity.UserTenantUserGroups);
            }
            if (entity.UserGroupManagement != null)
            {
                ctx.UserToUserGroupManagement.RemoveRange(entity.UserGroupManagement);
            }

            ctx.UserTenants.Remove(entity);

            await ctx.SaveChangesAsync();

            return 1;
        }

        private string BuildUserName(UserIdentifier userIdentifier)
        {
            return userIdentifier.Username + "@" + userIdentifier.TenantCode + "." + userIdentifier.ProjectCode;
        }
    }
}
