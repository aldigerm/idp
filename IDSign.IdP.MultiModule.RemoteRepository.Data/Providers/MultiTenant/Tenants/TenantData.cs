﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class TenantData : ITenantDataProvider
    {
        private RemoteRepositoryDbContext ctx;
        private IProjectDataProvider _ProjectDataProvider;
        private ILogger<TenantData> _logger;
        public TenantData
            (RemoteRepositoryDbContext _ctx
            , IProjectDataProvider projectDataProvider
            , ILogger<TenantData> logger)
        {
            _logger = logger;
            ctx = _ctx;
            _ProjectDataProvider = projectDataProvider;
        }

        public async Task<Tenant> GetSimpleAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await ctx.Tenants
                    .AsNoTracking()
                    .SingleOrDefaultAsync(c => c.Code == tenantIdentifier.TenantCode && c.Project.Code == tenantIdentifier.ProjectCode);

            if (tenant == null)
            {
                _logger.LogWarning("Tenant not found {0}", tenantIdentifier);
            }
            return tenant;
        }

        public async Task<Tenant> GetAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await ctx.Tenants
                    .TenantFilter(tenantIdentifier)
                    .SingleOrDefaultAsync();

            if (tenant == null)
            {
                _logger.LogWarning("Tenant not found {0}", tenantIdentifier);
            }
            return tenant;
        }

        public async Task<IList<Tenant>> GetListAsync()
        {
            IList<Tenant> list;
            list = await ctx.Tenants
                .TenantFilter()
                .ToListAsync();
            return list;
        }


        public async Task<TenantIdentifier> CreateOrUpdateAndGetTenantAsync(TenantUpdateViewModel model)
        {
            Tenant tenant = await GetAsync(model.Identifier);
            if (tenant == null)
            {
                return await CreateAsync(new TenantCreateModel()
                {
                    ProjectIdentifier = new ProjectIdentifier(model.Identifier.ProjectCode),
                    TenantCode = model.Identifier.TenantCode,
                    TenantName = model.Name
                });
            }
            else
            {
                return await UpdateAsync(model);
            }
        }

        public async Task<IList<Tenant>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            var project = await _ProjectDataProvider.GetAsync(projectIdentifier);
            if (project == null)
            {
                return null;
            }

            return await ctx.Tenants
                    .TenantFilter(projectIdentifier)
                    .Where(t => t.ProjectId == project.Id)
                    .ToListAsync();
        }

        public async Task<TenantIdentifier> UpdateAsync(TenantUpdateViewModel model)
        {
            Tenant tenant = await GetSimpleAsync(model.Identifier);

            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.Identifier.ToString());
            }

            var project = await _ProjectDataProvider.GetAsync(model.Identifier.GetProjectIdentifier());

            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, "Project not found " + model.Identifier.ProjectCode);
            }

            tenant.Code = model.Identifier.TenantCode;
            tenant.Name = model.Name;
            tenant.ProjectId = project.Id;
            tenant.Enabled = model.Enabled.GetValueOrDefault(true);

            ctx.Tenants.Update(tenant);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(tenant);

            return await GetIdentifier(tenant);
        }

        private async Task<TenantIdentifier> GetIdentifier(Tenant entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<Tenant> GetAsync(int id)
        {
            return await ctx.Tenants
                     .TenantFilter()
                     .SingleOrDefaultAsync(c => c.Id == id);
        }

        public async Task<TenantIdentifier> CreateAsync(TenantCreateModel model)
        {
            Tenant tenant = await GetAsync(model.GetTenantIdentifier());

            if (tenant != null)
            {
                throw new IdPException(ErrorCode.TENANT_ALREADY_EXISTS);
            }

            tenant = new Tenant();
            ctx.Tenants.Add(tenant);

            var project = await _ProjectDataProvider.GetAsync(model.ProjectIdentifier);

            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, "Project not found " + model.ProjectIdentifier);
            }

            tenant.Code = model.TenantCode;
            tenant.Name = model.TenantName;
            tenant.ProjectId = project.Id;

            await ctx.SaveChangesAsync();

            return await GetIdentifier(tenant);
        }

        public async Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier)
        {
            return await ctx.Tenants.AnyAsync(t => t.Code == tenantIdentifier.TenantCode && t.Project.Code == tenantIdentifier.ProjectCode);
        }

        public async Task<int> DeleteAsync(TenantIdentifier tenantIdentifier)
        {
            var entity = await GetSimpleAsync(tenantIdentifier);
            if (entity == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, tenantIdentifier.ToString());
            }

            ctx.Tenants.Remove(entity);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
