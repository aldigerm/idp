﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface ITenantDataProvider
    {
        Task<Tenant> GetAsync(TenantIdentifier tenantIdentifier);
        Task<Tenant> GetSimpleAsync(TenantIdentifier tenantIdentifier);
        Task<IList<Tenant>> GetListAsync();
        Task<IList<Tenant>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<TenantIdentifier> CreateOrUpdateAndGetTenantAsync(TenantUpdateViewModel model);
        Task<TenantIdentifier> CreateAsync(TenantCreateModel model);
        Task<TenantIdentifier> UpdateAsync(TenantUpdateViewModel model);
        Task<bool> ExistsAsync(TenantIdentifier tenantIdentifier);
        Task<int> DeleteAsync(TenantIdentifier tenantIdentifier);
    }
}
