﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IProjectDataProvider
    {
        Task<Project> GetAsync(ProjectIdentifier projectIdentifier);
        Task<Project> GetSimpleAsync(ProjectIdentifier projectIdentifier);
        Task<IList<Project>> GetListAsync();
        Task<Project> CreateOrUpdateAndGetProjectAsync(ProjectUpdateViewModel model);
        Task<Project> CreateAsync(ProjectCreateModel model);
        Task<ProjectIdentifier> UpdateAsync(ProjectUpdateViewModel model);
        Task<int> DeleteAsync(ProjectIdentifier projectIdentifier);
    }
}
