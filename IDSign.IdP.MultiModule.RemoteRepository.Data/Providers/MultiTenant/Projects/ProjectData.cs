﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class ProjectData : IProjectDataProvider
    {
        private RemoteRepositoryDbContext ctx;
        public ProjectData
            (RemoteRepositoryDbContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<Project> GetAsync(ProjectIdentifier projectIdentifier)
        {
            var project = new Project();
            project = await ctx.Projects
                .Include(p => p.Tenants)
                .Include(p => p.Modules)
                .Include(p => p.UserClaimTypes)
                .Include(p => p.UserGroupClaimTypes)
                .Include(p => p.RoleClaimTypes)
                .Include(p => p.TenantClaimTypes)
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Code == projectIdentifier.ProjectCode);
            return project;
        }

        public async Task<Project> GetSimpleAsync(ProjectIdentifier projectIdentifier)
        {
            var project = new Project();
            project = await ctx.Projects
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Code == projectIdentifier.ProjectCode);
            return project;
        }

        public async Task<Project> GetToDeleteAsync(ProjectIdentifier projectIdentifier)
        {
            var project = new Project();
            project = await ctx.Projects
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Code == projectIdentifier.ProjectCode);
            return project;
        }

        public async Task<IList<Project>> GetListAsync()
        {
            IList<Project> list;
            list = await ctx.Projects
                .Include(p => p.Tenants)
                .Include(p => p.Modules)
                .Include(p => p.UserClaimTypes)
                .Include(p => p.UserGroupClaimTypes)
                .Include(p => p.RoleClaimTypes)
                .Include(p => p.TenantClaimTypes)
                .AsNoTracking()
                .ToListAsync();
            return list;
        }


        public async Task<Project> CreateOrUpdateAndGetProjectAsync(ProjectUpdateViewModel model)
        {
            Project tenant = await GetAsync(model.Identifier);
            if (tenant == null)
            {
                return await CreateAsync(new ProjectCreateModel()
                {
                    ProjectCode = model.Identifier.ProjectCode,
                    ProjectName = model.Name
                });
            }
            else
            {
                return await GetAsync(await UpdateAsync(model));
            }
        }


        public async Task<ProjectIdentifier> UpdateAsync(ProjectUpdateViewModel model)
        {

            Project project = await GetSimpleAsync(model.Identifier);
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND);
            }

            project.Code = model.NewProjectCode ?? model.Identifier.ProjectCode;
            project.Name = model.Name;
            project.Enabled = model.Enabled.GetValueOrDefault(true);

            ctx.Projects.Update(project);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(project);

            return project.GetIdentifier();
        }

        public async Task<Project> CreateAsync(ProjectCreateModel model)
        {
            Project project = await GetAsync(model.GetProjectIdentifier());

            if (project != null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_ALREADY_EXISTS);
            }

            project = new Project();
            ctx.Projects.Add(project);

            project.Code = model.ProjectCode;
            project.Name = model.ProjectName;

            await ctx.SaveChangesAsync();

            return project;
        }

        public async Task<int> DeleteAsync(ProjectIdentifier projectIdentifier)
        {
            var entity = await GetToDeleteAsync(projectIdentifier);
            if (entity == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, projectIdentifier.ToString());
            }

            ctx.Projects.Remove(entity);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
