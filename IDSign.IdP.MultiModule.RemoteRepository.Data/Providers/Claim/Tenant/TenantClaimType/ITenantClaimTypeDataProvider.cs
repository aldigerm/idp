﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface ITenantClaimTypeDataProvider
    {
        Task<TenantClaimType> GetAsync(TenantClaimTypeIdentifier identifier);
        Task<TenantClaimType> GetSimpleAsync(TenantClaimTypeIdentifier identifier);
        Task<TenantClaimType> CreateAsync(TenantClaimTypeCreateModel model);
        Task<int> DeleteAsync(TenantClaimTypeIdentifier userIdentifier);
        Task<TenantClaimTypeIdentifier> UpdateAsync(TenantClaimTypeUpdateModel model);
        Task<List<TenantClaimType>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task RemoveTenant(TenantIdentifier tenant, TenantClaimTypeSetTenantsModel model);
    }
}
