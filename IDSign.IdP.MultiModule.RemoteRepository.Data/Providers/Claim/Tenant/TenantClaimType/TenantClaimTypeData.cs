﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class TenantClaimTypeData : ITenantClaimTypeDataProvider
    {
        private IProjectDataProvider _ProjectDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<TenantClaimTypeData> _logger;
        public TenantClaimTypeData(
            IProjectDataProvider projectDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<TenantClaimTypeData> logger
            , ITenantDataProvider tenantDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _ProjectDataProvider = projectDataProvider;
            _TenantDataProvider = tenantDataProvider;
        }

        public async Task<TenantClaimType> GetSimpleAsync(TenantClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.TenantClaimTypes.Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<TenantClaimType> GetAsync(TenantClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.TenantClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.TenantClaims)
                    .ThenInclude(uc => uc.Tenant)
                        .ThenInclude(u => u.Project)
                .Where(uct => uct.ProjectId == project.Id && uct.Type == identifier.Type)
                .FirstOrDefaultAsync();
        }

        public async Task<TenantClaimType> CreateAsync(TenantClaimTypeCreateModel model)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(model.GetClaimTypeIdentifier().GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, model.ToString());
            }

            var tenantClaimType = await GetSimpleAsync(model.GetClaimTypeIdentifier());

            if (tenantClaimType != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.TENANT_CLAIM_TYPE_EXISTS, model.ToString());
            }

            tenantClaimType = new TenantClaimType();

            tenantClaimType.Type = model.Type;
            tenantClaimType.ProjectId = project.Id;
            tenantClaimType.Description = model.Description;

            await ctx.TenantClaimTypes.AddAsync(tenantClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(tenantClaimType);

            return tenantClaimType;
        }

        public async Task<TenantClaimTypeIdentifier> UpdateAsync(TenantClaimTypeUpdateModel model)
        {
            var tenantClaimType = await GetSimpleAsync(model.Identifier);

            if (tenantClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }

            tenantClaimType.Type = model.NewType ?? model.Identifier.Type;
            tenantClaimType.Description = model.Description;

            ctx.TenantClaimTypes.Update(tenantClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(tenantClaimType);

            return await GetIdentifier(tenantClaimType);
        }
        private async Task<TenantClaimTypeIdentifier> GetIdentifier(TenantClaimType entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<TenantClaimType> GetAsync(int id)
        {
            return await ctx.TenantClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.TenantClaims)
                    .ThenInclude(uc => uc.Tenant)
                        .ThenInclude(u => u.Project)
                .Where(uct => uct.Id == id)
                .FirstOrDefaultAsync();
        }

        private async Task<TenantClaimType> GetToDeleteAsync(TenantClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.TenantClaimTypes.Include(ty => ty.TenantClaims).Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteAsync(TenantClaimTypeIdentifier identifier)
        {
            var type = await GetToDeleteAsync(identifier);
            if (type == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND, identifier.ToString());
            }

            if (type.TenantClaims != null)
            {
                ctx.TenantClaims.RemoveRange(type.TenantClaims);
            }

            ctx.TenantClaimTypes.Remove(type);

            await ctx.SaveChangesAsync();

            return 1;
        }
        
        public async Task<List<TenantClaimType>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await ctx.TenantClaimTypes
                .Include(uc => uc.Project)
                .Where(uc => projectIdentifier == null || projectIdentifier.ProjectCode == null || uc.Project.Code == projectIdentifier.ProjectCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task RemoveTenant(TenantIdentifier tenantIdentifier, TenantClaimTypeSetTenantsModel model)
        {
            var TenantClaimType = await GetAsync(model.Identifier);
            if (TenantClaimType == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            var tenant = await _TenantDataProvider.GetSimpleAsync(tenantIdentifier);
            if(tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, tenantIdentifier.ToString());
            }

            var TenantClaims = ctx.TenantClaims.Where(uc => uc.TenantClaimTypeId == TenantClaimType.Id && uc.Tenant.Id == tenant.Id);

            if (TenantClaims != null && TenantClaims.Any())
            {
                ctx.TenantClaims.RemoveRange(TenantClaims);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
