﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class TenantClaimData : ITenantClaimDataProvider
    {
        private ITenantDataProvider _TenantDataProvider;
        private ITenantClaimTypeDataProvider _TenantClaimTypeDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<TenantClaimData> _logger;
        public TenantClaimData(
            ITenantDataProvider tenantDataProvider
            , ITenantClaimTypeDataProvider tenantClaimTypeDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<TenantClaimData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _TenantClaimTypeDataProvider = tenantClaimTypeDataProvider;
            _TenantDataProvider = tenantDataProvider;
        }

        public async Task<TenantClaim> GetAsync(TenantClaimIdentifier identifier)
        {
            Tenant tenant = await _TenantDataProvider.GetSimpleAsync(identifier.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, identifier.GetTenantIdentifier().ToString());
            }

            return await ctx.TenantClaims
                .Include(uc => uc.TenantClaimType)
                    .ThenInclude(tct => tct.Project)
                .Include(uc => uc.Tenant)
                    .ThenInclude(t => t.Project)
                  .Where(uc => uc.TenantId == tenant.Id && uc.TenantClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }
        public async Task<TenantClaim> GetSimpleAsync(TenantClaimIdentifier identifier)
        {
            Tenant tenant = await _TenantDataProvider.GetSimpleAsync(identifier.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.TenantClaims.Where(uc => uc.TenantId == tenant.Id && uc.TenantClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }

        public async Task<TenantClaimIdentifier> CreateAsync(TenantClaimCreateModel model)
        {
            Tenant tenant = await _TenantDataProvider.GetSimpleAsync(model.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.ToString());
            }

            var tenantClaim = await GetSimpleAsync(model.GetTenantClaimIdentifier());

            if (tenantClaim != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.TENANT_CLAIM_EXISTS, model.ToString());
            }

            var tenantClaimType = await _TenantClaimTypeDataProvider.GetSimpleAsync(model.GetTenantClaimTypeIdentifier());
            if (tenantClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            tenantClaim = new TenantClaim();

            tenantClaim.TenantClaimTypeId = tenantClaimType.Id;
            tenantClaim.Value = model.Value;
            tenantClaim.TenantId = tenant.Id;

            await ctx.TenantClaims.AddAsync(tenantClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(tenantClaim);

            return await GetIdentifier(tenantClaim);
        }

        public async Task<List<TenantClaim>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);

            return await ctx.TenantClaims
                .Include(uc => uc.TenantClaimType)
                    .ThenInclude(tct => tct.Project)
                .Include(uc => uc.Tenant)
                    .ThenInclude(t => t.Project)
                .Where(uc => tenant == null || uc.Tenant.Id == tenant.Id)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<TenantClaim>> GetListWithSameTypeAndValueAsync(TenantClaimIdentifier tenantClaimIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantClaimIdentifier.GetTenantIdentifier());

            return await ctx.TenantClaims
                .Include(uc => uc.TenantClaimType)
                    .ThenInclude(tct => tct.Project)
                .Include(uc => uc.Tenant)
                    .ThenInclude(t => t.Project)
                .Where(uc => (tenant == null || uc.Tenant.Id == tenant.Id) && uc.TenantClaimType.Type == tenantClaimIdentifier.Type && uc.Value == tenantClaimIdentifier.Value)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<TenantClaimIdentifier> UpdateAsync(TenantClaimUpdateModel model)
        {
            var tenantClaim = await GetSimpleAsync(model.Identifier);

            if (tenantClaim == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.TENANT_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            
            tenantClaim.Value = model.NewValue;

            ctx.TenantClaims.Update(tenantClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(tenantClaim);

            return await GetIdentifier(tenantClaim);
        }
        private async Task<TenantClaimIdentifier> GetIdentifier(TenantClaim entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<TenantClaim> GetAsync(int id)
        {

            return await ctx.TenantClaims
                .Include(uc => uc.TenantClaimType)
                    .ThenInclude(tct => tct.Project)
                .Include(uc => uc.Tenant)
                    .ThenInclude(t => t.Project)
                  .Where(uc => uc.Id == id).FirstOrDefaultAsync();
        }
        public async Task<int> DeleteAsync(TenantClaimIdentifier identifier)
        {
            var tenantClaim = await GetSimpleAsync(identifier);
            if (tenantClaim == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_CLAIM_NOT_FOUND, identifier.ToString());
            }

            ctx.TenantClaims.Remove(tenantClaim);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
