﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface ITenantClaimDataProvider
    {
        Task<TenantClaim> GetAsync(TenantClaimIdentifier identifier);
        Task<TenantClaim> GetSimpleAsync(TenantClaimIdentifier identifier);
        Task<List<TenantClaim>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<TenantClaim>> GetListWithSameTypeAndValueAsync(TenantClaimIdentifier TenantClaimIdentifier);
        Task<TenantClaimIdentifier> CreateAsync(TenantClaimCreateModel model);
        Task<TenantClaimIdentifier> UpdateAsync(TenantClaimUpdateModel model);
        Task<int> DeleteAsync(TenantClaimIdentifier userIdentifier);
    }
}
