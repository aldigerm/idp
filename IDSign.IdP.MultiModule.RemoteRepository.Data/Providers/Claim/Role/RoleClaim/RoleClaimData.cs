﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class RoleClaimData : IRoleClaimDataProvider
    {
        private IRoleDataProvider _RoleDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private IRoleClaimTypeDataProvider _RoleClaimTypeDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<RoleClaimData> _logger;
        public RoleClaimData(
            IRoleDataProvider roleDataProvider
            , ITenantDataProvider tenantDataProvider
            , IRoleClaimTypeDataProvider roleClaimTypeDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<RoleClaimData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _RoleDataProvider = roleDataProvider;
            _RoleClaimTypeDataProvider = roleClaimTypeDataProvider;
            _TenantDataProvider = tenantDataProvider;
        }

        public async Task<RoleClaim> GetAsync(RoleClaimIdentifier identifier)
        {
            Role role = await _RoleDataProvider.GetSimpleAsync(identifier.GetRoleIdentifier());
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, identifier.GetRoleIdentifier().ToString());
            }

            return await ctx.RoleClaims
                .Include(uc => uc.RoleClaimType)
                .Include(uc => uc.Role)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                  .Where(uc => uc.RoleId == role.Id && uc.RoleClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }
        public async Task<RoleClaim> GetSimpleAsync(RoleClaimIdentifier identifier)
        {
            Role role = await _RoleDataProvider.GetSimpleAsync(identifier.GetRoleIdentifier());
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, identifier.ToString());
            }

            return await ctx.RoleClaims.Where(uc => uc.RoleId == role.Id && uc.RoleClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }

        public async Task<RoleClaimIdentifier> CreateAsync(RoleClaimCreateModel model)
        {
            Role role = await _RoleDataProvider.GetSimpleAsync(model.GetRoleIdentifier());
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, model.ToString());
            }

            var roleClaim = await GetSimpleAsync(model.GetRoleClaimIdentifier());

            if (roleClaim != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_EXISTS, model.ToString());
            }

            var roleClaimType = await _RoleClaimTypeDataProvider.GetSimpleAsync(model.GetRoleClaimTypeIdentifier());
            if (roleClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            roleClaim = new RoleClaim();

            roleClaim.RoleClaimTypeId = roleClaimType.Id;
            roleClaim.Value = model.Value;
            roleClaim.RoleId = role.Id;

            await ctx.RoleClaims.AddAsync(roleClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(roleClaim);

            return await GetIdentifier(roleClaim);
        }

        public async Task<List<RoleClaim>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);

            return await ctx.RoleClaims
                .Include(uc => uc.RoleClaimType)
                    .ThenInclude(t => t.Project)
                .Include(uc => uc.Role)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(uc => tenant == null || uc.Role.Tenant.Id == tenant.Id)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<RoleClaim>> GetListWithSameTypeAndValueAsync(RoleClaimIdentifier roleClaimIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(roleClaimIdentifier.GetTenantIdentifier());

            return await ctx.RoleClaims
                .Include(uc => uc.RoleClaimType)
                     .ThenInclude(t => t.Project)
                .Include(uc => uc.Role)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(uc => (tenant == null || uc.Role.Tenant.Id == tenant.Id) && uc.RoleClaimType.Type == roleClaimIdentifier.Type && uc.Value == roleClaimIdentifier.Value)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<RoleClaimIdentifier> UpdateAsync(RoleClaimUpdateModel model)
        {
            var roleClaim = await GetSimpleAsync(model.Identifier);

            if (roleClaim == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            
            roleClaim.Value = model.NewValue;

            ctx.RoleClaims.Update(roleClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(roleClaim);

            return await GetIdentifier(roleClaim);
        }
        private async Task<RoleClaimIdentifier> GetIdentifier(RoleClaim entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<RoleClaim> GetAsync(int id)
        {

            return await ctx.RoleClaims
                .Include(uc => uc.RoleClaimType)
                .Include(uc => uc.Role)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                  .Where(uc => uc.Id == id).FirstOrDefaultAsync();
        }
        public async Task<int> DeleteAsync(RoleClaimIdentifier identifier)
        {
            var roleClaim = await GetSimpleAsync(identifier);
            if (roleClaim == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_NOT_FOUND, identifier.ToString());
            }

            ctx.RoleClaims.Remove(roleClaim);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
