﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IRoleClaimDataProvider
    {
        Task<RoleClaim> GetAsync(RoleClaimIdentifier identifier);
        Task<RoleClaim> GetSimpleAsync(RoleClaimIdentifier identifier);
        Task<List<RoleClaim>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<RoleClaim>> GetListWithSameTypeAndValueAsync(RoleClaimIdentifier RoleClaimIdentifier);
        Task<RoleClaimIdentifier> CreateAsync(RoleClaimCreateModel model);
        Task<RoleClaimIdentifier> UpdateAsync(RoleClaimUpdateModel model);
        Task<int> DeleteAsync(RoleClaimIdentifier userIdentifier);
    }
}
