﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class RoleClaimTypeData : IRoleClaimTypeDataProvider
    {
        private IProjectDataProvider _ProjectDataProvider;
        private IRoleDataProvider _RoleDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<RoleClaimTypeData> _logger;
        public RoleClaimTypeData(
            IProjectDataProvider projectDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<RoleClaimTypeData> logger
            , IRoleDataProvider roleDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _ProjectDataProvider = projectDataProvider;
            _RoleDataProvider = roleDataProvider;
        }

        public async Task<RoleClaimType> GetSimpleAsync(RoleClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.RoleClaimTypes.Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<RoleClaimType> GetAsync(RoleClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.RoleClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.RoleClaims)
                    .ThenInclude(uc => uc.Role)
                        .ThenInclude(u => u.Tenant)
                            .ThenInclude(u => u.Project)
                .Where(uct => uct.ProjectId == project.Id && uct.Type == identifier.Type)
                .FirstOrDefaultAsync();
        }

        public async Task<RoleClaimType> CreateAsync(RoleClaimTypeCreateModel model)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(model.GetClaimTypeIdentifier().GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, model.ToString());
            }

            var roleClaimType = await GetSimpleAsync(model.GetClaimTypeIdentifier());

            if (roleClaimType != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.ROLE_CLAIM_TYPE_EXISTS, model.ToString());
            }

            roleClaimType = new RoleClaimType();

            roleClaimType.Type = model.Type;
            roleClaimType.ProjectId = project.Id;
            roleClaimType.Description = model.Description;

            await ctx.RoleClaimTypes.AddAsync(roleClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(roleClaimType);

            return roleClaimType;
        }

        public async Task<RoleClaimTypeIdentifier> UpdateAsync(RoleClaimTypeUpdateModel model)
        {
            var roleClaimType = await GetSimpleAsync(model.Identifier);

            if (roleClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }

            roleClaimType.Type = model.NewType ?? model.Identifier.Type;
            roleClaimType.Description = model.Description;

            ctx.RoleClaimTypes.Update(roleClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(roleClaimType);

            return await GetIdentifier(roleClaimType);
        }
        private async Task<RoleClaimTypeIdentifier> GetIdentifier(RoleClaimType entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<RoleClaimType> GetAsync(int id)
        {
            return await ctx.RoleClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.RoleClaims)
                    .ThenInclude(uc => uc.Role)
                        .ThenInclude(u => u.Tenant)
                            .ThenInclude(u => u.Project)
                .Where(uct => uct.Id == id)
                .FirstOrDefaultAsync();
        }

        private async Task<RoleClaimType> GetToDeleteAsync(RoleClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.RoleClaimTypes.Include(ty => ty.RoleClaims).Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteAsync(RoleClaimTypeIdentifier identifier)
        {
            var type = await GetToDeleteAsync(identifier);
            if (type == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND, identifier.ToString());
            }

            if(type.RoleClaims != null)
            {
                ctx.RoleClaims.RemoveRange(type.RoleClaims);
            }

            ctx.RoleClaimTypes.Remove(type);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<List<RoleClaimType>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await ctx.RoleClaimTypes
                .Include(uc => uc.Project)
                .Where(uc => projectIdentifier == null || projectIdentifier.ProjectCode == null || uc.Project.Code == projectIdentifier.ProjectCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task RemoveRole(RoleIdentifier roleIdentifier, RoleClaimTypeSetRolesModel model)
        {
            var RoleClaimType = await GetAsync(model.Identifier);
            if (RoleClaimType == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            var role = await _RoleDataProvider.GetSimpleAsync(roleIdentifier);
            if(role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }

            var RoleClaims = ctx.RoleClaims.Where(uc => uc.RoleClaimTypeId == RoleClaimType.Id && uc.Role.Id == role.Id);

            if (RoleClaims != null && RoleClaims.Any())
            {
                ctx.RoleClaims.RemoveRange(RoleClaims);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
