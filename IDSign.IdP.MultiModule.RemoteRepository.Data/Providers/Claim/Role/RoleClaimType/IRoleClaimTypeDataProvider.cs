﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IRoleClaimTypeDataProvider
    {
        Task<RoleClaimType> GetAsync(RoleClaimTypeIdentifier identifier);
        Task<RoleClaimType> GetSimpleAsync(RoleClaimTypeIdentifier identifier);
        Task<RoleClaimType> CreateAsync(RoleClaimTypeCreateModel model);
        Task<int> DeleteAsync(RoleClaimTypeIdentifier userIdentifier);
        Task<RoleClaimTypeIdentifier> UpdateAsync(RoleClaimTypeUpdateModel model);
        Task<List<RoleClaimType>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task RemoveRole(RoleIdentifier role, RoleClaimTypeSetRolesModel model);
    }
}
