﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserGroupClaimTypeDataProvider
    {
        Task<UserGroupClaimType> GetAsync(UserGroupClaimTypeIdentifier identifier);
        Task<UserGroupClaimType> GetSimpleAsync(UserGroupClaimTypeIdentifier identifier);
        Task<UserGroupClaimType> CreateAsync(UserGroupClaimTypeCreateModel model);
        Task<int> DeleteAsync(UserGroupClaimTypeIdentifier userGroupIdentifier);
        Task<UserGroupClaimTypeIdentifier> UpdateAsync(UserGroupClaimTypeUpdateModel model);
        Task<List<UserGroupClaimType>> GetListAsync(ProjectIdentifier userGroupIdentifier);
        Task RemoveUserGroup(UserGroupIdentifier userGroup, UserGroupClaimTypeSetUserGroupsModel model);
    }
}
