﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserGroupClaimTypeData : IUserGroupClaimTypeDataProvider
    {
        private IProjectDataProvider _ProjectDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserGroupClaimTypeData> _logger;
        public UserGroupClaimTypeData(
            IProjectDataProvider projectDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<UserGroupClaimTypeData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _ProjectDataProvider = projectDataProvider;
        }

        public async Task<UserGroupClaimType> GetSimpleAsync(UserGroupClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserGroupClaimTypes.Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<UserGroupClaimType> GetAsync(UserGroupClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserGroupClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.UserGroupClaims)
                    .ThenInclude(uc => uc.UserGroup)
                        .ThenInclude(u => u.Tenant)
                            .ThenInclude(u => u.Project)
                .Where(uct => uct.ProjectId == project.Id && uct.Type == identifier.Type)
                .FirstOrDefaultAsync();
        }

        public async Task<UserGroupClaimType> CreateAsync(UserGroupClaimTypeCreateModel model)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(model.GetClaimTypeIdentifier().GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, model.ToString());
            }

            var userClaimType = await GetSimpleAsync(model.GetClaimTypeIdentifier());

            if (userClaimType != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_EXISTS, model.ToString());
            }

            userClaimType = new UserGroupClaimType();

            userClaimType.Type = model.Type;
            userClaimType.ProjectId = project.Id;
            userClaimType.Description = model.Description;

            await ctx.UserGroupClaimTypes.AddAsync(userClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaimType);

            return userClaimType;
        }

        public async Task<UserGroupClaimTypeIdentifier> UpdateAsync(UserGroupClaimTypeUpdateModel model)
        {
            var userGroupClaimType = await GetSimpleAsync(model.Identifier);

            if (userGroupClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }

            userGroupClaimType.Type = model.NewType ?? model.Identifier.Type;
            userGroupClaimType.Description = model.Description;

            ctx.UserGroupClaimTypes.Update(userGroupClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userGroupClaimType);

            return await GetIdentifier(userGroupClaimType);
        }
        private async Task<UserGroupClaimTypeIdentifier> GetIdentifier(UserGroupClaimType entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<UserGroupClaimType> GetAsync(int id)
        {
            return await ctx.UserGroupClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.UserGroupClaims)
                    .ThenInclude(uc => uc.UserGroup)
                        .ThenInclude(u => u.Tenant)
                            .ThenInclude(u => u.Project)
                .Where(uct => uct.Id == id)
                .FirstOrDefaultAsync();
        }
        public async Task<int> DeleteAsync(UserGroupClaimTypeIdentifier identifier)
        {
            var user = await GetSimpleAsync(identifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, identifier.ToString());
            }

            ctx.UserGroupClaimTypes.Remove(user);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<List<UserGroupClaimType>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await ctx.UserGroupClaimTypes
                .Include(uc => uc.Project)
                .Where(uc => projectIdentifier == null || projectIdentifier.ProjectCode == null || uc.Project.Code == projectIdentifier.ProjectCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task RemoveUserGroup(UserGroupIdentifier userGroupIdentifier, UserGroupClaimTypeSetUserGroupsModel model)
        {
            var userClaimType = await GetAsync(model.Identifier);
            if (userClaimType == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            var user = await _ProjectDataProvider.GetSimpleAsync(model.Identifier.GetProjectIdentifier());
            if(user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userGroupIdentifier.ToString());
            }

            var userClaims = ctx.UserGroupClaims.Where(uc => uc.UserGroupClaimTypeId == userClaimType.Id && uc.UserGroup.Id == user.Id);

            if (userClaims != null && userClaims.Any())
            {
                ctx.UserGroupClaims.RemoveRange(userClaims);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
