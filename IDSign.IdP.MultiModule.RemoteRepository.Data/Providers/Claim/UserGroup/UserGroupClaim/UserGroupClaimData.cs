﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserGroupClaimData : IUserGroupClaimDataProvider
    {
        private IUserGroupDataProvider _UserGroupDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private IUserGroupClaimTypeDataProvider _UserGroupClaimTypeDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserGroupClaimData> _logger;
        public UserGroupClaimData(
            IUserGroupDataProvider userDataProvider
            , ITenantDataProvider tenantDataProvider
            , IUserGroupClaimTypeDataProvider userClaimTypeDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<UserGroupClaimData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserGroupDataProvider = userDataProvider;
            _UserGroupClaimTypeDataProvider = userClaimTypeDataProvider;
            _TenantDataProvider = tenantDataProvider;
        }

        public async Task<UserGroupClaim> GetAsync(UserGroupClaimIdentifier identifier)
        {
            UserGroup user = await _UserGroupDataProvider.GetSimpleAsync(identifier.GetUserGroupIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserGroupClaims
                .Include(uc => uc.UserGroupClaimType)
                .Include(uc => uc.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                  .Where(uc => uc.UserGroupId == user.Id && uc.UserGroupClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }
        public async Task<UserGroupClaim> GetSimpleAsync(UserGroupClaimIdentifier identifier)
        {
            UserGroup user = await _UserGroupDataProvider.GetSimpleAsync(identifier.GetUserGroupIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, identifier.GetUserGroupIdentifier().ToString());
            }

            return await ctx.UserGroupClaims.Where(uc => uc.UserGroupId == user.Id && uc.UserGroupClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }

        public async Task<UserGroupClaimIdentifier> CreateAsync(UserGroupClaimCreateModel model)
        {
            UserGroup user = await _UserGroupDataProvider.GetSimpleAsync(model.GetUserGroupIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, model.ToString());
            }

            var userClaim = await GetSimpleAsync(model.GetUserGroupClaimIdentifier());

            if (userClaim != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_EXISTS, model.ToString());
            }

            var userClaimType = await _UserGroupClaimTypeDataProvider.GetSimpleAsync(model.GetUserGroupClaimTypeIdentifier());
            if (userClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            userClaim = new UserGroupClaim();

            userClaim.UserGroupClaimTypeId = userClaimType.Id;
            userClaim.Value = model.Value;
            userClaim.UserGroupId = user.Id;

            await ctx.UserGroupClaims.AddAsync(userClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaim);

            return await GetIdentifier(userClaim);
        }

        public async Task<List<UserGroupClaim>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);

            return await ctx.UserGroupClaims
                .Include(uc => uc.UserGroupClaimType)
                        .ThenInclude(t => t.Project)
                .Include(uc => uc.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(uc => tenant == null || uc.UserGroup.Tenant.Id == tenant.Id)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<UserGroupClaim>> GetListWithSameTypeAndValueAsync(UserGroupClaimIdentifier userClaimIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(userClaimIdentifier.GetTenantIdentifier());

            return await ctx.UserGroupClaims
                .Include(uc => uc.UserGroupClaimType)
                .Include(uc => uc.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(uc => (tenant == null || uc.UserGroup.Tenant.Id == tenant.Id) 
                    && uc.UserGroupClaimType.Type == userClaimIdentifier.Type 
                    && uc.Value == userClaimIdentifier.Value)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<UserGroupClaimIdentifier> UpdateAsync(UserGroupClaimUpdateModel model)
        {
            var userGroupClaim = await GetSimpleAsync(model.Identifier);

            if (userGroupClaim == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            
            userGroupClaim.Value = model.NewValue;

            ctx.UserGroupClaims.Update(userGroupClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userGroupClaim);

            return await GetIdentifier(userGroupClaim);
        }
        private async Task<UserGroupClaimIdentifier> GetIdentifier(UserGroupClaim entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<UserGroupClaim> GetAsync(int id)
        {

            return await ctx.UserGroupClaims
                .Include(uc => uc.UserGroupClaimType)
                .Include(uc => uc.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                  .Where(uc => uc.Id == id).FirstOrDefaultAsync();
        }
        public async Task<int> DeleteAsync(UserGroupClaimIdentifier identifier)
        {
            var userClaim = await GetSimpleAsync(identifier);
            if (userClaim == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_NOT_FOUND, identifier.ToString());
            }

            ctx.UserGroupClaims.Remove(userClaim);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
