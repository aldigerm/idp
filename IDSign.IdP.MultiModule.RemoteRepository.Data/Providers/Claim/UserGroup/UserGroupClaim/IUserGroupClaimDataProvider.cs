﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserGroupClaimDataProvider
    {
        Task<UserGroupClaim> GetAsync(UserGroupClaimIdentifier identifier);
        Task<UserGroupClaim> GetSimpleAsync(UserGroupClaimIdentifier identifier);
        Task<List<UserGroupClaim>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<UserGroupClaim>> GetListWithSameTypeAndValueAsync(UserGroupClaimIdentifier userClaimIdentifier);
        Task<UserGroupClaimIdentifier> CreateAsync(UserGroupClaimCreateModel model);
        Task<UserGroupClaimIdentifier> UpdateAsync(UserGroupClaimUpdateModel model);
        Task<int> DeleteAsync(UserGroupClaimIdentifier userIdentifier);
    }
}
