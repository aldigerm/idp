﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserClaimDataProvider
    {
        Task<UserClaim> GetAsync(UserClaimIdentifier identifier);
        Task<UserClaim> GetSimpleAsync(UserClaimIdentifier identifier);
        Task<List<UserClaim>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<UserClaim>> GetListWithSameTypeAndValueAsync(UserClaimIdentifier userClaimIdentifier);
        Task<UserClaimIdentifier> CreateAsync(UserClaimCreateModel model);
        Task<UserClaimIdentifier> UpdateAsync(UserClaimUpdateModel model);
        Task<int> DeleteAsync(UserClaimIdentifier userIdentifier);
    }
}
