﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserClaimData : IUserClaimDataProvider
    {
        private IUserDataProvider _UserDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private IUserTenantDataProvider _UserTenantDataProvider;
        private IUserClaimTypeDataProvider _UserClaimTypeDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserClaimData> _logger;
        public UserClaimData(
            IUserDataProvider userDataProvider
            , ITenantDataProvider tenantDataProvider
            , IUserClaimTypeDataProvider userClaimTypeDataProvider
            , IUserTenantDataProvider userTenantDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<UserClaimData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserDataProvider = userDataProvider;
            _UserClaimTypeDataProvider = userClaimTypeDataProvider;
            _TenantDataProvider = tenantDataProvider;
            _UserTenantDataProvider = userTenantDataProvider;
        }

        public async Task<UserClaim> GetAsync(UserClaimIdentifier identifier)
        {
            AppUser user = await _UserDataProvider.GetSimpleAsync(identifier.GetUserIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserClaims
                .Include(uc => uc.UserClaimType)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(u => u.User)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(p => p.Project)
                  .Where(uc => uc.UserTenant.UserId == user.Id && uc.UserClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }
        public async Task<UserClaim> GetSimpleAsync(UserClaimIdentifier identifier)
        {
            AppUser user = await _UserDataProvider.GetSimpleAsync(identifier.GetUserIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserClaims.Where(uc => uc.UserTenant.UserId == user.Id && uc.UserClaimType.Type == identifier.Type && uc.Value == identifier.Value).FirstOrDefaultAsync();
        }

        public async Task<UserClaimIdentifier> CreateAsync(UserClaimCreateModel model)
        {
            UserTenant userTenant = await _UserTenantDataProvider.GetSimpleAsync(model.GetUserTenantIdentifier());
            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND, model.GetUserTenantIdentifier().ToString());
            }
            
            var userClaim = await GetSimpleAsync(model.GetUserClaimIdentifier());

            if (userClaim != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_EXISTS, model.ToString());
            }

            var userClaimType = await _UserClaimTypeDataProvider.GetSimpleAsync(model.GetUserClaimTypeIdentifier());
            if (userClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            userClaim = new UserClaim();

            userClaim.UserClaimTypeId = userClaimType.Id;
            userClaim.Value = model.Value;
            userClaim.UserTenantId = userTenant.Id;

            await ctx.UserClaims.AddAsync(userClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaim);

            return await GetIdentifier(userClaim);
        }

        public async Task<List<UserClaim>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);

            return await ctx.UserClaims
                .Include(uc => uc.UserClaimType)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.User)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(u => u.Project)
                .Where(uc => tenant == null || uc.UserTenant.TenantId == tenant.Id)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<UserClaim>> GetListWithSameTypeAndValueAsync(UserClaimIdentifier userClaimIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(userClaimIdentifier.GetTenantIdentifier());

            return await ctx.UserClaims
                .Include(uc => uc.UserClaimType)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.User)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(u => u.Project)
                .Where(uc => (tenant == null || uc.UserTenant.TenantId == tenant.Id) && uc.UserClaimType.Type == userClaimIdentifier.GetUserClaimTypeIdentifier().Type && uc.Value == userClaimIdentifier.Value)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<UserClaimIdentifier> UpdateAsync(UserClaimUpdateModel model)
        {
            var userClaim = await GetSimpleAsync(model.Identifier);

            if (userClaim == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            
            userClaim.Value = model.NewValue;

            ctx.UserClaims.Update(userClaim);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaim);

            return await GetIdentifier(userClaim);
        }
        private async Task<UserClaimIdentifier> GetIdentifier(UserClaim entity)
        {
            var data = await GetAsync(entity.Id);
            return data.GetIdentifier();
        }

        private async Task<UserClaim> GetAsync(int id)
        {

            return await ctx.UserClaims
                .Include(uc => uc.UserClaimType)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.User)
                .Include(uc => uc.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(u => u.Project)
                .Where(uc => uc.Id == id).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteAsync(UserClaimIdentifier identifier)
        {
            var userClaim = await GetSimpleAsync(identifier);
            if (userClaim == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_NOT_FOUND, identifier.ToString());
            }

            ctx.UserClaims.Remove(userClaim);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
