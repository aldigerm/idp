﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserClaimTypeDataProvider
    {
        Task<UserClaimType> GetAsync(UserClaimTypeIdentifier identifier);
        Task<UserClaimType> GetSimpleAsync(UserClaimTypeIdentifier identifier);
        Task<UserClaimType> CreateAsync(UserClaimTypeCreateModel model);
        Task<int> DeleteAsync(UserClaimTypeIdentifier userIdentifier);
        Task<UserClaimTypeIdentifier> UpdateAsync(UserClaimTypeUpdateModel model);
        Task<List<UserClaimType>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task RemoveUser(UserIdentifier user, UserClaimTypeSetUsersModel model);
    }
}
