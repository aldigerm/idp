﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserClaimTypeData : IUserClaimTypeDataProvider
    {
        private IProjectDataProvider _ProjectDataProvider;
        private IUserDataProvider _UserDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserClaimTypeData> _logger;
        public UserClaimTypeData(
            IProjectDataProvider projectDataProvider
            , IUserDataProvider userDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<UserClaimTypeData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _ProjectDataProvider = projectDataProvider;
            _UserDataProvider = userDataProvider;
        }

        public async Task<UserClaimType> GetSimpleAsync(UserClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserClaimTypes.Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<UserClaimType> GetAsync(UserClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.UserClaims)
                    .ThenInclude(uc => uc.UserTenant)
                        .ThenInclude(u => u.User)
                .Include(uct => uct.UserClaims)
                    .ThenInclude(uc => uc.UserTenant)
                        .ThenInclude(ut => ut.Tenant)
                            .ThenInclude(p => p.Project)
                .Where(uct => uct.ProjectId == project.Id && uct.Type == identifier.Type)
                .FirstOrDefaultAsync();
        }

        public async Task<UserClaimType> CreateAsync(UserClaimTypeCreateModel model)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(model.ProjectIdentifier);
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, model.ToString());
            }

            var userClaimType = await GetSimpleAsync(model.GetClaimTypeIdentifier());

            if (userClaimType != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_EXISTS, model.ToString());
            }

            userClaimType = new UserClaimType();

            userClaimType.Type = model.Type;
            userClaimType.ProjectId = project.Id;
            userClaimType.Description = model.Description;

            await ctx.UserClaimTypes.AddAsync(userClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaimType);

            return userClaimType;
        }

        public async Task<UserClaimTypeIdentifier> UpdateAsync(UserClaimTypeUpdateModel model)
        {
            var userClaimType = await GetSimpleAsync(model.Identifier);

            if (userClaimType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }

            userClaimType.Type = model.NewType ?? model.Identifier.Type;
            userClaimType.Description = model.Description;

            ctx.UserClaimTypes.Update(userClaimType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userClaimType);

            return await GetIdentifier(userClaimType);
        }
        private async Task<UserClaimTypeIdentifier> GetIdentifier(UserClaimType entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<UserClaimType> GetAsync(int id)
        {
            return await ctx.UserClaimTypes
                .Include(uct => uct.Project)
                .Include(uct => uct.UserClaims)
                    .ThenInclude(uc => uc.UserTenant)
                        .ThenInclude(u => u.User)
                .Include(uct => uct.UserClaims)
                    .ThenInclude(uc => uc.UserTenant)
                        .ThenInclude(ut => ut.Tenant)
                            .ThenInclude(p => p.Project)
                .Where(uct => uct.Id == id)
                .FirstOrDefaultAsync();
        }

        private async Task<UserClaimType> GetToDeleteAsync(UserClaimTypeIdentifier identifier)
        {
            Project project = await _ProjectDataProvider.GetSimpleAsync(identifier.GetProjectIdentifier());
            if (project == null)
            {
                throw new IdPNotFoundException(ErrorCode.PROJECT_NOT_FOUND, identifier.ToString());
            }

            return await ctx.UserClaimTypes.Include(ty => ty.UserClaims).Where(uc => uc.ProjectId == project.Id && uc.Type == identifier.Type).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteAsync(UserClaimTypeIdentifier identifier)
        {
            var type = await GetToDeleteAsync(identifier);
            if (type == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, identifier.ToString());
            }

            if (type.UserClaims != null)
            {
                ctx.UserClaims.RemoveRange(type.UserClaims);
            }

            ctx.UserClaimTypes.Remove(type);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<List<UserClaimType>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await ctx.UserClaimTypes
                .Include(uc => uc.Project)
                .Where(uc => projectIdentifier == null || projectIdentifier.ProjectCode == null || uc.Project.Code == projectIdentifier.ProjectCode)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task RemoveUser(UserIdentifier userIdentifier, UserClaimTypeSetUsersModel model)
        {
            var userClaimType = await GetAsync(model.Identifier);
            if (userClaimType == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_CLAIM_TYPE_NOT_FOUND, model.ToString());
            }
            var user = await _UserDataProvider.GetSimpleAsync(userIdentifier);
            if(user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userIdentifier.ToString());
            }

            var userClaims = ctx.UserClaims.Where(uc => uc.UserClaimTypeId == userClaimType.Id && uc.UserTenant.User.Id == user.Id);

            if (userClaims != null && userClaims.Any())
            {
                ctx.UserClaims.RemoveRange(userClaims);

                await ctx.SaveChangesAsync();
            }
        }
    }
}
