﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserPreviousPasswordData : IUserPreviousPasswordDataProvider
    {
        private IUserDataProvider _UserDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserPreviousPasswordData> _logger;
        public UserPreviousPasswordData(
             RemoteRepositoryDbContext ctx
            , ILogger<UserPreviousPasswordData> logger
            , IUserDataProvider userDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserDataProvider = userDataProvider;
        }

        public async Task<UserPreviousPassword> CreateAsync(UserIdentifier userIdentifier, string hash)
        {
            var user = await _UserDataProvider.GetSimpleAsync(userIdentifier);

            if (user == null)
                return null;

            var previousPassword = new UserPreviousPassword();

            previousPassword.UserId = user.Id;
            previousPassword.PasswordHash = hash;
            previousPassword.CreateDate = DateTimeOffset.Now;

            await ctx.UserPreviousPasswords.AddAsync(previousPassword);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(previousPassword);

            return previousPassword;
        }


        public async Task<IList<UserPreviousPassword>> GetPreviousPasswords(UserIdentifier userIdentifier, int limit)
        {
            var user = await _UserDataProvider.GetSimpleAsync(userIdentifier);

            if (user == null)
                return null;

            return await ctx.UserPreviousPasswords
                .Where(upp => upp.UserId == user.Id)
                .OrderByDescending(x => x.CreateDate)
                .Take(limit)
                .ToListAsync();
        }

    }
}
