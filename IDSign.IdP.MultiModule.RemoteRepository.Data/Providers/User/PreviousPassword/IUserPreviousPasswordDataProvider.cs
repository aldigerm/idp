﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserPreviousPasswordDataProvider
    {
        Task<IList<UserPreviousPassword>> GetPreviousPasswords(UserIdentifier userIdentifier, int limit);
        Task<UserPreviousPassword> CreateAsync(UserIdentifier userIdentifier, string hash);
    }
}
