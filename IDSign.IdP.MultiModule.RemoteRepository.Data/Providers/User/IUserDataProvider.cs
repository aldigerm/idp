﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserDataProvider
    {
        Task<AppUser> GetAsync(UserIdentifier userIdentifier);
        Task<byte[]> GetProfileImageAsync(UserIdentifier userIdentifier);
        Task<AppUser> GetSimpleAsync(UserIdentifier userIdentifier);
        Task<UserDetails> GetUserDetailsAsync(UserIdentifier userIdentifier);
        Task<AppUser> CreateAsync(UserRegisterViewModel model);
        Task<UserIdentifier> UpdateAsync(UserUpdateViewModel model);
        Task<AppUser> GetByEmailAsync(string emailAddress);
        Task<AppUser> GetAsync(int id);
        Task<AppUser> SetPasswordHashAsync(UserIdentifier userIdentifier, string passwordHash);
        Task<IList<AppUser>> GetListAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true);
        Task<int> DeleteAsync(UserIdentifier userIdentifier);
        Task<bool> ExistsAsync(UserIdentifier userIdentifier);
        Task<IList<AppUser>> GetListAsync(bool includeTemporaryUsers = true);
        Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, byte[] data);
        Task<AppUser> UpdatePasswordAsync(UpdatePasswordViewModel request);
        Task<AppUser> UpdateSecuritySettingsAsync(UpdateSecuritySettingsViewModel request);
    }
}
