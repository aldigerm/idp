﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserTenantSessionData : IUserTenantSessionDataProvider
    {
        private IUserTenantDataProvider _UserTenantDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserTenantSessionData> _logger;
        public UserTenantSessionData(
             RemoteRepositoryDbContext ctx
            , ILogger<UserTenantSessionData> logger
            , IUserTenantDataProvider userTenantDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserTenantDataProvider = userTenantDataProvider;
        }

        public async Task<UserTenantSession> CreateAsync(UserTenantSessionModel model)
        {
            var userTenant = await _UserTenantDataProvider.GetSimpleAsync(model.UserTenantIdentifier);

            if (userTenant == null)
                return null;

            var userTenantSessionType = await ctx.UserTenantSessionTypes.FirstOrDefaultAsync(ust => ust.Code == model.UserTenantSessionTypeCode);

            var entity = new UserTenantSession();

            entity.UserTenantId = userTenant.Id;
            entity.UserTenantSessionTypeId = userTenantSessionType.Id;
            entity.ExpiryDate = model.ExpiryDate;
            entity.Identifier = model.UserTenantSessionIdentifier;
            entity.CreationDate = DateTimeOffset.Now;
            entity.Data = model.Data;

            await ctx.UserTenantSessions.AddAsync(entity);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(entity);

            return entity;
        }


        public async Task<IList<UserTenantSession>> GetListAsync(UserTenantIdentifier userTenantIdentifier)
        {
            var userTenant = await _UserTenantDataProvider.GetSimpleAsync(userTenantIdentifier);

            if (userTenant == null)
                return null;

            return await ctx.UserTenantSessions
                .Include(us => us.UserTenantSessionType)
                .Where(us => us.UserTenantId == userTenant.Id)
                .OrderByDescending(x => x.ExpiryDate)
                .ToListAsync();
        }

        public async Task<UserTenantSession> GetAsync(string userTenantSessionIdentifier)
        {
            var entity = await ctx.UserTenantSessions
                .Include(us => us.UserTenantSessionType)
                .Include(us => us.UserTenant)
                    .ThenInclude(ut => ut.User)
                .Include(us => us.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .SingleOrDefaultAsync(us => us.Identifier == userTenantSessionIdentifier);


            ctx.DetachEntity(entity);

            return entity;
        }

        public async Task<UserTenantSession> GetSimpleAsync(string userTenantSessionIdentifier)
        {
            return await ctx.UserTenantSessions
                .SingleOrDefaultAsync(us => us.Identifier == userTenantSessionIdentifier);
        }

        public async Task<string> UpdateAsync(UserTenantSessionModel model)
        {
            var session = await GetSimpleAsync(model.UserTenantSessionIdentifier);

            if (session == null)
                throw new IdPNotFoundException(ErrorCode.USER_SESSION_NOT_FOUND, model.UserTenantSessionIdentifier);

            session.ExpiryDate = model.ExpiryDate;

            ctx.UserTenantSessions.Update(session);

            await ctx.SaveChangesAsync();

            return session.Identifier;
        }

        public async Task<int> DeleteAsync(string identifier)
        {
            var session = await GetSimpleAsync(identifier);
            if (session == null)
                throw new IdPNotFoundException(ErrorCode.USER_SESSION_NOT_FOUND, identifier);

            session = await ctx.UserTenantSessions.FirstAsync(us => us.Id == session.Id);

            ctx.UserTenantSessions.Remove(session);

            await ctx.SaveChangesAsync();

            return 1;
        }
    }
}
