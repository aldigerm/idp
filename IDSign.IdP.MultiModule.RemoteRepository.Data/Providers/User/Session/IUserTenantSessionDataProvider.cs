﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserTenantSessionDataProvider
    {
        Task<IList<UserTenantSession>> GetListAsync(UserTenantIdentifier userIdentifier);
        Task<UserTenantSession> CreateAsync(UserTenantSessionModel model);
        Task<UserTenantSession> GetSimpleAsync(string userTenantSessionIdentifier);
        Task<UserTenantSession> GetAsync(string userTenantSessionIdentifier);
        Task<string> UpdateAsync(UserTenantSessionModel model);
        Task<int> DeleteAsync(string identifier);
    }
}
