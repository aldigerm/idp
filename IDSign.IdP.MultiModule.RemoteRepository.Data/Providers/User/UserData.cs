﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserData : IUserDataProvider
    {
        private ITenantDataProvider _TenantDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserData> _logger;
        public UserData(
            ITenantDataProvider companyDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<UserData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _TenantDataProvider = companyDataProvider;
        }
        public async Task<AppUser> CreateAsync(UserRegisterViewModel model)
        {
            AppUser user = new AppUser()
            {
                Email = model.EmailAddress,
                PasswordHash = "",
                NewPasswordRequired = model.SetPasswordOnFirstLogin,
                TwoFactorEnabled = model.UseMobileOTP,
                IsTemporaryUser = model.IsTemporaryUser
            };


            #region Building UserDetails object
            UserDetails userDetails = new UserDetails()
            {
                Title = model.Title,
                FirstName = model.FirstName,
                LastName = model.LastName,
                EmailAddress = model.EmailAddress,
                MobileNumber = model.MobileNumber,
                ApplicationUser = user
            };
            #endregion

            user.UserDetails = userDetails;

            user.TenantUsername = model.Username;

            await ctx.Users.AddAsync(user);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(user);
            ctx.DetachEntity(user.UserDetails);

            return user;
        }

        public async Task<AppUser> GetSimpleAsync(UserIdentifier userIdentifier)
        {
            AppUser user = await ctx.Users
                .AsNoTracking()
                .SingleOrDefaultAsync(au => au.TenantUsername == userIdentifier.Username
                && au.UserTenants.Any(ut => ut.Tenant.Code == userIdentifier.TenantCode
                && ut.Tenant.Project.Code == userIdentifier.ProjectCode));

            if (user == null)
            {
                _logger.LogWarning("User not found {0}", userIdentifier);
            }

            ctx.DetachEntity(user);

            return user;
        }

        public async Task<UserDetails> GetUserDetailsAsync(UserIdentifier userIdentifier)
        {
            return await ctx.UserDetails
                .AsNoTracking()
                .SingleOrDefaultAsync(ud => ud.ApplicationUser.ObjectIdentifier == userIdentifier.UserGUID
                || (userIdentifier.UserGUID == null && ud.ApplicationUser.TenantUsername == userIdentifier.Username
                 && ud.ApplicationUser.UserTenants.Any(ut => ut.Tenant.Code == userIdentifier.TenantCode
                    && ut.Tenant.Project.Code == userIdentifier.ProjectCode)));
        }

        public async Task<AppUser> GetAsync(UserIdentifier userIdentifier)
        {
            if (userIdentifier == null)
            {
                _logger.LogWarning("userIdentifier is null. User returned will be null");
                return null;
            }

            AppUser user = await ctx.Users
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.TenantClaims)
                            .ThenInclude(tc => tc.TenantClaimType)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserClaims)
                        .ThenInclude(uc => uc.UserClaimType)
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantUserGroups)
                        .ThenInclude(ug => ug.UserGroup)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserGroupManagement)
                        .ThenInclude(ugm => ugm.UserGroup)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantSessions)
                        .ThenInclude(uts => uts.UserTenantSessionType)
                .AsNoTracking()
                .SingleOrDefaultAsync(au => au.ObjectIdentifier == userIdentifier.UserGUID || (userIdentifier.UserGUID == null && au.TenantUsername == userIdentifier.Username
                && au.UserTenants != null
                && au.UserTenants.Any(ut =>
                            ut != null && ut.Tenant != null && ut.Tenant.Project != null
                            && ut.Tenant.Code == userIdentifier.TenantCode
                            && ut.Tenant.Project.Code == userIdentifier.ProjectCode)));

            if (user == null)
            {
                _logger.LogWarning("User not found {0}", userIdentifier);
            }

            ctx.DetachEntity(user);

            return user;
        }

        public async Task<byte[]> GetProfileImageAsync(UserIdentifier userIdentifier)
        {
            byte[] userImage = await ctx.Users
                .Where(au => au.TenantUsername == userIdentifier.Username
                && au.UserTenants.Any(ut => ut.Tenant.Code == userIdentifier.TenantCode
                && ut.Tenant.Project.Code == userIdentifier.ProjectCode))
                .Select(u => u.UserDetails.ProfileImage.Data)
                .AsNoTracking()
                .SingleOrDefaultAsync();

            if (userImage == null)
            {
                _logger.LogWarning("User image not found {0}", userIdentifier);
            }
            return userImage;
        }

        public async Task<AppUser> GetToDeleteAsync(UserIdentifier userIdentifier)
        {
            AppUser user = await ctx.Users
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantUserGroups)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserGroupManagement)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserClaims)
                .Include(u => u.UserTenants)
                .AsNoTracking()
                .SingleOrDefaultAsync(au => au.ObjectIdentifier == userIdentifier.UserGUID
                || (userIdentifier.UserGUID == null && au.TenantUsername == userIdentifier.Username
                && au.UserTenants.Any(ut => ut.Tenant.Code == userIdentifier.TenantCode
                && ut.Tenant.Project.Code == userIdentifier.ProjectCode)));

            if (user == null)
            {
                _logger.LogWarning("User not found {0}", userIdentifier);
            }

            ctx.DetachEntity(user);

            return user;
        }
        public async Task<AppUser> GetByEmailAsync(string emailAddress)
        {
            AppUser user;

            user = await ctx.Users
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantUserGroups)
            .AsNoTracking()
            .SingleOrDefaultAsync(au => au.Email == emailAddress);

            return user;
        }

        private async Task<UserIdentifier> GetIdentifier(AppUser user, TenantIdentifier tenantIdentifier)
        {
            return (await GetAsync(user.Id)).GetIdentifier(tenantIdentifier);
        }

        public async Task<UserIdentifier> UpdateAsync(UserUpdateViewModel model)
        {
            AppUser user = await GetSimpleAsync(model.Identifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

            #region User Details

            UserDetails userDetails = await GetUserDetailsAsync(model.Identifier);

            if (userDetails == null)
            {
                // wasn't found so we create
                userDetails = new UserDetails();
                await ctx.UserDetails.AddAsync(userDetails);
                await ctx.SaveChangesAsync();
            }

            userDetails.FirstName = model.FirstName;
            userDetails.LastName = model.LastName;
            userDetails.EmailAddress = model.EmailAddress;

            ctx.UserDetails.Update(userDetails);

            #endregion

            #region User 

            user.TenantUsername = model.Identifier.Username;
            user.Email = model.EmailAddress;

            // we only update this if a value is given
            if (model.Enabled.HasValue)
            {
                user.LockoutEnabled = !model.Enabled.Value;
            }
            user.UserDetailsId = userDetails.Id;

            #endregion

            ctx.Users.Update(user);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(user);

            return await GetIdentifier(user, model.Identifier.GetTenantIdentifier());
        }

        public async Task<AppUser> UpdatePasswordAsync(UpdatePasswordViewModel model)
        {
            AppUser user = await GetSimpleAsync(model.Identifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }
            if (!String.IsNullOrWhiteSpace(model.Password))
            {
                user.PasswordHash = model.Password;
            }

            if (model.IsLoginReset)
            {
                user.NewPasswordRequired = false;
            }

            ctx.Users.Update(user);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(user);

            return user;
        }

        public async Task<AppUser> UpdateSecuritySettingsAsync(UpdateSecuritySettingsViewModel model)
        {
            AppUser user = await GetSimpleAsync(model.Identifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

            user.NewPasswordRequired = model.SetPasswordOnNextLogin;

            ctx.Users.Update(user);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(user);

            return user;
        }
        
        public async Task<AppUser> GetAsync(int id)
        {
            AppUser user;

            user = await ctx.Users
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ug => ug.UserTenantUserGroups)
                        .ThenInclude(ug => ug.UserGroup)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantSessions)
                        .ThenInclude(uts => uts.UserTenantSessionType)

            .AsNoTracking()
            .SingleOrDefaultAsync(au => au.Id == id);

            ctx.DetachEntity(user);

            return user;
        }

        public async Task<AppUser> SetPasswordHashAsync(UserIdentifier userIdentifier, string passwordHash)
        {
            AppUser user = await GetSimpleAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

            user.PasswordHash = passwordHash;

            ctx.Users.Update(user);

            await ctx.SaveChangesAsync();

            return user;
        }

        public async Task<IList<AppUser>> GetListAsync(bool includeTemporaryUsers = true)
        {
            IList<AppUser> appUsers = new List<AppUser>();

            appUsers = await ctx.Users
                .FilterTemporaryUsersEFQuery(includeTemporaryUsers)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ug => ug.UserTenantUserGroups)
                        .ThenInclude(aug => aug.UserGroup)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantSessions)
                        .ThenInclude(uts => uts.UserTenantSessionType)
                .AsNoTracking()
                .ToListAsync();

            ctx.DetachEntity(appUsers);

            return appUsers;
        }

        public async Task<IList<AppUser>> GetListAsync(TenantIdentifier tenantIdentifier, bool includeTemporaryUsers = true)
        {
            IList<AppUser> appUsers = new List<AppUser>();

            // if there is a tenant code, then we need a project code
            if (!string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode) && string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                throw new IdPException(ErrorCode.GENERIC_ERROR, "A project code is needed when supplying a tenant code");

            appUsers = await ctx.Users
                .FilterTemporaryUsersEFQuery(includeTemporaryUsers)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(u => u.UserDetails)
                .Include(u => u.UserTenants)
                    .ThenInclude(ug => ug.UserTenantUserGroups)
                        .ThenInclude(ug => ug.UserGroup)
                .Include(u => u.UserTenants)
                    .ThenInclude(ut => ut.UserTenantSessions)
                        .ThenInclude(uts => uts.UserTenantSessionType)
                // if there is a project code then get for the project, otherwise get all
                // if there isn't a tenant code, then get for the given project
                .Where(u => u.UserTenants.Any(ut => (ut.Tenant.Project.Code == tenantIdentifier.ProjectCode || string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                && (ut.Tenant.Code == tenantIdentifier.TenantCode || string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode))))
                .AsNoTracking()
                .ToListAsync();

            ctx.DetachEntity(appUsers);

            return appUsers;
        }

        public async Task<int> DeleteAsync(UserIdentifier userIdentifier)
        {
            var user = await GetToDeleteAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userIdentifier.ToString());
            }

            if (user.UserDetails != null)
            {
                ctx.UserDetails.Remove(user.UserDetails);
            }

            if (user.UserTenants != null)
            {
                foreach (var userTenant in user.UserTenants)
                {
                    if (userTenant.UserClaims != null)
                    {
                        ctx.UserClaims.RemoveRange(userTenant.UserClaims);
                    }
                    if (userTenant.UserTenantUserGroups != null)
                    {
                        ctx.ApplicationUserToUserGroups.RemoveRange(userTenant.UserTenantUserGroups);
                    }
                    if (userTenant.UserGroupManagement != null)
                    {
                        ctx.UserToUserGroupManagement.RemoveRange(userTenant.UserGroupManagement);
                    }
                }
                ctx.UserTenants.RemoveRange(user.UserTenants);

            }


            ctx.Users.Remove(user);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<bool> ExistsAsync(UserIdentifier userIdentifier)
        {
            return await ctx.Users.CountAsync(au => au.ObjectIdentifier == userIdentifier.UserGUID
                || (userIdentifier.UserGUID == null && au.TenantUsername == userIdentifier.Username
                && au.UserTenants.Any(ut => ut.Tenant.Code == userIdentifier.TenantCode
                && ut.Tenant.Project.Code == userIdentifier.ProjectCode))) > 0;
        }


        public async Task<bool> UploadProfilePictureAsync(UserIdentifier userIdentifier, byte[] data)
        {
            AppUser user = await GetSimpleAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, userIdentifier.ToString());
            }

            UserDetails userDetails = await GetUserDetailsAsync(userIdentifier);

            if (userDetails == null)
            {
                // wasn't found so we create
                userDetails = new UserDetails();

                await ctx.UserDetails.AddAsync(userDetails);
                await ctx.SaveChangesAsync();

                // attach new details to user and save
                user.UserDetailsId = userDetails.Id;

                ctx.Users.Update(user);
                await ctx.SaveChangesAsync();

                ctx.DetachEntity(user);
            }

            if (!userDetails.ProfileImageId.HasValue)
            {
                // we create the image
                var profileImage = new UserProfileImage();
                profileImage.Data = data;

                // save image
                await ctx.UserProfileImages.AddAsync(profileImage);

                await ctx.SaveChangesAsync();

                // save image reference
                userDetails.ProfileImageId = profileImage.Id;
                ctx.UserDetails.Update(userDetails);

                await ctx.SaveChangesAsync();
            }
            else
            {
                // we set the image
                var profileImage = await ctx.UserProfileImages.FirstAsync(upi => upi.Id == userDetails.ProfileImageId.Value);
                profileImage.Data = data;
                ctx.UserProfileImages.Update(profileImage);

                // save image
                await ctx.SaveChangesAsync();
            }



            ctx.DetachEntity(userDetails);

            // all went well
            return true;
        }
    }
}
