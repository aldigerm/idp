﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IPasswordPolicyDataProvider
    {
        Task<PasswordPolicy> GetAsync(PasswordPolicyIdentifier userIdentifier);
        Task<PasswordPolicy> GetSimpleAsync(PasswordPolicyIdentifier userIdentifier);
        Task<PasswordPolicyIdentifier> CreateAsync(PasswordPolicyCreateModel model);
        Task<PasswordPolicyIdentifier> UpdateAsync(PasswordPolicyUpdateModel model);
        Task<PasswordPolicy> GetAsync(int id);
        Task<IList<PasswordPolicy>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<int> DeleteAsync(PasswordPolicyIdentifier userIdentifier);
        Task<bool> ExistsAsync(PasswordPolicyIdentifier userIdentifier);
        Task<IList<PasswordPolicy>> GetListAsync();
    }
}
