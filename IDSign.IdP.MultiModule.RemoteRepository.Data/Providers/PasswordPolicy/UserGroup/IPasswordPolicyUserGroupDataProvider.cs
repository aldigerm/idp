﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IPasswordPolicyUserGroupDataProvider
    {
        Task<PasswordPolicyUserGroup> GetAsync(int id);
        Task<int> DeleteAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier);
        Task<PasswordPolicyUserGroupIdentifier> AddAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier);
        Task<PasswordPolicyUserGroupIdentifier> UpdateAsync(PasswordPolicyUserGroupModel passwordPolicyUserGroupModel);
        Task<PasswordPolicyUserGroup> GetAsync(PasswordPolicyUserGroupIdentifier passwordPolicyUserGroupIdentifier);
        Task<List<PasswordPolicyUserGroup>> GetListAsync(UserGroupIdentifier userGroupIdentifier);

    }
}
