﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class PasswordPolicyUserGroupData : IPasswordPolicyUserGroupDataProvider
    {
        private IPasswordPolicyDataProvider _PasswordPolicyDataProvider;
        private IUserGroupDataProvider _UserGroupDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<PasswordPolicyData> _logger;
        public PasswordPolicyUserGroupData(
              IPasswordPolicyDataProvider passwordPolicyDataProvider
            , IUserGroupDataProvider userGroupDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<PasswordPolicyData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _PasswordPolicyDataProvider = passwordPolicyDataProvider;
            _UserGroupDataProvider = userGroupDataProvider;
        }

        public async Task<PasswordPolicyUserGroup> GetAsync(int id)
        {
            return await ctx.PasswordPolicyUserGroup
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.PasswordPolicyType)
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.Tenant)
                .Include(ppug => ppug.UserGroup)
                    .ThenInclude(ug => ug.Tenant)
                        .ThenInclude(t => t.Project)
                .SingleOrDefaultAsync(ppug => ppug.Id == id);
        }

        public async Task<PasswordPolicyUserGroup> GetAsync(PasswordPolicyUserGroupIdentifier passwordPolicyUserGroupIdentifier)
        {
            return await ctx.PasswordPolicyUserGroup
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.PasswordPolicyType)
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.Tenant)
                .Include(ppug => ppug.UserGroup)
                    .ThenInclude(ug => ug.Tenant)
                        .ThenInclude(t => t.Project)
                .SingleOrDefaultAsync(ppug =>
                ppug.UserGroup.Code == passwordPolicyUserGroupIdentifier.UserGroupCode
                && ppug.UserGroup.Tenant.Code == passwordPolicyUserGroupIdentifier.TenantCode
                && ppug.UserGroup.Tenant.Project.Code == passwordPolicyUserGroupIdentifier.ProjectCode
                && ppug.PasswordPolicy.Code == passwordPolicyUserGroupIdentifier.PasswordPolicyCode
                && ppug.PasswordPolicy.Tenant.Code == passwordPolicyUserGroupIdentifier.ProjectCode);
        }

        public async Task<List<PasswordPolicyUserGroup>> GetListAsync(UserGroupIdentifier userGroupIdentifier)
        {
            return await ctx.PasswordPolicyUserGroup
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.PasswordPolicyType)
                .Include(ppug => ppug.PasswordPolicy)
                    .ThenInclude(pp => pp.Tenant)
                .Include(ppug => ppug.UserGroup)
                    .ThenInclude(ug => ug.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(ppug =>
                    ppug.UserGroup.Code == userGroupIdentifier.UserGroupCode
                    && ppug.UserGroup.Tenant.Code == userGroupIdentifier.TenantCode
                    && ppug.UserGroup.Tenant.Project.Code == userGroupIdentifier.ProjectCode)
                .ToListAsync();
        }

        public async Task<int> DeleteAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier)
        {
            var ug = await _UserGroupDataProvider.GetSimpleAsync(userGroupIdentifier);
            if (ug == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifier?.ToString());
            }
            var pp = await _PasswordPolicyDataProvider.GetSimpleAsync(passwordPolicyIdentifier);
            if (pp == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND, passwordPolicyIdentifier?.ToString());
            }

            var toDelete = ctx.PasswordPolicyUserGroup.Where(ppug => ppug.UserGroupId == ug.Id && ppug.PasswordPolicyId == pp.Id);

            if (toDelete != null && toDelete.Any())
            {
                var count = toDelete.Count();

                ctx.PasswordPolicyUserGroup.RemoveRange(toDelete);

                await ctx.SaveChangesAsync();

                return count;
            }
            else
            {
                return 0;
            }

        }

        public async Task<PasswordPolicyUserGroupIdentifier> AddAsync(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier)
        {
            var identifier = new PasswordPolicyUserGroupIdentifier(passwordPolicyIdentifier, userGroupIdentifier);
            if (await ExistsAsync(identifier))
            {
                throw new IdPException(ErrorCode.PASSWORD_POLICY_USERGROUP_EXISTS, identifier.ToString());
            }
            var ug = await _UserGroupDataProvider.GetSimpleAsync(userGroupIdentifier);
            if (ug == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifier.ToString());
            }
            var pp = await _PasswordPolicyDataProvider.GetSimpleAsync(passwordPolicyIdentifier);
            if (pp == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND, passwordPolicyIdentifier.ToString());
            }

            var entity = new PasswordPolicyUserGroup();

            entity.PasswordPolicyId = pp.Id;
            entity.UserGroupId = ug.Id;

            await ctx.PasswordPolicyUserGroup.AddAsync(entity);

            await ctx.SaveChangesAsync();

            return identifier;
        }

        public async Task<PasswordPolicyUserGroupIdentifier> UpdateAsync(PasswordPolicyUserGroupModel model)
        {
            var ent = await ctx.PasswordPolicyUserGroup
                .SingleOrDefaultAsync(ppug =>
                ppug.UserGroup.Code == model.Identifier.UserGroupCode
                && ppug.UserGroup.Tenant.Code == model.Identifier.TenantCode
                && ppug.UserGroup.Tenant.Project.Code == model.Identifier.ProjectCode
                && ppug.PasswordPolicy.Code == model.Identifier.PasswordPolicyCode
                && ppug.PasswordPolicy.Tenant.Code == model.Identifier.ProjectCode);

            if (ent == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_USERGROUP_NOT_FOUND);
            }

            ctx.PasswordPolicyUserGroup.Update(ent);

            await ctx.SaveChangesAsync();

            return model.Identifier;
        }

        public async Task<bool> ExistsAsync(PasswordPolicyUserGroupIdentifier identifier)
        {
            return await ctx.PasswordPolicyUserGroup
                .CountAsync(ppug =>
                ppug.UserGroup.Code == identifier.UserGroupCode
                && ppug.UserGroup.Tenant.Code == identifier.TenantCode
                && ppug.UserGroup.Tenant.Project.Code == identifier.ProjectCode
                && ppug.PasswordPolicy.Code == identifier.PasswordPolicyCode
                && ppug.PasswordPolicy.Tenant.Code == identifier.ProjectCode) > 0;
        }
    }
}
