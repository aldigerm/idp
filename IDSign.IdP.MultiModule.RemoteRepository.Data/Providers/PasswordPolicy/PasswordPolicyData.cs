﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class PasswordPolicyData : IPasswordPolicyDataProvider
    {
        private IPasswordPolicyTypeDataProvider _PasswordPolicyTypeDataProvider;
        private ITenantDataProvider _TenantDataProvider;
        private IUserGroupDataProvider _UserGroupDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<PasswordPolicyData> _logger;
        public PasswordPolicyData(
             ITenantDataProvider tenantDataProvider
            , IPasswordPolicyTypeDataProvider passwordPolicyTypeDataProvider
            , IUserGroupDataProvider userGroupDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<PasswordPolicyData> logger
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _PasswordPolicyTypeDataProvider = passwordPolicyTypeDataProvider;
            _TenantDataProvider = tenantDataProvider;
            _UserGroupDataProvider = userGroupDataProvider;
        }

        public async Task<PasswordPolicyIdentifier> CreateAsync(PasswordPolicyCreateModel model)
        {

            PasswordPolicy passwordPolicy = await GetSimpleAsync(model.GetPasswordPolicyIdentifier());
            if (passwordPolicy != null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_EXISTS, model.GetPasswordPolicyIdentifier()?.ToString());
            }
            PasswordPolicyType type = await _PasswordPolicyTypeDataProvider.GetAsync(model.PasswordPolicyTypeIdentifier);
            if (type == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND, model.PasswordPolicyTypeIdentifier.ToString());
            }
            Tenant tenant = await _TenantDataProvider.GetSimpleAsync(model.GetPasswordPolicyIdentifier().GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.GetPasswordPolicyIdentifier()?.GetTenantIdentifier()?.ToString());
            }

            passwordPolicy = new PasswordPolicy();
            passwordPolicy.PasswordPolicyTypeId = type.Id;
            passwordPolicy.TenantId = tenant.Id;
            passwordPolicy.Code = model.PasswordPolicyCode;
            passwordPolicy.Data = model.Data;
            passwordPolicy.DefaultErrorMessage = model.DefaultErrorMessage;
            passwordPolicy.Description = model.Description;

            await ctx.PasswordPolicies.AddAsync(passwordPolicy);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(passwordPolicy);

            return await GetIdentifier(passwordPolicy);
        }

        public async Task<PasswordPolicy> GetSimpleAsync(PasswordPolicyIdentifier identifier)
        {
            PasswordPolicy passwordPolicy = await ctx.PasswordPolicies
                .AsNoTracking()
                .SingleOrDefaultAsync(pp => pp.Code == identifier.PasswordPolicyCode
                && pp.Tenant.Code == identifier.TenantCode);

            if (passwordPolicy == null)
            {
                _logger.LogWarning("PasswordPolicy not found {0}", identifier);
            }
            return passwordPolicy;
        }

        public async Task<PasswordPolicy> GetAsync(PasswordPolicyIdentifier identifier)
        {
            if (identifier == null)
            {
                _logger.LogWarning("identifier is null. PasswordPolicy returned will be null");
                return null;
            }

            PasswordPolicy passwordPolicy = await ctx.PasswordPolicies
                .Include(pp => pp.PasswordPolicyType)
                .Include(pp => pp.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(pp => pp.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.UserGroup)
                        .ThenInclude(ug => ug.Tenant)
                .AsNoTracking()
                .SingleOrDefaultAsync(pp => 
                pp.Code == identifier.PasswordPolicyCode
                && pp.Tenant.Code == identifier.TenantCode
                && pp.Tenant.Project.Code == identifier.ProjectCode);

            if (passwordPolicy == null)
            {
                _logger.LogWarning("PasswordPolicy not found {0}", identifier);
            }
            return passwordPolicy;
        }

        public async Task<PasswordPolicy> GetToDeleteAsync(PasswordPolicyIdentifier identifier)
        {
            PasswordPolicy passwordPolicy = await ctx.PasswordPolicies
                .Include(pp => pp.PasswordPolicyUserGroup)
                .AsNoTracking()
                .SingleOrDefaultAsync(pp => pp.Tenant.Code == identifier.TenantCode && pp.Code == identifier.PasswordPolicyCode);

            if (passwordPolicy == null)
            {
                _logger.LogWarning("PasswordPolicy not found {0}", identifier);
            }
            return passwordPolicy;
        }

        private async Task<PasswordPolicyIdentifier> GetIdentifier(PasswordPolicy passwordPolicy)
        {
            return (await GetAsync(passwordPolicy.Id)).GetIdentifier();
        }

        public async Task<PasswordPolicyIdentifier> UpdateAsync(PasswordPolicyUpdateModel model)
        {
            PasswordPolicy passwordPolicy = await GetSimpleAsync(model.Identifier);
            if (passwordPolicy == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND, model.Identifier?.ToString());
            }
            Tenant tenant = await _TenantDataProvider.GetSimpleAsync(model.Identifier.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, model.Identifier?.GetTenantIdentifier()?.ToString());
            }
            PasswordPolicyType passwordPolicyType = await _PasswordPolicyTypeDataProvider.GetSimpleAsync(model.PasswordPolicyTypeIdentifier);
            if (passwordPolicyType == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND, model.PasswordPolicyTypeIdentifier?.ToString());
            }

            passwordPolicy.TenantId = tenant.Id;
            passwordPolicy.PasswordPolicyTypeId = passwordPolicyType.Id;
            passwordPolicy.Code = model.NewCode ?? model.Identifier.PasswordPolicyCode;
            passwordPolicy.Data = model.Data;
            passwordPolicy.DefaultErrorMessage = model.DefaultErrorMessage;
            passwordPolicy.Description = model.Description;

            ctx.PasswordPolicies.Update(passwordPolicy);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(passwordPolicy);

            return await GetIdentifier(passwordPolicy);
        }

        public async Task<PasswordPolicy> GetAsync(int id)
        {
            PasswordPolicy passwordPolicy;

            passwordPolicy = await ctx.PasswordPolicies
                .Include(pp => pp.PasswordPolicyType)
                .Include(pp => pp.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(pp => pp.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.UserGroup)
                        .ThenInclude(ug => ug.Tenant)
            .AsNoTracking()
            .SingleOrDefaultAsync(pp => pp.Id == id);

            return passwordPolicy;
        }

        public async Task<IList<PasswordPolicy>> GetListAsync()
        {
            IList<PasswordPolicy> passwordPolicies = new List<PasswordPolicy>();

            passwordPolicies = await ctx.PasswordPolicies
                .Include(pp => pp.PasswordPolicyType)
                .Include(pp => pp.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(pp => pp.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.UserGroup)
                        .ThenInclude(ug => ug.Tenant)
                .AsNoTracking()
                .ToListAsync();

            return passwordPolicies;
        }

        public async Task<IList<PasswordPolicy>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            IList<PasswordPolicy> passwordPolicies = new List<PasswordPolicy>();

            // if there is a tenant code, then we need a tenant code
            if (tenantIdentifier != null && !string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode) && string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode))
                throw new IdPException(ErrorCode.GENERIC_ERROR, "A tenant code is needed when supplying a tenant code");

            passwordPolicies = await ctx.PasswordPolicies
                .Include(pp => pp.PasswordPolicyType)
                .Include(pp => pp.Tenant)
                    .ThenInclude(t => t.Project)
                // if there is a tenant code then get for the tenant, otherwise get all
                // if there isn't a tenant code, then get for the given tenant
                .Where(pp => tenantIdentifier == null || 
                    ((pp.Tenant.Code == tenantIdentifier.TenantCode || string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode))
                    && (pp.Tenant.Project.Code == tenantIdentifier.ProjectCode || string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))))
                .AsNoTracking()
                .ToListAsync();

            return passwordPolicies;
        }

        public async Task<int> DeleteAsync(PasswordPolicyIdentifier identifier)
        {
            var passwordPolicy = await GetToDeleteAsync(identifier);
            if (passwordPolicy == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_NOT_FOUND, identifier.ToString());
            }

            if (passwordPolicy.PasswordPolicyUserGroup != null)
            {
                ctx.PasswordPolicyUserGroup.RemoveRange(passwordPolicy.PasswordPolicyUserGroup);
            }

            ctx.PasswordPolicies.Remove(passwordPolicy);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<bool> ExistsAsync(PasswordPolicyIdentifier identifier)
        {
            return await ctx.PasswordPolicies.CountAsync(pp => 
                   pp.Code == identifier.PasswordPolicyCode
                && pp.Tenant.Code == identifier.TenantCode
                && pp.Tenant.Project.Code == identifier.ProjectCode) > 0;
        }

    }
}
