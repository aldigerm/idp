﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IPasswordPolicyTypeDataProvider
    {
        Task<PasswordPolicyType> GetAsync(PasswordPolicyTypeIdentifier identifier);
        Task<PasswordPolicyType> GetSimpleAsync(PasswordPolicyTypeIdentifier identifier);
        Task<PasswordPolicyTypeIdentifier> CreateAsync(PasswordPolicyTypeCreateModel model);
        Task<int> DeleteAsync(PasswordPolicyTypeIdentifier identifier);
        Task<PasswordPolicyTypeIdentifier> UpdateAsync(PasswordPolicyTypeUpdateModel model);
        Task<List<PasswordPolicyType>> GetListAsync();
    }
}
