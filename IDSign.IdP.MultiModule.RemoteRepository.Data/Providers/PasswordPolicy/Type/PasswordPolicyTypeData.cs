﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class PasswordPolicyTypeData : IPasswordPolicyTypeDataProvider
    {
        private IProjectDataProvider _ProjectDataProvider;
        private IRoleDataProvider _RoleDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<PasswordPolicyTypeData> _logger;
        public PasswordPolicyTypeData(
            IProjectDataProvider projectDataProvider
            , RemoteRepositoryDbContext ctx
            , ILogger<PasswordPolicyTypeData> logger
            , IRoleDataProvider roleDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _ProjectDataProvider = projectDataProvider;
            _RoleDataProvider = roleDataProvider;
        }

        public async Task<PasswordPolicyType> GetSimpleAsync(PasswordPolicyTypeIdentifier identifier)
        {
            return await ctx.PasswordPolicyTypes.Where(ppt => ppt.Code == identifier.PasswordPolicyTypeCode).FirstOrDefaultAsync();
        }

        public async Task<PasswordPolicyType> GetAsync(PasswordPolicyTypeIdentifier identifier)
        {
            return await ctx.PasswordPolicyTypes
                .Include(ppt => ppt.PasswordPolicies)
                .Where(ppt => ppt.Code == identifier.PasswordPolicyTypeCode)
                .FirstOrDefaultAsync();
        }

        public async Task<PasswordPolicyTypeIdentifier> CreateAsync(PasswordPolicyTypeCreateModel model)
        {
            var passwordPolicyType = await GetSimpleAsync(model.GetIdentifier());

            if (passwordPolicyType != null)
            {
                // exact match found
                throw new IdPException(ErrorCode.PASSWORD_POLICY_TYPE_EXISTS, model.ToString());
            }

            passwordPolicyType = new PasswordPolicyType();

            passwordPolicyType.Code = model.Code;
            passwordPolicyType.Description = model.Description;

            await ctx.PasswordPolicyTypes.AddAsync(passwordPolicyType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(passwordPolicyType);

            return await GetIdentifier(passwordPolicyType);
        }

        public async Task<PasswordPolicyTypeIdentifier> UpdateAsync(PasswordPolicyTypeUpdateModel model)
        {
            var passwordPolicyType = await GetSimpleAsync(model.Identifier);

            if (passwordPolicyType == null)
            {
                // exact match found
                throw new IdPException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND, model.ToString());
            }

            passwordPolicyType.Code = model.NewCode ?? model.Identifier.PasswordPolicyTypeCode;
            passwordPolicyType.Description = model.Description;

            ctx.PasswordPolicyTypes.Update(passwordPolicyType);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(passwordPolicyType);

            return await GetIdentifier(passwordPolicyType);
        }
        private async Task<PasswordPolicyTypeIdentifier> GetIdentifier(PasswordPolicyType entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<PasswordPolicyType> GetAsync(int id)
        {
            return await ctx.PasswordPolicyTypes
                .Include(ppt => ppt.PasswordPolicies)
                    .ThenInclude(pp => pp.Tenant)
                        .ThenInclude(t => t.Project)
                .Where(ppt => ppt.Id == id)
                .FirstOrDefaultAsync();
        }

        private async Task<PasswordPolicyType> GetToDeleteAsync(PasswordPolicyTypeIdentifier identifier)
        {            
            return await ctx.PasswordPolicyTypes.Include(ty => ty.PasswordPolicies).Where(ppt => ppt.Code == identifier.PasswordPolicyTypeCode).FirstOrDefaultAsync();
        }

        public async Task<int> DeleteAsync(PasswordPolicyTypeIdentifier identifier)
        {
            var type = await GetToDeleteAsync(identifier);
            if (type == null)
            {
                throw new IdPNotFoundException(ErrorCode.PASSWORD_POLICY_TYPE_NOT_FOUND, identifier.ToString());
            }

            if(type.PasswordPolicies != null)
            {
                ctx.PasswordPolicies.RemoveRange(type.PasswordPolicies);
            }

            ctx.PasswordPolicyTypes.Remove(type);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<List<PasswordPolicyType>> GetListAsync()
        {
            return await ctx.PasswordPolicyTypes
                .Include(ppt => ppt.PasswordPolicies)
                    .ThenInclude(pp => pp.PasswordPolicyType)
                .Include(ppt => ppt.PasswordPolicies)
                    .ThenInclude(pp => pp.Tenant)
                        .ThenInclude(t => t.Project)
                .AsNoTracking()
                .ToListAsync();
        }    
    }
}
