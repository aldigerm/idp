﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserGroupDataProvider
    {
        Task<UserGroup> GetAsync(int id);
        Task<UserGroup> GetSimpleAsync(UserGroupIdentifier identifier);
        Task<List<UserGroup>> GetForClaimsAsync(List<string> claims, TenantIdentifier tenantIdentifier);
        Task<UserGroup> GetAsync(UserGroupIdentifier userGroupIdentifier);
        Task<List<UserGroup>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<List<UserGroup>> GetListAsync();
        Task<UserGroup> CreateOrUpdateAsync(UserGroupUpdateViewModel model);
        Task<UserGroup> CreateAsync(UserGroupCreateModel model);
        Task<UserGroupIdentifier> UpdateAsync(UserGroupUpdateViewModel model);
        Task<int> DeleteAsync(UserGroupIdentifier userGroupIdentifier);
        Task<AppUser> AddUserToGroup(UserIdentifier userIdentifier, string userGroupCode);
        Task<AppUser> RemoveUserFromGroup(UserIdentifier userIdentifier, string code);
        Task<UserGroup> SetInheritingUserGroups(UserGroupIdentifier userGroupIdentifier, IList<string> inheritsFromUserGroups);
        Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task<bool> ExistsAsync(UserGroupIdentifier userGroupIdentifier);
        Task<List<UserGroup>> GetListAsync(ProjectIdentifier projectIdentifier);
    }
}
