﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserGroupData : IUserGroupDataProvider
    {
        protected readonly ILogger<UserGroupData> _logger;
        private RemoteRepositoryDbContext ctx;
        private ITenantDataProvider _TenantDataProvider;
        private IUserDataProvider _UserDataProvider;
        public UserGroupData(
              RemoteRepositoryDbContext _ctx
            , ILogger<UserGroupData> logger
            , ITenantDataProvider tenantDataProvider
            , IUserDataProvider userDataProvider
            )
        {
            ctx = _ctx;
            _logger = logger;
            _TenantDataProvider = tenantDataProvider;
            _UserDataProvider = userDataProvider;
        }

        public async Task<UserGroup> GetAsync(int id)
        {
            UserGroup group = null;

            group = await
                ctx.UserGroups
                .Include(ug => ug.UserGroupRoles)
                    .ThenInclude(ugr => ugr.Role)
                .Include(ug => ug.UserGroupsWhichInheritFromCurrent)
                    .ThenInclude(ing => ing.UserGroup)
                .Include(ug => ug.UserGroupsWhichInheritFromCurrent)
                    .ThenInclude(ing => ing.InheritsFrom)
                .Include(ug => ug.InheritingFromTheseUserGroups)
                    .ThenInclude(ing => ing.UserGroup)
                .Include(ug => ug.InheritingFromTheseUserGroups)
                    .ThenInclude(ing => ing.InheritsFrom)
                .Include(ug => ug.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(ug => ug.ApplicationUserGroups)
                    .ThenInclude(aug => aug.UserTenant)
                        .ThenInclude(ut => ut.User)
                .Include(ug => ug.UserGroupClaims)
                    .ThenInclude(ugc => ugc.UserGroupClaimType)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.UserGroup)
                        .ThenInclude(ug => ug.Tenant)
                            .ThenInclude(t => t.Project)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.PasswordPolicy)
                        .ThenInclude(pp => pp.Tenant)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.PasswordPolicy)
                        .ThenInclude(pp => pp.PasswordPolicyType)
                .AsNoTracking()
                .SingleOrDefaultAsync(ug => ug.Id == id);

            return group;
        }

        public async Task<UserGroup> GetSimpleAsync(UserGroupIdentifier identifier)
        {
            UserGroup group = null;

            group = await
                ctx.UserGroups
                .AsNoTracking()
                .SingleOrDefaultAsync(ug => ug.Code == identifier.UserGroupCode && ug.Tenant.Code == identifier.TenantCode && ug.Tenant.Project.Code == identifier.ProjectCode);


            return group;
        }

        public async Task<UserGroup> GetAsync(UserGroupIdentifier identifier)
        {
            UserGroup group = null;

            group = await
                ctx.UserGroups
                .Include(ug => ug.UserGroupRoles)
                    .ThenInclude(ugr => ugr.Role)
                .Include(ug => ug.UserGroupsWhichInheritFromCurrent)
                    .ThenInclude(ing => ing.UserGroup)
                .Include(ug => ug.UserGroupsWhichInheritFromCurrent)
                    .ThenInclude(ing => ing.InheritsFrom)
                .Include(ug => ug.InheritingFromTheseUserGroups)
                    .ThenInclude(ing => ing.UserGroup)
                .Include(ug => ug.InheritingFromTheseUserGroups)
                    .ThenInclude(ing => ing.InheritsFrom)
                .Include(ug => ug.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(ug => ug.ApplicationUserGroups)
                    .ThenInclude(aug => aug.UserTenant)
                        .ThenInclude(ut => ut.User)
                .Include(ug => ug.UserGroupClaims)
                    .ThenInclude(ugc => ugc.UserGroupClaimType)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.UserGroup)
                        .ThenInclude(ug => ug.Tenant)
                            .ThenInclude(t => t.Project)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.PasswordPolicy)
                        .ThenInclude(pp => pp.Tenant)
                .Include(ug => ug.PasswordPolicyUserGroup)
                    .ThenInclude(ppug => ppug.PasswordPolicy)
                        .ThenInclude(pp => pp.PasswordPolicyType)
                .AsNoTracking()
                .SingleOrDefaultAsync(ug =>
                    ug.Code == identifier.UserGroupCode
                    && ug.Tenant.Code == identifier.TenantCode
                    && ug.Tenant.Project.Code == identifier.ProjectCode
                    );


            return group;
        }


        public async Task<UserGroup> GetToDeleteAsync(UserGroupIdentifier identifier)
        {
            UserGroup group = null;

            group = await
                ctx.UserGroups
                .Include(ug => ug.UserGroupRoles)
                .Include(ug => ug.UserGroupsWhichInheritFromCurrent)
                .Include(ug => ug.InheritingFromTheseUserGroups)
                .Include(ug => ug.ApplicationUserGroups)
                .Include(ug => ug.UserGroupClaims)
                .Include(ug => ug.UserGroupManagement)
                .AsNoTracking()
                .SingleOrDefaultAsync(ug => ug.Code == identifier.UserGroupCode && ug.Tenant.Code == identifier.TenantCode && ug.Tenant.Project.Code == identifier.ProjectCode);


            return group;
        }

        public async Task<List<UserGroup>> GetForClaimsAsync(List<string> claims, TenantIdentifier tenantIdentifier)
        {
            List<UserGroup> groups = null;

            groups = await
                ctx.UserGroups
                .Where(ug => ug.Tenant.Code == tenantIdentifier.TenantCode && ug.Tenant.Project.Code == tenantIdentifier.ProjectCode && claims.Any(c => c == ug.Code))
                .AsNoTracking()
                .ToListAsync();

            return groups;
        }

        public async Task<List<UserGroup>> GetListAsync(TenantIdentifier tenantIdentifier)
        {
            List<UserGroup> userGroups = null;

            // if there is a tenant code, then we need a project code
            if (!string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode) && string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                throw new IdPException(ErrorCode.GENERIC_ERROR, "A project code is needed when supplying a tenant code");


            userGroups = await
                ctx.UserGroups
                .Include(ug => ug.Tenant)
                    .ThenInclude(t => t.Project)
                .Where(u => (u.Tenant.Project.Code == tenantIdentifier.ProjectCode || string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                && (u.Tenant.Code == tenantIdentifier.TenantCode || string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode)))
                .AsNoTracking()
                .ToListAsync();

            return userGroups;
        }

        public async Task<List<UserGroup>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            return await GetListAsync(new TenantIdentifier(null, projectIdentifier));
        }

        public async Task<List<UserGroup>> GetListAsync()
        {
            List<UserGroup> users;

            users = await ctx.UserGroups
            .AsNoTracking()
            .ToListAsync();

            return users;
        }

        public async Task<UserGroup> CreateOrUpdateAsync(UserGroupUpdateViewModel model)
        {
            UserGroup newUserGroup;
            var exists = await ExistsAsync(model.Identifier);
            if (!exists)
            {
                newUserGroup = await CreateAsync(new UserGroupCreateModel()
                {
                    UserGroupCode = model.NewUserGroupCode ?? model.Identifier.UserGroupCode,
                    Description = model.Description,
                    InheritsFromUserGroups = model.InheritsFromUserGroups,
                    TenantIdentifier = new TenantIdentifier(model.Identifier.TenantCode, model.Identifier.ProjectCode),
                    Usernames = model.Usernames
                });
            }
            else
            {
                newUserGroup = await GetAsync(await UpdateAsync(model));
            }
            return newUserGroup;
        }

        public async Task<UserGroup> CreateAsync(UserGroupCreateModel model)
        {
            var newUserGroup = await GetSimpleAsync(model.GetUserGroupIdentifier());
            if (newUserGroup != null)
            {
                throw new IdPException(ErrorCode.USERGROUP_ALREADY_EXISTS, "Couldn't create usergroup.");
            }
            var tenant = await _TenantDataProvider.GetAsync(model.TenantIdentifier);
            if (tenant == null)
            {
                throw new IdPException(ErrorCode.TENANT_NOT_FOUND, "Couldn't create usergroup.");
            }

            newUserGroup = new UserGroup();

            newUserGroup.Code = model.UserGroupCode;
            newUserGroup.Description = model.Description;
            newUserGroup.TenantId = tenant.Id;

            await ctx.UserGroups.AddAsync(newUserGroup);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(newUserGroup);

            return newUserGroup;
        }

        public async Task<UserGroupIdentifier> UpdateAsync(UserGroupUpdateViewModel model)
        {
            var userGroup = await GetSimpleAsync(model.Identifier);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, "Couldn't find user group.");
            }
            var tenant = await _TenantDataProvider.GetAsync(model.Identifier.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPException(ErrorCode.TENANT_NOT_FOUND, "Couldn't create usergroup.");
            }
            // we update the code ONLY IF a new one is provided
            userGroup.Code = model.NewUserGroupCode ?? model.Identifier.UserGroupCode;
            userGroup.Description = model.Description;
            userGroup.TenantId = tenant.Id;

            ctx.UserGroups.Update(userGroup);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userGroup);

            return await GetIdentifier(userGroup);
        }

        private async Task<UserGroupIdentifier> GetIdentifier(UserGroup entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        public async Task<UserGroup> SetInheritingUserGroups(UserGroupIdentifier userGroupIdentifier, IList<string> inheritsFromUserGroups)
        {
            var userGroup = await GetAsync(userGroupIdentifier);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifier.ToString());
            }
            #region User Groups

            if (userGroup.InheritingFromTheseUserGroups == null)
            {
                userGroup.InheritingFromTheseUserGroups = new List<GroupInheritance>();
            }
            if (userGroup.UserGroupsWhichInheritFromCurrent == null)
            {
                userGroup.UserGroupsWhichInheritFromCurrent = new List<GroupInheritance>();
            }
            if (inheritsFromUserGroups == null)
            {
                inheritsFromUserGroups = new List<string>();
            }

            var toRemove = userGroup.InheritingFromTheseUserGroups.Select(inh => inh.InheritsFrom.Code).Except(inheritsFromUserGroups, r => r);
            var toAdd = inheritsFromUserGroups.Except(userGroup.InheritingFromTheseUserGroups.Select(inh => inh.InheritsFrom.Code), r => r);

            if (toRemove.Any())
            {
                var codes = toRemove.ToList();
                foreach (var code in codes)
                {
                    var id = userGroup.InheritingFromTheseUserGroups.Where(inh => inh.InheritsFrom.Code == code).Select(inh => inh.Id);
                    var gs = ctx.UserGroupInheritances.Where(ri => id.Any(i => ri.Id == i));
                    if (gs != null && gs.Any())
                    {
                        ctx.UserGroupInheritances.RemoveRange(gs);
                    }
                }
            }

            if (toAdd.Any())
            {
                foreach (var code in toAdd.ToList().Distinct())
                {
                    var inheritingGroupIdentifier = new UserGroupIdentifier(code, userGroupIdentifier.GetTenantIdentifier());
                    var inheritingGroup = await GetSimpleAsync(inheritingGroupIdentifier);
                    if (inheritingGroup == null)
                    {
                        _logger.LogError("User group to add was not found {0}", inheritingGroupIdentifier.ToString());
                        continue;
                    }
                    var groupInheritance = new GroupInheritance();
                    groupInheritance.InheritsFromId = inheritingGroup.Id;
                    groupInheritance.UserGroupId = userGroup.Id;

                    await ctx.UserGroupInheritances.AddAsync(groupInheritance);

                }
            }
            #endregion

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userGroup);

            return userGroup;
        }


        public async Task<int> DeleteAsync(UserGroupIdentifier userGroupIdentifier)
        {
            var userGroup = await GetToDeleteAsync(userGroupIdentifier);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, "Couldn't delete usergroup.");
            }

            if (userGroup.InheritingFromTheseUserGroups != null)
            {
                ctx.UserGroupInheritances.RemoveRange(userGroup.InheritingFromTheseUserGroups);
            }

            if (userGroup.UserGroupsWhichInheritFromCurrent != null)
            {
                ctx.UserGroupInheritances.RemoveRange(userGroup.UserGroupsWhichInheritFromCurrent);
            }

            if (userGroup.UserGroupRoles != null)
            {
                ctx.UserGroupRoles.RemoveRange(userGroup.UserGroupRoles);
            }

            if (userGroup.UserGroupClaims != null)
            {
                ctx.UserGroupClaims.RemoveRange(userGroup.UserGroupClaims);
            }

            if (userGroup.UserGroupManagement != null)
            {
                ctx.UserToUserGroupManagement.RemoveRange(userGroup.UserGroupManagement);
            }

            if (userGroup.ApplicationUserGroups != null)
            {
                ctx.ApplicationUserToUserGroups.RemoveRange(userGroup.ApplicationUserGroups);
            }

            ctx.UserGroups.Remove(userGroup);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<AppUser> AddUserToGroup(UserIdentifier userIdentifier, string userGroupCode)
        {
            var user = await _UserDataProvider.GetAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, $"User not found for {userIdentifier.Username} {userIdentifier.TenantCode} {userIdentifier.ProjectCode}");
            }
            var userGroup = await GetSimpleAsync(new UserGroupIdentifier(userGroupCode, userIdentifier.GetTenantIdentifier()));
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, $"User group not found for {userGroupCode} {userIdentifier.TenantCode} {userIdentifier.ProjectCode}");
            }

            var userTenant = user.UserTenants.FirstOrDefault(ut => ut.Tenant.GetIdentifier().IsEquals(userIdentifier.GetTenantIdentifier()));
            if (userTenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERTENANT_NOT_FOUND, $"User tenant not found for {userIdentifier.ToString()} ");
            }

            if (user.UserTenants.Any(ut => ut.UserTenantUserGroups.Any(aug => aug.UserGroupId == userGroup.Id)))
            {
                _logger.LogWarning("User already in user group.");
                return user;
            }

            // add user
            UserToUserGroup userToUserGroup = new UserToUserGroup();
            userToUserGroup.UserTenantId = userTenant.Id;
            userToUserGroup.UserGroupId = userGroup.Id;
            await ctx.ApplicationUserToUserGroups.AddAsync(userToUserGroup);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userToUserGroup);

            return user;
        }

        public async Task<AppUser> RemoveUserFromGroup(UserIdentifier userIdentifier, string userGroupCode)
        {
            var user = await _UserDataProvider.GetSimpleAsync(userIdentifier);
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND, $"User not found for {userIdentifier.Username} {userIdentifier.TenantCode} {userIdentifier.ProjectCode}");
            }
            var userGroup = await GetSimpleAsync(new UserGroupIdentifier(userGroupCode, userIdentifier.GetTenantIdentifier()));
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, $"User group not found for {userGroupCode} {userIdentifier.TenantCode} {userIdentifier.ProjectCode}");
            }
            var userToUserGroups = ctx.ApplicationUserToUserGroups.Where(aug => aug.UserGroupId == userGroup.Id && aug.UserTenant.UserId == user.Id);

            if (!userToUserGroups.Any())
            {
                _logger.LogWarning("User not in user group.");
                return user;
            }

            // remove from groups
            ctx.ApplicationUserToUserGroups.RemoveRange(userToUserGroups);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(user);

            return user;
        }

        public async Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, tenantIdentifier.ToString());
            }

            int total = 0;

            if (tenant.UserGroups != null)
            {
                foreach (var userGroupCode in tenant.UserGroups.Select(r => r.Code).ToList())
                {
                    total += await DeleteAsync(new UserGroupIdentifier(userGroupCode, tenantIdentifier));
                }
            }

            ctx.DetachEntity(tenant);
            return total;
        }

        public async Task<bool> ExistsAsync(UserGroupIdentifier userGroupIdentifier)
        {
            return await ctx.UserGroups.CountAsync(au => au.Code == userGroupIdentifier.UserGroupCode
                && au.Tenant.Code == userGroupIdentifier.TenantCode
                && au.Tenant.Project.Code == userGroupIdentifier.ProjectCode) > 0;
        }
    }
}
