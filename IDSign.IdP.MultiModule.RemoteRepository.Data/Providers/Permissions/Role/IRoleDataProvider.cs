﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IRoleDataProvider
    {
        Task<Role> GetAsync(RoleIdentifier roleIdentifier);
        Task<Role> GetAsync(int id);
        Task<Role> GetSimpleAsync(RoleIdentifier identifier);
        Task<Role> CreateOrUpdateAsync(RoleUpdateViewModel roleUpdateViewModel);
        Task<Role> CreateAsync(RoleCreateModel roleUpdateViewModel);
        Task<RoleIdentifier> UpdateAsync(RoleUpdateViewModel roleUpdateViewModel);
        Task<IList<Role>> GetListAsync(TenantIdentifier tenantIdentifier);
        Task<int> DeleteAsync(RoleIdentifier roleIdentifier);
        Task<Role> SetModules(RoleIdentifier roleIdentifier, IList<string> modules);
        Task<Role> SetRoleInheritances(RoleIdentifier roleIdentifier, IList<string> inheritsFromThese);
        Task<Role> SetUserGroups(RoleIdentifier userIdentifier, IList<string> userGroupCodes);
        Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier);
        Task<bool> ExistsAsync(RoleIdentifier roleIdentifier);
        Task<IList<Role>> GetListAsync();
    }
}
