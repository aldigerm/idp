﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class RoleData : IRoleDataProvider
    {
        protected readonly ILogger<RoleData> _logger;
        private ITenantDataProvider _TenantDataProvider;
        private IUserDataProvider _UserDataProvider;
        private IModuleDataProvider _ModuleDataProvider;
        private IUserGroupDataProvider _UserGroupDataProvider;
        private RemoteRepositoryDbContext ctx;
        public RoleData(
              ITenantDataProvider tenantDataProvider
            , RemoteRepositoryDbContext _ctx
            , ILogger<RoleData> logger
            , IUserDataProvider userDataProvider
            , IUserGroupDataProvider userGroupDataProvider
            , IModuleDataProvider moduleDataProvider
            )
        {
            ctx = _ctx;
            _TenantDataProvider = tenantDataProvider;
            _logger = logger;
            _UserGroupDataProvider = userGroupDataProvider;
            _UserDataProvider = userDataProvider;
            _ModuleDataProvider = moduleDataProvider;
        }

        public async Task<IList<Role>> GetListAsync()
        {
            return await ctx.Roles
                .Include(r => r.Tenant)
                    .ThenInclude(t => t.Project)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IList<Role>> GetListAsync(TenantIdentifier tenantIdentifier)
        {

            // if there is a tenant code, then we need a project code
            if (!string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode) && string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                throw new IdPException(ErrorCode.GENERIC_ERROR, "A project code is needed when supplying a tenant code");

            return await ctx.Roles
                .Include(r => r.Tenant)
                    .ThenInclude(t => t.Project)
                .AsNoTracking()
                .Where(u => (u.Tenant.Project.Code == tenantIdentifier.ProjectCode || string.IsNullOrWhiteSpace(tenantIdentifier.ProjectCode))
                && (u.Tenant.Code == tenantIdentifier.TenantCode || string.IsNullOrWhiteSpace(tenantIdentifier.TenantCode)))
               .ToListAsync();

        }

        public async Task<Role> GetAsync(RoleIdentifier roleIdentifier)
        {
            Role role = null;

            role = await ctx.Roles
                .Include(r => r.InheritingFromTheseRoles)
                    .ThenInclude(inh => inh.Role)
                .Include(r => r.InheritingFromTheseRoles)
                    .ThenInclude(inh => inh.InheritsFrom)
                .Include(r => r.RolesWhichInheritFromCurrent)
                    .ThenInclude(inh => inh.Role)
                .Include(r => r.RolesWhichInheritFromCurrent)
                    .ThenInclude(inh => inh.InheritsFrom)
                .Include(r => r.RoleModules)
                    .ThenInclude(rm => rm.Module)
                .Include(r => r.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(r => r.UserGroupsRoles)
                    .ThenInclude(ugr => ugr.UserGroup)
                .Include(r => r.RoleClaims)
                    .ThenInclude(rc => rc.RoleClaimType)
                .AsNoTracking()
                .SingleOrDefaultAsync(r => r.Code == roleIdentifier.RoleCode && r.Tenant.Code == roleIdentifier.TenantCode && r.Tenant.Project.Code == roleIdentifier.ProjectCode);


            return role;
        }


        public async Task<Role> GetToDeleteAsync(RoleIdentifier roleIdentifier)
        {
            Role role = null;

            role = await ctx.Roles
                .Include(r => r.InheritingFromTheseRoles)
                .Include(r => r.RolesWhichInheritFromCurrent)
                .Include(r => r.RoleModules)
                .Include(r => r.UserGroupsRoles)
                .AsNoTracking()
                .SingleOrDefaultAsync(r => r.Code == roleIdentifier.RoleCode && r.Tenant.Code == roleIdentifier.TenantCode && r.Tenant.Project.Code == roleIdentifier.ProjectCode);


            return role;
        }
        public async Task<Role> GetAsync(int id)
        {
            Role role = null;

            role = await ctx.Roles
                .Include(r => r.InheritingFromTheseRoles)
                    .ThenInclude(inh => inh.Role)
                .Include(r => r.InheritingFromTheseRoles)
                    .ThenInclude(inh => inh.InheritsFrom)
                .Include(r => r.RolesWhichInheritFromCurrent)
                    .ThenInclude(inh => inh.Role)
                .Include(r => r.RolesWhichInheritFromCurrent)
                    .ThenInclude(inh => inh.InheritsFrom)
                .Include(r => r.RoleModules)
                    .ThenInclude(rm => rm.Module)
                .Include(r => r.Tenant)
                    .ThenInclude(t => t.Project)
                .Include(r => r.UserGroupsRoles)
                    .ThenInclude(ugr => ugr.UserGroup)
                .Include(r => r.RoleClaims)
                    .ThenInclude(rc => rc.RoleClaimType)
                .AsNoTracking()
                .SingleOrDefaultAsync(r => r.Id == id);


            return role;
        }

        public async Task<Role> GetSimpleAsync(RoleIdentifier identifier)
        {
            Role role = null;

            role = await
                ctx.Roles
                .AsNoTracking()
                .SingleOrDefaultAsync(ug => ug.Code == identifier.RoleCode && ug.Tenant.Code == identifier.TenantCode && ug.Tenant.Project.Code == identifier.ProjectCode);

            return role;
        }

        public async Task<Role> SetRoleInheritances(RoleIdentifier roleIdentifier, IList<string> inheritsFromThese)
        {
            var role = await GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }

            if (inheritsFromThese == null)
            {
                inheritsFromThese = new List<string>();
            }

            var toRemoveInheritsFromThese = role.InheritingFromTheseRoles.Select(inh => inh.InheritsFrom.Code).ToList()?.Except(inheritsFromThese, r => r).ToList();
            var toAddInheritsFromThese = inheritsFromThese.Except(role.InheritingFromTheseRoles.Select(inh => inh.InheritsFrom.Code).ToList() ?? new List<string>(), r => r);

            if (toRemoveInheritsFromThese != null && toRemoveInheritsFromThese.Any())
            {
                var ids = toRemoveInheritsFromThese.ToList();
                foreach (var gid in ids)
                {
                    var id = role.InheritingFromTheseRoles.Where(inh => inh.InheritsFrom.Code == gid).Select(inh => inh.Id);
                    var gs = ctx.RoleInheritances.Where(ri => id.Any(i => ri.Id == i));
                    if (gs != null && gs.Any())
                    {
                        ctx.RoleInheritances.RemoveRange(gs);
                    }
                }
            }

            if (toAddInheritsFromThese != null && toAddInheritsFromThese.Any())
            {
                if (role.InheritingFromTheseRoles == null)
                {
                    role.InheritingFromTheseRoles = new List<RoleInheritance>();
                }
                foreach (var id in toAddInheritsFromThese.ToList().Distinct())
                {
                    var getIdentifier = new RoleIdentifier(id, roleIdentifier.GetTenantIdentifier());
                    var inheritingRole = await GetAsync(getIdentifier);
                    if (inheritingRole == null)
                    {
                        _logger.LogError("Role to inherit from not found. Won't be added: {0}", getIdentifier.ToString());
                        continue;
                    }

                    var roleInheritance = new RoleInheritance();
                    roleInheritance.InheritsFromId = inheritingRole.Id;
                    roleInheritance.RoleId = role.Id;
                    await ctx.RoleInheritances.AddAsync(roleInheritance);

                }
            }

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(role);

            return role;
        }

        public async Task<Role> SetModules(RoleIdentifier roleIdentifier, IList<string> modules)
        {
            var role = await GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }

            #region RoleModules
            if (modules == null)
            {
                modules = new List<string>();
            }

            var toRemoveRoleModules = role.RoleModules?.Where(rm => !modules.Any(m => m.ToString().Equals(rm.Module.Code, System.StringComparison.OrdinalIgnoreCase)));
            var toAddModules = role.RoleModules == null ? modules : modules.Where(m => !role.RoleModules.Any(rm => m.ToString().Equals(rm.Module.Code, System.StringComparison.OrdinalIgnoreCase)));

            if (toRemoveRoleModules != null && toRemoveRoleModules.Any())
            {
                foreach (var id in toRemoveRoleModules.Select(m => m.Id).ToList().Distinct())
                {
                    var roleModule = await ctx.RoleModules.SingleOrDefaultAsync(rm => rm.Id == id);
                    if (roleModule != null)
                    {
                        ctx.RoleModules.Remove(roleModule);
                    }
                }
            }

            if (toAddModules != null && toAddModules.Any())
            {
                foreach (var m in toAddModules.ToList().Distinct())
                {
                    ModuleIdentifier moduleIdentifier = new ModuleIdentifier(m, roleIdentifier.ProjectCode);
                    Module module = await _ModuleDataProvider.GetAsync(moduleIdentifier);
                    if (module == null)
                    {
                        _logger.LogError("Module not found for module code {0}. Couldn't be added to {1}", m.ToString(), roleIdentifier.ToString());
                        continue;
                    }

                    RoleModule roleModule = new RoleModule();
                    roleModule.ModuleId = module.Id;
                    roleModule.RoleId = role.Id;
                    await ctx.RoleModules.AddAsync(roleModule);
                }
            }
            #endregion

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(role);

            return role;
        }


        public async Task<Role> CreateOrUpdateAsync(RoleUpdateViewModel model)
        {
            Role role;
            bool exists = await ExistsAsync(model.Identifier);
            if (!exists)
            {
                role = await CreateAsync(new RoleCreateModel()
                {
                    Description = model.Description,
                    InheritedModuleCodes = model.InheritedModuleCodes,
                    InheritsFromRoles = model.InheritsFromRoles,
                    ModuleCodes = model.ModuleCodes,
                    TenantIdentifier = new TenantIdentifier(model.Identifier.TenantCode, model.Identifier.ProjectCode),
                    RoleCode = model.NewRoleCode ?? model.Identifier.RoleCode,
                    UserGroups = model.UserGroups
                });
            }
            else
            {
                role = await GetAsync(await UpdateAsync(model));
            }
            return role;
        }

        public async Task<Role> CreateAsync(RoleCreateModel model)
        {
            var newRole = await GetSimpleAsync(model.GetRoleIdentifier());
            if (newRole != null)
            {
                throw new IdPException(ErrorCode.ROLE_ALREADY_EXISTS, "Couldn't create role.");
            }
            var tenant = await _TenantDataProvider.GetSimpleAsync(model.TenantIdentifier);
            if (tenant == null)
            {
                throw new IdPException(ErrorCode.TENANT_NOT_FOUND, "Couldn't create role.");
            }

            newRole = new Role();

            newRole.Code = model.RoleCode;
            newRole.Description = model.Description;
            newRole.TenantId = tenant.Id;

            await ctx.Roles.AddAsync(newRole);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(newRole);

            return newRole;
        }

        public async Task<RoleIdentifier> UpdateAsync(RoleUpdateViewModel model)
        {
            var role = await GetSimpleAsync(model.Identifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, "Couldn't find user role.");
            }
            var tenant = await _TenantDataProvider.GetSimpleAsync(model.Identifier.GetTenantIdentifier());
            if (tenant == null)
            {
                throw new IdPException(ErrorCode.TENANT_NOT_FOUND, "Couldn't create role.");
            }
            // we update the code ONLY IF a new one is provided
            role.Code = model.NewRoleCode ?? model.Identifier.RoleCode;
            role.Description = model.Description;
            role.TenantId = tenant.Id;

            ctx.Roles.Update(role);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(role);

            return await GetIdentifier(role);
        }
        private async Task<RoleIdentifier> GetIdentifier(Role entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        public async Task<int> DeleteAsync(RoleIdentifier roleIdentifier)
        {
            var entity = await GetToDeleteAsync(roleIdentifier);
            if (entity == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, "Couldn't delete role.");
            }

            if (entity.InheritingFromTheseRoles != null)
            {
                ctx.RoleInheritances.RemoveRange(entity.InheritingFromTheseRoles);
            }
            if (entity.RolesWhichInheritFromCurrent != null)
            {
                ctx.RoleInheritances.RemoveRange(entity.RolesWhichInheritFromCurrent);
            }
            if (entity.RoleModules != null)
            {
                ctx.RoleModules.RemoveRange(entity.RoleModules);
            }
            if (entity.UserGroupsRoles != null)
            {
                ctx.UserGroupRoles.RemoveRange(entity.UserGroupsRoles);
            }

            ctx.Roles.Remove(entity);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<Role> AddUserGroup(RoleIdentifier roleIdentifier, string userGroupCode)
        {
            UserGroupIdentifier userGroupIdentifier = new UserGroupIdentifier(userGroupCode, roleIdentifier.GetTenantIdentifier());
            var userGroup = await _UserGroupDataProvider.GetSimpleAsync(userGroupIdentifier);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifier.ToString());
            }
            var role = await GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }

            if (role.UserGroupsRoles.Any(ugr => ugr.UserGroup.Code == userGroupIdentifier.UserGroupCode))
            {
                _logger.LogWarning("Group already in role.");
                return role;
            }

            // add user
            UserGroupRole newUSerGoupRole = new UserGroupRole();
            newUSerGoupRole.RoleId = role.Id;
            newUSerGoupRole.UserGroupId = userGroup.Id;
            await ctx.UserGroupRoles.AddAsync(newUSerGoupRole);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(newUSerGoupRole);

            return role;
        }

        public async Task<Role> RemoveUserGroup(RoleIdentifier roleIdentifier, string groupCode)
        {
            var userGroupIdentifer = new UserGroupIdentifier(groupCode, roleIdentifier.GetTenantIdentifier());
            var userGroup = await _UserGroupDataProvider.GetSimpleAsync(userGroupIdentifer);
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND, userGroupIdentifer.ToString());
            }
            var role = await GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }
            var userGroupRoles = role.UserGroupsRoles.Where(aug => aug.UserGroupId == userGroup.Id);

            if (!userGroupRoles.Any())
            {
                _logger.LogWarning("User not in user role.");
                return role;
            }

            // remove from groups
            ctx.UserGroupRoles.RemoveRange(userGroupRoles);

            await ctx.SaveChangesAsync();

            return role;
        }

        public async Task<Role> SetUserGroups(RoleIdentifier roleIdentifier, IList<string> userGroupCodes)
        {
            var role = await GetAsync(roleIdentifier);
            if (role == null)
            {
                throw new IdPNotFoundException(ErrorCode.ROLE_NOT_FOUND, roleIdentifier.ToString());
            }
            if (userGroupCodes == null)
            {
                userGroupCodes = new List<string>();
            }
            var currentCodes = role.UserGroupsRoles.Select(ug => ug.UserGroup.Code);
            var toRemove = currentCodes.Except(userGroupCodes.ToList(), c => c);
            var toAdd = userGroupCodes.Except(currentCodes, c => c);

            if (toRemove.Any())
            {
                foreach (var code in toRemove)
                {
                    role = await RemoveUserGroup(roleIdentifier, code);
                }
            }

            if (toAdd.Any())
            {
                foreach (var code in toAdd)
                {
                    role = await AddUserGroup(roleIdentifier, code);
                }
            }

            return role;
        }

        public async Task<int> DeleteAllAsync(TenantIdentifier tenantIdentifier)
        {
            var tenant = await _TenantDataProvider.GetAsync(tenantIdentifier);
            if (tenant == null)
            {
                throw new IdPNotFoundException(ErrorCode.TENANT_NOT_FOUND, tenantIdentifier.ToString());
            }

            var roles = await GetListAsync(tenantIdentifier);

            int total = 0;
            if (tenant.Roles != null)
            {
                foreach (var roleCode in tenant.Roles.Select(r => r.Code).ToList())
                {
                    total += await DeleteAsync(new RoleIdentifier(roleCode, tenantIdentifier));
                }
            }

            ctx.DetachEntity(tenant);

            return total;
        }


        public async Task<bool> ExistsAsync(RoleIdentifier roleIdentifier)
        {
            return await ctx.Roles.CountAsync(au =>
                au.Code == roleIdentifier.RoleCode
                && au.Tenant.Code == roleIdentifier.TenantCode
                && au.Tenant.Project.Code == roleIdentifier.ProjectCode) > 0;
        }
    }
}
