﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IModuleDataProvider
    {
        Task<Module> GetAsync(ModuleIdentifier moduleCode);
        Task<List<Module>> GetListAsync(ProjectIdentifier projectIdentifier);
        Task<List<Module>> GetAllModulesAsync();
        Task<ModuleIdentifier> CreateOrUpdateAsync(ModuleUpdateViewModel model);
        Task<ModuleIdentifier> CreateAsync(ModuleCreateModel model);
        Task<ModuleIdentifier> UpdateAsync(ModuleUpdateViewModel model);
        Task<int> DeleteAsync(ModuleIdentifier moduleCode);
        Task<bool> ExistsAsync(ModuleIdentifier moduleIdentifier);
    }
}
