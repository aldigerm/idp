﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class ModuleData : IModuleDataProvider
    {
        protected readonly ILogger<ModuleData> _logger;
        private RemoteRepositoryDbContext ctx;
        private IProjectDataProvider _ProjectDataProvider;
        public ModuleData(
              RemoteRepositoryDbContext _ctx
            , IProjectDataProvider projectDataProvider
            , ILogger<ModuleData> logger
            )
        {
            ctx = _ctx;
            _logger = logger;
            _ProjectDataProvider = projectDataProvider;
        }

        public async Task<List<Module>> GetListAsync(ProjectIdentifier projectIdentifier)
        {
            List<Module> modules = null;

            modules = await ctx.Modules
                 .Include(m => m.Project)
                 .Where(u => (u.Project.Code == projectIdentifier.ProjectCode || string.IsNullOrWhiteSpace(projectIdentifier.ProjectCode)))
                 .AsNoTracking()
                 .ToListAsync();


            return modules;
        }

        public async Task<List<Module>> GetAllModulesAsync()
        {
            List<Module> modules;

            modules = await ctx.Modules
            .Include(m => m.Project)
            .Include(u => u.RoleModules)
            .AsNoTracking()
            .ToListAsync();

            return modules;
        }

        public async Task<Module> GetSimpleAsync(ModuleIdentifier moduleIdentifier)
        {
            if (moduleIdentifier == null)
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Cannot get module as ModuleCode is null or empty");

            Module module;
            module = await ctx.Modules
            .AsNoTracking()
            .SingleOrDefaultAsync(m => m.Code == moduleIdentifier.ModuleCode && m.Project.Code == moduleIdentifier.ProjectCode);

            if (module == null)
            {
                throw new IdPNotFoundException(ErrorCode.MODULE_NOT_FOUND, moduleIdentifier.ToString());
            }

            return module;
        }

        public async Task<Module> GetAsync(ModuleIdentifier moduleIdentifier)
        {
            if (moduleIdentifier == null)
                throw new IdPException(ErrorCode.GENERIC_ERROR, "Cannot get module as ModuleCode is null or empty");

            Module module;
            module = await ctx.Modules
            .Include(m => m.Project)
            .Include(m => m.RoleModules)
            .AsNoTracking()
            .SingleOrDefaultAsync(m => m.Code == moduleIdentifier.ModuleCode && m.Project.Code == moduleIdentifier.ProjectCode);

            if (module == null)
            {
                throw new IdPNotFoundException(ErrorCode.MODULE_NOT_FOUND, moduleIdentifier.ToString());
            }

            return module;
        }

        public async Task<ModuleIdentifier> CreateOrUpdateAsync(ModuleUpdateViewModel model)
        {
            ModuleIdentifier moduleIdentifier;

            // try to get
            var exists = await ExistsAsync(model.Identifier);
            if (!exists)
            {
                // not found so create
                moduleIdentifier = await CreateAsync(new ModuleCreateModel()
                {
                    Description = model.Description,
                    ModuleCode = model.NewModuleCode ?? model.Identifier.ModuleCode,
                    ProjectIdentifier = new ProjectIdentifier(model.Identifier.ProjectCode)
                });
            }
            else
            {
                // no error thrown so update
                moduleIdentifier = await UpdateAsync(model);
            }
            return moduleIdentifier;
        }

        public async Task<ModuleIdentifier> CreateAsync(ModuleCreateModel model)
        {             
            try
            {
                await GetAsync(model.GetModuleIdentifier());
                throw new IdPException(ErrorCode.MODULE_CODE_EXISTS, "Couldn't create module.");
            }
            catch (IdPNotFoundException ex)
            {
                // we expect it to not be found
                // so we continue
            }

            var project = await _ProjectDataProvider.GetSimpleAsync(model.ProjectIdentifier);
            if (project == null)
            {
                throw new IdPException(ErrorCode.PROJECT_NOT_FOUND, "Couldn't create module. Project not found " + model.ProjectIdentifier);
            }

            Module newModule = new Module();

            newModule.Code = model.ModuleCode;
            newModule.Description = model.Description;
            newModule.ProjectId = project.Id;

            await ctx.Modules.AddAsync(newModule);
            await ctx.SaveChangesAsync();

            ctx.DetachEntity(newModule);

            return await GetIdentifier(newModule);
        }

        public async Task<ModuleIdentifier> UpdateAsync(ModuleUpdateViewModel model)
        {
            var module = await GetSimpleAsync(model.Identifier);
            if (module == null)
            {
                throw new IdPNotFoundException(ErrorCode.MODULE_NOT_FOUND, "Couldn't update module.");
            }

            // we update the code ONLY IF a new one is provided
            module.Code = model.NewModuleCode ?? model.Identifier.ModuleCode;
            module.Description = model.Description;

            ctx.Modules.Update(module);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(module);

            return await GetIdentifier(module);
        }

        private async Task<ModuleIdentifier> GetIdentifier(Module entity)
        {
            return (await GetAsync(entity.Id)).GetIdentifier();
        }

        private async Task<Module> GetAsync(int id)
        {
            return await ctx.Modules
                .Include(m => m.Project)
                .Include(m => m.RoleModules)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.Id == id);
        }

        public async Task<int> DeleteAsync(ModuleIdentifier moduleIdentifier)
        {
            var module = await GetAsync(moduleIdentifier);
            if (module == null)
            {
                throw new IdPNotFoundException(ErrorCode.MODULE_NOT_FOUND, "Couldn't delete module.");
            }

            ctx.Modules.Remove(module);

            await ctx.SaveChangesAsync();

            return 1;
        }

        public async Task<bool> ExistsAsync(ModuleIdentifier moduleIdentifier)
        {
            return await ctx.Modules.CountAsync(au =>
                au.Code == moduleIdentifier.ModuleCode
                && au.Project.Code == moduleIdentifier.ProjectCode) > 0;
        }
    }
}
