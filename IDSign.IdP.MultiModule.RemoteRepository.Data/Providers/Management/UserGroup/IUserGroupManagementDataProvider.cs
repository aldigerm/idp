﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers
{
    public interface IUserGroupManagementDataProvider
    {
        Task<UserGroupManagementIdentifier> CreateAsync(UserGroupManagementIdentifier identifier);
        Task<UserToUserGroupManagement> GetAsync(UserGroupManagementIdentifier userGroupManagementIdentifier);
        Task<IList<UserToUserGroupManagement>> GetListAsync(UserIdentifier userIdentifier);
        Task<int> DeleteAsync(UserGroupManagementIdentifier userGroupManagementIdentifier);
    }
}
