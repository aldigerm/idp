﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Data.Providers.EF
{
    public class UserGroupManagementData : IUserGroupManagementDataProvider
    {
        private IUserDataProvider _UserDataProvider;
        private IUserGroupDataProvider _UserGroupDataProvider;
        private RemoteRepositoryDbContext ctx;
        private ILogger<UserData> _logger;
        public UserGroupManagementData(
             RemoteRepositoryDbContext ctx
            , ILogger<UserData> logger
            , IUserDataProvider userDataProvider
            , IUserGroupDataProvider userGroupDataProvider
            )
        {
            _logger = logger;
            this.ctx = ctx;
            _UserDataProvider = userDataProvider;
            _UserGroupDataProvider = userGroupDataProvider;
        }

        public async Task<UserGroupManagementIdentifier> CreateAsync(UserGroupManagementIdentifier identifier)
        {
            AppUser user = await _UserDataProvider.GetAsync(identifier.GetUserIdentifier());
            if (user == null)
            {
                throw new IdPNotFoundException(ErrorCode.USER_NOT_FOUND);
            }

            UserGroup userGroup = await _UserGroupDataProvider.GetSimpleAsync(identifier.GetUserGroupIdentifier());
            if (userGroup == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_NOT_FOUND);
            }
            UserToUserGroupManagement userManagement = new UserToUserGroupManagement();

            userManagement.UserTenantId = user.Id;
            userManagement.UserGroupId = userGroup.Id;

            await ctx.UserToUserGroupManagement.AddAsync(userManagement);

            await ctx.SaveChangesAsync();

            ctx.DetachEntity(userManagement);

            return await GetIdentifier(userManagement);
        }

        private async Task<UserToUserGroupManagement> GetAsync(int id)
        {
            UserToUserGroupManagement userToUserGroupManagement = await ctx.UserToUserGroupManagement
                    .Include(u => u.UserTenant)
                        .ThenInclude(ut => ut.User)
                    .Include(u => u.UserTenant)
                        .ThenInclude(ut => ut.Tenant)
                            .ThenInclude(t => t.Project)
                .Include(um => um.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .AsNoTracking()
                .SingleOrDefaultAsync(au => au.Id == id);

            if (userToUserGroupManagement == null)
            {
                _logger.LogWarning("UserToUserGroupManagement not found {0}", id);
            }
            return userToUserGroupManagement;
        }

        private async Task<UserToUserGroupManagement> GetToDeleteAsync(UserGroupManagementIdentifier userGroupManagementIdentifier)
        {
            UserToUserGroupManagement userToUserGroupManagement = await ctx.UserToUserGroupManagement                
                .AsNoTracking()
                .SingleOrDefaultAsync(au =>
                au.UserTenant.User.TenantUsername == userGroupManagementIdentifier.Username
                && au.UserGroup.Code == userGroupManagementIdentifier.UserGroupCode
                && au.UserGroup.Tenant.Code == userGroupManagementIdentifier.TenantCode
                && au.UserGroup.Tenant.Project.Code == userGroupManagementIdentifier.ProjectCode);

            if (userToUserGroupManagement == null)
            {
                _logger.LogWarning("UserToUserGroupManagement not found {0}", userGroupManagementIdentifier);
            }
            return userToUserGroupManagement;
        }

        public async Task<UserToUserGroupManagement> GetAsync(UserGroupManagementIdentifier userGroupManagementIdentifier)
        {
            if (userGroupManagementIdentifier == null)
                return null;

            UserToUserGroupManagement userToUserGroupManagement = await ctx.UserToUserGroupManagement
                    .Include(u => u.UserTenant)
                        .ThenInclude(ut => ut.User)
                    .Include(u => u.UserTenant)
                        .ThenInclude(ut => ut.Tenant)
                            .ThenInclude(t => t.Project)
                .Include(um => um.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .AsNoTracking()
                .SingleOrDefaultAsync(au => 
                au.UserTenant.User.TenantUsername == userGroupManagementIdentifier.Username
                && au.UserGroup.Code == userGroupManagementIdentifier.UserGroupCode
                && au.UserGroup.Tenant.Code == userGroupManagementIdentifier.TenantCode
                && au.UserGroup.Tenant.Project.Code == userGroupManagementIdentifier.ProjectCode);

            if (userToUserGroupManagement == null)
            {
                _logger.LogWarning("User management not found {0}", userGroupManagementIdentifier);
            }
            return userToUserGroupManagement;
        }
        public async Task<IList<UserToUserGroupManagement>> GetListAsync(UserIdentifier userIdentifier)
        {
            return await ctx.UserToUserGroupManagement
                .Include(u => u.UserTenant)
                    .ThenInclude(ut => ut.User)
                .Include(u => u.UserTenant)
                    .ThenInclude(ut => ut.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(um => um.UserGroup)
                    .ThenInclude(u => u.Tenant)
                        .ThenInclude(t => t.Project)
                .Include(um => um.UserGroup)
                    .ThenInclude(ug => ug.ApplicationUserGroups)
                        .ThenInclude(aug => aug.UserTenant)
                            .ThenInclude(ut => ut.User)
                .AsNoTracking()
                .Where(au =>
                au.UserTenant.User.TenantUsername == userIdentifier.Username
                && au.UserGroup.Tenant.Code == userIdentifier.TenantCode
                && au.UserGroup.Tenant.Project.Code == userIdentifier.ProjectCode)
                .ToListAsync();
        }

        private async Task<UserGroupManagementIdentifier> GetIdentifier(UserToUserGroupManagement userToUserGroupManagement)
        {
            return (await GetAsync(userToUserGroupManagement.Id)).GetIdentifier();
        }
        
        public async Task<int> DeleteAsync(UserGroupManagementIdentifier userGroupManagementIdentifier)
        {
            var userToUserGroupManagement = await GetToDeleteAsync(userGroupManagementIdentifier);
            if (userToUserGroupManagement == null)
            {
                throw new IdPNotFoundException(ErrorCode.USERGROUP_MANAGEMENT_NOT_FOUND, userGroupManagementIdentifier.ToString());
            }

            ctx.UserToUserGroupManagement.Remove(userToUserGroupManagement);

            await ctx.SaveChangesAsync();

            return 1;
        }

    }
}
