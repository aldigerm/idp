﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class Tenant
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int ProjectId { get; set; }
        public bool Enabled { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }
        public virtual ICollection<UserTenant> UserTenants { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<TenantClaim> TenantClaims { get; set; }
        public virtual ICollection<PasswordPolicy> PasswordPolicies { get; set; }

        public TenantIdentifier GetIdentifier()
        {
            return new TenantIdentifier(Code, Project.GetIdentifier());
        }
    }
}
