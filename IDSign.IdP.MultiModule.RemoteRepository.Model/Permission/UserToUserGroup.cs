﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserToUserGroup
    {
        public int Id { get; set; }
        public int UserTenantId { get; set; }
        public int UserGroupId { get; set; }
        public virtual UserTenant UserTenant { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
