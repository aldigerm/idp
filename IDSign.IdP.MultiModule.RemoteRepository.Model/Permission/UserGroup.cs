﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    /// <summary>
    /// The model to represent a usergroup
    /// </summary>
    public class UserGroup
    {

        #region Properties
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<GroupInheritance> UserGroupsWhichInheritFromCurrent { get; set; }

        public virtual ICollection<GroupInheritance> InheritingFromTheseUserGroups { get; set; }

        public virtual ICollection<UserGroupRole> UserGroupRoles { get; set; }
        public virtual ICollection<UserToUserGroup> ApplicationUserGroups { get; set; }
        public virtual ICollection<UserGroupClaim> UserGroupClaims { get; set; }
        public virtual ICollection<UserToUserGroupManagement> UserGroupManagement { get; set; }
        public virtual ICollection<PasswordPolicyUserGroup> PasswordPolicyUserGroup { get; set; }


        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public UserGroup()
        {

        }
        #endregion


        public UserGroupIdentifier GetIdentifier()
        {
            return new UserGroupIdentifier(Code, Tenant.GetIdentifier());
        }
    }
}
