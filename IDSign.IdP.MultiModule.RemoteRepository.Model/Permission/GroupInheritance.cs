﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class GroupInheritance
    {
        public int Id { get; set; }
        public int UserGroupId { get; set; }
        public int InheritsFromId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual UserGroup InheritsFrom { get; set; }
    }
}
