﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class Role
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public int TenantId { get; set; }
        /// <summary>
        /// Owner company of this role
        /// </summary>
        public virtual Tenant Tenant { get; set; }

        /// <summary>
        /// Parent role if there is any
        /// </summary>
        public virtual List<RoleInheritance> InheritingFromTheseRoles { get; set; }

        /// <summary>
        /// List of child roles
        /// </summary>
        public virtual ICollection<RoleInheritance> RolesWhichInheritFromCurrent { get; set; }

        /// <summary>
        /// All the user groups which are assigned this role
        /// </summary>
        public virtual ICollection<UserGroupRole> UserGroupsRoles { get; set; }

        /// <summary>
        /// Collection of RoleModules
        /// </summary>
        public virtual ICollection<RoleModule> RoleModules { get; set; }

        /// <summary>
        /// Collection of Role Claims
        /// </summary>
        public virtual ICollection<RoleClaim> RoleClaims { get; set; }

        public RoleIdentifier GetIdentifier()
        {
            return new RoleIdentifier(Code, Tenant.GetIdentifier());
        }
    }
}
