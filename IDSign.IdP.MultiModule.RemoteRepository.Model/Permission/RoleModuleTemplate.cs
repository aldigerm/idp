﻿namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class RoleModuleTemplate
    {
        public virtual string RoleName { get; set; }
        public virtual string ModuleCode { get; set; }
    }
}
