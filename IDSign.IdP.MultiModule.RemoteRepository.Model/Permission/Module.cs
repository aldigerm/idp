﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class Module
    {

        /// <summary>
        /// Unique Identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Description of Module
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Module code
        /// </summary>
        public string Code { get; set; }        

        /// <summary>
        /// list of role modules
        /// </summary>
        public virtual ICollection<RoleModule> RoleModules { get; set; }


        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public ModuleIdentifier GetIdentifier()
        {
            return new ModuleIdentifier(Code, Project.Code);
        }

        public override string ToString()
        {
            return Code;
        }
    }
}
