﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class RoleInheritance
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int InheritsFromId { get; set; }
        public virtual Role Role { get; set; }
        public virtual Role InheritsFrom { get; set; }
    }
}
