﻿using System;
using System.Runtime.Serialization;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserDetails
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset? DateOfBirth { get; set; }
        public string Occupation { get; set; }
        public string CustomAttributes { get; set; }
        public string CityOfBirth { get; set; }
        public string NINS { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public int? ProfileImageId { get; set; }
        public UserProfileImage ProfileImage { get; set; }
        public virtual AppUser ApplicationUser { get; set; }
    }
}
