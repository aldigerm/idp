﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class PasswordPolicyType
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<PasswordPolicy> PasswordPolicies { get; set; }

        public PasswordPolicyTypeIdentifier GetIdentifier()
        {
            return new PasswordPolicyTypeIdentifier(Code);
        }
    }
}
