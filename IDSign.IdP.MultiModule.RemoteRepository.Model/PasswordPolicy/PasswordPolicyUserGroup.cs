﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class PasswordPolicyUserGroup
    {
        public int Id { get; set; }
        public int UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public int PasswordPolicyId { get; set; }
        public virtual PasswordPolicy PasswordPolicy { get; set; }

        public PasswordPolicyUserGroupIdentifier GetIdentifier()
        {
            return new PasswordPolicyUserGroupIdentifier(PasswordPolicy.GetIdentifier(), UserGroup.GetIdentifier());
        }
    }
}
