﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class PasswordPolicy : PasswordPolicyDataOwner
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Data { get; set; }
        public string DefaultErrorMessage { get; set; }
        public string Description { get; set; }
        public int PasswordPolicyTypeId { get; set; }
        public virtual PasswordPolicyType PasswordPolicyType { get; set; }
        public int TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<PasswordPolicyUserGroup> PasswordPolicyUserGroup { get; set; }


        public PasswordPolicyIdentifier GetIdentifier()
        {
            return new PasswordPolicyIdentifier(Code, Tenant.GetIdentifier());
        }
    }
}
