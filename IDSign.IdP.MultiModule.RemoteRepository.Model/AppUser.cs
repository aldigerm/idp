﻿namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    using IDSign.IdP.Model.RemoteRepositoryMultiModule;
    using IDSign.IdP.MultiModule.RemoteRepository.Model.AspNet;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;

    public class AppUser : IdentityUser
    {
        public AppUser()
        {
            ObjectIdentifier = Guid.NewGuid().ToString();
        }        
        /// <summary>
        ///     Object Identifier
        /// </summary>
        public string ObjectIdentifier { get; set; }
        public string TenantUsername { get; set; }
        public int? UserDetailsId { get; set; }
        public bool IsTemporaryUser { get; set; }
        public virtual ICollection<UserTenant> UserTenants { get; set; }
        public virtual UserDetails UserDetails { get; set; }
        public virtual IList<UserPreviousPassword> PreviousPasswords { get; set; }
        [NotMapped]
        public IList<string> AllowedModulesCodes { get; set; }
        [NotMapped]
        public IList<BasicClaim> BasicClaims { get; set; }
        [NotMapped]
        public IList<string> Roles { get; set; }
        [NotMapped]
        public string TenantCode { get; set; }
        [NotMapped]
        public string ProjectCode { get; set; }
        [NotMapped]
        public IList<PasswordPolicyUserGroup> PasswordPolicies { get; set; }
        [NotMapped]
        public IList<UserGroup> InferredUserGroups { get; set; }

        public bool HasAllowedModule(ModuleCode moduleCode)
        {

            return this.AllowedModulesCodes != null && this.AllowedModulesCodes.Contains(moduleCode.ToString());
        }

        public UserIdentifier GetIdentifier(TenantIdentifier tenantIdentifier)
        {
            return new UserIdentifier(ObjectIdentifier, TenantUsername, tenantIdentifier);
        }

    }
    
    public class ApplicationUserPrincipal : GenericPrincipal, IPrincipal
    {
        public ApplicationUserPrincipal(IIdentity identity) : base(identity, new string[0])
        { }

        public AppUser ApplicationUser { get; set; }
        public string ConnectionId { get; set; }
        public string ProjectCode
        {
            get
            {
                return ApplicationUser?.ProjectCode;
            }
        }
        public string TenantCode
        {
            get
            {
                return ApplicationUser?.TenantCode;
            }
        }
        public bool HasAllowedModule(ModuleCode moduleCode)
        {
            return ApplicationUser != null && ApplicationUser.HasAllowedModule(moduleCode);
        }

        public bool HasAnyAllowedModules(params ModuleCode[] moduleCode)
        {
            return ApplicationUser != null && moduleCode.Any(mc => ApplicationUser.HasAllowedModule(mc));
        }
    }


    public class UserPreviousPassword
    {
        public UserPreviousPassword()
        {
            CreateDate = DateTimeOffset.Now;
        }
        public int Id { get; set; }
        public string PasswordHash { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public int UserId { get; set; }

        public virtual AppUser User { get; set; }

    }
}