﻿using System;
using System.Runtime.Serialization;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserProfileImage
    {
        public int Id { get; set; }
        public byte[] Data { get; set; }
        public UserDetails UserDetails { get; set; }
    }
}
