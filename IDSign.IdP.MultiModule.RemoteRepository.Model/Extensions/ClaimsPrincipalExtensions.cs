﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string ProjectCode(this ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal == null)
                return null;

            return claimsPrincipal.FindFirstValue(ClaimName.Project);
        }

        public static string UserGuid(this ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal == null)
                return null;

            return claimsPrincipal.FindFirstValue(ClaimName.UserGuid);
        }

        public static string TenantCode(this ClaimsPrincipal claimsPrincipal)
        {

            if (claimsPrincipal == null)
                return null;

            return claimsPrincipal.FindFirstValue(ClaimName.Tenant);
        }
        public static string Username(this ClaimsPrincipal claimsPrincipal)
        {

            if (claimsPrincipal == null)
                return null;

            return claimsPrincipal.FindFirstValue("sub") ?? claimsPrincipal.FindFirstValue(ClaimTypes.Name);
        }

        public static bool HasAdminScope(this ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal == null)
                return false;

            return claimsPrincipal.HasClaim(cl => Settings.AdminClaimTypes?.Any(t => t == cl.Type) == true && cl.Value == Settings.AdminClaimValue);
        }
    }
}
