﻿using IDSign.IdP.Core;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions
{
    public static class PasswordPolicyExtensions
    {
        public static string GetRegex(this PasswordPolicyDataOwner passwordPolicyUserGroup)
        {
            var regexModel = HelpersService.DeserializeObject<PasswordPolicyRegexModel>(passwordPolicyUserGroup.Data);
            return regexModel?.Regex;
        }

        public static int? GetLength(this PasswordPolicyDataOwner passwordPolicyUserGroup)
        {
            var regexModel = HelpersService.DeserializeObject<PasswordPolicyLengthModel>(passwordPolicyUserGroup.Data);
            return regexModel?.Length;
        }

        public static int? GetHistoryLimit(this PasswordPolicyDataOwner passwordPolicyUserGroup)
        {
            var regexModel = HelpersService.DeserializeObject<PasswordPolicyHistoryModel>(passwordPolicyUserGroup.Data);
            return regexModel?.Limit;
        }
    }
}
