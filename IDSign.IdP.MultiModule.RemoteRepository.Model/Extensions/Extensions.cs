﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public static class Generic
    {
        public static IEnumerable<T> Except<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other,
                                                                            Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where ReferenceEquals(null, temp) || temp.Equals(default(T))
                   select item;

        }
        public static IEnumerable<T> Common<T, TKey>(this IEnumerable<T> items, IEnumerable<T> other,
                                                                            Func<T, TKey> getKey)
        {
            return from item in items
                   join otherItem in other on getKey(item)
                   equals getKey(otherItem) into tempItems
                   from temp in tempItems.DefaultIfEmpty()
                   where !(ReferenceEquals(null, temp) || temp.Equals(default(T)))
                   select item;

        }

        public static string CleanBase64(this string base64WithDataTag)
        {
            return Regex.Replace(base64WithDataTag, "^data:image\\/[a-zA-Z]+;base64,", string.Empty);
        }
    }
    public static class Selectors
    {
        public static bool EqualsUser(this AppUser user, UserIdentifier userIdentifier)
        {
            return user != null && userIdentifier.Username != null && userIdentifier.TenantCode != null && userIdentifier.ProjectCode != null && user.TenantUsername == userIdentifier.Username && user.UserTenants.Select(ut => ut.Tenant).Any(t => t.Code == userIdentifier.TenantCode && t.Project.Code == userIdentifier.ProjectCode);
        }
        public static bool EqualsTenant(this Tenant tenant, TenantIdentifier tenantIdentifier)
        {
            return tenant != null && tenantIdentifier.TenantCode != null && tenantIdentifier.ProjectCode != null && tenant.Code == tenantIdentifier.TenantCode && tenant.Project.Code == tenantIdentifier.ProjectCode;
        }
        public static bool EqualsProject(this Project project, ProjectIdentifier projectIdentifier)
        {
            return project != null && projectIdentifier.ProjectCode != null && project.Code == projectIdentifier.ProjectCode;
        }

        public static IList<AppUser> FilterTemporaryUsers(this IEnumerable<AppUser> appUsers, bool includeTemporaryUsers)
        {
            return appUsers.Where(u => includeTemporaryUsers || !u.IsTemporaryUser).ToList();
        }
        public static IList<UserViewModel> FilterTemporaryUsers(this IEnumerable<UserViewModel> userViewModels, bool includeTemporaryUsers)
        {
            return userViewModels.Where(u => includeTemporaryUsers || !u.IsTemporaryUser).ToList();
        }

    }

    public static class EFExtensions
    {
        public static IQueryable<AppUser> FilterTemporaryUsersEFQuery(this DbSet<AppUser> dbSet, bool includeTemporaryUsers)
        {
            return dbSet.Where(u => includeTemporaryUsers || !u.IsTemporaryUser);
        }
        public static IQueryable<Tenant> TenantFilter(this DbSet<Tenant> dbSet)
        {
            return dbSet
                    .Include(t => t.Project)
                    .Include(t => t.TenantClaims)
                        .ThenInclude(tc => tc.TenantClaimType)
                    .Include(t => t.UserGroups)
                    .Include(t => t.Roles)
                    .Include(t => t.PasswordPolicies)
                    .TenantIncludeUserTenantsFilter()
                    .AsNoTracking();
        }

        public static IQueryable<Tenant> TenantIncludeUserTenantsFilter(this IQueryable<Tenant> dbSet)
        {
            return dbSet
                    .Include(u => u.UserTenants)
                        .ThenInclude(ut => ut.User)
                    .AsNoTracking();
        }

        public static IQueryable<Tenant> TenantFilter(this DbSet<Tenant> dbSet, TenantIdentifier tenantIdentifier)
        {
            return dbSet
                    .TenantFilter()
                    .Where(c => c.Code == tenantIdentifier.TenantCode && c.Project.Code == tenantIdentifier.ProjectCode)
                    .AsNoTracking();
        }

        public static IQueryable<Tenant> TenantFilter(this DbSet<Tenant> dbSet, ProjectIdentifier tenantIdentifier)
        {
            return dbSet
                    .TenantFilter()
                    .Where(c => c.Project.Code == tenantIdentifier.ProjectCode)
                    .AsNoTracking();
        }

        public static Tenant UserTenantFilter(this DbSet<UserTenant> dbSet, Tenant tenant, bool includeTemporaryUsers)
        {
            var userTenants = dbSet
                        .Include(ut => ut.User)
                            .ThenInclude(u => u.UserDetails)
                        .Where(ut => includeTemporaryUsers || ut.User.IsTemporaryUser == false)
                        .Where(ut => ut.TenantId == tenant.Id)
                        .ToList();

            // assign the list to the tenant
            // and bind the objects logically
            userTenants.ForEach(ut => ut.Tenant = tenant);
            tenant.UserTenants = userTenants;
            return tenant;
        }
    }

    public static class Modules
    {
        public static bool HasModule(this IList<Module> modules, string code)
        {
            if (modules == null)
                return false;

            return modules.Any(m => m.Code == code.ToString());
        }

        public static bool HasModule(this IList<Module> modules, ModuleCode code)
        {
            if (modules == null)
                return false;

            return modules.Any(m => m.Code == code.ToString());
        }
        public static bool HasAnyAllowedModules(this IList<Module> modules, params ModuleCode[] code)
        {
            if (modules == null)
                return false;

            return modules.All(m => code.Any(c => c.ToString() == m.Code));
        }

        public static bool IsSuperUser(this IEnumerable<string> list)
        {
            if (list == null)
                return false;

            return list.Any(mc => mc == SupportConstants.SupportModuleCode);
        }

        public static bool IsAdmin(this IEnumerable<string> list)
        {
            if (list == null)
                return false;

            var adminModules = new ModuleCode[] { ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN };

            return list.Any(mc => adminModules.Any(adm => adm.ToString() == mc));
        }
    }

    public static class HttpResponseExtensions
    {
        public static bool IsSuccessStatusCode(this HttpResponse response)
        {
            return ((int)response.StatusCode >= 200) && ((int)response.StatusCode <= 299);
        }
    }
    public static class EntityFramework
    {
        public static DbContext DetachEntityObject(this DbContext context, object entity)
        {
            if (context == null)
                return null;

            if (entity == null)
                return context;

            context.Entry(entity).State = EntityState.Detached;
            return context;
        }

        // context is our ObjectContext
        public static DbContext DetachEntity<T>(this DbContext context, IList<T> entityList, bool recursive = true) where T : class
        {
            return context.DetachEntity(entityList?.ToList(), recursive);
        }

        public static DbContext DetachEntity<T>(this DbContext context, List<T> entityList, bool recursive = true) where T : class
        {

            foreach (var entity in entityList)
            {
                context = context.DetachEntity(entity);
            }
            return context;
        }

        // context is our ObjectContext
        public static DbContext DetachEntity<T>(this DbContext context, T entity, bool recursive = true) where T : class
        {
            if (!recursive)
            {
                context.Entry<T>(entity).State = EntityState.Detached;
            }
            else
            {
                context.DetachEntity(entity, new HashSet<object>());
            }
            return context;
        }

        private static DbContext DetachEntity<T>(this DbContext context, T entity, ISet<object> objectSet) where T : class
        {
            if (entity == null) return context;

            EntityState objectState = context.Entry<T>(entity).State;

            context.Entry<T>(entity).State = EntityState.Detached;

            // This is to prevent an infinite recursion when the child object has a navigation property
            // that points back to the parent
            if (!objectSet.Add(entity))
                return context;

            // Recursively detach navigation property
            foreach (var property in entity.GetType().GetNavigationProperties())
            {
                if (property.IsEntityCollection())
                {
                    var collection = property.GetValue(entity, null) as IEnumerable<object>;
                    if (collection == null) continue;
                    foreach (var child in collection.ToList())
                    {
                        context.DetachEntity(child, objectSet);
                    }
                }
                else
                {
                    context.DetachEntity(property.GetValue(entity, null), objectSet);
                }
            }

            // detach
            context.Entry(entity).State = EntityState.Detached;

            return context;
        }

        // Navigation properties have the EdmRelationshipNavigationPropertyAttribute attribute
        public static IEnumerable<PropertyInfo> GetNavigationProperties(this Type type)
        {
            return type.GetProperties().Where(prop => prop.PropertyType.IsClass && prop.PropertyType.Assembly.FullName == type.Assembly.FullName);
        }

        public static bool IsEntityCollection(this PropertyInfo property)
        {
            return property.PropertyType.IsGenericType &&
                   property.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>);
        }
    }


    public static class Strings
    {
        public static bool EqualsIgnoreCase(this string input, string compareTo, bool allowNullInput = false)
        {
            if (input == null && !allowNullInput)
                throw new ArgumentNullException("input", "String to compare with cannot be null");

            if (input == null)
            {
                if (compareTo == null)
                {
                    // two nulls are the same
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // input it not null, so proceed as usual
                return input.Equals(compareTo, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public static bool ContainsIgnoreCase(this string input, string toFind)
        {
            if (input == null)
                throw new ArgumentNullException("input", "String to find contained value cannot be null");

            return !string.IsNullOrEmpty(toFind) && input?.IndexOf(toFind, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static bool EqualsTrimIgnoreCase(this string input, string compareTo)
        {
            if (input == null)
            {
                // if what's being compared is null
                // then they match they're both null
                return compareTo == null;
            }
            input = input.Trim();
            compareTo = compareTo?.Trim();
            return input.Equals(compareTo, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool StartsWithIgnoreCase(this string input, string toFind)
        {
            if (input == null)
                throw new ArgumentNullException("input", "String to find contained value cannot be null");

            return !string.IsNullOrEmpty(toFind) && input?.IndexOf(toFind, StringComparison.OrdinalIgnoreCase) == 0;
        }

        public static bool HasValue(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return false;

            return true;
        }

        public static IEnumerable<string> GetCommon(this IEnumerable<string> _, IEnumerable<string> list)
        {
            return _.Where(s => list.Any(sl => sl == s));
        }

        public static IEnumerable<string> GetUnCommon(this IEnumerable<string> _, IEnumerable<string> list)
        {
            return _.Where(s => list.Any(sl => sl != s));
        }
    }

}
