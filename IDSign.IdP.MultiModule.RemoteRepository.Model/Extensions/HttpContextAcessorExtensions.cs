﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions
{
    public static class HttpContextAccessorExtensions
    {
        public static List<string> AllowedModules(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor?.HttpContext == null)
                return new List<string>();

            return httpContextAccessor.HttpContext.AllowedModules();            
        }

        public static bool HasAllowedModule(this IHttpContextAccessor httpContextAccessor, string code)
        {
            if (httpContextAccessor == null)
                return false;

            return httpContextAccessor.AllowedModules().Any(m => m == code);
        }

        public static bool HasAllowedModule(this IHttpContextAccessor httpContextAccessor, ModuleCode code)
        {
            return httpContextAccessor.HasAllowedModule(code.ToString());
        }

        public static bool HasAnyAllowedModules(this IHttpContextAccessor httpContextAccessor, params string[] moduleCodes)
        {
            if (httpContextAccessor == null)
                return false;

            return moduleCodes.Any(mc => httpContextAccessor.HasAllowedModule(mc));
        }

        public static bool HasAnyAllowedModules(this IHttpContextAccessor httpContextAccessor, params ModuleCode[] moduleCodes)
        {
            if (httpContextAccessor == null)
                return false;

            return moduleCodes.Any(mc => httpContextAccessor.HasAllowedModule(mc));
        }


        public static string ProjectCode(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor == null)
                return null;

            return httpContextAccessor.HttpContext.ProjectCode();
        }

        public static string UserGuid(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor == null)
                return null;

            return httpContextAccessor.HttpContext.UserGuid();
        }

        public static string TenantCode(this IHttpContextAccessor httpContextAccessor)
        {

            if (httpContextAccessor == null)
                return null;

            return httpContextAccessor.HttpContext.TenantCode();
        }

        public static string Username(this IHttpContextAccessor httpContextAccessor)
        {

            if (httpContextAccessor == null)
                return null;

            return httpContextAccessor.HttpContext.Username();
        }

        public static List<string> Roles(this IHttpContextAccessor httpContextAccessor)
        {

            if (httpContextAccessor?.HttpContext == null)
                return new List<string>();

            return httpContextAccessor.HttpContext.Roles();
        }

        public static string RepositoryUsername(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor == null)
                return null;

            return httpContextAccessor.HttpContext.RepositoryUsername();
        }

        public static bool IsSameAs(this IHttpContextAccessor httpContextAccessor, UserIdentifier userIdentifier)
        {

            if (httpContextAccessor == null || userIdentifier == null)
                return false;

            return httpContextAccessor.HttpContext.IsSameAs(userIdentifier);
        }

        public static UserIdentifier LoggedInUserIdentifier(this IHttpContextAccessor httpContextAccessor)
        {

            if (httpContextAccessor == null ||
                string.IsNullOrWhiteSpace(httpContextAccessor.UserGuid()) ||
                string.IsNullOrWhiteSpace(httpContextAccessor.Username()) ||
                string.IsNullOrWhiteSpace(httpContextAccessor.TenantCode()) ||
                string.IsNullOrWhiteSpace(httpContextAccessor.ProjectCode()))

                return null;

            return new UserIdentifier(httpContextAccessor.UserGuid(), httpContextAccessor.Username(), httpContextAccessor.TenantCode(), httpContextAccessor.ProjectCode());
        }
        public static bool IsUser(this IHttpContextAccessor httpContextAccessor)
        {
            return httpContextAccessor.LoggedInUserIdentifier() != null;
        }

        public static bool IsAdmin(this IHttpContextAccessor httpContextAccessor)
        {
            if(httpContextAccessor == null)
                return false;

            return httpContextAccessor.AllowedModules().IsAdmin()  || httpContextAccessor.HttpContext?.User?.HasAdminScope() == true;
        }

        public static bool IsSuperUser(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor == null)
                return false;

            return httpContextAccessor.AllowedModules().IsSuperUser();
        }

        public static ILoggingUser GetLoggingUser(this IHttpContextAccessor httpContextAccessor)
        {

            if (httpContextAccessor == null ||
                string.IsNullOrWhiteSpace(httpContextAccessor.Username()) ||
                string.IsNullOrWhiteSpace(httpContextAccessor.TenantCode()) ||
                string.IsNullOrWhiteSpace(httpContextAccessor.ProjectCode()))

                return null;

            return new LoggingUser(httpContextAccessor.Username(), httpContextAccessor.TenantCode() + ":" + httpContextAccessor.ProjectCode());
        }
    }
}
