﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions
{
    public static class HttpContexExtensions
    {
        public static List<string> AllowedModules(this HttpContext httpContext)
        {
            if (httpContext == null)
                return new List<string>();

            var list = httpContext.Items[ClaimName.ModuleList] as List<string>;

            if (list == null)
            {
                return new List<string>();
            }

            return list;
        }
        public static void SetAllowedModules(this HttpContext httpContext, List<string> modules)
        {
            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.ModuleList] = modules;            
        }

        public static bool HasAllowedModule(this HttpContext httpContext, string code)
        {
            if (httpContext == null)
                return false;

            return httpContext.AllowedModules().Any(m => m == code);
        }

        public static bool HasAllowedModule(this HttpContext httpContext, ModuleCode code)
        {
            return httpContext.HasAllowedModule(code.ToString());
        }

        public static bool HasAnyAllowedModules(this HttpContext httpContext, params string[] moduleCodes)
        {
            if (httpContext == null)
                return false;

            return moduleCodes.Any(mc => httpContext.HasAllowedModule(mc));
        }

        public static bool HasAnyAllowedModules(this HttpContext httpContext, params ModuleCode[] moduleCodes)
        {
            if (httpContext == null)
                return false;

            return moduleCodes.Any(mc => httpContext.HasAllowedModule(mc));
        }


        public static string ProjectCode(this HttpContext httpContext)
        {
            if (httpContext == null)
                return null;

            return httpContext.Items[ClaimName.Project] as string;
        }

        public static void SetProjectCode(this HttpContext httpContext, string projectCode)
        {
            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.Project] = projectCode;
        }

        public static string UserGuid(this HttpContext httpContext)
        {
            if (httpContext == null)
                return null;

            return httpContext.Items[ClaimName.UserGuid] as string;
        }
        public static void SetUserGuid(this HttpContext httpContext, string userGuid)
        {
            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.UserGuid] = userGuid;
        }

        public static string TenantCode(this HttpContext httpContext)
        {

            if (httpContext == null)
                return null;

            return httpContext.Items[ClaimName.Tenant] as string;
        }

        public static void SetTenantCode(this HttpContext httpContext, string tenantCode)
        {

            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.Tenant] = tenantCode;
        }

        public static string Username(this HttpContext httpContext)
        {

            if (httpContext == null)
                return null;

            var username = httpContext.Items["sub"] ?? httpContext.Items[ClaimTypes.Name];
            return username as string;
        }

        public static void SetUsername(this HttpContext httpContext, string username)
        {

            if (httpContext == null)
                return;

            httpContext.Items[ClaimTypes.Name]  = username;
        }

        public static List<string> Roles(this HttpContext httpContext)
        {

            if (httpContext == null)
                return new List<string>();

            var list = httpContext.Items[ClaimName.Role] as List<string>;

            if (list == null)
            {
                return new List<string>();
            }

            return list;
        }
        public static void SetRoles(this HttpContext httpContext, List<string> roles)
        {

            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.Role] = roles;
        }

        public static string RepositoryUsername(this HttpContext httpContext)
        {
            if (httpContext == null)
                return null;

            return httpContext.Items[ClaimName.RepositoryUsername] as string;
        }
        public static void SetRepositoryUsername(this HttpContext httpContext, string repositoryUsername)
        {
            if (httpContext == null)
                return;

            httpContext.Items[ClaimName.RepositoryUsername] = repositoryUsername;
        }

        public static bool IsSameAs(this HttpContext httpContext, UserIdentifier userIdentifier)
        {

            if (httpContext == null || userIdentifier == null)
                return false;

            return userIdentifier.Username == httpContext.Username()
                && userIdentifier.TenantCode == httpContext.TenantCode()
                && userIdentifier.ProjectCode == httpContext.ProjectCode();
        }
        public static UserIdentifier LoggedInUserIdentifier(this HttpContext httpContext)
        {

            if (httpContext == null ||
                string.IsNullOrWhiteSpace(httpContext.UserGuid()) ||
                string.IsNullOrWhiteSpace(httpContext.Username()) ||
                string.IsNullOrWhiteSpace(httpContext.TenantCode()) ||
                string.IsNullOrWhiteSpace(httpContext.ProjectCode()))

                return null;

            return new UserIdentifier(httpContext.UserGuid(), httpContext.Username(), httpContext.TenantCode(), httpContext.ProjectCode());
        }
        public static bool IsUser(this HttpContext httpContext)
        {
            return httpContext.LoggedInUserIdentifier() != null;
        }

        public static bool IsAdmin(this HttpContext httpContext)
        {
            if (httpContext == null)
                return false;

            return httpContext.AllowedModules().IsAdmin();
        }

        public static bool IsSuperUser(this HttpContext httpContext)
        {
            if (httpContext == null)
                return false;

            return httpContext.AllowedModules().IsSuperUser();
        }

        public static ILoggingUser GetLoggingUser(this HttpContext httpContext)
        {

            if (httpContext == null ||
                string.IsNullOrWhiteSpace(httpContext.Username()) ||
                string.IsNullOrWhiteSpace(httpContext.TenantCode()) ||
                string.IsNullOrWhiteSpace(httpContext.ProjectCode()))

                return null;

            return new LoggingUser(httpContext.Username(), httpContext.TenantCode() + ":" + httpContext.ProjectCode());
        }
    }
}
