﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserClaim
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int UserTenantId { get; set; }
        public virtual UserTenant UserTenant { get; set; }
        public int? UserClaimTypeId { get; set; }
        public virtual UserClaimType UserClaimType { get; set; }

        public UserClaimIdentifier GetIdentifier()
        {
            return new UserClaimIdentifier(Value, UserTenant.GetUserIdentifier(), UserClaimType.GetIdentifier());
        }
    }
}
