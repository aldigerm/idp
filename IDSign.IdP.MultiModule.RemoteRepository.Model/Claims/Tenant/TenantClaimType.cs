﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class TenantClaimType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<TenantClaim> TenantClaims { get; set; }
        
        public TenantClaimTypeIdentifier GetIdentifier()
        {
            return new TenantClaimTypeIdentifier(Type, Project.GetIdentifier());
        }
    }
}
