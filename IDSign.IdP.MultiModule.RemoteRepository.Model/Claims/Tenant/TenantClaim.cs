﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class TenantClaim
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int? TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
        public int? TenantClaimTypeId { get; set; }
        public virtual TenantClaimType TenantClaimType { get; set; }

        public TenantClaimIdentifier GetIdentifier()
        {
            return new TenantClaimIdentifier(Value, Tenant.GetIdentifier(), TenantClaimType.GetIdentifier());
        }
    }
}
