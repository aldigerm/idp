﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class RoleClaim
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int? RoleId { get; set; }
        public virtual Role Role { get; set; }
        public int? RoleClaimTypeId { get; set; }
        public virtual RoleClaimType RoleClaimType { get; set; }

        public RoleClaimIdentifier GetIdentifier()
        {
            return new RoleClaimIdentifier(Value, Role.GetIdentifier(), RoleClaimType.GetIdentifier());
        }
    }
}
