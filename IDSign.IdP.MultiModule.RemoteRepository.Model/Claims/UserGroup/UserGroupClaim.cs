﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserGroupClaim
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public int? UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public int? UserGroupClaimTypeId { get; set; }
        public virtual UserGroupClaimType UserGroupClaimType { get; set; }

        public UserGroupClaimIdentifier GetIdentifier()
        {
            return new UserGroupClaimIdentifier(Value, UserGroup.GetIdentifier(), UserGroupClaimType.GetIdentifier());
        }
    }
}
