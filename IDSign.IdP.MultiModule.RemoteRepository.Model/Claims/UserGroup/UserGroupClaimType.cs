﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserGroupClaimType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<UserGroupClaim> UserGroupClaims { get; set; }
        
        public UserGroupClaimTypeIdentifier GetIdentifier()
        {
            return new UserGroupClaimTypeIdentifier(Type, Project.GetIdentifier());
        }
    }
}
