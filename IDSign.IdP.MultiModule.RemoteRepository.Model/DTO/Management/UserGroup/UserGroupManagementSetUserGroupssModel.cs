﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserGroupManagementSetUserGroupssModel
    {
        [JsonProperty("identifier")]
        [Required]
        public UserIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("userGroups")]
        public IList<string> userGroupCodes { get; set; }
    }
}
