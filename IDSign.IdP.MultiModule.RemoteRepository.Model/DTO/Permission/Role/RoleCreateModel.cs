﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class RoleCreateModel
    {

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [Required]
        [JsonProperty("roleCode")]
        [StringLength(maximumLength: 128)]
        public string RoleCode { get; set; }

        [Required]
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }



        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [JsonProperty("inheritsFromRoles")]
        public IList<string> InheritsFromRoles { get; set; }

        [JsonProperty("moduleCodes")]
        public IList<string> ModuleCodes { get; set; }

        [JsonProperty("inheritedModuleCodes")]
        public IList<string> InheritedModuleCodes { get; set; }

        public RoleIdentifier GetRoleIdentifier()
        {
            return new RoleIdentifier(RoleCode, TenantIdentifier);
        }
    }
}
