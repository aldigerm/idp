﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class RoleSetUserGroupsModel
    {

        [JsonProperty("identifier")]
        [Required]
        public RoleIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }
    }
}
