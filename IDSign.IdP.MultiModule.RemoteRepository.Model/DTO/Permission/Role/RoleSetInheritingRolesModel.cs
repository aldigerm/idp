﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class RoleSetInheritingRolesModel
    {
        [JsonProperty("identifier")]
        [Required]
        public RoleIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("inheritsFrom")]
        public IList<string> InheritsFromRoles { get; set; }
    }
}
