﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserGroupCreateModel
    {

        [JsonProperty("userGroupCode")]
        [StringLength(maximumLength: 128)]
        public string UserGroupCode { get; set; }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("inheritsFromUserGroups")]
        public IList<string> InheritsFromUserGroups { get; set; }
        
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }

        [JsonProperty("usernames")]
        public IList<string> Usernames { get; set; }

        public UserGroupIdentifier GetUserGroupIdentifier()
        {
            return new UserGroupIdentifier(UserGroupCode, TenantIdentifier);
        }
    }
}
