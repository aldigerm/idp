﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserGroupSetUsersModel
    {
        [JsonProperty("identifier")]
        [Required]
        public UserGroupIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("usernames")]
        public IList<string> Usernames { get; set; }
    }
}
