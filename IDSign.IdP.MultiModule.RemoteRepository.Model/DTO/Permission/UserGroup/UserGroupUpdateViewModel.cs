﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserGroupUpdateViewModel : UserGroupViewModel
    {
        [JsonProperty("newUserGroupCode")]
        [StringLength(maximumLength: 150)]

        public string NewUserGroupCode { get; set; }
    }
}
