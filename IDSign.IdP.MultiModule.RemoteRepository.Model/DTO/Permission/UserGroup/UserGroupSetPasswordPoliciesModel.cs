﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserGroupSetPasswordPoliciesModel
    {

        [JsonProperty("identifier")]
        [Required]
        public UserGroupIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("passwordPolicies")]
        public IList<string> PasswordPolicies { get; set; }
    }
}
