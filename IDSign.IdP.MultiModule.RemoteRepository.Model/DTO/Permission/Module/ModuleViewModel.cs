﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class ModuleCreateModel
    {
        [JsonProperty("projectIdentifier")]
        [Required]
        public ProjectIdentifier ProjectIdentifier { get; set; }
        
        [JsonProperty("moduleCode")]
        public string ModuleCode { get; set; }
        
        [Required]
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }

        public ModuleIdentifier GetModuleIdentifier()
        {
            return new ModuleIdentifier(ModuleCode, ProjectIdentifier);
        }
    }
}
