﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class SetUserTenants
    {
        [JsonProperty("userGuid")]
        [Required]
        public string UserGuid { get; set; }

        [JsonProperty("tenants")]
        [Required]
        public List<TenantIdentifier> Tenants { get; set; }
    }
}
