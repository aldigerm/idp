﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserTenantUpdateViewModel : UserTenantViewModel
    {
        [JsonProperty("newProjectCode")]
        [StringLength(maximumLength: 150)]

        public string NewProjectCode { get; set; }
    }
}
