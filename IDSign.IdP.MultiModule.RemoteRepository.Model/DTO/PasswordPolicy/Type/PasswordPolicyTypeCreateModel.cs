﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class PasswordPolicyTypeCreateModel
    {

        [JsonProperty("code")]
        [Required]
        public string Code { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        public PasswordPolicyTypeIdentifier GetIdentifier()
        {
            return new PasswordPolicyTypeIdentifier(Code);
        }
    }
}
