﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class PasswordPolicyTypeUpdateModel: PasswordPolicyTypeModel
    {

        [JsonProperty("newCode")]
        [Required]
        public string NewCode { get; set; }
        
    }
}
