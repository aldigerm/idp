﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class PasswordPolicyCreateModel
    {

        [JsonProperty("passwordPolicyTypeIdentifier")]
        [Required]
        public PasswordPolicyTypeIdentifier PasswordPolicyTypeIdentifier { get; set; }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("userGroups")]
        public IList<UserGroupIdentifier> UserGroups { get; set; }

        [Required]
        [JsonProperty("passwordPolicyCode")]
        public string PasswordPolicyCode { get; set; }
        
        [JsonProperty("regex")]
        public string Regex { get; set; }
        
        [JsonProperty("length")]
        public int? Length { get; set; }

        [JsonProperty("historyLimit")]
        public int? HistoryLimit { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("defaultErrorMessage")]
        public string DefaultErrorMessage { get; set; }

        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }

        public PasswordPolicyIdentifier GetPasswordPolicyIdentifier()
        {
            return new PasswordPolicyIdentifier(PasswordPolicyCode, TenantIdentifier);
        }
    }
}
