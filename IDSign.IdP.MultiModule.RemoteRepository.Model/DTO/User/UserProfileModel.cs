﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserProfileModel
    {
        [JsonProperty("identifier")]
        public UserIdentifier Identifier { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("MiddleName")]
        public string MiddleName { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
        
        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }
        
        [JsonProperty("title")]
        public string Title { get; set; }
                                        
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTimeOffset? DateOfBirth { get; set; }

        [JsonProperty("occupation")]
        public string Occupation { get; set; }

        [JsonProperty("customAttributes")]
        public string CustomAttributes { get; set; }

        [JsonProperty("cityOfBirth")]
        public string CityOfBirth { get; set; }

        [JsonProperty("nins")]
        public string NINS { get; set; }

        [JsonProperty("uuid")]
        public string UserIdentifier { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [JsonProperty("modules")]
        public IList<string> AllowedModules { get; set; }

        [JsonProperty("roles")]
        public IList<string> Roles { get; set; }

        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [JsonProperty("partnerName")]
        public string PartnerName { get; set; }

        [JsonProperty("companyCode")]
        public string CompanyCode { get; set; }

        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        [JsonProperty("properties")]
        public List<BasicClaim> Properties { get; set; }
    }
}
