﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UserRegisterViewModel
    {
        [EmailAddress]
        [StringLength(maximumLength: 150)]
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [Required]
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [Phone]
        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }

        [JsonProperty("setPasswordOnFirstLogin")]
        public bool SetPasswordOnFirstLogin { get; set; }

        [JsonProperty("isTemporaryUser")]
        public bool IsTemporaryUser { get; set; }

        [JsonProperty("useMobileOTP")]
        public bool UseMobileOTP { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantIdentifier);
        }
    }


}