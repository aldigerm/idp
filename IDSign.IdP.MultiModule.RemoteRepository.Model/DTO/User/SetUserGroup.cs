﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class SetUserGroup
    {
        [JsonProperty("identifier")]
        [Required]
        public UserIdentifier Identifier { get; set; }

        [JsonProperty("userGroups")]
        [Required]
        public List<string> UserGroups { get; set; }
    }
}
