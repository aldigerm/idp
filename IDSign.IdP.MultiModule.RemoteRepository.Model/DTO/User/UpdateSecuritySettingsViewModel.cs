﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.DTO
{
    public class UpdateSecuritySettingsViewModel
    {
        [JsonProperty("identifier")]
        [Required]
        public UserIdentifier Identifier { get; set; }

        [JsonProperty("setPasswordOnNextLogin")]
        public bool SetPasswordOnNextLogin { get; set; }
    }
}
