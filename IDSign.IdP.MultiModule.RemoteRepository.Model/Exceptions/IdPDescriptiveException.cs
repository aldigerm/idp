﻿using IDSign.IdP.Core;
using Microsoft.AspNetCore.Http;
using System;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class IdPDescriptiveException: IdPException
    {
        public string ExceptionData { get; set; }
        public IdPDescriptiveException() : base(ErrorCode.GENERIC_ERROR, StatusCodes.Status500InternalServerError)
        { }
        public IdPDescriptiveException(ErrorCode errorCode) : base(errorCode, StatusCodes.Status500InternalServerError)
        { }
        public IdPDescriptiveException SetObject<T>(T exceptionData) 
        {
            ExceptionData = HelpersService.DescribeObject(exceptionData);
            return this;
        }
        
    }
}
