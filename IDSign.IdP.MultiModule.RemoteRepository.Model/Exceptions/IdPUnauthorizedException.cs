﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class IdPUnauthorizedException : IdPException
    {
        public IdPUnauthorizedException(ErrorCode errorCode) : base(errorCode, StatusCodes.Status401Unauthorized)
        {
        }
        public IdPUnauthorizedException(ErrorCode errorCode, string message) : base(errorCode, StatusCodes.Status401Unauthorized, message)
        {
        }
    }
}
