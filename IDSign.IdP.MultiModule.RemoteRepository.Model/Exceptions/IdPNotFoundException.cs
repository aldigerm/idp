﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class IdPNotFoundException : IdPException
    {
        public IdPNotFoundException(ErrorCode errorCode) : base(errorCode, StatusCodes.Status404NotFound)
        {
        }
        public IdPNotFoundException(ErrorCode errorCode, string message) : base(errorCode, StatusCodes.Status404NotFound, message)
        {
        }
    }
}
