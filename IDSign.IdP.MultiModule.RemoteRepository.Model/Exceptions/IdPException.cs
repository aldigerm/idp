﻿using Microsoft.AspNetCore.Http;
using System;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class IdPException : Exception
    {
        public int StatusCode { get; set; }
        public ErrorCode ErrorCode { get; set; }
        public IdPException() : this(ErrorCode.GENERIC_ERROR, StatusCodes.Status500InternalServerError)
        { }

        public IdPException(string message) : this(ErrorCode.GENERIC_ERROR, message)
        { }

        public IdPException(ErrorCode errorCode) : this(errorCode, StatusCodes.Status500InternalServerError)
        { }
        public IdPException(ErrorCode errorCode, string message) : this(errorCode, StatusCodes.Status500InternalServerError, message)
        { }

        public IdPException(ErrorCode errorCode, int statusCode) : base(errorCode.ToString())
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }
        public IdPException(ErrorCode errorCode, int statusCode, string message) : base(errorCode.ToString() + " " + message)
        {
            ErrorCode = errorCode;
            StatusCode = statusCode;
        }
    }
}
