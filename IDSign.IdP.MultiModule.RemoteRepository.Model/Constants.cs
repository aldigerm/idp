﻿using IDSign.IdP.Core.Config;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public static class ClaimName
    {
        public static readonly string Tenant = new AppSettingsConfigValue("Claims.Names.Tenant", "remote-repository-tenant");
        public static readonly string Partner = new AppSettingsConfigValue("Claims.Names.PartnerCompany", "id-onboard-partner");
        public static readonly string UserGroup = new AppSettingsConfigValue("Claims.Names.Group", "remote-repository-usergroup");
        public static readonly string Project = new AppSettingsConfigValue("Claims.Names.Project", "remote-repository-project");
        public static readonly string Module = new AppSettingsConfigValue("Claims.Names.Module", "remote-repository-module");
        public static readonly string Role = new AppSettingsConfigValue("Claims.Names.Role", "remote-repository-role");
        public static readonly string RepositoryUsername = new AppSettingsConfigValue("Claims.Names.RepositoryUsername", "remote-repository-repo-username");
        public static readonly string ConnectionId = new AppSettingsConfigValue("Claims.Names.Tenant", "connectionId");
        public static readonly string InitialisedKey = new AppSettingsConfigValue("Claims.Names.InitialisedKey", "$initialised$");
        public static readonly string UserGuid = new AppSettingsConfigValue("Claims.Names.UserGuid", "user-guid");
        public static readonly string ModuleList = new AppSettingsConfigValue("Claims.Names.ModuleList", "remote-repository-module-list");
    }

    public static class RoleCode
    {
        public static readonly string Psp = new AppSettingsConfigValue("Claims.Values.Groups.Psp", "psp");
        public static readonly string Risk = new AppSettingsConfigValue("Claims.Values.Groups.Risk", "risk");
        public static readonly string User = new AppSettingsConfigValue("Claims.Values.Groups.User", "user");
        public static readonly string InternalUser = new AppSettingsConfigValue("Claims.Values.Groups.InternalUser", "internal-user");
        public static readonly string Partner = new AppSettingsConfigValue("Claims.Values.Groups.Partner", "partner");
        public static readonly string Supervisor = new AppSettingsConfigValue("Claims.Values.Groups.Supervisor", "manager");
        public static readonly string PspSupervisor = new AppSettingsConfigValue("Claims.Values.Groups.PspSupervisor", "psp-manager");
        public static readonly string RiskSupervisor = new AppSettingsConfigValue("Claims.Values.Groups.RiskSupervisor", "risk-manager");
    }

    public static class Settings
    {
        public static readonly bool Debug = new AppSettingsConfigValue("Debug", "false");

        public static readonly string CorsOrigins = new AppSettingsConfigValue("cors.origins", "*");
        public static readonly string[] AdminClaimTypes = new AppSettingsConfigValue("Settings.adminClaim.type", "aud,scope")?.ToString().Split(',');
        public static readonly string AdminClaimValue = new AppSettingsConfigValue("Settings.adminClaim.value", "remote-user-repository-admin");
        public static readonly int ForcedNewPasswordExpiryMinutes = new AppSettingsConfigValue("UserSessions.DefaultExpiryTimeInMinutes", "1440");
        
        #region OTP

        public static readonly int OTPExpiryMinutes = new AppSettingsConfigValue("UserSessions.DefaultExpiryTimeInMinutes", "5");
        public static readonly int OTPTimeFrameInMinutes = new AppSettingsConfigValue("UserSessions.OTPTimeFrameInMinutes", "1");
        public static readonly int OTPMaxCountInTimeFrame = new AppSettingsConfigValue("UserSessions.OTPMaxCountInTimeFrame", "2");
        public static readonly bool OTPEncryptionEnabled = new AppSettingsConfigValue("UserSessions.OTPEncryptionEnabled", "false");
        public static readonly bool OTPReturnIfNotEncrypted = new AppSettingsConfigValue("UserSessions.OTPReturnIfNotEncrypted", "false");
        
        #endregion
    }
    public static class MimeTypes
    {
        public static readonly string[] Images = new AppSettingsConfigValue("mimeTypes.images", "image/jpg|image/jpeg|image/pjpeg|image/gif|image/x-png|image/png").ToString().Split("|");
    }

    public static class InitialClaimName
    {
        public static readonly string User = new AppSettingsConfigValue("Initial.Claim.Group", "id-onboard-usergroup");
        public static readonly string Role = new AppSettingsConfigValue("Initial.Claim.Role", "id-onboard-role");
        public static readonly string TenantClaimType = new AppSettingsConfigValue("Initial.Claim.Tenant.ClaimType", "id-onboard-company");
        public static readonly string RoleClaimType = new AppSettingsConfigValue("Initial.Claim.Role.ClaimType", "id-onboard-role");
    }

    public static class InitialGroupCodes
    {
        public static readonly string User = new AppSettingsConfigValue("Initial.GroupCode.User", "USER_GRP");
        public static readonly string Partner1GroupCode = new AppSettingsConfigValue("Initial.GroupCode.Partner1", "PARTNER_CO_GRP");
        public static readonly string Partner2GroupCode = new AppSettingsConfigValue("Initial.GroupCode.Partner2", "PARTNER_LTD_GRP");
        public static readonly string InternalUser = new AppSettingsConfigValue("Initial.GroupCode.InternalUser", "INTERNAL_USER_GRP");
        public static readonly string TenantRisk = new AppSettingsConfigValue("Initial.GroupCode.TenantRisk", "TNT_RISK_GRP");
        public static readonly string TenantRiskSupervisor = new AppSettingsConfigValue("Initial.GroupCode.TenantRiskSupervisor", "TNT_MGR_RISK_GRP");
        public static readonly string TenantPSP = new AppSettingsConfigValue("Initial.GroupCode.TenantPSP", "TNT_PSP_GRP");
        public static readonly string TenantPSPSupervisors = new AppSettingsConfigValue("Initial.GroupCode.TenantPSPSupervisors", "TNT_MGR_PSP_GRP");
    }

    public static class InitialClaimValues
    {
        public static readonly string Partner1UserGroupClaimValue = new AppSettingsConfigValue("Initial.Claim.Partner.ValuePartner1", "partnerco");
        public static readonly string Partner2UserGroupClaimValue = new AppSettingsConfigValue("Initial.Claim.Partner.ValuePartner2", "partnerltd");
    }

    public static class Constants
    {
        public static readonly string InitialProjectCode = new AppSettingsConfigValue("Initial.Code.Project", "idonboard");
        public static readonly string InitialTenantCode = new AppSettingsConfigValue("Initial.Code.Tenant", "bspay");
        public static TenantIdentifier GetInitialTenantIdentifier()
        {
            return new TenantIdentifier(InitialTenantCode, InitialProjectCode);
        }
    }

    public static class SupportConstants
    {
        public static readonly string SupportProjectCode = new AppSettingsConfigValue("Support.Code.Project", "system");
        public static readonly string SupportTenantCode = new AppSettingsConfigValue("Support.Code.Tenant", "support_tenant");
        public static readonly string SupportRoleCode = new AppSettingsConfigValue("Support.Code.Role", "support");
        public static readonly string SupportModuleCode = new AppSettingsConfigValue("Support.Code.Module", ModuleCode.SUPERUSER.ToString());
        public static readonly string SupportUserGroupCode = new AppSettingsConfigValue("Support.Code.UserGroup", "SPRT_GRP");
        public static readonly string SupportUsername = new AppSettingsConfigValue("Support.Code.Username", "support_admin");
        public static readonly string TenantAdminUserGroupCode = new AppSettingsConfigValue("Admin.Code.UserGroup", "TNT_ADMIN_GRP");
        public static readonly string TenantAdminRoleCode = new AppSettingsConfigValue("Admin.Code.Role", "tenant-admin");

        public static ProjectIdentifier GetSupportProjectIdentifier()
        {
            return new ProjectIdentifier(SupportProjectCode);
        }
        public static TenantIdentifier GetSupportTenantIdentifier()
        {
            return new TenantIdentifier(SupportTenantCode, SupportProjectCode);
        }

        public static RoleIdentifier GetSupportRoleIdentifier()
        {
            return new RoleIdentifier(SupportRoleCode, GetSupportTenantIdentifier());
        }

        public static UserGroupIdentifier GetSupportUserGroupIdentifier()
        {
            return new UserGroupIdentifier(SupportUserGroupCode, GetSupportTenantIdentifier());
        }
        public static ModuleIdentifier GetSupportModuleIdentifier()
        {
            return new ModuleIdentifier(SupportModuleCode, GetSupportProjectIdentifier());
        }
    }

    public static class OTPServiceConstants
    {
        public static readonly string Token = new AppSettingsConfigValue("OTPService.Token", "LYqunllEcsg9JIiFkit4ICg9i4872jRkzWwjmiam");
        public static readonly string SmsMessage = new AppSettingsConfigValue("OTPService.SmsMessage", "Hi{{recipientFullName}}, Your OTP Value to login to {{companyName}} is {{value}}");
        public static readonly string SmsSender = new AppSettingsConfigValue("OTPService.SmsSender", "Test");// "{{companyName}} Login ");
    }

}
