﻿namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{

    public class MessageObject
    {
        public MessageObject(string message)
        {
            Message = message;
            DebugMessage = message;
            ErrorCode = ErrorCode.GENERIC_ERROR;
            ErrorCodeDisplay = ErrorCode.ToString();
        }
        public MessageObject(string message, ErrorCode errorCode)
        {
            Message = message;
            DebugMessage = message;
            ErrorCode = errorCode;
            ErrorCodeDisplay = ErrorCode.ToString();
        }
        public MessageObject(string message, string debugMessage)
        {
            Message = message;
            DebugMessage = debugMessage;
            ErrorCode = ErrorCode.GENERIC_ERROR;
            ErrorCodeDisplay = ErrorCode.ToString();
        }
        public MessageObject(string message, string debugMessage, ErrorCode errorCode)
        {
            Message = message;
            DebugMessage = debugMessage;
            ErrorCode = errorCode;
            ErrorCodeDisplay = ErrorCode.ToString();
        }

        public string Message { get; set; }
        public string DebugMessage { get; set; }
        public ErrorCode ErrorCode { get; set; }
        public string ErrorCodeDisplay { get; set; }

        public bool IsSuccess
        {
            get
            {
                return this.ErrorCode == ErrorCode.NO_ERROR;
            }
        }

        public string StackTrace { get; set; }
    }

    public class MessageResult<T> : MessageObject
    {
        public MessageResult() : base("")
        {
            this.ResultObject = default(T);
        }
        public MessageResult(ErrorCode errorCode) : base("", errorCode)
        {
            this.ResultObject = default(T);
            this.ErrorCode = errorCode;
        }

        public MessageResult(string message) : base(message)
        {
            this.ResultObject = default(T);
        }

        public MessageResult(T viewModel) : base("", ErrorCode.NO_ERROR)
        {
            if (viewModel == null)
            {
                this.ErrorCode = ErrorCode.GENERIC_ERROR;
                this.ErrorCodeDisplay = ErrorCode.ToString();
            }
            this.ResultObject = viewModel;
        }

        public MessageResult(string message, ErrorCode errorCode) : base(message, errorCode)
        {
            this.ResultObject = default(T);
        }

        public MessageResult(string message, string debugMessage) : base(message, debugMessage)
        {
        }

        public MessageResult(string message, string debugMessage, ErrorCode errorCode) : base(message, debugMessage, errorCode)
        {
            this.ResultObject = default(T);
        }
        public MessageResult(string message, string debugMessage, ErrorCode errorCode, T resultObject) : base(message, debugMessage, errorCode)
        {
            this.ResultObject = resultObject;
        }

        public T ResultObject { get; set; }
    }

}
