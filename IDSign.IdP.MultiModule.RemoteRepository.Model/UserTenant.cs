﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserTenant
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TenantId { get; set; }

        /// <summary>
        ///     User name
        /// </summary>
        public string UserName { get; set; }
        public virtual AppUser User { get; set; }   
        public virtual Tenant Tenant { get; set; }
        public virtual ICollection<UserClaim> UserClaims { get; set; }
        public virtual ICollection<UserToUserGroup> UserTenantUserGroups { get; set; }
        public virtual ICollection<UserToUserGroupManagement> UserGroupManagement { get; set; }
        public virtual ICollection<UserTenantSession> UserTenantSessions { get; set; }
        public UserIdentifier GetUserIdentifier()
        {
            return User.GetIdentifier(Tenant.GetIdentifier());
        }
        public UserTenantIdentifier GetIdentifier()
        {
            return new UserTenantIdentifier(User.TenantUsername, Tenant.GetIdentifier());
        }
    }
}
