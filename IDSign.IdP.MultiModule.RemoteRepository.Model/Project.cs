﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Enabled { get; set; }
        public virtual ICollection<Tenant> Tenants { get; set; }
        public virtual ICollection<Module> Modules { get; set; }
        public virtual ICollection<UserClaimType> UserClaimTypes { get; set; }
        public virtual ICollection<UserGroupClaimType> UserGroupClaimTypes { get; set; }
        public virtual ICollection<RoleClaimType> RoleClaimTypes { get; set; }
        public virtual ICollection<TenantClaimType> TenantClaimTypes { get; set; }

        public ProjectIdentifier GetIdentifier()
        {
            return new ProjectIdentifier(Code);
        }
    }
}
