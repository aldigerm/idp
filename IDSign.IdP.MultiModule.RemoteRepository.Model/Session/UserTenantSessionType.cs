﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserTenantSessionType
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public virtual ICollection<UserTenantSession> UserTenantSessions { get; set; }
    }
}
