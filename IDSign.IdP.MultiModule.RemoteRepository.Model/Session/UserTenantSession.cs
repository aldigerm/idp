﻿using System;
using System.Collections.Generic;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserTenantSession
    {
        public int Id { get; set; }
        public string Identifier { get; set; }
        public string Data { get; set; }
        public DateTimeOffset? ExpiryDate { get; set; }
        public int UserTenantId { get; set; }
        public UserTenant UserTenant { get; set; }
        public int UserTenantSessionTypeId { get; set; }
        public UserTenantSessionType UserTenantSessionType { get; set; }
        public DateTimeOffset CreationDate { get; set; }
    }
}
