﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model
{
    public class UserToUserGroupManagement
    {
        public int Id { get; set; }
        public int UserTenantId { get; set; }
        public virtual UserTenant UserTenant { get; set; }
        public int UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }

        public UserGroupManagementIdentifier GetIdentifier()
        {
            return new UserGroupManagementIdentifier(UserTenant.GetUserIdentifier(), UserGroup.GetIdentifier());
        }
    }
}
