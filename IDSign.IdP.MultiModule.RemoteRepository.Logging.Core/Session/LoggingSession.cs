﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Session
{
    public static class LoggingSession
    {
        public static string GetConnectionId(this HttpContext httpContext)
        {
             return httpContext.Items[HttpContextKey.ConnectionId]?.ToString();
        }
        public static void SetConnectionId(this HttpContext httpContext, string value)
        {
            httpContext.Items[HttpContextKey.ConnectionId] = value;
        }
    }
}
