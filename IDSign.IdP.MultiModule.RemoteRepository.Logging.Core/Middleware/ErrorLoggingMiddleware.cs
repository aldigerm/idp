﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Session;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Middleware
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorLoggingMiddleware(
            RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context
            , ILoggingServiceCollection loggerCollection
            , ILogger<ErrorLoggingMiddleware> logger)
        {

            await _next.Invoke(context);

            var error = context.Features.Get<IExceptionHandlerFeature>();
            if (error != null)
            {
                var ex = error.Error;

                // log it to the text
                string stackTrace = loggerCollection.LoggerHelper.PopulateStackTrace(ex);
                logger.LogError(ex + stackTrace + $"Connection id:{ context.GetConnectionId()}");

                // log it to db
                await loggerCollection.ErrorLogger.LogExceptionAsync(LogSource.TraceLogger, ex, context, context.GetLoggingUser());
            }

        }
    }

}
