﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Session;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Middleware
{
    public class ConnectionIdMiddleware
    {
        private RequestDelegate _next;

        public ConnectionIdMiddleware(
            RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.SetConnectionId(Guid.NewGuid().ToString());
            await _next(context);            
        }
    }
}
