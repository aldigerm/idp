﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Middleware
{
    public class RequestResponseLoggingMiddleware
    {
        private RequestDelegate _next;
        private ILoggingServiceCollection _LoggerCollection;
        protected readonly LogSource logSource;
        private ILogger<RequestResponseLoggingMiddleware> _logger;

        public RequestResponseLoggingMiddleware(
            RequestDelegate next)
        {
            logSource = LogSource.WebHubApiHandler;
            _next = next;
        }

        public async Task Invoke(HttpContext context
            , ILoggingServiceCollection loggerService
            , ILogger<RequestResponseLoggingMiddleware> logger)
        {
            _LoggerCollection = loggerService;
            _logger = logger;
            ApiLog apiLogEntry = null;
            try
            {
                apiLogEntry = await _LoggerCollection.LoggerHelper.BuildApiLogEntryAsync(logSource, context, context.GetLoggingUser());
            }
            catch (Exception e)
            {
                _logger.LogError("Error while doing api log", e);
            }

            if (apiLogEntry != null)
            {
                // Save the request API log entry to the database
                apiLogEntry = await _LoggerCollection.ApiLogger.LogApiEventAsync(apiLogEntry);
            }

            // we hijack the stream
            var originalBodyStream = context.Response.Body;

            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                //Continue down the Middleware pipeline, eventually returning to this class


                await _next(context);

                if (apiLogEntry != null)
                {
                    // log response to the database
                    apiLogEntry = await _LoggerCollection.ApiLogHelper.UpdateApiLogEntryWithResponseDataAsync(apiLogEntry, context.Response, logSource, context.GetLoggingUser());
                }

                // we assign the data back
                await responseBody.CopyToAsync(originalBodyStream);

                // Update the API log entry with result to the database
                await _LoggerCollection.ApiLogger.LogToDbAsync(apiLogEntry);
            }
        }
    }

}
