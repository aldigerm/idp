﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums
{
    
    public enum AuditEventType
    {
        NOT_SPECIFIED,
        USER_ADD,
        USER_GET,
        USER_MODIFY,
        USER_MODIFY_PERMISSIONS,
        USER_DELETE,
        SESSION_LIST_VIEW,
        SESSION_DETAIL_COMPLETE,
        SESSION_DETAIL_DOWNLOAD_DOCUMENTS,
        SESSION_DETAIL_SIGN,
        SESSION_DETAIL_VIEW,
        SESSION_DETAIL_ATTACHMENT_VIEW,
        SESSION_DETAIL_ATTACHMENT_DOWNLOAD_ALL,
        CERTIFICATE_ACTIVATE,
        CERTIFICATE_REVOKE,
        CERTIFICATE_VIEW,
        COMPANY_LIST_VIEW,
        COMPANY_DETAIL_VIEW,
        COMPANY_LIST_GET,
        COMPANY_ADD,
        COMPANY_MODIFY,
        COMPANY_REGISTER_DEFAULT,
        VIDEO_IDENTIFICATION_DETAIL_VIEW,
        VIDEO_IDENTIFICATION_LIST_VIEW,
        VIDEO_IDENTIFICATION_DETAIL_APPROVE,
        VIDEO_IDENTIFICATION_DETAIL_REJECT,
        VIDEO_IDENTIFICATION_DETAIL_REQUEST_CERTIFICATE
    }
    
    public enum HttpVerbs
    {
        GET,
        HEAD,
        POST,
        PUT,
        DELETE,
        TRACE,
        OPTIONS,
        CONNECT
    }
    public enum LogSource
    {
        Test,
        Unspecified,
        TraceLogger,
        LifeCycleService,
        VideoIdenitificationPlatform,
        VideoIdenitificationApi,
        WebHubApiHandler,
        GenericApiHandler,
        ClientLogger,
        Audit,
        MicrosoftFaceApi,
        AdminWebHub,
        ChatHub,
        WexflowDataApi,
        WorkflowDataApi,
        RemoteRepositoryApi
    }    
}
