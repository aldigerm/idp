﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Infrastructure
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddApiLogging(this IServiceCollection services, string loggingConnectionString)
        {
            services.AddTransient<IAuditLogServiceProvider, AuditLogger>();
            services.AddTransient<IApiLogServiceProvider, ApiLogger>();
            services.AddTransient<ApiLogger, ApiLogger>();
            services.AddTransient<LoggerHelper, LoggerHelper>();
            services.AddTransient<ApiLogHelper, ApiLogHelper>();
            services.AddTransient<IErrorLoggerServiceProvider, ErrorLogger>();
            services.AddTransient<IClientErrorLogServiceProvider, ClientErrorLogger>();
            services.AddTransient<ILoggingServiceCollection, LoggingServiceCollection>();

            services.AddDbContext<LoggingModelContext>(options =>
                    options.UseSqlServer(loggingConnectionString));
            return services;
        }
    }
}
