﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Middleware;
using Microsoft.AspNetCore.Builder;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Infrastructure
{

    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ConnectionIdMiddleware>();
            builder.UseMiddleware<RequestResponseLoggingMiddleware>();
            builder.UseMiddleware<ErrorLoggingMiddleware>();
            return builder;
        }
    }

}
