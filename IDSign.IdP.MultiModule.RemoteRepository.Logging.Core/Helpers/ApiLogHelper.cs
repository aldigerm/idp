﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers
{
    public class ApiLogHelper
    {
        private IErrorLoggerServiceProvider _ErrorLogger;
        private LoggerHelper LoggerHelper;
        public ApiLogHelper(IErrorLoggerServiceProvider errorLogger
            , LoggerHelper loggerHelper)
        {
            this.LoggerHelper = loggerHelper;
            _ErrorLogger = errorLogger;
        }

        public ApiLog UpdateApiLogEntryWithResponseData(ApiLog apiLogEntry, Exception ex)
        {
            var now = DateTimeOffset.Now;
            // Update the API log entry with error info
            apiLogEntry.ResponseTimestamp = now;
            apiLogEntry.ErrorMessage = ex.Message;
            apiLogEntry.ErrorStackTrace = LoggerHelper.PopulateStackTrace(ex);
            apiLogEntry.ErrorType = null;
            apiLogEntry.ErrorTimestamp = now;
            return apiLogEntry;
        }

        public async Task<ApiLog> UpdateApiLogEntryWithResponseDataAsync(ApiLog apiLogEntry, HttpResponse response, LogSource logSource, ILoggingUser user)
        {
            var now = DateTimeOffset.Now;

            // Update the API log entry with response info
            apiLogEntry.ResponseStatusCode = (int)response.StatusCode;
            apiLogEntry.ResponseTimestamp = now;

            // add username/companycode which may have been updated
            apiLogEntry.User = user?.UserName;
            apiLogEntry.CompanyCode = user?.CompanyCode;

            if (response.Body != null)
            {
                apiLogEntry.ResponseContentType = response.ContentType;

                if (LoggerHelper.RemoveContentType(apiLogEntry.ResponseContentType))
                {
                    apiLogEntry.ResponseContentBody = apiLogEntry.ResponseContentType + " content was removed from log";
                }
                else
                {
                    response.Body.Seek(0, SeekOrigin.Begin);
                    var text = await new StreamReader(response.Body).ReadToEndAsync();
                    response.Body.Seek(0, SeekOrigin.Begin);

                    apiLogEntry.ResponseContentBody = text;
                }

                apiLogEntry.ResponseHeaders = SerializeHeaders(response.Headers);
            }

            if (!response.IsSuccessStatusCode())
            {
                string reasonPhrase = response?.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase;
                // Update log entry with exception
                apiLogEntry.ErrorTimestamp = now;

                var error = response.HttpContext.Features.Get<IExceptionHandlerFeature>();
                apiLogEntry.ErrorStackTrace = LoggerHelper.PopulateStackTrace(error?.Error);
                apiLogEntry.ErrorType = error?.Error?.GetType().ToString();
                apiLogEntry.ErrorMessage = error?.Error?.Message + "(" + reasonPhrase + ")";
            }

            return apiLogEntry;
        }

        private string SerializeHeaders(IHeaderDictionary headers)
        {
            var dict = new Dictionary<string, string>();

            foreach (var item in headers.ToList())
            {
                if (item.Value.Any())
                {
                    var header = String.Empty;
                    foreach (var value in item.Value)
                    {
                        header += value + " ";
                    }

                    // Trim the trailing space and add item to the dictionary
                    header = header.TrimEnd(" ".ToCharArray());
                    dict.Add(item.Key, header);
                }
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }
    }
}