﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers
{
    public static class HelpersService
    {
        #region Copy Properties
        public static T CopyProperties<T>(object source) where T : new()
        {
            Type type = source.GetType();
            T destination = new T();
            while (type != null)
            {
                CopyPropertiesForType(type, source, destination);
                type = type.BaseType;
            }
            return destination;
        }

        private static void CopyPropertiesForType(Type type, object source, object destination)
        {
            PropertyInfo[] myObjectFields = type.GetProperties(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo property = null;
            foreach (var fi in myObjectFields)
            {
                property = destination.GetType().GetProperty(fi.Name);

                // check if the destination has that property/field and of the same type
                if (property != null && property.PropertyType == fi.PropertyType && property.CanWrite)
                {
                    //if (property.PropertyType == typeof(Array))
                    //{
                    //    Array a = (Array)fi.GetValue(source);
                    //    property.SetValue(destination, a.Clone(), null);
                    //}
                    //else
                    {
                        property.SetValue(destination, fi.GetValue(source));
                    }
                }
            }
        }
        #endregion
        
        #region DescribeObject
        public static string DescribeObject(Object obj)
        {
            try
            {
                var json = JsonConvert.SerializeObject(obj,
                    Newtonsoft.Json.Formatting.None,
                    new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Include,
                        Formatting = Formatting.None
                    });
                return json;
            }
            catch (Exception e)
            {
                return "Cannot serialize: " + e.Message;
            }
        }

        public static string DescribeObjectReflection(Object obj)
        {
            string result = "";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(obj);
                string output = value.ToString();
                if (value is IEnumerable)
                {
                    foreach (var listitem in value as IEnumerable)
                    {
                        output += ",[" + listitem.ToString() + "]";
                    }
                }

                result += String.Format(" [{0}={1}] ", name, value);

            }
            return result;
        }
        #endregion

        #region Binary/String conversions

        public static byte[] EncodeStringToByteArray(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                return new byte[0];

            return Encoding.Unicode.GetBytes(text);
        }

        public static string DecodeByteArrayToString(byte[] byteArray)
        {
            if (byteArray == null)
                return String.Empty;

            return Encoding.Unicode.GetString(byteArray); ;
        }

        public static string GetBytesAsBase64(byte[] byteArray)
        {
            if (byteArray == null)
                return String.Empty;

            return Convert.ToBase64String(byteArray);
        }

        public static byte[] ConvertFromBase64ToBytes(string data)
        {
            if (String.IsNullOrWhiteSpace(data))
                return new byte[0];

            return Convert.FromBase64String(data);
        }

        public static byte[] GetBytes(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        #endregion
    }
}
