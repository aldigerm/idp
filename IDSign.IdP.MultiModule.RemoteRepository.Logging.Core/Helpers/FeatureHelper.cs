﻿using IDSign.IdP.Core.Config;
using Microsoft.Extensions.Logging;
using System;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers
{
    public class FeatureHelper
    {
        #region Members

        public static ILogger<FeatureHelper> logger;
        private static readonly string FeatureSuffix = "Feature.";
        private static bool enableLogging = new AppSettingsConfigValue("Feature.Logging", "false");

        #endregion

        public static bool ApiLogEnabled = isFeatureEnabled("ApiLog");
        public static bool AuditLogEnabled = isFeatureEnabled("AuditLog");
        public static bool ErrorLogEnabled = isFeatureEnabled("ErrorLog");
        public static bool ClientLogEnabled = isFeatureEnabled("ClientLog");
        public static bool LogAuthorizationHeaderEnabled = isFeatureEnabled("LogAuthorizationHeader");

        public static bool isFeatureEnabled(string name)
        {

            string appSettingsName = FeatureSuffix + name;
            if (enableLogging)
            {
                logger?.LogDebug($"The appsettings.config will be checked for {appSettingsName}");
            }
            string feautureValue = new AppSettingsConfigValue(appSettingsName, "false");
            bool featureEnabled = false;
            if (Boolean.TryParse(feautureValue, out featureEnabled))
            {
                if (enableLogging)
                {
                    logger?.LogDebug($"{name} is " + (featureEnabled ? "enabled" : "disabled"));
                }
                return featureEnabled;
            }
            else
            {
                logger?.LogError($"The value \"{feautureValue}\" for {appSettingsName} is not a valid boolean.");
                return false;
            }
        }
    }
}
