﻿using IDSign.IdP.Core.Config;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Session;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers
{
    public class LoggerHelper
    {
        #region Members

        protected readonly string removeContent = new AppSettingsConfigValue("Loggin.RemoveContentForTypes", "application/octet-stream|application/zip|application/pdf|multipart/form-data");
        private IHttpContextAccessor _HttpContextAccessor ;
        #endregion

        public LoggerHelper(
                IHttpContextAccessor httpContextAccessor 
            )
        {
            _HttpContextAccessor = httpContextAccessor  ;
        }

        public T BuildBaseLog<T>(ILoggingObject log, HttpContext httpContext, ILoggingUser ApplicationUser) where T : class
        {
            log.ConnectionId = GetConnectionIDInitializeIfRequired(httpContext);
            log.User = ApplicationUser?.UserName;
            log.CompanyCode = ApplicationUser?.CompanyCode;
            log.Server = Environment.MachineName;
            log.RequestIpAddress = httpContext?.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString();
            log.RequestMachineName = httpContext?.Request?.Host.Value;
            log.RequestContentType = httpContext?.Request?.ContentType?.ToString().Split(';')[0];
            log.RequestMethod = httpContext?.Request.Method;
            log.RequestHeaders = SerializeHeaders(httpContext?.Request?.Headers);
            log.LogTimestamp = DateTimeOffset.Now;
            log.RequestAbsolutePath = httpContext?.Request?.Path.Value;
            log.RequestAuthority = httpContext?.Request?.Host.HasValue == true ? httpContext?.Request?.Host.Value : null;
            log.RequestScheme = httpContext?.Request?.Scheme;
            log.RequestQuery = httpContext?.Request?.QueryString.ToUriComponent();
            return log as T;
        }

        public async Task<ApiLog> BuildApiLogEntryAsync(LogSource logSource, HttpContext httpContext, ILoggingUser ApplicationUser)
        {
            ApiLog log = new ApiLog();
            log = BuildBaseLog<ApiLog>(log, httpContext, ApplicationUser);
            log.Application = logSource.ToString();
            

            if (httpContext.Request.Body != null)
            {
                var result = "";
                string types = removeContent;
                if (RemoveContentType(log.RequestContentType))
                {
                    result = log.RequestContentType + " content was removed from log";
                }
                else
                {

                    // Enable seeking
                    httpContext.Request.EnableBuffering();
                    // Read the stream as text
                    result = await new System.IO.StreamReader(httpContext.Request.Body).ReadToEndAsync();
                    // Set the position of the stream to 0 to enable rereading
                    httpContext.Request.Body.Position = 0;
                }
                log.RequestContentBody = result;
            }

            return log;
        }
        
        public bool RemoveContentType(string contentType)
        {
            string types = removeContent;
            if (types != null && contentType != null && types.Split('|').Any(t => contentType.StartsWith(t)))
            {
                return true;
            }
            return false;
        }

        public AuditLog BuildAuditLog(AuditEventType eventType, HttpContext httpContext, ILoggingUser user)
        {
            AuditLog log = new AuditLog();
            log = BuildBaseLog<AuditLog>(log, httpContext, user);
            log.AuditEventType = eventType.ToString();
            log.ClientUserAgent = httpContext?.Request?.Headers[HeaderNames.UserAgent];
            log.Application = LogSource.Audit.ToString();
            return log;
        }
        public string GetAnyValidationError(Exception ex)
        {
            //if (ex is DbEntityValidationException)
            //{
            //    System.Data.Entity.Validation.DbEntityValidationException validationException = (System.Data.Entity.Validation.DbEntityValidationException)ex;
            //    if (validationException.EntityValidationErrors != null && validationException.EntityValidationErrors.Any())
            //    {
            //        string errors = " Errors -> ";
            //        foreach (var validationError in validationException.EntityValidationErrors.Select(eve => eve.ValidationErrors))
            //        {
            //            foreach (var ve in validationError)
            //            {
            //                errors += " Validation Error => Property -> {" + ve.PropertyName + "} Error -> {" + ve.ErrorMessage + "}";
            //            }
            //        }
            //        return errors;
            //    }
            //}
            return "";
        }


        public string PopulateStackTrace(Exception exception)
        {
            return PopulateStackTrace(exception, "");
        }

        private string GetConnectionIDInitializeIfRequired(HttpContext httpContext)
        {
            string connectionId = httpContext.GetConnectionId();
            if (string.IsNullOrWhiteSpace(connectionId))
            {
                throw new Exception("ConnectionId not found in items. Is ConnectionIdMiddleware registered?");
            }
            return connectionId;
        }

        private string SerializeHeaders(IHeaderDictionary headers)
        {
            if (headers == null)
                return null;

            var dict = headers.ToDictionary(a => a.Key, a => string.Join(";", a.Value));
            return SerializeHeaders(dict);
        }

        private string SerializeHeaders(NameValueCollection headers)
        {
            if (headers == null)
                return null;
            var dict = headers.AllKeys.ToDictionary(x => x, x => headers[x]);
            return SerializeHeaders(dict);
        }

        private string SerializeHeaders(Dictionary<string, string> dict)
        {
            if (dict == null || !dict.Any())
                return null;

            if (!FeatureHelper.LogAuthorizationHeaderEnabled && dict.Keys.Any(k => k == "Authorization"))
            {
                dict["Authorization"] = "<truncated by logger>";
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }

        private string PopulateStackTrace(Exception exception, string result)
        {
            if (exception == null)
            {
                return result;
            }
            else
            {
                result += "\n" + exception.Message;
                result += "\n" + exception.StackTrace;
                return PopulateStackTrace(exception.InnerException, result);
            }
        }

    }
}
