﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{

    public class AuditData : Dictionary<string, object>
    { }

    public class AuditLogger : IAuditLogServiceProvider
    {
        #region Members

        protected readonly ILogger<AuditLogger> _logger;
        protected readonly IErrorLoggerServiceProvider _ErrorLogger;
        protected readonly LoggerHelper LoggerHelper;
        protected readonly LoggingModelContext _ctx;
        #endregion

        #region ctor
        public AuditLogger(
            IErrorLoggerServiceProvider errorLogger
            , LoggerHelper loggerHelper
            , ILogger<AuditLogger> logger)
        {
            LoggerHelper = loggerHelper;
            _ErrorLogger = errorLogger;
            _logger = logger;
        }
        #endregion

        public async Task<AuditLog> LogAuditEventAsync(AuditEventType eventType, HttpContext httpContext, ILoggingUser user)
        {
            AuditLog log = LoggerHelper.BuildAuditLog(eventType, httpContext, user);
            return await LogAuditEventToDB(log);
        }

        public async Task<AuditLog> LogAuditEventAsync(AuditLog log)
        {
            return await LogAuditEventToDB(log);
        }

        private async Task<AuditLog> LogAuditEventToDB(AuditLog log)
        {
            return await LogToDbAsync(log);
        }

        public async Task<AuditLog> LogErrorAsync(AuditLog log)
        {
            log.ErrorTimestamp = DateTimeOffset.Now;
            log.ResponseTimestamp = DateTimeOffset.Now;
            return await LogToDbAsync(log);
        }

        public async Task<AuditLog> LogSuccessAsync(AuditLog log)
        {
            log.ResponseTimestamp = DateTimeOffset.Now;
            return await LogToDbAsync(log);
        }

        private async Task<AuditLog> LogToDbAsync(AuditLog log)
        {
            if (FeatureHelper.AuditLogEnabled)
            {
                try
                {

                    var logXML = XmlHelper.ObjectToXMLGeneric<AuditLog>(log);
                    var logParam = new SqlParameter()
                    {
                        ParameterName = "@log",
                        Value = logXML,
                        SqlDbType = SqlDbType.Xml
                    };

                    var outParm = new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = 0,
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output
                    };

                    var result = await _ctx.Database
                            .ExecuteSqlCommandAsync("EXEC [" + Constants.LoggingDatabaseSchema + "].[LOG001_AuditLog] @log, @id out", logParam, outParm);
                    log.Id = (int)outParm.Value;
                    _logger.LogDebug($"audit event logged {log.RequestAbsolutePath} Id is : {log.Id} {(log.ResponseTimestamp.HasValue ? (log.ErrorTimestamp.HasValue ? "Error" : "Success") : "Pending")}");

                }
                catch (Exception ex)
                {
                    this._logger.LogError("Couldn't add to audit log:", ex);
                }
            }
            return log;
        }
    }
}
