﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public interface ILoggingServiceCollection
    {
        ApiLogHelper ApiLogHelper { get; }
        LoggerHelper LoggerHelper { get; }
        IApiLogServiceProvider ApiLogger { get; }
        IErrorLoggerServiceProvider ErrorLogger { get; }
        IClientErrorLogServiceProvider ClientErrorLogService { get; }
        IAuditLogServiceProvider AuditLogger { get; }
    }
}
