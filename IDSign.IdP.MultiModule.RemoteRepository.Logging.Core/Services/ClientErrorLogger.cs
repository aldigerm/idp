﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public class ClientErrorLogger : IClientErrorLogServiceProvider
    {
        #region Members

        protected readonly ILogger<ClientErrorLogger> _logger;
        protected readonly IErrorLoggerServiceProvider _ErrorLogger;
        protected readonly LoggerHelper LoggerHelper;
        protected readonly LoggingModelContext _ctx;
        private readonly HttpContext HttpContext;

        #endregion

        #region ctor
        public ClientErrorLogger(
              IErrorLoggerServiceProvider errorLogger
            , LoggerHelper loggerHelper
            , ILogger<ClientErrorLogger> logger
            , LoggingModelContext ctx
            , IHttpContextAccessor httpContextAccessor)
        {
            LoggerHelper = loggerHelper;
            _ErrorLogger = errorLogger;
            _logger = logger;
            _ctx = ctx;
            HttpContext = httpContextAccessor?.HttpContext;
        }
        #endregion

        public async Task<ClientErrorLog> LogErrorAsync(string source, string errorMessage, HttpContext httpContext, ILoggingUser user)
        {
            ClientErrorLog log = new ClientErrorLog();
            log = LoggerHelper.BuildBaseLog<ClientErrorLog>(log, httpContext, user);

            log.ErrorSource = source == null ? "no source" : source;
            log.ErrorMessage = errorMessage == null ? "no message" : errorMessage;
            log.ClientUserAgent = HttpContext?.Request?.Headers[HeaderNames.UserAgent];
            log.ErrorTimestamp = DateTimeOffset.Now;
            log.Application = LogSource.ClientLogger.ToString();
            return await LogToDbAsync(log);
        }

        public async Task<ClientErrorLog> LogToDbAsync(ClientErrorLog log)
        {
            if (FeatureHelper.ClientLogEnabled)
            {
                try
                {
                   
                        var logXML = XmlHelper.ObjectToXMLGeneric<ClientErrorLog>(log);
                        var logParam = new SqlParameter()
                        {
                            ParameterName = "@log",
                            Value = logXML,
                            SqlDbType = SqlDbType.Xml
                        };

                        var outParm = new SqlParameter
                        {
                            ParameterName = "@id",
                            Value = 0,
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Output
                        };

                        var result = await _ctx.Database
                                .ExecuteSqlCommandAsync("EXEC [" + Constants.LoggingDatabaseSchema + "].[LOG004_ClientClientErrorLog] @log, @id out", logParam, outParm);
                        log.Id = (int)outParm.Value;
                        _logger.LogDebug($"client error logged to db. Id is : {log.Id}");
                    
                }
                catch (Exception ex)
                {
                    this._logger.LogError("Couldn't add to client error log:", ex);
                }
            }
            return log;

        }
    }
}
