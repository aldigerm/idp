﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public class ApiLogger : IApiLogServiceProvider
    {
        #region Members

        protected readonly ILogger<ApiLogger> _logger;
        protected readonly IErrorLoggerServiceProvider _ErrorLogger;
        protected readonly LoggingModelContext _ctx;
        #endregion

        #region ctor
        public ApiLogger(IErrorLoggerServiceProvider errorLogger
            , ILogger<ApiLogger> logger
            , LoggingModelContext ctx)
        {
            _ErrorLogger = errorLogger;
            _logger = logger;
            _ctx = ctx;
        }
        #endregion

        public async Task<ApiLog> LogApiEventAsync(ApiLog log)
        {
            return await LogToDbAsync(log);
        }

        public async Task<ApiLog> LogToDbAsync(ApiLog log)
        {
            if (FeatureHelper.ApiLogEnabled)
            {
                try
                {

                    var logXML = XmlHelper.ObjectToXMLGeneric<ApiLog>(log);
                    var logParam = new SqlParameter()
                    {
                        ParameterName = "@log",
                        Value = logXML,
                        SqlDbType = SqlDbType.Xml
                    };

                    var outParm = new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = 0,
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output
                    };

                    var result = await _ctx.Database
                            .ExecuteSqlCommandAsync("EXEC [" + Constants.LoggingDatabaseSchema + "].[LOG002_ApiLog] @log, @id out", logParam, outParm);
                    log.Id = (int)outParm.Value;
                    _logger.LogDebug($"api event logged {log.Application} Id is : {log.Id} {log.RequestAbsolutePath} {(log.ResponseTimestamp.HasValue ? (log.ErrorTimestamp.HasValue ? "Error" : "Success") : "Pending")}");
                }
                catch (Exception ex)
                {
                    this._logger.LogError("Couldn't add to api log:", ex);
                }
            }
            return log;
        }

    }
}
