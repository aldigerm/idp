﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public class LoggingServiceCollection : ILoggingServiceCollection
    {
        public LoggingServiceCollection(
          LoggerHelper loggerHelper
        , ApiLogHelper apiLogHelper
        , IApiLogServiceProvider apiLogServiceProvider
        , IErrorLoggerServiceProvider errorLoggerServiceProvider
        , IClientErrorLogServiceProvider clientErrorLogServiceProvider
        , IAuditLogServiceProvider auditLogServiceProvider
            )
        {
            LoggerHelper = loggerHelper;
            ApiLogHelper = apiLogHelper;
            ApiLogger = apiLogServiceProvider;
            ErrorLogger = errorLoggerServiceProvider;
            ClientErrorLogService = clientErrorLogServiceProvider;
            AuditLogger = auditLogServiceProvider;
        }
        public LoggerHelper LoggerHelper { get; }
        public ApiLogHelper ApiLogHelper { get; }
        public IApiLogServiceProvider ApiLogger { get; }
        public IErrorLoggerServiceProvider ErrorLogger { get; }
        public IClientErrorLogServiceProvider ClientErrorLogService { get; }
        public IAuditLogServiceProvider AuditLogger { get; }
    }
}
