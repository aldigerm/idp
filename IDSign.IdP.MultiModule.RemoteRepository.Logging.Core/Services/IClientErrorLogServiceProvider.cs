﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public interface IClientErrorLogServiceProvider
    {
        Task<ClientErrorLog> LogErrorAsync(string source, string errorMessage, HttpContext httpContext, ILoggingUser user);
        Task<ClientErrorLog> LogToDbAsync(ClientErrorLog log);
    }
}
