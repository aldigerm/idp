﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public interface IErrorLoggerServiceProvider
    {
        Task<ErrorLog> LogExceptionAsync(LogSource source, Exception exception, HttpContext httpContext, ILoggingUser user);
        
        Task<ErrorLog> LogErrorAsync(LogSource source, string errorMessag, Exception exception, HttpContext httpContext, ILoggingUser user);

    }
}
