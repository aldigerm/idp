﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public interface IAuditLogServiceProvider
    {
        Task<AuditLog> LogAuditEventAsync(AuditEventType eventType, HttpContext httpContext, ILoggingUser user);
        Task<AuditLog> LogAuditEventAsync(AuditLog log);
        Task<AuditLog> LogErrorAsync(AuditLog log);
        Task<AuditLog> LogSuccessAsync(AuditLog log);
    }
}
