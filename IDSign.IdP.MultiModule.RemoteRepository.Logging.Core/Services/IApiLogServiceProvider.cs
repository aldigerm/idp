﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public interface IApiLogServiceProvider
    {
        Task<ApiLog> LogApiEventAsync(ApiLog log);
        Task<ApiLog> LogToDbAsync(ApiLog log);
    }
}
