﻿using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Helpers;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Core
{
    public class ErrorLogger : IErrorLoggerServiceProvider
    {
        #region Members

        protected readonly ILogger<ErrorLogger> _logger;
        protected readonly LoggerHelper LoggerHelper;
        protected readonly LoggingModelContext _ctx;
        #endregion

        #region ctor
        public ErrorLogger(LoggerHelper loggerHelper
            , LoggingModelContext ctx
            , ILogger<ErrorLogger> logger)
        {
            LoggerHelper = loggerHelper;
            _ctx = ctx;
            _logger = logger;
        }
        #endregion

        public async Task<ErrorLog> LogExceptionAsync(LogSource source, Exception exception, HttpContext httpContext, ILoggingUser user)
        {
            return await LogErrorAsync(source, exception.Message, exception, httpContext, user);
        }

        public async Task<ErrorLog> LogErrorAsync(LogSource source, string errorMessage, Exception exception, HttpContext httpContext, ILoggingUser user)
        {
            ErrorLog log = new ErrorLog();
            log = LoggerHelper.BuildBaseLog<ErrorLog>(log, httpContext, user);
            if (String.IsNullOrWhiteSpace(errorMessage))
            {
                errorMessage = exception?.Message;
                errorMessage = (errorMessage == null ? "<no error message>" : errorMessage);
            }
            log.ErrorMessage = errorMessage + LoggerHelper.GetAnyValidationError(exception);
            log.ErrorStackTrace = LoggerHelper.PopulateStackTrace(exception);
            log.ErrorType = exception?.GetType().ToString();
            log.ErrorTimestamp = DateTimeOffset.Now;
            log.Application = source.ToString();

            return await LogToDbAsync(log);
        }


        private async Task<ErrorLog> LogToDbAsync(ErrorLog log)
        {
            if (FeatureHelper.ErrorLogEnabled)
            {
                try
                {

                    var logXML = XmlHelper.ObjectToXMLGeneric<ErrorLog>(log);
                    var logParam = new SqlParameter()
                    {
                        ParameterName = "@log",
                        Value = logXML,
                        SqlDbType = SqlDbType.Xml
                    };

                    var outParm = new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = 0,
                        SqlDbType = SqlDbType.Int,
                        Direction = ParameterDirection.Output
                    };

                    var result = await _ctx.Database
                            .ExecuteSqlCommandAsync("EXEC [" + Constants.LoggingDatabaseSchema + "].[LOG003_ErrorLog] @log, @id out", logParam, outParm);
                    log.Id = (int)outParm.Value;
                    _logger.LogDebug($"error logged to db {log.Application} Id is : {log.Id}");

                }
                catch (Exception ex)
                {
                    this._logger.LogError("Couldn't add to error log:", ex);
                }
            }
            return log;
        }
    }
}
