﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/Module")]
    [ApiController]
    public class ModuleController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IModuleServiceProvider _ModuleServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public ModuleController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IModuleServiceProvider moduleServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _ModuleServiceProvider = moduleServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // GET: api/Module
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get()
        {
            IList<ModuleViewModel> model = await _ModuleServiceProvider.GetLoggedInModuleAsync();
            return Ok(model);
        }
        
        // GET: api/Module/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] ModuleIdentifier moduleIdentifier)
        {
            ModuleViewModel result = await _ModuleServiceProvider.GetModuleViewModelAsync(moduleIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Module/Detail
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.get)]
        public async Task<IActionResult> List([FromQuery] string projectCode = null)
        {
            ProjectIdentifier projectIdentifier = new ProjectIdentifier(projectCode);
            IList<ModuleViewModel> result = await _ModuleServiceProvider.GetAccessibleListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Module/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(ModuleCreateModel model)
        {
            // cannot create the support module
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.GetModuleIdentifier());

            ModuleIdentifier result = await _ModuleServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // Post: api/Module/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(ModuleUpdateViewModel model)
        {
            // cannot update the support module
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.Identifier);

            var result = await _ModuleServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // POST: api/Module/Update
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.Module, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] ModuleIdentifier moduleIdentifier)
        {
            // cannot update the support module
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(moduleIdentifier);

            // cannot delete admin modules
            _ManagementServiceProvider.EnforceNotChangeSystemModulesAsync(moduleIdentifier);

            int result = await _ModuleServiceProvider.DeleteAsync(moduleIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }
    }

}
