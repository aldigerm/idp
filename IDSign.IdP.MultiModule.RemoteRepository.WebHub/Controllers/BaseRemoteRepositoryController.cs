﻿using IdentityServer4.Models;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Produces("application/json")]
    [RemoteRepositoryAuthorize]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BaseRemoteRepositoryController : Controller
    {
        protected bool IsIdentityResourceReadOnly(string identityResourceName)
        {
            if (identityResourceName == new IdentityResources.OpenId().Name || identityResourceName == new IdentityResources.Profile().Name)
                return true;
            return false;
        }

        protected bool IsApiResourceReadOnly(string identityResourceName)
        {
            return false;
        }

        protected bool IsApiScopeReadOnly(string identityResourceName)
        {
            return false;
        }

    }
}
