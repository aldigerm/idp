﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/TenantClaimType")]
    [ApiController]
    public class TenantClaimTypeController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly ITenantClaimTypeServiceProvider _TenantClaimTypeServiceProvider;

        //Inject context instance here.
        public TenantClaimTypeController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , ITenantClaimTypeServiceProvider userClaimTypeServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _TenantClaimTypeServiceProvider = userClaimTypeServiceProvider;
        }


        // GET: api/Tenant/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] TenantClaimTypeIdentifier userClaimTypeIdentifier)
        {
            TenantClaimTypeModel result = await _TenantClaimTypeServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Tenant/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null)
        {
            ProjectIdentifier projectIdentifier = new ProjectIdentifier(projectCode);
            List<TenantClaimTypeModel> result = await _TenantClaimTypeServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Tenant/SetTenants
        [HttpPost]
        [Route("SetTenants")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.TenantClaimType.setTenants)]
        public async Task<IActionResult> SetTenants(TenantClaimTypeSetTenantsModel model)
        {
            var result = await _TenantClaimTypeServiceProvider.SetTenants(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/TenantClaimType/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(TenantClaimTypeCreateModel model)
        {
            TenantClaimTypeIdentifier result = await _TenantClaimTypeServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/TenantClaimType/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(TenantClaimTypeUpdateModel model)
        {
            var result = await _TenantClaimTypeServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/TenantClaimType/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.TenantClaimType, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] TenantClaimTypeIdentifier model)
        {
            var result = await _TenantClaimTypeServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
