﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/Role")]
    [ApiController]
    public class RoleController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IRoleServiceProvider _RoleServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public RoleController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IRoleServiceProvider roleServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _RoleServiceProvider = roleServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // GET: api/Role
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get()
        {
            IList<RoleViewModel> model = await _RoleServiceProvider.GetLoggedInRolesAsync();
            return Ok(model);
        }

        // GET: api/Role/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] RoleIdentifier roleIdentifier)
        {
            RoleViewModel result = await _RoleServiceProvider.GetViewModelAsync(roleIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Role/Detail
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.get)]
        public async Task<IActionResult> List(string tenantCode = null, string projectCode = null)
        {
            TenantIdentifier tenantIdentifier = new TenantIdentifier(tenantCode, projectCode);
            IList<RoleViewModel> result = await _RoleServiceProvider.GetAccessibleListAsync(tenantIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Role/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(RoleCreateModel model)
        {
            // cannot create support role
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.GetRoleIdentifier());

            RoleIdentifier result = await _RoleServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // Post: api/Role/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(RoleUpdateViewModel model)
        {

            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            // cannot modify the same roles as your own
            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            // cannot modify support role
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.Identifier);

            RoleIdentifier result = await _RoleServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // Post: api/Role/setModules
        [HttpPost]
        [Route("SetModules")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Role.setModule)]
        public async Task<IActionResult> SetModules(RoleSetModulesModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            // cannot modify the same roles
            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            // cannot modify support role
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.Identifier);

            foreach (var module in model.ModuleCodes)
            {
                // we cannot add/remove support modules
                _ManagementServiceProvider.EnforceNotChangeSupportAsync(new ModuleIdentifier(module, model.Identifier.GetProjectIdentifier()));
            }

            RoleViewModel result = await _RoleServiceProvider.SetModules(model.Identifier, model.ModuleCodes);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }


        // Post: api/Role/SetInheritingRoles
        [HttpPost]
        [Route("SetInheritingRoles")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Role.setInheritingRoles)]
        public async Task<IActionResult> SetInheritingRoles(RoleSetInheritingRolesModel model)
        {

            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            foreach(var roleCode in model.InheritsFromRoles)
            {
                await _ManagementServiceProvider.EnforceAccessibilty(new RoleIdentifier(roleCode, model.Identifier.GetTenantIdentifier()));

                await _ManagementServiceProvider.EnforceNotRelatedAsync(new RoleIdentifier(roleCode, model.Identifier.GetTenantIdentifier()));
            }

            RoleViewModel result = await _RoleServiceProvider.SetInheritingRoles(model.Identifier, model.InheritsFromRoles);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // Post: api/Role/setUserGroups
        [HttpPost]
        [Route("SetUserGroups")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Role.setUserGroups)]
        public async Task<IActionResult> SetUserGroups(RoleSetUserGroupsModel model)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            RoleViewModel result = await _RoleServiceProvider.SetUserGroups(model.Identifier, model.UserGroups);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // Delete: api/Role/delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.Role, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] RoleIdentifier roleIdentifier)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(roleIdentifier);

            _ManagementServiceProvider.EnforceNotChangeSupportAsync(roleIdentifier);

            int result = await _RoleServiceProvider.DeleteAsync(roleIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }
    }

}
