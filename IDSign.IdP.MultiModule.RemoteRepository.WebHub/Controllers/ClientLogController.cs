﻿using IDSign.IdP.Core;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/Clientlog")]
    [ApiController]
    public class ClientLogController : BaseRemoteRepositoryController
    {
        protected readonly IClientErrorLogServiceProvider _ClientErrorLogger;
        protected readonly IUserServiceProvider _UserServiceProvider;
        protected readonly IHttpContextAccessor _HttpContextAccessor;
        protected readonly ILogger<ClientLogController> _Logger;

        //Inject context instance here.
        public ClientLogController(
             IUserServiceProvider userServiceProvider
            , IClientErrorLogServiceProvider clientErrorLogger
            , IHttpContextAccessor httpContextAccessor
            , ILogger<ClientLogController> logger)
        {
            _ClientErrorLogger = clientErrorLogger;
            _UserServiceProvider = userServiceProvider;
            _HttpContextAccessor = httpContextAccessor;
            _Logger = logger;
        }

        // GET: api/clientlog/error
        #region Error
        [HttpPost]
        [Route("error")]
        [AllowAnonymous]
        public async Task<IActionResult> Error([FromBody] LogRequest request)
        {

            // try to get user if logged in. this is needed for logging the username if anyone is logged in
            var principal = await _UserServiceProvider.InitialiseUserFromPrincipal(_HttpContextAccessor?.HttpContext?.User);

            if (request == null)
            {
                request = new LogRequest();
                request.Message = "Received to log client error but it was null. Logging empty error for reference.";
                _Logger.LogError("Received to log client error but it was null. Logging empty error for reference.");
            }
            else
            {
                _Logger.LogError("Received to log client error : " + HelpersService.DescribeObject(request));
            }
            var clientErrorLog = await _ClientErrorLogger.LogErrorAsync(request.Source, request.Message, Request.HttpContext, Request.HttpContext.GetLoggingUser());
            var connectionId = clientErrorLog?.ConnectionId == null ? "error" : clientErrorLog.ConnectionId;
            return Json(clientErrorLog.ConnectionId);
        }
        #endregion

    }

}
