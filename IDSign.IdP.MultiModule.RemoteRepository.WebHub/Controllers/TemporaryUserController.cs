﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/TemporaryUser")]
    [ApiController]
    public class TemporaryUserControllerController : BaseRemoteRepositoryController
    {
        private readonly ITemporaryUserServiceProvider _TemporaryUserServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public TemporaryUserControllerController(
             ITemporaryUserServiceProvider temporaryUserServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _TemporaryUserServiceProvider = temporaryUserServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // POST: api/UserTenantSession/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.TemporaryUser, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(CreateTemporaryUserRequestModel model)
        {

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.GetTenantIdentifier());

            var result = await _TemporaryUserServiceProvider.CreateTemporaryUserAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException("Couldn't create Temporary User Access");
            }
        }

        // POST: api/UserTenantSession/Create
        [HttpPost]
        [Route("Login")]
        [ResourceAuthorize(ResourcesTypes.TemporaryUser, ActionsTypes.User.login)]
        public async Task<IActionResult> Login(OTPTemporaryUserLoginRequestModel model)
        {

            var result = await _TemporaryUserServiceProvider.LoginAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException("Couldn't create Temporary User Access");
            }
        }

    }

}
