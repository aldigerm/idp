﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/TenantClaim")]
    [ApiController]
    public class TenantClaimController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly ITenantClaimServiceProvider _TenantClaimServiceProvider;

        //Inject context instance here.
        public TenantClaimController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , ITenantClaimServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _TenantClaimServiceProvider = userServiceProvider;
        }

        // GET: api/Tenant/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] TenantClaimIdentifier userClaimTypeIdentifier)
        {
            TenantClaimModel result = await _TenantClaimServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/Tenant/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode,projectCode);
            var result = await _TenantClaimServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/TenantClaim/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(TenantClaimCreateModel model)
        {
            TenantClaimIdentifier result = await _TenantClaimServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/TenantClaim/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(TenantClaimUpdateModel model)
        {
            TenantClaimIdentifier result = await _TenantClaimServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // POST: api/Tenant/SetTenants
        [HttpPost]
        [Route("SetTenants")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.TenantClaim.setTenants)]
        public async Task<IActionResult> SetTenants(TenantClaimSetTenantsModel model)
        {
            var result = await _TenantClaimServiceProvider.SetTenants(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/TenantClaim/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.TenantClaim, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] TenantClaimIdentifier model)
        {
            var result = await _TenantClaimServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
