﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserServiceProvider _UserServiceProvider;
        private readonly IUserTenantServiceProvider _UserTenantServiceProvider;
        private readonly IUserSharedServiceProvider _UserSharedServiceProvider;
        private readonly IHttpContextAccessor _HttpContextAccessor ;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public UserController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserServiceProvider userServiceProvider
            , IHttpContextAccessor httpContextAccessor 
            , IManagementServiceProvider managementServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IUserTenantServiceProvider userTenantServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserServiceProvider = userServiceProvider;
            _HttpContextAccessor = httpContextAccessor  ;
            _ManagementServiceProvider = managementServiceProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _UserTenantServiceProvider = userTenantServiceProvider;
        }

        // GET: api/User
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get(bool getAccessDetail = false)
        {
            UserViewModel model = await _UserSharedServiceProvider.GetLoggedInUser(getAccessDetail);
            return Ok(model);
        }

        // POST: api/User/Login
        [HttpPost]
        [Route("Login")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.login)]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var result = await _UserServiceProvider.LoginUserAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/User/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.detail)]
        public async Task<IActionResult> Details([FromQuery] UserIdentifier userIdentifier)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(userIdentifier);

            var result = await _UserSharedServiceProvider.GetUserViewModelAsync(userIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/User/Profile
        [HttpGet]
        [Route("Profile")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.profile)]
        public async Task<IActionResult> Profile(string username = null, string tenantCode = null, string projectCode = null)
        {
            var result = await _UserSharedServiceProvider.GetUserProfileAsync(username, tenantCode, projectCode);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/User/ProfileImage
        [HttpGet]
        [Route("ProfileImage")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.profile)]
        public async Task<IActionResult> ProfileImage(string username = null, string tenantCode = null, string projectCode = null)
        {
            var result = await _UserSharedServiceProvider.GetUserProfileImageAsync(username, tenantCode, projectCode);
            if (result != null)
            {
                return Ok(result); ;
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/User/Detail
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.list)]
        public async Task<IActionResult> List(string tenantCode = null, string projectCode = null, [FromQuery] bool includeTemporaryUsers = false)
        {
            TenantIdentifier tenantIdentifier = null;
            if (!string.IsNullOrWhiteSpace(tenantCode) && !string.IsNullOrWhiteSpace(projectCode))
            {
                tenantIdentifier = new TenantIdentifier(tenantCode, projectCode);
            }
            var result = await _UserSharedServiceProvider.GetAccessibleListAsync(tenantIdentifier, includeTemporaryUsers);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/User/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.create)]
        public async Task<IActionResult> Create(UserRegisterViewModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.TenantIdentifier);

            foreach (var ug in model.UserGroups)
            {
                await _ManagementServiceProvider.EnforceAccessibilty(new UserGroupIdentifier(ug, model.TenantIdentifier));
            }

            UserIdentifier result = await _UserSharedServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/User/SetUserGroups
        [HttpPost]
        [Route("SetUserGroups")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.setUserGroups)]
        public async Task<IActionResult> SetUserGroups(SetUserGroup model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.Identifier);

            foreach (var ug in model.UserGroups)
            {
                await _ManagementServiceProvider.EnforceNeedingSupportCredentials(new UserGroupIdentifier(ug, model.Identifier.GetTenantIdentifier()));
            }

            var result = await _UserServiceProvider.SetGroupsForUser(model.Identifier, model.UserGroups.ToArray());
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/User/SetUserGroups
        [HttpPost]
        [Route("SetManagedUserGroups")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.setManagedUserGroups)]
        public async Task<IActionResult> SetManagedUserGroups(SetUserGroup model)
        {
            foreach (var ug in model.UserGroups)
            {
                await _ManagementServiceProvider.EnforceNeedingSupportCredentials(new UserGroupIdentifier(ug, model.Identifier.GetTenantIdentifier()));
            }

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.Identifier);

            _ManagementServiceProvider.EnforceNotSameUser(model.Identifier);

            if (_HttpContextAccessor.LoggedInUserIdentifier().IsEquals(model.Identifier))
            {
                throw new IdPUnauthorizedException(ErrorCode.USER_NOT_ACCESSIBLE, "Cannot set your own user groups.");
            }

            var result = await _UserServiceProvider.SetManagedGroupsForUser(model.Identifier, model.UserGroups.ToArray());
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }
        // POST: api/User/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.update)]
        public async Task<IActionResult> Update(UserUpdateViewModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.Identifier);

            UserIdentifier result = await _UserServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        [HttpPost]
        [Route("setPassword")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.setPassword)]
        public async Task<IActionResult> SetPassword(UpdatePasswordViewModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.Identifier);

            var result = await _UserServiceProvider.UpdatePasswordAsync(model);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        [HttpPost]
        [Route("setSecuritySettings")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.setSecuritySettings)]
        public async Task<IActionResult> SetSecuritySettings(UpdateSecuritySettingsViewModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.Identifier);

            var result = await _UserServiceProvider.UpdateSecuritySettingsAsync(model);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserGroup/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserIdentifier userIdentifier)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(userIdentifier);

            int result = await _UserServiceProvider.DeleteAsync(userIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }


        [HttpPost]
        [Route("Upload")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.upload)]
        public async Task<IActionResult> Upload()
        {
            var httpRequest = Request;
            string username = httpRequest.Headers["username"];
            string tenantCode = httpRequest.Headers["tenantCode"];
            string projectCode = httpRequest.Headers["projectCode"];
            string userGuid = httpRequest.Headers["userGuid"];

            UserIdentifier userIdentifier = new UserIdentifier(userGuid, username, tenantCode, projectCode);

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(userIdentifier);

            if (httpRequest.Form.Files.Count == 1)
            {
                var postedFile = httpRequest.Form.Files[0];
                bool uploadedDocumentResult = false;
                using (var stream = postedFile.OpenReadStream())
                {
                    uploadedDocumentResult = await _UserServiceProvider.UploadProfilePictureAsync(userIdentifier, postedFile.ContentType, stream);
                }

                if (uploadedDocumentResult)
                {
                    return Ok();
                }
                else
                {
                    throw new IdPException();
                }
            }
            else
            {
                throw new IdPException(ErrorCode.INCORRECT_FORM, (int)HttpStatusCode.BadRequest, "1 file is required to be uploaded, not 0 or more.");
            }
        }

        // POST: api/User/Link
        [HttpPost]
        [Route("Link")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.link)]
        public async Task<IActionResult> Link(UserTenantCreateModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.TenantIdentifier);

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.UserIdentifier);

            UserTenantIdentifier result = await _UserTenantServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/User/Unlink
        [HttpPost]
        [Route("Unlink")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.unlink)]
        public async Task<IActionResult> Unlink(UserTenantIdentifier model)
        {
            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.GetTenantIdentifier());

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(model.GetUserIdentifier());

            var result = await _UserTenantServiceProvider.DeleteAsync(model);
            if (result > 0)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/User/SetUserTenants
        [HttpPost]
        [Route("SetUserTenants")]
        [ResourceAuthorize(ResourcesTypes.User, ActionsTypes.User.setUserTenants)]
        public async Task<IActionResult> SetUserTenants(SetUserTenants model)
        {
            foreach (var t in model.Tenants)
            {
                await _ManagementServiceProvider.EnforceAccessibiltyAsync(t);
            }

            UserIdentifier userIdentifier = new UserIdentifier(model.UserGuid);

            await _ManagementServiceProvider.EnforceAccessibiltyAsync(userIdentifier);

            var result = await _UserTenantServiceProvider.SetUserTenantsForUser(userIdentifier, model.Tenants.ToArray());
            if (result)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }
    }

}
