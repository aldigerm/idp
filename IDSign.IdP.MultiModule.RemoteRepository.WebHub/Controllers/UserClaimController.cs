﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserClaim")]
    [ApiController]
    public class UserClaimController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserClaimServiceProvider _UserClaimServiceProvider;

        //Inject context instance here.
        public UserClaimController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserClaimServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserClaimServiceProvider = userServiceProvider;
        }

        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] UserClaimIdentifier userClaimTypeIdentifier)
        {
            UserClaimModel result = await _UserClaimServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode,projectCode);
            var result = await _UserClaimServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserClaim/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserClaimCreateModel model)
        {
            UserClaimIdentifier result = await _UserClaimServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserClaim/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserClaimUpdateModel model)
        {
            UserClaimIdentifier result = await _UserClaimServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // POST: api/UserGroup/SetUsers
        [HttpPost]
        [Route("SetUsers")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.UserClaim.setUsers)]
        public async Task<IActionResult> SetUsers(UserClaimSetUsersModel model)
        {
            var result = await _UserClaimServiceProvider.SetUsers(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserClaim/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserClaim, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserClaimIdentifier model)
        {
            var result = await _UserClaimServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
