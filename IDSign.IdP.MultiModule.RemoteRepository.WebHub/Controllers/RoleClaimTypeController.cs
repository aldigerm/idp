﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/RoleClaimType")]
    [ApiController]
    public class RoleClaimTypeController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IRoleClaimTypeServiceProvider _RoleClaimTypeServiceProvider;

        //Inject context instance here.
        public RoleClaimTypeController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IRoleClaimTypeServiceProvider userClaimTypeServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _RoleClaimTypeServiceProvider = userClaimTypeServiceProvider;
        }


        // GET: api/Role/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] RoleClaimTypeIdentifier userClaimTypeIdentifier)
        {
            RoleClaimTypeModel result = await _RoleClaimTypeServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Role/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null)
        {
            ProjectIdentifier projectIdentifier = new ProjectIdentifier(projectCode);
            List<RoleClaimTypeModel> result = await _RoleClaimTypeServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Role/SetRoles
        [HttpPost]
        [Route("SetRoles")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.RoleClaimType.setRoles)]
        public async Task<IActionResult> SetRoles(RoleClaimTypeSetRolesModel model)
        {
            var result = await _RoleClaimTypeServiceProvider.SetRoles(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/RoleClaimType/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(RoleClaimTypeCreateModel model)
        {
            RoleClaimTypeIdentifier result = await _RoleClaimTypeServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/RoleClaimType/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(RoleClaimTypeUpdateModel model)
        {
            var result = await _RoleClaimTypeServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/RoleClaimType/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.RoleClaimType, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] RoleClaimTypeIdentifier model)
        {
            var result = await _RoleClaimTypeServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
