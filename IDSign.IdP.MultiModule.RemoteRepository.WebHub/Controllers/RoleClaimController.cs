﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/RoleClaim")]
    [ApiController]
    public class RoleClaimController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IRoleClaimServiceProvider _RoleClaimServiceProvider;

        //Inject context instance here.
        public RoleClaimController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IRoleClaimServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _RoleClaimServiceProvider = userServiceProvider;
        }

        // GET: api/Role/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] RoleClaimIdentifier userClaimTypeIdentifier)
        {
            RoleClaimModel result = await _RoleClaimServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/Role/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode,projectCode);
            var result = await _RoleClaimServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/RoleClaim/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(RoleClaimCreateModel model)
        {
            RoleClaimIdentifier result = await _RoleClaimServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/RoleClaim/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(RoleClaimUpdateModel model)
        {
            RoleClaimIdentifier result = await _RoleClaimServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // POST: api/Role/SetRoles
        [HttpPost]
        [Route("SetRoles")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.RoleClaim.setRoles)]
        public async Task<IActionResult> SetRoles(RoleClaimSetRolesModel model)
        {
            var result = await _RoleClaimServiceProvider.SetRoles(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/RoleClaim/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.RoleClaim, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] RoleClaimIdentifier model)
        {
            var result = await _RoleClaimServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
