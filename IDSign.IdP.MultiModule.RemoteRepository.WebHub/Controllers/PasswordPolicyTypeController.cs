﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/PasswordPolicyType")]
    [ApiController]
    public class PasswordPolicyTypeController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IPasswordPolicyTypeServiceProvider _PasswordPolicyTypeServiceProvider;

        //Inject context instance here.
        public PasswordPolicyTypeController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IPasswordPolicyTypeServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _PasswordPolicyTypeServiceProvider = userServiceProvider;
        }

        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyType, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] PasswordPolicyTypeIdentifier userClaimTypeIdentifier)
        {
            PasswordPolicyTypeModel result = await _PasswordPolicyTypeServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyType, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode, projectCode);
            var result = await _PasswordPolicyTypeServiceProvider.GetListAsync();
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/PasswordPolicyType/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyType, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(PasswordPolicyTypeCreateModel model)
        {
            PasswordPolicyTypeIdentifier result = await _PasswordPolicyTypeServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/PasswordPolicyType/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyType, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(PasswordPolicyTypeUpdateModel model)
        {
            PasswordPolicyTypeIdentifier result = await _PasswordPolicyTypeServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/PasswordPolicyType/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyType, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] PasswordPolicyTypeIdentifier model)
        {
            var result = await _PasswordPolicyTypeServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

    }

}
