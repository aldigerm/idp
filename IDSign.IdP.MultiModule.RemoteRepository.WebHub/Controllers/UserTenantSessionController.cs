﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserTenantSession")]
    [ApiController]
    public class UserTenantSessionController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserTenantSessionServiceProvider _UserTenantSessionServiceProvider;
        private readonly IUserTenantSessionActionServiceProvider _UserTenantSessionActionServiceProvider;

        //Inject context instance here.
        public UserTenantSessionController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserTenantSessionServiceProvider userServiceProvider
            , IUserTenantSessionActionServiceProvider userTenantSessionActionServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserTenantSessionServiceProvider = userServiceProvider;
            _UserTenantSessionActionServiceProvider = userTenantSessionActionServiceProvider;
        }

        // POST: api/UserTenantSession/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserTenantSession, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserTenantSessionModel model)
        {
            var result = await _UserTenantSessionServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // Get: api/UserTenantSession/Detail
        [HttpGet]
        [Route("Detail")]
        [AllowAnonymous]
        public async Task<IActionResult> Details(string identifier)
        {
            var result = await _UserTenantSessionActionServiceProvider.GetAsync(identifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserTenantSession/Action
        [HttpPost]
        [Route("Action")]
        [AllowAnonymous]
        public async Task<IActionResult> Action(UserTenantSessionActionRequestModel model)
        {
            var result = await _UserTenantSessionActionServiceProvider.ActionAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserTenantSession/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserTenantSession, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserTenantSessionModel model)
        {
            var result = await _UserTenantSessionServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserTenantSession/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserTenantSession, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] string identifier)
        {
            var result = await _UserTenantSessionServiceProvider.DeleteAsync(identifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

    }

}
