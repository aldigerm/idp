﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/TwoFAController")]
    [ApiController]
    public class TwoFAController : BaseRemoteRepositoryController
    {
        private readonly ITemporaryUserServiceProvider _TemporaryUserServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public TwoFAController(
             ITemporaryUserServiceProvider temporaryUserServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _TemporaryUserServiceProvider = temporaryUserServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // POST: api/UserTenantSession/Create
        [HttpPost]
        [Route("SubmitOTP")]
        [ResourceAuthorize(ResourcesTypes.TwoFAController, ActionsTypes.TwoFAController.validate)]
        public async Task<IActionResult> SubmitOTP(OTPTemporaryUserLoginRequestModel model)
        {

            var result = await _TemporaryUserServiceProvider.SubmitOTPAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException("Couldn't create Temporary User Access");
            }
        }

        // POST: api/UserTenantSession/Create
        [HttpPost]
        [Route("RequestDelivery")]
        [ResourceAuthorize(ResourcesTypes.TwoFAController, ActionsTypes.TwoFAController.sendNew)]
        public async Task<IActionResult> RequestDelivery(BaseTemporaryUserLoginRequestModel model)
        {

            var result = await _TemporaryUserServiceProvider.RequestDeliveryOTPValueDAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException("Couldn't create Temporary User Access");
            }
        }

    }

}
