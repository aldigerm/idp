﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/Project")]
    [ApiController]
    public class ProjectController : BaseRemoteRepositoryController
    {
        private readonly IMapper _mapper;
        private readonly IProjectServiceProvider _ProjectServiceProvider;
        private readonly IMultiTenantServiceProvider _MultiTenantServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public ProjectController(
              IMapper mapper
            , IProjectServiceProvider projectServiceProvider
            , IMultiTenantServiceProvider multiTenantServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _mapper = mapper;
            _ProjectServiceProvider = projectServiceProvider;
            _MultiTenantServiceProvider = multiTenantServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // GET: api/Project
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get()
        {
            var result = await _ProjectServiceProvider.GetLoggedInProjectAsync();
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Project/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] ProjectIdentifier projectIdentifier)
        {
            var result = await _ProjectServiceProvider.GetAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Project/Detail
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.get)]
        public async Task<IActionResult> List()
        {
            var result = await _ProjectServiceProvider.GetListAsync();
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Project/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(ProjectCreateModel model)
        {
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.GetProjectIdentifier());

            ProjectIdentifier result = await _MultiTenantServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Project/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(ProjectUpdateViewModel model)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            var result = await _ProjectServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Tenant/Update
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.Project, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] ProjectIdentifier projectIdentifier)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(projectIdentifier);

            _ManagementServiceProvider.EnforceNotChangeSupportAsync(projectIdentifier);

            int result = await _MultiTenantServiceProvider.DeleteAsync(projectIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }
    }

}
