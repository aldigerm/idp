﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/PasswordPolicy")]
    [ApiController]
    public class PasswordPolicyController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IPasswordPolicyServiceProvider _PasswordPolicyServiceProvider;

        //Inject context instance here.
        public PasswordPolicyController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IPasswordPolicyServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _PasswordPolicyServiceProvider = userServiceProvider;
        }

        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicy, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] PasswordPolicyIdentifier userClaimTypeIdentifier)
        {
            PasswordPolicyModel result = await _PasswordPolicyServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicy, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode, projectCode);
            var result = await _PasswordPolicyServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/PasswordPolicy/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicy, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(PasswordPolicyCreateModel model)
        {
            PasswordPolicyIdentifier result = await _PasswordPolicyServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/PasswordPolicy/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicy, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(PasswordPolicyUpdateModel model)
        {
            PasswordPolicyIdentifier result = await _PasswordPolicyServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/PasswordPolicy/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicy, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] PasswordPolicyIdentifier model)
        {
            var result = await _PasswordPolicyServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

    }

}
