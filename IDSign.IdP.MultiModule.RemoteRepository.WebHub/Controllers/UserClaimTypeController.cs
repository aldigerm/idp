﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserClaimType")]
    [ApiController]
    public class UserClaimTypeController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserClaimTypeServiceProvider _UserClaimTypeServiceProvider;

        //Inject context instance here.
        public UserClaimTypeController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserClaimTypeServiceProvider userClaimTypeServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserClaimTypeServiceProvider = userClaimTypeServiceProvider;
        }


        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.UserClaimType, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] UserClaimTypeIdentifier userClaimTypeIdentifier)
        {
            UserClaimTypeModel result = await _UserClaimTypeServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.UserClaimType, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null)
        {
            ProjectIdentifier projectIdentifier = new ProjectIdentifier(projectCode);
            List<UserClaimTypeModel> result = await _UserClaimTypeServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/SetUsers
        [HttpPost]
        [Route("SetUsers")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.UserGroup.setUsers)]
        public async Task<IActionResult> SetUsers(UserClaimTypeSetUsersModel model)
        {
            var result = await _UserClaimTypeServiceProvider.SetUsers(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserClaimType/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserClaimType, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserClaimTypeCreateModel model)
        {
            UserClaimTypeIdentifier result = await _UserClaimTypeServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserClaimType/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserClaimType, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserClaimTypeUpdateModel model)
        {
            var result = await _UserClaimTypeServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserClaimType/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserClaimType, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserClaimTypeIdentifier model)
        {
            var result = await _UserClaimTypeServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
