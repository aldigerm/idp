﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/PasswordPolicyUserGroup")]
    [ApiController]
    public class PasswordPolicyUserGroupController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IPasswordPolicyUserGroupServiceProvider _PasswordPolicyUserGroupServiceProvider;

        //Inject context instance here.
        public PasswordPolicyUserGroupController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IPasswordPolicyUserGroupServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _PasswordPolicyUserGroupServiceProvider = userServiceProvider;
        }

        // GET: api/Project/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyUserGroup, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] PasswordPolicyUserGroupIdentifier identifier)
        {
            var result = await _PasswordPolicyUserGroupServiceProvider.GetAsync(identifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/User/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.PasswordPolicyUserGroup, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(PasswordPolicyUserGroupModel model)
        {
            var result = await _PasswordPolicyUserGroupServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

    }

}
