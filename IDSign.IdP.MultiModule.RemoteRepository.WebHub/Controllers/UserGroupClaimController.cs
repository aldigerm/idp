﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserGroupClaim")]
    [ApiController]
    public class UserGroupClaimController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserGroupClaimServiceProvider _UserGroupClaimServiceProvider;

        //Inject context instance here.
        public UserGroupClaimController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserGroupClaimServiceProvider userServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserGroupClaimServiceProvider = userServiceProvider;
        }

        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] UserGroupClaimIdentifier userClaimTypeIdentifier)
        {
            UserGroupClaimModel result = await _UserGroupClaimServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }


        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null, string tenantCode = null)
        {
            TenantIdentifier projectIdentifier = new TenantIdentifier(tenantCode,projectCode);
            var result = await _UserGroupClaimServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroupClaim/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserGroupClaimCreateModel model)
        {
            UserGroupClaimIdentifier result = await _UserGroupClaimServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroupClaim/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserGroupClaimUpdateModel model)
        {
            UserGroupClaimIdentifier result = await _UserGroupClaimServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // POST: api/UserGroup/SetUserGroups
        [HttpPost]
        [Route("SetUserGroups")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.UserGroupClaim.setUserGroups)]
        public async Task<IActionResult> SetUserGroups(UserGroupClaimSetUserGroupsModel model)
        {
            var result = await _UserGroupClaimServiceProvider.SetUserGroups(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserGroupClaim/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaim, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserGroupClaimIdentifier model)
        {
            var result = await _UserGroupClaimServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
