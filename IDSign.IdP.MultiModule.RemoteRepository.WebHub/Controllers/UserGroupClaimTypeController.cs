﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserGroupClaimType")]
    [ApiController]
    public class UserGroupClaimTypeController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserGroupClaimTypeServiceProvider _UserGroupClaimTypeServiceProvider;

        //Inject context instance here.
        public UserGroupClaimTypeController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserGroupClaimTypeServiceProvider userClaimTypeServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserGroupClaimTypeServiceProvider = userClaimTypeServiceProvider;
        }


        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] UserGroupClaimTypeIdentifier userClaimTypeIdentifier)
        {
            UserGroupClaimTypeModel result = await _UserGroupClaimTypeServiceProvider.GetViewModelAsync(userClaimTypeIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string projectCode = null)
        {
            ProjectIdentifier projectIdentifier = new ProjectIdentifier(projectCode);
            List<UserGroupClaimTypeModel> result = await _UserGroupClaimTypeServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/SetUserGroups
        [HttpPost]
        [Route("SetUserGroups")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.UserGroupClaimType.setUserGroups)]
        public async Task<IActionResult> SetUserGroups(UserGroupClaimTypeSetUserGroupsModel model)
        {
            var result = await _UserGroupClaimTypeServiceProvider.SetUserGroups(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroupClaimType/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserGroupClaimTypeCreateModel model)
        {
            UserGroupClaimTypeIdentifier result = await _UserGroupClaimTypeServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroupClaimType/Create
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserGroupClaimTypeUpdateModel model)
        {
            var result = await _UserGroupClaimTypeServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // DELETE: api/UserGroupClaimType/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserGroupClaimType, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserGroupClaimTypeIdentifier model)
        {
            var result = await _UserGroupClaimTypeServiceProvider.DeleteAsync(model);
            if (result)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }        

    }

}
