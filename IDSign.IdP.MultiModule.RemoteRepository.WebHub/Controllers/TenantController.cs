﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/Tenant")]
    [ApiController]
    public class TenantController : BaseRemoteRepositoryController
    {
        private readonly IMapper _mapper;
        private readonly ITenantServiceProvider _TenantServiceProvider;
        private readonly IMultiTenantServiceProvider _MultiTenantServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;

        //Inject context instance here.
        public TenantController(
              IMapper mapper
            , ITenantServiceProvider tenantServiceProvider
            , IMultiTenantServiceProvider multiTenantServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _mapper = mapper;
            _TenantServiceProvider = tenantServiceProvider;
            _MultiTenantServiceProvider = multiTenantServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // GET: api/Tenant
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get()
        {
            TenantViewModel result = await _TenantServiceProvider.GetLoggedInTenantAsync();
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // GET: api/Tenant/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] TenantIdentifier tenantIdentifier, [FromQuery] bool includeTemporaryUsers = false)
        {
            TenantViewModel result = await _TenantServiceProvider.GetViewModelAsync(tenantIdentifier, includeTemporaryUsers);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // GET: api/Tenant/Detail
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.get)]
        public async Task<IActionResult> List([FromQuery] ProjectIdentifier projectIdentifier)
        {
            IList<TenantViewModel> result = await _TenantServiceProvider.GetListAsync(projectIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Tenant/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(TenantCreateModel model)
        {
            // we do not allow the support project to be created
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.GetTenantIdentifier());

            TenantIdentifier result = await _MultiTenantServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Tenant/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(TenantUpdateViewModel model)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            TenantIdentifier result = await _TenantServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/Tenant/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.Tenant, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] TenantIdentifier tenantIdentifier)
        {
            await _ManagementServiceProvider.EnforceNotRelatedAsync(tenantIdentifier);

            int result = await _MultiTenantServiceProvider.DeleteAsync(tenantIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

    }

}
