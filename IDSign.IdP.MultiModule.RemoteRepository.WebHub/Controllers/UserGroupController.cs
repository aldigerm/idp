﻿using AutoMapper;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.DTO;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using AutoMapper;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Controllers
{
    [Route("api/UserGroup")]
    [ApiController]
    public class UserGroupController : BaseRemoteRepositoryController
    {
        private readonly RemoteRepositoryDbContext ctx;
        private readonly IMapper _mapper;
        private readonly IUserGroupServiceProvider _UserGroupServiceProvider;
        private readonly IManagementServiceProvider _ManagementServiceProvider;
        private readonly IUserSharedServiceProvider _UserSharedServiceProvider;

        //Inject context instance here.
        public UserGroupController(
              RemoteRepositoryDbContext ctx
            , IMapper mapper
            , IUserGroupServiceProvider userGroupServiceProvider
            , IUserSharedServiceProvider userSharedServiceProvider
            , IManagementServiceProvider managementServiceProvider)
        {
            _mapper = mapper;
            this.ctx = ctx;
            _UserGroupServiceProvider = userGroupServiceProvider;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _ManagementServiceProvider = managementServiceProvider;
        }

        // GET: api/UserGroup/Get
        [HttpGet]
        [Route("Get")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.get)]
        public async Task<IActionResult> Get()
        {
            IList<UserGroupViewModel> model = await _UserGroupServiceProvider.GetLoggedInUserGroupsAsync();
            return Ok(model);
        }

        // GET: api/UserGroup/Detail
        [HttpGet]
        [Route("Detail")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.detail)]
        public async Task<IActionResult> Details([FromQuery] UserGroupIdentifier userGroupIdentifier)
        {
            await _ManagementServiceProvider.EnforceAccessibilty(userGroupIdentifier);

            UserGroupViewModel result = await _UserGroupServiceProvider.GetViewModelAsync(userGroupIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/UserGroup/List
        [HttpGet]
        [Route("List")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.list)]
        public async Task<IActionResult> List(string tenantCode = null, string projectCode = null)
        {
            TenantIdentifier tenantIdentifier = new TenantIdentifier(tenantCode, projectCode);
            List<UserGroupViewModel> result = await _UserGroupServiceProvider.GetAccessibleListAsync(tenantIdentifier);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/Create
        [HttpPost]
        [Route("Create")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.create)]
        public async Task<IActionResult> Create(UserGroupCreateModel model)
        {
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(new UserGroupIdentifier(model.UserGroupCode, model.TenantIdentifier));

            UserGroupIdentifier result = await _UserGroupServiceProvider.CreateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/SetInheritingUserGroups
        [HttpPost]
        [Route("SetInheritingUserGroups")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.UserGroup.setInheritingUserGroups)]
        public async Task<IActionResult> SetInheritingUserGroups(UserGroupSetInheritanceModel model)
        {
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.Identifier);

            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);


            foreach (var userGroupCode in model.InheritsFromUserGroups)
            {
                await _ManagementServiceProvider.EnforceAccessibilty(new UserGroupIdentifier(userGroupCode, model.Identifier.GetTenantIdentifier()));

                await _ManagementServiceProvider.EnforceNotRelatedAsync(new UserGroupIdentifier(userGroupCode, model.Identifier.GetTenantIdentifier()));
            }

            UserGroupViewModel result = await _UserGroupServiceProvider.SetInheritingUserGroups(model.Identifier, model.InheritsFromUserGroups);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/SetUsers
        [HttpPost]
        [Route("SetUsers")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.UserGroup.setUsers)]
        public async Task<IActionResult> SetUsers(UserGroupSetUsersModel model)
        {

            var users = await _UserSharedServiceProvider.GetAccessibleListAsync(model.Identifier.GetTenantIdentifier());
            var accessibleUsers = users.Where(u => u.Identifier.GetTenantIdentifier().IsEquals(model.Identifier.GetTenantIdentifier())).Select(u => u.Identifier.Username).ToList();

            foreach (var username in model.Usernames)
            {
                var userIdentifier = new UserIdentifier(username, model.Identifier.GetTenantIdentifier());

                // the user cannot add himself
                _ManagementServiceProvider.EnforceNotSameUser(userIdentifier);

                // only accessible users are processed
                await _ManagementServiceProvider.EnforceAccessibiltyAsync(userIdentifier);
            }

            // the group must be under the user's visibility
            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            UserGroupViewModel result = await _UserGroupServiceProvider.SetUsers(model.Identifier, model.Usernames, accessibleUsers);

            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // POST: api/UserGroup/SetPasswordPolicies
        [HttpPost]
        [Route("SetPasswordPolicies")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.UserGroup.setPasswordPolicies)]
        public async Task<IActionResult> SetPasswordPolicies(UserGroupSetPasswordPoliciesModel model)
        {
            _ManagementServiceProvider.EnforceNotChangeSupportAsync(model.Identifier);

            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            UserGroupViewModel result = await _UserGroupServiceProvider.SetPasswordPolicies(model.Identifier, model.PasswordPolicies);
            if (result != null)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }

        // Post: api/UserGroup/Update
        [HttpPost]
        [Route("Update")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.update)]
        public async Task<IActionResult> Update(UserGroupUpdateViewModel model)
        {
            await _ManagementServiceProvider.EnforceAccessibilty(model.Identifier);

            await _ManagementServiceProvider.EnforceNotRelatedAsync(model.Identifier);

            UserGroupIdentifier result = await _UserGroupServiceProvider.UpdateAsync(model);
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                throw new IdPException();
            }
        }


        // DELETE: api/UserGroup/Delete
        [HttpDelete]
        [Route("Delete")]
        [ResourceAuthorize(ResourcesTypes.UserGroup, ActionsTypes.Common.delete)]
        public async Task<IActionResult> Delete([FromQuery] UserGroupIdentifier userGroupIdentifier)
        {
            await _ManagementServiceProvider.EnforceAccessibilty(userGroupIdentifier);

            int result = await _UserGroupServiceProvider.DeleteAsync(userGroupIdentifier);
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                throw new IdPException();
            }
        }
    }

}
