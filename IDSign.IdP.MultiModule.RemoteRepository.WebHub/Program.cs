﻿using IDSign.IdP.Core;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.IO;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // build host
            var host = BuildWebHost(args);

            // do initial logic
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {

                    // initialise database
                    var ctx = services.GetRequiredService<RemoteRepositoryDbContext>();
                    var initialiseDatabase = services.GetRequiredService<ILogger<RemoteRepositoryDbContextInitializer>>();
                    RemoteRepositoryDbContextInitializer.InitializeDatabase(ctx, initialiseDatabase);

                    //initialise logging
                    var ctxLog = services.GetRequiredService<LoggingModelContext>();
                    var loggerInitialiseDatabase = services.GetRequiredService<ILogger<LoggingModelDbContextInitializer>>();
                    LoggingModelDbContextInitializer.InitializeDatabase(ctxLog, loggerInitialiseDatabase);

                    var multitenantServiceProvider = services.GetRequiredService<IMultiTenantServiceProvider>();
                    var permissionsServiceProvider = services.GetRequiredService<IPermissionsServiceProvider>();
                    var userServiceProvider = services.GetRequiredService<IUserServiceProvider>();


                    if (FeatureHelper.LoadSupportEnabled)
                    {
                        // initialise default tenants
                        AsyncHelpers.RunSync(() => multitenantServiceProvider.InitSupportAsync());
                        // initialise permissions
                        AsyncHelpers.RunSync(() => permissionsServiceProvider.InitPermissionsSupportAsync());
                        // initialise default tenants
                        AsyncHelpers.RunSync(() => userServiceProvider.InitSupportAsync());
                    }

                    if (FeatureHelper.SeedMultiTenantsEnabled)
                    {
                        // initialise default tenants
                        AsyncHelpers.RunSync(() => multitenantServiceProvider.InitAsync());
                    }

                    if (FeatureHelper.LoadAccessModelsEnabled)
                    {
                        // initialise permissions
                        AsyncHelpers.RunSync(() => permissionsServiceProvider.InitPermissionsAsync());
                    }

                    if (FeatureHelper.SeedUsersEnabled)
                    {
                        // initialise default tenants
                        AsyncHelpers.RunSync(() => userServiceProvider.InitAsync());
                    }

                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred creating the DB.");
                }
            }

            // start webapi
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                 .CaptureStartupErrors(true)
                 .UseKestrel()
                 .UseContentRoot(Directory.GetCurrentDirectory())
                 .ConfigureAppConfiguration((hostingContext, config) =>
                 {
                     var env = hostingContext.HostingEnvironment;
                     config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                     config.AddEnvironmentVariables();

                     var builtConfig = config.Build();

                     Log.Logger = new LoggerConfiguration()
                     .ReadFrom.Configuration(builtConfig)
                     .CreateLogger();

                 })
                 .ConfigureLogging((hostingContext, logging) =>
                 {
                     logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                     logging.AddConsole();
                     logging.AddDebug();
                     logging.AddSerilog();
                 })
                 .UseStartup<Startup>()
                 .Build();
        }
    }
}
