﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization
{

    public class ResourceAuthorizeAttribute : AuthorizeAttribute
    {
        public const string delimeter = "_";
        public const string POLICY_PREFIX = "ResourceAuthorizations";
        public ResourcesTypes Resource { get; private set; }
        public string Action { get; private set; }

        public ResourceAuthorizeAttribute(ResourcesTypes resource, string action)
        {
            Resource = resource;
            Action = action;
            Policy = BuildPolicyName(resource, action);
        }

        public static string BuildPolicyName(ResourcesTypes resource, string action)
        {
            return POLICY_PREFIX + delimeter + (int)resource + delimeter + action;
        }
        public static bool GetResource(string policyName, out ResourcesTypes resource)
        {
            resource = ResourcesTypes.AllowAll;
            var split = policyName.Split(delimeter);
            if (split.Length > 1 && int.TryParse(split[1], out int val))
            {
                resource = (ResourcesTypes)val;
                return true;
            }
            return false;
        }
        public static bool GetAction(string policyName, out string action)
        {
            action = ActionsTypes.AllowAll;
            var split = policyName.Split(delimeter);
            if (split.Length > 2)
            {
                action = split[2];
                return true;
            }
            return false;
        }
    }
    public class ResourcePolicyProvider : IAuthorizationPolicyProvider
    {
        const string POLICY_PREFIX = ResourceAuthorizeAttribute.POLICY_PREFIX;
        string POLICY_ALLOW_ALL = "";

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
        {
            POLICY_ALLOW_ALL = ResourceAuthorizeAttribute.BuildPolicyName(ResourcesTypes.AllowAll, ActionsTypes.AllowAll);
            return GetPolicyAsync(POLICY_ALLOW_ALL);
        }

        // Policies are looked up by string name, so expect 'parameters' (like age)
        // to be embedded in the policy names. This is abstracted away from developers
        // by the more strongly-typed attributes derived from AuthorizeAttribute
        // (like [MinimumAgeAuthorize()] in this sample)
        public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (policyName.StartsWith(POLICY_PREFIX, StringComparison.OrdinalIgnoreCase) &&
               ResourceAuthorizeAttribute.GetResource(policyName, out ResourcesTypes resource)
               && ResourceAuthorizeAttribute.GetAction(policyName, out string action))
            {
                var policy = new AuthorizationPolicyBuilder();
                if (policyName == POLICY_ALLOW_ALL)
                {
                    policy.AddRequirements(new ResourceRequirement());
                }
                else
                {
                    policy.AddRequirements(new ResourceRequirement(resource, action));
                }
                return Task.FromResult(policy.Build());
            }

            return Task.FromResult<AuthorizationPolicy>(null);
        }
    }
    public class ResourceRequirement : IAuthorizationRequirement
    {
        public ResourcesTypes Resource { get; private set; }
        public string Action { get; private set; }
        public bool AllowAll { get; private set; }
        public ResourceRequirement() { AllowAll = true; }
        public ResourceRequirement(ResourcesTypes resource, string action) { Resource = (ResourcesTypes)resource; Action = action; }
    }


    // This class contains logic for determining whether MinimumAgeRequirements in authorizaiton
    // policies are satisfied or not
    public class ResourceAuthorizationHandler : AuthorizationHandler<ResourceRequirement>
    {
        private readonly ILogger<ResourceAuthorizationHandler> _logger;
        private AppSettings _Settings;
        private IHttpContextAccessor _HttpContextAccessor;

        public ResourceAuthorizationHandler(
            ILogger<ResourceAuthorizationHandler> logger
            , IHttpContextAccessor httpContextAccessor
            , AppSettings settings)
        {
            _HttpContextAccessor = httpContextAccessor;
            _Settings = settings;
            _logger = logger;
        }

        // Check whether a given MinimumAgeRequirement is satisfied or not for a particular context
        protected async override Task HandleRequirementAsync(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            if (requirement.AllowAll)
            {
                context.Succeed(requirement);
                await Task.CompletedTask;
                return;
            }


            // Log as a warning so that it's very clear in sample output which authorization policies 
            // (and requirements/handlers) are in use
            _logger.LogInformation("Evaluating authorization requirement for resource/action '{0}'/'{1}'", requirement.Resource.ToString(), requirement.Action);
            if (AuthorizeResource(context, requirement))
            {
                _logger.LogInformation("Authorized");
                context.Succeed(requirement);
            }
            else
            {
                _logger.LogWarning("Not authorized.");
            }

            await Task.CompletedTask;
            return;
        }

        private bool AuthorizeResource(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Resource)
            {
                case ResourcesTypes.User:
                    return AuthorizeUser(context, requirement);
                case ResourcesTypes.Project:
                    return AuthorizeProject(context, requirement);
                case ResourcesTypes.Module:
                    return AuthorizeModule(context, requirement);
                case ResourcesTypes.Role:
                    return AuthorizeRole(context, requirement);
                case ResourcesTypes.Tenant:
                    return AuthorizeTenant(context, requirement);
                case ResourcesTypes.UserGroup:
                    return AuthorizeUserGroup(context, requirement);
                case ResourcesTypes.UserClaim:
                    return AuthorizeUserClaim(context, requirement);
                case ResourcesTypes.UserClaimType:
                    return AuthorizeUserClaimType(context, requirement);
                case ResourcesTypes.UserGroupClaim:
                    return AuthorizeUserGroupClaim(context, requirement);
                case ResourcesTypes.UserGroupClaimType:
                    return AuthorizeUserGroupClaimType(context, requirement);
                case ResourcesTypes.RoleClaim:
                    return AuthorizeRoleClaim(context, requirement);
                case ResourcesTypes.RoleClaimType:
                    return AuthorizeRoleClaimType(context, requirement);
                case ResourcesTypes.TenantClaim:
                    return AuthorizeTenantClaim(context, requirement);
                case ResourcesTypes.TenantClaimType:
                    return AuthorizeTenantClaimType(context, requirement);
                case ResourcesTypes.ClientError:
                    return AuthorizeClientError(context, requirement);
                case ResourcesTypes.PasswordPolicy:
                    return AuthorizePasswordPolicy(context, requirement);
                case ResourcesTypes.PasswordPolicyType:
                    return AuthorizePasswordPolicyType(context, requirement);
                case ResourcesTypes.UserTenantSession:
                    return AuthorizeUserTenantSession(context, requirement);
                case ResourcesTypes.TemporaryUser:
                    return AuthorizeTemporaryUser(context, requirement);
                case ResourcesTypes.TwoFAController:
                    return AuthorizeTwoFAController(context, requirement);
                default:
                    return false;
            }
        }

        private bool AuthorizeUser(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.detail:
                case ActionsTypes.User.upload:
                case ActionsTypes.User.login:
                case ActionsTypes.User.profile:
                    return HasAdminScope(context) || _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.update:
                case ActionsTypes.User.setPassword:
                    return HasAdminScope(context) || _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.create:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.list:
                case ActionsTypes.User.setUserGroups:
                case ActionsTypes.User.setSecuritySettings:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN);
                case ActionsTypes.User.setManagedUserGroups:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN);
                case ActionsTypes.User.link:
                case ActionsTypes.User.unlink:
                case ActionsTypes.User.setUserTenants:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN);
                default:
                    return false;
            }
        }
        private bool AuthorizeProject(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.list:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN);
                case ActionsTypes.Common.create:
                case ActionsTypes.Common.delete:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }
        private bool AuthorizeModule(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.create:
                case ActionsTypes.Common.update:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeRole(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.create:
                case ActionsTypes.Common.update:
                case ActionsTypes.Role.setInheritingRoles:
                case ActionsTypes.Role.setModule:
                case ActionsTypes.Role.setUserGroups:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }
        private bool AuthorizeTenant(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.detail:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }
        private bool AuthorizeUserGroup(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.loggedIn:
                    return _HttpContextAccessor.IsUser();
                case ActionsTypes.Common.get:
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.UserGroup.setInheritingUserGroups:
                case ActionsTypes.UserGroup.setUsers:
                case ActionsTypes.UserGroup.setPasswordPolicies:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                case ActionsTypes.UserGroup.setUserGroups:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                case ActionsTypes.Common.delete:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeUserClaim(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.UserClaim.setUsers:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_USER_MANAGEMENT, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeUserClaimType(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizePasswordPolicy(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_USER_MANAGEMENT);
                default:
                    return false;
            }
        }

        private bool AuthorizePasswordPolicyType(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeTemporaryUser(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.User.login:
                    return HasAdminScope(context);
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_USER_MANAGEMENT);
                default:
                    return false;
            }
        }
        private bool AuthorizeTwoFAController(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.TwoFAController.sendNew: 
                case ActionsTypes.TwoFAController.validate:
                    return HasAdminScope(context);
                default:
                    return false;
            }
        }
        private bool AuthorizeUserTenantSession(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.UserTenantSession.action:
                    return true; // any user can perform an action on a session
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizePasswordPolicyUserGroup(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.PasswordPolicyUserGroup.add:
                case ActionsTypes.PasswordPolicyUserGroup.delete:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_USER_MANAGEMENT);
                default:
                    return false;
            }
        }

        private bool AuthorizeUserGroupClaim(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.UserGroupClaim.setUserGroups:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeUserGroupClaimType(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.UserGroupClaimType.setUserGroups:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeRoleClaim(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.RoleClaim.setRoles:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeRoleClaimType(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.RoleClaimType.setRoles:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeTenantClaim(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.TenantClaim.setTenants:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_TENANT_ADMIN, ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }

        private bool AuthorizeTenantClaimType(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.Common.detail:
                case ActionsTypes.Common.list:
                case ActionsTypes.Common.update:
                case ActionsTypes.Common.delete:
                case ActionsTypes.Common.create:
                case ActionsTypes.TenantClaimType.setTenants:
                    return HasAdminScope(context) || HasAllowedModules(ModuleCode.REPOSITORY_PROFILE_PROJECT_ADMIN, ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN);
                default:
                    return false;
            }
        }
        private bool AuthorizeClientError(AuthorizationHandlerContext context, ResourceRequirement requirement)
        {
            switch (requirement.Action)
            {
                case ActionsTypes.ClientError.log:
                    return true;
                default:
                    return false;
            }
        }
        private bool HasAllowedModules(params ModuleCode[] moduleCodes)
        {
            return (_HttpContextAccessor.HasAllowedModule(SupportConstants.SupportModuleCode) || _HttpContextAccessor.HasAnyAllowedModules(moduleCodes));
        }


        private bool HasAdminScope(AuthorizationHandlerContext context)
        {
            return context.User.HasAdminScope();
        }
    }


    public enum ResourcesTypes
    {
        AllowAll = -1,
        User = 0,
        Tenant = 1,
        Project = 2,
        Module = 3,
        Role = 4,
        UserGroup = 5,
        UserClaim = 6,
        UserClaimType = 7,
        UserGroupClaim = 8,
        UserGroupClaimType = 9,
        RoleClaim = 10,
        RoleClaimType = 11,
        ClientError = 12,
        TenantClaim = 13,
        TenantClaimType = 14,
        PasswordPolicy = 15,
        PasswordPolicyType = 16,
        PasswordPolicyUserGroup = 17,
        UserTenantSession = 18,
        TemporaryUser = 19,
        TwoFAController = 20
    }
    public static class ActionsTypes
    {
        public const string AllowAll = "allowall";
        public class Common
        {
            public const string loggedIn = "loggedIn";
            public const string get = "get";
            public const string detail = "detail";
            public const string delete = "delete";
            public const string list = "list";
            public const string create = "create";
            public const string update = "update";
        }
        public class User : Common
        {
            public const string upload = "upload";
            public const string login = "login";
            public const string setUserGroups = "setUserGroups";
            public const string setPassword = "setPassword";
            public const string profile = "profile";
            public const string setManagedUserGroups = "setManagedUserGroups";
            public const string unlink = "unlink";
            public const string link = "link";
            public const string setUserTenants = "setUserTenants";
            public const string setSecuritySettings = "setSecuritySettings";
        }
        public class Role : Common
        {
            public const string setModule = "setModule";
            public const string setInheritingRoles = "setInheritingRoles";
            public const string setUserGroups = "setUserGroups";
        }
        public class UserGroup : Common
        {
            public const string setInheritingUserGroups = "setInheritingUserGroups";
            public const string setUsers = "setUsers";
            public const string setUserGroups = "setUserGroups";
            public const string setPasswordPolicies = "setPasswordPolicies";
        }
        public class UserClaim : Common
        {
            public const string setUsers = "setUsers";
        }
        public class UserGroupClaim : Common
        {
            public const string setUserGroups = "setGroups";
        }
        public class UserGroupClaimType : Common
        {
            public const string setUserGroups = "setGroups";
        }
        public class RoleClaim : Common
        {
            public const string setRoles = "setRoles";
        }
        public class RoleClaimType : Common
        {
            public const string setRoles = "setRoles";
        }
        public class ClientError : Common
        {
            public const string log = "log";
        }
        public class TenantClaim : Common
        {
            public const string setTenants = "setTenants";
        }
        public class TenantClaimType : Common
        {
            public const string setTenants = "setTenants";
        }

        public class PasswordPolicyUserGroup
        {
            public const string add = "add";
            public const string delete = "delete";
        }

        public class UserTenantSession : Common
        {
            public const string action = "action";
        }

        public class TwoFAController
        {
            public const string validate = "validate";
            public const string sendNew = "sendnew";
        }
    }
}
