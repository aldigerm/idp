﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization
{

    public class RemoteRepositoryClaimsTransformer : IClaimsTransformation
    {
        private AppSettings _Settings;
        private IUserServiceProvider _UserServiceProvider;
        private readonly ILogger<RemoteRepositoryClaimsTransformer> _logger;
        public RemoteRepositoryClaimsTransformer(ILogger<RemoteRepositoryClaimsTransformer> logger
            , AppSettings settings
            , IUserServiceProvider userServiceProvider)
        {
            _Settings = settings;
            _UserServiceProvider = userServiceProvider;
            _logger = logger;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
           
            if (principal.FindFirst(ClaimName.InitialisedKey) != null)
            {
                _logger.LogDebug("Principal initialised.");
                return principal;
            }
            return await _UserServiceProvider.InitialiseUserFromPrincipal(principal);
        }
    }
}
