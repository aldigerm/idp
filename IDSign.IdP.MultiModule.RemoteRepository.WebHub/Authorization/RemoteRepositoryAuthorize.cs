﻿using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization
{
    public class RemoteRepositoryAuthorizeAttribute : TypeFilterAttribute
    {
        public RemoteRepositoryAuthorizeAttribute() : base(typeof(RemoteRepositoryAuthorizeFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class RemoteRepositoryAuthorizeFilter : IAuthorizationFilter
    {
        private IUserServiceProvider _UserServiceProvider;
        private ILogger<RemoteRepositoryAuthorizeAttribute> _logger;

        public RemoteRepositoryAuthorizeFilter(
             IUserServiceProvider userServiceProvider
            , ILogger<RemoteRepositoryAuthorizeAttribute> logger)
        {
            _UserServiceProvider = userServiceProvider;
            _logger = logger;
        }

        /// <summary>
        /// If it is an authenticated user or an api, then we let it through
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (SkipAuthorization(context))
            {
                _logger.LogDebug("Anonymous access is set on this action");
            }
            else
            {
                var repoUsername = context.HttpContext?.RepositoryUsername();
                if (repoUsername != null)
                {
                    _logger.LogDebug("User allowed access: '{0}'", repoUsername);
                    _logger.LogTrace("User allowed access has claims: '{0}'", string.Join(',', context.HttpContext?.User.Claims.Select(c => "'" + c.Type + "':'" + c.Value + "'")));
                }
                else if (context.HttpContext.User.HasAdminScope())
                {
                    _logger.LogDebug("User allowed access with admin scope");
                }
                else
                {
                    _logger.LogWarning("User or api not found. Denying access.");
                    context.Result = new UnauthorizedResult();
                }
            }
        }


        private static bool SkipAuthorization(AuthorizationFilterContext actionContext)
        {
            return actionContext.Filters.Any(f => f is AllowAnonymousFilter);
        }
    }
}
