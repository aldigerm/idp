﻿using IDSign.IdP.Core;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Middleware
{
    public static class GlobalErrorHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseGlobalErrorHandlerMiddleware(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<GlobalErrorHandlerMiddleware>();
        }
    }
    public class GlobalErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public GlobalErrorHandlerMiddleware(
            RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context
            , ILogger<GlobalErrorHandlerMiddleware> logger
            , IHostingEnvironment env)
        {
            var originalBodyStream = context.Response.Body;

            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                context.Features.Set<IExceptionHandlerFeature>(new ExceptionHandlerFeature() { Error = ex, Path = context.Request.Path });
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Response.ContentType = "application/json";
                string serialized = "";
                WebHubException messageObject = new WebHubException(ErrorCode.GENERIC_ERROR);
                if (env.IsDevelopment())
                {
                    messageObject.StackTrace = ex.Message + "\r\n" + ex.StackTrace.ToString();
                }
                if (ex is IdPDescriptiveException)
                {
                    var idpEx = ex as IdPDescriptiveException;
                    messageObject.ErrorCode = idpEx.ErrorCode.ToString();
                    messageObject.ExceptionData = idpEx.ExceptionData;
                    logger.LogError("Custom error thrown: {0} {1}", idpEx.ErrorCode.ToString(), idpEx.Message);
                }
                else if (ex is IdPException)
                {
                    var idpEx = ex as IdPException;
                    messageObject.ErrorCode = idpEx.ErrorCode.ToString();
                    serialized = HelpersService.DescribeObject(messageObject, false, false);
                    logger.LogError("Custom error thrown: {0} {1}", idpEx.ErrorCode.ToString(), idpEx.Message);
                }
                else
                {
                    logger.LogError(ex, "An unhandled exception has occured");
                }

                // we return a 404 specifically for not found exceptions
                if (ex is IdPNotFoundException)
                {
                    context.Response.StatusCode = StatusCodes.Status404NotFound;
                }

                serialized = HelpersService.DescribeObject(messageObject, false, false);


                await originalBodyStream.WriteAsync(Encoding.ASCII.GetBytes(serialized));
            }
        }
    }

}
