﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IDSign.IdP.MultiModule.RemoteRepository.Model;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models
{
    public class WebHubException
    {
        public WebHubException(ErrorCode errorCode)
        {
            ErrorCode = errorCode.ToString();
        }

        public string ErrorCode { get; set; }
        public string StackTrace { get; set; }
        public string ExceptionData { get; set; }
    }
}
