﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration
{
    public class CorsSettings
    {
        [JsonProperty("EnableCors")]
        public bool EnableCors { get; set; }

        [JsonProperty("AllowAnyOrigin")]
        public bool AllowAnyOrigin { get; set; }

        [JsonProperty("WithOrigins")]
        public string[] WithOrigins { get; set; }
    }

}
