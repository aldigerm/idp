﻿using Newtonsoft.Json;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration
{
    public class AppSettings
    {
        [JsonProperty("isDebug")]
        public bool IsDebug { get; set; }

        [JsonProperty("IdPAdmin")]
        public IdPAdmin IdPAdmin { get; set; }

        [JsonProperty("corsSettings")]
        public CorsSettings CorsSettings { get; set; }

        [JsonProperty("settingsName")]
        public string SettingsName { get; set; }
    }
  
    public class Claim
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
    public class Scopes
    {
        [JsonProperty("admin")]
        public string Admin{ get; set; }

        [JsonProperty("login")]
        public string Login { get; internal set; }
    }
}
