﻿using Newtonsoft.Json;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration
{
    public class IdPAdmin
    {
        [JsonProperty("Authority")]
        public string Authority { get; set; }

        [JsonProperty("ApiName")]
        public string ApiName { get; set; }

        [JsonProperty("ApiSecret")]
        public string ApiSecret { get; set; }
    }
}
