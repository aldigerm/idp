﻿using IDSign.IdP.Core;
using IDSign.IdP.Core.Config;
using IDSign.IdP.MultiModule.RemoteRepository.Data;
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Core.Infrastructure;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF;
using IDSign.IdP.MultiModule.RemoteRepository.Services;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Authorization;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Middleware;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using Serilog;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub
{
    public class Startup
    {
        private readonly ILogger<Startup> _logger;
        private readonly ILogger<FeatureHelper> _featureHelperLogger;
        public Startup(IConfiguration configuration
            , ILogger<Startup> logger
            , ILogger<FeatureHelper> featureHelperLogger)
        {
            Configuration = configuration;
            _logger = logger;
            _featureHelperLogger = featureHelperLogger;

            IdentityModelEventSource.ShowPII = true;
        }

        public AppSettings AppSettings { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // auto mapper
            services.AddAutoMapper();


            string connectionString = "";
            string logginConnectionString = "";

            connectionString = Configuration.GetConnectionString("DefaultConnection");
            logginConnectionString = Configuration.GetConnectionString("LoggingConnection");

            services.AddDbContext<RemoteRepositoryDbContext>(options =>
                    options.UseSqlServer(connectionString));

            services.AddApiLogging(logginConnectionString);


            // add dependency injection layer
            services
                .AddDataLayerServices()
                .AddServicesLayerServices();

            // httpcontext is not injectable
            services.AddHttpContextAccessor();

            // used to intialise ApplicationUserPrincipal
            services.AddTransient<IClaimsTransformation, RemoteRepositoryClaimsTransformer>();

            // claims transformation to get custom user
            services.AddTransient<ClaimsPrincipal>(
                provider =>
                {
                    return provider.GetService<IHttpContextAccessor>().HttpContext?.User ?? new ClaimsPrincipal();
                });


            services.AddSingleton<IAuthorizationHandler, ResourceAuthorizationHandler>();
            services.AddSingleton<IAuthorizationPolicyProvider, ResourcePolicyProvider>();

            #region AppSettings

            string baseAppSettings = Configuration.GetValue<string>("AppSettingsName") ?? "RemoteRepository";

            // settings which are bound can be specified here
            IConfigurationSection sectionData = Configuration.GetSection(baseAppSettings);
            AppSettings = new AppSettings();
            sectionData.Bind(AppSettings);

            // other app settings can be read here
            string settingsName = AppSettings.SettingsName ?? "Settings";
            string settingsRoot = baseAppSettings + ":" + settingsName;
            _logger.LogInformation("Settings root read from : {0}", settingsRoot);

            // assign the settings to the config values where they will be read
            AppSettingsConfigValue.Init(settingsRoot, Configuration);

            // appsettings made injectable
            services.AddSingleton<AppSettings>(AppSettings);

            #endregion

            // set the logger for FeatureHelper
            FeatureHelper.logger = _featureHelperLogger;

            #region Cors

            var corsSettings = AppSettings?.CorsSettings;

            if (corsSettings != null && corsSettings.EnableCors)
            {
                if (corsSettings.AllowAnyOrigin)
                {
                    services.AddCors(options =>
                    {
                        // this defines a CORS policy called "default"
                        options.AddPolicy("default", builder =>
                        {
                            builder.AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        });
                    });
                }
                else if (corsSettings.WithOrigins.Any())
                {
                    services.AddCors(options =>
                    {
                        // this defines a CORS policy called "default"
                        options.AddPolicy("default", builder =>
                        {
                            builder.WithOrigins(corsSettings.WithOrigins).AllowAnyHeader().AllowAnyMethod()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        });
                    });
                }
            }
            #endregion


            services.AddMvcCore()
                .AddAuthorization(options =>
                {
                    //options.AddPolicy(Constants.PolicyNames.Admin, policy => 
                    //    policy.RequireAssertion(a => 
                    //        a.User.HasClaim("scope", requiredAdminScope) 
                    //        || RemoteRepositorySession.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_ALL_ADMIN)));
                    //options.AddPolicy(Constants.PolicyNames.User.View, policy => 
                    //    policy.RequireAssertion(a => 
                    //        a.User.HasClaim("scope", requiredAdminScope) 
                    //        || RemoteRepositorySession.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_VIEW)));
                    //options.AddPolicy(Constants.PolicyNames.User.Login, policy =>
                    //    policy.RequireAssertion(a =>
                    //        a.User.HasClaim("scope", requiredAdminScope)
                    //        || RemoteRepositorySession.HasAllowedModule(ModuleCode.REPOSITORY_PROFILE_VIEW)));
                })
                .AddJsonFormatters()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                // needed for tags in models such as [Required]
                // otherwise the ModelState will remain valid for null values
                .AddDataAnnotations();

            #region Authentication

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = AppSettings?.IdPAdmin?.Authority;
                    options.RequireHttpsMetadata = false;
                    if (!string.IsNullOrWhiteSpace(AppSettings?.IdPAdmin?.ApiName))
                    {
                        options.ApiName = AppSettings?.IdPAdmin?.ApiName;
                        options.ApiSecret = AppSettings?.IdPAdmin?.ApiSecret;
                    }
                    options.SupportedTokens = IdentityServer4.AccessTokenValidation.SupportedTokens.Both;
                });

            #endregion

            // automapper
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                          IHostingEnvironment env)
        {
            var corsSettings = AppSettings?.CorsSettings;

            if (corsSettings != null && corsSettings.EnableCors && (corsSettings.AllowAnyOrigin || (corsSettings.WithOrigins != null && corsSettings.WithOrigins.Any())))
            {
                app.UseCors("default");
            }

            if (env.IsDevelopment() || AppSettings.IsDebug)
            {
                app.UseMaintainCorsHeaders();
                app.UseDeveloperExceptionPage(); // Beautiful exception pages!
            }
            else
            {
                app.UseHsts();
            }


            app.UseRequestResponseLogging();
            app.UseGlobalErrorHandlerMiddleware();


            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();


            string onlineUrlCheck = Configuration["OnlineUrlCheck"];
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    context.Request.Path.Value.ToLower().EndsWith(onlineUrlCheck))
                {
                    _logger.LogDebug(onlineUrlCheck + " called.");
                    context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync("true");
                }
            });
        }
    }
}
