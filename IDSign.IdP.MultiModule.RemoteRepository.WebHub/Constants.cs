﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub
{
    public static class Constants
    {
        public static class PolicyNames
        {
            public const string Admin = "Admin";

            public static class User
            {
                private const string Name = "User_";
                public const string View = Name + "View";
                public const string Login = Name + "Login";
            }
        }
    }
}
