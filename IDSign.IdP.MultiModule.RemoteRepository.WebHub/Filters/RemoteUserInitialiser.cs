﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.Extensions;
using IDSign.IdP.MultiModule.RemoteRepository.Services.Providers;
using IDSign.IdP.MultiModule.RemoteRepository.WebHub.Models.Configuration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.WebHub.Filters
{
    public class RemoteUserInitialiserAttribute : TypeFilterAttribute
    {
        public RemoteUserInitialiserAttribute() : base(typeof(RemoteUserInitialiserFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class RemoteUserInitialiserFilter : IAsyncResourceFilter
    {
        private AppSettings _Settings;
        private IUserSharedServiceProvider _UserSharedServiceProvider;
        private ILogger<RemoteUserInitialiserFilter> _logger;

        public RemoteUserInitialiserFilter(
            AppSettings settings
            , IUserSharedServiceProvider userSharedServiceProvider
            , ILogger<RemoteUserInitialiserFilter> logger)
        {
            _Settings = settings;
            _UserSharedServiceProvider = userSharedServiceProvider;
            _logger = logger;
        }

        public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
        {
            // check if the user is in the list of allowed users set in the appsettings
            string username = context.HttpContext.User.Identity.Name;
            string tenantCode = context.HttpContext.User.FindFirst(ClaimName.Tenant)?.Value;
            string projectCode = context.HttpContext.User.FindFirst(ClaimName.Project)?.Value;
            string userGuid = context.HttpContext.User.FindFirst(ClaimName.UserGuid)?.Value;
            bool adminScope = context.HttpContext.User.HasAdminScope();

            if (adminScope)
            {
                _logger.LogDebug("Admin scope was found.");
            }
            else if (String.IsNullOrWhiteSpace(username))
            {
                _logger.LogError("User name was not found. Include 'name' claim in token.");
            }

            if (String.IsNullOrWhiteSpace(tenantCode))
            {
                _logger.LogError("Tenant claim was not found {0}", ClaimName.Tenant);
            }
            if (String.IsNullOrWhiteSpace(projectCode))
            {
                _logger.LogError("Project claim was not found {0}", ClaimName.Project);
            }

            _logger.LogDebug($"Checking authorization of user {username} {tenantCode} {projectCode}");

            ClaimsPrincipal adminScopePrincipal = null;
            AppUser user = null;
            try
            {
                if (adminScope)
                {
                    adminScopePrincipal = _UserSharedServiceProvider.SetApiAdminAsLoggedInUser(new TenantIdentifier(tenantCode, projectCode));
                }
                else
                {
                    user = await _UserSharedServiceProvider.GetAsync(new UserIdentifier(userGuid, username, tenantCode, projectCode), true, true);
                }
            }
            catch (IdPException e)
            {
                _logger.LogWarning("IdPException thrown : {0}", e.ErrorCode.ToString());
            }

            if (adminScope && adminScopePrincipal == null)
            {
                _logger.LogError($"Api admin scope couldn't be configured for {tenantCode} {projectCode} and therefore not allowed access.");
            }
            else if (user == null)
            {
                _logger.LogError($"User {username} {tenantCode} {projectCode} not configured for access.");
            }

            if (user != null)
            {
                _logger.LogDebug($"User found.");
            }
            if (adminScopePrincipal != null)
            {
                _logger.LogDebug("Api allowed access.");
            }
        }
    }
}
