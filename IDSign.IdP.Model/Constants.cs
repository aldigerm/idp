﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.Constants
{
    public class IdpClaimType
    {
        public static string ProviderId = "provider-id";
        public static string TenantCode = "provider-user-tenant";
        public static string ProjectCode = "provider-user-project";
    }

    public class RequestVariableName
    {
        public static readonly string ProviderId = "provider-id";
        public static readonly string Username = "username";
    }

    public class PasswordPolicyTypes
    {
        public static readonly string RegexMatch = "RegexMatch";
        public static readonly string MinimumLength = "MinimumLength";
        public static readonly string MaximumLength = "MaximumLength";
        public static readonly string History = "History";
    }

    public class UserTenantSessionTypes
    {
        public const string OTP = "OTP";
        public const string AccessCode = "AccessCode";
        public const string ResetPassword = "ResetPassword";
        public const string ForceNewPassword = "ForceNewPassword";
    }
}
