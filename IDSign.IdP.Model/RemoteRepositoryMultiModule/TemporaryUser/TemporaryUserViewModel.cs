﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class ExternalAccessModel
    {
        [JsonProperty("a")]
        public ExternalAccessSessionIdModel AccessIdentifier { get; set; }

        [JsonProperty("t")]
        public TenantIdentifier TenantIdentifier { get; set; }
    }

    public class ExternalAccessSessionIdModel
    {
        [JsonProperty("s")]
        public string SessionId { get; set; }

        [JsonProperty("e")]
        public DateTimeOffset ExpiryDateTime { get; set; }

    }

}
