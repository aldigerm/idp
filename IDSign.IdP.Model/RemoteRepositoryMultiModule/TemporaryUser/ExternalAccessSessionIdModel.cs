﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TemporaryUserViewModel
    {
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("user")]
        public UserViewModel User { get; set; }

        [JsonProperty("createdOTP")]
        public bool? CreatedOTP { get; set; }

        [JsonProperty("requireOTP")]
        public bool? RequireOTP { get; set; }

        [JsonProperty("nextOTPRequestAllowedAt")]
        public DateTimeOffset? NextOTPRequestAllowedAt { get; set; }

        [JsonProperty("expiryDateTime")]
        public DateTimeOffset? ExpiryDateTime { get; set; }

        [JsonProperty("projectCode")]
        public string ProjectCode { get; set; }

        [JsonProperty("tenantCode")]
        public string TenantCode { get; set; }

        [JsonProperty("expectedOTP")]
        public string ExpectedOTP { get; set; }

        [JsonProperty("error")]
        public SessionError Error { get; set; }
    }

    public class SessionError
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
