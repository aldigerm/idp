﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class CreateTemporaryUserResponseModel
    {
        [JsonProperty("expiryDate")]
        public DateTimeOffset? ExpiryDate { get; set; }
         
        [JsonProperty("key")] 
        public string Key { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

    }
}
