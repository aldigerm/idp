﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class BaseTemporaryUserLoginRequestModel
    {
        [JsonProperty("easid")] 
        public string ExternalAccessSessionId { get; set; }

    }

    public class OTPTemporaryUserLoginRequestModel : BaseTemporaryUserLoginRequestModel
    {
        [JsonProperty("otp")]
        public string OTP { get; set; }

    }
}
