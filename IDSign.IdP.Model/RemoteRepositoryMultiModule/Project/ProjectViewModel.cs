﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class ProjectViewModel
    {
        public ProjectViewModel()
        {
            Tenants = new List<TenantListElementModel>();
            ModulesCodes = new List<string>();
            UserClaimTypes = new List<string>();
            UserClaimTypesDetail = new List<UserClaimTypeModel>();
            UserGroupClaimTypes = new List<string>();
            UserGroupClaimTypesDetail = new List<UserGroupClaimTypeModel>();
            RoleClaimTypes = new List<string>();
            RoleClaimTypesDetail = new List<RoleClaimTypeModel>();
            TenantClaimTypes = new List<string>();
            TenantClaimTypesDetail = new List<TenantClaimTypeModel>();
        }


        [JsonProperty("identifier")]
        [Required]
        public ProjectIdentifier Identifier { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [JsonProperty("loggedInUserProject")]
        public bool LoggedInUserProject { get; set; }

        [JsonProperty("tenants")]
        public IList<TenantListElementModel> Tenants { get; set; }

        [JsonProperty("moduleCodes")]
        public IList<string> ModulesCodes { get; set; }

        [JsonProperty("userClaimTypes")]
        public IList<string> UserClaimTypes { get; set; }

        [JsonProperty("userClaimTypesDetail")]
        public IList<UserClaimTypeModel> UserClaimTypesDetail { get; set; }

        [JsonProperty("userGroupClaimTypes")]
        public IList<string> UserGroupClaimTypes { get; set; }

        [JsonProperty("userGroupClaimTypesDetail")]
        public IList<UserGroupClaimTypeModel> UserGroupClaimTypesDetail { get; set; }


        [JsonProperty("roleClaimTypes")]
        public IList<string> RoleClaimTypes { get; set; }

        [JsonProperty("roleClaimTypesDetail")]
        public IList<RoleClaimTypeModel> RoleClaimTypesDetail { get; set; }

        [JsonProperty("tenantClaimTypes")]
        public IList<string> TenantClaimTypes { get; set; }

        [JsonProperty("tenantClaimTypesDetail")]
        public IList<TenantClaimTypeModel> TenantClaimTypesDetail { get; set; }
    }
}
