﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class ProjectCreateModel
    {
        public ProjectCreateModel()
        {
        }

        [JsonProperty("projectCode")]
        [Required]
        public string ProjectCode { get; set; }


        [JsonProperty("projectName")]
        [Required]
        public string ProjectName { get; set; }


        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }
    }
}
