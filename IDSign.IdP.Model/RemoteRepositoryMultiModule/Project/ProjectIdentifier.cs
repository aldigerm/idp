﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class ProjectIdentifier
    {        
        public ProjectIdentifier()
        {

        }

        public ProjectIdentifier(string projectCode)
        {
            ProjectCode = projectCode;
        }

        public ProjectIdentifier(ProjectIdentifier projectIdentifier): this(projectIdentifier?.ProjectCode)
        {
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        public bool IsEquals(ProjectIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode;
        }
        
        public Dictionary<string, string> ToDictionary()
        {
            return new Dictionary<string, string>
            {
                {"projectCode",ProjectCode }
            };
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode+"'";
        }


    }
}
