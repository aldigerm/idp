﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantSessionBasicModel
    {
        [JsonProperty("userTenantSessionTypeCode")]
        [Required]
        public string UserTenantSessionTypeCode { get; set; }

        [JsonProperty("identifier")]
        public string UserTenantSessionIdentifier { get; set; }

        [JsonProperty("expiryDate")]
        public DateTimeOffset? ExpiryDate { get; set; }

        [JsonProperty("creationDate")]
        public DateTimeOffset CreationDate { get; set; }
    }
}
