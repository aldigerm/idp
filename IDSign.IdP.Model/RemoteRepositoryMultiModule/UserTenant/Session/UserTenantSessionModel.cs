﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantSessionModel : UserTenantSessionBasicModel
    {
        [JsonProperty("userTenantIdentifier")]
        [Required]
        public UserTenantIdentifier UserTenantIdentifier { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("tenantName")]
        public string TenantName { get; set; }

        public override string ToString()
        {
            return $"{UserTenantSessionIdentifier} Expires:{ExpiryDate} Created:{CreationDate} Data:{Data}";
        }
    }
}
