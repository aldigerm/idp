﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantSessionActionResponseModel
    {
        [JsonProperty("data")]
        public string Data { get; set; }
    }
}
