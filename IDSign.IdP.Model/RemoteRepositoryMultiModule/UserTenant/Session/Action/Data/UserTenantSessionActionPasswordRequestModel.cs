﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantSessionActionPasswordRequestModel
    {
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
