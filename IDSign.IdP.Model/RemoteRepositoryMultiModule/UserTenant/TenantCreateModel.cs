﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantCreateModel
    {
        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("userIdentifier")]
        [Required]
        public UserIdentifier UserIdentifier { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        public UserTenantIdentifier GetUserTenantIdentifier()
        {
            return new UserTenantIdentifier(UserIdentifier.Username, TenantIdentifier);
        }
    }
}
