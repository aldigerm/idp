﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class UserTenantIdentifier
    {
        public UserTenantIdentifier()
        {

        }
        public UserTenantIdentifier(UserIdentifier userIdentifier) : this(userIdentifier.Username, userIdentifier.TenantCode, userIdentifier.ProjectCode)
        {

        }
        public UserTenantIdentifier(string username, TenantIdentifier tenantIdentifier) : this(username, tenantIdentifier?.TenantCode, tenantIdentifier?.ProjectCode)
        {
        }

        public UserTenantIdentifier(string username, string tenantCode, string projectCode)
        {
            Username = username;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }


        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }
        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }


        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantCode, ProjectCode);
        }

        public bool IsEquals(UserIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   Username == identifier.Username;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("username", Username);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + " TenantCode:'" + TenantCode + "' Username:'" + Username + "'";
        }
    }
}
