﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserTenantViewModel
    {
        public UserTenantViewModel()
        {
        }

        public UserTenantViewModel(string userName, string tenantCode, string projectCode, string repositoryUsername)
        {
            RepositoryUsername = repositoryUsername;
            Identifier = new UserTenantIdentifier(userName, tenantCode, projectCode);
        }

        [JsonProperty("repositoryUsername")]
        [StringLength(maximumLength: 150)]
        [Required]
        public string RepositoryUsername { get; set; }

        [JsonProperty("identifier")]
        [Required]
        public UserTenantIdentifier Identifier { get; set; }

        [JsonProperty("userClaims")]
        [Required]
        public List<BasicClaim> UserClaims { get; set; }
    }
}
