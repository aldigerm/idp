﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupManagementIdentifier
    {

        public UserGroupManagementIdentifier(UserIdentifier userIdentifier, UserGroupIdentifier userGroupIdentifier)
        {
            Username = userIdentifier.Username;
            ProjectCode = userGroupIdentifier.ProjectCode;
            TenantCode = userGroupIdentifier.TenantCode;
            UserGroupCode = userGroupIdentifier.UserGroupCode;
        }

        [Required]
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        [Required]
        [JsonProperty("userGroupCode")]
        [StringLength(maximumLength: 150)]
        public string UserGroupCode { get; set; }

        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantCode, ProjectCode);
        }
        public UserGroupIdentifier GetUserGroupIdentifier()
        {
            return new UserGroupIdentifier(UserGroupCode, TenantCode, ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' "
                + " TenantCode:'" + TenantCode
                + "' UserGroupCode:'" + (UserGroupCode ?? "null")
                + "' Username:'" + (Username ?? "null") + "'";
        }
    }
}
