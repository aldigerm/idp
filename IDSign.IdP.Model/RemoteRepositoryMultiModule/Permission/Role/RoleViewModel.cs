﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleViewModel
    {

        [JsonProperty("identifier")]
        [Required]
        public RoleIdentifier Identifier { get; set; }

        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [JsonProperty("inheritsFromRoles")]
        public IList<string> InheritsFromRoles { get; set; }

        [JsonProperty("moduleCodes")]
        public IList<string> ModuleCodes { get; set; }

        [JsonProperty("inheritedModuleCodes")]
        public IList<string> InheritedModuleCodes { get; set; }

        [Required]
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }        
    }
}
