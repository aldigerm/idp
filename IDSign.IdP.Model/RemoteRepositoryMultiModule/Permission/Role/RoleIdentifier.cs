﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class RoleIdentifier
    {
        public RoleIdentifier()
        {
        }

        public RoleIdentifier(string roleCode, TenantIdentifier tenantIdentifier) : this(roleCode, tenantIdentifier.TenantCode, tenantIdentifier.ProjectCode)
        {
        }

        public RoleIdentifier(string roleCode, string tenantCode, string projectCode)
        {
            RoleCode = roleCode;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }


        [Required]
        [JsonProperty("roleCode")]
        [StringLength(maximumLength: 128)]
        public string RoleCode { get; set; }
        
        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public bool IsEquals(RoleIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   RoleCode == identifier.RoleCode;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("roleCode", RoleCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + " TenantCode:'" + TenantCode + "' " + " RoleCode:'" + RoleCode + "'";
        }
    }
}
