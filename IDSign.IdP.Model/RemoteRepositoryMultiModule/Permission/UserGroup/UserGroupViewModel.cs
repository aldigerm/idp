﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupViewModel
    {

        [JsonProperty("identifier")]
        [Required]
        public UserGroupIdentifier Identifier { get; set; }

        [JsonProperty("inheritsFromUserGroups")]
        public IList<string> InheritsFromUserGroups { get; set; }
        
        [Required]
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }

        [JsonProperty("usernames")]
        public IList<string> Usernames { get; set; }

        [JsonProperty("passwordPolicies")]
        public IList<string> PasswordPolicies { get; set; }
    }
}
