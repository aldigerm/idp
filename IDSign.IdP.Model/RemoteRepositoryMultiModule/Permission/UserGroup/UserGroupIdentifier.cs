﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class UserGroupIdentifier
    {
        public UserGroupIdentifier()
        {
        }

        public UserGroupIdentifier(string userGroupCode, TenantIdentifier tenantIdentifier) : this(userGroupCode, tenantIdentifier.TenantCode, tenantIdentifier.ProjectCode)
        {
        }

        public UserGroupIdentifier(string userGroupCode, string tenantCode, string projectCode)
        {
            UserGroupCode = userGroupCode;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }


        [Required]
        [JsonProperty("userGroupCode")]
        [StringLength(maximumLength: 128)]
        public string UserGroupCode { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public bool IsEquals(UserGroupIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   UserGroupCode == identifier.UserGroupCode;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + " TenantCode:'" + TenantCode + "' UserGroupCode:'" + (UserGroupCode ?? "null") + "'";
        }
    }
}
