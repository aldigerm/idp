﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public enum ModuleCode
    {
        None,
        PARENT,
        SUPERUSER,
        DASHBOARD_MANAGEMENT,
        DASHBOARD_VIEW,
        DOSSIER_MANAGEMENT,
        DOSSIER_LIST_VIEW,
        DOSSIER_DETAIL_VIEW,
        DOSSIER_DETAIL_PSP_VIEW,
        DOSSIER_DETAIL_RISK_VIEW,
        DOSSIER_DETAIL_REJECT_REASON_VIEW,
        DOSSIER_DETAIL_DATA_VIEW,
        DOSSIER_DETAIL_HISTORY_VIEW,
        DOSSIER_DETAIL_COLLECT_DOCS_BASIC,
        DOSSIER_DETAIL_COLLECT_DOCS_EXTENDED,
        DOSSIER_DETAIL_COLLECT_DOCS_ACTION,
        DOSSIER_VIEW_ALL,
        USER_INTERNAL,
        DOSSIER_DETAIL_INTERNAL_STEPS_VIEW,
        DOSSIER_DETAIL_VIEW_PERSONAL,
        DOSSIER_DETAIL_EDIT,
        TASK_PERSONAL_CREATE,
        TASK_PERSONAL_VIEW,
        TASK_PERSONAL_CLOSE,
        TASK_ALL_CREATE,
        TASK_ALL_VIEW,
        TASK_ALL_CLOSE,
        TASK_ALL_REASSIGN,
        DOSSIER_DETAIL_CREATE,
        DOSSIER_DETAIL_UPDATE,
        USER,
        DOSSIER_DETAIL_RISK_VERIFY,
        DOSSIER_DETAIL_POST_COMPLETE_VIEW,
        DOSSIER_DETAIL_POST_COMPLETE_ACTION,
        DOSSIER_DETAIL_RISK_CREATE_MERCHANT_IDS,
        COUNTRY_VIEW,
        COUNTRY_FILE_DOWNLOAD,
        DOSSIER_DETAIL_RISK_UPLOAD_FILE,
        DOSSIER_DETAIL_RISK_PERMANENT_REJECT,
        DOSSIER_DETAIL_SEND_BACK_TO_PARTNER,
        DOSSIER_DETAIL_SEND_TO_RISK,
        REPORTING,
        CONFIGURATION,

        // local to RemoteRepository Webhub
        REPOSITORY_PROFILE_TENANT_ADMIN,
        REPOSITORY_PROFILE_PROJECT_ADMIN,
        REPOSITORY_PROFILE_ALL_ADMIN,
        PARTNER,
        REPOSITORY_USER_MANAGEMENT,
    }
}
