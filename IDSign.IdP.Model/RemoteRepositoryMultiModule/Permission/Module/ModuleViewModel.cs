﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class ModuleViewModel
    {
        [JsonProperty("identifier")]
        [Required]
        public ModuleIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }      
    }
}
