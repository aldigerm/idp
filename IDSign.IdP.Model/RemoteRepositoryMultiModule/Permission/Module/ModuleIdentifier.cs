﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class ModuleIdentifier
    {
        public ModuleIdentifier()
        {
        }

        public ModuleIdentifier(string moduleCode, ProjectIdentifier projectIdentifier) : this(moduleCode, projectIdentifier.ProjectCode)
        {
        }

        public ModuleIdentifier(string moduleCode, string projectCode)
        {
            ModuleCode = moduleCode;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("moduleCode")]
        [StringLength(maximumLength: 128)]
        public string ModuleCode { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        public bool IsEquals(ModuleIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode && 
                   ModuleCode == identifier.ModuleCode;
        }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' ModuleCode:'" + (ModuleCode ?? "null") + "'";
        }
    }
}
