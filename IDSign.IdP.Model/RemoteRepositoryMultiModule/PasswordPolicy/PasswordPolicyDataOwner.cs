﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public interface PasswordPolicyDataOwner
    {
        string Data { get; set; }
    }
}
