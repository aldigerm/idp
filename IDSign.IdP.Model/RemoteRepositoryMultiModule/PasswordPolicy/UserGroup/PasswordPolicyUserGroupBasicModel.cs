﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyUserGroupBasicModel
    {
        [Required]
        [JsonProperty("identifier")]
        public PasswordPolicyUserGroupIdentifier Identifier { get; set; }

        public UserGroupIdentifier GetUserGroupIdentifier()
        {
            return new UserGroupIdentifier(Identifier.UserGroupCode, Identifier.TenantCode, Identifier.ProjectCode);
        }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(Identifier.TenantCode, Identifier.ProjectCode);
        }

        public PasswordPolicyIdentifier GetPasswordPolicyIdentifier()
        {
            return new PasswordPolicyIdentifier(Identifier.PasswordPolicyCode, GetTenantIdentifier());
        }

        public override string ToString()
        {
            return $"[{GetUserGroupIdentifier()?.ToString()}] [{GetPasswordPolicyIdentifier()?.ToString()}]";
        }

    }
}
