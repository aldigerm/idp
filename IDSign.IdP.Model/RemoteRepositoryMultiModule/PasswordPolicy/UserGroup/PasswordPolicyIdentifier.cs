﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class PasswordPolicyUserGroupIdentifier
    {
        public PasswordPolicyUserGroupIdentifier()
        {
        }

        public PasswordPolicyUserGroupIdentifier(PasswordPolicyIdentifier passwordPolicyIdentifier, UserGroupIdentifier userGroupIdentifier)
            : this(passwordPolicyIdentifier.PasswordPolicyCode, passwordPolicyIdentifier.ProjectCode, userGroupIdentifier.UserGroupCode, userGroupIdentifier.TenantCode)
        {
        }

        public PasswordPolicyUserGroupIdentifier(string passwordPolicyCode, string projectCode, string userGroupCode, string tenantCode)
        {
            PasswordPolicyCode = passwordPolicyCode;
            ProjectCode = projectCode;
            UserGroupCode = userGroupCode;
            TenantCode = tenantCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("passwordPolicyCode")]
        [StringLength(maximumLength: 128)]
        public string PasswordPolicyCode { get; set; }

        [Required]
        [JsonProperty("userGroupCode")]
        [StringLength(maximumLength: 128)]
        public string UserGroupCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 128)]
        public string TenantCode { get; set; }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public bool IsEquals(PasswordPolicyUserGroupIdentifier identifier)
        {
            return identifier != null
                   && ProjectCode == identifier.ProjectCode
                   && TenantCode == identifier.TenantCode
                   && PasswordPolicyCode == identifier.PasswordPolicyCode
                   && UserGroupCode == identifier.UserGroupCode;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("policyCode", PasswordPolicyCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' "
                + " TenantCode:'" + TenantCode + "'"
                + " PolicyCode:'" + PasswordPolicyCode + "'"
                + " UserGroupCode:'" + UserGroupCode + "'";
        }
    }
}
