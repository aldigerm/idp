﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyModel
    {
        [JsonProperty("userGroups")]
        public IList<UserGroupIdentifier> UserGroups { get; set; }

        [Required]
        [JsonProperty("identifier")]
        public PasswordPolicyIdentifier Identifier { get; set; }

        [Required]
        [JsonProperty("passwordPolicyTypeIdentifier")]
        public PasswordPolicyTypeIdentifier PasswordPolicyTypeIdentifier { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("regex")]
        public string Regex { get; set; }

        [JsonProperty("length")]
        public int? Length { get; set; }

        [JsonProperty("historyLimit")]
        public int? HistoryLimit { get; set; }

        [JsonProperty("defaultErrorMessage")]
        public string DefaultErrorMessage { get; set; }

        [JsonProperty("description")]
        [StringLength(maximumLength: 128)]
        public string Description { get; set; }

        [JsonProperty("passed")]
        public bool Passed { get; set; }

    }
}
