﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyRegexModel
    {
        [JsonProperty("regex")]
        public string Regex { get; set; }
    }
}
