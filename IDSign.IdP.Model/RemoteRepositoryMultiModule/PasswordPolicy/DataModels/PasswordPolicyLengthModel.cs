﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyLengthModel
    {
        [JsonProperty("length")]
        public int Length { get; set; }
    }
}
