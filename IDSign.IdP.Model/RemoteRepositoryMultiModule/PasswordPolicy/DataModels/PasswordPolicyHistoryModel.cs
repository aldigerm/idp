﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyHistoryModel
    {
        [JsonProperty("limit")]
        public int Limit { get; set; }
    }
}
