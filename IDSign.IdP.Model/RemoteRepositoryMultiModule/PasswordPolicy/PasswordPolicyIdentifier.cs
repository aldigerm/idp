﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class PasswordPolicyIdentifier
    {
        public PasswordPolicyIdentifier()
        {
        }

        public PasswordPolicyIdentifier(string passwordPolicyCode, TenantIdentifier tenantIdentifier) : this(passwordPolicyCode, tenantIdentifier.TenantCode, tenantIdentifier.ProjectCode)
        {
        }

        public PasswordPolicyIdentifier(string passwordPolicyCode, string tenantCode, string projectCode)
        {
            PasswordPolicyCode = passwordPolicyCode;
            ProjectCode = projectCode;
            TenantCode = tenantCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        [Required]
        [JsonProperty("passwordPolicyCode")]
        [StringLength(maximumLength: 128)]
        public string PasswordPolicyCode { get; set; }
        
        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }
        
        public bool IsEquals(PasswordPolicyIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   PasswordPolicyCode == identifier.PasswordPolicyCode;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("passwordPolicyCode", PasswordPolicyCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + "TenantCode:'" + TenantCode + "' " + " PolicyCode:'" + PasswordPolicyCode + "'";
        }
    }
}
