﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class PasswordPolicyTypeIdentifier
    {
        public PasswordPolicyTypeIdentifier()
        {
        }

        public PasswordPolicyTypeIdentifier(string passwordPolicyTypeCode)
        {
            PasswordPolicyTypeCode = passwordPolicyTypeCode;
        }

        [Required]
        [JsonProperty("passwordPolicyTypeCode")]
        [StringLength(maximumLength: 128)]
        public string PasswordPolicyTypeCode { get; set; }

        public bool IsEquals(PasswordPolicyTypeIdentifier identifier)
        {
            return identifier != null &&
                   PasswordPolicyTypeCode == identifier.PasswordPolicyTypeCode;
        }


        public override string ToString()
        {
            return "PasswordPolicyTypeCode:'" + PasswordPolicyTypeCode + "'";
        }
    }
}
