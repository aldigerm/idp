﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyTypeModel : PasswordPolicyTypeBasicModel
    {

        [JsonProperty("passwordPolicies")]
        [Required]
        public List<PasswordPolicyIdentifier> PasswordPolicies { get; set; }

        
    }
}
