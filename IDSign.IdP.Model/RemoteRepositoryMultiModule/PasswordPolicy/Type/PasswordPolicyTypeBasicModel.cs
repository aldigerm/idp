﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class PasswordPolicyTypeBasicModel
    {

        [JsonProperty("identifier")]
        [Required]
        public PasswordPolicyTypeIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
        
    }
}
