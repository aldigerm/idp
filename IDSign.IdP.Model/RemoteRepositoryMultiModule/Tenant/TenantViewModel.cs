﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantViewModel
    {
        public TenantViewModel()
        {
            Usernames = new List<string>();
            UserGroups = new List<string>();
            Roles = new List<string>();
            PasswordPolicies = new List<string>();
            UserGroupsInferred = new List<string>();
            BasicClaims = new List<BasicClaim>();
        }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonProperty("identifier")]
        [Required]
        public TenantIdentifier Identifier { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("loggedInUserTenant")]
        public bool LoggedInUserTenant { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [JsonProperty("usernames")]
        public IList<string> Usernames { get; set; }

        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [JsonProperty("roles")]
        public IList<string> Roles { get; set; }

        [JsonProperty("userGroupsInferred")]
        public List<string> UserGroupsInferred { get; set; }

        [JsonProperty("passwordPolicies")]
        public List<string> PasswordPolicies { get; set; }

        [JsonProperty("claims")]
        public IList<BasicClaim> BasicClaims { get; set; }
    }
}
