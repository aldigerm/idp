﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class TenantIdentifier
    {
        public TenantIdentifier()
        {

        }

        public TenantIdentifier(TenantIdentifier tenantIdentifier): this(tenantIdentifier?.TenantCode, tenantIdentifier?.ProjectCode)
        {
        }

        public TenantIdentifier(string tenantCode, string projectCode)
        {
            TenantCode = tenantCode;
            ProjectCode = projectCode;
        }

        public TenantIdentifier(string tenantCode, ProjectIdentifier projectIdentifier): this(tenantCode, projectIdentifier?.ProjectCode)
        {
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public bool IsEquals(TenantIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode;
        }
        
        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + " TenantCode:'" + TenantCode + "'";
        }

        public bool IsUndefined()
        {
            return string.IsNullOrWhiteSpace(ProjectCode) && string.IsNullOrWhiteSpace(TenantCode);
        }
    }
}
