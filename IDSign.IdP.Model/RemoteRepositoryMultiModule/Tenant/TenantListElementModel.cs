﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantListElementModel
    {
        public TenantListElementModel()
        {
        }

        public TenantListElementModel(string name, bool enabled, TenantIdentifier tenantIdentifier)
        {
            Name = name;
            Enabled = enabled;
            Identifier = tenantIdentifier;
        }


        [JsonProperty("identifier")]
        public TenantIdentifier Identifier { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }
    }
}
