﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimUpdateModel : RoleClaimModel
    {
        public RoleClaimUpdateModel()
        {
        }
        
        [JsonProperty("newValue")]
        [Required]
        public string NewValue { get; set; }

        public override string ToString()
        {
            return base.ToString() + " NewValue:" + NewValue;
        }
    }
}
