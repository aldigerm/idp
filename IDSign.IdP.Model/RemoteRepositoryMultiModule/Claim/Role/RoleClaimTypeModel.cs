﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimTypeModel 
    {
        public RoleClaimTypeModel()
        {
            Roles = new List<RoleViewModel>();
        }

        public RoleClaimTypeModel(string type, string description, ProjectIdentifier projectIdentifier)
        {
            Description = description;
            Roles = new List<RoleViewModel>();
            Identifier = new RoleClaimTypeIdentifier(type, projectIdentifier);
        }
        
        [JsonProperty("identifier")]
        [Required]
        public RoleClaimTypeIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("roles")]
        public IList<RoleViewModel> Roles { get; set; }
    }
}
