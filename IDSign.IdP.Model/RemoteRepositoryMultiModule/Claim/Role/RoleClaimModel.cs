﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class RoleClaimModel
    {
        public RoleClaimModel()
        {
        }

        public RoleClaimModel(string value, RoleIdentifier u, RoleClaimTypeSetRolesModel model)
        {
            Identifier = new RoleClaimIdentifier(value, u, model.Identifier);
        }
        public RoleClaimModel(string value, string type, string description, RoleIdentifier u)
        {
            Identifier = new RoleClaimIdentifier(u.RoleCode, u.TenantCode,u.ProjectCode,type,value);
            Description = description;
        }

        [JsonProperty("identifier")]
        [Required]
        public RoleClaimIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("rolesWithSameClaim")]
        public List<RoleIdentifier> RolesWithSameClaim { get; set; }
    }
}
