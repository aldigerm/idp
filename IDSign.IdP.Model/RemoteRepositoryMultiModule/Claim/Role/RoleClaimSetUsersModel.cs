﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimSetRolesModel
    {
        public RoleClaimSetRolesModel()
        {
        }

        [JsonProperty("identifier")]
        [Required]
        public RoleClaimIdentifier Identifier { get; set; }


        [JsonProperty("roles")]
        [Required]
        public List<RoleIdentifier> Roles { get; set; }
    }
}
