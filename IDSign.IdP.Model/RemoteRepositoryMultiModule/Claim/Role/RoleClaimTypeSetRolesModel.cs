﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimTypeSetRolesModel : RoleClaimTypeModel
    {
        public RoleClaimTypeSetRolesModel()
        {
        }


        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [JsonProperty("roles")]
        [Required]
        public new IList<RoleIdentifier> Roles { get; set; }
    }
}
