﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimIdentifier
    {
        public RoleClaimIdentifier()
        {

        }
        public RoleClaimIdentifier(string value, RoleIdentifier roleIdentifier, RoleClaimTypeIdentifier roleClaimTypeIdentifier)
            : this(roleIdentifier.RoleCode, roleIdentifier.TenantCode, roleIdentifier.ProjectCode, roleClaimTypeIdentifier.Type, value)
        {
        }

        public RoleClaimIdentifier(string roleCode, string tenantCode, string projectCode, string type, string value)
        {
            RoleCode = roleCode;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
            Type = type;
            Value = value;
        }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [Required]
        [JsonProperty("roleCode")]
        [StringLength(maximumLength: 150)]
        public string RoleCode { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }
        
        public RoleIdentifier GetRoleIdentifier()
        {
            return new RoleIdentifier(RoleCode,TenantCode,ProjectCode);
        }

        public bool IsEquals(RoleClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   RoleCode == identifier.RoleCode &&
                   Type == identifier.Type &&
                   Value == identifier.Value;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("roleCode", RoleCode);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("type", Type);
            dictionary.Add("value", Value);
            return dictionary;
        }

        public override string ToString()
        {
            return base.ToString() + " Type:'" + (Type ?? "null") + "' Value:'" + (Value ?? "null") + "'";
        }
    }
}
