﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimTypeUpdateModel : RoleClaimTypeModel
    {
        public RoleClaimTypeUpdateModel()
        {
        }

        [JsonProperty("newType")]
        [Required]
        public string NewType { get; set; }
    }
}
