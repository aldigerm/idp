﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimTypeCreateModel
    {
                
        [JsonProperty("projectIdentifier")]
        [Required]
        public ProjectIdentifier ProjectIdentifier { get; set; }

        public RoleClaimTypeIdentifier GetClaimTypeIdentifier()
        {
            return new RoleClaimTypeIdentifier(Type, ProjectIdentifier);
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("roles")]
        public IList<RoleViewModel> Roles { get; set; }
    }
}
