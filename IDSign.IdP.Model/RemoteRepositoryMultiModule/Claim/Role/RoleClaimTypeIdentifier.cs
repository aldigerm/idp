﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class RoleClaimTypeIdentifier
    {       
        public RoleClaimTypeIdentifier()
        {
        }

        public RoleClaimTypeIdentifier(RoleClaimIdentifier model): this(model?.Type, model?.ProjectCode)
        {
        }

        public RoleClaimTypeIdentifier(string type, ProjectIdentifier roleIdentifier) : this(type, roleIdentifier.ProjectCode)
        {
        }

        public RoleClaimTypeIdentifier(string type, string projectCode)
        {
            Type = type;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }


        public bool IsEquals(RoleClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   Type == identifier.Type;
        }
        
        [JsonProperty("type")]
        [Required]
        [StringLength(maximumLength: 150)]
        public string Type { get; set; }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "'" + " Type:" + Type;
        }
    }
}
