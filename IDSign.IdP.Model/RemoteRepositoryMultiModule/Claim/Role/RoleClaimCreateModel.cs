﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class RoleClaimCreateModel
    {
        public RoleClaimCreateModel()
        {
        }
        public RoleClaimCreateModel(string value, RoleIdentifier u, RoleClaimTypeSetRolesModel model)
        {
            TenantIdentifier = u.GetTenantIdentifier();
            Type = model.Identifier.Type;
            Value = value;
            RoleCode = u.RoleCode;
        }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("RoleCode")]
        [Required]
        public string RoleCode { get; set; }

        public RoleIdentifier GetRoleIdentifier()
        {
            return new RoleIdentifier(RoleCode, TenantIdentifier);
        }

        public RoleClaimIdentifier GetRoleClaimIdentifier()
        {
            return new RoleClaimIdentifier(RoleCode, TenantIdentifier.TenantCode, TenantIdentifier.ProjectCode, Type, Value);
        }

        public RoleClaimTypeIdentifier GetRoleClaimTypeIdentifier()
        {
            return new RoleClaimTypeIdentifier(GetRoleClaimIdentifier());
        }
    }
}
