﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimTypeSetUsersModel : UserClaimTypeModel
    {
        public UserClaimTypeSetUsersModel()
        {
        }


        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [JsonProperty("users")]
        [Required]
        public new IList<UserIdentifier> Users { get; set; }
    }
}
