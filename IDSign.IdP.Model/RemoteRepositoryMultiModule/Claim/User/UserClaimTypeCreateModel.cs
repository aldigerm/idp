﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimTypeCreateModel
    {
                
        [JsonProperty("projectIdentifier")]
        [Required]
        public ProjectIdentifier ProjectIdentifier { get; set; }

        public UserClaimTypeIdentifier GetClaimTypeIdentifier()
        {
            return new UserClaimTypeIdentifier(Type, ProjectIdentifier);
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

    }
}
