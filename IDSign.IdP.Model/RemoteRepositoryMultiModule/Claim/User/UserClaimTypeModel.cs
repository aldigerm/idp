﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimTypeModel 
    {
        public UserClaimTypeModel()
        {
            Users = new List<UserViewModel>();
        }

        public UserClaimTypeModel(string type, string description, ProjectIdentifier projectIdentifier)
        {
            Description = description;
            Users = new List<UserViewModel>();
            Identifier = new UserClaimTypeIdentifier(type, projectIdentifier);
        }
        
        [JsonProperty("identifier")]
        [Required]
        public UserClaimTypeIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("users")]
        public IList<UserViewModel> Users { get; set; }
    }
}
