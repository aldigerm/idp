﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimTypeIdentifier
    {       
        public UserClaimTypeIdentifier()
        {
        }

        public UserClaimTypeIdentifier(UserClaimIdentifier model): this(model?.Type, model?.ProjectCode)
        {
        }

        public UserClaimTypeIdentifier(string type, ProjectIdentifier projectIdentifier) : this(type, projectIdentifier.ProjectCode)
        {
        }

        public UserClaimTypeIdentifier(string type, string projectCode)
        {
            Type = type;
            ProjectCode = projectCode;
        }

        [JsonProperty("projectCode")]
        [Required]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        public bool IsEquals(UserClaimTypeIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   Type == identifier.Type;
        }
        
        [JsonProperty("type")]
        [Required]
        [StringLength(maximumLength: 150)]
        public string Type { get; set; }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "'" + " Type:" + Type;
        }
    }
}
