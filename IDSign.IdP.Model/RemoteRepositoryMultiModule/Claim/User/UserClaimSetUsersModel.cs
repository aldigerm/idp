﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimSetUsersModel
    {
        public UserClaimSetUsersModel()
        {
        }

        [JsonProperty("identifier")]
        [Required]
        public UserClaimIdentifier Identifier { get; set; }


        [JsonProperty("users")]
        [Required]
        public List<UserIdentifier> Users { get; set; }
    }
}
