﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimIdentifier
    {
        public UserClaimIdentifier()
        {

        }
        public UserClaimIdentifier(string value, UserIdentifier userIdentifier, UserClaimTypeIdentifier userClaimTypeIdentifier)
            : this(userIdentifier.Username, userIdentifier.TenantCode, userIdentifier.ProjectCode, userClaimTypeIdentifier.Type, value)
        {
        }

        public UserClaimIdentifier(string username, string tenantCode, string projectCode, string type, string value)
        {
            Username = username;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
            Type = type;
            Value = value;
        }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [Required]
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }

        public UserClaimTypeIdentifier GetUserClaimTypeIdentifier()
        {
            return new UserClaimTypeIdentifier(Type, ProjectCode);
        }

        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantCode,ProjectCode);
        }

        public bool IsEquals(UserClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   Username == identifier.Username &&
                   Type == identifier.Type &&
                   Value == identifier.Value;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("username", Username);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("type", Type);
            dictionary.Add("value", Value);
            return dictionary;
        }

        public override string ToString()
        {
            return base.ToString() + " Type:'" + (Type ?? "null") + "' Value:'" + (Value ?? "null") + "'";
        }
    }
}
