﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class UserClaimCreateModel
    {
        public UserClaimCreateModel()
        {
        }
        public UserClaimCreateModel(string value, UserIdentifier u, UserClaimTypeSetUsersModel model)
        {
            TenantIdentifier = u.GetTenantIdentifier();
            Type = model.Identifier.Type;
            Value = value;
            Username = u.Username;
        }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("username")]
        [Required]
        public string Username { get; set; }

        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantIdentifier);
        }

        public UserTenantIdentifier GetUserTenantIdentifier()
        {
            return new UserTenantIdentifier(Username, TenantIdentifier);
        }

        public UserClaimIdentifier GetUserClaimIdentifier()
        {
            return new UserClaimIdentifier(Username, TenantIdentifier.TenantCode, TenantIdentifier.ProjectCode, Type, Value);
        }

        public UserClaimTypeIdentifier GetUserClaimTypeIdentifier()
        {
            return new UserClaimTypeIdentifier(GetUserClaimIdentifier());
        }
    }
}
