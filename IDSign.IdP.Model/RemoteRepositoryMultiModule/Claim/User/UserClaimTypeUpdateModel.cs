﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserClaimTypeUpdateModel : UserClaimTypeModel
    {
        public UserClaimTypeUpdateModel()
        {
        }

        [JsonProperty("newType")]
        [Required]
        public string NewType { get; set; }
    }
}
