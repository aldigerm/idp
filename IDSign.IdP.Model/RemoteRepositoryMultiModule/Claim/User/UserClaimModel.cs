﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class UserClaimModel
    {
        public UserClaimModel()
        {
        }

        public UserClaimModel(string value, UserIdentifier u, UserClaimTypeSetUsersModel model)
        {
            Identifier = new UserClaimIdentifier(value, u, model.Identifier);
        }
        public UserClaimModel(string value, string type, string description, UserIdentifier u)
        {
            Identifier = new UserClaimIdentifier(u.Username, u.TenantCode,u.ProjectCode,type,value);
            Description = description;
        }

        [JsonProperty("identifier")]
        [Required]
        public UserClaimIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("usersWithSameClaim")]
        public List<UserIdentifier> UsersWithSameClaim { get; set; }
    }
}
