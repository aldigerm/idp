﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimTypeModel 
    {
        public TenantClaimTypeModel()
        {
            Tenants = new List<TenantViewModel>();
        }

        public TenantClaimTypeModel(string type, string description, ProjectIdentifier projectIdentifier)
        {
            Description = description;
            Tenants = new List<TenantViewModel>();
            Identifier = new TenantClaimTypeIdentifier(type, projectIdentifier);
        }
        
        [JsonProperty("identifier")]
        [Required]
        public TenantClaimTypeIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("tenants")]
        public IList<TenantViewModel> Tenants { get; set; }
    }
}
