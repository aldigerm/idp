﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class TenantClaimModel
    {
        public TenantClaimModel()
        {
        }

        public TenantClaimModel(string value, TenantIdentifier u, TenantClaimTypeSetTenantsModel model)
        {
            Identifier = new TenantClaimIdentifier(value, u, model.Identifier);
        }
        public TenantClaimModel(string value, string type, string description, TenantIdentifier u)
        {
            Identifier = new TenantClaimIdentifier(u.TenantCode, u.ProjectCode, type, value);
            Description = description;
        }

        [JsonProperty("identifier")]
        [Required]
        public TenantClaimIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("tenantsWithSameClaim")]
        public List<TenantIdentifier> TenantsWithSameClaim { get; set; }
    }
}
