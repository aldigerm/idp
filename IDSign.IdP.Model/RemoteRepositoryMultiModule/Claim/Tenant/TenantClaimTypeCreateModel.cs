﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimTypeCreateModel
    {
                
        [JsonProperty("projectIdentifier")]
        [Required]
        public ProjectIdentifier ProjectIdentifier { get; set; }

        public TenantClaimTypeIdentifier GetClaimTypeIdentifier()
        {
            return new TenantClaimTypeIdentifier(Type, ProjectIdentifier);
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("tenants")]
        public IList<TenantViewModel> Tenants { get; set; }
    }
}
