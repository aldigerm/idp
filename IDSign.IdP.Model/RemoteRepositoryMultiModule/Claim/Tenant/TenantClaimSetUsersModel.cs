﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimSetTenantsModel
    {
        public TenantClaimSetTenantsModel()
        {
        }

        [JsonProperty("identifier")]
        [Required]
        public TenantClaimIdentifier Identifier { get; set; }


        [JsonProperty("tenants")]
        [Required]
        public List<TenantIdentifier> Tenants { get; set; }
    }
}
