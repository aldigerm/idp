﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimIdentifier
    {
        public TenantClaimIdentifier()
        {

        }
        public TenantClaimIdentifier(string value, TenantIdentifier tenantIdentifier, TenantClaimTypeIdentifier tenantClaimTypeIdentifier)
            : this(tenantIdentifier.TenantCode, tenantIdentifier.ProjectCode, tenantClaimTypeIdentifier.Type, value)
        {
        }

        public TenantClaimIdentifier(string tenantCode, string projectCode, string type, string value)
        {
            TenantCode = tenantCode;
            ProjectCode = projectCode;
            Type = type;
            Value = value;
        }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }
        
        public bool IsEquals(TenantClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   TenantCode == identifier.TenantCode &&
                   Type == identifier.Type &&
                   Value == identifier.Value;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("type", Type);
            dictionary.Add("value", Value);
            return dictionary;
        }

        public override string ToString()
        {
            return base.ToString() + " Type:'" + (Type ?? "null") + "' Value:'" + (Value ?? "null") + "'";
        }
    }
}
