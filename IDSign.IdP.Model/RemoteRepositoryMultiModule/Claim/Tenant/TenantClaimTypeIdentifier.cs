﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimTypeIdentifier
    {       
        public TenantClaimTypeIdentifier()
        {
        }

        public TenantClaimTypeIdentifier(TenantClaimIdentifier model): this(model?.Type, model?.ProjectCode)
        {
        }

        public TenantClaimTypeIdentifier(string type, ProjectIdentifier tenantIdentifier) : this(type, tenantIdentifier.ProjectCode)
        {
        }

        public TenantClaimTypeIdentifier(string type, string projectCode)
        {
            Type = type;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }


        public bool IsEquals(TenantClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   Type == identifier.Type;
        }
        
        [JsonProperty("type")]
        [Required]
        [StringLength(maximumLength: 150)]
        public string Type { get; set; }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "'" + " Type:" + Type;
        }
    }
}
