﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimTypeSetTenantsModel : TenantClaimTypeModel
    {
        public TenantClaimTypeSetTenantsModel()
        {
        }


        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [JsonProperty("tenants")]
        [Required]
        public new IList<TenantIdentifier> Tenants { get; set; }
    }
}
