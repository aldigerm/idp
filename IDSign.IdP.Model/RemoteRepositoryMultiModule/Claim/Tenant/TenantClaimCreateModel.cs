﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class TenantClaimCreateModel
    {
        public TenantClaimCreateModel()
        {
        }
        public TenantClaimCreateModel(string value, TenantIdentifier u, TenantClaimTypeSetTenantsModel model)
        {
            TenantIdentifier = u;
            Type = model.Identifier.Type;
            Value = value;
        }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }
        
        public TenantIdentifier GetTenantIdentifier()
        {
            return TenantIdentifier;
        }

        public TenantClaimIdentifier GetTenantClaimIdentifier()
        {
            return new TenantClaimIdentifier(TenantIdentifier.TenantCode, TenantIdentifier.ProjectCode, Type, Value);
        }

        public TenantClaimTypeIdentifier GetTenantClaimTypeIdentifier()
        {
            return new TenantClaimTypeIdentifier(GetTenantClaimIdentifier());
        }
    }
}
