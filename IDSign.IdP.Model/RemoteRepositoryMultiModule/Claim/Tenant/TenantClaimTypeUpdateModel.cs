﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimTypeUpdateModel : TenantClaimTypeModel
    {
        public TenantClaimTypeUpdateModel()
        {
        }

        [JsonProperty("newType")]
        [Required]
        public string NewType { get; set; }
    }
}
