﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class TenantClaimUpdateModel : TenantClaimModel
    {
        public TenantClaimUpdateModel()
        {
        }
        
        [JsonProperty("newValue")]
        [Required]
        public string NewValue { get; set; }

        public override string ToString()
        {
            return base.ToString() + " NewValue:" + NewValue;
        }
    }
}
