﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class UserGroupClaimCreateModel
    {
        public UserGroupClaimCreateModel()
        {
        }
        public UserGroupClaimCreateModel(string value, UserGroupIdentifier u, UserGroupClaimTypeSetUserGroupsModel model)
        {
            TenantIdentifier = u.GetTenantIdentifier();
            Type = model.Identifier.Type;
            Value = value;
            UserGroupCode = u.UserGroupCode;
        }

        [JsonProperty("tenantIdentifier")]
        [Required]
        public TenantIdentifier TenantIdentifier { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("userGroupCode")]
        [Required]
        public string UserGroupCode { get; set; }

        public UserGroupIdentifier GetUserGroupIdentifier()
        {
            return new UserGroupIdentifier(UserGroupCode, TenantIdentifier);
        }

        public UserGroupClaimIdentifier GetUserGroupClaimIdentifier()
        {
            return new UserGroupClaimIdentifier(UserGroupCode, TenantIdentifier.TenantCode, TenantIdentifier.ProjectCode, Type, Value);
        }

        public UserGroupClaimTypeIdentifier GetUserGroupClaimTypeIdentifier()
        {
            return new UserGroupClaimTypeIdentifier(GetUserGroupClaimIdentifier());
        }
    }
}
