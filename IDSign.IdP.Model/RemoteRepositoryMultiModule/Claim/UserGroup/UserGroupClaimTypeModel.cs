﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimTypeModel 
    {
        public UserGroupClaimTypeModel()
        {
            UserGroups = new List<UserGroupViewModel>();
        }

        public UserGroupClaimTypeModel(string type, string description, ProjectIdentifier projectIdentifier)
        {
            Description = description;
            UserGroups = new List<UserGroupViewModel>();
            Identifier = new UserGroupClaimTypeIdentifier(type, projectIdentifier);
        }
        
        [JsonProperty("identifier")]
        [Required]
        public UserGroupClaimTypeIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("userGroups")]
        public IList<UserGroupViewModel> UserGroups { get; set; }
    }
}
