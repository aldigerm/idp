﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimTypeIdentifier
    {       
        public UserGroupClaimTypeIdentifier()
        {
        }

        public UserGroupClaimTypeIdentifier(UserGroupClaimIdentifier model): this(model?.Type, model?.ProjectCode)
        {
        }

        public UserGroupClaimTypeIdentifier(string type, ProjectIdentifier userGroupIdentifier) : this(type, userGroupIdentifier?.ProjectCode)
        {
        }

        public UserGroupClaimTypeIdentifier(string type, string projectCode)
        {
            Type = type;
            ProjectCode = projectCode;
        }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [JsonProperty("type")]
        [Required]
        [StringLength(maximumLength: 150)]
        public string Type { get; set; }


        public bool IsEquals(UserClaimTypeIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   Type == identifier.Type;
        }

        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "'" + " Type:" + Type;
        }
    }
}
