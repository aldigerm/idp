﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimSetUserGroupsModel
    {
        public UserGroupClaimSetUserGroupsModel()
        {
        }

        [JsonProperty("identifier")]
        [Required]
        public UserGroupClaimIdentifier Identifier { get; set; }


        [JsonProperty("userGroups")]
        [Required]
        public List<UserGroupIdentifier> UserGroups { get; set; }
    }
}
