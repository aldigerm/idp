﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimTypeCreateModel
    {
                
        [JsonProperty("projectIdentifier")]
        [Required]
        public ProjectIdentifier ProjectIdentifier { get; set; }

        public UserGroupClaimTypeIdentifier GetClaimTypeIdentifier()
        {
            return new UserGroupClaimTypeIdentifier(Type, ProjectIdentifier);
        }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("userGroups")]
        public IList<UserGroupViewModel> UserGroups { get; set; }
    }
}
