﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimTypeUpdateModel : UserGroupClaimTypeModel
    {
        public UserGroupClaimTypeUpdateModel()
        {
        }

        [JsonProperty("newType")]
        [Required]
        public string NewType { get; set; }
    }
}
