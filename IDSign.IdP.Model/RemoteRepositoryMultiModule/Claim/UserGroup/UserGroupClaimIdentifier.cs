﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimIdentifier
    {
        public UserGroupClaimIdentifier()
        {

        }
        public UserGroupClaimIdentifier(string value, UserGroupIdentifier userIdentifier, UserGroupClaimTypeIdentifier userClaimTypeIdentifier)
            : this(userIdentifier.UserGroupCode, userIdentifier.TenantCode, userIdentifier.ProjectCode, userClaimTypeIdentifier.Type, value)
        {
        }

        public UserGroupClaimIdentifier(string userGroupCode, string tenantCode, string projectCode, string type, string value)
        {
            UserGroupCode = userGroupCode;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
            Type = type;
            Value = value;
        }

        [JsonProperty("type")]
        [Required]
        public string Type { get; set; }

        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [Required]
        [JsonProperty("userGroupCode")]
        [StringLength(maximumLength: 150)]
        public string UserGroupCode { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }

        public UserClaimTypeIdentifier GetUserClaimTypeIdentifier()
        {
            return new UserClaimTypeIdentifier(Type, ProjectCode);
        }

        public UserGroupIdentifier GetUserGroupIdentifier()
        {
            return new UserGroupIdentifier(UserGroupCode,TenantCode,ProjectCode);
        }

        public bool IsEquals(UserGroupClaimIdentifier identifier)
        {
            return identifier != null &&
                   ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   UserGroupCode == identifier.UserGroupCode &&
                   Type == identifier.Type &&
                   Value == identifier.Value;
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("userGroupCode", UserGroupCode);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            dictionary.Add("type", Type);
            dictionary.Add("value", Value);
            return dictionary;
        }

        public override string ToString()
        {
            return base.ToString() + " Type:'" + (Type ?? "null") + "' Value:'" + (Value ?? "null") + "'";
        }
    }
}
