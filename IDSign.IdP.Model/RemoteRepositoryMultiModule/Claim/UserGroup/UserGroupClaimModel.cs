﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{

    public class UserGroupClaimModel
    {
        public UserGroupClaimModel()
        {
        }

        public UserGroupClaimModel(string value, UserGroupIdentifier u, UserGroupClaimTypeSetUserGroupsModel model)
        {
            Identifier = new UserGroupClaimIdentifier(value, u, model.Identifier);
        }
        public UserGroupClaimModel(string value, string type, string description, UserGroupIdentifier u)
        {
            Identifier = new UserGroupClaimIdentifier(u.UserGroupCode, u.TenantCode,u.ProjectCode,type,value);
            Description = description;
        }

        [JsonProperty("identifier")]
        [Required]
        public UserGroupClaimIdentifier Identifier { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("userGroupsWithSameClaim")]
        public List<UserGroupIdentifier> UserGroupsWithSameClaim { get; set; }
    }
}
