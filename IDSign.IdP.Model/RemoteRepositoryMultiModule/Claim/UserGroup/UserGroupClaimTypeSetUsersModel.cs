﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserGroupClaimTypeSetUserGroupsModel : UserGroupClaimTypeModel
    {
        public UserGroupClaimTypeSetUserGroupsModel()
        {
        }


        [JsonProperty("value")]
        [Required]
        public string Value { get; set; }


        [JsonProperty("userGroups")]
        [Required]
        public new IList<UserGroupIdentifier> UserGroups { get; set; }
    }
}
