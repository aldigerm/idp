﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserViewModel : UserBasicModel
    {
        public UserViewModel()
        {
            UserClaims = new List<UserClaimModel>();
            UserGroupClaims = new List<UserGroupClaimModel>();
            RoleClaims = new List<RoleClaimModel>();
            AllClaims = new List<BasicClaim>();
            ManagedUserGroups = new List<string>();
            UserTenants = new List<TenantIdentifier>();
        }

        public UserViewModel(string userGuid, string userName, string tenantCode, string projectCode, string repositoryUsername) : base(userGuid, userName, tenantCode, projectCode)
        {
            RepositoryUsername = repositoryUsername;
        }

        [EmailAddress]
        [StringLength(maximumLength: 150)]
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("dateOfBirth")]
        public DateTimeOffset? DateOfBirth { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("cityOfBirth")]
        public string CityOfBirth { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("nins")]
        public string NINS { get; set; }

        [Phone]
        [JsonProperty("mobileNumber")]
        public string MobileNumber { get; set; }

        [JsonProperty("tenantUsername")]
        public string TenantUsername { get; set; }

        [JsonProperty("repositoryUsername")]
        public string RepositoryUsername { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("middleName")]
        public string MiddleName { get; set; }

        [JsonProperty("occupation")]
        public string Occupation { get; set; }

        [JsonProperty("customAttributes")]
        public string CustomAttributes { get; set; }

        [JsonProperty("userIdentifier")]
        public string UserIdentifier { get; set; }

        [JsonProperty("allowedModules")]
        public IList<string> AllowedModules { get; set; }

        [JsonProperty("userAccessDetail")]
        public UserAccessDetail UserAccessDetail { get; set; }

        [JsonProperty("userGroups")]
        public IList<string> UserGroups { get; set; }

        [JsonProperty("claims")]
        public IList<BasicClaim> AllClaims { get; set; }

        [JsonProperty("userClaims")]
        public IList<UserClaimModel> UserClaims { get; set; }

        [JsonProperty("userGroupClaims")]
        public IList<UserGroupClaimModel> UserGroupClaims { get; set; }

        [JsonProperty("managedUserGroups")]
        public IList<string> ManagedUserGroups { get; set; }

        [JsonProperty("roleClaims")]
        public IList<RoleClaimModel> RoleClaims { get; set; }

        [JsonProperty("isSuperUser")]
        public bool IsSuperUser { get; set; }

        [JsonProperty("isAdmin")]
        public bool IsAdmin { get; set; }

        [JsonProperty("isTemporaryUser")]
        public bool IsTemporaryUser { get; set; }

        [JsonProperty("tenants")]
        public IList<TenantIdentifier> UserTenants { get; set; }

        [JsonProperty("isTenantSpecific")]
        public bool IsTenantSpecific { get; set; }

        [JsonProperty("passwordPolicies")]
        public IList<PasswordPolicyModel> PasswordPolicies { get; set; }

        [JsonProperty("sessions")]
        public IList<UserTenantSessionModel> Sessions { get; set; }

    }
}
