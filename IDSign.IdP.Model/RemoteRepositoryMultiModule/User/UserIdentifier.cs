﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public sealed class UserIdentifier
    {
        public UserIdentifier()
        {
        }

        public UserIdentifier(UserIdentifier userIdentifier) : this(userIdentifier.UserGUID, userIdentifier.Username, userIdentifier.TenantCode, userIdentifier.ProjectCode)
        {
        }

        public UserIdentifier(string userGUID, string username, TenantIdentifier tenantIdentifier) : this(userGUID, username, tenantIdentifier?.TenantCode, tenantIdentifier?.ProjectCode)
        {
        }

        public UserIdentifier(string username, TenantIdentifier tenantIdentifier) : this(username, tenantIdentifier?.TenantCode, tenantIdentifier?.ProjectCode)
        {
        }

        public UserIdentifier(string userGUID)
        {
            UserGUID = userGUID;
            Username = null;
            TenantCode = null;
            ProjectCode = null;
        }

        public UserIdentifier(string username, string tenantCode, string projectCode)
        {
            UserGUID = null;
            Username = username;
            TenantCode = tenantCode;
            ProjectCode = projectCode;
        }

        public UserIdentifier(string userGUID, string username, string tenantCode, string projectCode) : this(username, tenantCode, projectCode)
        {
            UserGUID = userGUID;
        }

        [JsonProperty("userGuid")]
        [StringLength(maximumLength: 150)]
        public string UserGUID { get; set; }
        
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }
        
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }
        
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }


        public ProjectIdentifier GetProjectIdentifier()
        {
            return new ProjectIdentifier(ProjectCode);
        }
        public TenantIdentifier GetTenantIdentifier()
        {
            if(string.IsNullOrWhiteSpace(TenantCode) || string.IsNullOrWhiteSpace(ProjectCode))
            {
                return null;
            }
            return new TenantIdentifier(TenantCode, ProjectCode);
        }

        public bool IsEquals(UserIdentifier identifier)
        {
            return identifier != null &&
                   ((ProjectCode == identifier.ProjectCode &&
                   TenantCode == identifier.TenantCode &&
                   Username == identifier.Username)
                   || UserGUID == identifier.UserGUID);
        }

        public Dictionary<string, string> ToDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(UserGUID))
            {
                dictionary.Add("userGuid", UserGUID);
            }
            dictionary.Add("username", Username);
            dictionary.Add("projectCode", ProjectCode);
            dictionary.Add("tenantCode", TenantCode);
            return dictionary;
        }

        public override string ToString()
        {
            return "ProjectCode:'" + ProjectCode + "' " + " TenantCode:'" + TenantCode + "' Username:'" + Username + "'" + "' UserGuid:'" + UserGUID + "'";
        }
    }
}
