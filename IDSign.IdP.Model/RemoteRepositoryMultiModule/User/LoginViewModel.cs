﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class LoginViewModel
    {
        public LoginViewModel(string username, string password, string tenantCode, string projectCode)
        {
            Username = username;
            ProjectCode = projectCode;
            TenantCode = tenantCode;
            Password = password;
        }

        [Required]
        [JsonProperty("username")]
        [StringLength(maximumLength: 150)]
        public string Username { get; set; }

        [Required]
        [JsonProperty("projectCode")]
        [StringLength(maximumLength: 150)]
        public string ProjectCode { get; set; }

        [Required]
        [JsonProperty("tenantCode")]
        [StringLength(maximumLength: 150)]
        public string TenantCode { get; set; }

        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }

        public TenantIdentifier GetTenantIdentifier()
        {
            return new TenantIdentifier(TenantCode, ProjectCode);
        }
        public UserIdentifier GetUserIdentifier()
        {
            return new UserIdentifier(Username, TenantCode, ProjectCode);
        }
    }
}
