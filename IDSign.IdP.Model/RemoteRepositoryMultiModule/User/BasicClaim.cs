﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class BasicClaim
    {
        public BasicClaim(string type, string value)
        {
            Type = type;
            Value = value;
        }

        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        
        public override string ToString()
        {
            return "Type:" + (Type ?? "") + " Value:" + (Value ?? ""); 
        }
    }
}
