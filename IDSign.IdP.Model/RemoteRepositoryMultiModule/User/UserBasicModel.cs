﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserBasicModel
    {
        public UserBasicModel()
        {

        }

        public UserBasicModel(string userGuid, string username, string tenantCode, string projectCode)
        {
            this.Identifier = new UserIdentifier(userGuid, username, tenantCode, projectCode);
        }

        [JsonProperty("identifier")]
        [Required]
        public UserIdentifier Identifier { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [StringLength(maximumLength: 100)]
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [JsonProperty("isLoggedInUser")]
        public bool? IsLoggedInUser { get; set; }

        [JsonProperty("isNewPasswordRequired")]
        public bool IsNewPasswordRequired { get; set; }
    }
}
