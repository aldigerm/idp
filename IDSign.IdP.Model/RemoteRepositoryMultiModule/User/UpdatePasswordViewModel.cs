﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UpdatePasswordViewModel
    {
        [JsonProperty("identifier")]
        [Required]
        public UserIdentifier Identifier { get; set; }
        
        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("isLoginReset")]
        public bool IsLoginReset { get; set; }
    }
}
