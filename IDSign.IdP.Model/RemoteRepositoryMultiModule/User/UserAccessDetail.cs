﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IDSign.IdP.Model.RemoteRepositoryMultiModule
{
    public class UserAccessDetail
    {
        public UserAccessDetail()
        {
        }

        [JsonProperty("projects")]
        public IList<ProjectViewModel> Projects { get; set; }

        [JsonProperty("tenants")]
        public IList<TenantViewModel> Tenants { get; set; }

        [JsonProperty("users")]
        public IList<UserIdentifier> Users { get; set; }

        [JsonProperty("modules")]
        public IList<string> Modules { get; set; }

        [JsonProperty("passwordPolicyTypes")]
        public IList<PasswordPolicyTypeBasicModel> PasswordPolicyTypes { get; set; }
    }
}
