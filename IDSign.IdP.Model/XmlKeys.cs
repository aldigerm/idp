﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class XmlKey
    {
        public Guid Id { get; set; }
        public string Xml { get; set; }
    }
}
