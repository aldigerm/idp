﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class SetPasswordReturn
    {
        public bool IsValid { get; set; }
        public List<string> Errors { get; set; }
    }
}
