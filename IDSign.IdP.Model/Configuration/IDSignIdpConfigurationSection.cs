﻿using IdentityServer4.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace IDSign.IdP.Model.Configuration
{
    public enum ProviderType
    {
        InMemory,
        MSSQL,
        ActiveDirectory,
        MariaDB,
        SPID,
        RemoteRepository
    }
    public class IDSignIdpConfigurationSection
    {
        [JsonProperty("PublicBaseUri")]
        public string PublicBaseUri { get; set; }

        [JsonProperty("PublicOrigin")]
        public string PublicOrigin { get; set; }

        [JsonProperty("Issuer")]
        public string Issuer { get; set; }

        [JsonProperty("IdPAdmin")]
        public IdPAdmin IdPAdmin { get; set; }

        [JsonProperty("CorsSettings")]
        public CorsSettings CorsSettings { get; set; }

        [JsonProperty("Providers")]
        public Providers Providers { get; set; }

        [JsonProperty("MultiModuleClients")]
        public List<MultiModuleClient> MultiModuleClients { get; set; }

        [JsonProperty("IdentityResources")]
        public List<IdPIdentityResources> IdentityResources { get; set; }

        [JsonProperty("SettingsName")]
        public string SettingsName { get; set; }

        [JsonProperty("UseHttps")]
        public bool UseHttps { get; set; }

        [JsonProperty("UseHttpsPort")]
        public int UserHttpsPort { get; set; }

        [JsonProperty("TokenCleanupInterval")]
        public int? TokenCleanupInterval { get; set; }

        [JsonProperty("CertificateConfiguration")]
        public CertificateConfiguration CertificateConfiguration { get; set; }

        [JsonProperty("DataProtectionCertificateConfiguration")]
        public CertificateConfiguration DataProtectionCertificateConfiguration { get; set; }

        [JsonProperty("CacheBuster")]
        public string CacheBuster { get; set; }
    }


    #region Providers

    public class Providers
    {


        [JsonProperty("ActiveDirectory")]
        public List<ActiveDirectoryProviderElement> ActiveDirectory { get; set; }

        [JsonProperty("InMemory")]
        public List<InMemoryProviderElement> InMemory { get; set; }

        [JsonProperty("MSSQL")]
        public List<MSSQLProviderElement> MSSQL { get; set; }

        [JsonProperty("MariaDB")]
        public List<MariaDBProviderElement> MariaDB { get; set; }

        [JsonProperty("RemoteRepository")]
        public List<RemoteRepositoryProviderElement> RemoteRepository { get; set; }
    }
    #endregion

    #region Base Provider Classes and Interfaces
    public abstract class BaseProvider
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("isFallback")]
        public bool IsFallback { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }
    }

    public interface ISelectableUserService
    {
        bool IsFallback { get; }

        string Name { get; }

        string UserServiceId { get; }
    }

    public class SQLProviderElement : BaseProvider
    {
        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }
    }

    public abstract class BaseUser
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Claims")]
        public List<BaseClaim> Claims { get; set; }
    }

    public abstract class BaseClaim
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    #endregion

    #region Active Directory

    public class ActiveDirectoryProviderElement : BaseProvider
    {

        [JsonProperty("ldapEndpoint")]
        public string LdapEndpoint { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
    #endregion

    #region In Memory
    public class InMemoryProviderElement : BaseProvider
    {
        #region nested types

        public class ClaimElement : BaseClaim
        {
        }

        public class UserElement
        {

            [JsonProperty("username")]
            public string Username { get; set; }

            [JsonProperty("subject")]
            public string Subject { get; set; }

            [JsonProperty("password")]
            public string Password { get; set; }

            [JsonProperty("enabled")]
            public bool Enabled { get; set; }

            [JsonProperty("Claims")]
            public List<ClaimElement> Claims { get; set; }
        }

        #endregion


        [JsonProperty("Users")]
        public List<UserElement> Users { get; set; }

    }

    public class InMemoryUserConfig
    {
        #region nested types

        public class InMemoryClaim : BaseClaim
        {
        }

        public class InMemoryUser : BaseUser
        {
            public string Subject { get; set; }
            public new List<InMemoryClaim> Claims { get; set; }
        }

        #endregion
        public List<InMemoryUser> Users { get; set; }

    }
    #endregion

    #region MSSQL

    public class MSSQLProviderElement : SQLProviderElement
    {
    }

    public class MSSQLUser : BaseUser
    {
        [JsonProperty("Claims")]
        public new List<MSSQLClaim> Claims { get; set; }
    }

    public class MSSQLClaim : BaseClaim
    {
    }

    #endregion

    #region MariaDB

    public class MariaDBProviderElement : SQLProviderElement
    {
    }

    public class MariaDBUser : BaseUser
    {
        [JsonProperty("Claims")]
        public new List<MariaDBClaim> Claims { get; set; }
    }

    public class MariaDBClaim : BaseClaim
    {
    }

    #endregion

    #region RemoteRepository

    public class RemoteRepositoryProviderElement : BaseProvider
    {
        [JsonProperty("endpointBaseUri")]
        public string EndpointBaseUri { get; set; }

        [JsonProperty("idPBaseUri")]
        public string IdPBaseUri { get; set; }

        [JsonProperty("idPBaseAppName")]
        public string IdPBaseAppName { get; set; }

        [JsonProperty("baseAppName")]
        public string BaseAppName { get; set; }

        [JsonProperty("tenantClaimName")]
        public string TenantClaimName { get; set; }

        [JsonProperty("projectClaimName")]
        public string ProjectClaimName { get; set; }

        [JsonProperty("endpointUsername")]
        public string EndpointUsername { get; set; }

        [JsonProperty("setNewPasswordUri")]
        public string SetNewPasswordUri { get; set; }
    }


    #endregion

    #region MultiModule

    public class MultiModuleClient
    {
        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        [JsonProperty("MultiModuleProviders")]
        public List<MultiModuleProvider> MultiModuleProviders { get; set; }

    }

    public class MultiModuleProvider
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("providerId")]
        public string ProviderId { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        //[JsonProperty("providerType")]
        //public string ProviderType { get; set; }
    }

    #endregion

    public class IdPScope
    {
        //
        // Summary:
        //     Name of the scope. This is the value a client will use to request the scope.
        [JsonProperty("name")]
        public string Name { get; set; }
        //
        // Summary:
        //     Display name. This value will be used e.g. on the consent screen.
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
        //
        // Summary:
        //     Description. This value will be used e.g. on the consent screen.
        [JsonProperty("description")]
        public string Description { get; set; }
        //
        // Summary:
        //     Specifies whether the user can de-select the scope on the consent screen. Defaults
        //     to false.
        [JsonProperty("required")]
        public bool Required { get; set; }
        //
        // Summary:
        //     Specifies whether the consent screen will emphasize this scope. Use this setting
        //     for sensitive or important scopes. Defaults to false.
        [JsonProperty("emphasize")]
        public bool Emphasize { get; set; }
        //
        // Summary:
        //     Specifies whether this scope is shown in the discovery document. Defaults to
        //     true.
        [JsonProperty("showInDiscoveryDocument")]
        public bool ShowInDiscoveryDocument { get; set; }
        //
        // Summary:
        //     List of user-claim types that should be included in the access token.
        [JsonProperty("userClaims")]
        public ICollection<string> UserClaims { get; set; }
    }

    public class IdPIdentityResources : Resource
    {

    }

    public class CertificateConfiguration
    {
        [JsonProperty("KeyType")]
        public string KeyType { get; set; }

        [JsonProperty("StoreCertificateConfiguration")]
        public StoreCertificateConfiguration StoreCertificateConfiguration { get; set; }

        [JsonProperty("FileCertificateConfiguration")]
        public FileCertificateConfiguration FileCertificateConfiguration { get; set; }

        [JsonProperty("AzureCertificateConfiguration")]
        public AzureCertificateConfiguration AzureCertificateConfiguration { get; set; }

    }

    public class StoreCertificateConfiguration
    {
        [JsonProperty("KeyStoreIssuer")]
        public string KeyStoreIssuer { get; set; }
    }

    public class FileCertificateConfiguration
    {
        [JsonProperty("KeyFilePath")]
        public string KeyFilePath { get; set; }
        [JsonProperty("KeyFilePassword")]
        public string KeyFilePassword { get; set; }
    }

    public class AzureCertificateConfiguration
    {

        [JsonProperty("ClientId")]
        public string ClientId { get; set; }
        [JsonProperty("ClientSecret")]
        public string ClientSecret { get; set; }
        [JsonProperty("SecretIdentifier")]
        public string SecretIdentifier { get; set; }
    }
}