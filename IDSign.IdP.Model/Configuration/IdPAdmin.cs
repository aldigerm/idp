﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model.Configuration
{
    public class IdPAdmin
    {
        [JsonProperty("Authority")]
        public string Authority { get; set; }

        [JsonProperty("Users")]
        public string[] Users { get; set; }

        [JsonProperty("ApiName")]
        public string ApiName { get; set; }

    }
}
