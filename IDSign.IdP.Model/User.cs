﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace IDSign.IdP.Model
{
    //
    // Summary:
    //     In-memory user object for testing. Not intended for modeling users in production.
    public class User
    {
        public User()
        {

        }

        //
        // Summary:
        //     Gets or sets the subject identifier.
        public string SubjectId { get; set; }
        //
        // Summary:
        //     Gets or sets the username.
        public string Username { get; set; }
        //
        // Summary:
        //     Gets or sets the password.
        public string Password { get; set; }
        //
        // Summary:
        //     Gets or sets the provider id.
        public string ProviderId { get; set; }
        //
        // Summary:
        //     Gets or sets the provider subject identifier.
        public string ProviderSubjectId { get; set; }
        //
        // Summary:
        //     Gets or sets if the user is active.
        public bool IsActive { get; set; }
        //
        // Summary:
        //     Gets or sets the claims.
        public IList<Claim> Claims { get; set; }
    }
}
