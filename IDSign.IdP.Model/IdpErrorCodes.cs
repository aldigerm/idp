﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class IdpErrorCodes
    {
        public readonly static SessionError USER_NOT_AUTHENTICATED_INVALID_OTP_ERROR = new SessionError()
        {
            ErrorCode = "USER_NOT_AUTHENTICATED_INVALID_OTP_ERROR",
            ErrorMessage = "The OTP is invalid"
        };
        public readonly static SessionError OTP_FAILED_TO_SEND_ERROR = new SessionError()
        {
            ErrorCode = "OTP_UNABLE_TO_SEND_ERROR",
            ErrorMessage = "Unable to send the OTP. Please try again."
        };
        public readonly static SessionError OTP_REQUEST_THRESHOLD_REACHED_ERROR = new SessionError()
        {
            ErrorCode = "OTP_REQUEST_THRESHOLD_REACHED_ERROR",
            ErrorMessage = "There were too many OTP requests sent."
        };
        public readonly static SessionError USER_SESSION_NOT_FOUND_OR_EXPIRED = new SessionError()
        {
            ErrorCode = "USER_SESSION_NOT_FOUND_OR_EXPIRED",
            ErrorMessage = "User session was not found or it expired"
        };
    }
}
