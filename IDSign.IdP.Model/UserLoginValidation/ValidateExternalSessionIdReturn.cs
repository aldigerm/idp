﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class ValidateExternalSessionIdReturn : BaseValidateReturn
    { 
        public string ClientId { get; set; }
        public DateTimeOffset? ExpiryDateTime { get; set; }
        public bool RequireOTP { get; set; }
        public DateTimeOffset? NextOTPRequestAllowedAt { get; set; }
        public string PhoneNumber { get; set; }
        public string CensoredPhoneNumber { get; set; }
        public string ProjectCode { get; set; }
        public string TenantCode { get; set; }
        public string ExpectedOTP { get; set; }
    }
}
