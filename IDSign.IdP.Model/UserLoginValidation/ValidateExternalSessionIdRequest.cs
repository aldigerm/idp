﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class ValidateExternalSessionIdRequest
    {
        public string Easid { get; set; }    
        public string OTPValue { get; set; }
    }

    public class ExternalSessionActions
    {
        public const string RequestNewOTP = "requestNewOTP";
        public const string SubmitOTP = "submitOTP";
    }



}
