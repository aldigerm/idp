﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class ValidateCredentialsReturn : BaseValidateReturn
    { 
        public bool IsTemporaryPassword { get; set; }
        public bool ResetPasswordRequired { get; set; }
        public string RedirectToUri { get; set; } 
    }
}
