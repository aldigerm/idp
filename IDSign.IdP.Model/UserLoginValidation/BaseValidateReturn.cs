﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Model
{
    public class BaseValidateReturn
    {
        public SessionError Error { get; set; }
        public bool IsValid { get; set; }
        public User User { get; set; }
    }
}
