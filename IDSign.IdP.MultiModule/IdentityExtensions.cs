﻿using IDSign.IdP.Model.Configuration;
using IDSign.IdP.MultiModule;
using IDSign.IdP.MultiModule.Infrastructure;
using IDSign.IdP.MultiModule.Module;
using IDSign.IdP.MultiModule.PasswordValidators;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MultiModuleIdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddMultiModuleUserStore(this IIdentityServerBuilder builder, IDSignIdpConfigurationSection configurationSection, ILoggerFactory loggerFactory)
        {
            var logger = loggerFactory.CreateLogger("MultiModuleIdentityServerBuilderExtensions");
            List<BasePasswordValidator> svcs = new List<BasePasswordValidator>();
            if (configurationSection.Providers.InMemory != null)
            {
                foreach (var item in configurationSection.Providers.InMemory)
                {
                    logger.LogDebug($"+++Adding InMemory provider with id {item.Id}");
                    svcs.Add(new InMemoryPasswordValidator(item.Id, item.IsFallback, item.Order).Load(item));
                }
            }
            if (configurationSection.Providers.MSSQL != null)
            {
                foreach (var item in configurationSection.Providers.MSSQL)
                {
                    logger.LogDebug($"+++Adding MSSQL provider with id {item.Id}");
                    svcs.Add(new MSSQLPasswordValidator(item.Id, item.IsFallback, item.Order, item.ConnectionString));
                }
            }
            if (configurationSection.Providers.MariaDB != null)
            {
                foreach (var item in configurationSection.Providers.MariaDB)
                {
                    logger.LogDebug($"+++Adding MariaDB provider with id {item.Id}");
                    svcs.Add(new MariaDBPasswordValidator(item.Id, item.IsFallback, item.Order, item.ConnectionString));
                }
            }
            if (configurationSection.Providers.ActiveDirectory != null)
            {
                foreach (var item in configurationSection.Providers.ActiveDirectory)
                {
                    logger.LogDebug($"+++Adding ActiveDirectory provider with id {item.Id}");
                    svcs.Add(new ActiveDirectoryPasswordValidator(item.Id, item.IsFallback, item.Order, item.LdapEndpoint, item.Username, item.Password));
                }
            }
            if (configurationSection.Providers.RemoteRepository != null)
            {
                foreach (var item in configurationSection.Providers.RemoteRepository)
                {
                    logger.LogDebug($"+++Adding RemoteRepository provider with id {item.Id}");
                    svcs.Add(new RemoteRepositoryPasswordValidator(item.Id, item.IsFallback, item.Order, item.EndpointBaseUri, item.IdPBaseUri, item.IdPBaseAppName, item.BaseAppName, item.TenantClaimName, item.ProjectClaimName, item.EndpointUsername, item.EndpointUsername, item.SetNewPasswordUri));
                }
            }
            ModuleRepository.PasswordValidators = svcs;

            List<ClientMultiModule> clients = new List<ClientMultiModule>();
            foreach (var client in configurationSection.MultiModuleClients)
            {
                List<BasePasswordValidator> passwordValidators = new List<BasePasswordValidator>();
                foreach (var multiModuleProvider in client.MultiModuleProviders.OrderBy(m => m.Order))
                {
                    BasePasswordValidator provider = ModuleRepository.GetPasswordValidator(multiModuleProvider.ProviderId);
                    if (provider == null)
                    {
                        logger.LogError($"Provider not found. Client : '{client.ClientId}' ProviderId : '{multiModuleProvider.ProviderId}' Configuratin Id : '{multiModuleProvider.Id}' ");
                    }
                    else
                    {
                        passwordValidators.Add(provider);
                    }
                }
                logger.LogDebug($"+++Adding Client {client.ClientId} with {passwordValidators.Count} passwordValidators");
                clients.Add(new ClientMultiModule(client.ClientId, passwordValidators));
            }

            ModuleRepository.Clients = clients;

            builder.AddProfileService<MultiModuleProfileService>();
            builder.AddResourceOwnerValidator<MultiModuleResourceOwnerPasswordValidator>();

            // add multimodule services
            builder.Services.AddMultiModuleServices();

            return builder;
        }
    }
}