﻿using IDSign.IdP.MultiModule.Impl;
using IDSign.IdP.MultiModule.Module;
using IDSign.IdP.MultiModule.PasswordValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDSign.IdP.MultiModule
{
    public class ModuleRepository
    {
        public static List<BasePasswordValidator> PasswordValidators { get; set; }
        public static BasePasswordValidator GetPasswordValidator(string name)
        {
            return PasswordValidators.Where(pv => pv.Id == name).SingleOrDefault();
        }

        public static List<ClientMultiModule> Clients { get; set; }
        protected static ClientMultiModule GetClient(string clientId)
        {
            if (Clients != null)
            {
                return Clients.Where(cl => cl.ClientId.Equals(clientId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            }
            else
            {
                return null;
            }
        }
        public static List<BasePasswordValidator> GetFallBackServices()
        {

            var services = PasswordValidators.Where(s => s.IsFallback).ToList();
            if (services == null)
            {
                throw new Exception($"No fallback providers found. " +
                    "Fallback providers can be specified by adding 'isFallback' property to provider element e.g. <MSSQL id=\"test-mssql\" isFallback=\"true\" .... /> ");
            }
            return services;
        }

        public static List<BasePasswordValidator> GetServices(string clientId)
        {
            var services = GetClient(clientId)?.Services;

            // if there are no services for this client,
            // then we get all services with the fallback id
            if (services == null)
            {
                services = GetFallBackServices();
                if (services == null)
                {
                    throw new Exception($"No services found for clientid '{clientId}' and no fallback providers found. " +
                        "Fallback name can be specified by adding 'isFallback' property to provider element e.g. <MSSQL id=\"test-mssql\" isFallback=\"true\" .... /> ");
                }
            }
            return services;
        }

        public static BasePasswordValidator GetServicesById(string providerId)
        {
            return PasswordValidators.Where(pv => pv.Id == providerId).SingleOrDefault();
        }
    }
}
