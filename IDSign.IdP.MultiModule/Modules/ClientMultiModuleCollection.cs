﻿using IDSign.IdP.MultiModule.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Impl
{
    public class ClientMultiModuleCollection
    {
        protected string mClientId { get; set; }
        protected List<ClientMultiModule> _services;

        public ClientMultiModuleCollection(string clientId, List<ClientMultiModule> services)
        {
            this.mClientId = clientId;
            _services = services;
        }
    }
}
