﻿using IDSign.IdP.MultiModule.PasswordValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Module
{
    public class ClientMultiModule
    {
        public string ClientId { get; set; }
        public List<BasePasswordValidator> Services { get; set; }

        public ClientMultiModule(string clientId, List<BasePasswordValidator> services)
        {
            this.ClientId = clientId;
            
            // these should have been sorted already before initialisation
            Services = services;            
        }
    }
}
