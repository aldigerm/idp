﻿using IDSign.IdP.MultiModule.Helpers.Model;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public class HttpClientWrapper : IDisposable
    {
        protected HttpClient _client = null;
        protected ILogger<HttpClientWrapper> _logger;

        public HttpClientWrapper(string baseUrl, ILogger<HttpClientWrapper> logger)
        {
            _logger = logger;
            _client = new HttpClient();

            if (baseUrl.EndsWith("/", StringComparison.CurrentCulture))
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);

            _client.BaseAddress = new Uri(baseUrl);
        }

        public HttpClient HttpClient
        {
            get
            {
                return _client;
            }
            set
            {
                _client = value;
            }
        }

        public async Task<Result<AuthToken>> AuthenticateAsync(string idPBaseUri, string username, string password, string tokenBody)
        {
            return await AuthenticateAsync(idPBaseUri, Settings.IDP, username, password, tokenBody);
        }

        public async Task<Result<AuthToken>> AuthenticateAsync(string idPBaseUri, string idPBaseAppName, string username, string password, string tokenBody)
        {
            // use temporary client to get the auth token
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(idPBaseUri);

                // set Basic authorization token based on username/passeord
                client.SetBasicAuthentication(username, password);

                // endpoint to get token
                var controller = string.Concat(idPBaseAppName, Settings.IDPTokenApi);

                // use tokenbody from settings to create content for post request 
                var content = new StringContent(tokenBody, Encoding.UTF8, ContentType.FormURLEncoded);

                _logger.LogDebug($"Getting token BaseAddress='{client.BaseAddress}' AppName='{idPBaseAppName}' Controller='{controller}' ApiCalled='{Settings.IDPTokenApi}'");

                var response = await client.PostAsync(controller, content);

                Result<AuthToken> result = new Result<AuthToken>();

                if (response.IsSuccessStatusCode)
                {
                    result.Value = JsonConvert.DeserializeObject<AuthToken>(await response.Content.ReadAsStringAsync());

                    if (!string.IsNullOrWhiteSpace(result.Value.AccessToken))
                    {
                        // token is recieved successfully
                        _client.SetBearerToken(result.Value.AccessToken);
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Error = "Did not recieve token";
                    }
                }
                else
                {
                    result.Success = false;
                    result.Error = response.ReasonPhrase;
                    _logger.LogError($"Error {response.StatusCode} while getting token : {response.ReasonPhrase}");
                }

                return result;
            }
        }

        public async Task<Result<TOutbound>> PutAsync<TInbound, TOutbound>(TInbound data, string id, String controller)
        {

            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, ContentType.JSON);

            if (controller.EndsWith("/", StringComparison.CurrentCulture))
                controller = controller.Substring(0, controller.Length - 1);

            var api = controller + "/" + id;
            var result = await _client.PutAsync(api, content);

            if (result.IsSuccessStatusCode)
            {
                return new Result<TOutbound>()
                {
                    Success = true,
                    Value = JsonConvert.DeserializeObject<TOutbound>(await result.Content.ReadAsStringAsync())
                };
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while putting {_client.BaseAddress}{api} : {result.ReasonPhrase}");
                var r = new Result<TOutbound>();
                r.Value = default(TOutbound);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }
        }

        public async Task<Result<TOutbound>> PostBinaryDataAsync<TOutbound>(byte[] byteData, String controller)
        {

            using (ByteArrayContent content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                var result = await _client.PostAsync(controller, content);

                if (result.IsSuccessStatusCode)
                {
                    return new Result<TOutbound>()
                    {
                        Success = true,
                        Value = JsonConvert.DeserializeObject<TOutbound>(await result.Content.ReadAsStringAsync())
                    };
                }
                else
                {
                    _logger.LogError($"Error {result.StatusCode} while posting {_client.BaseAddress}{controller} : {result.ReasonPhrase}");
                    var r = new Result<TOutbound>();
                    r.Value = default(TOutbound);
                    r.Error = result.ReasonPhrase;
                    r.StatusCode = result.StatusCode;
                    r.Message = result.ReasonPhrase;
                    r.Success = false;
                    return r;
                }
            }

        }

        public async Task<Result<TOutbound>> PostAsync<TInbound, TOutbound>(TInbound data, String controller)
        {
            return await PostAsync<TInbound, TOutbound>(data, controller, ContentType.JSON);
        }

        public async Task<Result<TOutbound>> PostAsync<TInbound, TOutbound>(TInbound data, String controller, string contentType)
        {

            StringContent content = null;

            if (contentType.Equals(ContentType.Raw))
            {
                content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8);
            }
            else
            {
                content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, contentType);
            }

            var result = await _client.PostAsync(controller, content);

            if (result.IsSuccessStatusCode)
            {
                return new Result<TOutbound>()
                {
                    Success = true,
                    Value = JsonConvert.DeserializeObject<TOutbound>(await result.Content.ReadAsStringAsync())
                };
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while posting {_client.BaseAddress}{controller} : {result.ReasonPhrase}");
                var r = new Result<TOutbound>();
                r.Value = default(TOutbound);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }

        }


        public async Task<Result<IList<TOutbound>>> GetListAsync<TOutbound>(String controller, IDictionary<String, String> parameters = null)
        {

            HttpResponseMessage result = null;

            if (controller.EndsWith("/", StringComparison.CurrentCulture))
                controller = controller.Substring(0, controller.Length - 1);
            var uri = controller;
            if (parameters == null)
                result = await _client.GetAsync(controller);
            else
            {

                uri = QueryHelpers.AddQueryString(controller, parameters);
                result = await _client.GetAsync(uri);
            }
            if (result.IsSuccessStatusCode)
            {
                return new Result<IList<TOutbound>>()
                {
                    Success = true,
                    Value = JsonConvert.DeserializeObject<IList<TOutbound>>(await result.Content.ReadAsStringAsync())
                };
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while getting list {_client.BaseAddress}{uri} : {result.ReasonPhrase}");
                var r = new Result<IList<TOutbound>>();
                r.Value = default(IList<TOutbound>);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }
        }


        public async Task<Result<TOutbound>> GetAsync<TOutbound>(String controller, String id, IDictionary<String, String> parameters = null)
        {
            HttpResponseMessage result = null;

            if (controller.EndsWith("/", StringComparison.CurrentCulture))
                controller = controller.Substring(0, controller.Length - 1);

            if (String.IsNullOrWhiteSpace(id))
                id = String.Empty;

            if (id.StartsWith("/", StringComparison.CurrentCulture))
                id = id.Substring(1);

            string endPoint = "";

            if (parameters == null)
                endPoint = controller + "/" + id;
            else
                endPoint = controller + "/" + id + parameters.ToQueryString();
            try
            {
                result = await _client.GetAsync(endPoint);
            }
            catch (Exception e)
            {
                _logger.LogError($"GET request gave an error for {endPoint}", e);
                var r = new Result<TOutbound>();
                r.Value = default(TOutbound);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }

            if (result.IsSuccessStatusCode)
            {
                var r = new Result<TOutbound>();

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter> { new DateTimeOffsetFixingConverter() },
                    DateParseHandling = DateParseHandling.None
                };
                string c = await result.Content.ReadAsStringAsync();
                if (!String.IsNullOrWhiteSpace(c))
                {
                    r.Value = JsonConvert.DeserializeObject<TOutbound>(c, settings);
                }
                else
                {
                    r.Value = default(TOutbound);
                }
                r.Success = true;
                return r;
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while getting {_client.BaseAddress}{endPoint} : {result.ReasonPhrase}");
                var r = new Result<TOutbound>();
                try
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter> { new DateTimeOffsetFixingConverter() },
                        DateParseHandling = DateParseHandling.None
                    };

                    string c = await result.Content.ReadAsStringAsync();
                    r.Value = JsonConvert.DeserializeObject<TOutbound>(c, settings);
                }
                catch
                {
                    r.Value = default(TOutbound);
                }
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }
        }

        public async Task<Result<byte[]>> GetBytesAsync(String controller, String id, IDictionary<String, String> parameters = null)
        {
            HttpResponseMessage result = null;

            if (controller.EndsWith("/", StringComparison.CurrentCulture))
                controller = controller.Substring(0, controller.Length - 1);

            if (id.StartsWith("/", StringComparison.CurrentCulture))
                id = id.Substring(1);

            string endPoint = "";

            if (parameters == null)
                endPoint = controller + "/" + id;
            else
                endPoint = controller + "/" + id + parameters.ToQueryString();

            result = null;
            try
            {
                result = await _client.GetAsync(endPoint);
            }
            catch (Exception e)
            {
                _logger.LogError($"GET bytes request gave an error for {endPoint}", e);
                var r = new Result<byte[]>();
                r.Message = e.Message;
                r.Success = false;
                return r;
            }

            if (result.IsSuccessStatusCode)
            {
                var r = new Result<byte[]>();
                try
                {
                    var c = await result.Content.ReadAsByteArrayAsync();
                    r.Value = c;
                    r.Success = true;
                }
                catch (Exception e)
                {
                    _logger.LogError($"GET bytes request gave an error for {endPoint}", e);
                    r.Message = e.Message;
                    r.Success = false;
                }
                return r;
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while getting bytes {_client.BaseAddress}{endPoint} : {result.ReasonPhrase}");

                var r = new Result<byte[]>();
                r.Value = default(byte[]);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }
        }


        public async Task<Result<TOutbound>> DeleteAsync<TOutbound>(String controller, string id, IDictionary<String, String> parameters = null)
        {
            HttpResponseMessage result = null;
            var endpoint = "";
            if (parameters == null)
                endpoint = controller + "/" + id;
            else
                endpoint = controller + "/" + id + parameters.ToQueryString();

            result = await _client.DeleteAsync(endpoint);

            if (result.IsSuccessStatusCode)
            {
                return new Result<TOutbound>()
                {
                    Success = true //No Content
                };
            }
            else
            {
                _logger.LogError($"Error {result.StatusCode} while deleting {_client.BaseAddress}{endpoint} : {result.ReasonPhrase}");

                var r = new Result<TOutbound>();
                r.Value = default(TOutbound);
                r.Error = result.ReasonPhrase;
                r.StatusCode = result.StatusCode;
                r.Message = result.ReasonPhrase;
                r.Success = false;
                return r;
            }
        }


        public void Dispose()
        {
            _client.Dispose();
        }
    }



}