﻿using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.Helpers.Model;
using IDSign.IdP.MultiModule.Helpers.Rest.API.Implementation;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Helpers.Rest.API
{
    public interface IRemoteRepositoryApiProvider
    {
        HttpClientWrapper Client
        {
            get;
            set;
        }
        string BaseAppName
        {
            get;
            set;
        }

        Task InitHttpClient(string baseUri,string idPBaseUri, string idPBaseAppName, string username, string password, AuthType type);
        Task<Result<UserViewModel>> GetDetailAsync(string username, string tenant, string project);
        Task<Result<UserViewModel>> LoginAsync(string username, string password, string tenant, string project);
        Task<Result<TemporaryUserViewModel>> LoginUsingExternalAccessSessionIdAsync(string easid, string otpValue);
        Task<Result<object>> SetPasswordAsync(string username, string tenant, string project, string password);
        
        #region OTP

        Task<Result<TemporaryUserViewModel>> SubmitOTPAsync(string easid, string otpValue);
        Task<Result<TemporaryUserViewModel>> RequestOTPDeliveryAsync(string easid); 
        
        #endregion
    }
}
