﻿using IDSign.IdP.Core.Config;
using IDSign.IdP.Core.Helpers;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.Helpers.Model;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Helpers.Rest.API.Implementation
{
    public class RemoteRepositoryApi : IRemoteRepositoryApiProvider
    {
        private ILogger<RemoteRepositoryApi> _logger;
        private ILogger<HttpClientWrapper> _loggerWrapper;
        private string LoginUri = new AppSettingsConfigValue("RemoteRepository.Uris.LoginUri", "api/user/login");
        private string DetailsUri = new AppSettingsConfigValue("RemoteRepository.Uris.DetailsUri", "api/user/detail");
        private string SetPasswordUri = new AppSettingsConfigValue("RemoteRepository.Uris.SetPasswordUri", "api/user/setPassword");

        #region Temporary User

        private string LoginTemporaryUserUri = new AppSettingsConfigValue("RemoteRepository.Uris.LoginTemporaryUsercUri", "api/TemporaryUser/login");

        #endregion

        #region OTP

        private string SubmitOTPUri = new AppSettingsConfigValue("RemoteRepository.Uris.SendOTPemporaryUserOTPUri", "api/TwoFAController/SubmitOTP");
        private string RequestOTPDelivery = new AppSettingsConfigValue("RemoteRepository.Uris.RequestOTPUri", "api/TwoFAController/RequestDelivery");

        #endregion

        public RemoteRepositoryApi(
            ILogger<RemoteRepositoryApi> logger,
            ILogger<HttpClientWrapper> loggerWrapper
            )
        {
            _logger = logger;
            _loggerWrapper = loggerWrapper;
        }

        public HttpClientWrapper Client { get; set; }

        public string BaseAppName
        {
            get;
            set;
        }

        public async Task<Result<UserViewModel>> GetDetailAsync(string username, string tenant, string project)
        {
            var userViewModel = new UserIdentifier(username, tenant, project);
            string uri = UriHelper.CombineUrls(BaseAppName, DetailsUri);
            _logger.LogDebug("Getting user details login with " + uri);
            return await Client.GetAsync<UserViewModel>(uri, null, userViewModel.ToDictionary());
        }

        public async Task InitHttpClient(string baseUri, string idPBaseUri, string idPBaseAppName, string username, string password, AuthType type)
        {
            Client = new HttpClientWrapper(baseUri, _loggerWrapper);
            string body = "";
            switch (type)
            {
                case AuthType.Detail:
                    body = Settings.RemoteRepositoryAdminTokenBody;
                    break;
                case AuthType.Login:
                    body = Settings.RemoteRepositoryAdminTokenBody;
                    break;
            }
            await Client.AuthenticateAsync(idPBaseUri, idPBaseAppName, username, password, body);
        }

        public async Task<Result<UserViewModel>> LoginAsync(string username, string password, string tenant, string project)
        {
            var loginModel = new LoginViewModel(username, password, tenant, project);
            string uri = UriHelper.CombineUrls(BaseAppName, LoginUri);
            _logger.LogDebug("Checking user login with " + uri);
            return await Client.PostAsync<LoginViewModel, UserViewModel>(loginModel, uri);
        }

        public async Task<Result<TemporaryUserViewModel>> LoginUsingExternalAccessSessionIdAsync(string easid, string otpValue)
        {
            var loginModel = new OTPTemporaryUserLoginRequestModel()
            {
                ExternalAccessSessionId = easid,
                OTP = otpValue
            };
            string uri = UriHelper.CombineUrls(BaseAppName, LoginTemporaryUserUri);
            _logger.LogDebug("Checking ExternalAccessSessionId with " + uri);
            return await Client.PostAsync<BaseTemporaryUserLoginRequestModel, TemporaryUserViewModel>(loginModel, uri);
        }

        public async Task<Result<TemporaryUserViewModel>> SubmitOTPAsync(string easid, string otpValue)
        {
            var loginModel = new OTPTemporaryUserLoginRequestModel()
            {
                ExternalAccessSessionId = easid,
                OTP = otpValue
            };
            string uri = UriHelper.CombineUrls(BaseAppName, SubmitOTPUri);
            _logger.LogDebug("Checking ExternalAccessSessionId with " + uri);
            return await Client.PostAsync<BaseTemporaryUserLoginRequestModel, TemporaryUserViewModel>(loginModel, uri);
        }

        public async Task<Result<TemporaryUserViewModel>> RequestOTPDeliveryAsync(string easid)
        {
            var loginModel = new BaseTemporaryUserLoginRequestModel()
            {
                ExternalAccessSessionId = easid
            };
            string uri = UriHelper.CombineUrls(BaseAppName, RequestOTPDelivery);
            _logger.LogDebug("Sending request for OTP " + uri);
            return await Client.PostAsync<BaseTemporaryUserLoginRequestModel, TemporaryUserViewModel>(loginModel, uri);
        }

        public async Task<Result<object>> SetPasswordAsync(string username, string tenant, string project, string password)
        {
            var setPasswordModel = new UpdatePasswordViewModel()
            {
                Identifier = new UserIdentifier(username, tenant, project),
                Password = password,
                IsLoginReset = true
            };
            string uri = UriHelper.CombineUrls(BaseAppName, SetPasswordUri);
            _logger.LogDebug("Setting password with " + uri);
            return await Client.PostAsync<UpdatePasswordViewModel, object>(setPasswordModel, uri);
        }
    }

    public enum AuthType
    {
        Login,
        Detail
    }
}
