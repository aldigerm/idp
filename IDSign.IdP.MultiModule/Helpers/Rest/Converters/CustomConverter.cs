﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public class DateTimeOffsetFixingConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTimeOffset?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string rawDate = (string)reader.Value;
            DateTimeOffset date;

            // First try to parse the date string as is (in case it is correctly formatted)
            if (DateTimeOffset.TryParse(rawDate, out date))
            {
                return date;
            }
            
            return null;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

}
