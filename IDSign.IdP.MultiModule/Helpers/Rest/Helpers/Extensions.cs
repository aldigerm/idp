﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public static partial class Extension
    {
        public static string ToQueryString(this IDictionary<string, string> dictionary)
        {
            return '?' + string.Join("&", dictionary.Select(p => p.Key + '=' + Uri.EscapeUriString(p.Value)).ToArray());
        }
    }

    public static class AuthenticationExtensions 
    {
        public static void SetAuthenticationHeader(this HttpClient _client, AuthTypes type, string username, string password)
        {
            _client.SetAuthenticationHeader(type,EncodeCredential(username, password));
        }

        public static void SetAuthenticationHeader(this HttpClient _client, AuthTypes type, string token)
        {
            _client.DefaultRequestHeaders.Authorization =
                       new AuthenticationHeaderValue(type.ToString(), token);
        }

        private static string EncodeCredential(string userName, string password)
        {
            string credential = String.Format("{0}:{1}", userName, password);
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(credential));
        }

        public static void SetContentType(this HttpClient _client, string contentType)
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
        }
    }

}
