﻿using System;
namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public enum AuthTypes
    {
        Basic,
        Bearer
    }

    public static class ContentType
    {
        public static readonly string Raw = "<Raw>";
        public static readonly string JSON = "application/json";
        public static readonly string FormURLEncoded = "application/x-www-form-urlencoded";
    }
}
