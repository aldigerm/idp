﻿using IDSign.IdP.Core.Config;
using System;
using System.Linq;
using System.Web;

namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public static class Settings
    {
        public static readonly string IDP = new AppSettingsConfigValue("IDSign.Idp", "/idsign.idp.2");
        public static readonly string IDPTokenApi = new AppSettingsConfigValue("IDSign.IdpConnectController", "/connect/token");

        public static readonly string RemoteRepositoryAdminTokenBody = new AppSettingsConfigValue("RemoteRepository.WebHub.TokenBody", "grant_type=client_credentials&scope=remote-user-repository-admin remote-user-repository-basic");
    }
}