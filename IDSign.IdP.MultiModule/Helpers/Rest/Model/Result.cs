﻿using System;
using System.Net;

namespace IDSign.IdP.MultiModule.Helpers.Model
{
    public class Result<T>
    {
        public string Error { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
        public T Value { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
