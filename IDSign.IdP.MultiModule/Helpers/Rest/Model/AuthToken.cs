﻿using System;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace IDSign.IdP.MultiModule.Helpers.Rest
{
    public class AuthToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public long ExpiresIn { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }

}
