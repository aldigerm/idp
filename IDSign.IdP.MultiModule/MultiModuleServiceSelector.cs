﻿using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.EF;
using IDSign.IdP.MultiModule.PasswordValidators;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule
{
    public interface IMultiModuleResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null);
        Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null);
        Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null);
        Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values);
        Task<SetPasswordReturn> SetPassword(string username, string password, string providerId, Client client, Dictionary<string, string> values);
    }

    public class MultiModuleResourceOwnerPasswordValidator : IMultiModuleResourceOwnerPasswordValidator
    {
        protected ILogger<MultiModuleResourceOwnerPasswordValidator> _logger;
        private readonly string LogPrefix = "+++";
        protected IServiceProvider _ServiceProvider;
        protected ModelContext _ModelContext;
        private readonly IClientStore _clientStore;
        public MultiModuleResourceOwnerPasswordValidator(
            IServiceProvider serviceProvider
            , ModelContext modelContext
            , ILogger<MultiModuleResourceOwnerPasswordValidator> logger
            , IClientStore clientStore)
        {
            _logger = logger;
            _ServiceProvider = serviceProvider;
            _ModelContext = modelContext;
            _clientStore = clientStore;
            _logger.LogDebug($"{LogPrefix} MultiModuleResourceOwnerPasswordValidator initialized.");
        }
        public async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values)
        {
            _logger.LogDebug($"{LogPrefix} MultiModule login started");
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            res.IsValid = false;
            foreach (BasePasswordValidator item in await GetServicesForClientId(client.ClientId))
            {
                res = await item.Init(_ServiceProvider).ValidateCredentials(username, password, client, values);
                if (res != null && res.IsValid)
                {
                    // success!
                    _logger.LogDebug($"{LogPrefix} Found {username} with {item.Id}!");
                    return res;
                }
                _logger.LogDebug($"{LogPrefix} Did not find {username} with {item.Id}!");
            }
            _logger.LogDebug($"{LogPrefix} MultiModule Validate finish");

            return res;
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            _logger.LogDebug($"{LogPrefix} MultiModule Resource owner Validate started");
            foreach (var item in await GetServicesForClientId(context.Request.Client.ClientId))
            {
                await item.Init(_ServiceProvider).ValidateAsync(context);
                if (context.Result != null && !context.Result.IsError)
                {
                    // success!
                    _logger.LogDebug($"{LogPrefix} Found {context.UserName} with {item.Id}!");
                    return;
                }
                _logger.LogDebug($"{LogPrefix} Did not find {context.UserName} with {item.Id}!");
            }
            _logger.LogDebug($"{LogPrefix} MultiModule  Resource owner Validate finish");

            return;
        }

        public async Task<SetPasswordReturn> SetPassword(string username, string password, string providerId, Client client, Dictionary<string, string> values)
        {
            var clientId = client.ClientId;
            _logger.LogDebug($"{LogPrefix} MultiModule set password started. Looking for {providerId} in {clientId}");

            SetPasswordReturn res = new SetPasswordReturn();
            res.IsValid = true;

            // get the services for the client
            List<BasePasswordValidator> _services = ModuleRepository.GetServices(clientId);

            // get the provider which will be used to set the password
            BasePasswordValidator item = _services.FirstOrDefault(s => s.Id == providerId);

            if (item == null)
            {
                _logger.LogError($"Provider not found with id '{providerId}' in '{clientId}' therefore password was not set.");
                return res;
            }

            res = await item.Init(_ServiceProvider).SetPassword(username, password, client, values);
            if (res.IsValid)
            {
                // success!
                _logger.LogDebug($"{LogPrefix} Password set for {username} with {item.Id}!");
                return res;
            }

            _logger.LogDebug($"{LogPrefix} MultiModule set password finish");

            return res;
        }

        public async Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null)
        {
            _logger.LogDebug($"{LogPrefix} MultiModule ValidateExternalAccessSessionIdAsync started");
            var specificClientId = client?.ClientId;
            if (string.IsNullOrWhiteSpace(specificClientId))
            {
                _logger.LogWarning("Client was not provided. The password validators of all clients will all be checked indiviudally.");
            }
            // get all client modules if one wasn't specified when calling the method
            // or find the client module of the passed client object
            foreach (var clientAndValidators in await GetAllServicesForClient(specificClientId))
            {
                var currentClientId = clientAndValidators.Key;
                var validators = clientAndValidators.Value;
                var currentClient = await _clientStore.FindEnabledClientByIdAsync(currentClientId);
                _logger.LogDebug($"{LogPrefix} Found {validators.Count()} for clientId {currentClientId}.");
                foreach (BasePasswordValidator item in validators)
                {
                    _logger.LogDebug($"{LogPrefix} Looking in password validator {item.Id}'");
                    var res = await item.Init(_ServiceProvider).ValidateExternalAccessSessionIdAsync(easidValidateModel, currentClient, values);
                    if (res.IsValid)
                    {
                        // success!
                        _logger.LogDebug($"{LogPrefix} Found successful response with {item.Id} in client '{currentClientId}'!");
                        res.ClientId = currentClientId;
                        return res;
                    }
                    _logger.LogDebug($"{LogPrefix} Did not find user with {item.Id}!");
                }

            }
            _logger.LogDebug($"{LogPrefix} MultiModule Validate finish");
            return BuildInvalidResponse();

        }

        private ValidateExternalSessionIdReturn BuildInvalidResponse()
        {
            ValidateExternalSessionIdReturn notFoundModel = new ValidateExternalSessionIdReturn();
            notFoundModel.IsValid = false;
            return notFoundModel;
        }

        private async Task<IEnumerable<BasePasswordValidator>> GetServicesForClientId(string clientId)
        {
            if (clientId == null)
            {
                return new List<BasePasswordValidator>();
            }
            else
            {
                var l = await GetAllServicesForClient(clientId);
                return l.SelectMany(s => s.Value);
            };
        }

        private async Task<Dictionary<string, IEnumerable<BasePasswordValidator>>> GetAllServicesForClient(string specificClientId = null)
        {
            var result = new Dictionary<string, IEnumerable<BasePasswordValidator>>();
            foreach (var clientModule in ModuleRepository.Clients.Where(c => string.IsNullOrWhiteSpace(specificClientId) || (c.ClientId?.EqualsTrimIgnoreCase(specificClientId) ?? false)))
            {
                var currentClientId = clientModule.ClientId;
                if (string.IsNullOrWhiteSpace(currentClientId))
                    continue;

                _logger.LogDebug($"{LogPrefix} Looking for client with id {currentClientId}");
                var currentClient = await _clientStore.FindEnabledClientByIdAsync(currentClientId);

                if (currentClient == null)
                {
                    _logger.LogError("Client configured in settings couldn't be found in database: " + currentClientId);
                    continue;
                }

                var validators = ModuleRepository.GetServices(currentClientId);
                if (validators?.Any() ?? false)
                {
                    result.Add(currentClientId, validators);
                }
            }
            return result;
        }

        public async Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null)
        {
            _logger.LogDebug($"{LogPrefix} MultiModule ValidateExternalAccessSessionIdAsync started");
            var specificClientId = client?.ClientId;
            if (string.IsNullOrWhiteSpace(specificClientId))
            {
                _logger.LogWarning("Client was not provided. The password validators of all clients will all be checked indiviudally.");
            }
            // get all client modules if one wasn't specified when calling the method
            // or find the client module of the passed client object
            foreach (var clientAndValidators in await GetAllServicesForClient(specificClientId))
            {
                var currentClientId = clientAndValidators.Key;
                var validators = clientAndValidators.Value;
                var currentClient = await _clientStore.FindEnabledClientByIdAsync(currentClientId);
                _logger.LogDebug($"{LogPrefix} Found {validators.Count()} for clientId {currentClientId}.");
                foreach (BasePasswordValidator item in validators)
                {
                    _logger.LogDebug($"{LogPrefix} Looking in password validator {item.Id}'");
                    var res = await item.Init(_ServiceProvider).SubmitOTPAsync(easidValidateModel, currentClient, values);
                    if (res != null && res.IsValid)
                    {
                        // success!
                        _logger.LogDebug($"{LogPrefix} Found successful response with {item.Id} in client '{currentClientId}'!");
                        res.ClientId = currentClientId;
                        return res;
                    }
                    _logger.LogDebug($"{LogPrefix} Did not find user with {item.Id}!");
                }

            }
            _logger.LogDebug($"{LogPrefix} MultiModule Validate finish");
            return BuildInvalidResponse();

        }

        public async Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Dictionary<string, string> values, Client client = null)
        {
            _logger.LogDebug($"{LogPrefix} MultiModule ValidateExternalAccessSessionIdAsync started");
            var specificClientId = client?.ClientId;
            if (string.IsNullOrWhiteSpace(specificClientId))
            {
                _logger.LogWarning("Client was not provided. The password validators of all clients will all be checked indiviudally.");
            }
            // get all client modules if one wasn't specified when calling the method
            // or find the client module of the passed client object
            foreach (var clientAndValidators in await GetAllServicesForClient(specificClientId))
            {
                var currentClientId = clientAndValidators.Key;
                var validators = clientAndValidators.Value;
                var currentClient = await _clientStore.FindEnabledClientByIdAsync(currentClientId);
                _logger.LogDebug($"{LogPrefix} Found {validators.Count()} for clientId {currentClientId}.");
                foreach (BasePasswordValidator item in validators)
                {
                    _logger.LogDebug($"{LogPrefix} Looking in password validator {item.Id}'");
                    var res = await item.Init(_ServiceProvider).RequestNewOTPIdAsync(easidValidateModel, currentClient, values);
                    if (res != null && res.IsValid)
                    {
                        // success!
                        _logger.LogDebug($"{LogPrefix} Found successful response with {item.Id} in client '{currentClientId}'!");
                        res.ClientId = currentClientId;
                        return res;
                    }
                    _logger.LogDebug($"{LogPrefix} Did not find user with {item.Id}!");
                }

            }
            _logger.LogDebug($"{LogPrefix} MultiModule Validate finish");
            return BuildInvalidResponse();

        }
    }

    public class MultiModuleProfileService : IProfileService
    {
        protected ILogger<MultiModuleProfileService> _logger;
        protected IServiceProvider _ServiceProvider;
        protected ModelContext _ModelContext;
        public MultiModuleProfileService(
            IServiceProvider serviceProvider
            , ModelContext modelContext
            , ILogger<MultiModuleProfileService> logger)
        {
            _ModelContext = modelContext;
            _ServiceProvider = serviceProvider;
            _logger = logger;
        }
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {

            var providerId = context.Subject.Claims.Where(c => c.Type == IDSign.IdP.Model.Constants.IdpClaimType.ProviderId).Select(c => c.Value).SingleOrDefault();
            if (String.IsNullOrWhiteSpace(providerId))
                return;

            var provider = ModuleRepository.GetServicesById(providerId);
            if (provider == null)
            {
                _logger.LogError($"!!! Provider cannot be found for id {providerId}");
                return;
            }
            Dictionary<string, string> values = new Dictionary<string, string>();

            #region Remote Users

            var project = context.Subject.Claims.FirstOrDefault(c => c.Type == IDSign.IdP.Model.Constants.IdpClaimType.ProjectCode)?.Value;
            if (!string.IsNullOrWhiteSpace(project))
            {
                values.Add(IDSign.IdP.Model.Constants.IdpClaimType.ProjectCode, project);
            }
            var tenant = context.Subject.Claims.FirstOrDefault(c => c.Type == IDSign.IdP.Model.Constants.IdpClaimType.TenantCode)?.Value;
            if (!string.IsNullOrWhiteSpace(tenant))
            {
                values.Add(IDSign.IdP.Model.Constants.IdpClaimType.TenantCode, tenant);
            }

            #endregion

            var sub = context.Subject.GetSubjectId();

            var claims = await provider.Init(_ServiceProvider).GetUserClaims(sub, context.RequestedClaimTypes, context, values);

            if (claims.Count < context.RequestedClaimTypes.ToList().Count)
            {
                var notFound = context.RequestedClaimTypes.Where(claimType => !claims.Any(claim => claim.Type == claimType)).ToList();
                _logger.LogError($"The claims requested were not all found for the user '{sub}' in provider '{providerId}'. Missing : {String.Join(",", notFound)}");
            }

            context.IssuedClaims = claims.ToList();
            return;

        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.FromResult(0);
        }
    }
}
