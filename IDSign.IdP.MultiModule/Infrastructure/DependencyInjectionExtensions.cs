﻿using IDSign.IdP.MultiModule.Helpers.Rest.API;
using IDSign.IdP.MultiModule.Helpers.Rest.API.Implementation;
using Microsoft.Extensions.DependencyInjection;

namespace IDSign.IdP.MultiModule.Infrastructure
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddMultiModuleServices(this IServiceCollection services)
        {
            services.AddTransient<IMultiModuleResourceOwnerPasswordValidator, MultiModuleResourceOwnerPasswordValidator>();
            services.AddTransient<IRemoteRepositoryApiProvider, RemoteRepositoryApi>();
            return services;
        }
    }
}