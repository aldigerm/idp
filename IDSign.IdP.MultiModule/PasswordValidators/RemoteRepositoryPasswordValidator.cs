﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Core;
using IDSign.IdP.Core.Config;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using IDSign.IdP.Model.Constants;
using IDSign.IdP.Model.RemoteRepositoryMultiModule;
using IDSign.IdP.MultiModule.Helpers.Rest;
using IDSign.IdP.MultiModule.Helpers.Rest.API;
using IDSign.IdP.MultiModule.Helpers.Rest.API.Implementation;
using IDSign.IdP.MultiModule.RemoteRepository.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using mdl = IdentityServer4.Models;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public class RemoteRepositoryPasswordValidator : BasePasswordValidator
    {
        protected string Uri, IdPBaseUri, IdPBaseAppName, BaseAppName, TenantClaimName, ProjectClaimName, SetPasswordRedirectUri;
        protected HttpClientWrapper httpClientWrapper;
        protected string RepositoryUsername, RepositoryPassword;
        protected ILogger<RemoteRepositoryPasswordValidator> _logger;
        protected IRemoteRepositoryApiProvider RemoteRepositoryApi;


        public RemoteRepositoryPasswordValidator(string id
            , bool isFallback
            , int order
            , string endpointBaseUri
            , string idPBaseUri
            , string idPBaseAppName
            , string baseAppName
            , string tenantClaimName
            , string projectClaimName
            , string repositoryUsername
            , string repositoryPassword
            , string setPasswordRedirectUri) : base(id, isFallback, order)
        {
            Uri = endpointBaseUri;
            BaseAppName = baseAppName;
            TenantClaimName = tenantClaimName;
            ProjectClaimName = projectClaimName;
            SetPasswordRedirectUri = setPasswordRedirectUri;

            RepositoryUsername = repositoryUsername ?? new AppSettingsConfigValue("RemoteRepository.Credentials.Username", "remote-user-repository");
            RepositoryPassword = repositoryPassword ?? new AppSettingsConfigValue("RemoteRepository.Credentials.Password", "remote-user-repository-test");
            IdPBaseUri = idPBaseUri ?? new AppSettingsConfigValue("RemoteRepository.Uri.IdPBaseUri", "http://LAPTOP-CT2F8UKM");
            IdPBaseAppName = idPBaseAppName ?? new AppSettingsConfigValue("RemoteRepository.Uri.IdPBaseAppName", "/idsign.idp.2");
        }



        public override string UserServiceId
        {
            get
            {
                return string.IsNullOrEmpty(Id) ? "identity-user" : $"identity-user--{Id}";
            }
        }

        public override ProviderType ProviderType { get { return ProviderType.RemoteRepository; } }

        public override async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            throw new NotImplementedException("This still requires logic to have the tenantCode and projectCode to be sent in some way in the context and extracted.");
        }

        public override async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, mdl.Client client, Dictionary<string, string> values)
        {
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            var result = await LoginUser(username, password, client, values);

            if (result == null)
            {
                _logger.LogError("Login failed.");
                res.IsValid = false;
                return res;
            }

            // found
            res.IsValid = true;
            res.User = GetUserModel(result);
            res.IsTemporaryPassword = result.IsNewPasswordRequired;
            if (res.IsTemporaryPassword)
            {
                var session = result.Sessions.FirstOrDefault(s => s.UserTenantSessionTypeCode == UserTenantSessionTypes.ForceNewPassword);
                if (session != null)
                {
                    var url = SetPasswordRedirectUri?.Replace("{{sessionId}}", session.UserTenantSessionIdentifier);
                    res.RedirectToUri = url;
                }
            }
            res.ResetPasswordRequired = false; // implementation pending. useful for expired passwords
            return res;
        }

        private async Task<UserViewModel> LoginUser(string username, string password, mdl.Client client, Dictionary<string, string> values)
        {
            var project = GetProject(client, values);

            var tenant = values?.GetValueOrDefault(IdpClaimType.TenantCode);
            if (string.IsNullOrWhiteSpace(tenant))
            {
                tenant = GetTenant(client, values);
                _logger.LogDebug($"Tenant read from client claims: '{tenant}'");
            }
            else
            {
                _logger.LogDebug($"Tenant read from values dictionary: '{tenant}'");
            }

            if (String.IsNullOrWhiteSpace(tenant) || String.IsNullOrWhiteSpace(project))
            {
                _logger.LogError("Tenant or project claims were not found.");
                return null;
            }

            await InitHttpClient(AuthType.Login);

            var result = await RemoteRepositoryApi.LoginAsync(username, password, tenant, project);

            if (!result.Success || result.Value == null)
            {
                _logger.LogError("Login failed.");
                return null;
            }
            return result.Value;
        }

        private async Task<UserViewModel> GetUserDetails(string username, mdl.Client client, Dictionary<string, string> values)
        {
            var project = GetProject(client, values);
            var tenant = GetTenant(client, values);
            if (string.IsNullOrWhiteSpace(tenant) || string.IsNullOrWhiteSpace(project))
            {
                _logger.LogError("Tenant or project claims were not found.");
                return null;
            }

            await InitHttpClient(AuthType.Detail);

            var result = await RemoteRepositoryApi.GetDetailAsync(username, tenant, project);

            if (!result.Success || result.Value == null)
            {
                _logger.LogError("Login failed.");
                return null;
            }
            _logger.LogDebug("User returned is " + HelpersService.DescribeObject(result.Value, true));

            return result.Value;
        }

        private async Task InitHttpClient(AuthType type)
        {
            // initialise api
            if (RemoteRepositoryApi == null)
            {
                RemoteRepositoryApi = _ServiceProvider.GetRequiredService<IRemoteRepositoryApiProvider>();
                RemoteRepositoryApi.Client = httpClientWrapper;
                RemoteRepositoryApi.BaseAppName = BaseAppName;
            }

            // init and authenticate 
            await RemoteRepositoryApi.InitHttpClient(Uri, IdPBaseUri, IdPBaseAppName, RepositoryUsername, RepositoryPassword, type);
        }

        private string GetProject(mdl.Client client, Dictionary<string, string> values)
        {
            var project = values?.GetValueOrDefault(IdpClaimType.ProjectCode);
            if (string.IsNullOrWhiteSpace(project))
            {
                project = client?.Claims?.SingleOrDefault(cc => cc.Type == ProjectClaimName)?.Value;
                _logger.LogDebug($"Project read from client claims: '{project}'");
                if (string.IsNullOrWhiteSpace(project))
                {
                    _logger.LogError("Client has no project claim");
                    return null;
                }
            }
            else
            {
                _logger.LogDebug($"Project read from values dictionary: '{project}'");
            }
            return project;
        }

        private string GetTenant(mdl.Client client, Dictionary<string, string> values)
        {
            var tenant = values?.GetValueOrDefault(IdpClaimType.TenantCode);
            if (string.IsNullOrWhiteSpace(tenant))
            {
                tenant = tenant = client?.Claims?.SingleOrDefault(cc => cc.Type == TenantClaimName)?.Value;
                _logger.LogDebug($"Tenant read from client claims: '{tenant}'");
                if (string.IsNullOrWhiteSpace(tenant))
                {
                    _logger.LogError("Client has no tenant claim.");
                    return null;
                }
            }
            else
            {
                _logger.LogDebug($"Tenant read from values dictionary: '{tenant}'");
            }
            return tenant;
        }

        private User GetUserModel(UserViewModel model)
        {
            if (model == null)
                return null;

            User user = new User()
            {
                IsActive = model.Enabled.GetValueOrDefault(true),
                Password = "",
                ProviderId = this.Id,
                ProviderSubjectId = "",
                SubjectId = model.Identifier.Username,
                Username = model.Identifier.Username
            };
            user.Claims = new List<Claim>();

            // adding openid name claim
            user.Claims.Add(new Claim(IdpClaimType.ProjectCode, model.Identifier.ProjectCode));
            user.Claims.Add(new Claim(IdpClaimType.TenantCode, model.Identifier.TenantCode));
            user.Claims.Add(new Claim(JwtClaimTypes.Name, model.Identifier.Username));
            user.Claims.Add(new Claim(JwtClaimTypes.GivenName, model.FirstName ?? ""));
            user.Claims.Add(new Claim(JwtClaimTypes.FamilyName, model.LastName ?? ""));

            if (model.AllClaims != null)
            {
                foreach (var claim in model.AllClaims)
                {
                    user.Claims.Add(new Claim(claim.Type, claim.Value));
                }
            }
            return user;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, mdl.ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = new List<Claim>();
            var identityuser = await GetUserDetails(username, context.ValidatedRequest.Client, values);
            var user = GetUserModel(identityuser);
            if (user == null)
                return claims;

            claims = user.Claims.ToList();

            var tenantCode = GetTenant(context?.Client, values);
            if (!string.IsNullOrWhiteSpace(tenantCode))
            {
                claims.Add(new Claim(ClaimName.Tenant, tenantCode));
            }

            var projectCode = GetProject(context?.Client, values);
            if (!string.IsNullOrWhiteSpace(projectCode))
            {
                claims.Add(new Claim(ClaimName.Project, projectCode));
            }

            return claims;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, IEnumerable<string> requestedClaimTypes, mdl.ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = await GetUserClaims(username, context, values);
            var filtered = claims.Where(claim => requestedClaimTypes.Contains(claim.Type)).ToList();
            return filtered;
        }

        public override void Initialized()
        {
            _logger = _logger ?? _ServiceProvider.GetRequiredService<ILogger<RemoteRepositoryPasswordValidator>>();
        }

        public override async Task<SetPasswordReturn> SetPassword(string username, string password, mdl.Client client, Dictionary<string, string> values)
        {
            var project = GetProject(client, values);
            var tenant = GetTenant(client, values);
            if (String.IsNullOrWhiteSpace(tenant) || String.IsNullOrWhiteSpace(project))
            {
                _logger.LogError("Tenant or project claims were not found.");
                return null;
            }

            await InitHttpClient(AuthType.Detail);

            var result = await RemoteRepositoryApi.SetPasswordAsync(username, tenant, project, password);

            if (!result.Success)
            {
                _logger.LogError("Password setting failed:" + result.StatusCode);
                return new SetPasswordReturn()
                {
                    IsValid = false
                };
            }
            _logger.LogDebug("User returned is " + HelpersService.DescribeObject(result.Value, true));
            return new SetPasswordReturn()
            {
                IsValid = true
            };
        }


        public async override Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            var res = await GetUserForExternalAccessSessionIdAsync(easidRequest);
            return ProcessResult(res);
        }

        private ValidateExternalSessionIdReturn ProcessResult(TemporaryUserViewModel result)
        {

            ValidateExternalSessionIdReturn res = new ValidateExternalSessionIdReturn();
            if (result == null || (result?.User == null && !result.RequireOTP.HasValue))
            {
                _logger.LogError("ExternalAccessSessionId Login failed.");
                res.IsValid = false;
                return res;
            }

            // found
            res.IsValid = true;
            res.User = GetUserModel(result.User);
            res.ExpiryDateTime = result.ExpiryDateTime;
            res.RequireOTP = result.RequireOTP.Value;
            res.NextOTPRequestAllowedAt = result.NextOTPRequestAllowedAt;
            res.TenantCode = result.TenantCode;
            res.ProjectCode = result.ProjectCode;
            res.Error = result.Error;
            res.ExpectedOTP = result.ExpectedOTP;

            string phoneNumber = result.PhoneNumber;

            if (!string.IsNullOrWhiteSpace(phoneNumber))
            {
                int n = 2;

                // 1. Start with a blank string
                var censoredNumber = "";

                // 2. Replace first Length - n characters with X
                for (var i = 0; i < phoneNumber.Length - n; i++)
                    censoredNumber += "*";

                // 3. Add in the last n characters from original string.
                censoredNumber += phoneNumber.Substring(phoneNumber.Length - n);

                res.PhoneNumber = phoneNumber;
                res.CensoredPhoneNumber = censoredNumber;
            }

            return res;
        } 

        private async Task<TemporaryUserViewModel> GetUserForExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest)
        {
            if (String.IsNullOrWhiteSpace(easidRequest.Easid) )
            {
                _logger.LogError("ExternalAccessSessionId was not provided.");
                return null;
            }

            await InitHttpClient(AuthType.Login);

            var result = await RemoteRepositoryApi.LoginUsingExternalAccessSessionIdAsync(easidRequest.Easid, easidRequest.OTPValue);

            if (!result.Success || result.Value == null)
            {
                _logger.LogError("Login failed.");
                return null;
            }
            return result.Value;
        }

        public override async Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            if (string.IsNullOrWhiteSpace(easidRequest.Easid))
            {
                _logger.LogError("ExternalAccessSessionId was not provided.");
                return null;
            }

            await InitHttpClient(AuthType.Login);

            var result = await RemoteRepositoryApi.SubmitOTPAsync(easidRequest.Easid, easidRequest.OTPValue);

            if (!result.Success || result.Value == null)
            {
                _logger.LogError("Login failed.");
                return BuildInvalidResult();
            } 
            return ProcessResult(result.Value);
        }

        public override async Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            if (String.IsNullOrWhiteSpace(easidRequest.Easid))
            {
                _logger.LogError("ExternalAccessSessionId was not provided.");
                return null;
            }

            await InitHttpClient(AuthType.Login);

            var result = await RemoteRepositoryApi.RequestOTPDeliveryAsync(easidRequest.Easid);

            if (!result.Success || result.Value == null)
            {
                _logger.LogError("Login failed.");
                return null;
            }
            return ProcessResult(result.Value);
        }
    }
}
