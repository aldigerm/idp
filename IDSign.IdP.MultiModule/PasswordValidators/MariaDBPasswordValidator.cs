﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public class MariaDBPasswordValidator : BasePasswordValidator
    {
        protected string ConnectionString;
        protected ILogger<MSSQLPasswordValidator> _logger;

        public MariaDBPasswordValidator(string id, bool isFallback, int order, string connectionString) : base(id, isFallback, order)
        {
            ConnectionString = connectionString;
        }

        public override string UserServiceId
        {
            get
            {
                return string.IsNullOrEmpty(Id) ? "mariadb" : $"mariadb-{Id}";
            }
        }

        public override ProviderType ProviderType { get { return ProviderType.MariaDB; } }

        public override async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var res = await ValidateCredentials(context.UserName, context.Password, context.Request.Client, null);
            if (res.IsValid)
            {
                context.Result = new GrantValidationResult(res.User.Username, OidcConstants.AuthenticationMethods.Password);
            }
        }

        public override async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values)
        {
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            MariaDBUser mariadbUser = GetMariaDBUser(username);
            if (mariadbUser == null || mariadbUser.Password != password)
            {
                res.IsValid = false;
                return res;
            }

            // found
            res.IsValid = mariadbUser.IsActive;
            res.User = GetUserModel(mariadbUser);
            return res;
        }

        private MariaDBUser GetMariaDBUser(string username)
        {
            DataTable userDataTable = new DataTable();
            DataTable claimsDataTable = new DataTable();
            string userSqlQuery = @"
                  SELECT Username,
                         Password,
                         IsActive
                  FROM dbo.vw_Users
                  WHERE Username = '" + username + "'";

            string claimsSqlQuery = @"
                SELECT Type
                        ,Value
                        ,Username
                    FROM dbo.vw_UserClaims
                  WHERE Username = '" + username + "'";

            using (MySqlConnection connection = new MySqlConnection(ConnectionString))
            using (MySqlCommand cmd = connection.CreateCommand())
            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = userSqlQuery;
                connection.Open();

                // get users
                sda.Fill(userDataTable);

                // get claims
                cmd.CommandText = claimsSqlQuery;
                sda.Fill(claimsDataTable);

                connection.Close();
            }

            if (userDataTable.Rows.Count == 0)
            {
                // query returned no rows
                // so user wasn't found
                _logger.LogDebug($"Couldn't find username : {username}");
                return null;
            }
            else if (userDataTable.Rows.Count == 1)
            {
                // one user found as expected
                _logger.LogDebug($"User found by username : {username}");
                string password = userDataTable.Rows[0]["Password"].ToString();
                bool isActive = Convert.ToBoolean(userDataTable.Rows[0]["IsActive"]);

                List<MariaDBClaim> claims = new List<MariaDBClaim>();
                foreach (DataRow row in claimsDataTable.Rows)
                {
                    MariaDBClaim claim = new MariaDBClaim();
                    claim.Type = row["Type"].ToString();
                    claim.Value = row["Value"].ToString();
                    claims.Add(claim);
                }

                return new MariaDBUser() { Username = username, Password = password, IsActive = isActive, Claims = claims };
            }
            else
            {
                _logger.LogError($"The username appeared more than once in the returned data : {username}");
                throw new Exception("The username and password returned more than 1 entry from the MariaDB database. This shouldn't happen.");
            }
        }

        private User GetUserModel(MariaDBUser mariadbUser)
        {
            if (mariadbUser == null)
                return null;

            User user = new User()
            {
                IsActive = mariadbUser.IsActive,
                Password = mariadbUser.Password,
                ProviderId = this.Id,
                ProviderSubjectId = "",
                SubjectId = mariadbUser.Username,
                Username = mariadbUser.Username
            };
            user.Claims = new List<Claim>();
            foreach (var mariadbClaim in mariadbUser.Claims)
            {
                user.Claims.Add(new Claim(mariadbClaim.Type, mariadbClaim.Value));
            }
            return user;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = new List<Claim>();
            var mariadbUser = GetMariaDBUser(username);
            var user = GetUserModel(mariadbUser);
            if (user == null)
                return claims;

            return user.Claims;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, IEnumerable<string> requestedClaimTypes, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            return (await GetUserClaims(username, context, values)).Where(claim => requestedClaimTypes.Contains(claim.Type)).ToList();
        }

        public override void Initialized()
        {
            _logger = _logger ?? _ServiceProvider.GetRequiredService<ILogger<MSSQLPasswordValidator>>();
        }

        public override Task<SetPasswordReturn> SetPassword(string username, string password, Client client, Dictionary<string, string> values)
        {
            throw new NotImplementedException("This multi module does not support setting of passwords");
        }

        public async override Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }
    }
}
