﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static IDSign.IdP.Model.Configuration.InMemoryUserConfig;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public class InMemoryPasswordValidator : BasePasswordValidator
    {
        protected List<InMemoryUser> Users;
        private InMemoryProviderElement item;

        public InMemoryPasswordValidator Load(InMemoryProviderElement item)
        {
            this.item = item;
            this.Users = new List<InMemoryUser>();
            foreach (var user in item.Users)
            {
                InMemoryUser inMemoryUser = new InMemoryUser();
                inMemoryUser.IsActive = user.Enabled;
                inMemoryUser.Password = user.Password;
                inMemoryUser.Subject = user.Subject;
                inMemoryUser.Username = user.Username;
                inMemoryUser.Claims = new List<InMemoryClaim>();
                foreach (var claim in user.Claims)
                {
                    InMemoryClaim inMemoryClaim = new InMemoryClaim();
                    inMemoryClaim.Id = claim.Id;
                    inMemoryClaim.Type = claim.Type;
                    inMemoryClaim.Value = claim.Value;
                    inMemoryUser.Claims.Add(inMemoryClaim);
                }
                if (!inMemoryUser.Claims.Any(c => c.Type == ClaimTypes.Name))
                {
                    inMemoryUser.Claims.Add(new InMemoryClaim() { Id = "99", Type = ClaimTypes.Name, Value = user.Username });
                }
                this.Users.Add(inMemoryUser);
            }
            return this;
        }

        public InMemoryPasswordValidator(string id, bool isFallback, int order) : base(id, isFallback, order)
        {
            Users = new List<InMemoryUser>();
        }

        public override string UserServiceId
        {
            get
            {
                return string.IsNullOrEmpty(Id) ? "in_memory" : $"in_memory-{Id}";
            }
        }

        public override ProviderType ProviderType { get { return ProviderType.InMemory; } }

        public override Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            InMemoryUser user = Users.Where(u => u.Username == context.UserName && u.Password == context.Password).SingleOrDefault();
            if (user != null)
            {
                context.Result = new GrantValidationResult(user.Subject, OidcConstants.AuthenticationMethods.Password);
            }

            return Task.FromResult(0);
        }

        public override async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values)
        {
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            InMemoryUser inMemoryUser = GetInMemoryUser(username);
            if (inMemoryUser == null || inMemoryUser.Password != password)
            {
                res.IsValid = false;
                return res;
            }

            // found
            res.IsValid = inMemoryUser.IsActive;
            res.User = GetUserModel(inMemoryUser);
            return res;
        }

        private InMemoryUser GetInMemoryUser(string username)
        {
            return Users.Where(u => u.Username == username).SingleOrDefault();
        }

        private User GetUserModel(InMemoryUser inMemoryUser)
        {
            if (inMemoryUser == null)
                return null;

            User user = new User()
            {
                IsActive = inMemoryUser.IsActive,
                Password = inMemoryUser.Password,
                ProviderId = this.Id,
                ProviderSubjectId = "",
                SubjectId = inMemoryUser.Username,
                Username = inMemoryUser.Username
            };
            user.Claims = new List<Claim>();
            foreach (var inMemoryClaim in inMemoryUser.Claims)
            {
                user.Claims.Add(new Claim(inMemoryClaim.Type, inMemoryClaim.Value));
            }
            return user;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = new List<Claim>();
            var inMemoryUser = GetInMemoryUser(username);
            var user = GetUserModel(inMemoryUser);
            if (user == null)
                return claims;

            return user.Claims;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, IEnumerable<string> requestedClaimTypes, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            return (await GetUserClaims(username, context, values)).Where(claim => requestedClaimTypes.Contains(claim.Type)).ToList();
        }

        public override void Initialized()
        {
        }

        public override Task<SetPasswordReturn> SetPassword(string username, string password, Client client, Dictionary<string, string> values)
        {
            throw new NotImplementedException("This multi module does not support setting of passwords");
        }

        public async override Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }
        public async override Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }
    }
}
