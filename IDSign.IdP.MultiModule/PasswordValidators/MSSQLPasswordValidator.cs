﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public class MSSQLPasswordValidator : BasePasswordValidator
    {
        protected string ConnectionString;
        protected ILogger<MSSQLPasswordValidator> _logger;
        public MSSQLPasswordValidator(string id, bool isFallback, int order, string connectionString) : base(id, isFallback, order)
        {
            ConnectionString = connectionString;
        }

        public override string UserServiceId
        {
            get
            {
                return string.IsNullOrEmpty(Id) ? "mssql" : $"mssql-{Id}";
            }
        }

        public override ProviderType ProviderType { get { return ProviderType.MSSQL; } }

        public override async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var res = await ValidateCredentials(context.UserName, context.Password, context.Request.Client, null);
            if (res.IsValid)
            {
                context.Result = new GrantValidationResult(res.User.Username, OidcConstants.AuthenticationMethods.Password);
            }
        }

        public override async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values)
        {
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            MSSQLUser mssqlUser = GetMSSQLUser(username);
            if (mssqlUser == null || mssqlUser.Password != password)
            {
                res.IsValid = false;
                return res;
            }

            // found
            res.IsValid = mssqlUser.IsActive;
            res.User = GetUserModel(mssqlUser);
            return res;
        }

        private MSSQLUser GetMSSQLUser(string username)
        {
            DataTable userDataTable = new DataTable();
            DataTable claimsDataTable = new DataTable();
            string userSqlQuery = @"
                  SELECT Username,
                         Password,
                         IsActive
                  FROM [dbo].[vw_Users]
                  WHERE Username = '" + username + "'";

            string claimsSqlQuery = @"
                SELECT [Type]
                        ,[Value]
                        ,[Username]
                    FROM [dbo].[vw_UserClaims]
                  WHERE Username = '" + username + "'";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            using (SqlCommand cmd = connection.CreateCommand())
            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = userSqlQuery;
                connection.Open();

                // get users
                sda.Fill(userDataTable);

                // get claims
                cmd.CommandText = claimsSqlQuery;
                sda.Fill(claimsDataTable);

                connection.Close();
            }

            if (userDataTable.Rows.Count == 0)
            {
                // query returned no rows
                // so user wasn't found
                _logger.LogDebug($"Couldn't find username : {username}");
                return null;
            }
            else if (userDataTable.Rows.Count == 1)
            {
                // one user found as expected
                _logger.LogDebug($"User found by username : {username}");
                string password = userDataTable.Rows[0]["Password"].ToString();
                bool isActive = (bool)userDataTable.Rows[0]["IsActive"];

                List<MSSQLClaim> claims = new List<MSSQLClaim>();
                foreach (DataRow row in claimsDataTable.Rows)
                {
                    MSSQLClaim claim = new MSSQLClaim();
                    claim.Type = row["Type"].ToString();
                    claim.Value = row["Value"].ToString();
                    claims.Add(claim);
                }

                return new MSSQLUser() { Username = username, Password = password, IsActive = isActive, Claims = claims };
            }
            else
            {
                _logger.LogError($"The username appeared more than once in the returned data : {username}");
                throw new Exception("The username and password returned more than 1 entry from the MSSQL database. This shouldn't happen.");
            }
        }

        private User GetUserModel(MSSQLUser mssqlUser)
        {
            if (mssqlUser == null)
                return null;

            User user = new User()
            {
                IsActive = mssqlUser.IsActive,
                Password = mssqlUser.Password,
                ProviderId = this.Id,
                ProviderSubjectId = "",
                SubjectId = mssqlUser.Username,
                Username = mssqlUser.Username
            };
            user.Claims = new List<Claim>();
            foreach (var mssqlClaim in mssqlUser.Claims)
            {
                user.Claims.Add(new Claim(mssqlClaim.Type, mssqlClaim.Value));
            }
            return user;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = new List<Claim>();
            var mssqlUser = GetMSSQLUser(username);
            var user = GetUserModel(mssqlUser);
            if (user == null)
                return claims;

            return user.Claims;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, IEnumerable<string> requestedClaimTypes, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            return (await GetUserClaims(username, context, values)).Where(claim => requestedClaimTypes.Contains(claim.Type)).ToList();
        }

        public override void Initialized()
        {
            _logger = _logger ?? _ServiceProvider.GetRequiredService<ILogger<MSSQLPasswordValidator>>();
        }

        public override Task<SetPasswordReturn> SetPassword(string username, string password, Client client, Dictionary<string, string> values)
        {
            throw new NotImplementedException("This multi module does not support setting of passwords");
        }

        public async override Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }
    }
}
