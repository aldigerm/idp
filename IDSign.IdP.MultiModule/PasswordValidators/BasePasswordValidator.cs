﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using IDSign.IdP.Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public abstract class BasePasswordValidator : IResourceOwnerPasswordValidator
    {
        protected IServiceProvider _ServiceProvider;

        public BasePasswordValidator(string id, bool isFallback, int order)
        {
            Id = id;
            IsFallback = isFallback;
            Order = order;
        }

        public string Id { get; private set; }
        public int Order { get; private set; }

        public abstract string UserServiceId { get; }

        public abstract ProviderType ProviderType { get; }

        public bool IsFallback { get; private set; }

        public BasePasswordValidator Init(IServiceProvider serviceProvider)
        {
            _ServiceProvider = serviceProvider;
            Initialized();
            return this;
        }

        protected ValidateExternalSessionIdReturn BuildInvalidResult()
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public abstract void Initialized();

        public abstract Task ValidateAsync(ResourceOwnerPasswordValidationContext context);

        public abstract Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values);

        public abstract Task<SetPasswordReturn> SetPassword(string username, string password, Client client, Dictionary<string, string> values);

        public abstract Task<ICollection<Claim>> GetUserClaims(string username, ProfileDataRequestContext context, Dictionary<string, string> values);

        public abstract Task<ICollection<Claim>> GetUserClaims(string sub, IEnumerable<string> requestedClaimTypes, ProfileDataRequestContext context, Dictionary<string, string> values);

        public abstract Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values);

        public abstract Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values);

        public abstract Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values);

    }
}
