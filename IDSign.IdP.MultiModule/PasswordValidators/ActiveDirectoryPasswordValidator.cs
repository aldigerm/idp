﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using IDSign.IdP.Model;
using IDSign.IdP.Model.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.PasswordValidators
{
    public class ActiveDirectoryPasswordValidator : BasePasswordValidator
    {
        #region nested types
        [Flags]
        protected enum UserFlags
        {
            // Reference - Chapter 10 (from The .NET Developer's Guide to Directory Services Programming)

            Script = 1,                                     // 0x1
            AccountDisabled = 2,                            // 0x2
            HomeDirectoryRequired = 8,                      // 0x8
            AccountLockedOut = 16,                          // 0x10
            PasswordNotRequired = 32,                       // 0x20
            PasswordCannotChange = 64,                      // 0x40
            EncryptedTextPasswordAllowed = 128,             // 0x80
            TempDuplicateAccount = 256,                     // 0x100
            NormalAccount = 512,                            // 0x200
            InterDomainTrustAccount = 2048,                 // 0x800
            WorkstationTrustAccount = 4096,                 // 0x1000
            ServerTrustAccount = 8192,                      // 0x2000
            PasswordDoesNotExpire = 65536,                  // 0x10000 (Also 66048 )
            MnsLogonAccount = 131072,                       // 0x20000
            SmartCardRequired = 262144,                     // 0x40000
            TrustedForDelegation = 524288,                  // 0x80000
            AccountNotDelegated = 1048576,                  // 0x100000
            UseDesKeyOnly = 2097152,                        // 0x200000
            DontRequirePreauth = 4194304,                   // 0x400000
            PasswordExpired = 8388608,                      // 0x800000 (Applicable only in Window 2000 and Window Server 2003)
            TrustedToAuthenticateForDelegation = 16777216,  // 0x1000000
            NoAuthDataRequired = 33554432                   // 0x2000000
        }
        #endregion

        protected ILogger<ActiveDirectoryPasswordValidator> _logger;
        protected Uri LdapEndpoint;
        private string Username { get; set; }
        private string Password { get; set; }
        public ActiveDirectoryPasswordValidator(string id, bool isFallback, int order, string ldapEnpoint, string username, string password) : base(id, isFallback, order)
        {
            LdapEndpoint = new Uri(ldapEnpoint);
            Username = username;
            Password = password;
        }

        public ActiveDirectoryPasswordValidator(string id, bool isFallback, int order) : base(id, isFallback, order)
        {
        }

        public override string UserServiceId
        {
            get
            {
                return string.IsNullOrEmpty(Id) ? "active-directory" : $"active-directory-{Id}";
            }
        }

        public override ProviderType ProviderType { get { return ProviderType.ActiveDirectory; } }

        public override async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var res = await ValidateCredentials(context.UserName, context.Password, context.Request.Client, null);
            if (res != null && res.IsValid)
            {
                context.Result = new GrantValidationResult(res.User.SubjectId, OidcConstants.AuthenticationMethods.Password);
            }
        }

        public override async Task<ValidateCredentialsReturn> ValidateCredentials(string username, string password, Client client, Dictionary<string, string> values)
        {
            ValidateCredentialsReturn res = new ValidateCredentialsReturn();
            PrincipalContext ctx = null;
            if (string.IsNullOrEmpty(Username))
                ctx = new PrincipalContext(ContextType.Domain, $"{LdapEndpoint.Host}:{LdapEndpoint.Port}", LdapEndpoint.Segments[1], LdapEndpoint.Scheme.EndsWith("s") ? ContextOptions.Negotiate | ContextOptions.SecureSocketLayer : ContextOptions.Negotiate);
            else
                ctx = new PrincipalContext(ContextType.Domain, $"{LdapEndpoint.Host}:{LdapEndpoint.Port}", LdapEndpoint.Segments[1], LdapEndpoint.Scheme.EndsWith("s") ? ContextOptions.Negotiate | ContextOptions.SecureSocketLayer : ContextOptions.Negotiate, Username, Password);

            if (string.IsNullOrEmpty(username))
                throw new ArgumentException("Parameter not set or empty", "username");

            if (ctx.ValidateCredentials(username, password))
            {
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);

                if (userPrincipal == null)
                {
                    _logger.LogInformation("User not found, even though credenitals are valid.");
                }
                else
                {
                    _logger.LogInformation("User found and authenticated");
                    res.User = GetUserModel(userPrincipal);
                    res.IsValid = res.User.IsActive;
                    return res;
                }
            }

            // not found or not authenticated
            res.IsValid = false;
            return res;
        }

        private UserPrincipal GetActiveDirectoryUser(string username)
        {
            PrincipalContext ctx = null;
            if (string.IsNullOrEmpty(Username))
                ctx = new PrincipalContext(ContextType.Domain, $"{LdapEndpoint.Host}:{LdapEndpoint.Port}", LdapEndpoint.Segments[1], LdapEndpoint.Scheme.EndsWith("s") ? ContextOptions.Negotiate | ContextOptions.SecureSocketLayer : ContextOptions.Negotiate);
            else
                ctx = new PrincipalContext(ContextType.Domain, $"{LdapEndpoint.Host}:{LdapEndpoint.Port}", LdapEndpoint.Segments[1], LdapEndpoint.Scheme.EndsWith("s") ? ContextOptions.Negotiate | ContextOptions.SecureSocketLayer : ContextOptions.Negotiate, Username, Password);

            if (string.IsNullOrEmpty(username))
                throw new ArgumentException("Parameter not set or empty", "username");

            UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, username);

            if (userPrincipal == null)
                _logger.LogInformation($"User not found: '{username}'");
            else
                _logger.LogDebug($"User not found : '{username}'");

            return userPrincipal;
        }

        private User GetUserModel(UserPrincipal userPrincipal)
        {
            if (userPrincipal == null)
                return null;

            User user = new User()
            {
                IsActive = userPrincipal.Enabled.GetValueOrDefault(false),
                Password = "",
                ProviderId = this.Id,
                ProviderSubjectId = "",
                SubjectId = userPrincipal.SamAccountName,
                Username = userPrincipal.SamAccountName
            };
            user.Claims = new List<Claim>();
            return user;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            var claims = new List<Claim>();
            var mssqlUser = GetActiveDirectoryUser(username);
            var user = GetUserModel(mssqlUser);
            if (user == null)
                return claims;

            return user.Claims;
        }

        public override async Task<ICollection<Claim>> GetUserClaims(string username, IEnumerable<string> requestedClaimTypes, ProfileDataRequestContext context, Dictionary<string, string> values)
        {
            return (await GetUserClaims(username, context, values)).Where(claim => requestedClaimTypes.Contains(claim.Type)).ToList();
        }


        public override void Initialized()
        {
            _logger = _logger ?? _ServiceProvider.GetRequiredService<ILogger<ActiveDirectoryPasswordValidator>>();
        }

        public override Task<SetPasswordReturn> SetPassword(string username, string password, Client client, Dictionary<string, string> values)
        {
            throw new NotImplementedException("This multi module does not support setting of passwords");
        }

        public async override Task<ValidateExternalSessionIdReturn> ValidateExternalAccessSessionIdAsync(ValidateExternalSessionIdRequest easidRequest, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> SubmitOTPAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }

        public async override Task<ValidateExternalSessionIdReturn> RequestNewOTPIdAsync(ValidateExternalSessionIdRequest easidValidateModel, Client client, Dictionary<string, string> values)
        {
            return new ValidateExternalSessionIdReturn()
            {
                IsValid = false
            };
        }
    }
}
