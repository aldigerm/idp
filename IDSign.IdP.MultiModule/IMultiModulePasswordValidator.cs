﻿using IdentityServer4.Validation;
using IDSign.IdP.MultiModule.PasswordValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule
{
    public interface IMultiModulePasswordValidator : IResourceOwnerPasswordValidator
    {
        int Order { get; }
        string ProviderId { get; }
        string Name { get; }
        string UserServiceId { get; }
        BasePasswordValidator SelectablePasswordValidator { get; }
    }
}
