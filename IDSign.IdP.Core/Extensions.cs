﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.Text;

namespace IDSign.IdP.Core.Extensions
{
    public static class NameValueCollectionExtensions
    {
        public static dynamic ToExpando(this NameValueCollection valueCollection)
        {
            var result = new ExpandoObject() as IDictionary<string, object>;
            foreach (var key in valueCollection.AllKeys)
            {
                result.Add(key, valueCollection[key]);
            }
            return result;
        }
    }
    public static class StringExtensions
    {
        public static string ToTitleCase(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;

            var tokens = str.Split(new[] { " ", "-" }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < tokens.Length; i++)
            {
                var token = tokens[i];
                tokens[i] = token == token.ToUpper()
                    ? token
                    : token.Substring(0, 1).ToUpper() + token.Substring(1).ToLower();
            }

            return string.Join(" ", tokens);
        }
    }
}
