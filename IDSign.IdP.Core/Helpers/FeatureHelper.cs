﻿using IDSign.IdP.Core.Config;
using Microsoft.Extensions.Logging;
using System;

namespace IDSign.IdP.Core
{
    public class FeatureHelper
    {
        public static ILogger<FeatureHelper> logger;

        #region Members

        private static readonly string FeatureSuffix = "Feature.";
        private static bool enableLogging = new AppSettingsConfigValue("Feature.Logging", "false");

        #endregion


        public static bool SeedDatabaseOnStartup = isFeatureEnabled("SeedDatabaseOnStartup");
        public static bool LoadAccessModelsEnabled = isFeatureEnabled("LoadAccessModels");
        public static bool SeedMultiTenantsEnabled = isFeatureEnabled("SeedMultiTenants");
        public static bool SeedUsersEnabled = isFeatureEnabled("SeedUsers");
        public static bool LoadSupportEnabled = isFeatureEnabled("LoadSupport");
        public static bool EnableOTPSms = isFeatureEnabled("EnableOTPSms");

        public static bool isFeatureEnabled(string name)
        {
            string appSettingsName = FeatureSuffix + name;
            if (enableLogging && logger != null)
            {
                logger.LogDebug($"The appsettings.config will be checked for {appSettingsName}");
            }
            string feautureValue = new AppSettingsConfigValue(appSettingsName, "false");
            bool featureEnabled = false;
            if (Boolean.TryParse(feautureValue, out featureEnabled))
            {
                if (enableLogging && logger != null)
                {
                    logger.LogDebug($"{name} is " + (featureEnabled ? "enabled" : "disabled"));
                }
                return featureEnabled;
            }
            else
            {
                if (logger != null)
                {
                    logger.LogError($"The value \"{feautureValue}\" for {appSettingsName} is not a valid boolean.");
                }
                return false;
            }
        }
    }
}
