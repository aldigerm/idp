﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.Core.Helpers
{
    public static class UriHelper
    {
        public static string CombineUrls(params string[] uris)
        {
            if(uris == null || uris.Length == 0)
            {
                return "";
            }
            string result = uris[0];
            for(int i = 1; i < uris.Length; i++)
            {
                string url = uris[i].TrimUrl();
                result = result.TrimUrl();
                result = string.Format("{0}/{1}", result, url);
            }
            return result;
        }

        public static string TrimUrl(this string uri)
        {
            return uri?.TrimEnd('/').TrimStart('/');
        }
    }
}
