﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace IDSign.IdP.Core
{
    public class XmlHelper
    {
        public static char[] IllegalXmlCharacters = new char[] {
        '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006', '\u0007',
        '\u0008', '\u000b', '\u000c', '\u000e', '\u000f', '\u0010', '\u0011',
        '\u0012', '\u0013', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018',
        '\u0019', '\u001a', '\u001b', '\u001c', '\u001d', '\u001e', '\u001f'
        };

        public static string RemoveIllegalSqlXmlCharacters(string value)
        {
            string[] validParts = value.Split(IllegalXmlCharacters, StringSplitOptions.RemoveEmptyEntries);
            return String.Join("", validParts);
        }


        public static String ObjectToXMLGeneric<T>(T filter)
        {
            string xml = null;
            using (StringWriter sw = new StringWriter())
            {

                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, filter);
                try
                {
                    xml = sw.ToString();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
            return xml;
        }

        private static string RemoveInvalidSystemXmlCharacters(string text)
        {
            var validXmlChars = text.Where(ch => System.Xml.XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }
    }
}
