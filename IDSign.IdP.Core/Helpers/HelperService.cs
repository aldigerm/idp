﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace IDSign.IdP.Core
{
    public static class HelpersService
    {
        public const string EmptyJsonString = "{}";

        #region Copy Properties
        public static T CopyProperties<T>(object source) where T : new()
        {
            Type type = source.GetType();
            T destination = new T();
            while (type != null)
            {
                CopyPropertiesForType(type, source, destination);
                type = type.BaseType;
            }
            return destination;
        }

        private static void CopyPropertiesForType(Type type, object source, object destination)
        {
            PropertyInfo[] myObjectFields = type.GetProperties(
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo property = null;
            foreach (var fi in myObjectFields)
            {
                property = destination.GetType().GetProperty(fi.Name);

                // check if the destination has that property/field and of the same type
                if (property != null && property.PropertyType == fi.PropertyType && property.CanWrite)
                {
                    //if (property.PropertyType == typeof(Array))
                    //{
                    //    Array a = (Array)fi.GetValue(source);
                    //    property.SetValue(destination, a.Clone(), null);
                    //}
                    //else
                    {
                        property.SetValue(destination, fi.GetValue(source));
                    }
                }
            }
        }
        #endregion

        #region DescribeObject
        public static string DescribeObject(Object obj, bool includeNulls = true, bool ignoreCyclic = true)
        {
            if (obj == null)
            {
                return null;
            }
            try
            {
                var json = JsonConvert.SerializeObject(obj,
                    Newtonsoft.Json.Formatting.None,
                    new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter> { new DateTimeOffsetFixingConverter() },
                        NullValueHandling = includeNulls ? NullValueHandling.Include : NullValueHandling.Ignore,
                        Formatting = Formatting.None,
                        ReferenceLoopHandling = ignoreCyclic ? ReferenceLoopHandling.Ignore : ReferenceLoopHandling.Error
                    });
                return json;
            }
            catch (Exception e)
            {
                throw new Exception("HelperService cannot serialize: " + e.Message, e);
            }
        }

        public static T DeserializeObject<T>(string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                return default(T);
            }
            T obj = JsonConvert.DeserializeObject<T>(json,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Include,
                    Formatting = Formatting.None
                });
            return obj;
        }

        public static string PretifyJson(string json)
        {
            return JToken.Parse(json).ToString(Formatting.Indented);
        }

        public static string SetProperty(string json, string name, int value)
        {
            var parsed = JObject.Parse(json);
            var property = parsed.Property(name);
            if (property == null)
            {
                parsed.Add(name, value);
            }
            else
            {
                parsed.Property(name).Value = value;
            }
            return parsed.ToString(Formatting.None);
        }

        public static string SetProperty(string json, string path, string value, bool setValueAsObjectIfValidJson = true)
        {
            JToken jValue = null;
            bool setAsValue = false;
            if (setValueAsObjectIfValidJson)
            {
                try
                {
                    // we check if the given value
                    // is really a json object
                    // if its not then it throws an
                    // error and we assume it is 
                    // a normal value
                    jValue = JObject.Parse(value);
                }
                catch
                {
                    // its not an object, therefore we threat it
                    // as a normal value
                    setAsValue = true;
                }
            }

            // if it threw an exception
            // or we specifically want this value as
            // a string even though it may be a good 
            // json object, then we store it
            if (setAsValue || !setValueAsObjectIfValidJson)
            {

                // its not an object, therefore we threat it
                // as a normal value
                jValue = value;

            }

            var parsed = JObject.Parse(json ?? @"{}");

            var token = parsed.SelectToken(path) as JValue;

            if (token == null)
            {
                dynamic jpart = parsed;

                // we start looking for the property
                foreach (var part in path.Split('.'))
                {
                    if (jpart[part] == null)
                        // the property wasn't found, so 
                        // we create it
                        jpart.Add(new JProperty(part, new JObject()));

                    jpart = jpart[part];
                }

                jpart.Replace(jValue);
            }
            else
            {
                token.Replace(jValue);
            }

            return parsed.ToString(Formatting.None);
        }

        public static T GetProperty<T>(string json, string path)
        {
            var parsed = JObject.Parse(json);
            JToken token = parsed.SelectToken(path);

            if (token == null)
            {
                return default(T);
            }

            if (token is JObject)
            {
                if (typeof(T) == typeof(String))
                {
                    return (T)(object)token.ToString(Formatting.None);
                }
                else
                {
                    return token.Value<T>();
                }
            }
            else
            {
                return token.Value<T>();
            }
        }

        public static string RemoveProperty(string json, string name)
        {
            var parsed = JObject.Parse(json);
            parsed.Property(name).Remove();
            return parsed.ToString(Formatting.None);
        }

        #region Merge Json

        /// <summary>
        /// Passes strings will be considered as json objects
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static string MergeJson<U, T>(U obj1, T obj2)
        {
            string json1, json2;
            if (obj1 == null)
            {
                json1 = EmptyJsonString;
            }
            else if (obj1 is string)
            {
                json1 = obj1 as string;
            }
            else
            {
                json1 = DescribeObject(obj1, false);
            }

            if (obj2 == null)
            {
                json2 = EmptyJsonString;
            }
            else if (obj2 is string)
            {
                json2 = obj2 as string;
            }
            else
            {
                json2 = DescribeObject(obj2, false);
            }

            return MergeJson(json1, json2);
        }

        public static string MergeJson(string json1, string json2)
        {
            if (string.IsNullOrWhiteSpace(json1))
            {
                json1 = EmptyJsonString;
            }

            if (string.IsNullOrWhiteSpace(json2))
            {
                json2 = EmptyJsonString;
            }

            JObject o1 = JObject.Parse(json1);
            JObject o2 = JObject.Parse(json2);

            o1.Merge(o2, new JsonMergeSettings
            {
                // union array values together to avoid duplicates
                MergeArrayHandling = MergeArrayHandling.Union

            });
            return o1.ToString(Formatting.None);
        }

        #endregion

        public static string DescribeObjectReflection(Object obj)
        {
            string result = "";
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(obj);
                string output = value.ToString();
                if (value is IEnumerable)
                {
                    foreach (var listitem in value as IEnumerable)
                    {
                        output += ",[" + listitem.ToString() + "]";
                    }
                }

                result += String.Format(" [{0}={1}] ", name, value);

            }
            return result;
        }

        #endregion

        #region Binary/String conversions

        public static byte[] EncodeStringToByteArray(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                return new byte[0];

            return Encoding.Unicode.GetBytes(text);
        }

        public static string DecodeByteArrayToString(byte[] byteArray)
        {
            if (byteArray == null)
                return String.Empty;

            return Encoding.Unicode.GetString(byteArray); ;
        }

        public static string GetBytesAsBase64(byte[] byteArray)
        {
            if (byteArray == null)
                return String.Empty;

            return Convert.ToBase64String(byteArray);
        }

        public static byte[] ConvertFromBase64ToBytes(string data)
        {
            if (String.IsNullOrWhiteSpace(data))
                return new byte[0];

            return Convert.FromBase64String(data);
        }

        public static byte[] GetBytes(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        #endregion


        #region Base64

        public static string ObjectToBase64String<T>(T obj) where T : class
        {
            using (MemoryStream ms = new MemoryStream())
            {
                var json = DescribeObject(obj, false);
                new BinaryFormatter().Serialize(ms, json);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static T Base64StringToObject<T>(string base64String) where T : class
        {
            byte[] bytes = Convert.FromBase64String(base64String);
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                var json = new BinaryFormatter().Deserialize(ms) as string;
                return DeserializeObject<T>(json);
            }
        }

        #endregion
    }
    public class DateTimeOffsetFixingConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTimeOffset?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string rawDate = (string)reader.Value;
            DateTimeOffset date;

            // First try to parse the date string as is (in case it is correctly formatted)
            if (DateTimeOffset.TryParse(rawDate, out date))
            {
                return date;
            }

            return null;
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
