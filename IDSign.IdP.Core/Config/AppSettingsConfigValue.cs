﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;

namespace IDSign.IdP.Core.Config
{
    public class AppSettingsConfigValue
    {
        private static string SettingsRoot;
        private static IConfiguration Configuration;

        public static void Init(string settingsRoot, IConfiguration configuration)
        {
            SettingsRoot = settingsRoot;
            Configuration = configuration;
        }

        public override string ToString()
        {
            return this.Value;
        }

        private string value = null;
        private string path = "";
        private string defaultValue = null;
        private bool attempted = false;
        public string Value
        {
            get
            {
                // if it was tried already, 
                // then return the computed value
                if (attempted)
                {
                    return value;
                }

                // we are trying
                attempted = true;

                // get the value
                value = Configuration?.GetValue<string>(SettingsRoot + ":" + path.Replace(".", ":"));

                // the value wasn't found or not configured
                if (value == null)
                {
                    // we set the default value as its value
                    value = defaultValue;
                }

                // return the result
                return value;
            }
        }

        public AppSettingsConfigValue(string path, string defaultValue)
        {
            this.path = path;
            this.defaultValue = defaultValue;
        }

        public static implicit operator bool(AppSettingsConfigValue val)
        {
            return bool.Parse(val.Value);
        }
        public static implicit operator int(AppSettingsConfigValue val)
        {
            return int.Parse(val.Value);
        }
        public static implicit operator long(AppSettingsConfigValue val)
        {
            return long.Parse(val.Value);
        }
        public static implicit operator double(AppSettingsConfigValue val)
        {
            return double.Parse(val.Value);
        }
        public static implicit operator string(AppSettingsConfigValue val)
        {
            return val.Value;
        }
    }
}
