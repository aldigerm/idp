﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/Scopes")]
    public class ScopesController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ScopesController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]ClientScope request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.scope))
                return base.StatusCode(500);

            request.scope = request.scope.Trim();

            var result = await dbContext.Clients.Include(s => s.AllowedScopes).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.AllowedScopes.Any((Func<idEnt.ClientScope, bool>)(s => s.Scope.Equals(request.scope))))
                {
                    // add the object and create entity for it
                    var model = new Client();
                    model.AllowedScopes.Add(request.scope);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.AllowedScopes.First();
                    entity.Client = result;
                    result.AllowedScopes.Add(temp.AllowedScopes.First());

                    await dbContext.SaveChangesAsync();
                    return base.Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]ClientScope request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.scope))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.AllowedScopes).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.AllowedScopes != null && result.AllowedScopes.Any())
            {
                // look for matching entity
                var entity = result.AllowedScopes.Where(s => s.Scope == request.scope).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientAllowedScopes.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.scope}' ");
        }
    }
}
