﻿using AutoMapper;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IDSign.IdP.WebHub.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;

namespace IDSign.IdP.Controllers
{
    [Route("api/ApiResource")]
    public class ApiResourceController : BaseAdminController
    {
        private readonly ModelContext dbContext;
        private readonly IMapper _mapper;

        //Inject context instance here.
        public ApiResourceController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApiResourceUpdatedViewModel, idModl.ApiResource>();
                cfg.CreateMap<idModl.ApiResource, ApiResourceUpdatedViewModel>();
            });
            _mapper = config.CreateMapper();
        }

        // GET: api/idEnt.ApiResource
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var resources = await dbContext.ApiResources
                .ToListAsync();
            var result = new ApiResourceListViewModel();
            result.ApiResourcesList = new List<ApiResourceDetailViewModel>();
            foreach (var resource in resources)
            {
                result.ApiResourcesList.Add(new ApiResourceDetailViewModel()
                {
                    EnabledStatus = resource.Enabled.ToString(),
                    Name = resource.Name,
                    DisplayName = resource.DisplayName,
                    Description = resource.Description
                });
            }
            result.ApiResourcesList = result.ApiResourcesList.OrderBy(ir => ir.Name).ToList();
            return Json(result);
        }

        // GET: api/idEnt.ApiResource/5
        [HttpGet("{apiResourceName}")]
        public async Task<IActionResult> Get(string apiResourceName)
        {
            var apiResource = await GetApiResourceAllProperties(apiResourceName);
            if (apiResource == null)
                return NotFound($"idEnt.ApiResource id doesn't exists already : '{apiResourceName}' ");

            var result = GetResourceDetailViewModel(apiResource);

            return Json(result);
        }

        // POST: api/idEnt.ApiResource
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody]ApiResourceUpdatedViewModel apiResourceViewModel)
        {
            if (apiResourceViewModel == null && String.IsNullOrWhiteSpace(apiResourceViewModel.Name))
                return base.StatusCode(500);

            apiResourceViewModel.Name = apiResourceViewModel.Name.Trim();

            // apiResourceid must not exist
            var result = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiResourceViewModel.Name);
            if (result == null)
            {
                idEnt.ApiResource apiResource = null;
                if (String.IsNullOrWhiteSpace(apiResourceViewModel.CopyFromName))
                {
                    // create a new blank apiResource
                    apiResource = GetApiResource(apiResourceViewModel);
                }
                else
                {
                    // create a copy
                    var copy = await GetApiResourceAllProperties(apiResourceViewModel.CopyFromName);

                    if (copy == null)
                        return NotFound($"ApiResource to copy not found {apiResourceViewModel.CopyFromName}");

                    var model = copy.ToModel();
                    model.Description = "Copy of " + model.Name;
                    model.Name = apiResourceViewModel.Name;
                    // scopes need to be unique
                    model.Scopes = null;
                    apiResource = model.ToEntity();
                }

                await dbContext.ApiResources.AddAsync(apiResource);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiResource id exists already : '{apiResourceViewModel.Name}'");
        }

        // PUT: api/idEnt.ApiResource/5
        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put([FromBody]ApiResourceUpdatedViewModel apiResourceViewModel)
        {
            if (apiResourceViewModel == null)
                return base.StatusCode(500);

            if (IsApiResourceReadOnly(apiResourceViewModel.Name))
                return base.StatusCode(500, "Api resource is readonly");

            var apiResource = GetApiResource(apiResourceViewModel);

            var result = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiResource.Name);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.ApiResources.AddAsync(apiResource);
                await dbContext.SaveChangesAsync();

                return Ok();
            }
            return NotFound($"idEnt.ApiResource id doesn't exists already : '{apiResource.Name}' ");
        }

        // PUT: api/idEnt.ApiResource/5
        [HttpPut]
        [Route("PutProperties")]
        public async Task<IActionResult> PutProperties([FromBody]ApiResourceUpdatedViewModel apiResourceViewModel)
        {
            if (apiResourceViewModel == null)
                return base.StatusCode(500);

            if (IsApiResourceReadOnly(apiResourceViewModel.Name))
                return base.StatusCode(500, "Api resource is readonly");

            var apiResource = GetApiResource(apiResourceViewModel);

            var result = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiResource.Name);
            if (result != null)
            {
                dbContext.Entry(result).State = EntityState.Detached;
                apiResource.Id = result.Id;
                apiResource.UserClaims = null;
                dbContext.ApiResources.Attach(apiResource);
                dbContext.Entry(apiResource).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiResource id doesn't exists already : '{apiResource.Name}' ");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{apiResourceId}")]
        public async Task<IActionResult> Delete(string apiResourceId)
        {
            if (String.IsNullOrWhiteSpace(apiResourceId))
                return base.StatusCode(500);

            if (IsApiResourceReadOnly(apiResourceId))
                return base.StatusCode(500, "OpenId and Profile cannot be deleted");

            var result = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiResourceId);
            if (result != null)
            {
                //var apiScopes = await dbContext.ApiScopes.Where(c => c.ApiResource.Name == apiResourceId).ToListAsync();
                //if(apiScopes != null && apiScopes.Any())
                //{
                //    dbContext.RemoveRange(apiScopes);
                //}

                dbContext.Remove(result);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiResource id doesn't exists already : '{apiResourceId}' ");
        }


        private idEnt.ApiResource GetApiResource(ApiResourceUpdatedViewModel apiResourceViewModel)
        {
            var resource = _mapper.Map<idModl.ApiResource>(apiResourceViewModel);
            resource.ApiSecrets = new List<idModl.Secret>();
            if (apiResourceViewModel.Secrets != null)
            {
                foreach (var secret in apiResourceViewModel.Secrets)
                {
                    resource.ApiSecrets.Add(new Secret(secret.Value.Sha256(), secret.Description, (secret.Expiration.HasValue ? secret.Expiration.Value.UtcDateTime : (DateTime?)null)));
                }
            }
            if (apiResourceViewModel.Scopes != null)
            {
                resource.Scopes = new List<Scope>();
                foreach (var prop in apiResourceViewModel.Scopes)
                {
                    resource.Scopes.Add(new Scope()
                    {
                        Description = prop.Description,
                        DisplayName = prop.DisplayName,
                        Emphasize = prop.Emphasize,
                        Name = prop.Name,
                        Required = prop.Required,
                        ShowInDiscoveryDocument = prop.ShowInDiscoveryDocument,
                        UserClaims = prop.UserClaims
                    });
                }
            }
            return resource.ToEntity();
        }

        private ApiResourceUpdatedViewModel GetResourceDetailViewModel(idEnt.ApiResource resource)
        {
            var apiResourceViewModel = _mapper.Map<ApiResourceUpdatedViewModel>(resource.ToModel());
            apiResourceViewModel.Secrets = new List<SecretViewModel>();
            foreach (var secret in resource.Secrets)
            {
                apiResourceViewModel.Secrets.Add(new SecretViewModel() { id = secret.Id, Description = secret.Description, Expiration = secret.Expiration, Type = secret.Type, Value = secret.Value });
            }

            apiResourceViewModel.Scopes = new List<ScopeViewModel>();
            if (resource.Scopes != null)
            {
                foreach (var scope in resource.Scopes)
                {
                    List<string> claims = new List<string>();
                    if (scope.UserClaims != null)
                    {
                        foreach (var claim in scope.UserClaims)
                        {
                            claims.Add(claim.Type);
                        }
                    }
                    apiResourceViewModel.Scopes.Add(new ScopeViewModel()
                    {
                        Description = scope.Description,
                        DisplayName = scope.DisplayName,
                        Emphasize = scope.Emphasize,
                        Name = scope.Name,
                        Required = scope.Required,
                        ShowInDiscoveryDocument = scope.ShowInDiscoveryDocument,
                        UserClaims = claims
                    });
                }
            }

            return apiResourceViewModel;
        }

        private async Task<idEnt.ApiResource> GetApiResourceAllProperties(string resourceName)
        {
            return await dbContext.ApiResources
                .Include(c => c.UserClaims)
                .Include(c => c.Scopes)
                .Include(c => c.Secrets)
                .SingleOrDefaultAsync(c => c.Name == resourceName);
        }
    }

}
