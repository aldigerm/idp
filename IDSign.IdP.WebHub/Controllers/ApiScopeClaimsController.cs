﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IdentityServer4.EntityFramework.Entities;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/ApiScopeClaims")]
    public class ApiScopeClaimsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ApiScopeClaimsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{apiScopeName}")]
        public async Task<IActionResult> Post(string apiScopeName, [FromBody]ApiScopeClaimViewModel claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            if (IsApiScopeReadOnly(apiScopeName))
                return base.StatusCode(500, "Api scope is readonly");

            claim.Type = claim.Type.Trim();

            var result = await dbContext.ApiScopes.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == apiScopeName);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.UserClaims.Any(s => s.Type == claim.Type.Trim()))
                {
                    // add the object and create entity for it
                    var model = new ApiScopeClaim();
                    model.Type = claim.Type;
                    result.UserClaims.Add(model);

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.ApiScope id doesn't exist : '{apiScopeName}'");
        }

        [HttpPost("delete/{apiScopeName}")]
        public async Task<IActionResult> Delete(string apiScopeName, [FromBody]ApiScopeClaimViewModel claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            if (IsApiScopeReadOnly(apiScopeName))
                return base.StatusCode(500, "Api scope is readonly");

            // look for client
            var result = await dbContext.ApiScopes.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == apiScopeName);
            if (result != null && result.UserClaims != null && result.UserClaims.Any())
            {
                // look for matching entity
                var entity = result.UserClaims.Where(s => s.Type == claim.Type).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ApiScopeClaims.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.ApiScope name / claim type doesn't exists : '{apiScopeName}'/'{claim.Type}' ");
        }       
    }
}
