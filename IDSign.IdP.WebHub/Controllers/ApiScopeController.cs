﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IDSign.IdP.WebHub.Models;

namespace IDSign.IdP.Controllers
{
    [Route("api/ApiScope")]
    public class ApiScopeController : BaseAdminController
    {
        private readonly ModelContext dbContext;
        private readonly IMapper _mapper;

        //Inject context instance here.
        public ApiScopeController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ApiScopeUpdatedViewModel, idModl.Scope>();
                cfg.CreateMap<idModl.Scope, ApiScopeUpdatedViewModel>();
                cfg.CreateMap<ApiScopeUpdatedViewModel, idEnt.ApiScope>();
                cfg.CreateMap<idEnt.ApiScope, ApiScopeUpdatedViewModel>();
            });
            _mapper = config.CreateMapper();
        }

        // GET: api/idEnt.ApiScope
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var resources = await dbContext.ApiResources
                .ToListAsync();
            var scopes = await dbContext.ApiScopes
                .ToListAsync();
            var result = new ApiScopeListViewModel();

            result.ApiResourcesList = new List<ApiResourceDetailViewModel>();
            foreach (var resource in resources)
            {
                result.ApiResourcesList.Add(new ApiResourceDetailViewModel()
                {
                    Name = resource.Name,
                    DisplayName = resource.DisplayName
                });
            }

            result.ScopesList = new List<ApiScopeDetailViewModel>();
            foreach (var scope in scopes)
            {
                result.ScopesList.Add(new ApiScopeDetailViewModel()
                {
                    Name = scope.Name,
                    DisplayName = scope.DisplayName,
                    ApiResourceName = scope.ApiResource.Name,
                    Description = scope.Description
                });
            }
            result.ApiResourcesList = result.ApiResourcesList.OrderBy(ir => ir.Name).ToList();
            return Json(result);
        }

        // GET: api/idEnt.ApiScope/5
        [HttpGet("{apiScopeName}")]
        public async Task<IActionResult> Get(string apiScopeName)
        {
            var apiScope = await GetApiResourceAllProperties(apiScopeName);
            if (apiScope == null)
                return NotFound($"idEnt.ApiScope id doesn't exists already : '{apiScopeName}' ");

            var result = GetResourceDetailViewModel(apiScope);

            var resources = await dbContext.ApiResources
                .ToListAsync();
            result.ApiResourcesList = new List<ApiResourceDetailViewModel>();
            foreach (var resource in resources)
            {
                result.ApiResourcesList.Add(new ApiResourceDetailViewModel()
                {
                    EnabledStatus = resource.Enabled.ToString(),
                    Name = resource.Name,
                    DisplayName = resource.DisplayName,
                    Description = resource.Description
                });
            }
            result.ApiResourcesList = result.ApiResourcesList.OrderBy(ir => ir.Name).ToList();
            return Json(result);
        }

        // POST: api/idEnt.ApiScope
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody]ApiScopeUpdatedViewModel apiScopeViewModel)
        {
            if (apiScopeViewModel == null)
                return base.StatusCode(500);

            apiScopeViewModel.Description = apiScopeViewModel.Description?.Trim();
            apiScopeViewModel.DisplayName = apiScopeViewModel.DisplayName?.Trim();
            apiScopeViewModel.Name = apiScopeViewModel.Name?.Trim();

            // apiScopeid must not exist
            var apiResource = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiScopeViewModel.ApiResourceName.Trim());
            if (apiResource != null)
            {
                var apiScopeExists = apiResource.Scopes?.Where(ar => ar.Name == apiScopeViewModel.Name.Trim()).SingleOrDefault();
                if (apiScopeExists != null)
                    return NotFound($"idEnt.ApiScope exists alread'{apiScopeViewModel.ApiResourceName}'");


                idEnt.ApiScope apiScope = null;
                if (String.IsNullOrWhiteSpace(apiScopeViewModel.CopyFromName))
                {
                    // create a new blank apiScope
                    apiScope = new idEnt.ApiScope();
                    apiScope.Description = apiScopeViewModel.Description;
                    apiScope.DisplayName = apiScopeViewModel.DisplayName;
                    apiScope.Emphasize = apiScopeViewModel.Emphasize;
                    apiScope.Name = apiScopeViewModel.Name;
                    apiScope.Required = apiScopeViewModel.Required;
                    apiScope.ShowInDiscoveryDocument = apiScopeViewModel.ShowInDiscoveryDocument;
                    apiScope.UserClaims = null;
                }
                else
                {
                    // create a copy
                    var copy = await GetApiResourceAllProperties(apiScopeViewModel.CopyFromName);

                    if (copy == null)
                        return NotFound($"ApiScope to copy not found {apiScopeViewModel.CopyFromName}");

                    apiScope = new idEnt.ApiScope();
                    apiScope.Description = "Copy of " + copy.Name.Trim();
                    apiScope.Name = apiScopeViewModel.Name.Trim();
                    apiScope.DisplayName = copy.DisplayName.Trim();
                    apiScope.Emphasize = copy.Emphasize;
                    apiScope.Required = copy.Required;
                    apiScope.ShowInDiscoveryDocument = copy.ShowInDiscoveryDocument;
                    apiScope.UserClaims = null;
                }

                apiScope.ApiResource = apiResource;

                await dbContext.ApiScopes.AddAsync(apiScope);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiResource id does not exist : '{apiScopeViewModel.Name}'");
        }

        // PUT: api/idEnt.ApiScope/5
        [HttpPut]
        [Route("PutProperties")]
        public async Task<IActionResult> PutProperties([FromBody]ApiScopeUpdatedViewModel apiScopeViewModel)
        {
            if (apiScopeViewModel == null)
                return base.StatusCode(500);

            if (IsApiScopeReadOnly(apiScopeViewModel.Name))
                return base.StatusCode(500, "Scope is readonly");

            var apiResource = await dbContext.ApiResources.SingleOrDefaultAsync(c => c.Name == apiScopeViewModel.ApiResourceName);
            if (apiResource == null)
                return NotFound($"idEnt.ApiResource does not exists : '{apiScopeViewModel.ApiResourceName}' ");

            apiScopeViewModel.Description = apiScopeViewModel.Description?.Trim();
            apiScopeViewModel.DisplayName = apiScopeViewModel.DisplayName?.Trim();

            var result = await dbContext.ApiScopes.SingleOrDefaultAsync(c => c.Name == apiScopeViewModel.Name);
            if (result != null)
            {
                result.Description = apiScopeViewModel?.Description;
                result.DisplayName = apiScopeViewModel?.DisplayName;
                result.Emphasize = apiScopeViewModel.Emphasize;
                result.Required = apiScopeViewModel.Required;
                result.ShowInDiscoveryDocument = apiScopeViewModel.ShowInDiscoveryDocument;
                result.UserClaims = null;
                result.ApiResource = apiResource;
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiScope id doesn't exists : '{apiScopeViewModel.Name}' ");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{apiScopeName}")]
        public async Task<IActionResult> Delete(string apiScopeName)
        {
            if (String.IsNullOrWhiteSpace(apiScopeName))
                return base.StatusCode(500);

            if (IsApiScopeReadOnly(apiScopeName))
                return base.StatusCode(500, "Scope cannot be deleted");

            var result = await dbContext.ApiScopes.SingleOrDefaultAsync(c => c.Name == apiScopeName);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.ApiScope name doesn't exists : '{apiScopeName}' ");
        }

        private ApiScopeUpdatedViewModel GetResourceDetailViewModel(idEnt.ApiScope resource)
        {
            var apiScopeViewModel = _mapper.Map<ApiScopeUpdatedViewModel>(resource);

            apiScopeViewModel.UserClaims = new List<string>();
            if (resource.UserClaims != null)
            {
                foreach (var prop in resource.UserClaims)
                {
                    apiScopeViewModel.UserClaims.Add(prop.Type);
                }
            }

            apiScopeViewModel.ApiResourceName = resource.ApiResource?.Name;

            return apiScopeViewModel;
        }

        private async Task<idEnt.ApiScope> GetApiResourceAllProperties(string resourceName)
        {
            return await dbContext.ApiScopes
                .Include(c => c.UserClaims)
                .Include(c => c.ApiResource)
                .SingleOrDefaultAsync(c => c.Name == resourceName);
        }
    }

}
