﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IDSign.IdP.WebHub.Models;

namespace IDSign.IdP.Controllers
{
    [Route("api/ClientSecrets")]
    public class ClientSecretsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ClientSecretsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]SecretViewModel secret)
        {
            if (secret == null)
                return base.StatusCode(500);
            
            var result = await dbContext.Clients.Include(s => s.ClientSecrets).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                secret.Value = secret.Value.Trim();
                // if a value with the same expiration state is already present, then we do not add
                if (!result.ClientSecrets.Any(s => s.Value == secret.Value.Sha256() && s.Expiration.HasValue == secret.Expiration.HasValue))
                {
                    // add the secret and create entity for it
                    var model = new Client();
                    model.ClientSecrets.Add(new Secret(secret.Value.Sha256(), secret.Description, (secret.Expiration.HasValue ? secret.Expiration.Value.UtcDateTime : (DateTime?)null)));
                    var temp = model.ToEntity();

                    // add new client secret
                    var secretEntity = temp.ClientSecrets.First();
                    secretEntity.Client = result;
                    result.ClientSecrets.Add(temp.ClientSecrets.First());

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }
        
        [HttpDelete("{clientId}/{secretId}")]
        public async Task<IActionResult> Delete(string clientId, int secretId)
        {
            var result = await dbContext.Clients.Include(s => s.ClientSecrets).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.ClientSecrets != null && result.ClientSecrets.Any())
            {
                var secret = result.ClientSecrets.Where(s => s.Id == secretId).SingleOrDefault();
                if (secret != null)
                {
                    dbContext.ClientSecrets.Remove(secret);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{secretId}' ");
        }
    }
}
