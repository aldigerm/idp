﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/PostLogoutRedirectUri")]
    public class PostLogoutRedirectUriController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public PostLogoutRedirectUriController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]PostLogoutRedirectUri request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.postLogoutUri))
                return base.StatusCode(500);

            request.postLogoutUri = request.postLogoutUri.Trim();

            var result = await dbContext.Clients.Include(s => s.PostLogoutRedirectUris).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.PostLogoutRedirectUris.Any(s => s.PostLogoutRedirectUri.Equals(request.postLogoutUri)))
                {
                    // add the object and create entity for it
                    var model = new Client();
                    model.PostLogoutRedirectUris.Add(request.postLogoutUri);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.PostLogoutRedirectUris.First();
                    result.PostLogoutRedirectUris.Add(entity);

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]PostLogoutRedirectUri request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.postLogoutUri))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.PostLogoutRedirectUris).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.PostLogoutRedirectUris != null && result.PostLogoutRedirectUris.Any())
            {
                // look for matching entity
                var entity = result.PostLogoutRedirectUris.Where(s => s.PostLogoutRedirectUri == request.postLogoutUri).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientPostLogoutRedirectUris.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.postLogoutUri}' ");
        }
    }
}
