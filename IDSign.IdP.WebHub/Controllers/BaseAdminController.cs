﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using Microsoft.AspNetCore.Authorization;
using IDSign.IdP.WebHub.Filters;

namespace IDSign.IdP.Controllers
{
    [Produces("application/json")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [TypeFilter(typeof(AuthorizeAdminFilter))]
    public class BaseAdminController : Controller
    {   
        protected bool IsIdentityResourceReadOnly(string identityResourceName)
        {
            if (identityResourceName == new IdentityResources.OpenId().Name || identityResourceName == new IdentityResources.Profile().Name)
                return true;
            return false;
        }

        protected bool IsApiResourceReadOnly(string identityResourceName)
        {
            return false;
        }

        protected bool IsApiScopeReadOnly(string identityResourceName)
        {
            return false;
        }
        
    }
}
