﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/Claims")]
    public class ClaimsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ClaimsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]ClientClaimProperty request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.type))
                return base.StatusCode(500);

            request.type = request.type.Trim();
            request.value = request.value.Trim();

            var result = await dbContext.Clients.Include(s => s.Claims).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.Claims.Any(s => s.Type.Equals(request.type) && s.Value.Equals(request.value)))
                {
                    // add the object and create entity for it
                    var model = new idModl.Client();
                    model.Claims.Add(new Claim(request.type, request.value));
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.Claims.First();
                    result.Claims.Add(entity);

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]ClientClaimProperty request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.type))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.Claims).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.Claims != null && result.Claims.Any())
            {
                // look for matching entity
                var entity = result.Claims.Where(s => s.Type == request.type && s.Value == request.value).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientClaims.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.type}' ");
        }

        // PUT
        [HttpPut("Properties")]
        public async Task<IActionResult> Put([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);
            
            var result = await dbContext.Clients
                .SingleOrDefaultAsync(c => c.ClientId == clientViewModel.ClientId);
            if (result != null)
            {
                // we remove the current ones
                result.ClientClaimsPrefix = clientViewModel.ClientClaimsPrefix.Trim();
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.Client id doesn't exists already : '{clientViewModel.ClientId}' ");
        }        
    }
}
