﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IDSign.IdP.WebHub.Models;
using IdentityServer4;

namespace IDSign.IdP.Controllers
{
    [Route("api/apiSecrets")]
    public class ApiResourceSecretsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ApiResourceSecretsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{apiResourceName}")]
        public async Task<IActionResult> Post(string apiResourceName, [FromBody]SecretViewModel secret)
        {
            if (secret == null)
                return base.StatusCode(500);
            
            var result = await dbContext.ApiResources.Include(s => s.Secrets).SingleOrDefaultAsync(c => c.Name == apiResourceName);
            if (result != null)
            {
                secret.Type = secret.Type?.Trim() ?? IdentityServerConstants.SecretTypes.X509CertificateBase64;
                secret.Value = secret.Value.Trim();

                // if a value with the same expiration state is already present, then we do not add
                if (!result.Secrets.Any(s => s.Value == secret.Value.Sha256() && s.Expiration.HasValue == secret.Expiration.HasValue))
                {
                    // add the secret and create entity for it
                    var model = new ApiResource();
                    model.ApiSecrets.Add(new Secret(secret.Value.Sha256(), secret.Description, (secret.Expiration.HasValue ? secret.Expiration.Value.UtcDateTime : (DateTime?)null)));
                    var temp = model.ToEntity();

                    // add new client secret
                    var secretEntity = temp.Secrets.First();
                    secretEntity.ApiResource = result;
                    result.Secrets.Add(temp.Secrets.First());

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.ApiResource id doesn't exist : '{apiResourceName}'");
        }
        
        [HttpDelete("{apiResourceName}/{secretId}")]
        public async Task<IActionResult> Delete(string apiResourceName, int secretId)
        {
            var result = await dbContext.ApiResources.Include(s => s.Secrets).SingleOrDefaultAsync(c => c.Name == apiResourceName);
            if (result != null && result.Secrets != null && result.Secrets.Any())
            {
                var secret = result.Secrets.Where(s => s.Id == secretId).SingleOrDefault();
                if (secret != null)
                {
                    dbContext.ApiSecrets.Remove(secret);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.ApiResource id / secret id doesn't exists : '{apiResourceName}'/'{secretId}' ");
        }
    }
}
