﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/GrantType")]
    public class GrantTypeController : BaseAdminController
    {
        private readonly ModelContext dbContext;
        private readonly IMapper _mapper;

        //Inject context instance here.
        public GrantTypeController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ClientUpdatedViewModel, idModl.Client>()
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    ;

                cfg.CreateMap<idModl.Client, ClientUpdatedViewModel>()
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    .ForMember(x => x.ClientProperties, opt => opt.Ignore())
                    ;
            });
            _mapper = config.CreateMapper();
        }

        // PUT
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);

            var client = GetClient(clientViewModel);

            var result = await dbContext.Clients
                .Include(c => c.AllowedGrantTypes)
                .SingleOrDefaultAsync(c => c.ClientId == client.ClientId);
            if (result != null)
            {
                // we remove the current ones
                // TODO: remove/add only missing/new
                if (result.AllowedGrantTypes != null && result.AllowedGrantTypes.Any())
                {
                    dbContext.GrantTypes.RemoveRange(result.AllowedGrantTypes);
                }

                result.AllowedGrantTypes = client.AllowedGrantTypes;
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.Client id doesn't exists already : '{client.ClientId}' ");
        }

        private idEnt.Client GetClient(ClientUpdatedViewModel clientViewModel)
        {
            var client = _mapper.Map<idModl.Client>(clientViewModel);

            client.AllowedGrantTypes = GrantTypes.Implicit;
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.Implicit.ToString()) { client.AllowedGrantTypes = GrantTypes.Implicit; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.ImplicitAndClientCredentials.ToString()) { client.AllowedGrantTypes = GrantTypes.ImplicitAndClientCredentials; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.Code.ToString()) { client.AllowedGrantTypes = GrantTypes.Code; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.CodeAndClientCredentials.ToString()) { client.AllowedGrantTypes = GrantTypes.CodeAndClientCredentials; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.Hybrid.ToString()) { client.AllowedGrantTypes = GrantTypes.Hybrid; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.HybridAndClientCredentials.ToString()) { client.AllowedGrantTypes = GrantTypes.HybridAndClientCredentials; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.ClientCredentials.ToString()) { client.AllowedGrantTypes = GrantTypes.ClientCredentials; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.ResourceOwnerPassword.ToString()) { client.AllowedGrantTypes = GrantTypes.ResourceOwnerPassword; }
            if (clientViewModel.AllowedGrantTypesName == GrantTypesNames.ResourceOwnerPasswordAndClientCredentials.ToString()) { client.AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials; }

            return client.ToEntity();
        }
    }
}
