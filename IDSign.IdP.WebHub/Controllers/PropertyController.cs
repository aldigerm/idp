﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IdentityServer4.EntityFramework.Entities;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/Property")]
    public class PropertyController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public PropertyController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]ClientNameValueProperty request)
        {
            if (request == null  || String.IsNullOrWhiteSpace(request.key))
                return base.StatusCode(500);

            request.key = request.key.Trim();
            request.value = request.value.Trim();

            var result = await dbContext.Clients.Include(s => s.Properties).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.Properties.Any(s => s.Key.Equals(request.key)))
                {
                    // add the object and create entity for it
                    var model = new idModl.Client();
                    model.Properties.Add(request.key, request.value);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.Properties.First();
                    entity.Client = result;
                    result.Properties.Add(temp.Properties.First());

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]ClientNameValueProperty request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.key))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.Properties).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.Properties != null && result.Properties.Any())
            {
                // look for matching entity
                var entity = result.Properties.Where(s => s.Key == request.key).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientProperties.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.key}' ");
        }
    }
}
