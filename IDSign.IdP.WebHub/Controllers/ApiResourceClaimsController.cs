﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IdentityServer4.EntityFramework.Mappers;

namespace IDSign.IdP.Controllers
{
    [Route("api/ApiResourceClaims")]
    public class ApiResourceClaimsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public ApiResourceClaimsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{apiResourceName}")]
        public async Task<IActionResult> Post(string apiResourceName, [FromBody]ApiScopeClaimViewModel claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            claim.Type = claim.Type.Trim();

            if (IsApiResourceReadOnly(apiResourceName))
                return base.StatusCode(500, "Api resource is readonly");

            var result = await dbContext.ApiResources.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == apiResourceName);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.UserClaims.Any(s => s.Type == claim.Type))
                {
                    var userClaim = new IdentityServer4.EntityFramework.Entities.ApiResourceClaim();
                    userClaim.Type = claim.Type;
                    result.UserClaims.Add(userClaim);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.ApiResource id doesn't exist : '{apiResourceName}'");
        }

        [HttpPost("delete/{apiResourceName}")]
        public async Task<IActionResult> Delete(string apiResourceName, [FromBody]ApiResourceClaim claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            if (IsApiResourceReadOnly(apiResourceName))
                return base.StatusCode(500, "Api resource is readonly");

            // look for client
            var result = await dbContext.ApiResources.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == apiResourceName);
            if (result != null && result.UserClaims != null && result.UserClaims.Any())
            {
                // look for matching entity
                var entity = result.UserClaims.Where(s => s.Type == claim.Type).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ApiResourceClaims.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.ApiResource id / secret id doesn't exists : '{apiResourceName}'/'{claim.Type}' ");
        }       
    }
}
