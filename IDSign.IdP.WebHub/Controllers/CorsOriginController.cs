﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/CorsOrigin")]
    public class CorsOriginController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public CorsOriginController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]CorsOrigin request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.origin))
                return base.StatusCode(500);

            request.origin = request.origin.Trim();

            var result = await dbContext.Clients.Include(s => s.AllowedCorsOrigins).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.AllowedCorsOrigins.Any(s => s.Origin.Equals(request.origin)))
                {
                    // add the object and create entity for it
                    var model = new Client();
                    model.AllowedCorsOrigins.Add(request.origin);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.AllowedCorsOrigins.First();
                    entity.Client = result;
                    result.AllowedCorsOrigins.Add(temp.AllowedCorsOrigins.First());

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]CorsOrigin request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.origin))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.AllowedCorsOrigins).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.AllowedCorsOrigins != null && result.AllowedCorsOrigins.Any())
            {
                // look for matching entity
                var entity = result.AllowedCorsOrigins.Where(s => s.Origin == request.origin).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientCorsOrigins.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.origin}' ");
        }
    }
}
