﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/IdentityResourceUserClaims")]
    public class IdentityResourceUserClaimsController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public IdentityResourceUserClaimsController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{identityResourceName}")]
        public async Task<IActionResult> Post(string identityResourceName, [FromBody]IdentityResourceClaim claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            if (IsIdentityResourceReadOnly(identityResourceName))
                return base.StatusCode(500, "Identity resource is readonly");

            var result = await dbContext.IdentityResources.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == identityResourceName);
            if (result != null)
            {
                claim.Type = claim.Type.Trim();
                // if a value with the same expiration state is already present, then we do not add
                if (!result.UserClaims.Any(s => s.Type == claim.Type))
                {
                    // add the object and create entity for it
                    var model = new idModl.IdentityResource();
                    model.UserClaims.Add(claim.Type);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.UserClaims.First();
                    result.UserClaims.Add(entity);

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.IdentityResource id doesn't exist : '{identityResourceName}'");
        }

        [HttpPost("delete/{identityResourceName}")]
        public async Task<IActionResult> Delete(string identityResourceName, [FromBody]IdentityResourceClaim claim)
        {
            if (claim == null || String.IsNullOrWhiteSpace(claim.Type))
                return base.StatusCode(500);

            if (IsIdentityResourceReadOnly(identityResourceName))
                return base.StatusCode(500, "Identity resource is readonly");

            // look for client
            var result = await dbContext.IdentityResources.Include(s => s.UserClaims).SingleOrDefaultAsync(c => c.Name == identityResourceName);
            if (result != null && result.UserClaims != null && result.UserClaims.Any())
            {
                // look for matching entity
                var entity = result.UserClaims.Where(s => s.Type == claim.Type).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.IdentityClaims.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.IdentityResource id / secret id doesn't exists : '{identityResourceName}'/'{claim.Type}' ");
        }       
    }
}
