﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;
using IDSign.IdP.WebHub.Models;

namespace IDSign.IdP.Controllers
{
    [Route("api/Client")]
    public class ClientController : BaseAdminController
    {
        private readonly ModelContext dbContext;
        private readonly IMapper _mapper;

        //Inject context instance here.
        public ClientController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<idModl.Client, idModl.Client>();

                cfg.CreateMap<ClientUpdatedViewModel, idModl.Client>()
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    ;

                cfg.CreateMap<idModl.Client, ClientUpdatedViewModel>()
                    .ForMember(x => x.ClientSecrets, opt => opt.Ignore())
                    .ForMember(x => x.ClientProperties, opt => opt.Ignore())
                    ;
            });
            _mapper = config.CreateMapper();
        }

        // GET: api/idEnt.Client
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var clients = await dbContext.Clients
                .ToListAsync();
            var result = new ClientListViewModel();
            result.ClientsList = new List<ClientBasicViewModel>();
            foreach (var client in clients)
            {
                result.ClientsList.Add(new ClientBasicViewModel() {EnabledStatus = client.Enabled.ToString(), ClientId = client.ClientId, ClientName = client.ClientName });
            }
            result.ClientsList = result.ClientsList.OrderBy(cl => cl.ClientId).ToList();
            return Json(result);
        }

        // GET: api/idEnt.Client/5
        [HttpGet("{clientId}")]
        public async Task<IActionResult> Get(string clientId)
        {
            var client = await GetClientAllProperties(clientId);
            if (client == null)
                return NotFound($"idEnt.Client id doesn't exists already : '{clientId}' ");

            var result = GetClientViewModel(client);
            return Json(result);
        }

        // POST: api/idEnt.Client
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);

            var client = GetClient(clientViewModel);

            var result = await dbContext.Clients.SingleOrDefaultAsync(c => c.ClientId == client.ClientId);
            if (result == null)
            {
                await dbContext.Clients.AddAsync(client);
                await dbContext.SaveChangesAsync();
                return Ok();
            }
            return NotFound($"idEnt.Client id exists already : '{client.ClientId}'. Use PUT to update.");
        }

        // POST: api/idEnt.Client
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);

            // clientid must not exist
            var result = await dbContext.Clients.SingleOrDefaultAsync(c => c.ClientId == clientViewModel.ClientId);
            if (result == null)
            {
                idEnt.Client client = null;
                if (String.IsNullOrWhiteSpace(clientViewModel.CopyFromClientId))
                {
                    // create a new blank client
                    client = GetClient(clientViewModel);
                }
                else
                {
                    // create a copy
                    var copy = await GetClientAllProperties(clientViewModel.CopyFromClientId);

                    if (copy == null)
                        return NotFound($"Client to copy not found {clientViewModel.CopyFromClientId}");

                    var model = copy.ToModel();
                    model.ClientId = clientViewModel.ClientId;
                    model.ClientName = "Copy of " + model.ClientName;
                    client = model.ToEntity();
                }

                await dbContext.Clients.AddAsync(client);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.Client id exists already : '{clientViewModel.ClientId}'");
        }

        // PUT: api/idEnt.Client/5
        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);

            var client = GetClient(clientViewModel);

            var result = await dbContext.Clients.SingleOrDefaultAsync(c => c.ClientId == client.ClientId);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.Clients.AddAsync(client);
                await dbContext.SaveChangesAsync();
                await dbContext.SaveChangesAsync();

                return Ok();
            }
            return NotFound($"idEnt.Client id doesn't exists already : '{client.ClientId}' ");
        }

        // PUT: api/idEnt.Client/5
        [HttpPut]
        [Route("PutProperties")]
        public async Task<IActionResult> PutProperties([FromBody]ClientUpdatedViewModel clientViewModel)
        {
            if (clientViewModel == null)
                return base.StatusCode(500);

            var client = GetClient(clientViewModel);

            var result = await dbContext.Clients.SingleOrDefaultAsync(c => c.ClientId == client.ClientId);
            if (result != null)
            {
                dbContext.Entry(result).State = EntityState.Detached;
                client.Id = result.Id;
                client.PostLogoutRedirectUris = null;
                client.ClientSecrets = null;
                client.RedirectUris = null;
                client.AllowedScopes = null;
                client.AllowedGrantTypes = null;
                client.Properties = null;
                client.AllowedCorsOrigins = null;
                client.PostLogoutRedirectUris = null;
                client.ClientSecrets = null;
                client.Claims = null;
                dbContext.Clients.Attach(client);
                dbContext.Entry(client).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.Client id doesn't exists already : '{client.ClientId}' ");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{clientId}")]
        public async Task<IActionResult> Delete(string clientId)
        {
            var result = await dbContext.Clients.SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.Client id doesn't exists already : '{clientId}' ");
        }

        private idEnt.Client GetClient(ClientUpdatedViewModel clientViewModel)
        {
            if (!(ProtocolTypes.OpenIdConnect == clientViewModel.ProtocolType ||
                   ProtocolTypes.WsFederation == clientViewModel.ProtocolType ||
                   ProtocolTypes.Saml2p == clientViewModel.ProtocolType))
            {
                // falling to a default
                clientViewModel.ProtocolType = ProtocolTypes.OpenIdConnect;
                //($"ProtocolType is invalid. Must be {ProtocolTypes.OpenIdConnect},{ProtocolTypes.WsFederation},{ProtocolTypes.Saml2p}");
            }

            var client = _mapper.Map<idModl.Client>(clientViewModel);
            client.ClientSecrets = new List<idModl.Secret>();
            if (clientViewModel.ClientSecrets != null)
            {
                foreach (var secret in clientViewModel.ClientSecrets)
                {
                    client.ClientSecrets.Add(new Secret(secret.Value.Sha256(), secret.Description, (secret.Expiration.HasValue ? secret.Expiration.Value.UtcDateTime : (DateTime?)null)));
                }
            }
            if (clientViewModel.ClientProperties != null)
            {
                client.Properties = new Dictionary<string, string>();
                foreach (var prop in clientViewModel.ClientProperties)
                {
                    client.Properties.Add(prop.key, prop.value);
                }
            }
            if (clientViewModel.ClientClaims != null)
            {
                client.Claims = new List<Claim>();
                foreach (var prop in clientViewModel.ClientClaims)
                {
                    client.Claims.Add(new Claim(prop.type, prop.value));
                }
            }
            return client.ToEntity();
        }

        private ClientUpdatedViewModel GetClientViewModel(idEnt.Client client)
        {
            var clientViewModel = _mapper.Map<ClientUpdatedViewModel>(client.ToModel());
            clientViewModel.ClientSecrets = new List<SecretViewModel>();
            foreach (var secret in client.ClientSecrets)
            {
                clientViewModel.ClientSecrets.Add(new SecretViewModel() { id = secret.Id, Description = secret.Description, Expiration = secret.Expiration, Type = secret.Type, Value = secret.Value });
            }

            clientViewModel.ClientProperties = new List<ClientNameValueProperty>();
            if (client.Properties != null)
            {
                foreach (var prop in client.Properties)
                {
                    clientViewModel.ClientProperties.Add(new ClientNameValueProperty() { key = prop.Key, value = prop.Value });
                }
            }

            clientViewModel.ClientClaims = new List<ClientClaimProperty>();
            if (client.Claims != null)
            {
                foreach (var prop in client.Claims)
                {
                    clientViewModel.ClientClaims.Add(new ClientClaimProperty() { id = prop.Id, type = prop.Type, value = prop.Value });
                }
            }

            clientViewModel.AllowedGrantTypesName = GrantTypesNames.Implicit.ToString();
            if (CompareLists(GrantTypes.Implicit, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.Implicit.ToString(); }
            if (CompareLists(GrantTypes.ImplicitAndClientCredentials, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.ImplicitAndClientCredentials.ToString(); }
            if (CompareLists(GrantTypes.Code, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.Code.ToString(); }
            if (CompareLists(GrantTypes.CodeAndClientCredentials, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.CodeAndClientCredentials.ToString(); }
            if (CompareLists(GrantTypes.Hybrid, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.Hybrid.ToString(); }
            if (CompareLists(GrantTypes.HybridAndClientCredentials, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.HybridAndClientCredentials.ToString(); }
            if (CompareLists(GrantTypes.ClientCredentials, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.ClientCredentials.ToString(); }
            if (CompareLists(GrantTypes.ResourceOwnerPassword, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.ResourceOwnerPassword.ToString(); }
            if (CompareLists(GrantTypes.ResourceOwnerPasswordAndClientCredentials, clientViewModel.AllowedGrantTypes)) { clientViewModel.AllowedGrantTypesName = GrantTypesNames.ResourceOwnerPasswordAndClientCredentials.ToString(); }

            return clientViewModel;
        }

        private bool CompareLists(ICollection<string> list1, ICollection<string> list2)
        {
            if (list1 == null || list2 == null)
                return false;

            if (list1.Count != list2.Count)
                return false;

            return list1.All(l => list2.Any(l2 => l == l2));
        }

        private async Task<idEnt.Client> GetClientAllProperties(string clientId)
        {
            return await dbContext.Clients
                .Include(c => c.RedirectUris)
                .Include(c => c.AllowedScopes)
                .Include(c => c.AllowedGrantTypes)
                .Include(c => c.Properties)
                .Include(c => c.AllowedCorsOrigins)
                .Include(c => c.PostLogoutRedirectUris)
                .Include(c => c.ClientSecrets)
                .Include(c => c.Claims)
                .Include(c => c.IdentityProviderRestrictions)
                .SingleOrDefaultAsync(c => c.ClientId == clientId);
        }
    }
}
