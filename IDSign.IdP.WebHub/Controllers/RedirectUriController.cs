﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/RedirectUri")]
    public class RedirectUriController : BaseAdminController
    {
        private readonly ModelContext dbContext;

        //Inject context instance here.
        public RedirectUriController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost("{clientId}")]
        public async Task<IActionResult> Post(string clientId, [FromBody]RedirectUri request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.redirectUri))
                return base.StatusCode(500);

            request.redirectUri = request.redirectUri.Trim();

            var result = await dbContext.Clients.Include(s => s.RedirectUris).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null)
            {
                // if a value with the same expiration state is already present, then we do not add
                if (!result.RedirectUris.Any(s => s.RedirectUri.Equals(request.redirectUri)))
                {
                    // add the object and create entity for it
                    var model = new Client();
                    model.RedirectUris.Add(request.redirectUri);
                    var temp = model.ToEntity();

                    // add new entity
                    var entity = temp.RedirectUris.First();
                    entity.Client = result;
                    result.RedirectUris.Add(temp.RedirectUris.First());

                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
                else
                {
                    return base.StatusCode(500);
                }
            }
            return NotFound($"idEnt.Client id doesn't exist : '{clientId}'");
        }

        [HttpPost("delete/{clientId}")]
        public async Task<IActionResult> Delete(string clientId, [FromBody]RedirectUri request)
        {
            if (request == null || String.IsNullOrWhiteSpace(request.redirectUri))
                return base.StatusCode(500);

            // look for client
            var result = await dbContext.Clients.Include(s => s.RedirectUris).SingleOrDefaultAsync(c => c.ClientId == clientId);
            if (result != null && result.RedirectUris != null && result.RedirectUris.Any())
            {
                // look for matching entity
                var entity = result.RedirectUris.Where(s => s.RedirectUri == request.redirectUri).SingleOrDefault();
                if (entity != null)
                {
                    // remove the entity
                    dbContext.ClientRedirectUris.Remove(entity);
                    await dbContext.SaveChangesAsync();
                    return Ok("Ok");
                }
            }
            return NotFound($"idEnt.Client id / secret id doesn't exists : '{clientId}'/'{request.redirectUri}' ");
        }
    }
}
