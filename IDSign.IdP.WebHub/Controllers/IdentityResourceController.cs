﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IdentityServer4.EntityFramework.DbContexts;
using idEnt = IdentityServer4.EntityFramework.Entities;
using idModl = IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static IdentityServer4.IdentityServerConstants;
using IdentityServer4.Models;
using IdentityServer4.EntityFramework.Mappers;
using System.Security.Claims;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Model;

namespace IDSign.IdP.Controllers
{
    [Route("api/IdentityResource")]
    public class IdentityResourceController : BaseAdminController
    {
        private readonly ModelContext dbContext;
        private readonly IMapper _mapper;

        //Inject context instance here.
        public IdentityResourceController(ModelContext dbContext)
        {
            this.dbContext = dbContext;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<IdentityResourceDetailViewModel, idModl.IdentityResource>();
                cfg.CreateMap<idModl.IdentityResource, IdentityResourceDetailViewModel>();
            });
            _mapper = config.CreateMapper();
        }

        // GET: api/idEnt.IdentityResource
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var resources = await dbContext.IdentityResources
                .ToListAsync();
            var result = new IdentityResourceListViewModel();
            result.IdentityResourcesList = new List<IdentityResourceDetailViewModel>();
            foreach (var resource in resources)
            {
                result.IdentityResourcesList.Add(new IdentityResourceDetailViewModel()
                {
                    EnabledStatus = resource.Enabled.ToString(),
                    Name = resource.Name,
                    DisplayName = resource.DisplayName,
                    Description = resource.Description
                });
            }
            result.IdentityResourcesList = result.IdentityResourcesList.OrderBy(ir => ir.Name).ToList();
            return Json(result);
        }

        // GET: api/idEnt.IdentityResource/5
        [HttpGet("{identityResourceName}")]
        public async Task<IActionResult> Get(string identityResourceName)
        {
            var identityResource = await GetIdentityResourceAllProperties(identityResourceName);
            if (identityResource == null)
                return NotFound($"idEnt.IdentityResource id doesn't exists already : '{identityResourceName}' ");

            var result = GetResourceDetailViewModel(identityResource);
            return Json(result);
        }
        
        // POST: api/idEnt.IdentityResource
        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody]IdentityResourceUpdatedViewModel identityResourceViewModel)
        {
            if (identityResourceViewModel == null)
                return base.StatusCode(500);

            // identityResourceid must not exist
            var result = await dbContext.IdentityResources.SingleOrDefaultAsync(c => c.Name == identityResourceViewModel.Name);
            if (result == null)
            {
                idEnt.IdentityResource identityResource = null;
                if (String.IsNullOrWhiteSpace(identityResourceViewModel.CopyFromName))
                {
                    // create a new blank identityResource
                    identityResource.Name = identityResource.Name.Trim();
                    identityResource = GetIdentityResource(identityResourceViewModel);
                }
                else
                {
                    // create a copy
                    var copy = await GetIdentityResourceAllProperties(identityResourceViewModel.CopyFromName);

                    if (copy == null)
                        return NotFound($"IdentityResource to copy not found {identityResourceViewModel.CopyFromName}");

                    var model = copy.ToModel();
                    model.Description = "Copy of " + model.Name;
                    model.Name = identityResourceViewModel.Name;
                    identityResource = model.ToEntity();
                }

                await dbContext.IdentityResources.AddAsync(identityResource);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.IdentityResource id exists already : '{identityResourceViewModel.Name}'");
        }

        // PUT: api/idEnt.IdentityResource/5
        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put([FromBody]IdentityResourceUpdatedViewModel identityResourceViewModel)
        {
            if (identityResourceViewModel == null)
                return base.StatusCode(500);

            if (IsIdentityResourceReadOnly(identityResourceViewModel.Name))
                return base.StatusCode(500, "Identity resource is readonly");

            var identityResource = GetIdentityResource(identityResourceViewModel);

            var result = await dbContext.IdentityResources.SingleOrDefaultAsync(c => c.Name == identityResource.Name);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.IdentityResources.AddAsync(identityResource);
                await dbContext.SaveChangesAsync();

                return Ok();
            }
            return NotFound($"idEnt.IdentityResource id doesn't exists already : '{identityResource.Name}' ");
        }

        // PUT: api/idEnt.IdentityResource/5
        [HttpPut]
        [Route("PutProperties")]
        public async Task<IActionResult> PutProperties([FromBody]IdentityResourceUpdatedViewModel identityResourceViewModel)
        {
            if (identityResourceViewModel == null)
                return base.StatusCode(500);

            if (IsIdentityResourceReadOnly(identityResourceViewModel.Name))
                return base.StatusCode(500, "Identity resource is readonly");

            var identityResource = GetIdentityResource(identityResourceViewModel);

            var result = await dbContext.IdentityResources.SingleOrDefaultAsync(c => c.Name == identityResource.Name);
            if (result != null)
            {
                dbContext.Entry(result).State = EntityState.Detached;
                identityResource.Id = result.Id;
                identityResource.UserClaims = null;
                dbContext.IdentityResources.Attach(identityResource);
                dbContext.Entry(identityResource).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();

                return Ok("Ok");
            }
            return NotFound($"idEnt.IdentityResource id doesn't exists already : '{identityResource.Name}' ");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{identityResourceId}")]
        public async Task<IActionResult> Delete(string identityResourceId)
        {
            if (String.IsNullOrWhiteSpace(identityResourceId))
                return base.StatusCode(500);

            if (IsIdentityResourceReadOnly(identityResourceId))
                return base.StatusCode(500, "OpenId and Profile cannot be deleted");

            var result = await dbContext.IdentityResources.SingleOrDefaultAsync(c => c.Name == identityResourceId);
            if (result != null)
            {
                dbContext.Remove(result);
                await dbContext.SaveChangesAsync();
                return Ok("Ok");
            }
            return NotFound($"idEnt.IdentityResource id doesn't exists already : '{identityResourceId}' ");
        }

        private idEnt.IdentityResource GetIdentityResource(IdentityResourceDetailViewModel identityResourceViewModel)
        {
            var resource = _mapper.Map<idModl.IdentityResource>(identityResourceViewModel);
            return resource.ToEntity();
        }

        private IdentityResourceDetailViewModel GetResourceDetailViewModel(idEnt.IdentityResource resource)
        {
            var resourceViewModel = _mapper.Map<IdentityResourceDetailViewModel>(resource.ToModel());
            return resourceViewModel;
        }

        private bool CompareLists(ICollection<string> list1, ICollection<string> list2)
        {
            if (list1 == null || list2 == null)
                return false;

            if (list1.Count != list2.Count)
                return false;

            return list1.All(l => list2.Any(l2 => l == l2));
        }

        private async Task<idEnt.IdentityResource> GetIdentityResourceAllProperties(string resourceName)
        {
            return await dbContext.IdentityResources
                .Include(c => c.UserClaims)
                .SingleOrDefaultAsync(c => c.Name == resourceName);
        }
    }

}
