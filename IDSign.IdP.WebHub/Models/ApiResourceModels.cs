﻿using IDSign.IdP.WebHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.WebHub.Model
{
    public class ApiResourceListViewModel
    {
        public List<ApiResourceDetailViewModel> ApiResourcesList { get; set; }
    }

    public class ApiResourceDetailViewModel : Resource
    {
        //
        // Summary:
        //     Specifies if client is enabled (defaults to true)
        public string EnabledStatus { get; set; }

        public List<SecretViewModel> Secrets { get; set; }
        public List<ScopeViewModel> Scopes { get; set; }
    }

    public class ScopeViewModel
    {
        //
        // Summary:
        //     Name of the scope. This is the value a client will use to request the scope.
        public string Name { get; set; }
        //
        // Summary:
        //     Display name. This value will be used e.g. on the consent screen.
        public string DisplayName { get; set; }
        //
        // Summary:
        //     Description. This value will be used e.g. on the consent screen.
        public string Description { get; set; }
        //
        // Summary:
        //     Specifies whether the user can de-select the scope on the consent screen. Defaults
        //     to false.
        public bool Required { get; set; }
        //
        // Summary:
        //     Specifies whether the consent screen will emphasize this scope. Use this setting
        //     for sensitive or important scopes. Defaults to false.
        public bool Emphasize { get; set; }
        //
        // Summary:
        //     Specifies whether this scope is shown in the discovery document. Defaults to
        //     true.
        public bool ShowInDiscoveryDocument { get; set; }
        //
        // Summary:
        //     List of user-claim types that should be included in the access token.
        public ICollection<string> UserClaims { get; set; }
    }

    public class ApiResourceUpdatedViewModel : ApiResourceDetailViewModel
    {
        public string CopyFromName { get; set; }
    }

    public class ApiResourceClaim
    {
        public string Type { get; set; }
    }
}
