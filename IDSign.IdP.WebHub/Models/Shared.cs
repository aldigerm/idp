﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.WebHub.Models
{
    public abstract class Resource
    {
        //
        // Summary:
        //     Indicates if this resource is enabled. Defaults to true.
        public bool Enabled { get; set; }
        //
        // Summary:
        //     The unique name of the resource.
        public string Name { get; set; }
        //
        // Summary:
        //     Display name of the resource.
        public string DisplayName { get; set; }
        //
        // Summary:
        //     Description of the resource.
        public string Description { get; set; }
        //
        // Summary:
        //     List of accociated user claims that should be included when this resource is
        //     requested.
        public ICollection<string> UserClaims { get; set; }
    }


    public class SecretViewModel
    {
        public int id { get; set; }
        //
        // Summary:
        //     Gets or sets the description.
        public string Description { get; set; }
        //
        // Summary:
        //     Gets or sets the value.
        public string Value { get; set; }
        //
        // Summary:
        //     Gets or sets the expiration.
        public DateTimeOffset? Expiration { get; set; }

        //
        // Summary:
        //     Gets or sets the type of the client secret.
        public string Type { get; set; }
    }
}
