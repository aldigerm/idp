﻿using IDSign.IdP.WebHub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDSign.IdP.WebHub.Model
{
    public class ApiScopeListViewModel
    {
        public List<ApiScopeDetailViewModel> ScopesList { get; set; }
        public List<ApiResourceDetailViewModel> ApiResourcesList { get; set; }
    }
    public class ApiScopeDetailViewModel : Resource
    {
        public string ApiResourceName { get; set; }
        public string EnabledStatus { get; internal set; }

        //
        // Summary:
        //     Specifies whether the user can de-select the scope on the consent screen (if
        //     the consent screen wants to implement such a feature). Defaults to false.
        public bool Required { get; set; }
        //
        // Summary:
        //     Specifies whether the consent screen will emphasize this scope (if the consent
        //     screen wants to implement such a feature). Use this setting for sensitive or
        //     important scopes. Defaults to false.
        public bool Emphasize { get; set; }
        //
        // Summary:
        //     Specifies whether this scope is shown in the discovery document. Defaults to
        //     true.
        public bool ShowInDiscoveryDocument { get; set; }

        public List<ApiResourceDetailViewModel> ApiResourcesList { get; set; }
    }


    public class ApiScopeUpdatedViewModel : ApiScopeDetailViewModel
    {
        public string CopyFromName { get; set; }
    }

    public class ApiScopeClaimViewModel
    {
        public string Type { get; set; }
    }
}
