﻿using IDSign.IdP.WebHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.WebHub.Model
{
    public class IdentityResourceListViewModel
    {
        public IList<IdentityResourceDetailViewModel> IdentityResourcesList { get; set; }
    }

    public class IdentityResourceDetailViewModel : Resource
    {

        public string EnabledStatus { get; internal set; }

        //
        // Summary:
        //     Specifies whether the user can de-select the scope on the consent screen (if
        //     the consent screen wants to implement such a feature). Defaults to false.
        public bool Required { get; set; }
        //
        // Summary:
        //     Specifies whether the consent screen will emphasize this scope (if the consent
        //     screen wants to implement such a feature). Use this setting for sensitive or
        //     important scopes. Defaults to false.
        public bool Emphasize { get; set; }
        //
        // Summary:
        //     Specifies whether this scope is shown in the discovery document. Defaults to
        //     true.
        public bool ShowInDiscoveryDocument { get; set; }
    }


    public class IdentityResourceUpdatedViewModel : IdentityResourceDetailViewModel
    {
        public string CopyFromName { get; set; }
    }

    public class IdentityResourceClaim
    {
        public string Type { get; set; }
    }
}
