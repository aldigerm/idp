﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using IDSign.IdP.Core;
using IDSign.IdP.Model.Configuration;
using IDSign.IdP.Model.EF;
using IDSign.IdP.WebHub.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using System.IO;
using System.Linq;
using System.Reflection;

namespace IDSign.IdP.WebHub
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IDSignIdpConfigurationSection IDSignConfiguration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            IConfigurationSection sectionData = Configuration.GetSection("IDSignIdP");
            IDSignConfiguration = new IDSignIdpConfigurationSection();
            sectionData.Bind(IDSignConfiguration);

            // add the settings to be able to inject them
            services.AddSingleton<IDSignIdpConfigurationSection>(IDSignConfiguration);
            services.AddScoped<AuthorizeAdminFilter>();
            services.AddScoped<AuthorizeAdminAttribute>();

            Log.Logger.Information($"IDSignIdP settings read are :\n {JsonConvert.SerializeObject(IDSignConfiguration, Formatting.Indented)}");


            string connectionString = "";
            connectionString = Configuration.GetConnectionString("DefaultConnection");

            string efMigrationAssemblies = typeof(IDSign.IdP.Model.EF.IdentityServerInitializer).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<ModelContext>(options =>
                    options.UseSqlServer(connectionString));

            services.AddDbContext<ConfigurationDbContext>(options =>
                    options.UseSqlServer(connectionString));

            services.AddSingleton<ConfigurationStoreOptions>(new ConfigurationStoreOptions());

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = IDSignConfiguration?.IdPAdmin?.Authority;
                    options.RequireHttpsMetadata = false;
                    options.ApiName = IDSignConfiguration?.IdPAdmin?.ApiName;// "api1";
                });

            #region Cors

            var corsSettings = IDSignConfiguration?.CorsSettings;

            if (corsSettings != null && corsSettings.EnableCors)
            {
                if (corsSettings.AllowAnyOrigin)
                {
                    services.AddCors(options =>
                    {
                        // this defines a CORS policy called "default"
                        options.AddPolicy("default", policy =>
                        {
                            policy.AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        });
                    });
                }
                else if (corsSettings.WithOrigins.Any())
                {
                    services.AddCors(options =>
                    {
                        // this defines a CORS policy called "default"
                        options.AddPolicy("default", policy =>
                        {
                            policy.WithOrigins(corsSettings.WithOrigins).AllowAnyHeader().AllowAnyMethod()
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                        });
                    });
                }
            }
            #endregion

            // automapper
            services.AddAutoMapper();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            var corsSettings = IDSignConfiguration?.CorsSettings;

            if (corsSettings != null && corsSettings.EnableCors && (corsSettings.AllowAnyOrigin || (corsSettings.WithOrigins != null && corsSettings.WithOrigins.Any())))
            {
                app.UseCors("default");
            }

            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            string onlineUrlCheck = Configuration["OnlineUrlCheck"];
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    context.Request.Path.Value.ToLower().EndsWith(onlineUrlCheck))
                {
                    context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync("true");
                }
            });
        }
    }
}