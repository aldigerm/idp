﻿using IDSign.IdP.WebHub.Helpers;
using IDSign.IdP.Model.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace IDSign.IdP.WebHub.Filters
{
    public class AuthorizeAdminAttribute : TypeFilterAttribute
    {
        public AuthorizeAdminAttribute(IDSignIdpConfigurationSection settings) : base(typeof(AuthorizeAdminFilter))
        {
            Arguments = new object[] { };
        }
    }

    public class AuthorizeAdminFilter : IAuthorizationFilter
    {
        public IDSignIdpConfigurationSection Settings { get; set; }

        public AuthorizeAdminFilter(IDSignIdpConfigurationSection settings)
        {
            this.Settings = settings;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // check if the user is in the list of allowed users set in the appsettings
            var username = context.HttpContext.User.Claims.Where(c => c.Type == "sub").Select(c => c.Value).FirstOrDefault();
            Log.Logger.Debug($"Checking authorization of user {username}");
            if (Settings?.IdPAdmin?.Users == null || !Settings.IdPAdmin.Users.Any())
            {
                Log.Logger.Debug($"There are no users configured for access.");
            }
            if(!Settings.IdPAdmin.Users.Any(u => u == username))
            {
                Log.Logger.Debug($"User {username} not configured for access.");
                context.Result = new ForbidResult();
            } else
            {
                Log.Logger.Debug($"Users {username} is configured for access.");
            }
        }        
    }

}
