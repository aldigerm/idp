:: Remove folders bin, obj and packages
@echo off
SETLOCAL EnableDelayedExpansion
@echo Deleting all BIN, OBJ and PACKAGES folders
set searchVal=\IDSign.IdP.MultiModule.RemoteRepository.Web\
set searchVal2=\Timepiece.Verification\
for /d /r . %%d in (bin,obj) do (
	set path=%%d
	if exist "!path!" ( 
		if "!path:%searchVal%=!"=="!path!" ( 
			if "!path:%searchVal2%=!"=="!path!" ( 
				echo deleteing "!path!"
				del /S /Q "!path!\*.*" >nul
				rmdir /S /Q "!path!" >nul
			) 
		)
	)
)
@echo BIN and OBJ folders successfully deleted. Close the window.
pause > nul