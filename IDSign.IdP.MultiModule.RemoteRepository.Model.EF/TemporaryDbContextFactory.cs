﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF
{
    public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<RemoteRepositoryDbContext>
    { 
        public RemoteRepositoryDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<RemoteRepositoryDbContext>();
            builder.UseSqlServer("Server=localhost;Database=IDSign.IdP.RemoteRepository;User ID=sa_idsign_idp_users;Password=sa_idsign_idp_users;MultipleActiveResultSets=true",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(RemoteRepositoryDbContext).GetTypeInfo().Assembly.GetName().Name));
            return new RemoteRepositoryDbContext(builder.Options);
        }
    }
}
