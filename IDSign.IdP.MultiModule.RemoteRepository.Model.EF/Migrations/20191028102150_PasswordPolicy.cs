﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class PasswordPolicy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PasswordPolicyType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordPolicyType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PasswordPolicy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 128, nullable: false),
                    Data = table.Column<string>(nullable: true),
                    DefaultErrorMessage = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PasswordPolicyTypeId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordPolicy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PasswordPolicy_PasswordPolicyType_PasswordPolicyTypeId",
                        column: x => x.PasswordPolicyTypeId,
                        principalTable: "PasswordPolicyType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PasswordPolicy_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PasswordPolicyUserGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserGroupId = table.Column<int>(nullable: false),
                    PasswordPolicyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordPolicyUserGroup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PasswordPolicyUserGroup_PasswordPolicy_PasswordPolicyId",
                        column: x => x.PasswordPolicyId,
                        principalTable: "PasswordPolicy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PasswordPolicyUserGroup_UserGroup_UserGroupId",
                        column: x => x.UserGroupId,
                        principalTable: "UserGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PasswordPolicy_PasswordPolicyTypeId",
                table: "PasswordPolicy",
                column: "PasswordPolicyTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordPolicy_TenantId",
                table: "PasswordPolicy",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordPolicyType_Code",
                table: "PasswordPolicyType",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PasswordPolicyUserGroup_UserGroupId",
                table: "PasswordPolicyUserGroup",
                column: "UserGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_PasswordPolicyUserGroup_PasswordPolicyId_UserGroupId",
                table: "PasswordPolicyUserGroup",
                columns: new[] { "PasswordPolicyId", "UserGroupId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PasswordPolicyUserGroup");

            migrationBuilder.DropTable(
                name: "PasswordPolicy");

            migrationBuilder.DropTable(
                name: "PasswordPolicyType");
        }
    }
}
