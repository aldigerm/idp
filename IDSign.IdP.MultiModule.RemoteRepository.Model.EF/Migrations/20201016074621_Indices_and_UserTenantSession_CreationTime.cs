﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class Indices_and_UserTenantSession_CreationTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserGroupClaimType_ProjectId",
                table: "UserGroupClaimType");

            migrationBuilder.DropIndex(
                name: "IX_UserClaimType_ProjectId",
                table: "UserClaimType");

            migrationBuilder.DropIndex(
                name: "IX_TenantClaimType_ProjectId",
                table: "TenantClaimType");

            migrationBuilder.DropIndex(
                name: "IX_RoleClaimType_ProjectId",
                table: "RoleClaimType");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "CreationDate",
                table: "UserTenantSession",
                type: "datetimeoffset",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupClaimType_ProjectId_Type",
                table: "UserGroupClaimType",
                columns: new[] { "ProjectId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserClaimType_ProjectId_Type",
                table: "UserClaimType",
                columns: new[] { "ProjectId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TenantClaimType_ProjectId_Type",
                table: "TenantClaimType",
                columns: new[] { "ProjectId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaimType_ProjectId_Type",
                table: "RoleClaimType",
                columns: new[] { "ProjectId", "Type" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserGroupClaimType_ProjectId_Type",
                table: "UserGroupClaimType");

            migrationBuilder.DropIndex(
                name: "IX_UserClaimType_ProjectId_Type",
                table: "UserClaimType");

            migrationBuilder.DropIndex(
                name: "IX_TenantClaimType_ProjectId_Type",
                table: "TenantClaimType");

            migrationBuilder.DropIndex(
                name: "IX_RoleClaimType_ProjectId_Type",
                table: "RoleClaimType");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "UserTenantSession");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupClaimType_ProjectId",
                table: "UserGroupClaimType",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaimType_ProjectId",
                table: "UserClaimType",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantClaimType_ProjectId",
                table: "TenantClaimType",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaimType_ProjectId",
                table: "RoleClaimType",
                column: "ProjectId");
        }
    }
}
