﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class Module_ClaimValue_Removed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Module_Module_ModuleId",
                table: "Module");

            migrationBuilder.DropIndex(
                name: "IX_Module_ModuleId",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "ClaimValue",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "Group",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "Level",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "ModuleId",
                table: "Module");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClaimValue",
                table: "Module",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Group",
                table: "Module",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Level",
                table: "Module",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ModuleId",
                table: "Module",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Module_ModuleId",
                table: "Module",
                column: "ModuleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Module_ModuleId",
                table: "Module",
                column: "ModuleId",
                principalTable: "Module",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
