﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class User_ProfileImage_as_Object : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProfileImageId",
                table: "UserDetails",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserProfileImage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Data = table.Column<byte[]>(type: "varbinary(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfileImage", x => x.Id);
                });


            migrationBuilder.Sql(@"
    
INSERT INTO UserProfileImage (Data)
SELECT ud.ProfileImage
FROM dbo.[UserDetails] ud 
WHERE ud.ProfileImage is not null

");


            migrationBuilder.CreateIndex(
                name: "IX_UserDetails_ProfileImageId",
                table: "UserDetails",
                column: "ProfileImageId",
                unique: true,
                filter: "[ProfileImageId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_UserDetails_UserProfileImage_ProfileImageId",
                table: "UserDetails",
                column: "ProfileImageId",
                principalTable: "UserProfileImage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
UPDATE ud
    SET ud.ProfileImageId = upi.id
FROM dbo.[UserDetails] ud
INNER JOIN dbo.[UserProfileImage] upi ON upi.Data = ud.ProfileImage

");

            migrationBuilder.DropColumn(
                name: "ProfileImage",
                table: "UserDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<byte[]>(
                name: "ProfileImage",
                table: "UserDetails",
                type: "varbinary(max)",
                nullable: true);

            migrationBuilder.Sql(@"
    
UPDATE ud
    SET ud.ProfileImage = upi.Data
FROM [UserDetails] ud
INNER JOIN dbo.[UserProfileImage] upi ON upi.Id = ud.ProfileImageId

");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDetails_UserProfileImage_ProfileImageId",
                table: "UserDetails");

            migrationBuilder.DropTable(
                name: "UserProfileImage");

            migrationBuilder.DropIndex(
                name: "IX_UserDetails_ProfileImageId",
                table: "UserDetails");

            migrationBuilder.DropColumn(
                name: "ProfileImageId",
                table: "UserDetails");

        }
    }
}
