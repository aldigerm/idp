﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class Enforce_Unique_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserToUserGroup_ApplicationUserId",
                table: "UserToUserGroup");

            migrationBuilder.DropIndex(
                name: "IX_UserGroupRole_RoleId",
                table: "UserGroupRole");

            migrationBuilder.DropIndex(
                name: "IX_UserGroupInheritance_InheritsFromId",
                table: "UserGroupInheritance");

            migrationBuilder.DropIndex(
                name: "IX_UserClaim_UserId",
                table: "UserClaim");

            migrationBuilder.DropIndex(
                name: "IX_User_UserName_TenantId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_RoleModule_RoleId",
                table: "RoleModule");

            migrationBuilder.DropIndex(
                name: "IX_RoleInheritance_InheritsFromId",
                table: "RoleInheritance");

            migrationBuilder.CreateIndex(
                name: "IX_UserToUserGroup_ApplicationUserId_UserGroupId",
                table: "UserToUserGroup",
                columns: new[] { "ApplicationUserId", "UserGroupId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupRole_RoleId_UserGroupId",
                table: "UserGroupRole",
                columns: new[] { "RoleId", "UserGroupId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupInheritance_InheritsFromId_UserGroupId",
                table: "UserGroupInheritance",
                columns: new[] { "InheritsFromId", "UserGroupId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserGroup_Code_TenantId",
                table: "UserGroup",
                columns: new[] { "Code", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId_UserClaimTypeId_Value",
                table: "UserClaim",
                columns: new[] { "UserId", "UserClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_TenantUsername_TenantId",
                table: "User",
                columns: new[] { "TenantUsername", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleModule_RoleId_ModuleId",
                table: "RoleModule",
                columns: new[] { "RoleId", "ModuleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleInheritance_InheritsFromId_RoleId",
                table: "RoleInheritance",
                columns: new[] { "InheritsFromId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Role_Code_TenantId",
                table: "Role",
                columns: new[] { "Code", "TenantId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Module_Code_ProjectId",
                table: "Module",
                columns: new[] { "Code", "ProjectId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserToUserGroup_ApplicationUserId_UserGroupId",
                table: "UserToUserGroup");

            migrationBuilder.DropIndex(
                name: "IX_UserGroupRole_RoleId_UserGroupId",
                table: "UserGroupRole");

            migrationBuilder.DropIndex(
                name: "IX_UserGroupInheritance_InheritsFromId_UserGroupId",
                table: "UserGroupInheritance");

            migrationBuilder.DropIndex(
                name: "IX_UserGroup_Code_TenantId",
                table: "UserGroup");

            migrationBuilder.DropIndex(
                name: "IX_UserClaim_UserId_UserClaimTypeId_Value",
                table: "UserClaim");

            migrationBuilder.DropIndex(
                name: "IX_User_TenantUsername_TenantId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_RoleModule_RoleId_ModuleId",
                table: "RoleModule");

            migrationBuilder.DropIndex(
                name: "IX_RoleInheritance_InheritsFromId_RoleId",
                table: "RoleInheritance");

            migrationBuilder.DropIndex(
                name: "IX_Role_Code_TenantId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Module_Code_ProjectId",
                table: "Module");

            migrationBuilder.CreateIndex(
                name: "IX_UserToUserGroup_ApplicationUserId",
                table: "UserToUserGroup",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupRole_RoleId",
                table: "UserGroupRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupInheritance_InheritsFromId",
                table: "UserGroupInheritance",
                column: "InheritsFromId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId",
                table: "UserClaim",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserName_TenantId",
                table: "User",
                columns: new[] { "UserName", "TenantId" });

            migrationBuilder.CreateIndex(
                name: "IX_RoleModule_RoleId",
                table: "RoleModule",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleInheritance_InheritsFromId",
                table: "RoleInheritance",
                column: "InheritsFromId");
        }
    }
}
