﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class TenantClaims : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TenantClaimType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(maxLength: 32, nullable: false),
                    Description = table.Column<string>(maxLength: 128, nullable: false),
                    ProjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantClaimType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantClaimType_Project_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TenantClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(maxLength: 128, nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    TenantClaimTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantClaim_TenantClaimType_TenantClaimTypeId",
                        column: x => x.TenantClaimTypeId,
                        principalTable: "TenantClaimType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TenantClaim_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TenantClaim_TenantClaimTypeId",
                table: "TenantClaim",
                column: "TenantClaimTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantClaim_TenantId_TenantClaimTypeId_Value",
                table: "TenantClaim",
                columns: new[] { "TenantId", "TenantClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TenantClaimType_ProjectId",
                table: "TenantClaimType",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TenantClaim");

            migrationBuilder.DropTable(
                name: "TenantClaimType");
        }
    }
}
