﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class UserTenant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "UserTenantId",
                table: "UserClaim",
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "UserTenant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTenant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTenant_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserTenant_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
INSERT INTO UserTenant (UserId,TenantId,UserName)
SELECT u.Id, t.Id, u.UserName
FROM dbo.[User] u
INNER JOIN dbo.Tenant t ON t.Id = u.TenantId
INNER JOIN dbo.Project p ON p.Id = t.ProjectId
");

            migrationBuilder.Sql(@"
UPDATE uc
set uc.UserTenantId = ut.Id
FROM UserClaim uc 
INNER JOIN dbo.[User] u on uc.UserId = u.Id
INNER JOIN dbo.UserTenant ut on ut.UserId = u.Id
");

            migrationBuilder.AlterColumn<int>(
                name: "UserTenantId",
                table: "UserClaim",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.DropForeignKey(
                name: "FK_User_Tenant_TenantId",
                table: "User");

            migrationBuilder.DropForeignKey(
                name: "FK_UserClaim_User_UserId",
                table: "UserClaim");

            migrationBuilder.DropForeignKey(
                name: "FK_UserToUserGroup_User_ApplicationUserId",
                table: "UserToUserGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_UserToUserGroupManagement_User_UserId",
                table: "UserToUserGroupManagement");

            migrationBuilder.DropIndex(
                name: "IX_UserClaim_UserId_UserClaimTypeId_Value",
                table: "UserClaim");

            migrationBuilder.DropIndex(
                name: "IX_User_TenantId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_TenantUsername_TenantId",
                table: "User");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UserClaim");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "User");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "User");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserToUserGroupManagement",
                newName: "UserTenantId");

            migrationBuilder.Sql(@"

UPDATE uugm
SET uugm.UserTenantId = ut.Id
FROM UserToUserGroupManagement uugm
INNER JOIN UserTenant ut ON ut.UserId = uugm.UserTenantId

");

            migrationBuilder.RenameIndex(
                name: "IX_UserToUserGroupManagement_UserId_UserGroupId",
                table: "UserToUserGroupManagement",
                newName: "IX_UserToUserGroupManagement_UserTenantId_UserGroupId");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserId",
                table: "UserToUserGroup",
                newName: "UserTenantId");

            migrationBuilder.Sql(@"
UPDATE utug
set utug.UserTenantId = (SELECT TOP 1 ut.id FROM [UserTenant] ut WHERE ut.UserId = utug.UserTenantId) 
FROM [UserToUserGroup] utug
");
            migrationBuilder.RenameIndex(
                name: "IX_UserToUserGroup_ApplicationUserId_UserGroupId",
                table: "UserToUserGroup",
                newName: "IX_UserToUserGroup_UserTenantId_UserGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserTenantId_UserClaimTypeId_Value",
                table: "UserClaim",
                columns: new[] { "UserTenantId", "UserClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserTenant_UserId",
                table: "UserTenant",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenant_TenantId_UserId",
                table: "UserTenant",
                columns: new[] { "TenantId", "UserId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserClaim_UserTenant_UserTenantId",
                table: "UserClaim",
                column: "UserTenantId",
                principalTable: "UserTenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserToUserGroup_UserTenant_UserTenantId",
                table: "UserToUserGroup",
                column: "UserTenantId",
                principalTable: "UserTenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserToUserGroupManagement_UserTenant_UserTenantId",
                table: "UserToUserGroupManagement",
                column: "UserTenantId",
                principalTable: "UserTenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "UserClaim",
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "User",
                defaultValue: 0);


            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "User",
                maxLength: 64,
                nullable: true);

            migrationBuilder.Sql(@"

UPDATE u
set u.TenantId = ut.TenantId, u.UserName = ut.UserName
FROM [User] u
INNER JOIN dbo.UserTenant ut on ut.UserId = u.Id

UPDATE uc
set uc.UserId = ut.UserId
FROM [UserClaim] uc
INNER JOIN dbo.UserTenant ut on ut.Id = uc.UserTenantId

");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "UserClaim",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.DropForeignKey(
                name: "FK_UserClaim_UserTenant_UserTenantId",
                table: "UserClaim");

            migrationBuilder.DropForeignKey(
                name: "FK_UserToUserGroup_UserTenant_UserTenantId",
                table: "UserToUserGroup");

            migrationBuilder.DropForeignKey(
                name: "FK_UserToUserGroupManagement_UserTenant_UserTenantId",
                table: "UserToUserGroupManagement");

            migrationBuilder.DropTable(
                name: "UserTenant");

            migrationBuilder.DropIndex(
                name: "IX_UserClaim_UserTenantId_UserClaimTypeId_Value",
                table: "UserClaim");

            migrationBuilder.DropColumn(
                name: "UserTenantId",
                table: "UserClaim");

            migrationBuilder.Sql(@"
UPDATE utug
set utug.UserId = (SELECT TOP 1 ut.UserId FROM [UserTenant] ut WHERE ut.UserId = utug.UserTenantId) 
FROM [UserToUserGroup] utug
");
            migrationBuilder.RenameColumn(
                name: "UserTenantId",
                table: "UserToUserGroupManagement",
                newName: "UserId");



            migrationBuilder.RenameIndex(
                name: "IX_UserToUserGroupManagement_UserTenantId_UserGroupId",
                table: "UserToUserGroupManagement",
                newName: "IX_UserToUserGroupManagement_UserId_UserGroupId");

            migrationBuilder.RenameColumn(
                name: "UserTenantId",
                table: "UserToUserGroup",
                newName: "ApplicationUserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserToUserGroup_UserTenantId_UserGroupId",
                table: "UserToUserGroup",
                newName: "IX_UserToUserGroup_ApplicationUserId_UserGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId_UserClaimTypeId_Value",
                table: "UserClaim",
                columns: new[] { "UserId", "UserClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_TenantId",
                table: "User",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_User_TenantUsername_TenantId",
                table: "User",
                columns: new[] { "TenantUsername", "TenantId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Tenant_TenantId",
                table: "User",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserClaim_User_UserId",
                table: "UserClaim",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserToUserGroup_User_ApplicationUserId",
                table: "UserToUserGroup",
                column: "ApplicationUserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserToUserGroupManagement_User_UserId",
                table: "UserToUserGroupManagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
