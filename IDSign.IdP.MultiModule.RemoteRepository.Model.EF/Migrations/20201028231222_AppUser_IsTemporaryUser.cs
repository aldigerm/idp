﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class AppUser_IsTemporaryUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsTemporaryUser",
                table: "User",
                nullable: false,
                defaultValue: false);

            migrationBuilder.Sql(@"
update [dbo].[User]
set IsTemporaryUser = 1
where TenantUsername like 'vaiie_%'

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsTemporaryUser",
                table: "User");
        }
    }
}
