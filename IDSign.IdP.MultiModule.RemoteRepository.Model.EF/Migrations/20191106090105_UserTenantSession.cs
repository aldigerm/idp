﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class UserTenantSession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserTenantSessionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTenantSessionType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserTenantSession",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Identifier = table.Column<string>(maxLength: 1028, nullable: false),
                    Data = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    UserTenantId = table.Column<int>(nullable: false),
                    UserTenantSessionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTenantSession", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserTenantSession_UserTenant_UserTenantId",
                        column: x => x.UserTenantId,
                        principalTable: "UserTenant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserTenantSession_UserTenantSessionType_UserTenantSessionTypeId",
                        column: x => x.UserTenantSessionTypeId,
                        principalTable: "UserTenantSessionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantSession_UserTenantId",
                table: "UserTenantSession",
                column: "UserTenantId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantSession_UserTenantSessionTypeId",
                table: "UserTenantSession",
                column: "UserTenantSessionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantSessionType_Code",
                table: "UserTenantSessionType",
                column: "Code",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserTenantSession");

            migrationBuilder.DropTable(
                name: "UserTenantSessionType");
        }
    }
}
