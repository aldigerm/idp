﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class UserGroup_ClaimValue_Removed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoleModule_Tenant_TenantId",
                table: "RoleModule");

            migrationBuilder.DropIndex(
                name: "IX_RoleModule_TenantId",
                table: "RoleModule");

            migrationBuilder.DropColumn(
                name: "ClaimValue",
                table: "UserGroup");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "RoleModule");

            migrationBuilder.AlterColumn<bool>(
                name: "Enabled",
                table: "UserGroup",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Enabled",
                table: "UserGroup",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<string>(
                name: "ClaimValue",
                table: "UserGroup",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "RoleModule",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleModule_TenantId",
                table: "RoleModule",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoleModule_Tenant_TenantId",
                table: "RoleModule",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
