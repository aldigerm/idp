﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class RoleModule_Tenant_Removed_and_Added_Enabled : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "UserGroup",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "Tenant",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "RoleModule",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "Role",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "Project",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "UserGroup");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "Tenant");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "Project");

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "RoleModule",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
