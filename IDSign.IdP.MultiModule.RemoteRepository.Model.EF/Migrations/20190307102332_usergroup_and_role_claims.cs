﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class usergroup_and_role_claims : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RoleClaimType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(maxLength: 32, nullable: false),
                    Description = table.Column<string>(maxLength: 128, nullable: false),
                    ProjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaimType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaimType_Project_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserGroupClaimType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(maxLength: 32, nullable: false),
                    Description = table.Column<string>(maxLength: 128, nullable: false),
                    ProjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroupClaimType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserGroupClaimType_Project_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Project",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(maxLength: 128, nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    RoleClaimTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaim_RoleClaimType_RoleClaimTypeId",
                        column: x => x.RoleClaimTypeId,
                        principalTable: "RoleClaimType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RoleClaim_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserGroupClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(maxLength: 128, nullable: false),
                    UserGroupId = table.Column<int>(nullable: false),
                    UserGroupClaimTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroupClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserGroupClaim_UserGroupClaimType_UserGroupClaimTypeId",
                        column: x => x.UserGroupClaimTypeId,
                        principalTable: "UserGroupClaimType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserGroupClaim_UserGroup_UserGroupId",
                        column: x => x.UserGroupId,
                        principalTable: "UserGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaim_RoleClaimTypeId",
                table: "RoleClaim",
                column: "RoleClaimTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaim_RoleId_RoleClaimTypeId_Value",
                table: "RoleClaim",
                columns: new[] { "RoleId", "RoleClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaimType_ProjectId",
                table: "RoleClaimType",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupClaim_UserGroupClaimTypeId",
                table: "UserGroupClaim",
                column: "UserGroupClaimTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupClaim_UserGroupId_UserGroupClaimTypeId_Value",
                table: "UserGroupClaim",
                columns: new[] { "UserGroupId", "UserGroupClaimTypeId", "Value" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserGroupClaimType_ProjectId",
                table: "UserGroupClaimType",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoleClaim");

            migrationBuilder.DropTable(
                name: "UserGroupClaim");

            migrationBuilder.DropTable(
                name: "RoleClaimType");

            migrationBuilder.DropTable(
                name: "UserGroupClaimType");
        }
    }
}
