﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Migrations
{
    public partial class Module_Per_Project : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "Module",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Module_ProjectId",
                table: "Module",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Module_Project_ProjectId",
                table: "Module",
                column: "ProjectId",
                principalTable: "Project",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Module_Project_ProjectId",
                table: "Module");

            migrationBuilder.DropIndex(
                name: "IX_Module_ProjectId",
                table: "Module");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "Module");
        }
    }
}
