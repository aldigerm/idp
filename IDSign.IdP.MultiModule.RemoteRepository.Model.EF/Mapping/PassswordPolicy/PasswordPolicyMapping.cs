﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class PasswordPolicyMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<PasswordPolicy>());
        }
        private void Map(EntityTypeBuilder<PasswordPolicy> builder)
        {
            builder.ToTable("PasswordPolicy");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Code).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Data);
            builder.Property(x => x.DefaultErrorMessage);

            // one type
            builder.HasOne(m => m.PasswordPolicyType).WithMany(c => c.PasswordPolicies).HasForeignKey(m => m.PasswordPolicyTypeId);
            builder.HasOne(m => m.Tenant).WithMany(c => c.PasswordPolicies).HasForeignKey(m => m.TenantId);

        }
    }
}
