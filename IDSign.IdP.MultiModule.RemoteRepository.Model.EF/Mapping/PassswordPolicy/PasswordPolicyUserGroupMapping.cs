﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class PasswordPolicyUserGroupMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<PasswordPolicyUserGroup>());
        }
        private void Map(EntityTypeBuilder<PasswordPolicyUserGroup> builder)
        {
            builder.ToTable("PasswordPolicyUserGroup");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            // has many user groups with many password policies
            builder.HasOne(ugpp => ugpp.UserGroup).WithMany(ug => ug.PasswordPolicyUserGroup).HasForeignKey(ugpp => ugpp.UserGroupId);
            builder.HasOne(ugpp => ugpp.PasswordPolicy).WithMany(pp => pp.PasswordPolicyUserGroup).HasForeignKey(ugpp => ugpp.PasswordPolicyId);

            // unique passwor policy per group
            builder.HasIndex(x => new { x.PasswordPolicyId, x.UserGroupId }).IsUnique();
        }
    }
}
