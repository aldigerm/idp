﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserToUserGroupManagementMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserToUserGroupManagement>());
        }
        private void Map(EntityTypeBuilder<UserToUserGroupManagement> builder)
        {
            builder.ToTable("UserToUserGroupManagement");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            // has many user groups with many roles
            builder.HasOne(n => n.UserGroup).WithMany(inh => inh.UserGroupManagement).HasForeignKey(inh => inh.UserGroupId);
            builder.HasOne(n => n.UserTenant).WithMany(inh => inh.UserGroupManagement).HasForeignKey(inh => inh.UserTenantId);

            // unique user per group
            builder.HasIndex(x => new { x.UserTenantId, x.UserGroupId }).IsUnique();
        }
    }
}
