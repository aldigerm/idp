﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public interface IEntityConfig
    {
        void MapEntity(ModelBuilder builder);
    }
}
