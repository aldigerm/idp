﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserGroupRoleMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserGroupRole>());
        }
        private void Map(EntityTypeBuilder<UserGroupRole> builder)
        {
            builder.ToTable("UserGroupRole");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            // has many user groups with many roles
            builder.HasOne(n => n.UserGroup).WithMany(inh => inh.UserGroupRoles).HasForeignKey(inh => inh.UserGroupId);
            builder.HasOne(n => n.Role).WithMany(inh => inh.UserGroupsRoles).HasForeignKey(inh => inh.RoleId);

            // unique group per role
            builder.HasIndex(x => new { x.RoleId, x.UserGroupId }).IsUnique();
        }
    }
}
