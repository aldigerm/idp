﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class RoleMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<Role>());
        }
        private void Map(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Role");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Code).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Description).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Enabled).IsRequired().HasDefaultValue(true);
            
            // unique role per tenant
            builder.HasIndex(x => new { x.Code, x.TenantId }).IsUnique();
        }
    }
}
