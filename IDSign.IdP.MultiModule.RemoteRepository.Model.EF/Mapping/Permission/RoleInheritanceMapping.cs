﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class RoleInheritanceMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<RoleInheritance>());
        }
        private void Map(EntityTypeBuilder<RoleInheritance> builder)
        {
            builder.ToTable("RoleInheritance");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            // many-many RELATIONSHIPS
            builder.HasOne(inh => inh.InheritsFrom).WithMany(n => n.RolesWhichInheritFromCurrent).HasForeignKey(inh => inh.InheritsFromId);
            builder.HasOne(inh => inh.Role).WithMany(u => u.InheritingFromTheseRoles).HasForeignKey(inh => inh.RoleId);

            // unique role inheritance
            builder.HasIndex(x => new { x.InheritsFromId, x.RoleId }).IsUnique();
        }
    }
}
