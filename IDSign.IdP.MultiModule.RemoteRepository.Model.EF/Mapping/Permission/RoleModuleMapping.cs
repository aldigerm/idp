﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class RoleModuleMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<RoleModule>());
        }
        private void Map(EntityTypeBuilder<RoleModule> builder)
        {
            builder.ToTable("RoleModule");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.HasOne(m => m.Role).WithMany(m => m.RoleModules).HasForeignKey(m => m.RoleId);
            builder.HasOne(m => m.Module).WithMany(m => m.RoleModules).HasForeignKey(m => m.ModuleId);
            
            // unique module per role
            builder.HasIndex(x => new { x.RoleId, x.ModuleId }).IsUnique();
        }
    }
}
