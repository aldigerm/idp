﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class ModuleMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<Module>());
        }
        private void Map(EntityTypeBuilder<Module> builder)
        {
            builder.ToTable("Module");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Description).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Code).IsRequired().HasMaxLength(128);

            // has a project
            builder.HasOne(x => x.Project).WithMany(p => p.Modules).HasForeignKey(x => x.ProjectId);

            // unique Code per Project
            builder.HasIndex(x => new { x.Code, x.ProjectId }).IsUnique();

        }
    }
}
