﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserGroupInheritanceMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<GroupInheritance>());
        }
        private void Map(EntityTypeBuilder<GroupInheritance> builder)
        {
            builder.ToTable("UserGroupInheritance");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            // many-many RELATIONSHIPS
            builder.HasOne(inh => inh.InheritsFrom).WithMany(n => n.UserGroupsWhichInheritFromCurrent).HasForeignKey(inh => inh.InheritsFromId);
            builder.HasOne(inh => inh.UserGroup).WithMany(u => u.InheritingFromTheseUserGroups).HasForeignKey(inh => inh.UserGroupId);


            // unique inheritance
            builder.HasIndex(x => new { x.InheritsFromId, x.UserGroupId }).IsUnique();
        }
    }
}
