﻿using IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class RoleModuleTemplateMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<RoleModuleTemplate>());
        }
        private void Map(EntityTypeBuilder<RoleModuleTemplate> builder)
        {
            builder.ToTable("RoleModuleTemplate");
            builder.HasKey(x => new { x.RoleName, x.ModuleCode });

            builder.Property(x => x.RoleName).IsRequired().HasMaxLength(128);
            builder.Property(x => x.ModuleCode).HasMaxLength(128).IsRequired();
        }
    }
}
