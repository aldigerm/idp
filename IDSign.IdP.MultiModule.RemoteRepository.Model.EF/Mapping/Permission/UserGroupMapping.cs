﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserGroupMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserGroup>());
        }
        private void Map(EntityTypeBuilder<UserGroup> builder)
        {
            builder.ToTable("UserGroup");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Description).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Code).IsRequired().HasMaxLength(128);

            // company specific
            builder.HasOne(m => m.Tenant).WithMany(c => c.UserGroups).HasForeignKey(m => m.TenantId);

            // unique group code per tenant
            builder.HasIndex(x => new { x.Code, x.TenantId }).IsUnique();
        }
    }
}
