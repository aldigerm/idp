﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class TenantMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<Tenant>());
        }
        private void Map(EntityTypeBuilder<Tenant> builder)
        {
            builder.ToTable("Tenant");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd(); ;
            builder.Property(x => x.Name).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Code).IsRequired().HasMaxLength(32);
            builder.Property(x => x.Enabled).IsRequired().HasDefaultValue(true);

            // one tenant per project
            builder.HasIndex(x => new { x.Code, x.ProjectId }).IsUnique();

            // has a project
            builder.HasOne(x => x.Project).WithMany(p => p.Tenants).HasForeignKey(x => x.ProjectId);

            // many roles
            builder.HasMany(x => x.Roles).WithOne(u => u.Tenant).HasForeignKey(u => u.TenantId);
        }
    }
}
