﻿using IDSign.IdP.MultiModule.RemoteRepository.Model;
using IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class ProjectMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<Project>());
        }
        private void Map(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Project");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Code).IsRequired().HasMaxLength(32);
            builder.Property(x => x.Enabled).IsRequired().HasDefaultValue(true);

            // index
            builder.HasIndex(x => x.Code).IsUnique();            
        }
    }
}
