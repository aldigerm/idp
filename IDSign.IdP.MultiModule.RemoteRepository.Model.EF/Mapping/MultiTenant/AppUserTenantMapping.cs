﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class AppUserTenantMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserTenant>());
        }
        private void Map(EntityTypeBuilder<UserTenant> builder)
        {
            builder.ToTable("UserTenant");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.UserName).HasMaxLength(128);

            // many-many RELATIONSHIPS
            builder.HasOne(ut => ut.Tenant).WithMany(t => t.UserTenants).HasForeignKey(ut => ut.TenantId);
            builder.HasOne(ut => ut.User).WithMany(u => u.UserTenants).HasForeignKey(ut => ut.UserId);

            // unique role inheritance
            builder.HasIndex(x => new { x.TenantId, x.UserId }).IsUnique();
        }
    }
}
