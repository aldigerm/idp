﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserTenantSessionMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserTenantSession>());
        }
        private void Map(EntityTypeBuilder<UserTenantSession> builder)
        {
            builder.ToTable("UserTenantSession");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Identifier).IsRequired().HasMaxLength(1028);
            builder.Property(x => x.Data);
            builder.Property(x => x.ExpiryDate).HasColumnType("datetimeoffset");
            builder.Property(x => x.CreationDate).HasColumnType("datetimeoffset");

            // one user tenant
            builder.HasOne(us => us.UserTenant).WithMany(u => u.UserTenantSessions).HasForeignKey(us => us.UserTenantId);

            // one type
            builder.HasOne(us => us.UserTenantSessionType).WithMany(ust => ust.UserTenantSessions).HasForeignKey(us => us.UserTenantSessionTypeId);

        }
    }
}
