﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserTenantSessionTypeMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserTenantSessionType>());
        }
        private void Map(EntityTypeBuilder<UserTenantSessionType> builder)
        {
            builder.ToTable("UserTenantSessionType");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Code).IsRequired().HasMaxLength(128);
            builder.Property(x => x.Description);

            // type is unique
            builder.HasIndex(x => new { x.Code }).IsUnique();
        }
    }
}
