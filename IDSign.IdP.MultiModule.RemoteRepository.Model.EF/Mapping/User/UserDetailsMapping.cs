﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserDetailsMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserDetails>());
        }
        private void Map(EntityTypeBuilder<UserDetails> builder)
        {
            builder.ToTable("UserDetails");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.CustomAttributes).HasColumnType("xml");
            builder.Property(x => x.Title).HasMaxLength(100);
            builder.Property(x => x.FirstName).HasMaxLength(100);
            builder.Property(x => x.MiddleName).HasMaxLength(100);
            builder.Property(x => x.LastName).HasMaxLength(100);
            builder.Property(x => x.DateOfBirth).HasColumnType("datetimeoffset");
            builder.Property(x => x.Occupation).HasMaxLength(200);
            builder.Property(x => x.CityOfBirth).HasMaxLength(100);
            builder.Property(x => x.NINS).HasMaxLength(100);
            builder.Property(x => x.EmailAddress).HasMaxLength(200);
            builder.Property(x => x.MobileNumber).HasMaxLength(32);

            // has an optional user details
            builder.HasOne(u => u.ProfileImage).WithOne(ud => ud.UserDetails).HasForeignKey<UserDetails>(u => u.ProfileImageId);
        }
    }
}
