﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserProfileImageMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserProfileImage>());
        }
        private void Map(EntityTypeBuilder<UserProfileImage> builder)
        {
            builder.ToTable("UserProfileImage");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Data).HasColumnType("varbinary(max)").IsRequired();

        }
    }
}
