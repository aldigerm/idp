﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class PreviousPasswordsMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserPreviousPassword>());
        }
        private void Map(EntityTypeBuilder<UserPreviousPassword> builder)
        {
            builder.ToTable("UserPreviousPassword");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.CreateDate).IsRequired().HasColumnType("datetimeoffset");
            builder.Property(x => x.PasswordHash).IsRequired();

            // has an optional user details
            builder.HasOne(pp => pp.User).WithMany(u => u.PreviousPasswords).HasForeignKey(u => u.UserId);            
        }
    }
}
