﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class AppUserMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<AppUser>());
        }
        private void Map(EntityTypeBuilder<AppUser> builder)
        {
            builder.ToTable("User");
            builder.Property(x => x.AccessFailedCount).IsRequired().HasColumnType("int");
            builder.Property(x => x.Email).HasMaxLength(256);
            builder.Property(x => x.EmailConfirmed).IsRequired();
            builder.Property(x => x.TenantUsername).IsRequired().HasMaxLength(32);
            builder.Property(x => x.LockoutEnabled).IsRequired();
            builder.Property(x => x.LockoutEndDateUtc).HasColumnType("datetimeoffset");
            builder.Property(x => x.PasswordHash).IsRequired().HasMaxLength(512);
            builder.Property(x => x.PhoneNumber).HasMaxLength(16);
            builder.Property(x => x.PhoneNumberConfirmed).IsRequired();
            builder.Property(x => x.TwoFactorEnabled).IsRequired();
            builder.Property(x => x.NewPasswordRequired).IsRequired();
            builder.Property(x => x.IsTemporaryUser).IsRequired().HasDefaultValue(false);

            // has an optional user details
            builder.HasOne(u => u.UserDetails).WithOne(ud => ud.ApplicationUser).HasForeignKey<AppUser>(u => u.UserDetailsId);            
        }
    }
}
