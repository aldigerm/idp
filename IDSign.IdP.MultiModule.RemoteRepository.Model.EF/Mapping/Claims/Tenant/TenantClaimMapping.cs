﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class TenantClaimMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<TenantClaim>());
        }
        private void Map(EntityTypeBuilder<TenantClaim> builder)
        {
            builder.ToTable("TenantClaim");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Value).IsRequired().HasMaxLength(128);
            builder.Property(x => x.TenantId).IsRequired();
            builder.Property(x => x.TenantClaimTypeId).IsRequired();

            // has a user
            builder.HasOne(x => x.Tenant).WithMany(p => p.TenantClaims).HasForeignKey(x => x.TenantId);

            // has a project
            builder.HasOne(x => x.TenantClaimType).WithMany(p => p.TenantClaims).HasForeignKey(x => x.TenantClaimTypeId);

            // unique claim value of the same type per user
            builder.HasIndex(x => new { x.TenantId, x.TenantClaimTypeId, x.Value }).IsUnique();
        }
    }
}
