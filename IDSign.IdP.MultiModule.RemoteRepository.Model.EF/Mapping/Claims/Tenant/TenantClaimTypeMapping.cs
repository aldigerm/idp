﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class TenantClaimTypeMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<TenantClaimType>());
        }
        private void Map(EntityTypeBuilder<TenantClaimType> builder)
        {
            builder.ToTable("TenantClaimType");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Type).IsRequired().HasMaxLength(32);
            builder.Property(x => x.Description).IsRequired().HasMaxLength(128);
            builder.Property(x => x.ProjectId).IsRequired();

            // has a project
            builder.HasOne(x => x.Project).WithMany(p => p.TenantClaimTypes).HasForeignKey(x => x.ProjectId);

            builder.HasIndex(x => new { x.ProjectId, x.Type }).IsUnique();
        }
    }
}
