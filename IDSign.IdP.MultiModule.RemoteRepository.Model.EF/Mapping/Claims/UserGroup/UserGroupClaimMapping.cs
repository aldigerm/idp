﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserGroupClaimMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserGroupClaim>());
        }
        private void Map(EntityTypeBuilder<UserGroupClaim> builder)
        {
            builder.ToTable("UserGroupClaim");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Value).IsRequired().HasMaxLength(128);
            builder.Property(x => x.UserGroupId).IsRequired();
            builder.Property(x => x.UserGroupClaimTypeId).IsRequired();

            // has a user
            builder.HasOne(x => x.UserGroup).WithMany(p => p.UserGroupClaims).HasForeignKey(x => x.UserGroupId);

            // has a project
            builder.HasOne(x => x.UserGroupClaimType).WithMany(p => p.UserGroupClaims).HasForeignKey(x => x.UserGroupClaimTypeId);

            // unique claim value of the same type per user
            builder.HasIndex(x => new { x.UserGroupId, x.UserGroupClaimTypeId, x.Value }).IsUnique();
        }
    }
}
