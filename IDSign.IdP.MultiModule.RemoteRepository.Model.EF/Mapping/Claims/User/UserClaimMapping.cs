﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class UserClaimMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<UserClaim>());
        }
        private void Map(EntityTypeBuilder<UserClaim> builder)
        {
            builder.ToTable("UserClaim");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Value).IsRequired().HasMaxLength(128);
            builder.Property(x => x.UserClaimTypeId).IsRequired();

            // has a user
            builder.HasOne(x => x.UserTenant).WithMany(p => p.UserClaims).HasForeignKey(x => x.UserTenantId);

            // has a project
            builder.HasOne(x => x.UserClaimType).WithMany(p => p.UserClaims).HasForeignKey(x => x.UserClaimTypeId);

            // unique claim value of the same type per user
            builder.HasIndex(x => new { x.UserTenantId, x.UserClaimTypeId, x.Value }).IsUnique();
        }
    }
}
