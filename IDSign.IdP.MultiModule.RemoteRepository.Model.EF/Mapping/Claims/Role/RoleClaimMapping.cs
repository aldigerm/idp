﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping
{
    public class RoleClaimMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<RoleClaim>());
        }
        private void Map(EntityTypeBuilder<RoleClaim> builder)
        {
            builder.ToTable("RoleClaim");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Value).IsRequired().HasMaxLength(128);
            builder.Property(x => x.RoleId).IsRequired();
            builder.Property(x => x.RoleClaimTypeId).IsRequired();

            // has a user
            builder.HasOne(x => x.Role).WithMany(p => p.RoleClaims).HasForeignKey(x => x.RoleId);

            // has a project
            builder.HasOne(x => x.RoleClaimType).WithMany(p => p.RoleClaims).HasForeignKey(x => x.RoleClaimTypeId);

            // unique claim value of the same type per user
            builder.HasIndex(x => new { x.RoleId, x.RoleClaimTypeId, x.Value }).IsUnique();
        }
    }
}
