﻿namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF
{
    using IDSign.IdP.MultiModule.RemoteRepository.Model;
    using IDSign.IdP.MultiModule.RemoteRepository.Model.EF.Mapping;
    using Microsoft.AspNetCore.Http;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;

    public class RemoteRepositoryDbContext : DbContext
    {
        public RemoteRepositoryDbContext(
            DbContextOptions<RemoteRepositoryDbContext> nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public DbSet<AppUser> Users { get; set; }
        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<UserProfileImage> UserProfileImages { get; set; }
        public DbSet<UserTenant> UserTenants { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<RemoteRepository.Model.Module> Modules { get; set; }
        public DbSet<RoleModule> RoleModules { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleModuleTemplate> RoleModuleTemplates { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<GroupInheritance> UserGroupInheritances { get; set; }
        public DbSet<RoleInheritance> RoleInheritances { get; set; }
        public DbSet<UserGroupRole> UserGroupRoles { get; set; }
        public DbSet<UserToUserGroup> ApplicationUserToUserGroups { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<UserClaimType> UserClaimTypes { get; set; }
        public DbSet<RoleClaim> RoleClaims { get; set; }
        public DbSet<RoleClaimType> RoleClaimTypes { get; set; }
        public DbSet<UserGroupClaim> UserGroupClaims { get; set; }
        public DbSet<UserGroupClaimType> UserGroupClaimTypes { get; set; }
        public DbSet<TenantClaim> TenantClaims { get; set; }
        public DbSet<TenantClaimType> TenantClaimTypes { get; set; }
        public DbSet<UserToUserGroupManagement> UserToUserGroupManagement { get; set; }
        public DbSet<PasswordPolicy> PasswordPolicies { get; set; }
        public DbSet<PasswordPolicyType> PasswordPolicyTypes { get; set; }
        public DbSet<PasswordPolicyUserGroup> PasswordPolicyUserGroup { get; set; }
        public DbSet<UserPreviousPassword> UserPreviousPasswords { get; set; }
        public DbSet<UserTenantSession> UserTenantSessions { get; set; }
        public DbSet<UserTenantSessionType> UserTenantSessionTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var entConfigType = typeof(IEntityConfig);

            // Init types
            Assembly.GetAssembly(entConfigType)
                    .GetTypes()
                    .Where(type => entConfigType.IsAssignableFrom(type) && type != entConfigType)
                    .ToList()
                    .ForEach(type => ((IEntityConfig)Activator.CreateInstance(type)).MapEntity(modelBuilder));

            // set all foreign keys to thrown an error if there is a violation
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }
    }
}