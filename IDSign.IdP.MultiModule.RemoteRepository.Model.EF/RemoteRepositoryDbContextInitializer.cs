﻿using IDSign.IdP.Model.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IDSign.IdP.MultiModule.RemoteRepository.Model.EF
{
    public class RemoteRepositoryDbContextInitializer
    {
        public static void InitializeDatabase(RemoteRepositoryDbContext context, ILogger<RemoteRepositoryDbContextInitializer> logger)
        {
            try
            {
                context.Database.Migrate();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                logger.LogError($"Database migration threw an error -> {e.Message}");
            }

            #region Password Policy Type

            context.PasswordPolicyTypes.UpsertRange(new PasswordPolicyType
            {
                Code = PasswordPolicyTypes.RegexMatch,
                Description = "Regex pattern match."
            }, new PasswordPolicyType
            {
                Code = PasswordPolicyTypes.MinimumLength,
                Description = "Minimum Length."
            }, new PasswordPolicyType
            {
                Code = PasswordPolicyTypes.MaximumLength,
                Description = "Maximum Length."
            }, new PasswordPolicyType
            {
                Code = PasswordPolicyTypes.History,
                Description = "Matching in History."
            })
            .On(v => v.Code)
            .Run();

            context.UserTenantSessionTypes.UpsertRange(new UserTenantSessionType
            {
                Code = UserTenantSessionTypes.ForceNewPassword,
                Description = "Force user to set a new password"
            }, new UserTenantSessionType
            {
                Code = UserTenantSessionTypes.ResetPassword,
                Description = "Reset password requested by user"
            }, new UserTenantSessionType
            {
                Code = UserTenantSessionTypes.AccessCode,
                Description = "Access code to log in as a user"
            }, new UserTenantSessionType
            {
                Code = UserTenantSessionTypes.OTP,
                Description = "OTP Value for Login Purposes"
            })
            .On(v => v.Code)
            .Run();

            #endregion

        }
    }
}
