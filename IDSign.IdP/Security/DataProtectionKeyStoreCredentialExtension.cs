﻿using IDSign.IdP.Model.Configuration;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace IdentityServerWithAspNetIdentity.Config
{
    /// <summary>
    /// Impl of adding a signin key for identity server 4,
    /// with an appsetting.json configuration look similar to:
    /// "SigninKeyCredentials": {
    ///     "KeyType": "KeyFile",
    ///     "KeyFilePath": "C:\\certificates\\idsv4.pfx",
    ///     "KeyStorePath": ""
    /// }
    /// </summary>
    public static class DataProtectionKeyStoreCredentialExtension
    {
        private const string KeyTypeKeyFile = "KeyFile";
        private const string KeyTypeKeyStore = "KeyStore";

        public static IDataProtectionBuilder ProtectKeysWithCertificateFromConfig(
            this IDataProtectionBuilder builder, IDSignIdpConfigurationSection options, ILoggerFactory _loggerFactory)
        {
            var configuration = options.DataProtectionCertificateConfiguration;
            var logger = _loggerFactory.CreateLogger("DataProtectionKeyStoreCredentialExtension");

            string keyType = configuration?.KeyType;
            logger.LogDebug($"DataProtectionKeyStoreCredentialExtension keyType is '{keyType}'");

            switch (keyType)
            {
                case CredentialHelper.KeyTypeTemporary:
                    throw new NotImplementedException($"Key Type not supported: {CredentialHelper.KeyTypeTemporary}");

                case CredentialHelper.KeyTypeKeyFile:
                    logger.LogDebug($"DataProtectionKeyStoreCredentialExtension adding from file");
                    AddCertificateFromFile(builder, configuration, logger);
                    break;

                case CredentialHelper.KeyTypeKeyStore:
                    logger.LogDebug($"DataProtectionKeyStoreCredentialExtension adding from KeyStore");
                    AddCertificateFromStore(builder, configuration, logger);
                    break;

                case CredentialHelper.KeyTypeAzureKeyVault:
                    logger.LogDebug($"DataProtectionKeyStoreCredentialExtension adding from Azure Key Vault");
                    AddCertificateFromAzureKeyVault(builder, configuration, logger);
                    break;

                default:
                    logger.LogDebug($"Key type '{keyType}' not recognised. DataProtectionKeyStoreCredentialExtension will work without certificate.");
                    break;
            }

            return builder;
        }

        private static void AddCertificateFromStore(IDataProtectionBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetCertificateFromStore(options?.StoreCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.ProtectKeysWithCertificate(certificate);
            }
            else
            {
                throw new Exception($"A matching valid key couldn't be found in the store for issuer name '{options?.StoreCertificateConfiguration?.KeyStoreIssuer}' in Personal LocalMachine");
            }
        }

        private static void AddCertificateFromFile(IDataProtectionBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetCertificateFromFile(options?.FileCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.ProtectKeysWithCertificate(certificate);
            }
            else
            {
                throw new Exception($"DataProtectionKeyStoreCredentialExtension cannot find key file {options?.FileCertificateConfiguration?.KeyFilePath}");
            }
        }

        private static void AddCertificateFromAzureKeyVault(IDataProtectionBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetFromAzure(options?.AzureCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.ProtectKeysWithCertificate(certificate);
            }
            else
            {
                throw new Exception($"DataProtectionKeyStoreCredentialExtension cannot find key file {options?.FileCertificateConfiguration?.KeyFilePath}");
            }
        }
    }
}