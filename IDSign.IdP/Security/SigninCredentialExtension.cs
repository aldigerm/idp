﻿using IDSign.IdP.Model.Configuration;
using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace IdentityServerWithAspNetIdentity.Config
{
    /// <summary>
    /// Impl of adding a signin key for identity server 4,
    /// with an appsetting.json configuration look similar to:
    /// "SigninKeyCredentials": {
    ///     "KeyType": "KeyFile",
    ///     "KeyFilePath": "C:\\certificates\\idsv4.pfx",
    ///     "KeyStorePath": ""
    /// }
    /// </summary>
    public static class SigninCredentialExtension
    {

        public static IIdentityServerBuilder AddSigninCredentialFromConfig(
            this IIdentityServerBuilder builder, IDSignIdpConfigurationSection options, ILoggerFactory _loggerFactory)
        {
            var configuration = options.CertificateConfiguration;
            var logger = _loggerFactory.CreateLogger("AddSigninCredentialFromConfig");

            string keyType = configuration?.KeyType;
            logger.LogDebug($"AddSigninCredentialFromConfig keyType is '{keyType}'");

            switch (keyType)
            {
                case CredentialHelper.KeyTypeTemporary:
                    logger.LogDebug($"SigninCredentialExtension adding Developer Signing Credential");
                    builder.AddDeveloperSigningCredential();
                    break;

                case CredentialHelper.KeyTypeKeyFile:
                    logger.LogDebug($"SigninCredentialExtension adding from file");
                    AddCertificateFromFile(builder, configuration, logger);
                    break;

                case CredentialHelper.KeyTypeKeyStore:
                    logger.LogDebug($"SigninCredentialExtension adding from KeyStore");
                    AddCertificateFromStore(builder, configuration, logger);
                    break;

                case CredentialHelper.KeyTypeAzureKeyVault:
                    logger.LogDebug($"SigninCredentialExtension adding from Azure Key Vault");
                    AddCertificateFromAzureKeyVault(builder, configuration, logger);
                    break;

                default:
                    logger.LogDebug($"Key type '{keyType}' not recognised. Adding Developer Signing Credential");
                    builder.AddDeveloperSigningCredential();
                    break;
            }

            return builder;
        }

        private static void AddCertificateFromStore(IIdentityServerBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetCertificateFromStore(options?.StoreCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.AddSigningCredential(certificate);
            }
            else
            {
                throw new Exception($"A matching valid key couldn't be found in the store for issuer name '{options?.StoreCertificateConfiguration?.KeyStoreIssuer}' in Personal LocalMachine");
            }
        }

        private static void AddCertificateFromFile(IIdentityServerBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetCertificateFromFile(options?.FileCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.AddSigningCredential(certificate);
            }
            else
            {
                throw new Exception($"DataProtectionKeyStoreCredentialExtension cannot find key file {options?.FileCertificateConfiguration?.KeyFilePath}");
            }
        }

        private static void AddCertificateFromAzureKeyVault(IIdentityServerBuilder builder,
            CertificateConfiguration options, ILogger logger)
        {
            var certificate = CredentialHelper.GetFromAzure(options?.AzureCertificateConfiguration, logger);
            if (certificate != null)
            {
                builder.AddSigningCredential(certificate);
            }
            else
            {
                throw new Exception($"DataProtectionKeyStoreCredentialExtension cannot find key file {options?.FileCertificateConfiguration?.KeyFilePath}");
            }
        }
    }
}