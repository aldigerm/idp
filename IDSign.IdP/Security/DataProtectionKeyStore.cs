﻿using IDSign.IdP.Model;
using IDSign.IdP.Model.EF;
using Microsoft.AspNetCore.DataProtection.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace IDSign.IdP
{
    public class DataProtectionKeyStore : IXmlRepository
    {
        private readonly InternalModelContext _ctx;
        public DataProtectionKeyStore(InternalModelContext ctx)
        {
            _ctx = ctx;
        }

        public IReadOnlyCollection<XElement> GetAllElements()
        {

            return _ctx.XmlKeys
                .Select(x => XElement.Parse(x.Xml))
                .ToList();

        }

        public void StoreElement(XElement element, string friendlyName)
        {
            var key = new XmlKey
            {
                Id = Guid.NewGuid(),
                Xml = element.ToString(SaveOptions.DisableFormatting)
            };

            _ctx.XmlKeys.Add(key);

            _ctx.SaveChanges();

        }

    }
}
