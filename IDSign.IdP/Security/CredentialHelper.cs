﻿using IDSign.IdP.Model.Configuration;
using Microsoft.Azure.KeyVault;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace IdentityServerWithAspNetIdentity.Config
{
    /// <summary>
    /// Impl of adding a signin key for identity server 4,
    /// with an appsetting.json configuration look similar to:
    /// "SigninKeyCredentials": {
    ///     "KeyType": "KeyFile",
    ///     "KeyFilePath": "C:\\certificates\\idsv4.pfx",
    ///     "KeyStorePath": ""
    /// }
    /// </summary>
    public static class CredentialHelper
    {
        public const string KeyTypeKeyFile = "KeyFile";
        public const string KeyTypeKeyStore = "KeyStore";
        public const string KeyTypeAzureKeyVault = "AzureKeyVault";
        public const string KeyTypeTemporary = "Temporary";

        public static X509Certificate2 GetCertificateFromStore(
            StoreCertificateConfiguration storeCertificateConfiguration, ILogger logger)
        {
            if (storeCertificateConfiguration == null)
            {
                string message = "StoreCertificateConfiguration was not provided. Certificate couldn't be set up.";
                logger.LogError(message);
                throw new Exception(message);
            }

            var keyIssuer = storeCertificateConfiguration.KeyStoreIssuer;
            logger.LogDebug($"SigninCredentialExtension adding key from store by '{keyIssuer}' from 'StoreName.My'");

            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);

            if (store.Certificates.Count == 0)
            {
                logger.LogWarning("No certificates were found in the Personal LocalMachine");
            }
            else
            {
                foreach (var cert in store.Certificates)
                {
                    logger.LogDebug($"Found in Personal LocalMachine: '{cert.IssuerName.Name}'");
                }
            }

            var certificates = store.Certificates.Find(X509FindType.FindByIssuerName, keyIssuer, true);

            if (certificates.Count == 1)
            {
                logger.LogDebug($"Certificate found.'");
                return certificates[0];
            }
            else if (certificates.Count > 1)
            {
                string message = $"Total of ({certificates.Count}) matching keys were found in the store for issuer name '{keyIssuer}'. There needs to be only 1 matching key.";
                logger.LogError(message);
                throw new Exception(message);
            }
            else
            {
                string message = $"A matching valid key couldn't be found in the store for issuer name '{keyIssuer}' in Personal LocalMachine";
                logger.LogError(message);
                throw new Exception(message);
            }
        }

        public static X509Certificate2 GetCertificateFromFile(
            FileCertificateConfiguration fileCertificateConfiguration, ILogger logger)
        {
            if (fileCertificateConfiguration == null)
            {
                string message = "FileCertificateConfiguration was not provided. Certificate couldn't be set up.";
                logger.LogError(message);
                throw new Exception(message);
            }
            var keyFilePath = fileCertificateConfiguration.KeyFilePath;
            var keyFilePassword = fileCertificateConfiguration.KeyFilePassword;

            if (File.Exists(keyFilePath))
            {
                return new X509Certificate2(keyFilePath, keyFilePassword);
            }
            else
            {
                string message = $"SigninCredentialExtension cannot find key file {keyFilePath}";
                logger.LogError(message);
                throw new Exception(message);
            }
        }

        public static X509Certificate2 GetFromAzure(
            AzureCertificateConfiguration azureCertificateConfiguration, ILogger logger)
        {
            if (azureCertificateConfiguration == null)
            {
                string message = "AzureCertificateConfiguration was not provided. Certificate couldn't be set up.";
                logger.LogError(message);
                throw new Exception(message);
            }
            var clientId = azureCertificateConfiguration.ClientId;
            var clientSecret = azureCertificateConfiguration.ClientSecret;
            var secretIdentifier = azureCertificateConfiguration.SecretIdentifier;

            var keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(async (authority, resource, scope) =>
            {
                var authContext = new AuthenticationContext(authority);
                ClientCredential clientCreds = new ClientCredential(clientId, clientSecret);

                AuthenticationResult result = await authContext.AcquireTokenAsync(resource, clientCreds);

                if (result == null)
                {
                    throw new InvalidOperationException("Failed to obtain the JWT token");
                }

                return result.AccessToken;
            }));

            var pfxSecret = keyVaultClient.GetSecretAsync(secretIdentifier).Result;
            var pfxBytes = Convert.FromBase64String(pfxSecret.Value);
            var certificate = new X509Certificate2(pfxBytes);
            return certificate;
        }
    }
}