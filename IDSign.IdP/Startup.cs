﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServerWithAspNetIdentity.Config;
using IDSign.IdP.Core;
using IDSign.IdP.Core.Config;
using IDSign.IdP.Model.Configuration;
using IDSign.IdP.Model.EF;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.DataProtection.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace IDSign.IdP
{
    public class Startup
    {
        private readonly ILogger _logger;
        private readonly ILogger<FeatureHelper> _featureHelperLogger;
        private readonly ILoggerFactory _loggerFactory;

        public Startup(IConfiguration configuration
            , ILogger<FeatureHelper> featureHelperLogger
            , ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            _logger = loggerFactory.CreateLogger("StartUp");
            _featureHelperLogger = featureHelperLogger;
            _loggerFactory = loggerFactory;
        }

        public IConfiguration Configuration { get; }
        public IDSignIdpConfigurationSection IDSignConfiguration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            // get the settings root name for the idp
            string baseAppSettings = Configuration.GetValue<string>("AppSettingsName") ?? "IDSignIdP";

            // bind known properties
            IConfigurationSection sectionData = Configuration.GetSection(baseAppSettings);
            IDSignConfiguration = new IDSignIdpConfigurationSection();
            sectionData.Bind(IDSignConfiguration);

            Log.Logger.Information($"IDSignIdP settings read are :\n {HelpersService.DescribeObject(IDSignConfiguration, true)}");

            // other app settings can be read here
            string settingsName = IDSignConfiguration.SettingsName ?? "Settings";
            string settingsRoot = baseAppSettings + ":" + settingsName;
            _logger.LogInformation("Settings root read from : {0}", settingsRoot);

            // assign the settings to the config values where they will be read
            AppSettingsConfigValue.Init(settingsRoot, Configuration);

            // set the logger for FeatureHelper
            FeatureHelper.logger = _featureHelperLogger;


            string connectionString = "";
            connectionString = Configuration.GetConnectionString("DefaultConnection");

            var migrationsAssembly = typeof(IDSign.IdP.Model.EF.IdentityServerInitializer).GetTypeInfo().Assembly.GetName().Name;


            services.AddDbContext<InternalModelContext>(options =>
                    options.UseSqlServer(connectionString));

            services.AddIdentityServer(options =>
                    {
                        if (!String.IsNullOrWhiteSpace(IDSignConfiguration?.PublicOrigin))
                        {
                            options.PublicOrigin = IDSignConfiguration.PublicOrigin;
                        }
                        if (!String.IsNullOrWhiteSpace(IDSignConfiguration?.Issuer))
                        {
                            options.IssuerUri = IDSignConfiguration?.Issuer;
                        }
                        options.Events.RaiseSuccessEvents = true;
                        options.Events.RaiseFailureEvents = true;
                        options.Events.RaiseErrorEvents = true;
                    })
                // this adds the config data from DB (clients, resources, CORS)
                .AddConfigurationStore(options =>
                {
                    options.ResolveDbContextOptions = (provider, builder) =>
                    {
                        builder.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                    };
                })
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    if (IDSignConfiguration?.TokenCleanupInterval.HasValue == true)
                    {
                        options.TokenCleanupInterval = IDSignConfiguration.TokenCleanupInterval.Value; // interval in seconds, short for testing, default is 1 hour (3600 seconds)
                    }
                })
                .AddSigninCredentialFromConfig(IDSignConfiguration, _loggerFactory)
                .AddMultiModuleUserStore(IDSignConfiguration, _loggerFactory);

            // front
            services.AddCors();

            services.AddMvc()
                 .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton<IDSignIdpConfigurationSection>(IDSignConfiguration);

            services.AddDbContext<ModelContext>(options =>
                    options.UseSqlServer(connectionString));


            // automapper
            services.AddAutoMapper();


            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            // httpcontext is not injectable
            services.AddHttpContextAccessor();

            services.AddSingleton<IXmlRepository, DataProtectionKeyStore>();

            var serviceProvider = services.BuildServiceProvider();
            // for load balancing reasons
            services.AddDataProtection()
                    .AddKeyManagementOptions(options =>
                    {
                        options.XmlRepository = serviceProvider.GetService<IXmlRepository>();
                    })
                    .SetDefaultKeyLifetime(TimeSpan.FromDays(7))
                    .ProtectKeysWithCertificateFromConfig(IDSignConfiguration, _loggerFactory)
                    .SetApplicationName("IDSign.Idp");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            // this will do the initial DB population
            InitializeDatabase(app);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            var corsSettings = IDSignConfiguration?.CorsSettings;

            if (corsSettings != null && corsSettings.EnableCors)
            {
                if (corsSettings.AllowAnyOrigin)
                {
                    app.UseCors(
                        options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()
                    );
                }
                else if (corsSettings.WithOrigins.Any())
                {
                    app.UseCors(
                        options => options.WithOrigins(corsSettings.WithOrigins).AllowAnyHeader().AllowAnyMethod()
                    );
                }
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseIdentityServer();

            app.UseMvcWithDefaultRoute();


            string onlineUrlCheck = Configuration["OnlineUrlCheck"];
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    context.Request.Path.Value.ToLower().EndsWith(onlineUrlCheck))
                {
                    context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                    context.Response.ContentType = "text/plain";
                    await context.Response.WriteAsync("true");
                }
            });
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                // get contexts
                var configurationDbContext = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                var persistedGrantDbContext = serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>();
                var internalModelContext = serviceScope.ServiceProvider.GetRequiredService<InternalModelContext>();
                var logger = serviceScope.ServiceProvider.GetRequiredService<ILogger<IdentityServerInitializer>>();

                // initialise using 
                IdentityServerInitializer.InitializeDatabase(configurationDbContext, persistedGrantDbContext, internalModelContext, logger);

            }
        }
    }
}
