﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.Quickstart.Shared
{
    public class WebViewModel
    {
        public string ClientId { get; set; }
        public string CssPathValue { get; set; }
        public string FaviconPathValue
        {
            get
            {
                return CssPathValue;
            }
        }
        public string CacheBuster { get; set; }
        public IList<NotificationModel> Notifications { get; set; }
    }

}
