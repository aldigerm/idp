﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.Quickstart.Shared
{ 
    public class NotificationModel
    {
        public string Type { get; set; }
        public string Message { get; set; }
    }

    public class NotificationTypes
    {
        public const string Info = "info";
        public const string Success = "success";
        public const string Warning = "warning";
        public const string Error = "error";
    }
    public class NotificationMessages
    {
        public const string WelomeScreen = "Please check your phone for the OTP value and input it in the form below. You may ask for a new OTP if this is not found.";
        public const string NewOTPSent = "A new OTP was successfully sent. Please check your phone for this new OTP.";
    }
}
