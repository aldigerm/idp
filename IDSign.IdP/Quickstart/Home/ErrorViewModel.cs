﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using IDSign.IdP.Quickstart.Shared;

namespace IdentityServer4.Quickstart.UI
{
    public class ErrorViewModel : WebViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}