﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IDSign.IdP.Quickstart.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace IdentityServer4.Quickstart.UI
{
    public class LoginInputModel : WebViewModel
    { 
        public string Username { get; set; } 
        public string Password { get; set; }

        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public bool RememberLogin { get; set; }
        public string ReturnUrl { get; set; }
        public string TenantCode { get; set; }
        public string ProjectCode { get; set; }
        public string OTPValue { get; set; }
        public string Easid { get; set; }
        public DateTimeOffset? NextOTPRequestAllowedAt { get; set; }
        public string PhoneNumber { get; set; }
        public string ExpectedOTP { get; set; }
    }
}