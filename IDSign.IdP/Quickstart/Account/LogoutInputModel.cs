﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IDSign.IdP.Quickstart.Shared;

namespace IdentityServer4.Quickstart.UI
{
    public class LogoutInputModel : WebViewModel
    {
        public string LogoutId { get; set; }
    }
}
