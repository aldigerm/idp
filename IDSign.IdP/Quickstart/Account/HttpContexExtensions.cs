﻿using IDSign.IdP.Model.Constants;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IDSign.IdP.Quickstart.Account.Extensions
{

    public static class HttpContexExtensions
    {
        public static string ProviderId(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor?.HttpContext == null)
                return null;

            return httpContextAccessor.HttpContext.Items[RequestVariableName.ProviderId] as string;
        }

        public static void SetProviderId(this IHttpContextAccessor httpContextAccessor, string providerId)
        {
            if (httpContextAccessor?.HttpContext == null)
                return;

            httpContextAccessor.HttpContext.Items[RequestVariableName.ProviderId] = providerId;
        }

        public static string Username(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor?.HttpContext == null)
                return null;

            return httpContextAccessor.HttpContext.Items[RequestVariableName.Username] as string;
        }
        public static void SetUsername(this IHttpContextAccessor httpContextAccessor, string username)
        {
            if (httpContextAccessor?.HttpContext == null)
                return;

            httpContextAccessor.HttpContext.Items[RequestVariableName.Username] = username;
        }
    }
}
