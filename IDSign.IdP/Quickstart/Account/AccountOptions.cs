﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;

namespace IdentityServer4.Quickstart.UI
{
    public class AccountOptions
    {
        public static bool AllowLocalLogin = true;
        public static bool AllowRememberLogin = true;
        public static TimeSpan RememberMeLoginDuration = TimeSpan.FromDays(30);

        public static bool ShowLogoutPrompt = false;
        public static bool AutomaticRedirectAfterSignOut = true;

        // specify the Windows authentication scheme being used
        public static readonly string WindowsAuthenticationSchemeName = Microsoft.AspNetCore.Server.IISIntegration.IISDefaults.AuthenticationScheme;
        // if user uses windows auth, should we load the groups from windows
        public static bool IncludeWindowsGroups = false;

        public static string PasswordGenericErrorMessage = "Password couldn't be set";
        public static string PasswordConfirmMismatchErrorMessage = "Password and confirm password must match";
        public static string InvalidCredentialsErrorMessage = "Invalid username or password";
        public static string InvalidClientIdErrorMessage = "Invalid client id";
        public static string InvalidConfigurationCannotRedirect = "Cannot redirect to renew password due to wrong server configuration. Please contact administration.";
        public static string ExternalAccessSessionIdErrorMessage = "The session token you're using is either invalid or it has expired.";
        public static string RequestNewOTPErrorMessage = "An error occured while trying to send the OTP. Please try again or contact support if this problem persists.";
        public static string OTPNotSubmittedErrorMessage = "The OTP Value was not submitted.";
        public static string OTPSubmittedThrewErrorMessage = "An error occured while processing your OTP. Please try again or contact support if this problem persists.";
    }
}
