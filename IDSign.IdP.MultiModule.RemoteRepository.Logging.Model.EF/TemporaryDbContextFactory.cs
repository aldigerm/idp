﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF
{
    public class TemporaryLoggingDbContextFactory : IDesignTimeDbContextFactory<LoggingModelContext>
    { 
        public LoggingModelContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LoggingModelContext>();
            builder.UseSqlServer("Server=localhost;Database=IDSign.IdP.RemoteRepository;User ID=sa_idsign_idp_users_log;Password=sa_idsign_idp_users_log;MultipleActiveResultSets=true",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(LoggingModelContext).GetTypeInfo().Assembly.GetName().Name));
            return new LoggingModelContext(builder.Options);
        }
    }
}
