﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDSign.IdP.Core.Config;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF
{
    public static class Constants
    {
        public static string LoggingDatabaseSchema { get; } = new AppSettingsConfigValue("Logging.DatabaseSchema", "log");
    }
}
