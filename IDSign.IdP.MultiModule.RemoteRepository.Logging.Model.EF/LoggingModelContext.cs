﻿
using IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF.Mapping;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Reflection;
using System.Security.Claims;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF
{

    public class LoggingModelContext : DbContext
    {
        public LoggingModelContext(
            DbContextOptions<LoggingModelContext> nameOrConnectionString )
            : base(nameOrConnectionString)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema(Constants.LoggingDatabaseSchema);

            var entConfigType = typeof(IEntityConfig);

            // Init types
            Assembly.GetAssembly(entConfigType)
                    .GetTypes()
                    .Where(type => entConfigType.IsAssignableFrom(type) && type != entConfigType)
                    .ToList()
                    .ForEach(type => ((IEntityConfig)Activator.CreateInstance(type)).MapEntity(modelBuilder));

            // set all foreign keys to thrown an error if there is a violation
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }
    }
}