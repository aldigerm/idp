﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF.Migrations
{
    public partial class start : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "log");

            migrationBuilder.CreateTable(
                name: "ApiLog",
                schema: "log",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionId = table.Column<string>(maxLength: 128, nullable: false),
                    CompanyCode = table.Column<string>(maxLength: 50, nullable: true),
                    User = table.Column<string>(maxLength: 50, nullable: true),
                    RequestIpAddress = table.Column<string>(maxLength: 39, nullable: false),
                    RequestContentType = table.Column<string>(maxLength: 50, nullable: true),
                    RequestContentBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestScheme = table.Column<string>(maxLength: 10, nullable: false),
                    RequestAuthority = table.Column<string>(maxLength: 256, nullable: false),
                    RequestAbsolutePath = table.Column<string>(type: "nvarchar(256)", nullable: false),
                    RequestQuery = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestMethod = table.Column<string>(maxLength: 10, nullable: false),
                    RequestHeaders = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Server = table.Column<string>(maxLength: 50, nullable: false),
                    RequestMachineName = table.Column<string>(maxLength: 256, nullable: true),
                    Application = table.Column<string>(maxLength: 50, nullable: false),
                    ResponseContentType = table.Column<string>(maxLength: 50, nullable: true),
                    ResponseContentBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponseStatusCode = table.Column<int>(nullable: true),
                    ResponseHeaders = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponseTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    ErrorStackTrace = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ErrorMessage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ErrorType = table.Column<string>(maxLength: 1024, nullable: true),
                    ErrorTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuditLog",
                schema: "log",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionId = table.Column<string>(maxLength: 128, nullable: false),
                    CompanyCode = table.Column<string>(maxLength: 50, nullable: true),
                    User = table.Column<string>(maxLength: 50, nullable: true),
                    RequestIpAddress = table.Column<string>(maxLength: 39, nullable: false),
                    RequestContentType = table.Column<string>(maxLength: 50, nullable: true),
                    RequestContentBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestScheme = table.Column<string>(maxLength: 10, nullable: false),
                    RequestAuthority = table.Column<string>(maxLength: 256, nullable: false),
                    RequestAbsolutePath = table.Column<string>(type: "nvarchar(256)", nullable: false),
                    RequestQuery = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestMethod = table.Column<string>(maxLength: 10, nullable: false),
                    RequestHeaders = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Server = table.Column<string>(maxLength: 50, nullable: false),
                    RequestMachineName = table.Column<string>(maxLength: 256, nullable: true),
                    Application = table.Column<string>(maxLength: 50, nullable: false),
                    AuditEventType = table.Column<string>(maxLength: 128, nullable: false),
                    ClientUserAgent = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ResponseTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    ErrorTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClientErrorLog",
                schema: "log",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionId = table.Column<string>(maxLength: 128, nullable: false),
                    CompanyCode = table.Column<string>(maxLength: 50, nullable: true),
                    User = table.Column<string>(maxLength: 50, nullable: true),
                    RequestIpAddress = table.Column<string>(maxLength: 39, nullable: false),
                    RequestContentType = table.Column<string>(maxLength: 50, nullable: true),
                    RequestContentBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestScheme = table.Column<string>(maxLength: 10, nullable: false),
                    RequestAuthority = table.Column<string>(maxLength: 256, nullable: false),
                    RequestAbsolutePath = table.Column<string>(type: "nvarchar(256)", nullable: false),
                    RequestQuery = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestMethod = table.Column<string>(maxLength: 10, nullable: false),
                    RequestHeaders = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Server = table.Column<string>(maxLength: 50, nullable: false),
                    RequestMachineName = table.Column<string>(maxLength: 256, nullable: true),
                    Application = table.Column<string>(maxLength: 50, nullable: false),
                    ErrorSource = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ErrorMessage = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ClientUserAgent = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ErrorTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientErrorLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ErrorLog",
                schema: "log",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConnectionId = table.Column<string>(maxLength: 128, nullable: false),
                    CompanyCode = table.Column<string>(maxLength: 50, nullable: true),
                    User = table.Column<string>(maxLength: 50, nullable: true),
                    RequestIpAddress = table.Column<string>(maxLength: 39, nullable: false),
                    RequestContentType = table.Column<string>(maxLength: 50, nullable: true),
                    RequestContentBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestScheme = table.Column<string>(maxLength: 10, nullable: false),
                    RequestAuthority = table.Column<string>(maxLength: 256, nullable: false),
                    RequestAbsolutePath = table.Column<string>(type: "nvarchar(256)", nullable: false),
                    RequestQuery = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequestMethod = table.Column<string>(maxLength: 10, nullable: false),
                    RequestHeaders = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Server = table.Column<string>(maxLength: 50, nullable: false),
                    RequestMachineName = table.Column<string>(maxLength: 256, nullable: true),
                    Application = table.Column<string>(maxLength: 50, nullable: false),
                    ErrorStackTrace = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ErrorMessage = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ErrorType = table.Column<string>(type: "nvarchar(1024)", nullable: true),
                    ErrorTimestamp = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ErrorLog", x => x.Id);
                });
            migrationBuilder.Sql(@"
IF EXISTS ( SELECT * 
			FROM   sysobjects 
			WHERE  id = object_id(N'[log].[LOG001_AuditLog]') 
				   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [log].[LOG001_AuditLog]
END
GO

-- =============================================
-- Author:		Michael Aquilina
-- Description:	Handles audit calls
-- Audit :	2018/03/01	Created
-- =============================================
CREATE  PROCEDURE [log].[LOG001_AuditLog]
	-- Add the parameters for the stored procedure here
@log XML,
@id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	 DECLARE @log_id INT = @log.value('(//AuditLog/Id)[1]', 'INT');
	 SET @id = @log_id
	
	 DECLARE @ConnectionId			NVARCHAR(MAX)  =  @log.value('(//AuditLog/ConnectionId)[1]',		'NVARCHAR(MAX)');
	 DECLARE @CompanyCode			NVARCHAR(MAX)  =  @log.value('(//AuditLog/CompanyCode)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @User					NVARCHAR(MAX)  =  @log.value('(//AuditLog/User)[1]', 				'NVARCHAR(MAX)');
	 DECLARE @RequestIpAddress		NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestIpAddress)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestContentType	NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestContentType)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestContentBody	NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestContentBody)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestScheme			NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestScheme)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestAuthority		NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestAuthority)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestAbsolutePath	NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestAbsolutePath)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestQuery			NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestQuery)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestMethod			NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestMethod)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestHeaders		NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestHeaders)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @LogTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//AuditLog/LogTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);
	 DECLARE @Server				NVARCHAR(MAX)  =  @log.value('(//AuditLog/Server)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @RequestMachineName				NVARCHAR(MAX)  =  @log.value('(//AuditLog/RequestMachineName)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @Application			NVARCHAR(MAX)  =  @log.value('(//AuditLog/Application)[1]', 			'NVARCHAR(MAX)');

	 DECLARE @AuditEventType		NVARCHAR(MAX)  =  @log.value('(//AuditLog/AuditEventType)[1]', 		'NVARCHAR(MAX)');
	 --DECLARE @AuditData				NVARCHAR(MAX)  =  @log.value('(//AuditLog/AuditData)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ClientUserAgent		NVARCHAR(MAX)  =  @log.value('(//AuditLog/ClientUserAgent)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @ResponseTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//AuditLog/ResponseTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);
	 DECLARE @ErrorTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//AuditLog/ErrorTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);

	 -- if no id is given or the id is 0, then we insert
	 IF(@log_id IS NULL OR @log_id = 0)
		 BEGIN

			INSERT INTO [log].[AuditLog]
					   ([ConnectionId]
					   ,[CompanyCode]
					   ,[User]
					   ,[RequestIpAddress]
					   ,[RequestContentType]
					   ,[RequestContentBody]
					   ,[RequestScheme]
					   ,[RequestAuthority]
					   ,[RequestAbsolutePath]
					   ,[RequestQuery]
					   ,[RequestMethod]
					   ,[RequestHeaders]
					   ,[LogTimestamp]
					   ,[Server]
					   ,[RequestMachineName]
					   ,[Application]

					   ,[AuditEventType]
					   --,[AuditData]
					   ,[ClientUserAgent]
					   ,[ResponseTimestamp]
					   ,[ErrorTimestamp])
				 VALUES
				 (
				 @ConnectionId			,  --(<ConnectionId, varchar(128),>
				 @CompanyCode			,  --,<CompanyCode, varchar(50),>
				 @User					,  --,<User, varchar(50),>
				 @RequestIpAddress		,  --,<RequestIpAddress, varchar(39),>
				 @RequestContentType	,  --,<RequestContentType, varchar(50),>
				 @RequestContentBody	,  --,<RequestContentBody, nvarchar(max),>
				 @RequestScheme			,  --,<RequestScheme, varchar(10),>
				 @RequestAuthority		,  --,<RequestAuthority, nvarchar(256),>
				 @RequestAbsolutePath	,  --,<RequestAbsolutePath, nvarchar(256),>
				 @RequestQuery			,  --,<RequestQuery, nvarchar(max),>
				 @RequestMethod			,  --,<RequestMethod, varchar(10),>
				 @RequestHeaders		,  --,<RequestHeaders, nvarchar(max),>
				 @LogTimestamp		,  --,<LogTimestamp, datetimeoffset(7),>
				 @Server				,  --,<Server, varchar(50),>
				 @RequestMachineName    ,
				 @Application			,

				 @AuditEventType		,  --,<AuditEventType, nvarchar(128),>
				-- @AuditData				,  --,<AuditData, nvarchar(max),>
				 @ClientUserAgent		,  --,<ClientUserAgent, nvarchar(max),>
				 @ResponseTimestamp		,  --,<ResponseTimestamp, datetimeoffset(7),>
				 @ErrorTimestamp		)  --,<ErrorTimestamp, datetimeoffset(7),>)
		
				SET @id = SCOPE_IDENTITY()
			END
		ELSE
			 BEGIN
			  UPDATE [log].[AuditLog] WITH (ROWLOCK)
			  SET 
			   [ConnectionId] =				@ConnectionId				
			  ,[CompanyCode] =				@CompanyCode				
			  ,[User] =						@User						
			  ,[RequestIpAddress] =			@RequestIpAddress			
			  ,[RequestContentType] =		@RequestContentType		
			  ,[RequestContentBody] =		@RequestContentBody		
			  ,[RequestScheme] =			@RequestScheme				
			  ,[RequestAuthority] =			@RequestAuthority			
			  ,[RequestAbsolutePath] =		@RequestAbsolutePath		
			  ,[RequestQuery] =				@RequestQuery				
			  ,[RequestMethod] =			@RequestMethod				
			  ,[RequestHeaders] =			@RequestHeaders			
			  ,[LogTimestamp] =			@LogTimestamp			
			  ,[Server] =					@Server		
					,[RequestMachineName] =		@RequestMachineName		

			  ,[Application] =				@Application
			  ,[AuditEventType] =			@AuditEventType			
			 -- ,[AuditData] =				@AuditData					
			  ,[ClientUserAgent] =			@ClientUserAgent			
			  ,[ResponseTimestamp] =		@ResponseTimestamp			
			  ,[ErrorTimestamp] =			@ErrorTimestamp		
			  WHERE Id = @log_id

			  SET @id = @log_id
			END
	
	RETURN @Id

END
		   
GO  

IF EXISTS ( SELECT * 
			FROM   sysobjects 
			WHERE  id = object_id(N'[log].[LOG002_ApiLog]') 
				   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [log].[LOG002_ApiLog]
END
GO

-- =============================================
-- Author:		Michael Aquilina
-- Description:	Handles api calls
-- Audit :	2018/03/01	Created
--			2018/07/19	Fixed timestamps
-- =============================================
CREATE  PROCEDURE [log].[LOG002_ApiLog]
	-- Add the parameters for the stored procedure here
@log XML,
@id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	 DECLARE @log_id INT = @log.value('(//ApiLog/Id)[1]', 'INT');
	 SET @id = @log_id
	
	 DECLARE @ConnectionId			NVARCHAR(MAX)  =  @log.value('(//ApiLog/ConnectionId)[1]',		'NVARCHAR(MAX)');
	 DECLARE @CompanyCode			NVARCHAR(MAX)  =  @log.value('(//ApiLog/CompanyCode)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @User					NVARCHAR(MAX)  =  @log.value('(//ApiLog/User)[1]', 				'NVARCHAR(MAX)');
	 DECLARE @RequestIpAddress		NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestIpAddress)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestContentType	NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestContentType)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestContentBody	NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestContentBody)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestScheme			NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestScheme)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestAuthority		NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestAuthority)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestAbsolutePath	NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestAbsolutePath)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestQuery			NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestQuery)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestMethod			NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestMethod)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestHeaders		NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestHeaders)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @LogTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ApiLog/LogTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);
	 DECLARE @Server				NVARCHAR(MAX)  =  @log.value('(//ApiLog/Server)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @RequestMachineName				NVARCHAR(MAX)  =  @log.value('(//ApiLog/RequestMachineName)[1]', 			'NVARCHAR(MAX)');

	 DECLARE @Application		    NVARCHAR(MAX)  =  @log.value('(//ApiLog/Application)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ResponseContentType	NVARCHAR(MAX)  =  @log.value('(//ApiLog/ResponseContentType)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ResponseContentBody	NVARCHAR(MAX)  =  @log.value('(//ApiLog/ResponseContentBody)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ResponseStatusCode	NVARCHAR(MAX)  =  @log.value('(//ApiLog/ResponseStatusCode)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ResponseHeaders		NVARCHAR(MAX)  =  @log.value('(//ApiLog/ResponseHeaders)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ResponseTimestamp		datetimeoffset  = CAST(NULLIF(@log.value('(//ApiLog/ResponseTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);	 
	 DECLARE @ErrorStackTrace		NVARCHAR(MAX)  =  @log.value('(//ApiLog/ErrorStackTrace)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @ErrorMessage			NVARCHAR(MAX)  =  @log.value('(//ApiLog/ErrorMessage)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @ErrorType				NVARCHAR(MAX)  =  @log.value('(//ApiLog/ErrorType)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ErrorTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ApiLog/ErrorTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);

	 -- if no id is given or the id is 0, then we insert
	 IF(@log_id IS NULL OR @log_id = 0)
		 BEGIN

			INSERT INTO [log].[ApiLog]
		   ([ConnectionId]
		   ,[CompanyCode]
		   ,[User]
		   ,[RequestIpAddress]
		   ,[RequestContentType]
		   ,[RequestContentBody]
		   ,[RequestScheme]
		   ,[RequestAuthority]
		   ,[RequestAbsolutePath]
		   ,[RequestQuery]
		   ,[RequestMethod]
		   ,[RequestHeaders]
		   ,[LogTimestamp]
		   ,[Server]
			,[RequestMachineName]
		   ,[Application]

		   ,[ResponseContentType]
		   ,[ResponseContentBody]
		   ,[ResponseStatusCode]
		   ,[ResponseHeaders]
		   ,[ResponseTimestamp]
		   ,[ErrorStackTrace]
		   ,[ErrorMessage]
		   ,[ErrorType]
		   ,[ErrorTimestamp])
				 VALUES
				 (
				 @ConnectionId			,	--<ConnectionId, varchar(128),>
				 @CompanyCode			,	--<CompanyCode, varchar(50),>
				 @User					,	--<User, varchar(50),>
				 @RequestIpAddress		,	--<RequestIpAddress, varchar(39),>
				 @RequestContentType	,	--<RequestContentType, varchar(50),>
				 @RequestContentBody	,	--<RequestContentBody, nvarchar(max),>
				 @RequestScheme			,	--<RequestScheme, varchar(10),>
				 @RequestAuthority		,	--<RequestAuthority, nvarchar(256),>
				 @RequestAbsolutePath	,	--<RequestAbsolutePath, nvarchar(256),>
				 @RequestQuery			,	--<RequestQuery, nvarchar(max),>
				 @RequestMethod			,	--<RequestMethod, varchar(10),>
				 @RequestHeaders		,	--<RequestHeaders, nvarchar(max),>
				 @LogTimestamp		,	--<LogTimestamp, datetimeoffset(7),>
				 @Server				,	--<Server, varchar(50),>
				 @RequestMachineName,
				 @Application		    ,	--<Application, varchar(50),>

				 @ResponseContentType	,	--<ResponseContentType, varchar(50),>
				 @ResponseContentBody	,	--<ResponseContentBody, nvarchar(max),>
				 @ResponseStatusCode	,	--<ResponseStatusCode, int,>,>
				 @ResponseHeaders		,	--<ResponseHeaders, nvarchar(max),>
				 @ResponseTimestamp		,	--<ResponseTimestamp, datetimeoffset(7)
				 @ErrorStackTrace		,	--<ErrorStackTrace, nvarchar(max),>
				 @ErrorMessage			,	--<ErrorMessage, nvarchar(max),>
				 @ErrorType				,	--<ErrorType, nvarchar(1024),>
				 @ErrorTimestamp			--<ErrorTimestamp, datetimeoffset(7),>)
				 )		
				SET @id = SCOPE_IDENTITY()
			END
		ELSE
			 BEGIN
			  UPDATE [log].[ApiLog]
				 SET [ConnectionId] =			 @ConnectionId					
					,[CompanyCode] =			 @CompanyCode					
					,[User] =					 @User							
					,[RequestIpAddress] =		 @RequestIpAddress				
					,[RequestContentType] =		 @RequestContentType		
					,[RequestContentBody] =		 @RequestContentBody		
					,[RequestScheme] =			 @RequestScheme				
					,[RequestAuthority] =		 @RequestAuthority				
					,[RequestAbsolutePath] =	 @RequestAbsolutePath			
					,[RequestQuery] =			 @RequestQuery					
					,[RequestMethod] =			 @RequestMethod				
					,[RequestHeaders] =			 @RequestHeaders			
					,[LogTimestamp] =		 @LogTimestamp				
					,[Server] =				 @Server	
					,[RequestMachineName] =		@RequestMachineName							
					,[Application] =			 @Application		
								
					,[ResponseContentType] =	 @ResponseContentType			
					,[ResponseContentBody] =	 @ResponseContentBody			
					,[ResponseStatusCode] =		 @ResponseStatusCode		
					,[ResponseHeaders] =		 @ResponseHeaders				
					,[ResponseTimestamp] =		 @ResponseTimestamp			
					,[ErrorStackTrace] =		 @ErrorStackTrace				
					,[ErrorMessage] =			 @ErrorMessage					
					,[ErrorType] =				 @ErrorType					
					,[ErrorTimestamp] =			 @ErrorTimestamp			
			  WHERE Id = @log_id

			  SET @id = @log_id
			END
	
	RETURN @Id

END
		   
GO  

IF EXISTS ( SELECT * 
			FROM   sysobjects 
			WHERE  id = object_id(N'[log].[LOG003_ErrorLog]') 
				   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [log].[LOG003_ErrorLog]
END
GO
-- =============================================
-- Author:		Michael Aquilina
-- Description:	Handles error calls
-- Audit :	2018/03/02	Created
-- =============================================
CREATE PROCEDURE [log].[LOG003_ErrorLog]
	-- Add the parameters for the stored procedure here
@log XML,
@id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	 DECLARE @log_id INT = @log.value('(//ErrorLog/Id)[1]', 'INT');
	 SET @id = @log_id
	
	 DECLARE @ConnectionId			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/ConnectionId)[1]',		'NVARCHAR(MAX)');
	 DECLARE @CompanyCode			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/CompanyCode)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @User					NVARCHAR(MAX)  =  @log.value('(//ErrorLog/User)[1]', 				'NVARCHAR(MAX)');
	 DECLARE @RequestIpAddress		NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestIpAddress)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestContentType	NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestContentType)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestContentBody	NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestContentBody)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestScheme			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestScheme)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestAuthority		NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestAuthority)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestAbsolutePath	NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestAbsolutePath)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestQuery			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestQuery)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestMethod			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestMethod)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestHeaders		NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestHeaders)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @LogTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ErrorLog/LogTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);
	 DECLARE @Server				NVARCHAR(MAX)  =  @log.value('(//ErrorLog/Server)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @RequestMachineName				NVARCHAR(MAX)  =  @log.value('(//ErrorLog/RequestMachineName)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @Application			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/Application)[1]', 			'NVARCHAR(MAX)');

	 DECLARE @ErrorStackTrace		NVARCHAR(MAX)  =  @log.value('(//ErrorLog/ErrorStackTrace)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @ErrorMessage			NVARCHAR(MAX)  =  @log.value('(//ErrorLog/ErrorMessage)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @ErrorType				NVARCHAR(MAX)  =  @log.value('(//ErrorLog/ErrorType)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ErrorTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ErrorLog/ErrorTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);

	 -- if no id is given or the id is 0, then we insert
	 IF(@log_id IS NULL OR @log_id = 0)
		 BEGIN

			INSERT INTO [log].[ErrorLog]
				   ([ConnectionId]
				   ,[CompanyCode]
				   ,[User]
				   ,[RequestIpAddress]
				   ,[RequestContentType]
				   ,[RequestContentBody]
				   ,[RequestScheme]
				   ,[RequestAuthority]
				   ,[RequestAbsolutePath]
				   ,[RequestQuery]
				   ,[RequestMethod]
				   ,[RequestHeaders]
				   ,[LogTimestamp]
				   ,[Server]
				   ,[RequestMachineName]
				   ,[Application]
				   
				   ,[ErrorStackTrace]
				   ,[ErrorMessage]
				   ,[ErrorType]
				   ,[ErrorTimestamp])
			 VALUES
				  (  @ConnectionId			-- <ConnectionId, varchar(128),>
				   , @CompanyCode			-- <CompanyCode, varchar(50),>
				   , @User					-- <User, varchar(50),>
				   , @RequestIpAddress		-- <RequestIpAddress, varchar(39),>
				   , @RequestContentType	-- <RequestContentType, varchar(50),>
				   , @RequestContentBody	-- <RequestContentBody, nvarchar(max),>
				   , @RequestScheme			-- <RequestScheme, varchar(10),>
				   , @RequestAuthority		-- <RequestAuthority, nvarchar(256),>
				   , @RequestAbsolutePath	-- <RequestAbsolutePath, nvarchar(256),>
				   , @RequestQuery			-- <RequestQuery, nvarchar(max),>
				   , @RequestMethod			-- <RequestMethod, varchar(10),>
				   , @RequestHeaders		-- <RequestHeaders, nvarchar(max),>
				   , @LogTimestamp		-- <LogTimestamp, datetimeoffset(7),>
				   , @Server				-- <Server, varchar(50),>
				   , @RequestMachineName
				   , @Application			-- <Application, varchar(50),

				   , @ErrorStackTrace		-- <ErrorStackTrace, nvarchar(max),>
				   , @ErrorMessage			-- <ErrorMessage, nvarchar(max),>
				   , @ErrorType				-- <ErrorType, nvarchar(1024),>
				   , @ErrorTimestamp		-- <ErrorTimestamp, datetimeoffset(7),>	
						 )		
				SET @id = SCOPE_IDENTITY()
			END
		ELSE
			 BEGIN
			  UPDATE [log].[ErrorLog]
				 SET [ConnectionId] =			 @ConnectionId					
					,[CompanyCode] =			 @CompanyCode					
					,[User] =					 @User							
					,[RequestIpAddress] =		 @RequestIpAddress				
					,[RequestContentType] =		 @RequestContentType		
					,[RequestContentBody] =		 @RequestContentBody		
					,[RequestScheme] =			 @RequestScheme				
					,[RequestAuthority] =		 @RequestAuthority				
					,[RequestAbsolutePath] =	 @RequestAbsolutePath			
					,[RequestQuery] =			 @RequestQuery					
					,[RequestMethod] =			 @RequestMethod				
					,[RequestHeaders] =			 @RequestHeaders			
					,[LogTimestamp] =		 @LogTimestamp				
					,[Server] =				 @Server		
					,[RequestMachineName] =		@RequestMachineName		
					
					,[Application] =			 @Application		
					,[ErrorStackTrace] =		 @ErrorStackTrace				
					,[ErrorMessage] =			 @ErrorMessage					
					,[ErrorType] =				 @ErrorType					
					,[ErrorTimestamp] =			 @ErrorTimestamp	
			  WHERE Id = @log_id

			  SET @id = @log_id
			END
	
	RETURN @Id

END
		   
GO  
IF EXISTS ( SELECT * 
			FROM   sysobjects 
			WHERE  id = object_id(N'[log].[LOG004_ClientClientErrorLog]') 
				   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN
	DROP PROCEDURE [log].[LOG004_ClientClientErrorLog]
END
GO
-- =============================================
-- Author:		Michael Aquilina
-- Description:	Handles error calls
-- Audit :	2018/03/02	Created
-- =============================================
CREATE PROCEDURE [log].[LOG004_ClientClientErrorLog]
	-- Add the parameters for the stored procedure here
@log XML,
@id INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	 DECLARE @log_id INT = @log.value('(//ClientErrorLog/Id)[1]', 'INT');
	 SET @id = @log_id
	
	 DECLARE @ConnectionId			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/ConnectionId)[1]',		'NVARCHAR(MAX)');
	 DECLARE @CompanyCode			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/CompanyCode)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @User					NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/User)[1]', 				'NVARCHAR(MAX)');
	 DECLARE @RequestIpAddress		NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestIpAddress)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestContentType	NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestContentType)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestContentBody	NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestContentBody)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestScheme			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestScheme)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestAuthority		NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestAuthority)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @RequestAbsolutePath	NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestAbsolutePath)[1]',	'NVARCHAR(MAX)');
	 DECLARE @RequestQuery			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestQuery)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestMethod			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestMethod)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @RequestHeaders		NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestHeaders)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @LogTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ClientErrorLog/LogTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);
	 DECLARE @Server				NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/Server)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @RequestMachineName				NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/RequestMachineName)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @Application			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/Application)[1]', 			'NVARCHAR(MAX)');

	 DECLARE @ErrorSource			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/ErrorSource)[1]', 	'NVARCHAR(MAX)');
	 DECLARE @ErrorMessage			NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/ErrorMessage)[1]', 		'NVARCHAR(MAX)');
	 DECLARE @ClientUserAgent		NVARCHAR(MAX)  =  @log.value('(//ClientErrorLog/ClientUserAgent)[1]', 			'NVARCHAR(MAX)');
	 DECLARE @ErrorTimestamp		datetimeoffset =  CAST(NULLIF(@log.value('(//ClientErrorLog/ErrorTimestamp)[1]', 'varchar(40)'),'') as	datetimeoffset);

	 -- if no id is given or the id is 0, then we insert
	 IF(@log_id IS NULL OR @log_id = 0)
		 BEGIN

			INSERT INTO [log].[ClientErrorLog]
				   ([ConnectionId]
				   ,[CompanyCode]
				   ,[User]
				   ,[RequestIpAddress]
				   ,[RequestContentType]
				   ,[RequestContentBody]
				   ,[RequestScheme]
				   ,[RequestAuthority]
				   ,[RequestAbsolutePath]
				   ,[RequestQuery]
				   ,[RequestMethod]
				   ,[RequestHeaders]
				   ,[LogTimestamp]
				   ,[Server]
				   ,[RequestMachineName]
				   ,[Application]
				   
				  ,[ErrorSource]
				  ,[ErrorMessage]
				  ,[ClientUserAgent]
				  ,[ErrorTimestamp] )
			 VALUES
				  (  @ConnectionId			-- <ConnectionId, varchar(128),>
				   , @CompanyCode			-- <CompanyCode, varchar(50),>
				   , @User					-- <User, varchar(50),>
				   , @RequestIpAddress		-- <RequestIpAddress, varchar(39),>
				   , @RequestContentType	-- <RequestContentType, varchar(50),>
				   , @RequestContentBody	-- <RequestContentBody, nvarchar(max),>
				   , @RequestScheme			-- <RequestScheme, varchar(10),>
				   , @RequestAuthority		-- <RequestAuthority, nvarchar(256),>
				   , @RequestAbsolutePath	-- <RequestAbsolutePath, nvarchar(256),>
				   , @RequestQuery			-- <RequestQuery, nvarchar(max),>
				   , @RequestMethod			-- <RequestMethod, varchar(10),>
				   , @RequestHeaders		-- <RequestHeaders, nvarchar(max),>
				   , @LogTimestamp		-- <LogTimestamp, datetimeoffset(7),>
				   , @Server				-- <Server, varchar(50),>
				   , @RequestMachineName
				   , @Application			-- <Application, varchar(50),

				   , @ErrorSource			-- <ErrorStackTrace, nvarchar(max),>
				   , @ErrorMessage			-- <ErrorMessage, nvarchar(max),>
				   , @ClientUserAgent		-- <ErrorType, nvarchar(1024),>
				   , @ErrorTimestamp		-- <ErrorTimestamp, datetimeoffset(7),>	
						 )		
				SET @id = SCOPE_IDENTITY()
			END
		ELSE
			 BEGIN
			  UPDATE [log].[ClientErrorLog]
				 SET [ConnectionId] =			 @ConnectionId					
					,[CompanyCode] =			 @CompanyCode					
					,[User] =					 @User							
					,[RequestIpAddress] =		 @RequestIpAddress				
					,[RequestContentType] =		 @RequestContentType		
					,[RequestContentBody] =		 @RequestContentBody		
					,[RequestScheme] =			 @RequestScheme				
					,[RequestAuthority] =		 @RequestAuthority				
					,[RequestAbsolutePath] =	 @RequestAbsolutePath			
					,[RequestQuery] =			 @RequestQuery					
					,[RequestMethod] =			 @RequestMethod				
					,[RequestHeaders] =			 @RequestHeaders			
					,[LogTimestamp] =			@LogTimestamp				
					,[Server] =					@Server		
					,[RequestMachineName] =		@RequestMachineName			
					,[Application] =			 @Application	
						
					,[ErrorSource] =			 @ErrorSource		
					,[ErrorMessage]	=			 @ErrorMessage		
					,[ClientUserAgent] =		 @ClientUserAgent	
					,[ErrorTimestamp] = 		 @ErrorTimestamp	
			  WHERE Id = @log_id

			  SET @id = @log_id
			END
	
	RETURN @Id

END
		   
GO  
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"

DROP PROCEDURE [log].[LOG001_AuditLog]
GO

DROP PROCEDURE [log].[LOG002_ApiLog]
GO

DROP PROCEDURE [log].[LOG003_ErrorLog]
GO

DROP PROCEDURE [log].[LOG004_ClientClientErrorLog]
GO
");
            migrationBuilder.DropTable(
                name: "ApiLog",
                schema: "log");

            migrationBuilder.DropTable(
                name: "AuditLog",
                schema: "log");

            migrationBuilder.DropTable(
                name: "ClientErrorLog",
                schema: "log");

            migrationBuilder.DropTable(
                name: "ErrorLog",
                schema: "log");
        }
    }
}
