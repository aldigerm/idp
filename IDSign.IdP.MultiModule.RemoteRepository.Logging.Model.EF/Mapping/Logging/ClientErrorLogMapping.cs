﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF.Mapping
{
    public class ClientErrorLogMapping : IEntityConfig
    {
        public void MapEntity(ModelBuilder builder)
        {
            Map(builder.Entity<ClientErrorLog>());
        }
        private void Map(EntityTypeBuilder<ClientErrorLog> builder)
        {
            builder.ToTable("ClientErrorLog");

            #region ILoggingObject
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id);

            builder.Property(x => x.ConnectionId).IsRequired().HasMaxLength(128);
            builder.Property(x => x.User).HasMaxLength(50);
            builder.Property(x => x.CompanyCode).HasMaxLength(50);
            builder.Property(x => x.Server).IsRequired().HasMaxLength(50);
            builder.Property(x => x.RequestMachineName).HasMaxLength(256);
            builder.Property(x => x.RequestIpAddress).IsRequired().HasMaxLength(39);
            builder.Property(x => x.RequestContentType).HasMaxLength(50);
            builder.Property(x => x.RequestContentBody).HasColumnType("nvarchar(max)");
            builder.Property(x => x.RequestScheme).IsRequired().HasMaxLength(10);
            builder.Property(x => x.RequestAuthority).IsRequired().HasMaxLength(256);
            builder.Property(x => x.RequestAbsolutePath).IsRequired().HasColumnType("nvarchar(256)");
            builder.Property(x => x.RequestQuery).HasColumnType("nvarchar(max)");
            builder.Property(x => x.RequestMethod).IsRequired().HasMaxLength(10);
            builder.Property(x => x.RequestHeaders).HasColumnType("nvarchar(max)");
            builder.Property(x => x.LogTimestamp).IsRequired().HasColumnType("datetimeoffset");
            builder.Property(x => x.Application).IsRequired().HasMaxLength(50);
            #endregion

            builder.Property(x => x.ErrorMessage).IsRequired().HasColumnType("nvarchar(max)");
            builder.Property(x => x.ErrorSource).IsRequired().HasColumnType("nvarchar(max)");
            builder.Property(x => x.ClientUserAgent).IsRequired().HasColumnType("nvarchar(max)");
            builder.Property(x => x.ErrorTimestamp).IsRequired().HasColumnType("datetimeoffset");
        }
    }
}
