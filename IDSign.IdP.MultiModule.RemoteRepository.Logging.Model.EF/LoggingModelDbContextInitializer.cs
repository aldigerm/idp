﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF
{
    public class LoggingModelDbContextInitializer
    {
        public static void InitializeDatabase(LoggingModelContext context, ILogger<LoggingModelDbContextInitializer> logger)
        {
            try
            {
                context.Database.Migrate();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                logger.LogError(e, $"Database migration threw an error -> {e.Message}");
            }
        }
    }
}
