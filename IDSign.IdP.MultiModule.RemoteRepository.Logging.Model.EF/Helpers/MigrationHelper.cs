﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSign.IdP.MultiModule.RemoteRepository.Logging.Model.EF.Helpers
{
    public class MigrationHelper
    {
        public static string GetSqlFromFile(string fileNameWithoutExtension)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\", "Migrations", fileNameWithoutExtension + ".sql");
            return File.ReadAllText(sqlFile);
        }
        public static string GetRevertSqlFromFile(string upFileNameWithoutExtension)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\", "Migrations", upFileNameWithoutExtension + "_Revert.sql");
            return File.ReadAllText(sqlFile);
        }
    }
}
